﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AtlanticBT.Common.GeoLocation
{
    [DataContract]
    public class GeocodeComponents
    {
        public GeocodeComponents()
        {
            Results = new List<GeocodeAddress>();
        }
        [DataMember(Name = "results")]
        public List<GeocodeAddress> Results { get; set; }
        [DataMember(Name = "status")]
        public string Status { get; set; }
    }

    [DataContract]
    public class GeocodeAddress
    {
        [DataMember(Name = "address_components")]
        public AddressComponent[] AddressComponents { get; set; }

        [DataMember(Name = "formatted_address")]
        public string Address { get; set; }

        [DataMember(Name = "geometry")]
        public Geometry Geometry { get; set; }
    }
    [DataContract]
    public class AddressComponent
    {
        [DataMember(Name = "long_name")]
        public string LongName { get; set; }

        [DataMember(Name = "short_name")]
        public string ShortName { get; set; }

        [DataMember(Name = "types")]
        public string[] Types { get; set; }
    }

    [DataContract]
    public class Geometry
    {
        [DataMember(Name = "location")]
        public Location Location { get; set; }

        [DataMember(Name = "location_type")]
        public string Type { get; set; }
    }
}