﻿using System;
using System.Web;
using System.Net;
using System.IO;
using System.Xml.Linq;

namespace AtlanticBT.Common.GeoLocation
{
    public static class IpLocator
    {
        private const string ApiAddress = "http://api.ipinfodb.com/v2/ip_query.php";

        /// <summary>
        /// Returns the IP Address of the customer. This will not work in testing locally (127.0.0.1) will return
        /// </summary>
        /// <returns>IP Address of Client</returns>
        public static string GetClientIpAddress()
        {
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }


        /// <summary>
        /// Returns information provided by "ipinfodb.com". Will throw custom exception + stack trace
        /// </summary>
        /// <param name="ipAddress">The IP Address you wish to search</param>
        /// <param name="apiKey">API Key provided by http://ipinfodb.com Register here: http://ipinfodb.com/register.php</param>
        /// <returns></returns>
        public static Location GetLocationFromIpAddress(string ipAddress, string apiKey)
        {
            var retval = new Location { TimeZone = new TimeZone() };

            try
            {
                if (String.IsNullOrEmpty(ipAddress))
                {
                    throw new ArgumentException("GetLocationFromIpAddress Method threw an exception." + Environment.NewLine + "Missing value for parm: IPAddress");
                }

                if (String.IsNullOrEmpty(apiKey))
                {
                    throw new ArgumentException("GetLocationFromIpAddress Method threw an exception." + Environment.NewLine + "Missing value for parm: APIKey");
                }

                var requestUrl = String.Format("{0}?key={1}&id={2}&timezone=true", ApiAddress, apiKey, ipAddress);

                var wc = new WebClient();
                var data = wc.OpenRead(requestUrl);
                var str = String.Empty;
                if (data != null)
                    using (var sr = new StreamReader(data))
                    {
                        str = sr.ReadToEnd();
                        data.Close();
                    }
                else
                    throw new Exception("GetLocationFromIpAddress Method threw an exception." + Environment.NewLine +
                                        "Request to ipinfodb.com has returned no result");

                if (String.IsNullOrEmpty(str))
                    throw new Exception("GetLocationFromIpAddress Method threw an exception." + Environment.NewLine +
                                        "Request to ipinfodb.com has returned no result");
                var result = XDocument.Parse(str);
                foreach (var node in result.Descendants("Response").Descendants())
                {
                    switch (node.Name.ToString())
                    {
                        case "Status":
                            retval.Status = node.Value;
                            break;
                        case "CountryCode":
                            retval.CountryCode = node.Value;
                            break;
                        case "CountryName":
                            retval.CountryName = node.Value;
                            break;
                        case "RegionCode":
                            break;
                        case "RegionName":
                            retval.RegionName = node.Value;
                            break;
                        case "City":
                            retval.CityName = node.Value;
                            break;
                        case "ZipPostalCode":
                            retval.ZipPostalCode = node.Value;
                            break;
                        case "Latitude":
                            retval.Latitude = Convert.ToDecimal(node.Value);
                            break;
                        case "Longitude":
                            retval.Longitude = Convert.ToDecimal(node.Value);
                            break;
                        case "Gmtoffset":
                            retval.TimeZone.Gmtoffset = node.Value;
                            break;
                        case "Dstoffset":
                            retval.TimeZone.Dstoffset = node.Value;
                            break;
                        case "TimezoneName":
                            retval.TimeZone.TimezoneName = node.Value;
                            break;
                    }
                    if (retval.Status.ToLower() != "ok")
                    {
                        break;
                    }
                }
            }
            catch (Exception exp)
            {
                throw new Exception("GetIPInformation Method threw an exception. " + Environment.NewLine + exp.StackTrace.ToString());
            }

            return retval;
        }
    }
}
