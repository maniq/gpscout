﻿namespace AtlanticBT.Common.GeoLocation
{
    public class TimeZone
    {
        public string TimezoneName { get; set; }
        public string Gmtoffset { get; set; }
        public string Dstoffset { get; set; }
    }
}