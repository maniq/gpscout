﻿using System;

namespace AtlanticBT.Common.GeoLocation
{
    public partial class _Default : System.Web.UI.Page
    {
        private const string ApiKey = "4eaac6c23dd0f780d9fff9e1db3d6175f9ff030cebb3db2a2da2a3d6fc2156ea";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSendRequest_Click(object sender, EventArgs e)
        {

            var locInfo = IpLocator.GetLocationFromIpAddress("174.254.149.135", ApiKey);

            lblMyIPAddress.Text = "174.254.149.135";

            lblCountryCode.Text = locInfo.CountryCode;
            lblCountryName.Text = locInfo.CountryName;
            
            lblRegionName.Text = locInfo.RegionName;
            lblCity.Text = locInfo.CityName;
            lblZipPostalCode.Text = locInfo.ZipPostalCode;

            lblTimezoneName.Text = locInfo.TimeZone.TimezoneName;
            lblGmtoffset.Text = locInfo.TimeZone.Gmtoffset;
            lblDstoffset.Text = locInfo.TimeZone.Dstoffset;

            lblLatitude.Text = locInfo.Latitude.ToString();
            lblLongitude.Text = locInfo.Longitude.ToString();
        }
    }
}
