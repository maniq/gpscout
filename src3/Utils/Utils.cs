﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Utility
{
    public static class Utils
    {
        public static void WriteCookieJarFile(string fileName, HttpRequestBase request)
        {
            var sb = new StringBuilder();
            if (request!= null && request.Cookies != null)
            {
                foreach (string cookieName in request.Cookies)
                {
                    HttpCookie cookie = request.Cookies[cookieName];
                    sb.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r\n"
                        , cookie.Domain, cookie.HttpOnly.ToString().ToUpper(), cookie.Path, cookie.Secure.ToString().ToUpper()
                        , cookie.Expires.ToUnixTimestamp()
                        , cookie.Name, cookie.Value);
                }
            }
            System.IO.File.WriteAllText(fileName, sb.ToString());
        }


        public static double ToUnixTimestamp(this DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        public static bool DeleteFile(string fileName) {
            try
            {
                System.IO.File.Delete(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string SafeFileName(string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        public static string GetTempFileName(Guid Id)
        {
            return Id.ToString().Replace("{", String.Empty).Replace("}", String.Empty);
        }
    }
}
