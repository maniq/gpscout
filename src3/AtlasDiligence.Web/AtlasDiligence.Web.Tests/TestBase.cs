﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Tests
{
    public class TestBase
    {
        /// <summary>
        /// transient aspnet_user with all bool fields set to true (also is a groupLeader)
        /// </summary>
        /// <returns></returns>
        protected aspnet_User CreateTransientAspNetUser()
        {
            return CreateTransientAspNetUser(Guid.NewGuid());
        }

        /// <summary>
        /// Transient aspnet_user.
        /// </summary>
        /// <param name="id">UserId</param>
        /// <returns></returns>
        protected aspnet_User CreateTransientAspNetUser(Guid id)
        {
            var userId = id;
            var groupId = Guid.NewGuid();
            var retval = new aspnet_User();
            retval.UserId = userId;
            retval.aspnet_Membership = new aspnet_Membership();
            retval.aspnet_Membership.Email = retval.UserName = "aspnetUser@bar.com";
            retval.aspnet_Membership.IsApproved = true;
            var userExt = new UserExt();
            userExt.FirstName = "Foo";
            userExt.LastName = "Bar";
            userExt.AccessGranted = true;
            userExt.CrmId = Guid.NewGuid();
            userExt.DiligenceAllowed = true;
            userExt.GroupId = groupId;
            userExt.RssAllowed = true;
            userExt.RssNextEmailDate = DateTime.Now;
            userExt.RssNextEmailOffsetEnum = (int)EmailFrequency.Week;
            userExt.RssSearchAliases = true;
            userExt.Phone = "919-555-1212";
            userExt.Title = "Test Title";
            userExt.OrganizationName = "Test Org";
            userExt.OrganizationType = (int)OrganizationType.MultiFamilyOffice;
            userExt.AUM = "TEst AUM";
            userExt.ReferredBy = "Test Referral";
            var userGroup = new AtlasDiligence.Common.Data.Models.Group()
            {
                Id = groupId,
                GroupLeaderId = userId,
                CrmId = Guid.NewGuid(),
            };
            retval.Groups.Add(userGroup);
            retval.UserExt = userExt;

            return retval;
        }
    }
}
