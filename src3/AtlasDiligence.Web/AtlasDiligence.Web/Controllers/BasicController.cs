﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Search;

namespace AtlasDiligence.Web.Controllers
{
    [AtlasAuthorize]
    public class BasicController : ControllerBase
    {
        public ActionResult Index()
        {
            if (UserEntity.DiligenceAllowed)
            {
                //return RedirectToAction("Index", "Search");
                return RedirectToAction("Start", "Search");
            }

            //Redirect to News Tracker if not a Diligence member
            if (UserEntity.RssAllowed)
            {
                return RedirectToAction("Index", "NewsTracker");
            }

            var model = new HomeViewModel()
                            {
                                UserEntity = UserEntity
                            };

            return View(model);
        }
    }
}
