﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Common.Data.Models;
using System.Linq.Expressions;
using System.IO;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Common.DTO.Services;

namespace AtlasDiligence.Web.Controllers
{
    public class RequestController : ControllerBase
    {
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public ActionResult Index()
        {
            return View();
        }


        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult OrganizationRequestGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        {
            var results = new GridList<RequestDataGrid>();
            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "CreatedOn";
                sortAscending = true;
            }
            if (sortColumn == "UserName")
            {
                sortColumn = "aspnet_User.LoweredUserName";
            }
            Expression<Func<OrganizationRequest, bool>> predicate =
                m => (m.OrganizationId.HasValue) &&
                     (m.IsActive) &&
                     (m.Type != (int)RequestType.Introduction) &&
                     (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
                     (m.OrganizationName.StartsWith(organizationName) || string.IsNullOrEmpty(organizationName));
            var allRequests = RequestRepository.FindAll(predicate);
            var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

            results.Total = allRequests.Count();
            results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

            return Json(results);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult OrganizationIntroductionGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        {
            var results = new GridList<RequestDataGrid>();
            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "CreatedOn";
                sortAscending = true;
            }
            if (sortColumn == "UserName")
            {
                sortColumn = "aspnet_User.LoweredUserName";
            }
            Expression<Func<OrganizationRequest, bool>> predicate =
                m => (m.OrganizationId.HasValue) &&
                     (m.IsActive) &&
                     (m.Type == (int)RequestType.Introduction) &&
                     (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
                     (m.OrganizationName.StartsWith(organizationName) || string.IsNullOrEmpty(organizationName));
            var allRequests = RequestRepository.FindAll(predicate);
            var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

            results.Total = allRequests.Count();
            results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

            return Json(results);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AnonymousOrganizationRequestGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        {
            var results = new GridList<RequestDataGrid>();
            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "CreatedOn";
                sortAscending = true;
            }
            if (sortColumn == "UserName")
            {
                sortColumn = "aspnet_User.LoweredUserName";
            }
            Expression<Func<OrganizationRequest, bool>> predicate =
                m => (!m.OrganizationId.HasValue) &&
                     (m.IsActive) &&
                     (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
                     (m.OrganizationName.Contains(organizationName) || string.IsNullOrEmpty(organizationName));
            var allRequests = RequestRepository.FindAll(predicate);
            var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

            results.Total = allRequests.Count();
            results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

            return Json(results);
        }

        /// <summary>
        /// Request Information for given Org.
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public JsonResult RequestOrganization(Guid organizationId, string comment, RequestType type)
        {
            RequestRepository.InsertRequest(organizationId, UserEntity.Id, comment, type);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// Request Information for an organization with name parameter.  No requirement that this is in the database
        /// </summary>
        /// <param name="name"></param>
        /// <param name="comment"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult RequestOrganizationName(string name, string comment, RequestType type)
        {
            RequestRepository.InsertRequest(name, UserEntity.Id, comment, type);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sets Request as inactive
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult HideRequest(Guid id)
        {
            var request = RequestRepository.FindAll(m => m.Id == id).SingleOrDefault();
            if (request == null) throw new ArgumentException("Invalid Id.");
            request.IsActive = false;
            RequestRepository.Edit(request);
            return Json(new MessageViewModel{Status = MessageStatus.Success}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PrintFullReport(string instanceId)
        {
            var task = TaskService.GetById(Guid.Parse(instanceId));
            if (task!=null && !task.IsCompleted()) {
                var user = UserService.GetUserById(task.UserId);
                if (user != null)
                {
                    HttpContext.Session.RemoveAll();
                    var _formsService = ComponentBrokerInstance.RetrieveComponent<IFormsAuthenticationService>();
                    _formsService.SignOut();
                    _formsService.SignIn(user.UserName, false);
                    //RedirectToAction("PrintAll", "Organization", new { instanceId = instanceId });
                }
            }
            return new EmptyResult();
        }

        //public ActionResult PrintAllHeader(string instanceId)
        //{
        //    string fileName = Path.Combine(AppSettings.TempPath, OrganizationHelperService.GetFullReportHeaderFileName(instanceId));
        //    return File(fileName, "text/html");
        //}

        //public ActionResult PrintAllFooter(string instanceId)
        //{
        //    string fileName = Path.Combine(AppSettings.TempPath, OrganizationHelperService.GetFullReportFooterFileName(instanceId));
        //    return File(fileName, "text/html");
        //}
    }
}
