﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.DTO.Model.UserNotifications;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
namespace AtlasDiligence.Web.Controllers
{
    [Authorize]
    public class UserNotificationController : ControllerBase
    {
        public JsonResult Check(Guid id)
        {
            IUserNotification notification = this.UserNotificationService.GetById(id);
            bool isDirty = this.UserNotificationService.Process(notification);

            IUserNotification _notification;
            if (isDirty || notification == null || !notification.IsValid)
            {
                UserEntity.Notifications.TryRemove(id, out _notification);
            }

            if (notification == null || !notification.IsValid)
                return new JsonResult { Data = new { Notification = new { } } };

            if (isDirty)
            {
                notification = UserEntity.Notifications.AddOrUpdate(notification);
            }

            return new JsonResult
            {
                Data = new
                {
                    Notification = notification,
                    Html = isDirty ? base.RenderPartialViewToString("UserNotification", notification) : string.Empty
                }
            };
        }

        public ActionResult Delete(Guid id)
        {
            this.UserNotificationService.Delete(id);
            base.UserEntity.Notifications.TryRemove(id);
            return new EmptyResult();
        }
    }
}
