﻿using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace AtlasDiligence.Web.Controllers
{
    public class ControllerBase : AsyncController
    {
        #region diagnostics
        private DateTime startActionExecuting;

        private DateTime startResultExecuting;

        private readonly DateTime startRequest = DateTime.Now;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.UserEntity = this.UserEntity;
            }
            startActionExecuting = DateTime.Now;
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var totalSeconds = (DateTime.Now - startActionExecuting).TotalSeconds;
            System.Diagnostics.Debug.WriteLine(string.Format("Request for {0}, {1} seconds.",
                                                             filterContext.RequestContext.HttpContext.Request.Url.
                                                                 AbsoluteUri,
                                                             totalSeconds.ToString("N2")));
            base.OnActionExecuted(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            startResultExecuting = DateTime.Now;
            base.OnResultExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var totalSeconds = (DateTime.Now - startResultExecuting).TotalSeconds;

            System.Diagnostics.Debug.WriteLine(string.Format("Result executed for {0}, {1} seconds.",
                                                                filterContext.RequestContext.HttpContext.Request.Url.
                                                                    AbsoluteUri,
                                                                totalSeconds.ToString("N2")));
            base.OnResultExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            //System.Diagnostics.Debug.WriteLine(filterContext.Exception.Message, "error");
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            //always reup the session variable with current UserEntity
            EntityCache.Add(Resources.Params.UserEntityCache, UserEntity);
            base.Dispose(disposing);
        }

        #region repositories
        private IOrganizationService _organizationService;

        protected internal IUserNotificationService UserNotificationService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserNotificationService>();
            }
        }

        protected ITaskService TaskService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ITaskService>();
            }
        }

        public IOrganizationService OrganizationService
        {
            get
            {
                if (_organizationService == null) 
                    _organizationService = ComponentBrokerInstance.RetrieveComponent<IOrganizationService>();
                return _organizationService;
            }
        }

        public IWebUserService WebUserService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IWebUserService>();
            }
        }

        public INewsItemService NewsItemService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<INewsItemService>();
            }
        }

        public IEulaRepository EulaRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEulaRepository>();
            }
        }

        public ISearchTermRepository SearchTermRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ISearchTermRepository>();
            }
        }

        public IFolderRepository FolderRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IFolderRepository>();
            }
        }

        public INoteRepository NoteRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<INoteRepository>();
            }
        }

        public INewsRepository NewsRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<INewsRepository>();
            }
        }

        public IRequestRepository RequestRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRequestRepository>();
            }
        }

        public IGroupRepository GroupRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>();
            }
        }

        public IUserService UserService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        public ISearchService SearchService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ISearchService>();
            }
        }

        #endregion

        #region user entity
        private UserEntity _userEntity { get; set; }

        /// <summary>
        /// Used to set UserEntity when user is edited.
        /// </summary>
        /// <remarks>Use this method sparingly.</remarks>
        /// <param name="userEntity"></param>
        [Authorize]
        protected void SetUserEntity(UserEntity userEntity)
        {
            _userEntity = userEntity;
        }

        protected internal UserEntity UserEntity
        {
            get
            {
                if (_userEntity == null)
                {
                    var cache = EntityCache.Get<UserEntity>(Resources.Params.UserEntityCache);
                    if (cache == null)
                    {
                        var membershipUser = Membership.GetUser();
                        if (membershipUser != null && membershipUser.ProviderUserKey != null)
                        {
                            var userId = (membershipUser.ProviderUserKey.ToString()).Maybe<Guid>();

                            if (userId.HasValue)
                            {
                                var userService = ComponentBrokerInstance.RetrieveComponent<IUserService>();
                                cache = userService.GetUserEntityByUserId(userId.Value);
                                EntityCache.Add(Resources.Params.UserEntityCache, cache);
                            }
                        }
                    }
                    _userEntity = cache;
                }
                return _userEntity;
            }
        }
        #endregion

        #region RenderPartialViewToString
        /// <summary>
        /// Render a partial view to string to return as a Json value
        /// </summary>
        /// <returns></returns>
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

		#region helpers

		/// <summary>
		/// returns UserId for logged in user.
		/// </summary>
		/// <returns>UserId or Empty Guid</returns>
		protected Guid GetLoggedInUserId()
		{
			var loggedInUser = Membership.GetUser();

			if (loggedInUser != null && loggedInUser.IsOnline && loggedInUser.ProviderUserKey != null)
			{
				return loggedInUser.ProviderUserKey.ToString().Maybe<Guid>().GetValueOrDefault(Guid.Empty);
			}
			return Guid.Empty;
		}

		/// <summary>
		/// Return the number of rowsPerPage displayed in a YUI datagrid
		/// </summary>
		/// <returns></returns>
		protected int DataGridRowsPerPage()
		{
			return ConfigurationManager.AppSettings.Get("DataGridRowsPerPage").Maybe<int>() ?? 10;
		}

		/// <summary>
		/// Check to see that current UserEntity can access group.
		/// </summary>
		/// <param name="groupId"></param>
		/// <returns></returns>
		protected bool CanAccessGroup(Guid groupId)
		{
			if (User.IsInRole(RoleNames.Admin))
				return true;
			if (groupId == UserEntity.GroupId)
				return true;

			return false;
		}

	    protected IEnumerable<LuceneSearchResult> GetAvailableOrganizations()
	    {
			// get segments and purchased orgs
			var group = UserService.GetUserById(UserEntity.Id).UserExt.Group;
			var segments = group != null ? group.GroupSegments.Select(m => m.Segment).ToList() : new List<Common.Data.Models.Segment>();
			var purchasedOrgs = group != null ? group.GroupOrganizations.Select(m => m.OrganizationId) : new List<Guid>();

			// get available orgs
			int total;
			var availableOrgs = SearchService.GetOrganizations(new SearchFilter { Segments = segments, PurchasedOrganizationIds = purchasedOrgs }, out total).ToList();
		    return availableOrgs;
	    }

	    #endregion
    }
}