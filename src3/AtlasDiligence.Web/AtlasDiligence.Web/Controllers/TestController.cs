﻿using System.Linq;
using System.Web.Mvc;
using AtlasDiligence.Common.DTO.Services;

namespace AtlasDiligence.Web.Controllers
{
    public class TestController : ControllerBase
    {
        public ActionResult Index()
        {
            var requests = new OrganizationService().GetAllClientRequests(true);
            return Content(requests.Count().ToString());
        }
    }
}
