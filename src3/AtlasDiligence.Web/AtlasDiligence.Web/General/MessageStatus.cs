﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.General
{
    public class MessageStatus
    {
        public const string Success = "Success";
        public const string Info = "Info";
        public const string Warning = "Warning";
        public const string Failure = "Failure";
    }
}
