﻿<%@ Application Codebehind="Global.asax.cs" Inherits="AtlasDiligence.Web.Umbraco.GPScoutApplication" Language="C#" %>
<script runat="server">
//#if DEBUG
void Session_OnStart() {
    //new AtlasDiligence.Common.DTO.Services.IndexService().ReIndex();
    return;
}
//#endif

void Application_Error(Object sender, EventArgs e)
{
    // avoid app pool restart during wkhtmltopdf calls
    // error logging is performed by Umbraco automatically
    if (Common.IsPrint)
    {
        Server.ClearError();
    }
}
</script>

