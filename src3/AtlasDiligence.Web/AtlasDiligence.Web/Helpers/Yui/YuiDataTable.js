﻿jQuery(function() {
    var dataTable = jQuery('#__ID__');
    var page = null;

    if (__SHOWPAGINATION__) {
        page = new YAHOO.widget.Paginator({ 
        pageLinks: 10, 
        rowsPerPage: __ROWSPERPAGE__,
        containers: '__ID__Paginator',
        pageReportTemplate: 'Displaying {startRecord} to {endRecord} of {totalRecords}'
        });
        if(__SHOWPAGINATION_ROWSPERPAGEOPTIONS__) {
            page.set('template', YAHOO.widget.Paginator.TEMPLATE_ROWS_PER_PAGE + '{CurrentPageReport}');
            page.set('rowsPerPageOptions', __ROWSPERPAGEOPTIONS__);
        }
        jQuery('__ID__Paginator').addClass('destroy').bind('destroyEvent', function() {
            page.destroy();
            page = null;
        });
    }
    
    var settings = {
        srcUrl: '__URL__',
        colDef: [__COLDEF__],
        paginator : page,
        rowSelectEvent : __ONSELECTFN__,
        selectionMode : '__SELECTIONMODE__',
        selectFirstRow: __SELECTFIRSTROW__,
        checkboxClickEvent: __ONCHECKBOXCLICK__,
        cellClickEvent: __ONCELLCLICK__,
        doBeforeLoadData: __DOBEFORELOADDATA__,
        postRenderEvent: __POSTRENDEREVENT__,
        responseSchema: {
            resultsList: 'Result',
            fields: [__FIELDS__],
            metaFields: {__METAFIELDS__}
        },
        requestParams : __INITIALREQUESTPARAMS__,
        searchFormId: '__SEARCHFORMID__',
        initialRequest: 'sortColumn=&sortAscending=true&pageStart=0&rowsPerPage=__ROWSPERPAGE__'
    };
    
    //configure data table
    var dt = dataTable.YUIDataTable(settings).data('dataTable');

    dt.validateColumnWidths();

    //configure show/hide columns
    if (__SHOWCOLUMNSELECTOR__) {
        var toggleColumn = function(el) {
            var currentState = this.cfg.getProperty('checked');
            if (currentState) {
                dt.hideColumn(this.value.id);
            }
            else {
                dt.showColumn(this.value.id);
            }
            this.cfg.setProperty('checked', !currentState);
        };

        var mapFunction = function(item, index) {
            var menuItem = { text: item.friendlyName, value: { id: item.key },
                checked: !item.hidden, onclick: { fn: toggleColumn}
            };
            return menuItem;
        };

        var menuItems = jQuery.map(settings.colDef, mapFunction);
        var button = new YAHOO.widget.Button({
            type: 'menu',
            id: '__ID__BUTTONEL',
            container: '__ID__COLUMNCONTAINER',
            menu: menuItems,
            label: 'Show/Hide'
        });

        jQuery('__ID__BUTTONEL').addClass('destroy').bind('destroyEvent', function() { button.destroy(); button = null; });
    }
    
    //configure button hooks.
    __BUTTONHOOKS__
  
});
