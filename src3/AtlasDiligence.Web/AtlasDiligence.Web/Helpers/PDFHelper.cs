﻿using AtlasDiligence.Common.Data.Models;
using log4net;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility;

namespace AtlasDiligence.Web.Helpers
{
    public static class PDFHelper
    {
        private static ILog _log;

        private static ILog Log
        {
            get {
                if (_log == null)
                    _log = LogManager.GetLogger(typeof(PDFHelper));

                return _log;
            }
        }


        public static bool GeneratePDF(string baseUrl, string fileName, double zoom, ulong jsdelayms, string headerUrl, string footerUrl, string instanceId, int timeoutMS)
        {
            /*
            baseUrl = baseUrl.Replace("://atlas", "://localhost:8081");
            headerUrl = headerUrl.Replace("://atlas", "://localhost:8081");
            footerUrl = footerUrl.Replace("://atlas", "://localhost:8081");
             */
            string jarFileName = fileName + ".jar";
            string arguments = string.Format(@"--custom-header ""CONTENT-TYPE"" ""application/x-www-form-urlencoded"" --post instanceId {0} --cookie-jar ""{1}"" {2} ""{3}"" ""{4}.pdf"" "
                , instanceId, jarFileName, AppSettings.PrintAllAdditionalCmdLineOptions, baseUrl, fileName);

            GeneratePDF(arguments, timeoutMS);

            if (File.Exists(jarFileName))
            {
                baseUrl = baseUrl.Replace("/Request/PrintFullReport", "/Organization/PrintAll");
                arguments = string.Format(@"--custom-header ""CONTENT-TYPE"" ""application/x-www-form-urlencoded"" --post instanceId {0} --zoom {1} --javascript-delay {2} {3} --header-html ""{4}"" --footer-html ""{5}"" --cookie-jar ""{6}"" ""{7}"" ""{8}.pdf"" "
                    , instanceId, zoom, jsdelayms, AppSettings.PrintAllAdditionalCmdLineOptions, headerUrl, footerUrl
                    , jarFileName, baseUrl, fileName);

                GeneratePDF(arguments, timeoutMS);

                var task = new Task<int>(() => {
                    baseUrl = baseUrl.Replace("/Organization/PrintAll", "/Organization/PrintAllDone");
                    arguments = string.Format(@"--cookie-jar ""{0}"" {1} ""{2}"" ""{3}"".log.pdf "
                    , jarFileName, AppSettings.PrintAllAdditionalCmdLineOptions, baseUrl, fileName);

                    GeneratePDF(arguments, timeoutMS);
                    Utils.DeleteFile(jarFileName);
                    Utils.DeleteFile(String.Format("{0}.log.pdf", fileName));
                    return 0;
                });
                task.Start();
                return true;
            }
            return false;
        }

        //internal static string SaveHtml(string html)
        //{
        //    string fileName = Utils.GetTempFileName();
        //    string destFileName = Path.Combine(AppSettings.TempPath, fileName/*+".html"*/);
        //    File.WriteAllText(destFileName, html);
        //    return fileName;
        //}

        //internal static void GeneratePDFFromFile(string url, string sourceFileName, double zoom, int jsdelayms)
        //{
        //    string path = AppSettings.TempPath;
        //    if (zoom < 0) zoom = 1;
        //    Process p = new Process();
        //    p.StartInfo.RedirectStandardError = true;
        //    p.StartInfo.RedirectStandardOutput = true;
        //    //p.StartInfo.WorkingDirectory = HttpContext.Current.Server.MapPath("~/wkHTMLtoPDF/");
        //    p.StartInfo.WorkingDirectory = path;
        //    p.StartInfo.UseShellExecute = false;
        //    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        //    p.StartInfo.CreateNoWindow = true;
        //    //p.StartInfo.FileName = HttpContext.Current.Server.MapPath("~/wkHTMLtoPDF/wkhtmltopdf.exe");
        //    p.StartInfo.FileName = AppSettings.PdfGeneratorPath;
        //    //p.StartInfo.Arguments = string.Format("--zoom {2} --javascript-delay {3} --cookie-jar \"{4}\" \"{0}/request/PrintFullReport?id={1}\" \"{1}.pdf\"", baseUrl, fileName, zoom, jsdelayms, jarFileName);
        //    p.StartInfo.Arguments = string.Format("--zoom {2} --javascript-delay {3} \"{0}\" \"{1}.pdf\"", url, sourceFileName, zoom, jsdelayms);
        //    //Logger.Write(string.Format("GeneratePDF: working directory: {0}, pdf generator.exe: {1}, arguments: {2}", p.StartInfo.WorkingDirectory, p.StartInfo.FileName, p.StartInfo.Arguments));
        //    p.Start();
        //    p.WaitForExit();
        //}

        internal static void SetPassword(string sourceFile, string password, string userDisplayName, string groupName)
        {
            PdfDocument document = PdfReader.Open(sourceFile, String.Empty);
            PdfSecuritySettings securitySettings = document.SecuritySettings;

            securitySettings.UserPassword = password;

            XFont font = new XFont("Arial", AppSettings.PdfWatermarkSizeEm);
            XPen pen = new XPen(XColor.FromArgb(128, 255, 0, 0), 2);
            foreach (PdfPage page in document.Pages)
            {
                XGraphics gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Append);
                XSize userDisplayNameSize = gfx.MeasureString(userDisplayName, font);
                XSize groupNameSize = gfx.MeasureString(groupName, font);
                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                // Create a string format
                XStringFormat format = new XStringFormat();
                format.Alignment = XStringAlignment.Near;
                format.LineAlignment = XLineAlignment.Near;

                // Create a dimmed red brush
                XBrush brush = new XSolidBrush(XColor.FromArgb(128, 200, 200, 200));
                // Draw the string
                gfx.DrawString(userDisplayName, font, brush, new XPoint((page.Width - userDisplayNameSize.Width) / 2
                    , page.Height / 2 - userDisplayNameSize.Height), format);

                gfx.DrawString(groupName, font, brush, new XPoint((page.Width /* 0.95*/ - groupNameSize.Width) / 2
                    , page.Height / 2), format);
            }

            document.Save(sourceFile);
        }

        //private static void GeneratePDF_NoOutputRedirect(string arguments, int timeoutMS)
        //{
        //    string path = AppSettings.TempPath;
        //    Process p = new Process();
        //    p.StartInfo.RedirectStandardError = false;
        //    p.StartInfo.RedirectStandardOutput = false;
        //    p.StartInfo.WorkingDirectory = path;
        //    p.StartInfo.UseShellExecute = false;
        //    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        //    p.StartInfo.CreateNoWindow = true;
        //    p.StartInfo.FileName = AppSettings.PdfGeneratorPath;
        //    //p.StartInfo.Arguments = string.Format("--zoom {2} --javascript-delay {3} --cookie-jar \"{4}\" \"{0}/request/PrintFullReport?id={1}\" \"{1}.pdf\"", baseUrl, fileName, zoom, jsdelayms, jarFileName);
        //    p.StartInfo.Arguments = arguments;
        //    //Logger.Write(string.Format("GeneratePDF: working directory: {0}, pdf generator.exe: {1}, arguments: {2}", p.StartInfo.WorkingDirectory, p.StartInfo.FileName, p.StartInfo.Arguments));
        //    p.Start();
        //    p.WaitForExit(timeoutMS);
        //    if (p.ExitCode != 0)
        //        throw new Exception(string.Format("Error: GeneratePDF - ExitCode: {0}, Timeout: {1}, Arguments: {2}", p.ExitCode, timeoutMS, arguments));
        //}

        private static void GeneratePDF(string arguments, int timeoutMS)
        {
            using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                using (Process process = new Process())
                {
                    string path = AppSettings.TempPath;
                    process.StartInfo.WorkingDirectory = path;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.FileName = AppSettings.PdfGeneratorPath;
                    process.StartInfo.Arguments = arguments;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;

                    StringBuilder output = new StringBuilder();
                    StringBuilder error = new StringBuilder();
                    try
                    {
                        process.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                outputWaitHandle.Set();
                            }
                            else
                            {
                                output.AppendLine(e.Data);
                            }
                        };
                        process.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                errorWaitHandle.Set();
                            }
                            else
                            {
                                error.AppendLine(e.Data);
                            }
                        };

                        process.Start();

                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();

                        if (process.WaitForExit(timeoutMS) &&
                            outputWaitHandle.WaitOne(timeoutMS) &&
                            errorWaitHandle.WaitOne(timeoutMS))
                        {
                            // Process completed. Check process.ExitCode here.
                            output.AppendFormat("\r\nExitcode: {0}", process.ExitCode);
                        }
                        else
                        {
                            error.AppendLine("Process timed out!");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("PDFHelper", ex);
                        throw;
                    }
                    //finally
                    //{
                    //    outputWaitHandle.WaitOne(timeoutMS);
                    //    errorWaitHandle.WaitOne(timeoutMS);
                    //}

                    if (process.HasExited)
                    {
                        Log.Error(error.ToString());
                        Log.Info(output.ToString());
                        if (process.ExitCode != 0)
                            throw new Exception(string.Format("Error: GeneratePDF - ExitCode: {0}, Timeout: {1}, Arguments: {2}. Further details may be logged above this log entry.", process.ExitCode, timeoutMS, arguments));
                    }
                }
            }
        }
    }
}