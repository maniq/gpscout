/*

ABT Placeholder Plugin v1.0

$('input[type=text]').placeholder();

-or-

$('input[type=text]').placeholder({
	noteID: '.note'
});

==================================================================================================================*/

(function($) {
						 
	$.fn.placeholder = function(options) {
		
		var defaults = {
			noteID: '.note'
		}
			
		var options = $.extend(defaults, options); 
		
		return this.each(function() {
			
			var obj = $(this);
			
			$note = obj.parent().find(options.noteID).html();
			obj.parent().find(options.noteID).remove();
			
			obj.attr('default',$note);
			obj.attr('value',$note);
			obj.addClass('default');
			
			obj.bind('focus', function(){
				if (obj.hasClass('default')) {
					obj.attr('value','');
					obj.removeClass('default');
				}
			});
			
			obj.bind('blur', function(){
				if ($.trim(obj.attr('value')).length == 0) { //if it's blank
					obj.attr("value", obj.attr("default"));
					obj.addClass('default');
				}
			});
			
		});
	
	}
	
})(jQuery);