﻿(function () {
    var setGotoBottom = function ($) {
        $('a[href="tc"]').click(function (e) {
            e.preventDefault();
            //$('#lastupdated1').get(0).scrollIntoView();
            $('script[src*="/js/terms.js"]').parent().parent().scrollTop(999999);
        });
    }

    if (window.jQuery) {
        jQuery(function () {
            setGotoBottom(window.jQuery);
        });
        return;
    }
    // Load jQuery
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function (callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function () { checkReady(callback); }, 500);
        }
    };

    // Start polling...
    checkReady(function ($) {
        setGotoBottom($);
    });
})();