﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models.ViewModels.Search;
using AtlasDiligence.Web.Models.ViewModels.Segment;


namespace AtlasDiligence.Web.Models.Mappers
{
	public static class SegmentMapper
	{
		public static Segment MapToSegment(SegmentOptionsViewModel viewModel, Segment segment)
		{
			segment.CreatedDate = DateTime.Now;
			segment.Name = viewModel.Name;
			MapToSegmentFilters(viewModel, segment);
			return segment;
		}

		private static void MapToSegmentFilters(SegmentOptionsViewModel viewModel, Segment segment)
		{
			segment.SegmentFilters.Clear();

			if (viewModel.SelectedStrategies.Any())
			{
				segment.SegmentFilters.AddRange(viewModel.SelectedStrategies.Select(x => new SegmentFilter
																			 {
																				 Field = (int)FieldType.Strategy,
																				 SegmentId = segment.Id,
																				 Value = x,
																				 Id = Guid.NewGuid()
																			 }));
			}


			if (viewModel.SelectedMarketStages.Any())
			{
				segment.SegmentFilters.AddRange(viewModel.SelectedMarketStages.Select(x => new SegmentFilter
							   {
								   Field = (int)FieldType.MarketStage,
								   SegmentId = segment.Id,
								   Value = x,
								   Id = Guid.NewGuid()
							   }));
			}

			if (viewModel.SelectedInvestmentRegions.Any())
			{
				segment.SegmentFilters.AddRange(viewModel.SelectedInvestmentRegions.Select(x => new SegmentFilter
							   {
								   Field = (int)FieldType.InvestmentRegion,
								   SegmentId = segment.Id,
								   Value = x,
								   Id = Guid.NewGuid()
							   }));
			}

			if (viewModel.SelectedCountries.Any())
			{
				segment.SegmentFilters.AddRange(viewModel.SelectedCountries.Select(x => new SegmentFilter
				{
					Field = (int)FieldType.Country,
					SegmentId = segment.Id,
					Value = x,
					Id = Guid.NewGuid()
				}));
			}

            if (viewModel.SbicFund.HasValue)
                segment.SegmentFilters.AddRange(new List<SegmentFilter>{ new SegmentFilter
				{
					Field = (int) FieldType.SBICFund,
					SegmentId =  segment.Id,
                    Value = viewModel.SbicFund.Value ? "1" :"0",
					Id = Guid.NewGuid()
				}});
		}
	}
}
