﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.DTO.Model.UserNotifications;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AtlasDiligence.Web.Models
{
    /// <summary>
    /// Fascade of Crm and asp_net user properties. Used as session variable after the user is logged in.
    /// </summary>
    public class UserEntity
    {
        private UserEntity()
        {
            RecentSearches = new Dictionary<Guid, string>();
            Notifications = new ConcurrentUserNotifications();
        }

        /// <summary>
        /// Maps entity from web db.
        /// </summary>
        /// <param name="dbUser"></param>
        /// <returns></returns>
        public static UserEntity GetInstance(aspnet_User dbUser)
        {
            if (dbUser == null) return null;

            var retval = new UserEntity()
            {
                Id = dbUser.UserId,
            };

            if (dbUser.aspnet_Membership != null)
            {
                retval.IsLockedOut = dbUser.aspnet_Membership.IsLockedOut;
                retval.Email = dbUser.aspnet_Membership.Email;
                retval.IsApproved = dbUser.aspnet_Membership.IsApproved;
            }
            if (dbUser.aspnet_UsersInRoles != null)
            {
                var userRoles = dbUser.aspnet_UsersInRoles.Select(x => x.aspnet_Role.LoweredRoleName).ToList();
                retval.IsEmployee = userRoles.Contains(RoleNames.Employee.ToLower());
                retval.IsAdmin = userRoles.Contains(RoleNames.Admin.ToLower());
            }
            if (dbUser.UserEulaAudits != null)
            {
                retval.EulaHistory = dbUser.UserEulaAudits.Select(m => m.AgreedOn);
            }

            if (dbUser.UserExt != null)
            {
                retval.CrmId = dbUser.UserExt.CrmId;
                retval.FirstName = dbUser.UserExt.FirstName;
                retval.LastName = dbUser.UserExt.LastName;
                retval.GroupId = dbUser.UserExt.GroupId;
                if (retval.GroupId.HasValue)
                    retval.IsGroupLeader =
                        dbUser.Groups.Where(x => x.Id == retval.GroupId.Value).Select(
                            y => y.GroupLeaderId.HasValue).FirstOrDefault();
                retval.RssAllowed = dbUser.UserExt.RssAllowed;
                retval.RssNextEmailDate = dbUser.UserExt.RssNextEmailDate;
                retval.RssSearchAliases = dbUser.UserExt.RssSearchAliases;
                retval.DiligenceAllowed = dbUser.UserExt.DiligenceAllowed;
                retval.AccessGranted = dbUser.UserExt.AccessGranted;
                if (dbUser.UserExt.RssNextEmailOffsetEnum.HasValue)
                    retval.RssEmailFrequency = (EmailFrequency)dbUser.UserExt.RssNextEmailOffsetEnum;
                retval.Phone = dbUser.UserExt.Phone;
                retval.Title = dbUser.UserExt.Title;
                retval.OrganizationName = dbUser.UserExt.OrganizationName;
                if (dbUser.UserExt.OrganizationType != null)
                    retval.OrganizationType = (OrganizationType)dbUser.UserExt.OrganizationType;
                retval.AUM = dbUser.UserExt.AUM;
                retval.ReferredBy = dbUser.UserExt.ReferredBy;
                retval.RecentSearches = dbUser.SearchTemplates.OrderByDescending(o => o.CreatedDate).Take(5).ToDictionary(x => x.Id, x =>
                    (String.IsNullOrWhiteSpace(x.Name) ? "Advanced Search - " : x.Name + " - ") + x.CreatedDate.ToString("MM/dd"));
                retval.RecentSearchTemplates = dbUser.SearchTemplates.OrderByDescending(o => o.CreatedDate).Take(5);
                retval.SavedSearches = dbUser.SearchTemplates.Where(m => m.SavedSearch != null).Select(m => m.SavedSearch).ToList();
                var service = ComponentBrokerInstance.RetrieveComponent<IUserService>();
                retval.RecentlyViewedOrganizations = service.GetRecentlyViewedOrganizations(dbUser.UserId, 10).ToList();
                retval.LastLogin = dbUser.UserExt.PreviousLogin;
                if (dbUser.UserExt.Group != null)
                {
                    retval.Group = dbUser.UserExt.Group;
                }
                
            }
            if(dbUser.UserRssEmails != null)
            {
                retval.RssEmails = dbUser.UserRssEmails.Select(m => m.RssEmailAddress);
            }
            if (dbUser.Folders != null)
            {
                retval.Folders = dbUser.Folders;
            }

            var userNotificationService = ComponentBrokerInstance.RetrieveComponent<IUserNotificationService>();
            var notifications =  userNotificationService.GetValidUserNotifications(retval.Id);
            retval.Notifications = new ConcurrentUserNotifications(notifications);

            return retval;
        }

        /// <summary>
        /// map userModel properties to existing user entity
        /// </summary>
        /// <param name="userEntity"></param>
        /// <param name="model"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        public static UserEntity Map(UserEntity userEntity, UserViewModel model, bool isAdmin)
        {
            userEntity.Email = model.Email;
            userEntity.FirstName = model.FirstName;
            userEntity.LastName = model.LastName;
            userEntity.GroupId = model.GroupId;
            userEntity.IsApproved = model.IsApproved;
            if (isAdmin)
            {
                userEntity.IsGroupLeader = model.IsGroupLeader;
                userEntity.DiligenceAllowed = model.DiligenceAllowed;
                userEntity.RssAllowed = model.RssAllowed;
                userEntity.IsEmployee = model.Role == RoleNames.Employee;
                userEntity.IsAdmin = model.Role == RoleNames.Admin;
            }
            if (model.GroupId.HasValue)
            {
                userEntity.DiligenceAllowed = model.DiligenceAllowed;
                userEntity.RssAllowed = model.RssAllowed;
            }
            userEntity.Phone = model.Phone;
            userEntity.Title = model.Title;
            userEntity.OrganizationName = model.OrganizationName;
            userEntity.OrganizationType = model.OrganizationType;
            userEntity.ReferredBy = model.ReferredBy;

            return userEntity;
        }

        /// <summary>
        /// Limited map for request access
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static UserEntity Map(ViewModels.Account.RequestAccessViewModel model)
        {
            var retval = new UserEntity();
            retval.Email = model.Email;
            retval.FirstName = model.FirstName;
            retval.LastName = model.LastName;
            retval.IsApproved = false;
            retval.AccessGranted = true;

            return retval;
        }

        public Guid Id { get; private set; }

        public Guid CrmId { get; set; }

        public DateTime? LastLogin { get; set; }

        public bool IsLockedOut { get; private set; }

        public bool IsApproved { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string Phone { get; set; }

        public string OrganizationName { get; set; }

        public OrganizationType OrganizationType { get; set; }

        public string AUM { get; set; }

        public string ReferredBy { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsEmployee { get; set; }

        public Guid? GroupId { get; set; }

        public Group Group { get; set; }

        public IEnumerable<Folder> Folders { get; set; } 

        public bool IsGroupLeader { get; private set; }

        public IEnumerable<DateTime> EulaHistory { get; private set; } 

        /// <summary>
        /// username and email are always the same. Need to be kept in sync.
        /// Use email.
        /// </summary>
        public string UserName { get { return this.Email; } }

        public string FullName { get { return FirstName + " " + LastName; } }

        /// <summary>
        /// Is the user allowed to access Rss App (invited to purchase)?
        /// </summary>
        public bool RssAllowed { get; set; }

        /// <summary>
        /// Is the user allowed to access Diligence App?
        /// </summary>
        public bool DiligenceAllowed { get; set; }

        /// <summary>
        /// Is this currently an open access request?
        /// </summary>
        public bool AccessGranted { get; set; }

        /// <summary>
        /// Rss App
        /// </summary>
        public DateTime? RssNextEmailDate { get; set; }

        /// <summary>
        /// Rss App Email Frequency
        /// </summary>
        public EmailFrequency? RssEmailFrequency { get; set; }

        /// <summary>
        /// Should the AssociatedTerms table be searched? 
        /// </summary>
        public bool RssSearchAliases { get; set; }

        /// <summary>
        /// Extra emails to send RSS search results
        /// </summary>
        public IEnumerable<string> RssEmails { get; set; }

        /// <summary>
        /// List of id and name of Recent Searches in Diligence App
        /// </summary>
        public IDictionary<Guid, string> RecentSearches { get; set; }

        /// <summary>
        /// Recent search templates
        /// </summary>
        public IEnumerable<SearchTemplate> RecentSearchTemplates { get; set; }

        /// <summary>
        /// Saved Searches
        /// </summary>
        public IList<SavedSearch> SavedSearches { get; set; } 

        /// <summary>
        /// Recently viewed organizations
        /// </summary>
        public IList<Common.Data.Models.RecentlyViewedOrganizationsResult> RecentlyViewedOrganizations { get; set; }

		public bool TrackUsersAllowed {
			get { return IsAdmin || IsEmployee; }
		}

        public ConcurrentUserNotifications Notifications { get; set; }
    }
}
