﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class OrganizationPrintAllHeaderViewModel
    {
        public string UserName { get; set; }
        public string OrganizationName { get; set; }
    }
}