﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.Controllers.Services;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
	public class RecentlyUpdatedViewModel
	{
		public IEnumerable<Common.Data.Models.Organization> FirmsRecentlyUpdated { get; set; }
		
		public void Build(IEnumerable<LuceneSearchResult> availableOrgs, IOrganizationService organizationService)
		{
			// set properties
			FirmsRecentlyUpdated = organizationService.GetAllByIds(availableOrgs.Where(m => m.LastUpdated.HasValue).OrderByDescending(m => m.LastUpdated).Select(m => m.Id).Take(10));
		}
	}
}