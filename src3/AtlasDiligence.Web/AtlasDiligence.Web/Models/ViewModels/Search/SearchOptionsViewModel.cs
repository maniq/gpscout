﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using System.Linq;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    public class SearchOptionsViewModel
    {
        public static readonly string FocusListStr = "Focus List";

        [SearchField]
        [DisplayName(@"Search:")]
        public string Search { get; set; }

        public IList<string> Strategies { get; set; }

        [SearchField]
        [DisplayName(@"Strategy:")]
        public IList<string> SelectedStrategy { get; set; }

        public IList<string> MarketStages { get; set; }

        [SearchField]
        [DisplayName(@"Market/Stage:")]
        public IList<string> SelectedMarketStage { get; set; }

        public IList<string> Sectors { get; set; }

        [SearchField]
        [DisplayName(@"Sector:")]
        public IList<string> SelectedSector { get; set; }

        public IList<string> InvestmentRegions { get; set; }

        [SearchField]
        [DisplayName(@"Region:")]
        public IList<string> SelectedInvestmentRegions { get; set; }

        public IList<string> FundraisingStatuses { get; set; }

        [SearchField]
        [DisplayName(@"Fundraising Status:")]
        public IList<string> SelectedFundraisingStatus { get; set; }

        [SearchField]
        [DisplayName(@"Fund Size Minimum:")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public double? FundSizeMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Fund Size Maximum:")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public double? FundSizeMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Number of Funds Closed Minimum:")]
        public int? NumberOfFundsClosedMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Number of Funds Closed Maximum:")]
        public int? NumberOfFundsClosedMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Emerging Manager:")]
        public bool? EmergingManager { get; set; }

        public IList<string> FocusLists { get; set; }

        [SearchField]
        [DisplayName(@"Focus List:")]
        public IList<string> SelectedFocusList { get; set; }
        public bool FocusListSelectedFlag { get; set; }

        [SearchField]
        [DisplayName(@"Access Constrained:")]
        public bool? AccessConstrained { get; set; }

        [SearchField]
        [DisplayName(@"Diligence Status Minimum:")]
        public int? DiligenceStatusMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Diligence Status Maximum:")]
        public int? DiligenceStatusMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Limit to Firms with Profiles:")]
        public bool ShowOnlyPublished { get; set; }

        public IList<string> SubStrategies { get; set; }

        [SearchField]
        [DisplayName(@"Sub-Strategy:")]
        public IList<string> SelectedSubStrategy { get; set; }

        [SearchField]
        [DisplayName(@"Sector Specialist:")]
        public bool? SectorSpecialist { get; set; }

        public IList<string> InvestmentSubRegions { get; set; }

        [SearchField]
        [DisplayName(@"Sub-Region:")]
        public IList<string> SelectedInvestmentSubRegion { get; set; }

        public IList<string> Countries { get; set; }

        [SearchField]
        [DisplayName(@"Country:")]
        public IList<string> SelectedCountry { get; set; }

        public IList<string> Currencies { get; set; }

        [SearchField]
        [DisplayName(@"Currency:")]
        public IList<string> SelectedCurrencies { get; set; }

        [SearchField]
        [DisplayName(@"SBIC Fund:")]
        public bool? SBICFund { get; set; }

        // Expected Next Fundraise variables
        [SearchField]
        [DisplayName(@"Expected Next Fundraise Start Date:")]
        public DateTime? SelectedNextFundraiseStartDate { get; set; }

        [SearchField]
        [DisplayName(@"Expected Next Fundraise End Date:")]
        public DateTime? SelectedNextFundraiseEndDate { get; set; }

        [SearchField]
        [DisplayName(@"Expected Next Fundraise:")]
        public int? SelectedNextFundraiseValue { get; set; }

        public IEnumerable<SelectListItem> FundRaiseMonthOptions
        {
            get
            {
                string[] months = new string[] { "3", "6", "9", "12", "18", "24" };
                return months.Select(s => new SelectListItem { Text = String.Format("{0} months", s), Value = s });
            }
        }

        // Grade Fields variables
        public IList<string> QualitativeGrades { get; set; }

        public IList<string> QuantitativeGrades { get; set; }

        [SearchField]
        [DisplayName(@"Qualitative Score:")]
        public int? SelectedQualitativeGradeID { get; set; }

        [SearchField]
        [DisplayName(@"QoR Score:")]
        public int? SelectedQuantitativeGradeID { get; set; }

        public IList<string> SelectedQualitativeGrades { get; set; }

        public IList<string> SelectedQuantitativeGrades { get; set; }

        public IEnumerable<SelectListItem> SelectedGradesOptions
        {
            get
            {
                var types = from Grades type in Enum.GetValues(typeof(Grades))
                            select new { ID = (int)type, Name = type.ToString() };
                return new SelectList(types, "ID", "Name");
            }
        }

        /// <summary>
        /// Returns a dictionary of the search option properties and values. Used to pass params into Search grid.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> SearchOptionsKvp()
        {
            var retval = new Dictionary<string, object>();

            foreach (var property in typeof(SearchOptionsViewModel).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(SearchFieldAttribute), false).Any()
                    && property.GetValue(this, null) != null)
                {
                    // HACK: collections are not serialized correctly for the YUI data
                    if (property.PropertyType == typeof(IList<string>))
                    {
                        if (((IList<string>)property.GetValue(this, null)).Any())
                        {
                            retval.Add(property.Name,
                                       HttpUtility.UrlEncode(
                                           ((IList<string>)property.GetValue(this, null)).Aggregate(
                                               (a, b) => a + "," + b)));
                        }
                    }
                    else
                    {
                        retval.Add(property.Name, HttpUtility.UrlEncode(property.GetValue(this, null).ToString()));
                    }
                }
            }

            return retval;
        }

        public Dictionary<string, object> SearchOptionsReadable()
        {
            var retval = new Dictionary<string, object>();

            foreach (var property in typeof(SearchOptionsViewModel).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(SearchFieldAttribute), false).Any()
                    && property.GetValue(this, null) != null)
                {
                    var displayName =
                        property.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast
                            <DisplayNameAttribute>().FirstOrDefault();
                    if (property.PropertyType == typeof(IList<string>))
                    {
                        if (((IList<string>)property.GetValue(this, null)).Any())
                        {
                            retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                                       String.Join(", ", ((IList<string>)property.GetValue(this, null)).OrderBy(m => m)));
                        }
                    }
                    else if (property.PropertyType == typeof(bool?))
                    {
                        var value = (bool?)property.GetValue(this, null);
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                            value.HasValue && value.Value ? "Yes" : "No");
                    }
                    else if (property.PropertyType == typeof(bool))
                    {
                        var value = (bool)property.GetValue(this, null);
                        if (value)
                        {
                            retval.Add(displayName == null ? property.Name : displayName.DisplayName, "Yes");
                        }
                    }
                    else if (property.PropertyType == typeof(DateTime?))
                    {
                        var value = (DateTime?)property.GetValue(this, null);
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                            value.HasValue ? value.Value.ToShortDateString() : string.Empty);
                    }
                    else
                    {
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                                   property.GetValue(this, null).ToString());
                    }
                }
            }

            return retval;

        }

        public void Build()
        {
            var searchRepository = ComponentBrokerInstance.RetrieveComponent<ISearchRepository>();
            var options = searchRepository.GetDistinctOptions().ToList();
            this.Strategies = this.GetSearchOptionsByFieldType(options, FieldType.Strategy);
            this.MarketStages = this.GetSearchOptionsByFieldType(options, FieldType.MarketStage);
            this.Sectors = this.GetSearchOptionsByFieldType(options, FieldType.SectorFocus);
            this.InvestmentRegions = this.GetSearchOptionsByFieldType(options, FieldType.InvestmentRegion);
            this.SubStrategies = this.GetSearchOptionsByFieldType(options, FieldType.SubStrategy);
            this.InvestmentSubRegions = this.GetSearchOptionsByFieldType(options, FieldType.InvestmentSubRegion);
            this.Countries = this.GetSearchOptionsByFieldType(options, FieldType.Country);
            this.QualitativeGrades = this.GetSearchOptionsByFieldType(options, FieldType.QualitativeGrade);
            this.QuantitativeGrades = this.GetSearchOptionsByFieldType(options, FieldType.QuantitativeGrade);
            this.FundraisingStatuses = this.GetSearchOptionsByFieldType(options, FieldType.FundraisingStatus);
            this.Currencies = this.GetSearchOptionsByFieldType(options, FieldType.Currency);
            this.FocusLists = this.GetSearchOptionsByFieldType(options, FieldType.FocusList);
        }

        public SearchFilter ToSearchFilter()
        {
            return new SearchFilter
            {
                AccessConstrained = AccessConstrained,
                DiligenceStatusMaximum = DiligenceStatusMaximum,
                DiligenceStatusMinimum = DiligenceStatusMinimum,
                EmergingManager = EmergingManager,
                SectorSpecialist = SectorSpecialist,
                FocusList = SelectedFocusList,
                FundSizeMaximum = FundSizeMaximum,
                FundSizeMinimum = FundSizeMinimum,
                FundraisingStatus = SelectedFundraisingStatus,
                InvestmentRegions = SelectedInvestmentRegions,
                InvestmentSubRegions = SelectedInvestmentSubRegion,
                SubStrategies = SelectedSubStrategy,
                Countries = SelectedCountry,
                QualitativeGrades = SelectedQualitativeGrades,
                QuantitativeGrades = SelectedQuantitativeGrades,
                MarketStage = SelectedMarketStage,
                NumberOfFundsClosedMaximum = NumberOfFundsClosedMaximum,
                NumberOfFundsClosedMinimum = NumberOfFundsClosedMinimum,
                Sector = SelectedSector,
                Strategies = SelectedStrategy,
                Term = Search,
                ExpectedNextFundraiseEndDate = SelectedNextFundraiseEndDate,
                ExpectedNextFundraiseStartDate = SelectedNextFundraiseStartDate,
                ShowOnlyPublished = ShowOnlyPublished,
                Currencies = SelectedCurrencies,
                SBICFund = SBICFund
            };
        }

        private List<string> GetSearchOptionsByFieldType(IEnumerable<SearchOption> options, FieldType fieldType)
        {
            return options
                .Where(m => m.Field == (int)fieldType)
                .Select(m => m.Value)
                .ToList();
        }
    }
}