﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlanticBT.Common.Validation;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            GroupOptions = new Dictionary<object, string>();
            RoleOptions = new Dictionary<object, string>();
        }

        /// <summary>
        /// the current UserEntity (of logged in or impersonated user)
        /// </summary>
        public UserEntity UserEntity { get; set; }

        /// <summary>
        /// Route (from routing map) to return to after user edit. 
        /// </summary>
        public string ReturnRoute { get; set; }

        public Guid? Id { get; set; }

        [Required]
        [AtlanticBT.Common.Validation.Email(ErrorMessage = "Email is not in correct format.")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string Email { get; set; }

        [Required]
        [DisplayName("First Name")]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        [StringLength(255)]
        public string LastName { get; set; }

        [DisplayName("Rss")]
        public bool RssAllowed { get; set; }

        [DisplayName("Diligence")]
        public bool DiligenceAllowed { get; set; }

        [DisplayName("Is Group Leader?")]
        public bool IsGroupLeader { get; set; }

        public bool IsLockedOut { get; set; }

        [DisplayName("Active User?")]
        public bool IsApproved { get; set; }

        [DisplayName("Role")]
        public string Role { get; set; }

        [DisplayName("Group")]
        public Guid? GroupId { get; set; }

        public Dictionary<object, string> GroupOptions { get; set; }

        public Dictionary<object, string> RoleOptions { get; set; }

        //Additional Information
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(100)]
        [DisplayName("Organization Name")]
        public string OrganizationName { get; set; }

        [DisplayName("Organization Type")]
        public OrganizationType OrganizationType { get; set; }

        [StringLength(255)]
        [DisplayName("Referred By")]
        public string ReferredBy { get; set; }

        public IEnumerable<DateTime> EulaHistory { get; set; } 

        /// <summary>
        /// build from existing/db
        /// </summary>
        /// <param name="userEntity">Currently logged in user</param>
        /// <param name="modelEntity">user being manipulated (model)</param>
        /// <param name="userService"></param>
        /// <returns></returns>
        public static UserViewModel Build(UserEntity userEntity, UserEntity modelEntity, IUserService userService)
        {
            var retval = new UserViewModel()
                       {
                           Id = modelEntity.Id,
                           Email = modelEntity.Email,
                           FirstName = modelEntity.FirstName,
                           LastName = modelEntity.LastName,
                           Phone = modelEntity.Phone,
                           Title = modelEntity.Title,
                           OrganizationName = modelEntity.OrganizationName,
                           OrganizationType = modelEntity.OrganizationType,
                           ReferredBy = modelEntity.ReferredBy,
                           RssAllowed = modelEntity.RssAllowed,
                           DiligenceAllowed = modelEntity.DiligenceAllowed,
                           IsLockedOut = modelEntity.IsLockedOut,
                           IsApproved = modelEntity.IsApproved,
                           GroupId = modelEntity.GroupId,
                           IsGroupLeader = modelEntity.IsGroupLeader,
                           Role = modelEntity.IsAdmin ? RoleNames.Admin : modelEntity.IsEmployee ? RoleNames.Employee : "member",
                           EulaHistory = modelEntity.EulaHistory
                       };

            retval.PopulateDefaults(userEntity, userService);

            return retval;

        }

        /// <summary>
        /// Rebuild from model.
        /// </summary>
        /// <param name="userEntity"></param>
        /// <param name="model"></param>
        /// <param name="userService"></param>
        public static void Build(UserEntity userEntity, UserViewModel model, IUserService userService)
        {
            model.PopulateDefaults(userEntity, userService);
        }

        /// <summary>
        /// Build a new model
        /// </summary>
        /// <param name="userEntity">logged in UserEntity</param>
        /// <param name="groupId"></param>
        /// <param name="userService"></param>
        public static UserViewModel Build(UserEntity userEntity, Guid? groupId, IUserService userService)
        {
            UserViewModel model = new UserViewModel();
            model.GroupId = groupId;
            model.PopulateDefaults(userEntity, userService);
            return model;
        }

        private void PopulateDefaults(UserEntity userEntity, IUserService userService)
        {
            this.UserEntity = userEntity;
            this.GroupOptions = userService.GetAllGroups().OrderBy(o => o.Name).ToDictionary(x => (object)x.Id, x => x.Name);
            this.RoleOptions = new Dictionary<object, string>()
                {
                    {"Member", "Member"},
                    {RoleNames.Employee, "RCP Employee"},
                    {RoleNames.Admin, "RCP Admin"}
                };
        }
    }
}