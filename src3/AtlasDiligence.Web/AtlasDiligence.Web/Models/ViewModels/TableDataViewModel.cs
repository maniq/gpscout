﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class TableDataViewModel
    {
        // organization Id
        public Guid Id { get; set; }

        // organization name
        public string Name { get; set; }

        // vintage years
        public IEnumerable<int> VintageYears { get; set; }

        // cell data to display
        public Dictionary<int, List<string>> CellData { get; set; }

        // funds
        public IEnumerable<Fund> Funds { get; set; }

        // grades
        public EntitySet<Grade> Grades { get; set; }

        public void AddCellData(int key, string value)
        {
            if (CellData.ContainsKey(key))
            {
                CellData[key].Add(value);
            }
            else
            {
                CellData.Add(key, new List<string> { value });
            }
        }
    }
}