﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Enums;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models.ViewModels.Search;

namespace AtlasDiligence.Web.Models.ViewModels
{


    public class OrganizationViewModel
    {
        public Guid Id { get; set; }

        public IEnumerable<Common.Data.Models.Folder> Folders { get; set; }

        public Common.Data.Models.Organization Organization { get; set; }

        public IEnumerable<OrganizationMember> OrganizationMembers { get; set; }

        public IEnumerable<OrganizationNote> OrganizationNotes { get; set; } 

        public IEnumerable<FirmNote> FirmNotes { get; set; }
        
        public IEnumerable<NewsItem> NewsItems { get; set; }

        public ProductTypes GroupProductType { get; set; }

        public List<LuceneSearchResult> OrganizationsInSameSegment { get; set; }

        public SearchOptionsViewModel SearchOptions { get; set; }

        public bool Preview { get; set; }
    }
}