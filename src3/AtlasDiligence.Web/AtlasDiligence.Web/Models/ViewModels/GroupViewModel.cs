﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels
{
    using AtlasDiligence.Common.DTO.Enums;

    public class GroupViewModel
    {
        public GroupViewModel()
        {
            Organizations = new Dictionary<Guid, string>();
            Segments = new Dictionary<Guid, string>();
        }

        public Guid Id { get; set; }

        public Guid CrmId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [Range(1, 999999, ErrorMessage = "Max Users must be at least 1.")]
        [DisplayName("Max Users")]
        public int MaxUsers { get; set; }

        [DisplayName("Default Product Type")]
        public ProductTypes DefaultProductType { get; set; }

        public Dictionary<Guid, string> Organizations { get; set; }

        public Dictionary<Guid, string> Segments { get; set; }

        /// <summary>
        /// number of active users for this group
        /// </summary>
        public int ActiveUsers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="_groupRepository"></param>
        /// <returns></returns>
        public static object Build(Group group, IGroupRepository _groupRepository)
        {
            var organizationIds = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
            var organizations = _groupRepository.GetGroupOrganizations(organizationIds).ToDictionary(organization => organization.Id, organization => organization.Name);
            var segments = group.GroupSegments.ToDictionary(x => x.SegmentId, x => x.Segment.Name);

            return new GroupViewModel
            {
                Id = group.Id,
                CrmId = group.CrmId,
                MaxUsers = group.MaxUsers,
                Name = group.Name,
                DefaultProductType = (ProductTypes)group.DefaultProductType,
                ActiveUsers = _groupRepository.GetActiveUsersByGroupId(group.Id).Count(),
                Organizations = organizations,
                Segments = segments
            };
        }

        public static void PopulateDefaults(GroupViewModel model,IGroupRepository _groupRepository)
        {
            var group = _groupRepository.GetById(model.Id);
            var organizationIds = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
            var organizations = _groupRepository.GetGroupOrganizations(organizationIds).ToDictionary(organization => organization.Id, organization => organization.Name);
            var segments = group.GroupSegments.ToDictionary(x => x.SegmentId, x => x.Segment.Name);
            model.ActiveUsers = _groupRepository.GetActiveUsersByGroupId(model.Id).Count();
            model.Organizations = organizations;
            model.Segments = segments;
        }
    }
}