﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AtlanticBT.Common.Validation;

namespace AtlasDiligence.Web.Models.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [AtlanticBT.Common.Validation.Email(ErrorMessage = "Email is not in correct format.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
