﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Common.DTO.General;
using AtlasDiligence.Common.DTO.Services;
using Umbraco_ = Umbraco;

namespace AtlasDiligence.Web.Umbraco
{
    public class GPScoutApplication : Umbraco_.Cms.Web.UI.MvcApplication
    {
        public override void Init()
        {
            base.Init();
            if (IsApplicationStart)
            {
                ApplicationHelper.RegisterTypes();
                OrganizationHelperService.RemoveObsoleteDataItems();
            }
        }

        public bool IsApplicationStart { 
            get {
                return !ComponentBrokerInstance.HasType<ITaskRepository>();
            }
        }
    }
}