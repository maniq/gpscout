$(function() {    
  
  //$(".members-inner").find("h3").click(function () { $("#members-form").slideToggle(); });
  //$(".request-inner").find("h3").click(function () { $("#request-access-inner").slideToggle(); });
  
  
  //Request Access ajax
  $("#request-access-submit").live("click", function (e, ui) {
      var thisForm = $("#request-access-form form");
      thisForm.find('#request-access-submit').hide();
      thisForm.find('.ajax-loader').show();
      $.post(thisForm.attr('action'),
          thisForm.serialize(),
          function (model) {
              _gaq.push(['_trackPageview', '/request_access']);
              $("#request-access-inner").replaceWith(model.RenderView);
            $("#request-access-inner").show();
          });
      e.preventDefault();
  });
  
  $('ul.team li').each(function(){
    //get the first paragraph
    var firstParagraph = $(this).find('p:first');
    
    //remove all other paragraphs
    $(this).find('p:not(:first)').remove();
    
    //truncate the paragraph
    if ($(firstParagraph).html().length > 300)
    {
      $(firstParagraph).html($(firstParagraph).html().substring(0,300) + '...');
    }    
  });
});