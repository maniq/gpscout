﻿using System;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.DTO.Services;
using log4net;
using Models = AtlasDiligence.Common.Data.Models;
using AtlanticBT.Common.GeoLocation;


namespace AtlasDiligence.DiligenceApplication
{
	/// <summary>
	/// Performs BL between CRM news entities and local news entities.
	/// </summary>
	public class SearchService
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(SearchService));
		private AtlasWebDbDataContext context;
		private readonly OrganizationService organizationService;

		public SearchService()
		{
			context = new AtlasWebDbDataContext();
			this.organizationService = new OrganizationService();
		}

		public void RunInserts()
		{
			DeleteTables();

			InsertClientRequests();

			InsertCurrencies();

			InsertAndUpdateOrganizations();

			InsertSearchOptions();

			InsertResearchPriorityOrganizations();

	        InsertBenchmarks();

			GetGeocodes();

			new AtlasDiligence.Common.DTO.Services.IndexService().ReIndex();
		}

	    private void InsertBenchmarks()
	    {
			context.Benchmarks.InsertAllOnSubmit(organizationService.ImportBenchmarks());
			context.SubmitChanges();
	    }

	    private void InsertClientRequests()
		{
			this.context.ClientRequests.InsertAllOnSubmit(organizationService.GetAllClientRequests(true));
			this.context.SubmitChanges();
		}

		private void InsertResearchPriorityOrganizations()
		{
			var organizations = this.organizationService.GetAllResearchPriorityOrganizations(true);
			context.ResearchPriorityOrganizations.InsertAllOnSubmit(organizations);
			context.SubmitChanges();
		}
		/// <summary>
		/// insert options for search drop down lists
		/// </summary>
		private void InsertSearchOptions()
		{
			try
			{

				////entitty multiple values
				//var searchOptions =
				//    context.EntityMultipleValues.Select(x => new { Field = x.Field, Value = x.Value }).Distinct();
				//foreach (var searchOption in searchOptions)
				//{
				//    context.SearchOptions.InsertOnSubmit(new SearchOption { Field = searchOption.Field, Value = searchOption.Value });
				//}


				//org & fund geographic focus
				var geographicFocues = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.GeographicFocus).Distinct().Union(context.Funds.Where(m => m.Organization.PublishSearch || m.Organization.PublishOverviewAndTeam).Select(x => x.GeographicFocus).Distinct());
				foreach (var geographicFocus in geographicFocues.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.GeographicFocus, Value = geographicFocus });
				}

				var strategiesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.StrategyPipeDelimited).Distinct();
				var strategies = String.Join("|", strategiesPiped).Split(new[] { '|' }).Distinct();
				foreach (var strategy in strategies.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.Strategy, Value = strategy });
				}

				var subStrategyPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SubStrategyPipeDelimited).Distinct();
				var subStrategies = String.Join("|", subStrategyPiped).Split(new[] { '|' }).Distinct();
				foreach (var subStrategy in subStrategies.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.SubStrategy, Value = subStrategy });
				}

				var sectorFocusesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SectorFocusPipeDelimited).Distinct();
				var sectorFocuses = String.Join("|", sectorFocusesPiped).Split(new[] { '|' }).Distinct();
				foreach (var focus in sectorFocuses.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.SectorFocus, Value = focus });
				}

				var countriesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.CountriesPipeDelimited).Distinct();
				var countries = String.Join("|", countriesPiped).Split(new[] { '|' }).Distinct();
				foreach (var country in countries.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.Country, Value = country });
				}

				var investmentRegionsPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.InvestmentRegionPipeDelimited).Distinct();
				var investmentRegions = String.Join("|", investmentRegionsPiped).Split(new[] { '|' }).Distinct();
				foreach (var investmentRegion in investmentRegions.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.InvestmentRegion, Value = investmentRegion });
				}

				var subRegionsPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SubRegionsPipeDelimited).Distinct();
				var subRegions = String.Join("|", subRegionsPiped).Split(new[] { '|' }).Distinct();
				foreach (var subRegion in subRegions.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.InvestmentSubRegion, Value = subRegion });
				}

				var quantitativeGrades = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.QuantitativeGrade).Distinct();
				foreach (var grade in quantitativeGrades.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.QuantitativeGrade, Value = grade });
				}

				var qualitativeGrades = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.QualitativeGrade).Distinct();
				foreach (var grade in qualitativeGrades.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.QualitativeGrade, Value = grade });
				}

				var marketStages = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.MarketStage).Distinct().Union(context.Funds.Where(m => m.Organization.PublishSearch || m.Organization.PublishOverviewAndTeam).Select(x => x.MarketStage).Distinct());
				foreach (var marketStage in marketStages.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.MarketStage, Value = marketStage });
				}

				var fundraisingStatuses = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.FundRaisingStatus).Distinct();
				foreach (var status in fundraisingStatuses.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
				{
					context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.FundraisingStatus, Value = status });
				}

				context.SearchOptions.InsertAllOnSubmit(
					context.Organizations.Select(m => m.FocusRadar).Distinct().ToList().Where(m => !String.IsNullOrWhiteSpace(m)).Select(
						m => new SearchOption { Field = (int)FieldType.FocusList, Value = m }));

				context.SearchOptions.InsertAllOnSubmit(context.Currencies.ToList().Select(m => new SearchOption { Field = (int)FieldType.Currency, Value = m.Name }));

				context.SubmitChanges();

			}
			catch (Exception ex)
			{
				Log.Fatal("Error inserting SearchOptions. Inner exception: " + ex.Message);
				throw;
			}
		}

		private void GenericDetach<T>(T entity) where T : class
		{
			foreach (PropertyInfo pi in entity.GetType().GetProperties())
			{
				if (pi.GetCustomAttributes(typeof(System.Data.Linq.Mapping.AssociationAttribute), false).Length > 0)
				{
					// Property is associated to another entity
					Type propType = pi.PropertyType;
					// Invoke Empty contructor (set to default value)
					ConstructorInfo ci = propType.GetConstructor(new Type[0]);
					pi.SetValue(entity, ci.Invoke(null), null);
				}
			}
		}

		private void GetGeocodes()
		{
			var allOrgs = this.context.Organizations.Where(w => (w.PublishOverviewAndTeam || w.PublishSearch) && (!w.Latitude.HasValue || !w.Longitude.HasValue));
			foreach (var org in allOrgs)
			{
				var retval = GetAddress(org.Address1, org.Address2, org.Address3, org.Address1City, org.PostalCode, org.Address1Country);
				if (retval != null)
				{
					org.Latitude = Convert.ToDouble(retval.Geometry.Location.Latitude);
					org.Longitude = Convert.ToDouble(retval.Geometry.Location.Longitude);
				}
			}

			var otherAddresses = this.context.OtherAddresses.Where(w => !w.Latitude.HasValue || !w.Longitude.HasValue);
			foreach (var org in otherAddresses)
			{
				var retval = GetAddress(org.Address1, org.Address2, org.Address3, org.City, org.PostalCode, org.Country);
				if (retval != null)
				{
					org.Latitude = Convert.ToDouble(retval.Geometry.Location.Latitude);
					org.Longitude = Convert.ToDouble(retval.Geometry.Location.Longitude);
				}
			}

			this.context.SubmitChanges();
		}

		private GeocodeAddress GetAddress(string address1, string address2, string address3, string city, string postalCode, string country)
		{
			try
			{
				var geocodeService = new GeocodingService();

				// first attempt
				var firstQuery = String.Format("{0} {1} {2} {3} {4} {5}", address1, address2, address3, city, postalCode, country).Trim();

				if (!String.IsNullOrWhiteSpace(firstQuery))
				{
					var retval = geocodeService.GetAddress(firstQuery).Results;
					if (retval.Any())
					{
						return retval.FirstOrDefault();
					}
				}

				// second attempt
				var secondQuery = String.Format("{0} {1} {2} {3}", address1, city, postalCode, country).Trim();
				if (!String.IsNullOrWhiteSpace(secondQuery))
				{
					var retval = geocodeService.GetAddress(secondQuery).Results;
					if (retval.Any())
					{
						return retval.FirstOrDefault();
					}
				}

			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}

			return null;
		}

		/// <summary>
		/// Organizations have FK dependencies in Atlas (OrganizationRequest, etc.) and should not be deleted.
		/// Therefore, we only insert for new orgs; otherwise update org data. All child data (funds, etc.) are
		/// deleted and inserted anew.
		/// </summary>
		private void InsertAndUpdateOrganizations()
		{
			try
			{
				var existingOrganizations = context.Organizations;
				var existingOrganizationIds = existingOrganizations.Select(x => x.Id);

				Func<Models.Organization, bool> isOrganizationInLocalDb = x => existingOrganizationIds.Contains(x.Id);
				Func<Models.Organization, Models.Organization> getLocalOrganizationFromId = m => existingOrganizations.FirstOrDefault(v => v.Id == m.Id);

				// unpublish everything that exists in local but is not in CRM published orgs
				foreach (var organization in existingOrganizations)
				{
					organization.PublishSearch = false;
					organization.PublishOverviewAndTeam = false;
				}
				context.SubmitChanges();

				// avoid memory leaks by paging large results from CRM
				int page = 0, pageSize = 100, count = 0;

				do
				{
					//var organizationsFromCrm = new List<Models.Organization> { organizationService.GetById(new Guid("d5d40b5e-86cc-e211-89b5-78e3b5100e8d"), true) };
					var organizationsFromCrm = organizationService.GetSegment(page * pageSize, pageSize).ToList();
					count = organizationsFromCrm.Count();

					foreach (var organizationFromCrm in organizationsFromCrm)
					{
						var orgName = string.Empty;
						var orgId = new Guid();
						try
						{
							context = new AtlasWebDbDataContext();
							var isNew = false;

							if (!isOrganizationInLocalDb(organizationFromCrm))
							{
								isNew = true;
								InsertEntity(organizationFromCrm.Id, EntityType.Organization);
							}

							if (isNew)
							{
								context.Organizations.InsertOnSubmit(organizationFromCrm);
								context.Grades.InsertAllOnSubmit(organizationFromCrm.Grades);
							}
							else
							{
								var localOrg = getLocalOrganizationFromId(organizationFromCrm);
								this.GenericDetach(localOrg);

								if (!organizationFromCrm.Latitude.HasValue && !organizationFromCrm.Longitude.HasValue)
								{
									organizationFromCrm.Latitude = localOrg.Latitude;
									organizationFromCrm.Longitude = localOrg.Longitude;
								}

								context.Organizations.Attach(organizationFromCrm);
								context.Refresh(RefreshMode.KeepCurrentValues, organizationFromCrm);
								context.Grades.InsertAllOnSubmit(organizationFromCrm.Grades);
							}
							orgName = organizationFromCrm.Name;
							orgId = organizationFromCrm.Id;

							//sector
							foreach (var sectorFocus in organizationFromCrm.SectorFocus)
							{
								var entityMultiple = MapEntityMultiple(organizationFromCrm.Id, FieldType.SectorFocus,
																	   sectorFocus);
								context.EntityMultipleValues.InsertOnSubmit(entityMultiple);
							}
							//strategy
							foreach (var strategy in organizationFromCrm.Strategy)
							{
								var entityMultiple = MapEntityMultiple(organizationFromCrm.Id, FieldType.Strategy, strategy);
								context.EntityMultipleValues.InsertOnSubmit(entityMultiple);
							}

							//Funds
							InsertFunds(organizationFromCrm);

							Log.Info(isNew
										 ? string.Format("Insert Organization: {0} {1}", organizationFromCrm.Name,
														 organizationFromCrm.Id)
										 : string.Format("Update Organization: {0} {1}", organizationFromCrm.Name,
														 organizationFromCrm.Id));

							try
							{
								context.SubmitChanges();
							}
							catch (Exception e)
							{
								var info = typeof(Models.Organization).GetProperties().Where(m => m.PropertyType.Equals(typeof(string)) || m.PropertyType.Equals(typeof(String)));
								foreach (var property in info.OrderBy(m => m.Name))
								{
									try
									{
										System.Diagnostics.Debug.WriteLine(property.Name + " - " + ((string)property.GetValue(organizationFromCrm, null)).Length);
									}
									catch
									{
									}
								}

								Log.Error("Error saving record for crm org id = " + organizationFromCrm.Id + ", Message: " + e.Message);
								throw e;
							}
						}
						catch (Exception e)
						{
							var errorText = "CRM Transport related error retrieving organization: " + orgName + ", Id: " + orgId + ", Message: " + e.Message;
							System.Diagnostics.Debug.WriteLine(errorText);
							Log.Error(errorText);
						}
					}

					page++;
				} while (count == pageSize);
			}
			catch (Exception e)
			{
				Log.Error("Error retrieving the list of organizations from CRM.  Inner exception... " + e.Message);
				throw;
			}
		}

		private void InsertCurrencies()
		{
			var currencies = organizationService.GetAllCurrencies();
			context.Currencies.InsertAllOnSubmit(currencies);
			context.SubmitChanges();
		}

		private void InsertEntity(Guid id, EntityType entityType)
		{
			var entity = new Entity()
			{
				Id = id,
				EntityType = (int)entityType
			};
			context.Entities.InsertOnSubmit(entity);
		}

		private void InsertFunds(AtlasDiligence.Common.Data.Models.Organization organization)
		{
			var crmFunds = this.organizationService.GetFundsByOrganizationId(organization.Id);

			var dueDiligenceItems = this.organizationService.GetDueDiligenceItemsByOrganizationId(organization.Id);
			context.DueDiligenceItems.InsertAllOnSubmit(dueDiligenceItems.Where(w => !context.DueDiligenceItems.Contains(w)));

			var otherAddresses = this.organizationService.GetOtherAddressesByOrganizationId(organization.Id);
			context.OtherAddresses.InsertAllOnSubmit(otherAddresses.Where(w => !context.OtherAddresses.Contains(w)));

			var trackRecords = this.organizationService.GetTrackRecordsByOrganizationId(organization.Id);
			context.TrackRecords.InsertAllOnSubmit(trackRecords.Where(w => !context.TrackRecords.Contains(w)));

			var organizationMembers =
				organizationService.GetOrganizationMembersByOrganizationId(organization.Id).ToList();
			context.OrganizationMembers.InsertAllOnSubmit(organizationMembers.Where(w => !context.OrganizationMembers.Contains(w)));

			var portfolioCompanies =
				organizationService.GetPortfolioCompaniesByOrganizationId(organization.Id).ToList();
			context.PortfolioCompanies.InsertAllOnSubmit(portfolioCompanies.Where(w => !context.PortfolioCompanies.Contains(w)));

			var evaluations =
				organizationService.GetEvaluationsByOrganizationId(organization.Id).ToList();
			context.Evaluations.InsertAllOnSubmit(evaluations.Where(w => !context.Evaluations.Contains(w)));

			foreach (var member in organizationMembers)
			{
				var empHistory = organizationService.GetEmploymentHistoriesByOrganizationMemberId(member.Id);
				var edHistory = organizationService.GetEducationHistoriesByOrganizationMemberId(member.Id);
				var boardSeats = organizationService.GetBoardSeatsByOrganizationMemberId(member.Id);
				context.EmploymentHistories.InsertAllOnSubmit(empHistory);
				context.EducationHistories.InsertAllOnSubmit(edHistory);
				context.BoardSeats.InsertAllOnSubmit(boardSeats);
			}

			foreach (var crmFund in crmFunds)
			{
				InsertEntity(crmFund.Id, EntityType.Fund);

				//sector
				foreach (var sectorFocus in crmFund.SectorFocus)
				{
					var entityMultiple = MapEntityMultiple(crmFund.Id, FieldType.SectorFocus, sectorFocus);
					context.EntityMultipleValues.InsertOnSubmit(entityMultiple);
				}

				context.Funds.InsertOnSubmit(crmFund);
#if DEBUG

                //try
                //{
                //    //context.SubmitChanges();
                //}
                //catch (Exception e)
                //{
                //    var info = typeof(Fund).GetProperties().Where(m => m.PropertyType.Equals(typeof(string)) || m.PropertyType.Equals(typeof(String)));
                //    foreach (var property in info.OrderBy(m => m.Name))
                //    {
                //        try
                //        {
                //            System.Diagnostics.Debug.WriteLine(property.Name + " - " +
                //                                               ((string)property.GetValue(crmFund, null)).Length);
                //        }
                //        catch { }
                //    }
                //    throw e;
                //}
#endif
				var limitedPartners = this.organizationService.GetLimitedPartnersByFundId(crmFund.Id)
					.ToList();
				limitedPartners.ForEach(m =>
				{
					m.FundId = crmFund.Id;
					m.Id = Guid.NewGuid();
				});
				context.LimitedPartners.InsertAllOnSubmit(limitedPartners);
				var advisoryBoards = this.organizationService.GetAdvisoryBoardsByFundId(crmFund.Id);
				context.AdvisoryBoards.InsertAllOnSubmit(advisoryBoards);


			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="entityId"></param>
		/// <param name="fieldTypeName"></param>
		/// <param name="sectorFocus"></param>
		/// <returns></returns>
		private EntityMultipleValue MapEntityMultiple(Guid entityId, FieldType fieldTypeName, string sectorFocus)
		{
			return new EntityMultipleValue()
			{
				Id = Guid.NewGuid(),
				EntityId = entityId,
				Field = (int)fieldTypeName,
				Value = sectorFocus
			};
		}

		/// <summary>
		/// delete all local tables related to search options
		/// </summary>
		private void DeleteTables()
		{
			context.ClientRequests.DeleteBatch(context.ClientRequests);
			context.ResearchPriorityOrganizations.DeleteBatch(context.ResearchPriorityOrganizations);
			context.EntityMultipleValues.DeleteBatch(context.EntityMultipleValues);
			context.Entities.DeleteBatch(context.Entities.Where(x => x.EntityType == (int)EntityType.Fund));
			context.OrganizationMembers.DeleteBatch(context.OrganizationMembers);
			context.BoardSeats.DeleteBatch(context.BoardSeats);
			context.DueDiligenceItems.DeleteBatch(context.DueDiligenceItems);
			context.EducationHistories.DeleteBatch(context.EducationHistories);
			context.EmploymentHistories.DeleteBatch(context.EmploymentHistories);
			context.Evaluations.DeleteBatch(context.Evaluations);
			context.LimitedPartners.DeleteBatch(context.LimitedPartners);
			context.OtherAddresses.DeleteBatch(context.OtherAddresses);
			context.PortfolioCompanies.DeleteBatch(context.PortfolioCompanies);
			context.TrackRecords.DeleteBatch(context.TrackRecords);
			context.Currencies.DeleteBatch(context.Currencies);
			context.Funds.DeleteBatch(context.Funds);
			context.SearchOptions.DeleteBatch(context.SearchOptions);
			context.Grades.DeleteBatch(context.Grades);
	        context.Benchmarks.DeleteBatch(context.Benchmarks);
			context.SubmitChanges();
		}
	}
}
