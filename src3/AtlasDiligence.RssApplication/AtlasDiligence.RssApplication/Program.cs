﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.DTO.Model;
using System.ComponentModel;
using AtlasDiligence.RssApplication.Properties;
using log4net;

namespace AtlasDiligence.RssApplication
{
    internal enum ExitCode
    {
        Success = 0,
        InvalidLogin = 1,
        InvalidFilename = 2,
        UnknownError = 10
    }

    public class Program
    {
        private static IList<NewsItem> _newsItems;

        /// <summary>
        /// Key = Organization ID in CRM, Values = Organization.Name and any Organization.Aliases
        /// </summary>
        private static IEnumerable<KeyValuePair<Guid, List<string>>> _organizationList;

        /// <summary>
        /// Key = Contact ID in CRM, Values = Contact.Name and any Contact.Aliases
        /// </summary>
        private static IEnumerable<KeyValuePair<Guid, List<string>>> _contactList;
        private static bool _isNewsItemsDone;
        private static bool _isOrganizationListDone;
        private static bool _isContactListDone;
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private const string LogFilePath = "Logs/";
        private static string _logFileName;

        static void Main(string[] args)
        {
            _isNewsItemsDone = false;
            _isOrganizationListDone = false;
            _isContactListDone = false;
            _logFileName = String.Format("NewsImportLog {0}{1}{2}{3}{4}.txt",
                                        DateTime.Now.Year.ToString().PadLeft(2, '0'),
                                        DateTime.Now.Month.ToString().PadLeft(2, '0'),
                                        DateTime.Now.Day.ToString().PadLeft(2, '0'),
                                        DateTime.Now.Hour.ToString().PadLeft(2, '0'),
                                        DateTime.Now.Minute.ToString().PadLeft(2, '0'));
            log4net.GlobalContext.Properties["LogName"] = Settings.Default.LogFilePath + _logFileName;
            log4net.Config.XmlConfigurator.Configure();

            var getNewsItemsWorker = new BackgroundWorker();
            getNewsItemsWorker.DoWork += GetNewsItemsWorkerDoWork;
            getNewsItemsWorker.RunWorkerCompleted += GetNewsItemsWorkerRunWorkerCompleted;
            getNewsItemsWorker.RunWorkerAsync();

            var getOrganizationListWorker = new BackgroundWorker();
            getOrganizationListWorker.DoWork += GetOrganizationListWorkerDoWork;
            getOrganizationListWorker.RunWorkerCompleted += GetOrganizationListWorkerRunWorkerCompleted;
            getOrganizationListWorker.RunWorkerAsync();

            var getContactListWorker = new BackgroundWorker();
            getContactListWorker.DoWork += GetContactListWorkerDoWork;
            getContactListWorker.RunWorkerCompleted += GetContactListWorkerRunWorkerCompleted;
            getContactListWorker.RunWorkerAsync();

            Console.ReadLine();
        }
        static void GetContactListWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _contactList = (IEnumerable<KeyValuePair<Guid, List<string>>>)e.Result;
            _isContactListDone = true;
            DoSearch();
        }

        static void GetNewsItemsWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _newsItems = (IList<NewsItem>)e.Result;
            _isNewsItemsDone = true;
            DoSearch();
        }

        static void GetOrganizationListWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _organizationList = (IEnumerable<KeyValuePair<Guid, List<string>>>)e.Result;
            _isOrganizationListDone = true;
            DoSearch();
        }

        static void GetOrganizationListWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = NewsService.GetOrganizationList();
            }
            catch (Exception ex)
            {
                Log.Fatal("Process halted due to error. " + ex.Message);
                SendMail();
                Environment.Exit((int)ExitCode.UnknownError);
            }
        }

        static void GetContactListWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = NewsService.GetContactList();
            }
            catch (Exception ex)
            {
                Log.Fatal("Process halted due to error. " + ex.Message);
                SendMail();
                Environment.Exit((int)ExitCode.UnknownError);
            }
        }

        static void GetNewsItemsWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = RssReaderFacade.GetNewsItems();
            }
            catch (Exception ex)
            {
                Log.Fatal("Process halted due to error. " + ex.Message);
                SendMail();
                Environment.Exit((int)ExitCode.UnknownError);
            }
        }

        private static void DoSearch()
        {
            if (_isNewsItemsDone && _isOrganizationListDone && _isContactListDone)
            {
                var count = 0;

                //Deletes and Rebuilds in AssociatedTerms table
                var termVals = _organizationList.Union(_contactList);
                NewsService.InsertAssociatedTerms(termVals);
                
                // remove duplicate
                var uniqueSubjects = _newsItems.GroupBy(g => g.Subject).Select(s => s.First()).ToList();

                Log.Debug("Doing search of organization list on news items.");
                foreach (var newsItem in uniqueSubjects)
                {
                    foreach (var organization in _organizationList)
                    {
                        var isInSubject = organization.Value.Any(s => newsItem.Subject.ToLower().Contains(s.ToLower()));
                        var isInContent = false;
                        var isInSummary = false;
                        if (!String.IsNullOrEmpty(newsItem.Content))
                        {
                            isInContent =
                                organization.Value.Any(
                                    s => newsItem.Content.ToLower().Contains(s.ToLower()));
                        }
                        if (!String.IsNullOrEmpty(newsItem.Summary))
                        {
                            isInSummary =
                                organization.Value.Any(
                                    s => newsItem.Summary.ToLower().Contains(s.ToLower()));
                        }
                        if (isInSubject || isInContent || isInSummary)
                        {
                            newsItem.OrganizationIds.Add(organization.Key);
                        }
                    }
                    foreach (var contact in _contactList)
                    {
                        var isInSubject = contact.Value.Any(s => newsItem.Subject.ToLower().Contains(s.ToLower()));
                        var isInContent = false;
                        if (!String.IsNullOrEmpty(newsItem.Content))
                        {
                            isInContent =
                                contact.Value.Any(
                                    s => newsItem.Content.ToLower().Contains(s.ToLower()));
                        }
                        if (isInSubject || isInContent)
                        {
                            newsItem.ContactIds.Add(contact.Key);
                        }
                    }
                    NewsService.InsertNews(newsItem);
                    count++;
                }
                Log.Debug("Attempted to add " + count + " news items to both databases");
                SendMail();
                Environment.Exit((int)ExitCode.Success);
            }
        }

        private static void SendMail()
        {
            try
            {
                var from = new MailAddress(AppSettings.MailFrom);
                var to = new MailAddress(ConfigurationManager.AppSettings["MailTo"]);
                var mail = new MailMessage
                               {
                                   From = from,
                                   Subject = AppSettings.NewsMailSubject + DateTime.Today.ToShortDateString(),
                                   IsBodyHtml = true,
                                   Body = System.IO.File.ReadAllText(LogFilePath + _logFileName)
                               };
                mail.BodyEncoding = new System.Text.UTF8Encoding();
                mail.To.Add(to);
                new SmtpClient().Send(mail);
            }
            catch (Exception ex)
            {
                Log.Fatal("Could not send log email.");
                throw;
            }
        }
    }
}
