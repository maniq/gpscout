﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Rhino.Mocks;

namespace AtlasDiligence.RssApplication.Tests
{
    [TestClass]
    public class NewsServiceTests
    {
        [TestMethod]
        public void GetAssociatedTermsTest()
        {
            var entityTerms = new List<KeyValuePair<Guid, List<string>>>()
                                  {
                                      new KeyValuePair<Guid, List<string>>(Guid.NewGuid(), new List<string>(){ "Citibank", "citicorp", "citi"})
                                  };

            var actual = NewsService.GetAssociatedTerms(entityTerms);

            Assert.AreEqual(6, actual.Count());
            Assert.IsTrue(actual.Where(x => x.SourceTerm == "citibank" && x.AssociatedTerm1 == "citicorp").Count() == 1);
            Assert.IsFalse(actual.Any(x => x.SourceTerm == "citibank" && x.AssociatedTerm1 == "citibank"));
        }

        /// <summary>
        /// Assure that only one AssociatedTerm gets created for similar strings found in multiple entityTerms
        /// </summary>
        [TestMethod]
        public void GetAssociatedTermsWithDupsTest()
        {
            var entityTerms = new List<KeyValuePair<Guid, List<string>>>()
                                  {
                                      new KeyValuePair<Guid, List<string>>(Guid.NewGuid(), new List<string>(){ "Citibank", "citicorp"}),
                                      new KeyValuePair<Guid, List<string>>(Guid.NewGuid(), new List<string>(){ "citi", "citicorp"}),
                                      new KeyValuePair<Guid, List<string>>(Guid.NewGuid(), new List<string>(){ "Citi", "Citicorp"})
                                  };

            var actual = NewsService.GetAssociatedTerms(entityTerms);

            Assert.AreEqual(4, actual.Count());
            Assert.IsTrue(actual.Where(x => x.SourceTerm == "citicorp").Count() == 2);
            Assert.IsFalse(actual.Any(x => x.SourceTerm == "citi" && x.AssociatedTerm1 == "citibank"));
        }
    }
}
