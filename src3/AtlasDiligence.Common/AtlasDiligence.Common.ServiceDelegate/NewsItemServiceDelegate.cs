﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;

namespace AtlasDiligence.Common.ServiceDelegate
{
    public class NewsItemServiceDelegate : ServiceDelegateBase
    {
        public void AssociateToOrganization(Guid newsItemId, Guid organizationId)
        {
                var linkedEntityRequest = new AssociateRequest
                                              {
                                                  Target =
                                                      new EntityReference(Account.EntityLogicalName, organizationId),
                                                  RelatedEntities = new EntityReferenceCollection
                                                                        {
                                                                            new EntityReference(
                                                                                new_newsitem.EntityLogicalName,
                                                                                newsItemId)
                                                                        },
                                                  Relationship = new Relationship("new_new_newsitem_account")
                                              };
                this.Context.Execute(linkedEntityRequest);
        }

        public void AssociateToContact(Guid newsItemId, Guid contactId)
        {
                var linkedEntityRequest = new AssociateRequest
                                              {
                                                  Target =
                                                      new EntityReference(Contact.EntityLogicalName, contactId),
                                                  RelatedEntities = new EntityReferenceCollection
                                                                        {
                                                                            new EntityReference(
                                                                                new_newsitem.EntityLogicalName,
                                                                                newsItemId)
                                                                        },
                                                  Relationship = new Relationship("new_new_newsitem_contact")
                                              };
                 this.Context.Execute(linkedEntityRequest);
        }

        public IEnumerable<new_newsitem> GetByOrganizationId(Guid id)
        {
            return from l in this.Context.CreateQuery<new_new_newsitem_account>()
                   join n in this.Context.CreateQuery<new_newsitem>()
                                  on l.new_newsitemid equals n.Id
                                  where l.accountid == id
                                  select n;
        }
    }
}
