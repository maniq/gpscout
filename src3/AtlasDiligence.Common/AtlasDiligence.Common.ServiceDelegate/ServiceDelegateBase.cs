﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk;
using System.Configuration;


namespace AtlasDiligence.Common.ServiceDelegate
{
	public class ServiceDelegateBase
	{
		private OrganizationServiceProxy ServiceProxy { get; set; }
		public OrganizationServiceContext Context { get; set; }

		public ServiceDelegateBase()
		{
			TimeSpan timeout;
			if (!TimeSpan.TryParse(ConfigurationManager.AppSettings["ServiceProxyTimeout"], out timeout))
				timeout = new TimeSpan(0, 1, 0);

			// setup proxy
			ServiceProxy = GetOrganizationServiceProxy();
			ServiceProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
			ServiceProxy.Timeout = timeout;

			// setup context
			Context = new OrganizationServiceContext(this.ServiceProxy);
		}

		internal OrganizationServiceProxy GetOrganizationServiceProxy()
		{
			// get configs
			var orgService = ConfigurationManager.AppSettings["OrganizationService"];
			if (string.IsNullOrEmpty(orgService)) throw new ConfigurationErrorsException("OrganizationService config setting is empty.");

			var userName = ConfigurationManager.AppSettings["UserName"];
			if (string.IsNullOrEmpty(orgService)) throw new ConfigurationErrorsException("UserName config setting is empty.");

			var password = ConfigurationManager.AppSettings["Password"];
			if (string.IsNullOrEmpty(orgService)) throw new ConfigurationErrorsException("Password config setting is empty.");
			
			// setup authentication
			IServiceManagement<IOrganizationService> orgServiceManagement =
					ServiceConfigurationFactory.CreateManagement<IOrganizationService>(
					new Uri(orgService));

			// Set the credentials.
			AuthenticationCredentials credentials = new AuthenticationCredentials();
			credentials.ClientCredentials.UserName.UserName = userName;
			credentials.ClientCredentials.UserName.Password = password;
			
			// return proxy
			return GetProxy<IOrganizationService, OrganizationServiceProxy>(orgServiceManagement, credentials);

		}

		private TProxy GetProxy<TService, TProxy>(
			IServiceManagement<TService> serviceManagement,
			AuthenticationCredentials authCredentials)
			where TService : class
			where TProxy : ServiceProxy<TService>
		{
			Type classType = typeof(TProxy);

			if (serviceManagement.AuthenticationType !=
				AuthenticationProviderType.ActiveDirectory)
			{
				AuthenticationCredentials tokenCredentials =
					serviceManagement.Authenticate(authCredentials);

				// Obtain discovery/organization service proxy for Federated, LiveId and OnlineFederated environments. 
				// Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and SecurityTokenResponse.
				return (TProxy)classType
					.GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) })
					.Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
			}

			// Obtain discovery/organization service proxy for ActiveDirectory environment.
			// Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and ClientCredentials.
			return (TProxy)classType
				.GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) })
				.Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
		}

		public void Dispose()
		{
			if (ServiceProxy != null)
			{
				ServiceProxy.Dispose();
			}
			if (Context != null)
			{
				Context.Dispose();
			}
		}
		/// <summary>
		/// Gets all objects of given Entity.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<T> GetAll<T>() where T : Entity
		{
			return from u in Context.CreateQuery<T>()
				   select u;
		}

		/// <summary>
		/// Gets all objects of given Entity.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<T> FindAll<T>(Func<T, bool> expression) where T : Entity
		{
			var res = from u in Context.CreateQuery<T>()
					  select u;
			return res.Where(expression);
		}

		/// <summary>
		/// Get first entity for given Id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>FirstOrDefault</returns>
		public T GetById<T>(Guid id) where T : Entity
		{
			var user = from u in Context.CreateQuery<T>()
					   where u.Id == id
					   select u;

			return user.FirstOrDefault();
		}

		/// <summary>
		/// Create entity in CRM
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public Guid Create<T>(T entity) where T : Entity
		{
			return this.ServiceProxy.Create(entity);
		}

		/// <summary>
		/// Delete entity in CRM
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public void Delete<T>(T entity) where T : Entity
		{
			this.ServiceProxy.Delete(entity.LogicalName, entity.Id);
		}

		/// <summary>
		/// Update entity in CRM.
		/// </summary>
		/// <param name="entity"></param>
		public void Update<T>(T entity) where T : Entity
		{
			this.ServiceProxy.Update(entity);
		}
	}
}
