﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AtlasDiligence.Common.ServiceDelegate
{
    public class WebUserServiceDelegate : ServiceDelegateBase
    {
        public IEnumerable<new_webuser> GetByGroupId(Guid id)
        {
                return from user in this.Context.CreateQuery<new_webuser>()
                       join grp in this.Context.CreateQuery<new_group>() on user.new_groupid.Id equals grp.Id
                            where grp.new_groupId == id
                            select user;
        }

        /// <summary>
        /// Returns webuser only if there is one entity. There should not be duplicates in the CRM.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public new_webuser GetByEmail(string email)
        {
            var user = from u in this.Context.CreateQuery<new_webuser>()
                           where u.new_email == email
                           select u;
                return user.ToList().Count() > 1 ? null : user.SingleOrDefault();
        }
    }
}
