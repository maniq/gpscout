﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Model
{
	public class ChartData
	{
		public string OrganizationName { get; set; }

		public string FundName { get; set; }

		public string TVPI { get; set; }

		public string NetIRR { get; set; }

		public int VintageYear { get; set; }

		public string FundSize { get; set; }

		public string AsOf { get; set; }

		public string DPI { get; set; }

		public string MOIC { get; set; }

		public string GrossIRR { get; set; }

		public string Source { get; set; }

        public string AsOfDate { get; set; }
	}
}
