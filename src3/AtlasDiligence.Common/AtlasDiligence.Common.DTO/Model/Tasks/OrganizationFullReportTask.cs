﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Model.Tasks
{
    public class OrganizationFullReportTask : DownloadableItemGenerationTask
    {
        public OrganizationFullReportTask()
        {
            this.Status = DownloadableItemLifeCycle.Started;
            this.Name = "Organization Full Report";
        }

        public OrganizationFullReportTask(string data)
        {
            base.DeserializeData(data);
        }
    }
}
