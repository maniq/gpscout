﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Model.Tasks
{
    public class TaskBase : ITask
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string ReferenceId { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public int Status { get; set; }

        public TaskBase()
        {
            DateCreated = DateTime.Now;
        }

        public virtual string SerializeData()
        {
            return String.Empty;
        }

        public virtual void DeserializeData(string data)
        {}

        public virtual bool IsCompleted()
        {
            return true;
        }
    }
}
