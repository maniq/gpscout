﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Model.Tasks
{
    public interface ITask
    {
        Guid Id { get; set; }
        string Name { get; set; }
        int Status { get; set; }
        string Data { get; set; }
        string ReferenceId { get; set; }
        Guid UserId { get; set; }
        DateTime DateCreated { get; set; }

        string SerializeData();
        void DeserializeData(string data);
        bool IsCompleted();
    }
}
