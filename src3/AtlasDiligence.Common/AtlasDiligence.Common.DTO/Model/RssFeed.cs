﻿using System;

namespace AtlasDiligence.Common.DTO.Model
{
    public class RssFeed : ServiceModel
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string Content { get; set; }
        public Guid? OrganizationSourceId { get; set; }
        public string OrganizationSourceName { get; set; }
    }
}
