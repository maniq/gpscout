﻿using System;

namespace AtlasDiligence.Common.DTO.Model
{
    public class FirmNote : ServiceModel
    {
        public string Description { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
    }
}
