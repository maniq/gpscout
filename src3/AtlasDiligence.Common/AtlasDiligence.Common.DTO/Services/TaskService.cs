﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Common.DTO.Model.Tasks;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using System;

namespace AtlasDiligence.Common.DTO.Services
{
    public class TaskService : ITaskService
    {
        ITaskRepository TaskRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ITaskRepository>();
            }
        }


        public bool Save(ITask task)
        {
            return this.TaskRepository.Save(ToModel(task));
        }

        public bool Delete(ITask task)
        {
            if (task != null)
            {
                var _task = this.TaskRepository.GetById(task.Id);
                if (_task != null)
                {
                    this.TaskRepository.DeleteOnSubmit(_task);
                    this.TaskRepository.SubmitChanges();
                }
            }
            return true;
        }


        #region Helper Methods
        private ITask TaskFactory(TaskEntity task)
        {
            ITask _task = null;
            if (task != null)
            {
               switch (task.TaskType) {
                   case TaskType.OrganizationFullReportTask:
                        _task = new OrganizationFullReportTask(task.Data);
                        break;
                }
               _task.Id = task.Id;
               _task.UserId = task.UserId;
               _task.Data = task.Data;
               _task.Name = task.Name;
               _task.ReferenceId = task.ReferenceId;
               _task.Status = task.StatusId;
            }
            return _task;
        }

        private TaskEntity ToModel(ITask task)
        {
            TaskEntity _task = null;
            if (task != null)
            {
                _task = new TaskEntity()
                {
                    DateCreated = task.DateCreated,
                    Name = task.Name,
                    ReferenceId = task.ReferenceId,
                    StatusId = task.Status,
                    Data = task.SerializeData(),
                    Id = task.Id,
                    UserId = task.UserId,
                };
                if (task is OrganizationFullReportTask)
                    _task.TaskType = TaskType.OrganizationFullReportTask;
            }
            return _task;
        }
        #endregion


        public ITask GetById(Guid id)
        {
            var task = this.TaskRepository.GetById(id);
            return this.TaskFactory(task);
        }
    }
}
