﻿using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.ServiceDelegate;

namespace AtlasDiligence.Common.DTO.Services
{
    public class RssFeedService : IRssFeedService
    {
        private readonly ServiceDelegateBase _serviceDelegate;
        public RssFeedService()
        {
            _serviceDelegate = new ServiceDelegateBase();
        }
        public IList<RssFeed> GetAll()
        {
            var rssFeeds = _serviceDelegate.GetAll<new_rssfeed>();
            var result = new List<RssFeed>();
            result.AddRange(CrmEntityMapper.RssFeedsFromCrmEntities(rssFeeds));
            return result;
        }
    }
}
