﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface IWebUserService
    {
        Guid Create(WebUser webUser);
        void Update(WebUser webUser);
        IList<WebUser> GetAll();
        WebUser GetById(Guid id);
        IList<WebUser> GetByGroupId(Guid id);

        /// <summary>
        /// Returns a count for number of users in a given group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        int GetActiveUsers(Guid groupId);

        /// <summary>
        /// Returns a mapped WebUser from Crm.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        WebUser GetByEmail(string email);
    }
}
