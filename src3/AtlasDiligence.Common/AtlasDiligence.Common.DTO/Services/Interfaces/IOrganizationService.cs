﻿using System;
using System.Collections.Generic;
//using AtlasDiligence.Common.DTO.Model;
using System.Linq;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface IOrganizationService
    {
        IQueryable<AtlasDiligence.Common.Data.Models.Organization> GetOrganizationsByName(string organizationName);
        IList<AtlasDiligence.Common.Data.Models.Organization> GetAllPublished(bool queryMasterRecord = false);
        IList<AtlasDiligence.Common.Data.Models.Organization> GetAll(bool queryMasterRecord = false, bool getUnpublishedOrgs = false);

        IList<ResearchPriorityOrganization> GetAllResearchPriorityOrganizations(bool queryMasterRecord = false);
        IEnumerable<Data.Models.Organization> GetAllPositions();
        AtlasDiligence.Common.Data.Models.Organization GetById(Guid id, bool queryMasterRecord = false);
        IEnumerable<Data.Models.Organization> GetAllByIds(IEnumerable<Guid> ids);
		
        void AddOrganizationViewing(Guid userId, Guid organizationId);
        IEnumerable<OrganizationViewing> GetOrganizationViewings(Guid userId, int skip, int take);
        IEnumerable<Fund> GetFundsByOrganizationId(Guid id);
	    IEnumerable<int> GetAvailbleFundVintageYears(IEnumerable<Guid> orgIds);
        IEnumerable<OrganizationMember> GetAllOrganizationMembers();
        IEnumerable<OrganizationMember> GetOrganizationMembersByOrganizationId(Guid id);
        IEnumerable<DueDiligenceItem> GetAllDueDiligenceItems();
        IEnumerable<OtherAddress> GetAllOtherAddresses();
        IEnumerable<TrackRecord> GetAllTrackRecords();
        IEnumerable<Evaluation> GetAllEvaluations();
        IEnumerable<AdvisoryBoard> GetAllAdvisoryBoards();
        IEnumerable<PortfolioCompany> GetAllPortfolioCompanies();
        IEnumerable<BoardSeat> GetAllBoardSeats();
        IEnumerable<Currency> GetAllCurrencies();
        IEnumerable<EmploymentHistory> GetAllEmploymentHistories();
        IEnumerable<EducationHistory> GetAllEducationHistories();
        IEnumerable<LimitedPartner> GetLimitedPartnersByFundId(Guid id);
        IEnumerable<ClientRequest> GetAllClientRequests(bool queryMasterRecord = false);
        IQueryable<ClientRequest> GetClientRequests();
        IDictionary<ClientRequest, Data.Models.Organization> GetClientRequestAndAssociatedOrganization(Guid groupId);

		Dictionary<Guid, string> GetComparTableOrgsByIds(IEnumerable<Guid> ids);
        IEnumerable<Benchmark> GetBenchmarks();
	    IEnumerable<Data.Models.Organization> GetComparetablesOrganizationFunds(IEnumerable<Guid> orgIds);
        IEnumerable<Data.Models.Benchmark> ImportBenchmarks();
    }
}
