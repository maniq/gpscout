﻿using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface IRssFeedService
    {
        IList<RssFeed> GetAll();
    }
}
