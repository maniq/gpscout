﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface INewsItemService
    {
        Guid Create(NewsItem newsItem);
        IEnumerable<NewsItem> GetByOrganizationId(Guid id);
    }
}
