﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.ServiceDelegate;

namespace AtlasDiligence.Common.DTO.Services
{
    public class NewsItemService : INewsItemService
    {
        private readonly NewsItemServiceDelegate _serviceDelegate;
        public NewsItemService()
        {
            _serviceDelegate = new NewsItemServiceDelegate();
        }
        public NewsItem GetById(Guid id)
        {
            return CrmEntityMapper.NewsItemFromCrmEntity(_serviceDelegate.GetById<new_newsitem>(id));
        }
        public IEnumerable<NewsItem> GetAll()
        {
            var result = new List<NewsItem>();
            var crmEntities = _serviceDelegate.GetAll<new_newsitem>();
            result.AddRange(CrmEntityMapper.NewsItemsFromCrmEntities(crmEntities));
            return result;
        } 
        public Guid Create(NewsItem newsItem)
        {
            var result = _serviceDelegate.Create(CrmEntityMapper.CrmEntityFromNewsItem(newsItem));
            foreach (var orgId in newsItem.OrganizationIds)
            {
                _serviceDelegate.AssociateToOrganization(result, orgId);
            }
            foreach (var conId in newsItem.ContactIds)
            {
                _serviceDelegate.AssociateToContact(result, conId);
            }
            return result;
        }

        public IEnumerable<NewsItem> GetByOrganizationId(Guid id)
        {
            var result = new List<NewsItem>();
            var crmEntities = _serviceDelegate.GetByOrganizationId(id);
            result.AddRange(CrmEntityMapper.NewsItemsFromCrmEntities(crmEntities));
            return result;
        }

    }
}
