﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.ServiceDelegate;

namespace AtlasDiligence.Common.DTO.Services
{
    public class WebUserService : IWebUserService
    {
        public IList<WebUser> GetAll()
        {
            var users = new WebUserServiceDelegate().GetAll<new_webuser>();
            var result = new List<WebUser>();
            result.AddRange(CrmEntityMapper.WebUsersFromCrmEntities(users));
            return result;
        }

        public WebUser GetById(Guid id)
        {
            var user = new WebUserServiceDelegate().GetById<new_webuser>(id);
            if (user == null) return null;

            new_group group = null;
            if (user.new_groupid != null)
                group = new GroupServiceDelegate().GetById<new_group>(user.new_groupid.Id);
            return CrmEntityMapper.WebUserFromCrmEntity(user, group);
        }

        public Guid Create(WebUser webUser)
        {
            return new WebUserServiceDelegate().Create(CrmEntityMapper.CrmEntityFromWebUser(webUser));
        }

        public void Update(WebUser webUser)
        {
            //TODO: log failures
            new WebUserServiceDelegate().Update(CrmEntityMapper.CrmEntityFromWebUser(webUser));
        }

        public IList<WebUser> GetByGroupId(Guid id)
        {
            var group = new GroupServiceDelegate().GetById<new_group>(id);
            if (group == null) return null;
            return
                new WebUserServiceDelegate().GetByGroupId(id).Select(x => CrmEntityMapper.WebUserFromCrmEntity(x, group))
                    .ToList();
        }

        public int GetActiveUsers(Guid groupId)
        {
            return new WebUserServiceDelegate().GetByGroupId(groupId).Where(x => x.new_validated == true).Count();
        }

        /// <summary>
        /// Returns a mapped WebUSer from Crm.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public WebUser GetByEmail(string email)
        {
            var user = new WebUserServiceDelegate().GetByEmail(email);
            if (user == null) return null;

            new_group group = null;
            if (user.new_groupid != null)
                group = new GroupServiceDelegate().GetById<new_group>(user.new_groupid.Id);
            return CrmEntityMapper.WebUserFromCrmEntity(user, group);
        }

        public void Delete(Guid crmId)
        {
            var serviceDelegate = new WebUserServiceDelegate();
            var user = serviceDelegate.GetById<new_webuser>(crmId);
            serviceDelegate.Delete(user);
        }

    }
}
