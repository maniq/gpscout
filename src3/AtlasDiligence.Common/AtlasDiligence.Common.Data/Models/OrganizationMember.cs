﻿using System.Collections.Generic;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class OrganizationMember
    {
        public IEnumerable<string> Aliases
        {
            get { return this.AliasesPipeDelimited.Split(new[] {'|'}); }
        } 
        public string FullName
        {
            get { return this.FirstName + " " + this.LastName; }
        }
    }
}
