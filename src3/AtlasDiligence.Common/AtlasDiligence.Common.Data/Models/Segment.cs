﻿using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Common.Data.Models
{

    public partial class Segment
    {
        public Dictionary<FieldType, List<string>> Filters
        {
            get { return this.SegmentFilters.GroupBy(m => (FieldType)m.Field).ToDictionary(m => m.Key, m => m.Select(v => v.Value).ToList()); }
        }
    }
}
