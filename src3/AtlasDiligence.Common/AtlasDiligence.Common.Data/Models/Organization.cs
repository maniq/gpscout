﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Common.Data.Models
{
	public partial class Organization
	{
		public int DiligenceLevelFromPublishFields
		{
			get
			{
				var retval = 1;

				if (this.PublishOverviewAndTeam)
				{
					switch (this.EvaluationLevel)
					{
						case "Evaluation":
							retval = 10;
							break;
						case "Preliminary Evaluation":
							retval = 7;
							break;
						case "Initial Assessment":
							retval = 4;
							break;
						case "Limited Information":
							retval = 2;
							break;
					}
				}

				return retval;
			}
		}

		public IEnumerable<string> Strategy
		{
			get { return string.IsNullOrWhiteSpace(this.StrategyPipeDelimited) ? new string[0] : this.StrategyPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> SubStrategy
		{
			get { return string.IsNullOrWhiteSpace(this.SubStrategyPipeDelimited) ? new string[0] : this.SubStrategyPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> SectorFocus
		{
			get { return string.IsNullOrWhiteSpace(this.SectorFocusPipeDelimited) ? new string[0] : this.SectorFocusPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> SubSectorFocus
		{
			get { return string.IsNullOrWhiteSpace(this.SubSectorFocusPipeDelimited) ? new string[0] : this.SubSectorFocusPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> Aliases
		{
			get { return string.IsNullOrWhiteSpace(this.AliasesPipeDelimited) ? new string[0] : this.AliasesPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> Countries
		{
			get { return string.IsNullOrWhiteSpace(this.CountriesPipeDelimited) ? new string[0] : this.CountriesPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> SubRegions
		{
			get { return string.IsNullOrWhiteSpace(this.SubRegionsPipeDelimited) ? new string[0] : this.SubRegionsPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<string> InvestmentRegions
		{
			get { return string.IsNullOrWhiteSpace(this.InvestmentRegionPipeDelimited) ? new string[0] : this.InvestmentRegionPipeDelimited.Split(new[] { '|' }); }
		}

		public IEnumerable<Segment> FindMatchingSegments(IList<Segment> availableSegments)
		{
			var retval = new List<Segment>();
			if (availableSegments == null) return retval;
			foreach (var segment in availableSegments)
			{
				var match = true;
				var strategies = segment.Filters.FirstOrDefault(m => m.Key == FieldType.Strategy);
				var marketStages = segment.Filters.FirstOrDefault(m => m.Key == FieldType.MarketStage);
				var investmentRegions = segment.Filters.FirstOrDefault(m => m.Key == FieldType.InvestmentRegion);
				var countries = segment.Filters.FirstOrDefault(m => m.Key == FieldType.Country);
				if (strategies.Value != null && strategies.Value.Any())
				{
					match = match && strategies.Value.Intersect(this.Strategy).Any();
				}
				if (marketStages.Value != null && marketStages.Value.Any())
				{
					match = match && marketStages.Value.Any(m => m == this.MarketStage);
				}
				if (investmentRegions.Value != null && investmentRegions.Value.Any())
				{
					match = match && investmentRegions.Value.Intersect(this.InvestmentRegions).Any();
				}
				if (countries.Value != null && countries.Value.Any())
				{
					match = match && countries.Value.Intersect(this.Countries).Any();
				}
				if (match)
				{
					retval.Add(segment);
				}
			}
			return retval;
		}

		public decimal? QualitativeGradeStars
		{
			get
			{
				if (!this.QualitativeGradeNumber.HasValue) return new decimal?();
				// not the most elegant, but the easiest to maintain over a formula.  So sue me.
				var qualityNum = QualitativeGradeNumber.Value;
				var stars = 0m;
				if (qualityNum < 20)
				{
					stars = 1;
				}
				else if (qualityNum >= 20 && qualityNum < 30)
				{
					stars = 1.5m;
				}
				else if (qualityNum >= 30 && qualityNum < 40)
				{
					stars = 2m;
				}
				else if (qualityNum >= 40 && qualityNum < 50)
				{
					stars = 2.5m;
				}
				else if (qualityNum >= 50 && qualityNum < 60)
				{
					stars = 3m;
				}
				else if (qualityNum >= 60 && qualityNum < 70)
				{
					stars = 3.5m;
				}
				else if (qualityNum >= 70 && qualityNum < 80)
				{
					stars = 4m;
				}
				else if (qualityNum >= 80 && qualityNum < 90)
				{
					stars = 4.5m;
				}
				else if (qualityNum >= 90 && qualityNum <= 100)
				{
					stars = 5m;
				}
				return stars;
			}
		}

		public decimal? QuantitativeGradeStars
		{
			get
			{
				if (!this.QuantitativeGradeNumber.HasValue) return new decimal?();
				// not the most elegant, but the easiest to maintain over a formula.  So sue me.
				var quantityNum = QuantitativeGradeNumber.Value;
				var quantityStars = 0m;
				if (quantityNum < 20)
				{
					quantityStars = 1;
				}
				else if (quantityNum >= 20 && quantityNum < 30)
				{
					quantityStars = 1.5m;
				}
				else if (quantityNum >= 30 && quantityNum < 40)
				{
					quantityStars = 2m;
				}
				else if (quantityNum >= 40 && quantityNum < 50)
				{
					quantityStars = 2.5m;
				}
				else if (quantityNum >= 50 && quantityNum < 60)
				{
					quantityStars = 3m;
				}
				else if (quantityNum >= 60 && quantityNum < 70)
				{
					quantityStars = 3.5m;
				}
				else if (quantityNum >= 70 && quantityNum < 80)
				{
					quantityStars = 4m;
				}
				else if (quantityNum >= 80 && quantityNum < 90)
				{
					quantityStars = 4.5m;
				}
				else if (quantityNum >= 90 && quantityNum <= 100)
				{
					quantityStars = 5m;
				}
				return quantityStars;
			}
		}

		public decimal? AdvancedSearchFundSize
		{
			get
			{
				if (String.IsNullOrEmpty(FundRaisingStatus) || FundRaisingStatus == "Closed")
				{
					if (LastFundSize.HasValue)
					{
						return LastFundSize;
					}
				}

				var mostRecentFund = Funds.OrderByDescending(o => o.VintageYear);
				if (mostRecentFund.Any())
				{
					return mostRecentFund.FirstOrDefault().FundSize;
				}
				
				return null;
			}
		}
	}
}
