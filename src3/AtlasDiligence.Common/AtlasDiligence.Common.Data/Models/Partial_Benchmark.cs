﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class Benchmark
    {
        public string AsOfDate {
            get { return this.AsOf.HasValue ? this.AsOf.Value.ToString("M/dd/yyyy") : ""; }
        }
    }
}
