﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public static class AppSettings
    {
        public static string TempPath
        {
            get
            {
                return ConfigurationManager.AppSettings["TempPath"];
            }
        }

        public static string PdfGeneratorPath
        {
            get
            {
                return ConfigurationManager.AppSettings["PdfGeneratorPath"];
            }
        }

        public static int PdfWatermarkSizeEm
        {
            get
            {
                string emSizeStr = ConfigurationManager.AppSettings["PdfWatermarkSizeEm"];
                int size = 30;
                int.TryParse(emSizeStr, out size);
                if (size < 1) size = 1;
                return size;
            }
        }

        public static string PrintAllAdditionalCmdLineOptions
        {
            get
            {
                return ConfigurationManager.AppSettings["PrintAllAdditionalCmdLineOptions"];
            }
        }

        public static bool SkipOrganizationServiceProxyInstantiation
        {
            get
            {
                return (ConfigurationManager.AppSettings["SkipOrganizationServiceProxyInstantiation"] ?? String.Empty).ToLower() == "true";
            }
        }

        public static string LongestWatermarkText
        {
            get
            {
                return ConfigurationManager.AppSettings["LongestWatermarkText"];
            }
        }

        public static int UserNotificationVisibilityMS {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationVisibilityMS"], out i))
                    return Math.Max(i, 0);

                return 5000;
            }
        }

        public static int UserNotificationCheckIntervalMS
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationCheckIntervalMS"], out i))
                    return Math.Max(i, 5000);

                return 5000;
            }
        }

        public static int CleanupIntervalDays
        {
            get
            {
                int i;
                if (!int.TryParse(ConfigurationManager.AppSettings["CleanupIntervalDays"], out i))
                    i = 30;
                return Math.Max(i, 1);
            }
        }

        public static double PdfZoom
        {
            get
            {
                double d;
                if (double.TryParse(ConfigurationManager.AppSettings["PdfZoom"], out d))
                    return Math.Max(d, 0.1);

                return 1.0;
            }
        }
        public static ulong PdfGenerationJSDelayMS
        {
            get
            {
                ulong i;
                if (ulong.TryParse(ConfigurationManager.AppSettings["PdfGenerationJSDelayMS"], out i))
                    return Math.Max(i, 0);

                return 1000;
            }
        }

        public static int PdfGenerationTimeoutMS
        {
            get
            {
                int i;
                    if (int.TryParse(ConfigurationManager.AppSettings["PdfGenerationTimeoutSec"], out i))
                        return Math.Max(i, 9) * 1000;

                return 6000000; // 100 minutes
            }
        }

        public static int UserNotificationMaxPollingSec
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationMaxPollingSec"], out i))
                    return Math.Max(i, 0) * 1000;

                return 60000; // 1 minute
            }
        }

        public static string MailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings["MailFrom"];
            }
        }

        public static string ContactToMail
        {
            get
            {
                return ConfigurationManager.AppSettings["ContactToMail"];
            }
        }

        public static string ContactMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["ContactMailSubject"];
            }
        }

        public static string NewsMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["NewsMailSubject"];
            }
        }
    }
}
