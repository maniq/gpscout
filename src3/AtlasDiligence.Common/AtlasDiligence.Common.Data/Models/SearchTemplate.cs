﻿using System.Collections.Generic;
using System.Linq;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class SearchTemplate
    {
        Dictionary<string, string> _SearchFields = null;

        public Dictionary<string, string> SearchFields
        {
            get
            {
                if (_SearchFields != null)
                {
                    return _SearchFields;
                }

                _SearchFields = new Dictionary<string, string>();
                if (SearchTemplateFilters.Any() && !(SearchTemplateFilters.Count() == 1 && SearchTemplateFilters.Any(m => m.Field == (int)FieldType.ShowOnlyPublished && m.Value == false.ToString())))
                {
                    if (!this.KeywordField.IsNullOrWhiteSpace())
                    {
                        _SearchFields.Add("Search", this.KeywordField);
                    }
                    foreach (var group in this.SearchTemplateFilters.GroupBy(m => m.Field).OrderBy(m => ((FieldType)m.Key).ToString()))
                    {
                        if (group.Key == (int)FieldType.ShowOnlyPublished && group.Select(m => m.Value).FirstOrDefault() == false.ToString())
                        {
                            continue;
                        }
                        if (group.Key == (int)FieldType.ShowOnlyPublished && group.Select(m => m.Value).FirstOrDefault() == true.ToString())
                        {
                            _SearchFields.Add(((FieldType)group.Key).GetDescription(), "Yes");
                        }
                        else
                        {
                            _SearchFields.Add(((FieldType)group.Key).GetDescription(),
                                       string.Join(", ", group.Select(m => m.Value)));
                        }
                    }

                    ScrubSearchField();

                    return _SearchFields;
                }

                _SearchFields.Add("Search", this.KeywordField.IsNullOrWhiteSpace() ? "Everything" : this.KeywordField);

                ScrubSearchField();

                return _SearchFields;
            }
        }

        public void ScrubSearchField()
        {
            if (_SearchFields.Keys.Any(a => a.Equals("Expected Next Fundraise") || a.Equals("Qualitative Grade") || a.Equals("Quantitative Grade")))
            {
                _SearchFields.Remove("Expected Next Fundraise Start Date");
                _SearchFields.Remove("Expected Next Fundraise End Date");

                UpdateValueText();
            }
        }

        private void UpdateValueText()
        {
            for (int i = 0; i < _SearchFields.Count; i++)
            {

                var item = _SearchFields.ElementAt(i);
                var key = item.Key;
                var value = item.Value;

                if (new [] {"Qualitative Grade", "Quantitative Grade"}.Contains(key) && (value.Contains("Poor") || value == "1"))
                {
                    _SearchFields[key] = "Poor or higher";
                }
                else if (new [] {"Qualitative Grade", "Quantitative Grade"}.Contains(key) && (value.Contains("Weak") || value == "2"))
                {
                    _SearchFields[key] = "Weak or higher";
                }
                else if (new[] { "Qualitative Grade", "Quantitative Grade" }.Contains(key) && (value.Contains("Standard") || value == "3"))
                {
                    _SearchFields[key] = "Standard or higher";
                }
                else if (new[] { "Qualitative Grade", "Quantitative Grade" }.Contains(key) && (value.Contains("Strong") || value == "4"))
                {
                    _SearchFields[key] = "Strong or higher";
                }
                else if (new[] { "Qualitative Grade", "Quantitative Grade" }.Contains(key) && (value.Contains("Exceptional") || value == "5"))
                {
                    _SearchFields[key] = "Exceptional or higher";
                }
                else if (key.Equals("Expected Next Fundraise")) {
                    _SearchFields[key] = string.Format("{0} months", value);
                }
            }
        }
    }
}
