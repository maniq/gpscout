﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Common.Data.Models
{
    public interface IAuditable
    {
        Guid ModifiedBy { get; set; }
        DateTime Modified { get; set; }
    }
}
