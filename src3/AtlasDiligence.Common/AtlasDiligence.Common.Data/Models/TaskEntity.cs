﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class TaskEntity
    {
        public TaskType TaskType
        {
            get
            {
                return (TaskType)this.TaskTypeId;
            }
            set
            {
                this.TaskTypeId = (int)value;
            }
        }
    }
}
