﻿// -----------------------------------------------------------------------
// <copyright file="EulaRepository.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;
using System.Linq.Dynamic;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class EulaRepository : Repository<Eula>, IEulaRepository
    {
        public void InsertEula(Eula eula)
        {
            this.DeleteAllOnSubmit(this.GetAll());
            this.InsertOnSubmit(eula);
            this.DataContext.WipeEulaDates();
            this.SubmitChanges();
        }

        public Eula GetCurrentEula()
        {
            return
                this.GetAll().SingleOrDefault();
        }

        public bool HasUserSignedCurrentEula(string username)
        {
            var user = this.DataContext.aspnet_Users.FirstOrDefault(m => m.UserName == username);
            if (user == null || user.UserExt == null)
            {
                return false;
            }
            var currentEula = this.GetCurrentEula();
            if (currentEula == null)
            {
                return true;
            }
            return user.UserExt.EulaSigned.HasValue;
        }

        public void UserSignedEula(string username)
        {
            var user = this.DataContext.aspnet_Users.FirstOrDefault(m => m.UserName == username);
            user.UserExt.EulaSigned = DateTime.Now;
            user.UserEulaAudits.Add(new UserEulaAudit { Id = Guid.NewGuid(), AgreedOn = DateTime.Now });
            this.SubmitChanges();
        }
    }
}
