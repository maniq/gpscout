﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IRequestRepository : IRepository<OrganizationRequest>
    {
        void InsertRequest(Guid organizationId, Guid userId, string comment, RequestType type);
        void InsertRequest(string organizationName, Guid userId, string comment, RequestType type);
        void Edit(OrganizationRequest request);
    }
}
