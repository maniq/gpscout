﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IUserRepository : IRepository<aspnet_User>
    {
        /// <summary>
        /// aspnet tables do not cascade on delete, so they must be individually deleted to avoid FK constraint conflict.
        /// </summary>
        /// <remarks>All non-aspnet Membership model user tables (UserDetails, etc) cascade on delete of aspnet_Users row.</remarks>
        /// <param name="dbUser"></param>
        void DeleteUser(aspnet_User dbUser);

        /// <summary>
        /// Update DateTime offset for Rss Email for user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="date"></param>
        void UpdateUserEmailDate(Guid userId, DateTime date);

        /// <summary>
        /// Finds all aspnet_user with userext table pre-populated
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        IEnumerable<aspnet_User> FindAllEager(Expression<Func<aspnet_User, bool>> expression);

        /// <summary>
        /// Get aspnet_user from Crm Id.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        aspnet_User GetUserByCrmId(Guid guid);

        /// <summary>
        /// Get user from email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        aspnet_User GetByEmail(string email);

        bool HasDuplicate(Guid userId, string userName);

        /// <summary>
        /// Get active users for given group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<aspnet_User> GetActiveUsers(Guid groupId);

        /// <summary>
        /// Replace all extra email addresses with emails param
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="emails"></param>
        void UpdateRssEmails(Guid userId, IEnumerable<string> emails);

        /// <summary>
        /// Remove extra email for user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="email"></param>
        void RemoveRssEmail(Guid userId, string email);

        /// <summary>
        /// Gets the recently viewed organizations in order
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="take"></param>
        IEnumerable<RecentlyViewedOrganizationsResult> GetRecentlyViewedOrganizations(Guid userId, int take);

        /// <summary>
        /// Gets a group by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Group GetGroupById(Guid id);

		void InsertUserLogin(Guid userId);

	    void InsertUserLogout(Guid userId);

	    void InsertUserProfileView(Guid userId, Guid orgId);

	    IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate);
    }
}
