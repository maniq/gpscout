﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface ISegmentRepository : IRepository<Segment>
    {
        IEnumerable<string> GetDistinctOptionsByName(General.FieldType type);
        IEnumerable<SearchOption> GetDistinctOptions();
        void Insert<T>(T item) where T : class;
        void InsertAll<T>(IEnumerable<T> items) where T : class;
        IEnumerable<Segment> GetSegmentByGroupId(Guid groupId);
        Segment GetSegmentById(Guid id);

        IEnumerable<Segment> GetSegmentsStartsWith(string term);
    }
}
