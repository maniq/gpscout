﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface ISearchRepository : IRepository<Organization>
    {
        IEnumerable<string> GetDistinctOptionsByName(General.FieldType type);
        IEnumerable<SearchOption> GetDistinctOptions();
        void Insert<T>(T item) where T : class;
        void InsertAll<T>(IEnumerable<T> items) where T : class;
        IEnumerable<SearchTemplate> GetSearchTemplatesByUserId(Guid userId);
        SearchTemplate GetSearchTemplateById(Guid userId, Guid id);
        void InsertSavedSearch(SavedSearch savedSearch);
        IQueryable<SavedSearch> GetSavedSearches(Guid userId);
        SavedSearch GetSavedSearch(Guid searchTemplateId);
        void DeleteSavedSearch(Guid searchTemplateId);
    }
}
