﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface INoteRepository : IRepository<OrganizationNote>
    {
        IEnumerable<OrganizationNote> GetByUserIdAndOrganizationId(Guid userId, Guid OrganizationId);
        void AddNote(OrganizationNote note);
        IQueryable<OrganizationNote> GetAllUserNotes(Guid userId);
    }
}
