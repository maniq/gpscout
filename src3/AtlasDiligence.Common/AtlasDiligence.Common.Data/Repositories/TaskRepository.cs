﻿using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class TaskRepository : Repository<TaskEntity>, ITaskRepository
    {
        public bool Save(TaskEntity task)
        {
            TaskEntity oldEntity = base.DataContext.TaskEntities.Where(x => x.Id == task.Id).FirstOrDefault();
            if (oldEntity != null)
            {
                base.DeleteOnSubmit(oldEntity);
            }

            base.InsertOnSubmit(task);
            base.SubmitChanges();
            return true;
        }
    }
}
