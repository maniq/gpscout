﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IClientRequestRepository : IRepository<ClientRequest>
    {
        IDictionary<ClientRequest, Organization> GetAllClientsAndAssociatedOrganizations(Guid groupId);
    }
    public class ClientRequestRepository : Repository<ClientRequest>, IClientRequestRepository
    {
        public IDictionary<ClientRequest, Organization> GetAllClientsAndAssociatedOrganizations(Guid groupId)
        {
            return (from cr in this.DataContext.ClientRequests
                    join o in this.DataContext.Organizations on cr.OrganizationId equals o.Id into orgs
                    from o in orgs.DefaultIfEmpty()
                    where cr.GroupId == groupId
                    select new
                        {
                            ClientRequest = cr,
                            Organization = o
                        }).ToDictionary(m => m.ClientRequest, m => m.Organization);
        }
    }
}
