﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class GroupRepository : Repository<Group>,  IGroupRepository
    {
        #region IGroupRepository Members

        public IEnumerable<aspnet_User> GetActiveUsersByGroupId(Guid id)
        {
            return DataContext.aspnet_Users.Where(x => x.aspnet_Membership.IsApproved && x.UserExt.GroupId == id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Group>  GetGroupsByGroupLeaderId(Guid userId)
        {
            return DataContext.Groups.Where(x => x.GroupLeaderId == userId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationIds"></param>
        /// <returns></returns>
        public IEnumerable<Organization> GetGroupOrganizations(List<Guid> organizationIds)
        {
            return DataContext.Organizations.Where(m => organizationIds.Contains(m.Id));
        }

        public void AddGroupOrganization(Guid groupId, Guid organizationId)
        {
            var groupOrganization = new GroupOrganization
                                        {
                                            GroupId = groupId,
                                            OrganizationId = organizationId
                                        };
            DataContext.GroupOrganizations.InsertOnSubmit(groupOrganization);
            DataContext.SubmitChanges();
        }

        public void RemoveGroupOrganization(Guid groupId, Guid organizationId)
        {
            DataContext.GroupOrganizations.DeleteAllOnSubmit(DataContext.GroupOrganizations.Where(m => m.GroupId == groupId && m.OrganizationId == organizationId));
            DataContext.SubmitChanges();
        }

        public void AddGroupSegment(Guid groupId, Guid segmentId)
        {
            var groupSegment = new GroupSegment
            {
                GroupId = groupId,
                SegmentId = segmentId
            };
            DataContext.GroupSegments.InsertOnSubmit(groupSegment);
            DataContext.SubmitChanges();
        }

        public void RemoveGroupSegment(Guid groupId, Guid segmentId)
        {
            DataContext.GroupSegments.DeleteAllOnSubmit(DataContext.GroupSegments.Where(x => x.GroupId == groupId && x.SegmentId == segmentId));
            DataContext.SubmitChanges();
        }

        public IEnumerable<Segment> GetSegmentsByName(string segmentName)
        {
            return DataContext.Segments.Where(x => x.Name == segmentName);
        }

        #endregion
    }
}