﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Common.Data.General
{
	/// <summary>
	/// For database audit table
	/// </summary>
	public enum AuditType
	{
		Insert,
		Update,
		Delete
	}

	public enum NewsEntityType
	{
		Organization = 1,
		Contact
	}

	/// <summary>
	/// Used in RssEmailFrequency
	/// </summary>
	public enum EmailFrequency
	{
		[Description("day")]
		Day = 1,
		[Description("week")]
		Week = 2,
		[Description("month")]
		Month = 3
	}

	/// <summary>
	/// 
	/// </summary>
	public enum EntityType
	{
		Organization = 1,
		Fund
	}

	public enum RequestType
	{
		Queue, Priority, Diligence, Introduction
	}

	public enum Grades
	{
		Poor = 1,
		Weak,
		Standard,
		Strong,
		Exceptional
	}

	/// <summary>
	/// Field Type maps to field name. For referencing fields in db lookup table
	/// </summary>
	public enum FieldType
	{
		[Description("Sector Focus")]
		SectorFocus = 1,
		Strategy = 2,
		[Description("Geographic Focus")]
		GeographicFocus = 3,
		[Description("Market/Stage")]
		MarketStage = 4,
		[Description("Fundraising Status")]
		FundraisingStatus = 5,
		[Description("Fund Size Minimum")]
		FundSizeMin = 6,
		[Description("Fund Size Maximum")]
		FundSizeMax = 7,
		[Description("Number of Funds Closed Minimum")]
		NumberOfFundsClosedMin = 8,
		[Description("Number of Funds Closed Maximum")]
		NumberOfFundsClosedMax = 9,
		[Description("Diligence Status Minimum")]
		DiligenceStatusMin = 10,
		[Description("Diligence Status Maximum")]
		DiligenceStatusMax = 11,
		[Description("Emerging Manager")]
		EmergingManager = 12,
		[Description("Focus List")]
		FocusList = 13,
		[Description("Access Constrained")]
		AccessConstrained = 14,
		Search = 15,
		[Description("Investment Region")]
		InvestmentRegion = 16,
		[Description("Investment Sub-Region")]
		SubRegion = 17,
		Country = 18,
		[Description("Expected Next Fundraise Start Date")]
		ExpectedNextFundraiseStartDate = 19,
		[Description("Expected Next Fundraise End Date")]
		ExpectedNextFundraiseEndDate = 20,
		[Description("Limit to Firms with Profiles")]
		ShowOnlyPublished = 21,
		[Description("Sub-Strategy")]
		SubStrategy = 22,
		[Description("Investment Sub-Region")]
		InvestmentSubRegion = 23,
		[Description("Quantitative Grade")]
		QuantitativeGrade = 24,
		[Description("Qualitative Grade")]
		QualitativeGrade = 25,
		[Description("Sector Specialist")]
		SectorSpecialist = 26,
		[Description("Currency")]
		Currency = 27,
		[Description("SBIC Fund")]
		SBICFund = 28,
		[Description("Expected Next Fundraise")]
		SelectedNextFundraiseValue = 29
	}

	public enum TrackUsersType
	{
		[Description("Logins")]
		Login,

		[Description("Logouts")]
		Logout,

		[Description("Profile Views")]
		ProfileVisit
	}

    public enum UserNotificationType : byte
    {
        GenerateOrganizationFullReportPdf = 1
    }

    public enum DownloadableItemLifeCycle : int
    {
        Unknown = 0,
        Started = 1,
        Running = 2,
        GenerationCompleted = 3,
        Downloaded = 4,
        Error = 5
    }

    public enum TaskType : int
    {
        OrganizationFullReportTask = 1
    }
}
