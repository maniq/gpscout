﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.ExcelHelper
{
    public class ExcelInfo
    {
        public string FileName { get; set; }
        public IEnumerable<string> Headers { get; set; }
        public IEnumerable<IDictionary<string, string>> Rows { get; set; }
        public string WorksheetName { get; set; }
    }
}
