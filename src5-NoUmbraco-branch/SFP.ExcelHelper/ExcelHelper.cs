﻿using ExtremeML.Packaging;
using ExtremeML.Spreadsheet.Address;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.ExcelHelper
{
    public class ExcelHelper
    {
        public string CreateExcel(ExcelInfo excelInfo)
        {
            excelInfo.FileName = excelInfo.FileName.Trim();
            if (!excelInfo.FileName.EndsWith(".xlsx"))
            {
                excelInfo.FileName = excelInfo.FileName + ".xlsx";
            }

            string filePath = Path.GetDirectoryName(excelInfo.FileName);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            using (SpreadsheetDocumentWrapper wrapper = SpreadsheetDocumentWrapper.Create(excelInfo.FileName))
            {
                WorksheetPartWrapper wrapper2 = wrapper.WorkbookPart.WorksheetParts.Add(string.IsNullOrEmpty(excelInfo.WorksheetName) ? "Sheet1" : excelInfo.WorksheetName);
                int colNo = 0;
                int rowNo = 0;
                foreach (string header in excelInfo.Headers)
                {
                    wrapper2.Worksheet.SetCellValue(new GridReference(rowNo, colNo), header);
                    colNo++;
                }
                
                rowNo = 1;
                foreach (IDictionary<string, string> dictionary in excelInfo.Rows)
                {
                    colNo = 0;
                    foreach (string header in excelInfo.Headers)
                    {
                        wrapper2.Worksheet.SetCellValue(new GridReference(rowNo, colNo), dictionary[header]);
                        colNo++;
                    }
                    rowNo++;
                }
            }
            return excelInfo.FileName;
        }
    }
}
