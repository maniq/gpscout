﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Infrastructure.Logging.Contracts
{
    public interface ILog
    {
        void Write(string msg);
        void Write(string msg, string category);
        void Write(Exception ex);
    }
}
