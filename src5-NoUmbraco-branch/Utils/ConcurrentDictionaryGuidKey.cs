﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace Utility
{
    public class ConcurrentDictionaryGuidKey<TValue> : ConcurrentDictionary<Guid, TValue>
    {
        public ConcurrentDictionaryGuidKey() { }

        public ConcurrentDictionaryGuidKey(IEnumerable<TValue> values, Func<TValue, Guid> keyExtractor)
            :base(BaseConverter(values, keyExtractor))
        {}

        public new IEnumerator<TValue> GetEnumerator()
        {
            return base.Values.GetEnumerator();
        }

        private static IEnumerable<KeyValuePair<Guid, TValue>> BaseConverter(IEnumerable<TValue> values, Func<TValue, Guid> keyExtractor)
        {
            if (values == null) return null;

            return values.Select(x => new KeyValuePair<Guid, TValue>(keyExtractor(x), x));
        }
    }
}
