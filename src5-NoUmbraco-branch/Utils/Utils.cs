﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;

namespace Utility
{
    public static class Utils
    {
        public static readonly double PIP180 = Math.PI / 180;

        public static void WriteCookieJarFile(string fileName, HttpRequestBase request)
        {
            var sb = new StringBuilder();
            if (request != null && request.Cookies != null)
            {
                foreach (string cookieName in request.Cookies)
                {
                    HttpCookie cookie = request.Cookies[cookieName];
                    sb.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r\n"
                        , cookie.Domain, cookie.HttpOnly.ToString().ToUpper(), cookie.Path, cookie.Secure.ToString().ToUpper()
                        , cookie.Expires.ToUnixTimestamp()
                        , cookie.Name, cookie.Value);
                }
            }
            System.IO.File.WriteAllText(fileName, sb.ToString());
        }


        public static double ToUnixTimestamp(this DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        public static readonly DateTime EpochStartDate = new DateTime(1970, 1, 1);

        public static double GetTicks1970()
        {
            return DateTime.Now.Subtract(EpochStartDate).TotalMilliseconds;
        }

        public static bool DeleteFile(string fileName) {
            try
            {
                System.IO.File.Delete(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static int DeleteFiles(string directoryName, string pattern, DateTime limitDate, out int err) {
            int deleted = 0;
            err = 0;
            foreach (string fileName in Directory.GetFiles(directoryName, pattern))
            {
                DateTime creationTime = File.GetCreationTime(fileName);
                if (creationTime < limitDate)
                {
                    if (Utils.DeleteFile(fileName)) deleted++; else err++;
                }
            }
            return deleted;
        }

        public static string SafeFileName(string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        public static string GetTempFileName(Guid Id)
        {
            return Id.ToString().Replace("{", String.Empty).Replace("}", String.Empty);
        }

        public static bool IsValidEmail(string email)
        {
            return new RegexUtilities().IsValidEmail(email);
        }

        public static string LoadShortFileFromBaseDirectory(string partialFilePath)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, partialFilePath);
            using (var sr = new StreamReader(path))
            {
                return sr.ReadToEnd();
            }
        }

        public static double Radians(double deg)
        {
            return deg * PIP180;
        }

        public static Exception Throw(string format, params object[] args)
        {
            return new Exception(string.Format(format, args));
        }

        public static void RunProcess(string arguments, int timeoutMS, string pathToExe, string workingDirectory, out StringBuilder output, out StringBuilder error)
        {
            using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                using (Process process = new Process())
                {
                    //string path = AppSettings.TempPath;
                    process.StartInfo.WorkingDirectory = workingDirectory;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.FileName = pathToExe;
                    process.StartInfo.Arguments = arguments;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;

                    var _output = new StringBuilder(); ;
                    var _error = new StringBuilder(); ;
                    try
                    {
                        process.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                outputWaitHandle.Set();
                            }
                            else
                            {
                                _output.AppendLine(e.Data);
                            }
                        };
                        process.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                errorWaitHandle.Set();
                            }
                            else
                            {
                                _error.AppendLine(e.Data);
                            }
                        };

                        process.Start();

                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();

                        if (process.WaitForExit(timeoutMS) &&
                            outputWaitHandle.WaitOne(timeoutMS) &&
                            errorWaitHandle.WaitOne(timeoutMS))
                        {
                            // Process completed. Check process.ExitCode here.
                            _output.AppendFormat("\r\nExitcode: {0}", process.ExitCode);
                        }
                        else
                        {
                            _error.AppendLine("Process timed out!");
                        }
                    }
                    catch (Exception ex)
                    {
                        _error.AppendLine(ex.ToString());
                        throw;
                    }
                    finally
                    {
                        output = new StringBuilder(_output.ToString());
                        error = new StringBuilder(_error.ToString());
                        //outputWaitHandle.WaitOne(timeoutMS);
                        //errorWaitHandle.WaitOne(timeoutMS);
                    }

                    if (process.HasExited)
                    {
                        string errStr = error.ToString();
                        //error.AppendLine(errStr);
                        //output.AppendLine(output.ToString());
                        if (process.ExitCode != 0)
                        {
                            if (process.ExitCode != 1 || !errStr.EndsWith("Exit with code 1 due to network error: ProtocolUnknownError\r\n"))
                            {
                                throw new Exception(string.Format("Error: GeneratePDF - ExitCode: {0}, Timeout: {1}, Arguments: {2}. Further details may be logged above this log entry.", process.ExitCode, timeoutMS, arguments));
                            }
                        }
                    }
                }
            }
        }

    }
}
