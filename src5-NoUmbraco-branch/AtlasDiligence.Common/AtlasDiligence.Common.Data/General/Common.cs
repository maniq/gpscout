﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.General
{
   public static class Common
    {
        /// <summary>
        /// Format a cell for comma-delimited.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string CsvFormatCell(this object item)
        {
            string delimiter = ",";
            StringBuilder sb = new StringBuilder();
            if (item == null) item = string.Empty;

            sb.Append('"');
            sb.Append(item.ToString().Replace(@"<[^>]*>", " ").Replace("\"", "\\\"").Replace(",", "\\,"));
            sb.Append('"');
            sb.Append(delimiter);

            return sb.ToString();
        }
    }
}
