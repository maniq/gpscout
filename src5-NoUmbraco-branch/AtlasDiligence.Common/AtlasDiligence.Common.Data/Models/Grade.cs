﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    partial class Grade
    {
        public static string GetGrade(double? value)
        {
            if (value.HasValue)
            {
                int checkMarkNumber = (int)Math.Round(value.Value, 0);
                if (checkMarkNumber >= 5) return "Exceptional";
                if (checkMarkNumber >= 4) return "Strong";
                if (checkMarkNumber >= 3) return "Standard";
                if (checkMarkNumber >= 2) return "Weak";
                return "Poor";
            }
            return String.Empty;
        }

        public string TeamWeighted
        {
            get
            {
                return GetGrade(TeamWeightedGrade);
            }
        }

        public string StrategyWeighted
        {
            get
            {
                return GetGrade(StrategyWeightedGrade);
            }
        }

        public string Process
        {
            get
            {
                return GetGrade(ProcessWeightedGrade);
            }
        }

        public string Firm
        {
            get
            {
                return GetGrade(FirmWeightedGrade);
            }
        }

        public string Performance
        {
            get
            {
                return GetGrade(PerformanceWeightedGrade);
            }
        }

        public string TrackRecord
        {
            get
            {
                return GetGrade(TrackRecordWeightedGrade);
            }
        }

        public string Consistency
        {
            get
            {
                return GetGrade(PerformanceConsistencyWeightedGrade);
            }
        }

    }
}
