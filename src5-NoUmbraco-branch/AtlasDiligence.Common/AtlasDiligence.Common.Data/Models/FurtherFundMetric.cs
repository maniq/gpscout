﻿namespace AtlasDiligence.Common.Data.Models
{
    using General;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public partial class FurtherFundMetric
    {
        private bool? _isPreqinMostRecentMetric;
        public bool IsPreqinMostRecentMetric
        {
            get
            {
                if (!_isPreqinMostRecentMetric.HasValue)
                    _isPreqinMostRecentMetric = this.Source == FundMetricSource.Preqin;

                return _isPreqinMostRecentMetric.GetValueOrDefault();
            }
        }


        //public decimal? SourceAwareDpi
        //{
        //    get
        //    {
        //        // dpi is same for RCP and RCP Gross so that branch is not required here
        //        return IsPreqinMostRecentMetric ? this.PreqinDpi : this.Dpi;
        //    }
        //}


        //public decimal? SourceAwareTvpi
        //{
        //    get
        //    {
        //        return IsPreqinMostRecentMetric ? this.PreqinTvpi :  (IsSourceRCPGross ? this.Moic : this.Tvpi);
        //    }
        //}


        //public decimal? SourceAwareIrr
        //{
        //    get
        //    {
        //        return IsPreqinMostRecentMetric ? this.PreqinIrr : (IsSourceRCPGross ? this.GrossIrr : this.Irr);
        //    }
        //}


        public DateTime? SourceAwareAsOf
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinAsOf : this.AsOf;
            }
        }


        public string DisplaySource
        {
            get
            {
                //return this.Source == "RCP" ? "GP" : this.Source;
                return (this.Source != null && this.Source.StartsWith("RCP")) ? "GP" : this.Source;
            }
        }


        public bool IsSourceRCPGross
        {
            get
            {
                return string.Compare(this.Source, "RCP (Gross)", true) == 0;
            }
        }

    }
}
