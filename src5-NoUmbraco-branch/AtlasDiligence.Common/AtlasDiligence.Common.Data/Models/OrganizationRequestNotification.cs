﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public class OrganizationRequestNotification: OrganizationRequest
    {
        public string Email { get; set; }
    }
}
