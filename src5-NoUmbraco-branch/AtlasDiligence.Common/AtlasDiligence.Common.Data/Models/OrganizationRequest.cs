﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class OrganizationRequest
    {
        public string UserEmail
        {
            get
            {
                return (aspnet_User != null && aspnet_User.LoweredUserName != null && aspnet_User.LoweredUserName != String.Empty)
                    ? aspnet_User.LoweredUserName
                    : RequestAdditionalInfo != null ? RequestAdditionalInfo.UserEmail : String.Empty;
            }
        }
    }
}
