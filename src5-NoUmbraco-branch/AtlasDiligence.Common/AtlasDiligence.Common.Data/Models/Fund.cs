﻿// -----------------------------------------------------------------------
// <copyright file="Fund.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace AtlasDiligence.Common.Data.Models
{
    using General;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public partial class Fund
    {
        public IEnumerable<string> SectorFocus
        {
            get { return SectorFocusPipeDelimited.Split(new[] {'|'}); }
        }


        private bool? _isPreqinMostRecentMetric;
        public bool IsPreqinMostRecentMetric
        {
            get
            {
                if (!_isPreqinMostRecentMetric.HasValue)
                    _isPreqinMostRecentMetric = this.Source == FundMetricSource.Preqin;

                return _isPreqinMostRecentMetric.GetValueOrDefault();
            }
        }


        //public decimal? SourceAwareDpi
        //{
        //    get
        //    {
        //        // dpi is same for RCP and RCP Gross so that branch is not required here
        //        return IsPreqinMostRecentMetric ? this.PreqinDpi : this.Dpi;
        //    }
        //}


        //public decimal? SourceAwareTvpi
        //{
        //    get
        //    {
        //        return IsPreqinMostRecentMetric ? this.PreqinTvpi : (IsSourceRCPGross ? this.Moic : this.Tvpi);
        //    }
        //}


        //public decimal? SourceAwareIrr
        //{
        //    get
        //    {
        //        return IsPreqinMostRecentMetric ? this.PreqinIrr : (IsSourceRCPGross ? this.GrossIrr : this.Irr);
        //    }
        //}


        public DateTime? SourceAwareAsOf
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinAsOf : this.AsOf;
            }
        }

        public bool? HasPreqinMetric { get; set; }


        public string DisplaySource
        {
            get
            {
                return (this.Source != null && this.Source.StartsWith("RCP")) ? "GP" : this.Source;
            }
        }


        public bool IsSourceRCPGross
        {
            get
            {
                return string.Compare(this.Source, "RCP (Gross)", true) == 0;
            }
        }


        public static string[] OpenFundStatuses = new string[] { "Open", "Pre-Marketing" };


        bool? _isOpen;
        public bool IsOpen {
            get
            {
                if (!_isOpen.HasValue) _isOpen = OpenFundStatuses.Contains(this.Status);
                return _isOpen.Value;
            }
        }


        bool _vintageYearSet;
        int? _vintageYear;
        public int? VintageYear
        {
            get
            {
                if (!_vintageYearSet)
                {
                    _vintageYearSet = true;
                    int vintageYear;
                    if (int.TryParse(VintageYearStr, out vintageYear))
                        _vintageYear = vintageYear;
                }
                return _vintageYear;
            }
            set
            {
                _vintageYear = value;
                VintageYearStr = Convert.ToString(value);
            }
        }

    }
}
