﻿
using System;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class Currency
    {
        public string FormattedSymbol
        {
            get
            {
                return String.IsNullOrWhiteSpace(Symbol)
                           ? Code
                           : (Symbol.Length > 1) ? String.Format("({0}) ", Symbol) : Symbol;
            }
        }
    }
}
