﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public static class AppSettings
    {
        public static string TempPath
        {
            get
            {
                return ConfigurationManager.AppSettings["TempPath"];
            }
        }

        public static string PdfGeneratorPath
        {
            get
            {
                return ConfigurationManager.AppSettings["PdfGeneratorPath"];
            }
        }

        public static int PdfWatermarkSizeEm
        {
            get
            {
                string emSizeStr = ConfigurationManager.AppSettings["PdfWatermarkSizeEm"];
                int size = 30;
                int.TryParse(emSizeStr, out size);
                if (size < 1) size = 1;
                return size;
            }
        }

        public static string PrintAllAdditionalCmdLineOptions
        {
            get
            {
                return ConfigurationManager.AppSettings["PrintAllAdditionalCmdLineOptions"];
            }
        }

        public static string LongestWatermarkText
        {
            get
            {
                return ConfigurationManager.AppSettings["LongestWatermarkText"];
            }
        }

        public static int UserNotificationVisibilityMS
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationVisibilityMS"], out i))
                    return Math.Max(i, 0);

                return 5000;
            }
        }

        public static int UserNotificationCheckIntervalMS
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationCheckIntervalMS"], out i))
                    return Math.Max(i, 5000);

                return 5000;
            }
        }

        public static int CleanupIntervalDays
        {
            get
            {
                int i;
                if (!int.TryParse(ConfigurationManager.AppSettings["CleanupIntervalDays"], out i))
                    i = 30;
                return Math.Max(i, 1);
            }
        }

        public static double PdfZoom
        {
            get
            {
                double d;
                if (double.TryParse(ConfigurationManager.AppSettings["PdfZoom"], out d))
                    return Math.Max(d, 0.1);

                return 1.0;
            }
        }
        public static ulong PdfGenerationJSDelayMS
        {
            get
            {
                ulong i;
                if (ulong.TryParse(ConfigurationManager.AppSettings["PdfGenerationJSDelayMS"], out i))
                    return Math.Max(i, 0);

                return 1000;
            }
        }

        public static int PdfGenerationTimeoutMS
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["PdfGenerationTimeoutSec"], out i))
                    return Math.Max(i, 9) * 1000;

                return 6000000; // 100 minutes
            }
        }

        public static int UserNotificationMaxPollingSec
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["UserNotificationMaxPollingSec"], out i))
                    return Math.Max(i, 0) * 1000;

                return 60000; // 1 minute
            }
        }

        public static string MailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings["MailFrom"];
            }
        }

        public static string ContactToMail
        {
            get
            {
                return ConfigurationManager.AppSettings["ContactToMail"];
            }
        }

        public static string ContactMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["ContactMailSubject"];
            }
        }

        public static string NewsMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["NewsMailSubject"];
            }
        }

        public static string IndexPath
        {
            get
            {
                return ConfigurationManager.AppSettings["IndexLocation"];
            }
        }


        public static int DbCommandTimeoutSeconds
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["DbCommandTimeoutSeconds"], out i))
                    return i;
                return 600;
            }
        }


        public static int MaxProfilePdfGenerationBatchSize
        {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["MaxProfilePdfGenerationBatchSize"], out i))
                    return i;
                return 3;
            }
        }

        //public static string DataImportConfigFilePath {
        //    get
        //    {
        //        string path = ConfigurationManager.AppSettings["DataImportConfigFilePath"];
        //        if (string.IsNullOrEmpty(path)) path = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
        //        return path;
        //    }
        //}

        public static int DataImportRespawnSeconds {
            get
            {
                int i;
                if (int.TryParse(ConfigurationManager.AppSettings["DataImportRespawnSeconds"], out i))
                    return i;
                return 7200;
            }
        }

        public static string DataImportApplication
        {
            get
            {
                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), ConfigurationManager.AppSettings["DataImportApplication"]);
                if (path != null && path.StartsWith(@"file:\")) path = path.Substring(6);
                return path;
            }
        }

    }
}