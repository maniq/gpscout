﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using AtlasDiligence.Common.Data.Models;

//namespace AtlasDiligence.Common.Data.Repositories
//{
//    public class NewsRepository : Repository<News>, INewsRepository
//    {
//        public IEnumerable<News> GetBetweenDates(DateTime startDate, DateTime endDate)
//        {
//            return DataContext.News.Where(m => m.PublishedDate > startDate && m.PublishedDate < endDate);
//        }

//        public void AddNews(News news)
//        {
//            DataContext.News.InsertOnSubmit(news);
//            DataContext.SubmitChanges();
//        }

//        public DateTime GetMostRecentPublishedDate()
//        {
//            if (DataContext.News.Count() == 0)
//            {
//                return DateTime.MinValue;
//            }
//            return DataContext.News.OrderByDescending(m => m.PublishedDate).First().PublishedDate;
//        }

//        public void AddNewsEntity(NewsEntity newsEntity)
//        {
//            DataContext.NewsEntities.InsertOnSubmit(newsEntity);
//            DataContext.SubmitChanges();
//        }
//    }
//}
