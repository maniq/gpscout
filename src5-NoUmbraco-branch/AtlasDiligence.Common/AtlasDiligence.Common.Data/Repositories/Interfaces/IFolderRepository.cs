﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IFolderRepository : IRepository<Folder>
    {
        IEnumerable<Folder> GetByUserId(Guid id);
        void AddOrganizationToFolder(Guid folderId, Guid organizationId);
        void AddOrganizationsToFolder(Guid folderId, IEnumerable<Guid> organizationId);
        void RemoveOrganizationFromFolder(Guid folderId, Guid organizationId);

        IQueryable<Organization> GetOrganizationByName(string organizationName);

        void RemoveFolder(Guid id);
        void AddFolder(Folder folder);

        IQueryable<Organization> GetOrganizationsStartsWith(string request);

        void RegisterUserClick(Guid userId, Guid folderId);
        IQueryable<Folder> GetRecentFoldersForUser(Guid userId, int numToTake);
    }
}
