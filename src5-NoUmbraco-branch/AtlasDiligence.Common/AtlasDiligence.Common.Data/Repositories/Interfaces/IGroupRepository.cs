﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IGroupRepository : IRepository<Group>
    {
        /// <summary>
        /// Return number of active users for given group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<aspnet_User> GetActiveUsersByGroupId(Guid id);

        /// <summary>
        /// Returns any groups that have a groupLeaderId of given userId
        /// </summary>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        IEnumerable<Group> GetGroupsByGroupLeaderId(Guid userId);

        /// <summary>
        /// Return organizations for the specified Ids
        /// </summary>
        /// <param name="organizationIds">List of organization Ids</param>
        /// <returns></returns>
        IEnumerable<Organization> GetGroupOrganizations(List<Guid> organizationIds);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="organizationId"></param>
        void AddGroupOrganization(Guid groupId, Guid organizationId);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="organizationId"></param>
        void RemoveGroupOrganization(Guid groupId, Guid organizationId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="segmentId"></param>
        void AddGroupSegment(Guid groupId, Guid segmentId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="segmentId"></param>
        void RemoveGroupSegment(Guid groupId, Guid segmentId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segmentName"></param>
        /// <returns></returns>
        IEnumerable<Segment> GetSegmentsByName(string segmentName);
    }
}
