﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IRequestRepository : IRepository<OrganizationRequest>
    {
        void InsertRequest(RequestType type, Guid organizationId, Guid userId, string comment);
        void InsertRequest(RequestType type, string organizationName, Guid? userId, string comment);
        void InsertRequest(RequestType type, string organizationName, Guid? userId, string comment, string userEmail);
        void Edit(OrganizationRequest request);
    }
}
