﻿//using System;
//using System.Collections.Generic;
//using AtlasDiligence.Common.Data.Models;

//namespace AtlasDiligence.Common.Data.Repositories
//{
//    public interface INewsRepository : IRepository<News>
//    {
//        /// <summary>
//        /// Return News with a publishDate between given start and end dates.
//        /// </summary>
//        /// <param name="startDate"></param>
//        /// <param name="endDate"></param>
//        /// <returns></returns>
//        IEnumerable<News> GetBetweenDates(DateTime startDate, DateTime endDate);
       
//        /// <summary>
//        /// Returns most recent News.PublishDate if there is data.
//        /// </summary>
//        /// <returns></returns>
//        DateTime GetMostRecentPublishedDate();

//        /// <summary>
//        /// Add News to dB.
//        /// </summary>
//        /// <param name="news"></param>
//        void AddNews(News news);

//        /// <summary>
//        /// Associate an entity (such as Organization or Contact) with a local News entity.
//        /// </summary>
//        /// <param name="newsEntity"></param>
//        void AddNewsEntity(NewsEntity newsEntity);
//    }
//}
