﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class SearchRepository : Repository<Organization>, ISearchRepository
    {
        public IEnumerable<string> GetDistinctOptionsByName(General.FieldType type)
        {
            var result = from so in this.DataContext.SearchOptions
                         where so.Field == (int)type
                         select so.Value;
            return result.Distinct();
        }

        public IEnumerable<SearchOption> GetDistinctOptions()
        {
            var result = from so in this.DataContext.SearchOptions
                         select so;
            return result.Distinct();
        }

        public void Insert<T>(T item) where T : class
        {
            DataContext.GetTable<T>().InsertOnSubmit(item);
            DataContext.SubmitChanges();
        }

        public void InsertAll<T>(IEnumerable<T> items) where T : class
        {
            DataContext.GetTable<T>().InsertAllOnSubmit(items);
            DataContext.SubmitChanges();
        }

        public IEnumerable<SearchTemplate> GetSearchTemplatesByUserId(Guid userId)
        {
            return DataContext.SearchTemplates.Where(m => m.UserId == userId).OrderByDescending(m => m.CreatedDate);
        }

        public SearchTemplate GetSearchTemplateById(Guid userId, Guid id)
        {
            return DataContext.SearchTemplates.SingleOrDefault(m => m.Id == id && m.UserId == userId);
        }

        public void InsertSavedSearch(SavedSearch savedSearch)
        {
            DataContext.SavedSearches.InsertOnSubmit(savedSearch);
            DataContext.SubmitChanges();
        }

        public IQueryable<SavedSearch> GetSavedSearches(Guid userId)
        {
            return DataContext.SavedSearches.Where(m => m.SearchTemplate.UserId == userId);
        }

        public SavedSearch GetSavedSearch(Guid searchTemplateId)
        {
            return DataContext.SavedSearches.SingleOrDefault(m => m.SearchTemplateId == searchTemplateId);
        }

        public void DeleteSavedSearch(Guid searchTemplateId)
        {
            DataContext.SavedSearches.DeleteAllOnSubmit(
                DataContext.SavedSearches.Where(m => m.SearchTemplateId == searchTemplateId));
            DataContext.SubmitChanges();
        }
    }
}
