﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using AtlasDiligence.Common.Data.Models;

//namespace AtlasDiligence.Common.Data.Repositories
//{
//    public class SearchTermRepository : Repository<SearchTerms>, ISearchTermRepository
//    {
//        public SearchTermRepository() { }

//        public IEnumerable<SearchTerms> GetAllByUserId(Guid userId)
//        {
//            return GetAll().Where(m => m.UserId == userId);
//        }

//        public void Add(SearchTerms searchTerm)
//        {
//            this.InsertOnSubmit(searchTerm);
//            SubmitChanges();
//        }

//        public void Delete(string searchTerm, Guid userId)
//        {
//            this.DeleteAllOnSubmit(m => m.SearchTerm == searchTerm && m.UserId == userId);
//            this.SubmitChanges();
//        }

//        public void DeleteAllForUser(Guid userId)
//        {
//            this.DeleteAllOnSubmit(m => m.UserId == userId);
//            this.SubmitChanges();
//        }

//        /// <summary>
//        /// Overload passing just userId
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns></returns>
//        //public IEnumerable<string> GetSearchTermsForUser(Guid userId)
//        //{
//        //    var user = DataContext.aspnet_Users.FirstOrDefault(x => x.UserId == userId);
//        //    if (user == null)
//        //        return null;
//        //    return GetSearchTermsForUser(user);
//        //}

//        /// <summary>
//        /// Returns the terms that will be searched for a user.
//        /// Will also include any aliases found in AssociatedTerms if the user's RssSearchAliases flag is true.
//        /// </summary>
//        /// <param name="user"></param>
//        /// <returns></returns>
//        //public IEnumerable<String> GetSearchTermsForUser(aspnet_User user)
//        //{
//        //    var userSearchTerms = GetAllByUserId(user.UserId).Select(m => m.SearchTerm).ToList();
//        //    if (user.UserExt != null && user.UserExt.RssSearchAliases)
//        //    {
//        //        var termAliases =
//        //            DataContext.AssociatedTerms.Where(x => userSearchTerms.Contains(x.SourceTerm)).Select(
//        //                y => y.AssociatedTerm1).ToList();
//        //        return userSearchTerms.Union(termAliases);
//        //    }

//        //    return userSearchTerms;
//        //}

//        /// <summary>
//        /// Returns kvp of search term and associated aliases (aliases will include search term)
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns></returns>
//        public IDictionary<String, IEnumerable<String>> GetSearchTermAndAliasesForUser(Guid userId)
//        {
//            var user = DataContext.aspnet_Users.FirstOrDefault(x => x.UserId == userId);
//            if (user == null)
//                return null;

//            var userSearchTerms = GetAllByUserId(user.UserId).Select(m => m.SearchTerm).ToList();
//            return userSearchTerms.ToDictionary(
//                y => y,
//                y =>
//                    {
//                        var aliases =
//                            DataContext.AssociatedTerms.Where(x => x.SourceTerm == y).Select(z => z.AssociatedTerm1).
//                                ToList();
//                        aliases.Add(y);
//                        return aliases.AsEnumerable();
//                    });
//        }
//    }
//}