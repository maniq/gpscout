﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class UserRepository : Repository<aspnet_User>, IUserRepository
    {
        public UserRepository() { }


        #region IUserRepository Members

        public void DeleteUser(aspnet_User dbUser)
        {
            DataContext.DeleteUser(dbUser.UserId);
        }

        //public void UpdateUserEmailDate(Guid userId, DateTime date)
        //{
        //    var user = DataContext.UserExts.Single(m => m.UserId == userId);
        //    user.RssNextEmailDate = date;
        //    DataContext.SubmitChanges();
        //}

        public IEnumerable<aspnet_User> FindAllEager(Expression<Func<aspnet_User, bool>> expression)
        {

            var dataContext = new AtlasWebDbDataContext();
            var dlo = new DataLoadOptions();
            dlo.LoadWith<aspnet_User>(m => m.UserExt);
            dataContext.LoadOptions = dlo;
#if DEBUG
			dataContext.Log = new AtlanticBT.Common.Instrumentation.DebugConsoleStream();
#endif
            return dataContext.GetTable<aspnet_User>().Where(expression);
        }


        public aspnet_User GetByEmail(string email)
        {
            return DataContext.aspnet_Users.FirstOrDefault(x => x.aspnet_Membership.Email.Equals(email));
        }

        public bool HasDuplicate(Guid userId, string userName)
        {
            return
                DataContext.aspnet_Users.Any(
                    x => !x.UserId.Equals(userId) && x.UserName.Equals(userName));
        }

        public IEnumerable<aspnet_User> GetActiveUsers(Guid groupId)
        {
            return DataContext.aspnet_Users.Where(x => x.UserExt.GroupId == groupId && x.aspnet_Membership.IsApproved);
        }

        //public void UpdateRssEmails(Guid userId, IEnumerable<string> emails)
        //{
        //    DataContext.UserRssEmails.DeleteAllOnSubmit(DataContext.UserRssEmails.Where(m => m.UserId == userId));
        //    DataContext.SubmitChanges();
        //    DataContext.UserRssEmails.InsertAllOnSubmit(emails.Select(m => new UserRssEmail { Id = Guid.NewGuid(), UserId = userId, RssEmailAddress = m }));
        //    DataContext.SubmitChanges();
        //}

        //public void RemoveRssEmail(Guid userId, string email)
        //{
        //    DataContext.UserRssEmails.DeleteAllOnSubmit(
        //        DataContext.UserRssEmails.Where(m => m.UserId == userId && m.RssEmailAddress == email));
        //    DataContext.SubmitChanges();
        //}

        public IEnumerable<RecentlyViewedOrganizationsResult> GetRecentlyViewedOrganizations(Guid userId, int take)
        {
            return DataContext.RecentlyViewedOrganizations(userId, take);
        }

        public Group GetGroupById(Guid id)
        {
            return DataContext.Groups.SingleOrDefault(m => m.Id == id);
        }

        public void InsertUserLogin(Guid userId)
        {
            DataContext.TrackUsers.InsertOnSubmit(new TrackUser
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Type = (int)TrackUsersType.Login,
                Date = DateTime.UtcNow
            });

            DataContext.SubmitChanges();
        }

        public void InsertUserLogout(Guid userId)
        {
            DataContext.TrackUsers.InsertOnSubmit(new TrackUser
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Type = (int)TrackUsersType.Logout,
                Date = DateTime.UtcNow
            });

            DataContext.SubmitChanges();
        }


        public IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate)
        {
            return DataContext.TrackUsers.Where(w => w.Type == (int)type
                                                     && w.Date.Date >= startDate
                                                     && w.Date.Date <= endDate);
        }

        public void InsertOrganizationUserAction(Guid userId, Guid orgId, TrackUsersType trackUsersType)
        {
            var trackuser = new TrackUser
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Type = (int)trackUsersType,
                Date = DateTime.UtcNow
            };

            DataContext.TrackUsers.InsertOnSubmit(trackuser);

            DataContext.TrackUsersProfileViews.InsertOnSubmit(new TrackUsersProfileView
            {
                Id = Guid.NewGuid(),
                TrackUsersId = trackuser.Id,
                OrganizationId = orgId
            });

            DataContext.SubmitChanges();
        }

        public void InsertUserAction(Guid userId, TrackUsersType actionType)
        {
            var trackuser = new TrackUser
            {
                Id = Guid.NewGuid(),
                UserId = userId,
                Type = (int)actionType,
                Date = DateTime.UtcNow
            };

            DataContext.TrackUsers.InsertOnSubmit(trackuser);
            DataContext.SubmitChanges();
        }

        #endregion
    }
}

