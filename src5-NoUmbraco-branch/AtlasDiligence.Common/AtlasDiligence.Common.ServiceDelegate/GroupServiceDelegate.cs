﻿using System;
using System.Linq;

namespace AtlasDiligence.Common.ServiceDelegate
{
    public class GroupServiceDelegate : ServiceDelegateBase
    {
        public void RemoveUserAsGroupLeader(Guid userId)
        {
            var groups = from g in this.Context.CreateQuery<new_group>()
                         where g.new_webuserid.Id == userId
                         select g;
            foreach (var group in groups)
            {
                group.new_webuserid = null;
                this.Context.UpdateObject(group);
            }
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Returns Group if only one exists in CRM. There should not be duplicates in the CRM.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public new_group GetByName(string name)
        {
            var groups = FindAll<new_group>((x => x.new_name == name));
            if (groups.ToList().Count() > 1)
                return null;
            return groups.SingleOrDefault();
        }
    }
}
