﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AtlasDiligence.Common.ServiceDelegate
{
    public class OrganizationServiceDelegate : ServiceDelegateBase
    {
        public List<KeyValuePair<Guid, List<string>>> GetAllOrgIdAndAliases()
        {
            var retval = from a in this.Context.CreateQuery<Account>()
                         select new
                         {
                             a.Id,
                             a.Name,
                             Alias = a.new_FormerName
                         };

            return
                retval.ToList()
                      .Select(
                          m =>
                          new KeyValuePair<Guid, List<string>>(m.Id,
                                                               m.Alias == null
                                                                   ? new List<string> { m.Name }
                                                                   : m.Alias.Split(new[] { '|' })
                                                                      .Union(new[] { m.Name })
                                                                      .ToList())).ToList();
        }

        public List<KeyValuePair<Guid, List<string>>> GetAllContactIdAndAliases()
        {
            var retval = (from a in this.Context.CreateQuery<Contact>()
                         select new
                         {
                             a.Id,
                             a.FirstName,
                             a.LastName,
                             Alias = a.new_FirstNameAliases
                         }).ToList();
        
            return
                retval.Select(
                          m =>
                          new KeyValuePair<Guid, List<string>>(m.Id,
                                                               m.Alias == null
                                                                   ? new List<string> { String.Format("{0} {1}", m.FirstName, m.LastName) }
                                                                   : m.Alias.Split(',').Select(s=> s.Trim() + " " + m.LastName)
                                                                      .Union(new[] { String.Format("{0} {1}", m.FirstName, m.LastName) })
                                                                      .ToList())).ToList();
        }

        public IEnumerable<Account> GetResearchPriorityOrganizations(int skip, int take)
        {
            return (from a in this.Context.CreateQuery<Account>()
                    where a.new_ResearchPriorityCurrent.Value > 0 && a.new_ResearchPriorityCurrent.Value < 100000004
                    select a).OrderBy(m => m.Name).Skip(skip).Take(take);
        }

        public IEnumerable<Account> GetAll(int skip, int take, bool getUnpublishedOrgs = false)
        {
            if (getUnpublishedOrgs)
            {
                return (from a in this.Context.CreateQuery<Account>()
                        orderby a.Name
                        select a).Skip(skip).Take(take);
            }
            else
            {
                return (from a in this.Context.CreateQuery<Account>()
                        where a.new_PublishLevel1.GetValueOrDefault(false) || a.new_PublishLevel0.GetValueOrDefault(false)
                        select a).OrderBy(m => m.Name).Skip(skip).Take(take);
            }
        }

        public IEnumerable<Connection> GetPortfolioCompaniesByOrganizationId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.Record1Id.Id.Equals(id)
                                    && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;
            foreach (var c in connections)
            {
                Context.LoadProperty(c, "account_connections2");
            }
            return connections.ToList().Where(c => c.Record1RoleId.Name == "GP for Sample Portfolio Company");
        }

        public IEnumerable<new_clientrequestentity> GetClientRequests(int skip, int take)
        {
            var requests = (from r in this.Context.CreateQuery<new_clientrequestentity>()
                            where r.new_GPScoutGroup != null
                            orderby r.Id
                            select r).Skip(skip).Take(take);
            foreach (var request in requests)
            {
                Context.LoadProperty(request, "new_account_new_clientrequestentity_AssociatedGP");
            }
            return requests;
        }

        public IEnumerable<Connection> GetAllPortfolioCompanies()
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;
            foreach (var c in connections)
            {
                Context.LoadProperty(c, "account_connections2");
            }
            return connections.ToList().Where(c => c.Record1RoleId.Name == "GP for Sample Portfolio Company");
        }

        public IEnumerable<Connection> GetAllConnectionsById(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.Record1Id.Id.Equals(id)
                                    && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;
            return connections;
        }

        public IEnumerable<Account> GetLimitedPartnersByFundId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              join o in this.Context.CreateQuery<Account>() on c.Record2Id.Id equals o.Id
                              where c.Record1Id.Id.Equals(id) && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select new
                              {
                                  Organization = o,
                                  Role1Name = c.Record1RoleId != null ? c.Record1RoleId.Name : String.Empty
                              };

            return connections.ToList()
                .Where(m => m.Role1Name == "GP Commitment")
                .Select(m => m.Organization);
        }

        public IEnumerable<Connection> GetAdvisoryBoardByFundId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              join o in this.Context.CreateQuery<Account>() on c.Record2Id.Id equals o.Id
                              where c.Record1Id.Id.Equals(id) && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;
            foreach (var c in connections)
            {
                Context.LoadProperty(c, "account_connections2");
            }
            return connections.ToList()
                .Where(m => m.Record1Id != null && m.Record1RoleId.Name == "Board Member")
                .Select(m => m);
        }

        public IEnumerable<Connection> GetAllAdvisoryBoards()
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              join o in this.Context.CreateQuery<Account>() on c.Record2Id.Id equals o.Id
                              where c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;
            foreach (var c in connections)
            {
                Context.LoadProperty(c, "account_connections2");
            }
            return connections.ToList()
                .Where(m => m.Record1Id != null && m.Record1RoleId.Name == "Board Member")
                .Select(m => m);
        }

        public IEnumerable<Connection> GetBoardSeatsByContactId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.Record1Id.Id.Equals(id)
                                    && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            return connections.ToList().Where(
                m =>
                (m.Record1RoleId != null && m.Record1RoleId.Name == "Board Member"));
        }

        public IEnumerable<Connection> GetAllBoardSeats()
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            return connections.ToList().Where(
                m =>
                (m.Record1RoleId != null && m.Record1RoleId.Name == "Board Member"));
        }

        public IEnumerable<Connection> GetEducationHistoryByContactId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.Record1Id.Id.Equals(id)
                                    && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            //TODO: Is this supposed to be "School Attended" or "Graduate"?
            return connections.ToList().Where(
                m =>
                (m.Record1RoleId != null && m.Record1RoleId.Name == "Graduate"));
        }

        public IEnumerable<Connection> GetAllEducationHistories()
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            //TODO: Is this supposed to be "School Attended" or "Graduate"?
            return connections.ToList().Where(
                m =>
                (m.Record1RoleId != null && m.Record1RoleId.Name == "Graduate"));
        }

        public IEnumerable<Connection> GetPreviousEmployerByContactId(Guid id)
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.Record1Id.Id.Equals(id)
                                    && c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            return
                connections.ToList().Where(
                    m =>
                    (m.Record1RoleId != null && m.Record1RoleId.Name == "Former Employee"));
        }

        public IEnumerable<Connection> GetAllEmploymentHistories()
        {
            var connections = from c in this.Context.CreateQuery<Connection>()
                              where c.new_Publish.GetValueOrDefault(false)
                              orderby c.new_DisplayOrder
                              select c;

            return
                connections.ToList().Where(
                    m =>
                    (m.Record1RoleId != null && m.Record1RoleId.Name == "Former Employee"));
        }

        public IEnumerable<new_strengths> GetAllStrengths()
        {
            return from s in this.Context.CreateQuery<new_strengths>()
                   where s.new_Publish.GetValueOrDefault(false)
                   select s;
        }

        public IEnumerable<new_concerns> GetAllConcerns()
        {
            return from c in this.Context.CreateQuery<new_concerns>()
                   where c.new_Publish.GetValueOrDefault(false)
                   select c;
        }

        public IEnumerable<new_strengths> GetStrengthsByOrganizationId(Guid id)
        {
            return from s in this.Context.CreateQuery<new_strengths>()
                   where s.new_Organization.Id.Equals(id) && s.new_Publish.GetValueOrDefault(false)
                   select s;
        }

        public IEnumerable<new_concerns> GetConcernsByOrganizationId(Guid id)
        {
            return from c in this.Context.CreateQuery<new_concerns>()
                   where c.new_Organization.Id.Equals(id) && c.new_Publish.GetValueOrDefault(false)
                   select c;
        }

        public IEnumerable<new_fund> GetFundsByOrganizationId(Guid id)
        {
            return from u in this.Context.CreateQuery<new_fund>()
                   where u.new_OrganizationId != null && u.new_OrganizationId.Id == id
                   && u.new_PublishLevel1.GetValueOrDefault(false)
                   orderby u.new_FundNumber descending
                   select u;
        }

        public IEnumerable<Contact> GetContactsByOrganizationId(Guid id)
        {
            var result = from c in this.Context.CreateQuery<Contact>()
                         where c.FirstName != String.Empty && c.LastName != String.Empty && c.ParentCustomerId.Id == id
                               && c.new_Publish.GetValueOrDefault(false)
                         orderby c.new_DisplayOrder
                         select c;
            return result.ToList();
        }

        public IEnumerable<Contact> GetAllContacts(int skip, int take)
        {
            var result = (from c in this.Context.CreateQuery<Contact>()
                          where
                              c.ParentCustomerId != null &&
                              c.FirstName != String.Empty && c.LastName != String.Empty &&
                              c.new_Publish.GetValueOrDefault(false)
                          orderby c.new_DisplayOrder
                          select c).Skip(skip).Take(take);
            return result.ToList();
        }

        public IEnumerable<new_duediligenceitems> GetAllDueDiligenceItems()
        {
            var result = from c in this.Context.CreateQuery<new_duediligenceitems>()
                         select c;
            return result.ToList();
        }

        public IEnumerable<new_duediligenceitems> GetDueDiligenceItemsByOrganizationId(Guid id)
        {
            var result = from c in this.Context.CreateQuery<new_duediligenceitems>()
                         where c.new_Organization.Id == id
                         select c;
            return result.ToList();
        }

        public IEnumerable<new_addressesprimary> GetAllOtherAddresses()
        {
            return from s in this.Context.CreateQuery<new_addressesprimary>()
                   select s;
        }

        public IEnumerable<new_addressesprimary> GetOtherAddressesByOrganizationId(Guid id)
        {
            return from s in this.Context.CreateQuery<new_addressesprimary>()
                   where s.new_Organization.Id.Equals(id)
                   select s;
        }

        public TransactionCurrency GetCurrencyById(Guid id)
        {
            var retval = (from c in this.Context.CreateQuery<TransactionCurrency>()
                          where c.Id == id
                          select c);
            return retval.SingleOrDefault();
        }

        public IEnumerable<TransactionCurrency> GetAllCurrencies()
        {
            var retval = (from c in this.Context.CreateQuery<TransactionCurrency>()
                          select c);
            return retval;
        }

		public IEnumerable<new_trackrecorditem> GetTrackRecordsByOrganizationId(Guid id)
        {
            var result = from t in Context.CreateQuery<new_trackrecorditem>()
                         where t.new_Organization.Id == id && t.new_Publish.GetValueOrDefault(false)
                         select t;
            return result;
        }

        public IEnumerable<new_trackrecorditem> GetAllTrackRecords()
        {
            var result = from t in Context.CreateQuery<new_trackrecorditem>()
                         where t.new_Publish.GetValueOrDefault(false)
                         select t;
            return result;
        }

		public IEnumerable<new_benchmark> GetAllBenchmarks()
		{
			var results = from t in Context.CreateQuery<new_benchmark>()
			              select t;

			return results;
		}
    }
}
