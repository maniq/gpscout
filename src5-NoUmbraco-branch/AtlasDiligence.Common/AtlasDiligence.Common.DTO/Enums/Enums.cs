﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Enums
{
    using System.ComponentModel;

    public enum EvaluationType { Strength, Concern }

    public enum ProductTypes
    {
        [Description("Profile Account")]
        ProfileAccount = 0,

        [Description("Diligence Account")]
        DiligenceAccount = 1 
    }
}
