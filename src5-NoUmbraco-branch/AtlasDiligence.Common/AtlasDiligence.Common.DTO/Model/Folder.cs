﻿using System;

namespace AtlasDiligence.Common.DTO.Model
{
    public class Folder : ServiceModel
    {
        public string Name { get; set; }
        public Guid WebUserId { get; set; }
        public string WebUserName { get; set; }
    }
}
