﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.ServiceDelegate;
using log4net;
using Fund = AtlasDiligence.Common.Data.Models.Fund;
using Organization = AtlasDiligence.Common.Data.Models.Organization;
using System.IO;
using Utility;

namespace AtlasDiligence.Common.DTO.Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly OrganizationServiceDelegate serviceDelegate;
        private readonly Repository<Data.Models.Organization> organizationRepository;
        private readonly Repository<Data.Models.Benchmark> benchmarkRepository;
        private readonly Repository<Data.Models.aspnet_User> userRepository;
        private readonly Repository<ResearchPriorityOrganization> researchPriorityRepository;
        private readonly Repository<OrganizationViewing> organizationViewingRepository;
        private readonly IClientRequestRepository clientRequestRepository;
        private static readonly ILog Log = LogManager.GetLogger(typeof(OrganizationService));

        public IEnumerable<KeyValuePair<Guid, List<string>>> GetAllOrgIdAndAliases()
        {
            return this.serviceDelegate.GetAllOrgIdAndAliases();
        }

        public IEnumerable<KeyValuePair<Guid, List<string>>> GetAllContactIdAndAliases()
        {
            return this.serviceDelegate.GetAllContactIdAndAliases();
        }

        public IEnumerable<Data.Models.Organization> GetSegment(int skip, int take, bool getUnpublishedOrgs = false)
        {
            int rows = 0;

            try
            {
                var orgs = this.serviceDelegate.GetAll(skip, take, getUnpublishedOrgs).ToList();

                return CrmEntityMapper.OrganizationsFromCrmEntities(orgs).ToList();
            }
            catch (Exception ex)
            {
                var newTake = take / 2;
                if (newTake > 1)
                {
                    var retval = new List<Data.Models.Organization>();
                    retval.AddRange(GetSegment(skip, newTake));
                    retval.AddRange(GetSegment(skip + newTake, take - newTake));
                    return retval;
                }
                else
                {
                    throw new Exception("Repeatedly retries to connect to CRM have been exhausted.", ex);
                }
            }
        }

        private IEnumerable<ResearchPriorityOrganization> GetResearchPrioritySegment(int skip, int take)
        {
            int rows = 0;

            try
            {
                var orgs = this.serviceDelegate.GetResearchPriorityOrganizations(skip, take).ToList();
                return CrmEntityMapper.RecentPriorityOrganizationsFromCrmEntities(orgs).ToList();
            }
            catch (Exception ex)
            {
                var newTake = take / 2;
                if (newTake > 1)
                {
                    var retval = new List<ResearchPriorityOrganization>();
                    retval.AddRange(GetResearchPrioritySegment(skip, newTake));
                    retval.AddRange(GetResearchPrioritySegment(skip + newTake, take - newTake));
                    return retval;
                }
                else
                {
                    throw new Exception("Repeatedly retries to connect to CRM have been exhausted.", ex);
                }
            }
        }

        private IEnumerable<ClientRequest> GetClientRequests(int skip, int take)
        {
            int rows = 0;

            try
            {
                var orgs = this.serviceDelegate.GetClientRequests(skip, take).ToList();
                return CrmEntityMapper.ClientRequestsFromCrmEntities(orgs).ToList();
            }
            catch (Exception ex)
            {
                var newTake = take / 2;
                if (newTake > 1)
                {
                    var retval = new List<ClientRequest>();
                    retval.AddRange(GetClientRequests(skip, newTake));
                    retval.AddRange(GetClientRequests(skip + newTake, take - newTake));
                    return retval;
                }
                else
                {
                    throw new Exception("Repeatedly retries to connect to CRM have been exhausted.", ex);
                }
            }
        }

        public OrganizationService()
        {
            if (!AppSettings.SkipOrganizationServiceProxyInstantiation)
                this.serviceDelegate = new OrganizationServiceDelegate();
            this.benchmarkRepository = new Repository<Benchmark>();
            this.organizationRepository = new Repository<Data.Models.Organization>();
            this.userRepository = new Repository<Data.Models.aspnet_User>();
            this.organizationViewingRepository = new Repository<OrganizationViewing>();
            this.clientRequestRepository = new ClientRequestRepository();
        }

        public IEnumerable<Data.Models.Organization> GetAllByIds(IEnumerable<Guid> ids)
        {
            return organizationRepository.FindAll(m => ids.Contains(m.Id));
        }

        public Dictionary<Guid, string> GetComparTableOrgsByIds(IEnumerable<Guid> ids)
        {
            int page = 0, pageSize = 500, count = 0;
            var returnDictionary = new Dictionary<Guid, string>();

            // get organizations in batches to avoid 2100 parameter limit 
            do
            {
                var idBatch = ids.Skip(page * pageSize).Take(pageSize).ToList();
                count = idBatch.Count();

                // search orgs with at least one fund and As Of is not null
                organizationRepository.FindAll(f => idBatch.Contains(f.Id))
                    .Where(w => w.Funds.Count > 1 && w.Funds.Any(a => a.AsOf.HasValue || a.PreqinAsOf.HasValue))
                    .ToList().ForEach(fe => returnDictionary.Add(fe.Id, fe.Name));

                page++;
            } while (count == pageSize);

            return returnDictionary.OrderBy(o => o.Value).ToDictionary(k => k.Key, v => v.Value);
        }

        public void AddOrganizationViewing(Guid userId, Guid organizationId)
        {
            organizationViewingRepository.InsertOnSubmit(new OrganizationViewing
                                                                             {
                                                                                 Id = Guid.NewGuid(),
                                                                                 OrganizationId = organizationId,
                                                                                 Timestamp = DateTime.Now,
                                                                                 UserId = userId
                                                                             });
            organizationViewingRepository.SubmitChanges();
        }

        public IEnumerable<OrganizationViewing> GetOrganizationViewings(Guid userId, int skip, int take)
        {
            return
                organizationViewingRepository.FindAll(m => m.UserId == userId).OrderByDescending(m => m.Timestamp).Skip(
                    skip).Take(take);
        }

        public IQueryable<Data.Models.Organization> GetOrganizationsByName(string organizationName)
        {
            return this.organizationRepository.FindAll(m => m.Name == organizationName && (m.PublishOverviewAndTeam || m.PublishSearch));
        }

        public IList<Data.Models.Organization> GetAllPublished(bool queryMasterRecord = false)
        {
            if (queryMasterRecord)
            {
                var organizations = new List<Data.Models.Organization>();
                var accounts = this.serviceDelegate.FindAll<Account>(m => m.new_PublishLevel1.GetValueOrDefault(false) || m.new_PublishLevel0.GetValueOrDefault(false));

                organizations.AddRange(CrmEntityMapper.OrganizationsFromCrmEntities(accounts));

                return organizations;
            }
            else
            {
                return this.organizationRepository.FindAll(m => m.PublishOverviewAndTeam || m.PublishSearch).ToList();
            }
        }

        public IList<Data.Models.Organization> GetAll(bool queryMasterRecord = false, bool getUnpublishedOrgs = false)
        {
            if (queryMasterRecord)
            {
                var pageSize = 100;
                var configPageSize = ConfigurationManager.AppSettings["GetAllPageSize"];
                if (!String.IsNullOrWhiteSpace(configPageSize))
                {
                    pageSize = Convert.ToInt32(configPageSize);
                }
                var page = 0;
                var count = 0;
                var retval = new List<Data.Models.Organization>();
                do
                {
                    var newOrgs = this.GetSegment(page * pageSize, pageSize, getUnpublishedOrgs);
                    count = newOrgs.Count();
                    retval.AddRange(newOrgs);
                    page++;
                } while (count == pageSize);

                return retval;
            }
            else
            {
                return this.organizationRepository.GetAll().ToList();
            }
        }

        public IEnumerable<Data.Models.Organization> GetAllPositions()
        {
            return this.organizationRepository.GetAll().Where(w =>
                                                                (w.PublishOverviewAndTeam || w.PublishSearch)
                                                                && w.Latitude != null
                                                                && w.Longitude != null).ToList()
                                                        .Select(s => new Data.Models.Organization()
                                                            {
                                                                Id = s.Id,
                                                                Name = s.Name,
                                                                FocusRadar = s.FocusRadar,
                                                                FundRaisingStatus = s.FocusRadar,
                                                                Latitude = s.Latitude,
                                                                Longitude = s.Longitude,
                                                                PublishOverviewAndTeam = s.PublishOverviewAndTeam,
                                                                PublishSearch = s.PublishSearch
                                                            });
        }

        public IList<ResearchPriorityOrganization> GetAllResearchPriorityOrganizations(bool queryMasterRecord = false)
        {
            if (queryMasterRecord)
            {
                var pageSize = 100;
                var configPageSize = ConfigurationManager.AppSettings["GetAllPageSize"];
                if (!String.IsNullOrWhiteSpace(configPageSize))
                {
                    pageSize = Convert.ToInt32(configPageSize);
                }
                var page = 0;
                var count = 0;
                var retval = new List<ResearchPriorityOrganization>();
                do
                {
                    var newOrgs = this.GetResearchPrioritySegment(page * pageSize, pageSize);
                    count = newOrgs.Count();
                    retval.AddRange(newOrgs);
                    page++;
                } while (count == pageSize);

                return retval;
            }
            else
            {
                return this.researchPriorityRepository.GetAll().ToList();
            }
        }

        public Data.Models.Organization GetById(Guid id, bool queryFromCrm = false)
        {
            if (queryFromCrm)
            {
                return CrmEntityMapper.OrganizationFromCrmEntity(this.serviceDelegate.GetById<Account>(id));
            }
            else
            {
                return this.organizationRepository.GetById(id);
            }
        }

        public IEnumerable<LimitedPartner> GetLimitedPartnersByFundId(Guid id)
        {
            var result = new List<LimitedPartner>();
            var crmEntities = this.serviceDelegate.GetLimitedPartnersByFundId(id);
            result.AddRange(CrmEntityMapper.LimitedPartnersFromCrmEntities(crmEntities));
            return result;
        }

        public IEnumerable<ClientRequest> GetAllClientRequests(bool queryMasterRecord = false)
        {
            if (queryMasterRecord)
            {
                var pageSize = 100;
                var configPageSize = ConfigurationManager.AppSettings["GetAllPageSize"];
                if (!String.IsNullOrWhiteSpace(configPageSize))
                {
                    pageSize = Convert.ToInt32(configPageSize);
                }
                var page = 0;
                var count = 0;
                var retval = new List<ClientRequest>();
                do
                {
                    var newOrgs = this.GetClientRequests(page * pageSize, pageSize);
                    count = newOrgs.Count();
                    retval.AddRange(newOrgs);
                    page++;
                } while (count == pageSize);

                return retval;
            }
            else
            {
                return clientRequestRepository.GetAll().ToList();
            }
        }

        public IQueryable<ClientRequest> GetClientRequests()
        {
            return this.clientRequestRepository.GetAll();
        }

        public IDictionary<ClientRequest, AtlasDiligence.Common.Data.Models.Organization> GetClientRequestAndAssociatedOrganization(Guid groupId)
        {
            return this.clientRequestRepository.GetAllClientsAndAssociatedOrganizations(groupId);
        }

        public IEnumerable<Benchmark> GetBenchmarks()
        {

            //var latest = (from b in benchmarkRepository.GetAll()
            //               group b by b.VintageYear into g
            //               select new { VintageYear = g.Key, AsOf = g.Max(x=>x.AsOf )});

            //var results = (from b in benchmarkRepository.GetAll()
            //               join l in latest on new {b.VintageYear, b.AsOf} equals new {l.VintageYear, l.AsOf}
            //               select b);

            //return results;

            var benchmarks = benchmarkRepository.GetAll();
            var result = new List<Benchmark>();
            var vintageYears = benchmarks.Select(s => s.VintageYear).Distinct();
            foreach (var vintageYear in vintageYears)
            {
                var latest = benchmarks.Where(w => w.VintageYear == vintageYear).OrderByDescending(o => o.AsOf).Take(1).FirstOrDefault();
                if (latest != null)
                {
                    result.Add(latest);
                }
            }

            return result;
        }

        public IEnumerable<Fund> GetFundsByOrganizationId(Guid id)
        {
            var funds = new List<Fund>();
            var crmFunds = this.serviceDelegate.GetFundsByOrganizationId(id);
            funds.AddRange(crmFunds.Select(f => CrmEntityMapper.FundFromCrmEntity(f, this.serviceDelegate.GetLimitedPartnersByFundId(f.Id))));
            return funds;
        }

        public IEnumerable<int> GetAvailbleFundVintageYears(IEnumerable<Guid> orgIds)
        {
            var vintageYearList = new List<int>();
            var orgIdsList = orgIds.ToList();

            int page = 0, pageSize = 2000, count = 0;
            do
            {
                var orgs = GetAllByIds(orgIdsList.Skip(page * pageSize).Take(pageSize)).ToList();
                count = orgs.Count();

                orgs.Where(w => orgIds.Contains(w.Id))
                    .SelectMany(sm => sm.Funds.Where(w => w.VintageYear != null)
                        .Select(s => Int32.Parse(s.VintageYear))).ToList()
                        .ForEach(vintageYearList.Add);

                page++;
            } while (count == pageSize);

            return vintageYearList;
        }

        public IEnumerable<OrganizationMember> GetAllOrganizationMembers()
        {
            var pageSize = 100;
            var configPageSize = ConfigurationManager.AppSettings["GetAllPageSize"];
            if (!String.IsNullOrWhiteSpace(configPageSize))
            {
                pageSize = Convert.ToInt32(configPageSize);
            }
            var pageNumber = 0;
            var result = new List<OrganizationMember>();
            while (true)
            {
                var members = this.serviceDelegate.GetAllContacts(pageNumber * pageSize, pageSize).ToList();
                var temp = CrmEntityMapper.OrganizationMembersFromCrmEntities(members);
                result.AddRange(temp);
                pageNumber++;
                if (members.Count < pageSize)
                {
                    break;
                }
            }
            return result;
        }

        public IEnumerable<OrganizationMember> GetOrganizationMembersByOrganizationId(Guid id)
        {
            var result = new List<OrganizationMember>();
            var members = this.serviceDelegate.GetContactsByOrganizationId(id);
            return CrmEntityMapper.OrganizationMembersFromCrmEntities(members);
            //foreach (var member in members)
            //{
            //    //var connections = this.serviceDelegate.GetAllConnectionsById(member.Id).ToList();
            //    //result.Add(CrmEntityMapper.OrganizationMemberFromCrmEntity(
            //    //    member,
            //    //    connections.Where(m => m.IsInRole("Board Member")),
            //    //    connections.Where(m => m.IsInRole("Former Employee")),
            //    //    connections.Where(m => m.IsInRole("Graduate"))
            //    //));
            //}
            //return result;
        }

        public IEnumerable<DueDiligenceItem> GetAllDueDiligenceItems()
        {
            var result = new List<DueDiligenceItem>();
            var items = this.serviceDelegate.GetAllDueDiligenceItems();
            var temp = CrmEntityMapper.DueDiligenceItemsFromCrmEntities(items);
            result.AddRange(temp);

            return result;
        }

        public IEnumerable<OtherAddress> GetAllOtherAddresses()
        {
            return CrmEntityMapper.OtherAddressesFromCrmEntities(serviceDelegate.GetAllOtherAddresses());
        }

        public IEnumerable<TrackRecord> GetAllTrackRecords()
        {
            return CrmEntityMapper.TrackRecordsFromCrmEntities(serviceDelegate.GetAllTrackRecords());
        }

        public IEnumerable<Evaluation> GetAllEvaluations()
        {
            var result = new List<Evaluation>();
            var strengths = this.serviceDelegate.GetAllStrengths();
            var concerns = this.serviceDelegate.GetAllConcerns();
            result.AddRange(CrmEntityMapper.EvaluationsFromCrmEntities(strengths));
            result.AddRange(CrmEntityMapper.EvaluationsFromCrmEntities(concerns));
            return result;
        }

        public IEnumerable<Evaluation> GetEvaluationsByOrganizationId(Guid id)
        {
            var result = new List<Evaluation>();
            var strengths = this.serviceDelegate.GetStrengthsByOrganizationId(id);
            var concerns = this.serviceDelegate.GetConcernsByOrganizationId(id);
            result.AddRange(CrmEntityMapper.EvaluationsFromCrmEntities(strengths));
            result.AddRange(CrmEntityMapper.EvaluationsFromCrmEntities(concerns));
            return result;
        }

        public IEnumerable<AdvisoryBoard> GetAllAdvisoryBoards()
        {
            return CrmEntityMapper.AdvisoryBoardFromCrmEntities(serviceDelegate.GetAllAdvisoryBoards());
        }

        public IEnumerable<PortfolioCompany> GetAllPortfolioCompanies()
        {
            var result = new List<PortfolioCompany>();
            var crmEntities = this.serviceDelegate.GetAllPortfolioCompanies();
            result.AddRange(CrmEntityMapper.GetPortfolioCompaniesFromConnections(crmEntities));
            return result;
        }

        public IEnumerable<PortfolioCompany> GetPortfolioCompaniesByOrganizationId(Guid id)
        {
            var result = new List<PortfolioCompany>();
            var crmEntities = this.serviceDelegate.GetPortfolioCompaniesByOrganizationId(id);
            result.AddRange(CrmEntityMapper.GetPortfolioCompaniesFromConnections(crmEntities));
            return result;
        }

        public IEnumerable<BoardSeat> GetAllBoardSeats()
        {
            return CrmEntityMapper.BoardSeatsFromCrmEntities(serviceDelegate.GetAllBoardSeats());
        }

        public IEnumerable<BoardSeat> GetBoardSeatsByOrganizationMemberId(Guid id)
        {
            return CrmEntityMapper.BoardSeatsFromCrmEntities(serviceDelegate.GetBoardSeatsByContactId(id));
        }

        public IEnumerable<Currency> GetAllCurrencies()
        {
            return CrmEntityMapper.CurrenciesFromCrmEntity(serviceDelegate.GetAllCurrencies());
        }

        public IEnumerable<EmploymentHistory> GetAllEmploymentHistories()
        {
            return CrmEntityMapper.EmploymentHistoryFromCrmEntities(serviceDelegate.GetAllEmploymentHistories());
        }

        public IEnumerable<EmploymentHistory> GetEmploymentHistoriesByOrganizationMemberId(Guid id)
        {
            return CrmEntityMapper.EmploymentHistoryFromCrmEntities(serviceDelegate.GetPreviousEmployerByContactId(id));
        }

        public IEnumerable<EducationHistory> GetEducationHistoriesByOrganizationMemberId(Guid id)
        {
            return CrmEntityMapper.EducationHistoryFromCrmEntities(serviceDelegate.GetEducationHistoryByContactId(id));
        }

        public IEnumerable<EducationHistory> GetAllEducationHistories()
        {
            return CrmEntityMapper.EducationHistoryFromCrmEntities(serviceDelegate.GetAllEducationHistories());
        }

        public IList<Fund> GetFundsBasicByOrganizationId(Guid id)
        {
            var funds = new List<Fund>();
            var crmFunds = this.serviceDelegate.GetFundsByOrganizationId(id);
            funds.AddRange(crmFunds.Select(CrmEntityMapper.FundFromCrmEntity));
            return funds;
        }

        public OrganizationMember GetOrganizationMembersById(Guid id)
        {
            var member = this.serviceDelegate.GetById<Contact>(id);
            return CrmEntityMapper.OrganizationMemberFromCrmEntity(member);
        }

        public IList<DueDiligenceItem> GetDueDiligenceItemsByOrganizationId(Guid id)
        {
            var result = new List<DueDiligenceItem>();
            var items = this.serviceDelegate.GetDueDiligenceItemsByOrganizationId(id);
            var temp = CrmEntityMapper.DueDiligenceItemsFromCrmEntities(items);
            result.AddRange(temp);

            return result;
        }

        public Currency GetCurrencyById(Guid? id)
        {
            return CrmEntityMapper.CurrencyFromCrmEntity(!id.HasValue ? null : this.serviceDelegate.GetCurrencyById(id.Value));
        }

        public IEnumerable<OtherAddress> GetOtherAddressesByOrganizationId(Guid id)
        {
            var result = new List<OtherAddress>();
            var items = this.serviceDelegate.GetOtherAddressesByOrganizationId(id);
            var temp = CrmEntityMapper.OtherAddressesFromCrmEntities(items);
            result.AddRange(temp);

            return result;
        }

        public IEnumerable<TrackRecord> GetTrackRecordsByOrganizationId(Guid id)
        {
            var result = new List<TrackRecord>();
            var items = this.serviceDelegate.GetTrackRecordsByOrganizationId(id);
            var temp = CrmEntityMapper.TrackRecordsFromCrmEntities(items);
            result.AddRange(temp);

            return result;
        }

        public IEnumerable<AdvisoryBoard> GetAdvisoryBoardsByFundId(Guid id)
        {
            var boards = this.serviceDelegate.GetAdvisoryBoardByFundId(id);
            var retval = new List<AdvisoryBoard>();
            retval.AddRange(CrmEntityMapper.AdvisoryBoardFromCrmEntities(boards));
            return retval;
        }

        public IEnumerable<Data.Models.Organization> GetComparetablesOrganizationFunds(IEnumerable<Guid> orgIds)
        {
            var orgList = new List<Data.Models.Organization>();
            var orgIdsList = orgIds.ToList();

            int page = 0, pageSize = 2000, count = 0;
            do
            {
                var orgs = GetAllByIds(orgIdsList.Skip(page * pageSize).Take(pageSize)).ToList();
                orgs.Where(w => orgIdsList.Contains(w.Id)).Select(s => new
                {
                    Id = s.Id,
                    Name = s.Name,
                    Funds = s.Funds
                }).ToList().ForEach(fe => orgList.Add(new Data.Models.Organization { Id = fe.Id, Name = fe.Name, Funds = fe.Funds }));

                page++;
            } while (count == pageSize);

            return orgList;
        }

        public IEnumerable<Data.Models.Benchmark> ImportBenchmarks()
        {
            var benchmarks = this.serviceDelegate.GetAllBenchmarks();
            return benchmarks.Select(s => CrmEntityMapper.BenchmarkFromCrmEntity(s)).ToList();
        }
    }
}
