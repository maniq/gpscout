﻿using System;
using AtlasDiligence.Common.DTO.Model;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface IGroupService
    {
        Group GetById(Guid id);
        Guid Create(Group group);
        void Update(Group group);
        void RemoveUserAsGroupLeader(Guid userId);
        void DeleteGroup(Guid groupId);

        /// <summary>
        /// Return Group by name. Will only return first result (dups should be controlled, so there should only be one)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Group GetByName(string name);
    }
}
