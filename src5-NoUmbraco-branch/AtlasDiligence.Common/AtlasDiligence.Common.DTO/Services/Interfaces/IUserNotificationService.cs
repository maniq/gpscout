﻿using AtlasDiligence.Common.DTO.Model.UserNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Services.Interfaces
{
    public interface IUserNotificationService
    {
        IEnumerable<IUserNotification> GetUserNotifications(Guid userId);
        IEnumerable<IUserNotification> GetValidUserNotifications(Guid userId);

        bool Save(IUserNotification notification);
        bool Delete(IUserNotification notification);
        bool Delete(Guid id);
        bool Process(IUserNotification notification);
        IUserNotification GetById(Guid id);
    }
}
