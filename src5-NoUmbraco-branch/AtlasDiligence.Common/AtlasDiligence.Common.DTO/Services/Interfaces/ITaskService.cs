﻿using AtlasDiligence.Common.DTO.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Services.Interfaces
{
    public interface ITaskService
    {
        ITask GetById(Guid id);
        bool Save(ITask task);
        bool Delete(ITask reportTask);
    }
}
