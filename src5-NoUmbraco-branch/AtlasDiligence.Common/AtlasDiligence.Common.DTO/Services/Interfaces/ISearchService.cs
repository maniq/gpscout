using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface ISearchService
    {
        IEnumerable<LuceneSearchResult> GetOrganizationsContainingTerm(IList<Segment> segments, IList<Guid> purchasedOrganizations, string term);
        IEnumerable<Data.Models.Organization> Search(SearchFilter filter, out int total);
        IEnumerable<LuceneSearchResult> GetOrganizations(SearchFilter filter, out int total);

        IEnumerable<LuceneSearchResult> GetOrganizationsMatchOrganizationSegment(IList<Segment> availableSegments,
                                                                                 Guid organizationId);
    }
}