﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Common.DTO.Model.UserNotifications;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace AtlasDiligence.Common.DTO.Services
{
    public class UserNotificationService : IUserNotificationService
    {
        IUserNotificationRepository UserNotificationRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserNotificationRepository>();
            }
        }

        public IEnumerable<IUserNotification> GetUserNotifications(Guid userId)
        {
            foreach (var notification in this.UserNotificationRepository.FindAll(x => !x.UserId.HasValue || x.UserId == userId))
            {
                yield return this.UserNotificationFactory(notification);
            }
        }


        public IEnumerable<IUserNotification> GetValidUserNotifications(Guid userId)
        {
            foreach (var notification in this.UserNotificationRepository
                .FindAll(x => (!x.UserId.HasValue || x.UserId == userId) && x.IsValid))
            {
                yield return this.UserNotificationFactory(notification);
            }
        }

        public bool Save(IUserNotification notification)
        {
            if (notification != null)
                notification.TimeStamp = DateTime.Now;

            return this.UserNotificationRepository.Save(ToModel(notification));
        }

        public bool Delete(IUserNotification notification)
        {
            if (notification != null)
            {
                UserNotification notif = this.UserNotificationRepository.GetById(notification.Id);
                if (notif != null)
                {
                    this.UserNotificationRepository.DeleteOnSubmit(notif);
                    this.UserNotificationRepository.SubmitChanges();
                }
            }
            return true;
        }

        public bool Process(IUserNotification notification)
        {
            if (notification == null)
                return false;

            bool isDirty = notification.Process();
            if (!notification.IsValid)
                this.Delete(notification);
            else if (isDirty)
                this.Save(notification);

            return isDirty;
        }

        #region Helper Methods
        private IUserNotification UserNotificationFactory(UserNotification notification)
        {
            IUserNotification notif = null;
            if (notification != null)
            {
                switch (notification.NotificationType)
                {
                    case UserNotificationType.GenerateOrganizationFullReportPdf:
                        notif = new OrganizationFullReportNotification(notification.Data);
                        break;
                }
                notif.CheckStatusIntervalMS = notification.CheckStatusIntervalMS;
                notif.Hidden = notification.Hidden;
                notif.HideTimeoutMS = notification.HideTimeoutMS;
                notif.Id = notification.Id;
                notif.IsValid = notification.IsValid;
                notif.UserId = notification.UserId;
                notif.DateCreated = notification.DateCreated;
                notif.PollingExpirationDate = notification.PollingExpirationDate;
                notif.ReferenceId = notification.ReferenceId;
                notif.TimeStamp = notification.TimeStamp;
            }
            return notif;
        }

        private UserNotification ToModel(IUserNotification notification)
        {
            UserNotification _notif = null;
            if (notification != null)
            {
                _notif = new UserNotification()
                {
                    Data = notification.SerializeData(),
                    Id = notification.Id,
                    UserId = notification.UserId,
                    CheckStatusIntervalMS = notification.CheckStatusIntervalMS,
                    DateCreated = notification.DateCreated,
                    PollingExpirationDate = notification.PollingExpirationDate,
                    ReferenceId = notification.ReferenceId,
                    Hidden = notification.Hidden,
                    HideTimeoutMS = notification.HideTimeoutMS,
                    IsValid = notification.IsValid,
                    TimeStamp = notification.TimeStamp
                };
                if (notification is OrganizationFullReportNotification)
                    _notif.NotificationType = UserNotificationType.GenerateOrganizationFullReportPdf;
            }
            return _notif;
        }
        #endregion


        public IUserNotification GetById(Guid id)
        {
            var notification = this.UserNotificationRepository.GetById(id);
            return this.UserNotificationFactory(notification);
        }

        public bool Delete(Guid id)
        {
            this.UserNotificationRepository.DeleteAllOnSubmit(x => x.Id == id);
            this.UserNotificationRepository.SubmitChanges();
            return true;
        }
    }
}
