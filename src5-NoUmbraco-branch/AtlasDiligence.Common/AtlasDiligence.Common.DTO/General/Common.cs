﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AtlasDiligence.Common.DTO.General
{
    public static class Common
    {

        /// <summary>
        /// this will return an embedded text file as a string.
        /// </summary>
        /// <param name="asm">The assembly in which the file was embedded.</param>
        /// <param name="resource">The name of the file in the assembly.</param>
        /// <returns>The contents of the specified resource.</returns>
        public static String GetEmbeddedString(this Assembly asm, String resource)
        {
            StreamReader sr = new StreamReader(asm.GetManifestResourceStream(resource));
            return sr.ReadToEnd();
        }
    }
}
