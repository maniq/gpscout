﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.General
{
    public static class ApplicationHelper
    {
        public static void RegisterTypes()
        {
            ComponentBrokerInstance.RegisterType<ITaskRepository, TaskRepository>();
            ComponentBrokerInstance.RegisterType<ITaskService, TaskService>();
            ComponentBrokerInstance.RegisterType<IUserNotificationRepository, UserNotificationRepository>();
            ComponentBrokerInstance.RegisterType<IUserNotificationService, UserNotificationService>();
        }
    }
}
