﻿using log4net;

namespace AtlasDiligence.DiligenceApplication
{
    public class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private static string _logFileName;

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            var searchService = new SearchService();
            searchService.RunInserts();
        }
    }
}
