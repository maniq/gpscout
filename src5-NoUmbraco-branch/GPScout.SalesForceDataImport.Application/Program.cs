﻿using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.DataImport.Contracts.Services;
using GPScout.Domain.Contracts.Models;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using SFP.Infrastructure.Email.Contracts;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.SalesForceDataImport.Application
{
    class Program
    {
        private static string _logFileName;
        private static IEmail _email;
        private static ILog _log;
        private static bool _reindexOnly;

        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.LoadConfiguration();

            container.RegisterType<IRepository<SystemStatus>, Repository<SystemStatus>>();

            IDataImportManager dataImportManager = container.Resolve<IDataImportManager>();
            _email = container.Resolve<IEmail>();
            _log = container.Resolve<ILog>();

            _reindexOnly = args.Contains("-reindexonly");
            bool fullImport = args.Contains("-fullimport");
            bool onlyImportToStage = args.Contains("-onlyimporttostage");
            IResults results = dataImportManager.StartImport(false, fullImport, _reindexOnly, onlyImportToStage);

            foreach (IResult result in results)
            {
                Console.WriteLine(result.Message);
            }
            SendNotifications(results);
        }

        private static void SendNotifications(IResults results)
        {
            try
            {
                string notificationLevel = ConfigurationManager.AppSettings["NotificationLevel"];
                if (!string.IsNullOrEmpty(notificationLevel) && (notificationLevel.ToLower() == "error" && !results.HasError))
                {
                    string msg = string.Format("Notification email not sent because notification level set to '{0}'.", notificationLevel);
                    _log.Write(msg);
                    Console.WriteLine(msg);
                    return;
                }
                StringBuilder sbResults = new StringBuilder();
                foreach (IResult result in results)
                {
                    sbResults.AppendLine(result.Message);
                    sbResults.AppendLine("<br />");
                }

                _email.EmailNotification(
                    string.Format(results.HasError 
                        ? "{0} ERROR: An error occured {1}." 
                        : "{0} SUCCESS: The data have been successfully {2}."
                            , ConfigurationManager.AppSettings["ApplicationName"]
                            , _reindexOnly ? "re-indexing data" : "during the data import"
                            , _reindexOnly ? "re-indexed" : "imported")
                    , sbResults.ToString());
            }
            catch (Exception ex)
            {
                _log.Write(ex);
            }
        }
    }
}
