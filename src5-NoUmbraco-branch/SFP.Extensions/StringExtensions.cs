﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Extensions
{
    public static class StringExtensions
    {
        public static string Intro(this string str, int maxLength, string ellipsis = " (...)")
        {
            if (str.Length <= maxLength) return str;
            string str2 = str.Substring(0, maxLength);
            int n = str2.LastIndexOfAny(new char[] { ' ' });

            if (n >= 0) str2 = str2.Substring(0, n);
            return str2 + ellipsis;
        }

    }
}
