﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Extensions
{
    public static class DataReader
    {
        public static T Get<T>(this DbDataReader dr,  string columnName)
        {
            object value = dr[columnName];
            if (value != DBNull.Value)
                return (T)value;

            return default(T);
        }
    }
}
