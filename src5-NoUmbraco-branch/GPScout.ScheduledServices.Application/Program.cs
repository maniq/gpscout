﻿using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using GPScout.Domain.Implementation.Common;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPScout.ScheduledServices.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.LoadConfiguration();

            ILog _log = container.Resolve<ILog>();
            IResults results = null;

            bool sendProfileViewReportEmails = args.Contains("-sendProfileViewReportEmails");
            if (sendProfileViewReportEmails)
            {
                ApplicationHelper.RegisterTypes();
                _log.Write("-----------------------------------------------------");
                IEmailDistributionListService emailDistributionListService = container.Resolve<IEmailDistributionListService>();
                results = emailDistributionListService.SendWeeklyReportEmails(ConfigurationManager.AppSettings["FromEmail"], null);
                results.LogTo(_log);
            }

            if (results != null)
                foreach (IResult result in results)
                {
                    Console.WriteLine(result.Message);
                }
        }
    }
}
