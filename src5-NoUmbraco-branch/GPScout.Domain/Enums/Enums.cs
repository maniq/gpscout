﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Enums
{
    using System.ComponentModel;

    public enum EvaluationType { Strength, Concern }

    public enum ProductTypes
    {
        [Description("Profile Account")]
        ProfileAccount = 0,

        [Description("Diligence Account")]
        DiligenceAccount = 1 
    }

    public enum OrgPublishLevelType : byte
    {
        Unknown = 0,
        PublishSearch = 1,
        PublishOverviewAndTeam = 2,
        PublishTrackRecord = 3,
        PublishProfile = 4
    }

    public struct GradeRangeLimit
    {
        public const int Ungraded = 5;
    }
}
