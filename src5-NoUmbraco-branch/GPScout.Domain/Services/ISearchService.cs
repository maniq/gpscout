using System;
using System.Collections.Generic;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models;

namespace GPScout.Domain.Contracts.Services
{
    public interface ISearchService
    {
        IEnumerable<LuceneSearchResult> GetOrganizationsContainingTerm(IList<Segment> segments, IList<Guid> purchasedOrganizations, string term);
        IEnumerable<Organization> Search(SearchFilter filter, out int total);
        IEnumerable<Guid> Search(SearchFilter filter);
        IEnumerable<Organization> Search(IEnumerable<Guid> allowedOrganizationIds, SearchFilter filter);
        IEnumerable<LuceneSearchResult> GetOrganizations(SearchFilter filter, out int total);
        IEnumerable<LuceneSearchResult> GetOrganizationsMatchOrganizationSegment(IList<Segment> availableSegments, Guid organizationId);
        IEnumerable<LuceneSearchResult> GetAvailableOrganizations(Guid userId);
        IEnumerable<LuceneSearchResult> GetOrganizationsFromLuceneString(string queryString, out int total, ProximitySearch geoProximitySearch);
        void InsertSearchOptions();
        string BuildQueryString(SearchFilter filter);
    }
}