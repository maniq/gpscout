﻿using GPScout.Domain.Contracts.Models.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface ITaskService
    {
        ITask GetById(Guid id);
        bool Save(ITask task);
        bool Delete(ITask reportTask);
        int GetCountOfRunningTasks(Guid? userI);
    }
}
