﻿using AtlasDiligence.Common.Data;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface IEmailDistributionListService
    {
        IEnumerable<EmailDistributionList> GetAllActiveLists();
        IEnumerable<EmailDistributionListEmail> GetActiveEmailsByListId(int id);
        IResults DeleteEmail(int emailID);
        IResults AddEmail(EmailDistributionListEmail email);
        IResults SendWeeklyReportEmails(string from, DateTime? weekOfDate);
        IResults SendToEmailDistributionList(EmailDistribution emailDistribution, string subject, string body, string from);
        IResults SendOrganizationFullReportPdfQuotaAchieved(string fromEmail, Group userGroup);
    }
}
