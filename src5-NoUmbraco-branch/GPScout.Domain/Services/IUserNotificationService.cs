﻿using GPScout.Domain.Contracts.Models.UserNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface IUserNotificationService
    {
        IEnumerable<IUserNotification> GetUserNotifications(Guid userId);
        IEnumerable<IUserNotification> GetValidUserNotifications(Guid userId);

        bool Save(IUserNotification notification);
        bool Delete(IUserNotification notification);
        bool Delete(Guid id);
        bool Process(IUserNotification notification);
        IUserNotification GetById(Guid id);
    }
}
