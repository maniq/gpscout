﻿using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using SFP.ExcelHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface IUserService
    {
        aspnet_User GetUserById(Guid userId);
        IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate);
        IEnumerable<IDictionary<string, string>> GetExcelData(TrackUsersType trackType, DateTime startDate, DateTime endDate, IEnumerable<string> headers);
        string CreateUserProfileViewTrackingExcel(TrackUsersType type, DateTime startDate, DateTime endDate, string fileName);
    }
}
