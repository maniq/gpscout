﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface IOrganizationService
    {
        string GetFullReportFileName(string instanceId);
        string GetFullReportFileNameFullPath(string instanceId);
        string GetFullReportHeaderFileName(string instanceId);
        string GetFullReportDownloadFileName(string organizationName);
        void RemoveObsoleteDataItems();
        string GetFullReportFooterFileName(string instanceId);
        void FullReportCleanup(string contextId);

        Organization GetById(Guid id);
        void AddOrganizationViewing(Guid userId, Guid organizationId);
        IEnumerable<ClientRequest> GetAllClientRequests();
        IEnumerable<Organization> GetAllByIds(IEnumerable<Guid> ids);
        IEnumerable<Organization> GetAllPositions();
        IEnumerable<Organization> GetAllOrganizations();
        IEnumerable<int> GetAvailbleFundVintageYears(IEnumerable<Guid> orgIds);
        IEnumerable<Benchmark> GetBenchmarks();
        IEnumerable<Organization> GetComparetablesOrganizationFunds(IEnumerable<Guid> orgIds);
        IQueryable<Organization> GetOrganizationsByName(string organizationName);
        Dictionary<Guid, string> GetComparTableOrgsByIds(IEnumerable<Guid> ids);

        // For News Service
        List<KeyValuePair<Guid, List<string>>> GetAllOrgIdAndAliases();

        IQueryable<Organization> SearchByNameWildcard(string term);
    }
}
