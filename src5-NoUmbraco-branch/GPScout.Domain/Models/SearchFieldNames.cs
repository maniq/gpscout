﻿using System;
using System.Linq;
using System.Reflection;
using Lucene.Net.Search;

namespace GPScout.Domain.Contracts.Models
{
    public enum SearchField
    {
		[LuceneField(SortField = SortField.STRING, FieldName = "universalSearch")]
		UniversalSearch,

		#region Organization SearchFields
		
		[LuceneField(SortField = SortField.STRING, FieldName = "isOrganization")]
		IsOrganization,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationId")]
        OrganizationId,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationPublishOverviewAndTeam")]
        OrganizationPublishOverviewAndTeam,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationPublishProfile")]
        OrganizationPublishProfile,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationName")]
        OrganizationName,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationAliases")]
        OrganizationAliases,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationPrimaryOffice")]
        OrganizationPrimaryOffice,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationGeographicFocus")]
        OrganizationGeographicFocus,

        //[LuceneField(SortField = SortField.STRING, FieldName = "organizationCountries")]
        //OrganizationCountries,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationMarketStage")]
        OrganizationMarketStage,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationStrategies")]
        OrganizationStrategies,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationSectorFocus")]
        OrganizationSectorFocus,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationFundraisingStatus")]
        OrganizationFundraisingStatus,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationExpectedNextFundraise")]
        OrganizationExpectedNextFundraise,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationLastFundSize")]
        OrganizationLastFundSize,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationNumberOfFunds")]
        OrganizationNumberOfFunds,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationEmergingManager")]
        OrganizationEmergingManager,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationFocusList")]
        OrganizationFocusList,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationAccessConstrained")]
        OrganizationAccessConstrained,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationDiligenceLevel")]
        OrganizationDiligenceLevel,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationInvestmentRegion")]
        OrganizationInvestmentRegion,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationYearFounded")]
        OrganizationYearFounded,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationLastUpdated")]
        OrganizationLastUpdated,

        //[LuceneField(SortField = SortField.STRING, FieldName = "organizationInvestmentSubRegion")]
        //OrganizationInvestmentSubRegion,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationSubStrategy")]
        OrganizationSubStrategy,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationQualitativeGrades")]
        OrganizationQualitativeGrades,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationQuantitativeGrades")]
        OrganizationQuantitativeGrades,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationSectorSpecialist")]
        OrganizationSectorSpecialist,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationQuantity")]
        OrganizationQuantity,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationQuality")]
        OrganizationQuality,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationCurrency")]
        OrganizationCurrency,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationPosition")]
        OrganizationPosition,

		[LuceneField(SortField = SortField.INT, FieldName = "organizationSBICFund")]
        OrganizationSBICFund,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationFocusRadar")]
        FocusRadar,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationPublishSearch")]
        OrganizationPublishSearch,

		[LuceneField(SortField = SortField.INT, FieldName = "organizationAdvancedSearchFundSize")]
		OrganizationAdvancedSearchFundSize,

        //[LuceneField(SortField = SortField.DOUBLE, FieldName = "organizationAdvancedSearchLatLng")]
        OrganizationAdvancedSearchLatLng,

        [LuceneField(SortField = SortField.STRING, FieldName = "organizationInvestmentRegionSpecialist")]
        OrganizationRegionalSpecialist,

        [LuceneField(SortField = SortField.BYTE, FieldName = "organizationInactiveFirm")]
        OrganizationInactiveFirm,

        [LuceneField(SortField = SortField.INT, FieldName = "organizationCustomSortColumn")]
        OrganizationCustomSortColumn,
        #endregion

        #region Fund SearchFields

        [LuceneField(SortField = SortField.STRING, FieldName = "fundName")]
		FundName,

		[LuceneField(SortField = SortField.STRING, FieldName = "fundGeographicFocus")]
		FundGeographicFocus,

		[LuceneField(SortField = SortField.STRING, FieldName = "fundSectorFocus")]
		FundSectorFocus,

		[LuceneField(SortField = SortField.STRING, FieldName = "fundCurrency")]
		FundCurrency,

		[LuceneField(SortField = SortField.INT, FieldName = "fundTargetSize")]
		FundTargetSize,

		[LuceneField(SortField = SortField.STRING, FieldName = "fundStatus")]
		FundStatus

        #endregion
    }

    public static class SearchFieldExtensions
    {
        public static string GetFieldName(this SearchField searchField)
        {
            var memberInfo = typeof(SearchField).GetMember(searchField.ToString()).FirstOrDefault();
            if (memberInfo != null)
            {
                var attribute = memberInfo.GetCustomAttributes(typeof(LuceneField), false).FirstOrDefault();
                if (attribute != null)
                {
                    return ((LuceneField)attribute).FieldName;
                }
            }
            return string.Empty;
        }

        public static string GetDotNotationSort(this SearchField searchField)
        {
            var memberInfo = typeof(SearchField).GetMember(searchField.ToString()).FirstOrDefault();
            if (memberInfo != null)
            {
                var attribute = memberInfo.GetCustomAttributes(typeof(LuceneField), false).FirstOrDefault();
                if (attribute != null)
                {
                    return ((LuceneField)attribute).DotNotationSort;
                }
            }
            return string.Empty;
        }

        public static LuceneField GetLuceneFieldAttribute(this SearchField searchField)
        {
            var memberInfo = typeof(SearchField).GetMember(searchField.ToString()).FirstOrDefault();
            if (memberInfo != null)
            {
                var attribute = memberInfo.GetCustomAttributes(typeof(LuceneField), false).FirstOrDefault();
                return (LuceneField)attribute;
            }
            return null;
        }

        public static T GetAttribute<T, TEnum>(this TEnum e) where T : System.Attribute
        {
            var memberInfo = typeof(TEnum).GetMember(e.ToString()).FirstOrDefault();
            if (memberInfo != null)
            {
                var attribute = memberInfo.GetCustomAttributes(typeof(T), false).FirstOrDefault();
                return (T)attribute;
            }
            return null;
        }
    }

    public class LuceneField : System.Attribute
    {
        public int SortField { get; set; }
        public string FieldName { get; set; }
        public string DotNotationSort { get; set; }
    }
}
