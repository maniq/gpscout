﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    [Serializable]
    public class ResultMessages
    {
        public string Error { get; set; }
        public string Output { get; set; }

        public bool HasError
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Error);
            }
        }


        public ResultMessages()
        {
            this.Error = this.Output = string.Empty;
        }

    }
}
