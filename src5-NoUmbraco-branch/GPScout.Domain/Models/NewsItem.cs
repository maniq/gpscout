﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace GPScout.Domain.Contracts.Models
{
    public class NewsItem
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public Guid? OrganizationSourceId { get; set; }
        public string OrganizationSourceName { get; set; }
        public DateTime PublishedDate { get; set; }
        public IList<Guid> OrganizationIds { get; set; }
        public IList<Guid> ContactIds { get; set; }
        public DateTime DateCreated { get; set; }

        public NewsItem()
        {
            Id = Guid.NewGuid();
            OrganizationIds = new List<Guid>();
            ContactIds = new List<Guid>();
        }
    }
}
