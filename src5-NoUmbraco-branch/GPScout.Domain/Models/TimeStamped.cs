﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public class TimeStamped : ITimeStamped
    {
        public double Tick { get; set; }

        public double SetTimeStamp()
        {
            Tick = Utility.Utils.GetTicks1970();
            return Tick;
        }
    }
}
