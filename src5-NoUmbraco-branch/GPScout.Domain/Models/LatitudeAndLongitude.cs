﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models
{
    public class LatitudeAndLongitude
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string City { get; set; }
    }
}
