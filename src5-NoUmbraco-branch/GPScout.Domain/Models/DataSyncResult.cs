﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public class DataSyncResult
    {
        public int Inserted { get; set; }
        public int Updated { get; set; }
        public int Deleted { get; set; }
        //public int Total { get; set; }

        public static DataSyncResult operator +(DataSyncResult dataSyncResult1, DataSyncResult dataSyncResult2)
        {
            if (dataSyncResult1 == null) return dataSyncResult2;

            if (dataSyncResult2 == null) return dataSyncResult1;

            dataSyncResult1.Inserted += dataSyncResult2.Inserted;
            dataSyncResult1.Updated += dataSyncResult2.Updated;
            dataSyncResult1.Deleted += dataSyncResult2.Deleted;

            return dataSyncResult1;
        }
    }
}
