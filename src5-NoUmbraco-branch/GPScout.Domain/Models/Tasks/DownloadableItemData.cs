﻿using GPScout.Domain.Contracts.Models.UserNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.Tasks
{
    [Serializable]
    public class DownloadableItemData : NotificationData
    {
        public string GeneratedFileName { get; set; }
        public string DownloadedFileName { get; set; }
        public string ContentType { get; set; }
    }
}
