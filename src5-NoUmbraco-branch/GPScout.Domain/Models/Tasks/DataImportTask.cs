﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models.Tasks
{
    public class DataImportTask : TaskBase<ResultMessages>
    {
        public DataImportTask()
        {
            this.Status = DownloadableItemLifeCycle.Started;
            this.Name = "Data Import";
        }

        public DataImportTask(string data)
        {
            DeserializeData(data);
        }

        public new DownloadableItemLifeCycle Status
        {
            get
            {
                return (DownloadableItemLifeCycle)base.Status;
            }
            set
            {
                base.Status = (int)value;
            }
        }

        public ResultMessages Results
        {
            get { return data; }
            set { data = value; }
        }

    }
}
