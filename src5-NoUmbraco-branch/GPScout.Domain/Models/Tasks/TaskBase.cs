﻿using AtlasDiligence.Common.Data.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.Tasks
{
    public class TaskBase<TData> : ITask
        where TData: new()
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string ReferenceId { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public int Status { get; set; }
        public DateTime DateExpired { get; set; }

        public TaskBase()
        {
            DateCreated = DateTime.Now;
            data = new TData();
        }

        public TaskBase(string data)
        {
            DeserializeData(data);
        }

        public virtual string SerializeData()
        {
            return JsonConvert.SerializeObject(data);
        }

        public virtual void DeserializeData(string data)
        {
            this.data = JsonConvert.DeserializeObject<TData>(data);
            if (this.data == null) this.data = new TData();
        }

        public virtual bool IsCompleted()
        {
            return this.Status == (int)DownloadableItemLifeCycle.GenerationCompleted;
        }

        protected TData data;
    }


    public class TaskBase : ITask
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string ReferenceId { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public int Status { get; set; }
        public DateTime DateExpired { get; set; }

        public TaskBase()
        {
            DateCreated = DateTime.Now;
        }

        public virtual string SerializeData()
        {
            return String.Empty;
        }

        public virtual void DeserializeData(string data)
        { }

        public virtual bool IsCompleted()
        {
            return true;
        }
    }
}
