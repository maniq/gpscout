﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models.Tasks;
using GPScout.Domain.Contracts.Services;
using Newtonsoft.Json;
using System;

namespace GPScout.Domain.Contracts.Models.UserNotifications
{
    public class DataImportNotification : UserNotificationBase
    {
        public NotificationData Data { get; set; }

        public DataImportNotification()
        { }

        public DataImportNotification(string data)
        {
            this.DeserializeData(data);
        }

        public override void DeserializeData(string data)
        {
            NotificationData obj = JsonConvert.DeserializeObject<NotificationData>(data);
            Data = obj ?? new NotificationData();
        }

        public override string SerializeData()
        {
            return JsonConvert.SerializeObject(this.Data);
        }


        public override bool Process()
        {
            bool isDirty = false;
            if (this.IsValid)
            {
                isDirty = true;
                var taskService = ComponentBrokerInstance.RetrieveComponent<ITaskService>();
                ITask task = taskService.GetById(new Guid(this.ReferenceId));
                if (task != null)
                {
                    if (task.IsCompleted())
                    {
                        this.Data.Status = DownloadableItemLifeCycle.GenerationCompleted;
                        this.CheckStatusIntervalMS = this.HideTimeoutMS = 0;
                        this.Hidden = false;
                    }
                    else if (this.Data.Status != DownloadableItemLifeCycle.Error && (DateTime.Now >= this.PollingExpirationDate || task.Status == (int)DownloadableItemLifeCycle.Error))
                    {
                        this.Data.Status = DownloadableItemLifeCycle.Error;
                        this.CheckStatusIntervalMS = this.HideTimeoutMS = 0;
                        this.Hidden = false;
                    }
                    else if (DateTime.Now < this.PollingExpirationDate)
                    {
                        this.Hidden = false;
                        this.Data.Status = task.Status == (int)DownloadableItemLifeCycle.Unknown ? DownloadableItemLifeCycle.Unknown : DownloadableItemLifeCycle.Running;
                        this.CheckStatusIntervalMS = AppSettings.UserNotificationCheckIntervalMS;
                        isDirty = false;
                    }
                    else
                    {
                        IsValid = false;
                    }
                }
                else
                    this.IsValid = false;
            }
            return isDirty;
        }

    }


    //[Serializable]
    //public class DataImportNotificationData : DownloadableItemData
    //{
    //}

}
