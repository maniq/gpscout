﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.UserNotifications
{
    [Serializable]
    public class UserNotificationBase : IUserNotification
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public int CheckStatusIntervalMS { get; set; }
        public DateTime PollingExpirationDate { get; set; }
        public bool Hidden { get; set; }
        public int HideTimeoutMS { get; set; }
        public bool IsValid { get; set; }
        public string ReferenceId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime TimeStamp { get; set; }

        protected UserNotificationBase()
        {
            this.CheckStatusIntervalMS = AppSettings.UserNotificationCheckIntervalMS;
            this.PollingExpirationDate = DateTime.UtcNow.AddSeconds(AppSettings.UserNotificationMaxPollingSec);
            this.HideTimeoutMS = 0;
            this.IsValid = true;
            DateCreated = DateTime.Now;
            TimeStamp = DateCreated;
        }

        public virtual void DeserializeData(string data)
        {}

        public virtual string SerializeData()
        {
            return String.Empty;
        }

        public virtual bool Process()
        {
            return false;
        }
    }
}
