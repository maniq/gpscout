﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models.UserNotifications
{
    [Serializable]
    public class NotificationData
    {
        public DownloadableItemLifeCycle Status { get; set; }
        public string Message { get; set; }
    }
}
