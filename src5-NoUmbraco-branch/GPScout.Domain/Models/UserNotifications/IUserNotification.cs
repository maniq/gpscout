﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.UserNotifications
{
    public interface IUserNotification
    {
        Guid Id { get; set; }
        Guid? UserId { get; set; }
        int CheckStatusIntervalMS { get; set; }
        bool Hidden { get; set; }
        int HideTimeoutMS { get; set; }
        bool IsValid { get; set; }       
        DateTime DateCreated { get; set; }
        DateTime TimeStamp { get; set; }
        DateTime PollingExpirationDate { get; set; }
        string ReferenceId { get; set; }

        void DeserializeData(string data);
        string SerializeData();
        bool Process();
    }
}
