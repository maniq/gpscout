﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models.Tasks;
using GPScout.Domain.Contracts.Services;
using Newtonsoft.Json;
using System;
using System.IO;

namespace GPScout.Domain.Contracts.Models.UserNotifications
{
    [Serializable]
    public class OrganizationFullReportNotification : UserNotificationBase
    {
        public OrganizationFullReportNotificationData Data { get; set; }

        public OrganizationFullReportNotification()
        {}

        public OrganizationFullReportNotification(string data)
        {
            this.DeserializeData(data);
        }

        public override void DeserializeData(string data)
        {
            OrganizationFullReportNotificationData obj = JsonConvert.DeserializeObject<OrganizationFullReportNotificationData>(data);
            Data = obj ?? new OrganizationFullReportNotificationData();
        }

        public override string SerializeData()
        {
            return JsonConvert.SerializeObject(this.Data);
        }

        public override bool Process()
        {
            bool isDirty = false;
            if (this.IsValid)
            {
                isDirty = true;
                var taskService = ComponentBrokerInstance.RetrieveComponent<ITaskService>();
                ITask task = taskService.GetById(new Guid(this.ReferenceId));
                if (task != null)
                {
                    if (task.IsCompleted())
                    {
                        var _task = task as DownloadableItemGenerationTask;
                        this.Data.Status = DownloadableItemLifeCycle.GenerationCompleted;
                        this.Data.ContentType = _task.Data.ContentType;
                        this.Data.DownloadedFileName = _task.Data.DownloadedFileName;
                        this.Data.Title = Path.GetFileNameWithoutExtension(_task.Data.DownloadedFileName);
                        this.Data.GeneratedFileName = _task.Data.GeneratedFileName;
                        this.CheckStatusIntervalMS = this.HideTimeoutMS = 0;
                        this.Hidden = false;
                        string fileName = OrganizationService.GetFullReportFileNameFullPath(this.ReferenceId);
                        if (!File.Exists(fileName)) this.IsValid = false;
                    }
                    else if (this.Data.Status != DownloadableItemLifeCycle.Error && (DateTime.Now >= this.PollingExpirationDate || task.Status == (int)DownloadableItemLifeCycle.Error))
                    {
                        this.Data.Status = DownloadableItemLifeCycle.Error;
                        this.CheckStatusIntervalMS = this.HideTimeoutMS = 0;
                        this.Hidden = false;
                    }
                    else if (DateTime.Now < this.PollingExpirationDate)
                    {
                        this.Hidden = false;
                        this.Data.Status = task.Status == (int)DownloadableItemLifeCycle.Unknown ? DownloadableItemLifeCycle.Unknown : DownloadableItemLifeCycle.Running;
                        this.CheckStatusIntervalMS = AppSettings.UserNotificationCheckIntervalMS;
                        isDirty = false;
                    }
                    else
                    {
                        IsValid = false;
                    }
                }
                else
                    this.IsValid = false;
            }

            if (!this.IsValid)
            {
                OrganizationService.FullReportCleanup(this.ReferenceId);
            }
            return isDirty;
        }


        private IOrganizationService _organizationService;
        private IOrganizationService OrganizationService
        {
            get
            {
                if (_organizationService == null)
                    _organizationService = ComponentBrokerInstance.RetrieveComponent<IOrganizationService>();
                return _organizationService;
            }
        }
    }

    [Serializable]
    public class OrganizationFullReportNotificationData : DownloadableItemData
    {
        public string Title { get; set; }
        public string Password {get; set;}
    }
}
