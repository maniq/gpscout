﻿using ErrorHandling;
using GPScout.Domain.Contracts.Enums;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public interface IResult : IResult<ResultCode, object> {
        string Message { get; }
        void LogTo(ILog log);
    }


    public interface IResults : IResults<ResultCode, Result, object> {
        void Clear();
        new IResult Add(ResultCode code);
        new IResult Add(ResultCode code, object context);
        void Add(IResults results);
        IResult Add(Exception ex);
        IResult Add(ResultCode resultCode, params object[] paramss);
        void LogTo(ILog log);
    };

    public partial class Result: ResultBase<ResultCode, object>, IResult
    {
        public string GetMessage(ResultCode resultCode, params object[] context)
        {
            return string.Format("{0, -7} {1} - {2}", GetResultType(resultCode).ToString().ToUpper(), DateTime.Now, GetMessageInternal(resultCode, context));
        }


        public ResultType GetResultType(ResultCode resultCode)
        {
            if (resultCode >= Enums.ResultCode.INFO) return ErrorHandling.ResultType.Info;
            if (resultCode >= Enums.ResultCode.WARNINGS) return ErrorHandling.ResultType.Warning;
            else return ErrorHandling.ResultType.Error;
        }


        public string GetMessage(ResultCode resultCode, object context = null)
        {
            return GetMessage(resultCode, new object[] { context });
        }


        public string GetMessageInternal(ResultCode resultCode, params object[] context)
        {
            switch (resultCode)
            {
                case ResultCode.ItemNotFound: return "Item not found.";
                case ResultCode.EmailDistributionList_NotFound: return string.Format("{0} email distribution list not found.", context);
                case ResultCode.EmailDistributionList_DuplicateEmail: return string.Format("{0} already exists in {1} email distribution list.", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.Validation_InvalidEmail: return string.Format("{0} is not a valid email address.", context);
                case ResultCode.Dataimport_IsRunningAlready: return "The data import process is running already. Please try again later.";
                case ResultCode.DataImport_Started: return "GPScout data import process started.";
                case ResultCode.DataImport_Starting_Forcibly: return "Called with ForceStart flag set.";
                case ResultCode.DataImport_Starting_Import_Into_GPScout: return "Start data processing...";
                case ResultCode.DataImport_Finished: return "Import into GPScout database finished.";
                case ResultCode.ErrorMsg: return Context != null ? Context.ToString() : "Unknown error.";
                case ResultCode.DataImport_From_Salesforce_Started: return string.Format("Import from Salesforce into GPScout Staging tables started. Data from {0} UTC to {1} UTC ({2} - {3} local time) will be imported.", ((object[])context[0])[0], ((object[])context[0])[1], ((object[])context[0])[2], ((object[])context[0])[3]);
                case ResultCode.DataImport_Salesforce_to_GPScout_Staging_Finished: return "Import from Salesforce into GPScout Staging tables finished.";
                case ResultCode.DataImport_StagingError: return string.Format("An error has occurred importing data into staging tables regarding the {0} entity.", (Context ?? "unknown").ToString());
                case ResultCode.DataImport_EntityNotTouchedInStaging: return string.Format("No {0} data has been touched in Staging.", (Context ?? "unknown").ToString());
                case ResultCode.DataImport_DataWrittenToStaging: return string.Format("{0} {1} entities updated into staging data.\r\n{2} new {1} entities inserted into staging data.", ((object[])context[0])[0], ((object[])context[0])[1], ((object[])context[0])[2]);
                case ResultCode.DataImport_DataWrittenToStagingInsertsOnly: return string.Format("{0}: {1} entities inserted into staging data.", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.DataImport_DataReadFromSource: return string.Format("{1}: {0} entities read from Salesforce.", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.InfoMsg: case ResultCode.WarningMsg: return Context != null ? Context.ToString() : String.Empty;
                case ResultCode.DataImport_ReIndex_Started: return "Lucene re-indexing started.";
                case ResultCode.DataImport_ReIndex_Finished: return "Lucene re-indexing finished.";
                case ResultCode.DataImport_NewImportFromDateSet: return string.Format("New import-from-timestamp set to {0} UTC ({1} local time).", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.DataImport_Inserting_SearchOptions_Started: return "Inserting Search Options started.";
                case ResultCode.DataImport_Inserting_SearchOptions_Finished: return "Inserting Search Options finished.";
                case ResultCode.DataImport_No_Data_Imported_From_Salesforce: return "No data changes detected in Salesforce since last import.";
                case ResultCode.DataImport_FullImport_From_Salesforce_Started: return string.Format("Full import from Salesforce into GPScout Staging tables started. Data before {0} UTC ({1} local time) will be imported.", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.DataImport_Getting_Geocode_Info_Started: return "Getting geocode info...";
                case ResultCode.DataImport_Getting_Geocode_Info_Finished: return "Done.";
                case ResultCode.PUCN_SendingMailsCompleted: return string.Format("{0} PUCN email(s) sent, {1} error(s).", ((object[])context[0])[0], ((object[])context[0])[1]);
                case ResultCode.PUCN_SendingMailsStarted: return "Sending Profile Update Completed Notification emails...";
                case ResultCode.PUCN_MailSent: return string.Format("PUCN mail sent to {0}. Firmlist: {1}", ((object[])context[0])[0], ((object[])context[0])[1]);
            }
            return base.Message;
        }


        public void LogTo(ILog log)
        {
            if (this.Context is Exception)
                log.Write(this.Context as Exception);
            else
                log.Write(this.Message, this.ResultType.ToString());
        }
    }


    // TO DO: Not the final version. Need to re-factor this class 
    public partial class Results : ErrorHandling.ResultSet<ResultCode, Result, object>, IResults
    {
        public void Clear()
        {
            if (base._results != null)
                base._results.Clear();
        }

        public new IResult Add(ResultCode code)
        {
            Result r = Create(code);
            this._results.Add(r);
            return r;
        }

        public void Add(IResults results)
        {
            if (results != null)
                base._results.AddRange((results as Results)._results);
        }


        public IResult Add(Exception ex)
        {
            string msg = ex.ToString();// ex.MessageAll();
            var r = Create(ResultCode.ErrorMsg, msg);
            _results.Add(r);
            return r;
        }


        public new IResult Add(ResultCode code, object context)
        {
            var r = Create(code, context);
            _results.Add(r);
            return r;
        }


        protected Result Create(ResultCode code, object context = null)
        {
            var rs = new Results();
            ((ErrorHandling.ResultSet<ResultCode, Result, object>)rs).Add(code, context);
            Result r = rs._results[0];
            rs._results.RemoveAt(0);
            return r;
        }


        public IResult Add(ResultCode resultCode, params object[] paramss)
        {
            return this.Add(resultCode, (object)paramss);
        }


        public void LogTo(ILog log)
        {
            foreach (IResult result in this)
                result.LogTo(log);
        }
    }
}
