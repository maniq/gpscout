﻿namespace GPScout.Domain.Contracts.Models
{
    using AtlasDiligence.Common.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SearchFilter
    {
        public static readonly string FocusListStr = "Focus List";

        public SearchFilter()
        {
            Segments = new List<Segment>();
            this.Strategies = new List<string>();
            this.PurchasedOrganizationIds = new List<Guid>();
        }

        public IEnumerable<Guid> PurchasedOrganizationIds { get; set; }
        public IEnumerable<Segment> Segments { get; set; }
        public SearchField[] SortFields { get; set; }
        public bool[] IsDescending { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public string Term { get; set; }
        public IList<string> Strategies { get; set; }
        public IList<string> SubStrategies { get; set; }
        public IList<string> MarketStage { get; set; }
        public IList<string> Sector { get; set; }
        public IList<string> InvestmentRegions { get; set; }
        //public IList<string> InvestmentSubRegions { get; set; }
        //public IList<string> Countries { get; set; }
        public IList<string> QualitativeGrades { get; set; }
        public IList<string> QuantitativeGrades { get; set; }
        public IList<string> FundraisingStatus { get; set; }
        public IList<string> Currencies { get; set; } 
        public double? FundSizeMinimum { get; set; }
        public double? FundSizeMaximum { get; set; }
        public int? NumberOfFundsClosedMinimum { get; set; }
        public int? NumberOfFundsClosedMaximum { get; set; }
        public bool? EmergingManager { get; set; }
        public IList<string> FocusList { get; set; }
        public bool? SectorSpecialist { get; set; }
        public bool? RegionalSpecialist { get; set; }
        public bool? AccessConstrained { get; set; }
        public int? DiligenceStatusMinimum { get; set; }
        public int? DiligenceStatusMaximum { get; set; }
        public DateTime? ExpectedNextFundraiseStartDate { get; set; }
        public DateTime? ExpectedNextFundraiseEndDate { get; set; }
        //public bool ShowOnlyPublished { get; set; }
        public bool? SBICFund { get; set; }
        public int? SelectedNextFundraiseValue { get; set; }
        public string FocusRadar { get; set; }
        public ProximitySearch ProximitySearch { get; set; }
        public bool? ShowInactiveFirms { get; set; }
        public string FirmFilter { get; set; }
    }
}
