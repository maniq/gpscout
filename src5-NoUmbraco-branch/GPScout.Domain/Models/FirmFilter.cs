﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public struct FirmFilter
    {
        public static string All = "all";
        public static string InMarket = "inmarket";
        public static string RecentlyUpdated = "recentlyupdated";
        public static string FocusList = "focuslist";
    }
}
