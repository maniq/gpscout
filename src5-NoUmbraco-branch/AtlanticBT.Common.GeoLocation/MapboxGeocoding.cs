﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AtlanticBT.Common.GeoLocation
{
    //[DataContract]
    public class MapboxGeocodingResponse
    {
        public MapboxGeocodingFeature[] features { get; set; }

        internal GeocodeComponents ToGeocodeComponents()
        {
            var result = new GeocodeComponents();

            if (features != null) {
                decimal[] coords = GetCoords(/*"address"*/); // ?? GetCoords("place") ?? GetCoords("region");
                if (coords != null)
                {
                    result.Status = "OK";
                    result.Results.Add(new GeocodeAddress
                    {
                        Geometry = new Geometry
                        {
                            Location = new Location
                            {
                                Latitude = coords[1],
                                Longitude = coords[0]
                            }
                        }
                    });
                }
            }
            return result;
        }


        decimal[] GetCoords()
        {
            foreach (MapboxGeocodingFeature item in features)
            {
                if (item != null && item.type == "Feature" && item.geometry != null && item.geometry.type == "Point" && item.center != null && item.center.Count() > 1 /*&& item.place_type != null*/)
                {
                    return item.center;
                    //foreach (string _placeType in item.place_type)
                    //{
                    //    if (_placeType == placeType)
                    //        return item.center;
                    //}
                }
            }
            return null;
        }

    }


    //[DataContract]
    public class MapboxGeocodingFeature
    {
        public string type { get; set; }
        public MapboxGeocodingGeometry geometry { get; set; }
        public decimal[] center { get; set; }
        public string [] place_type { get; set; }
        public string place_name { get; set; }
        public string text { get; set; }
    }

    public class MapboxGeocodingGeometry
    {
        public string type { get; set; }
    }

}