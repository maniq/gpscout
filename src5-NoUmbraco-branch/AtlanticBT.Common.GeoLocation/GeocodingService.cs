﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web;

namespace AtlanticBT.Common.GeoLocation
{
    public class GeocodingService : IGeocodingService
    {
        #region IGeocodingService Members

        /// <summary>
        /// Gets the full address information from Mapbox Geocoding API
        /// </summary>
        /// <param name="address">
        /// </param>
        /// <returns>
        /// </returns>
        public GeocodeComponents GetAddress(string address)
        {
            if (string.IsNullOrWhiteSpace(address))
            {
                throw new ArgumentNullException("address");
            }

            try
            {
                GeocodeComponents retval = null;
                using (var client = new WebClient())
                using (var stream = client.OpenRead("https://api.mapbox.com/geocoding/v5/mapbox.places/" +
                                System.Web.HttpUtility.UrlEncode(address) + ".json?access_token=pk.eyJ1IjoibXBsdW5rZXR0IiwiYSI6ImNpdnR5cGYzejAwOWQyenJxaWl6YzM3aWMifQ.yfSRdBlZZHsGEXtqVQQlSQ"))
                {
                    if (stream == null)
                    {
                        throw new NullReferenceException("invalid stream from Mapbox api");
                    }
                    var result = (MapboxGeocodingResponse)new DataContractJsonSerializer(typeof(MapboxGeocodingResponse)).ReadObject(stream);
                    if (result != null) retval = result.ToGeocodeComponents();
                }
                return retval;
            }
            catch (WebException ex)
            {
                throw new Exception("GetAddress threw a WebException", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("GetAddress threw an Exception", ex);
            }
        }

        /// <summary>
        /// Returns a tuple of latitude then longitude
        /// </summary>
        /// <returns></returns>
        public Tuple<decimal, decimal> GetGeoCode(string address, string address2, string city, string state, string zip)
        {
            var geocodeResult = GetAddress(string.Format("{0} {1}, {2}, {3} {4}", address, address2, city, state, zip));
            if (geocodeResult != null && geocodeResult.Results.Any())
            {
                var result = geocodeResult.Results.FirstOrDefault();
                return new Tuple<decimal, decimal>(
                    decimal.Round(result.Geometry.Location.Latitude, 5),
                    decimal.Round(result.Geometry.Location.Longitude, 5));
            }

            return null;
        }

        #endregion
    }
}
