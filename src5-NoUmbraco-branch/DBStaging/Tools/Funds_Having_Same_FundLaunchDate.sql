﻿SELECT o.Name FirmName, o.PublishLevelAsImported
, f.FundLaunchDate, COUNT(*) [Number of Funds Having Same FundLaunchDate]
FROM Organization o
JOIN Fund f on f.OrganizationId = o.id
GROUP BY O.ID, o.Name, f.FundLaunchDate, o.PublishLevelAsImported
HAVING COUNT(*)>1
ORDER BY 1