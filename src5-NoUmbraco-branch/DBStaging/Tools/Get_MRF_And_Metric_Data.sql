﻿
SELECT Name, ID, GPS_Current_Record_Date__c FROM Account where name like '%Revel%'

SELECT Id, GPScoutMetricSource__c, AIMPortMgmt__Effective_Date__c FROM AIMPortMgmt__Investment_Metric__c
where AIMPortMgmt__Fund__c = 'a08A000000tXI9EIAW'

--===========================================================
SELECT f2.Id FundId, f2.OrganizationID,
			CASE 
				WHEN f2.FundLaunchDate IS NOT NULL THEN f2.FundLaunchDate
				WHEN f2.VintageYear IS NOT NULL THEN CONVERT(datetime, f2.VintageYear)
				ELSE NULL
			END FundDate
			INTO #FundDates
			FROM Fund f2

SELECT O.Name, FundDates.OrganizationId, FundDates.FundDate, FundDates.FundId, fm.MetricSource
--, COUNT(*)
FROM (
	SELECT OrganizationId, MAX(FundDate) FundDate
	FROM #FundDates FindDates
	GROUP BY OrganizationID
) GroupedFundDates
JOIN #FundDates FundDates ON FundDates.OrganizationId = GroupedFundDates.OrganizationId AND GroupedFundDates.FundDate = FundDates.FundDate
JOIN Organization o ON o.ID = FundDates.OrganizationId
JOIN Fund F ON f.ID = FundDates.FundID
JOIN (
	SELECT  f2.FundId, f2.Id FundMetricID, f2.Source MetricSource,
	CASE 
		WHEN f2.source = 'preqin' THEN f2.PreqinAsOf
		WHEN f2.source LIKE 'RCP%' THEN f2.AsOf
		ELSE NULL
	END MetricDate
	FROM FurtherFundMetrics f2
) fm ON fm.FundId = FundDates.FundId AND fm.MetricDate = o.GPScoutCurrentRecordDate AND f.Status NOT IN ('open','Pre-Marketing')
--GROUP BY FundDates.OrganizationID, FundDates.FundDate
--HAVING COUNT(*)>1
Where MetricSource != 'Preqin'
ORDER BY o.Name

select * from fund f where f.id not in (SELECT fm.FundID from FurtherFundMetrics fm)

select o.Name, f.Name, f.FundLaunchDate, f.Source, f.PerformanceDatasource, f.Vintageyear
 from #FundDates f1
JOIN Fund f ON f.ID=f1.FundId
JOIN Organization o ON o.Id=f.OrganizationID
where f1.OrganizationId IN ('2E49E69B-6C2F-44EB-9CAD-6329F596C99C')
order by FundDate desc

DROP TABLE #FundDates

