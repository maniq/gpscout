ALTER TABLE SearchTemplate ADD ShowOnMap bit NULL
ALTER TABLE Task ADD DateExpired datetime NOT NULL

ALTER TABLE TrackRecord ADD [TopGraphDisplayOrder] int NULL
ALTER TABLE TrackRecord ADD [PublishDetail] int NULL

ALTER TABLE dbo.OrganizationRequest
ALTER COLUMN UserId uniqueidentifier NULL

ALTER TABLE dbo.OrganizationRequest
DROP COLUMN [RequestTypeDisplayName]

ALTER TABLE dbo.OrganizationRequest
ADD [RequestTypeDisplayName] AS (case [Type] when (2) then 'Research' when (3) then 'Introduction' when (4) then 'Restricted GP Access' when (1) then 'Priority' when (5) then 'Profile Update Completed' when (6) then 'Need Help' when (0) then 'Queue' else '' end)

/****** Object:  Table [dbo].[RequestAdditionalInfo]    Script Date: 8/23/2017 5:06:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RequestAdditionalInfo](
	[Id] [uniqueidentifier] NOT NULL,
	[UserEmail] [nvarchar](320) NULL,
 CONSTRAINT [PK_RequestAdditionalInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RequestAdditionalInfo]  WITH CHECK ADD  CONSTRAINT [FK_RequestAdditionalInfo_OrganizationRequest] FOREIGN KEY([Id])
REFERENCES [dbo].[OrganizationRequest] ([Id])
GO

ALTER TABLE [dbo].[RequestAdditionalInfo] CHECK CONSTRAINT [FK_RequestAdditionalInfo_OrganizationRequest]
GO


--=======================================================
--=======================================================
ALTER TABLE dbo.Fund ADD PersonnelSeniorProfessionals int NULL
ALTER TABLE dbo.Fund ADD PersonnelMidLevelProfessionals int NULL
ALTER TABLE dbo.Fund ADD PersonnelJuniorLevelProfessionals int NULL
ALTER TABLE dbo.Fund ADD PersonnelOperatingPartnerProfessionals int NULL
ALTER TABLE dbo.Fund ADD FundLaunchDate datetime NULL
ALTER TABLE dbo.Fund ADD PersonnelTotalDealProfessionals int NULL

--==========================================================================
--==========================================================================
ALTER TABLE Organization ADD GPScoutLevelCompleted nvarchar(50) NULL
ALTER TABLE Organization ADD LevelOfDisclosure nvarchar(50) NULL

ALTER TABLE Grade ADD FirmWeightedGrade float NULL
ALTER TABLE Grade ADD ProcessWeightedGrade float NULL
ALTER TABLE Grade ADD StrategyWeightedGrade float NULL
ALTER TABLE Grade ADD TeamWeightedGrade float NULL
ALTER TABLE Grade ADD PerformanceWeightedGrade float NULL
ALTER TABLE Grade ADD TrackRecordWeightedGrade float NULL
ALTER TABLE Grade ADD PerformanceConsistencyWeightedGrade float NULL
--==========================================================================
--==========================================================================
ALTER TABLE OrganizationMember ADD [PublishDetail] int NULL
--==========================================================================
--==========================================================================

ALTER TABLE Organization ADD GPScoutCurrentRecordDate datetime NULL

ALTER TABLE [dbo].[FurtherFundMetrics] DROP CONSTRAINT [IX_FurtherFundMetrics_FundId]
GO
CREATE NONCLUSTERED INDEX [IX_FurtherFundMetrics_FundId] ON [dbo].[FurtherFundMetrics]
(
	[FundId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
--==
--==
ALTER TABLE dbo.Fund ADD PersonnelSourcingProfessionals int NULL
GO
--==
--==
ALTER TABLE dbo.Fund ADD
	SourceAwareTvpi AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN Moic 
		WHEN Source LIKE 'RCP' THEN Tvpi 
		ELSE PreqinTvpi 
	END PERSISTED 
GO

ALTER TABLE dbo.Fund ADD
	SourceAwareIrr AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN GrossIrr 
		WHEN Source LIKE 'RCP' THEN Irr 
		ELSE PreqinIrr 
	END PERSISTED 
GO

ALTER TABLE dbo.Fund ADD
	SourceAwareDpi AS CASE 
		WHEN Source LIKE 'Preqin' THEN PreqinDpi 
		ELSE Dpi
	END PERSISTED 
GO

-- =================================================
ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareTvpi AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN Moic 
		WHEN Source LIKE 'RCP' THEN Tvpi 
		ELSE PreqinTvpi 
	END PERSISTED 
GO

ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareIrr AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN GrossIrr 
		WHEN Source LIKE 'RCP' THEN Irr 
		ELSE PreqinIrr 
	END PERSISTED 
GO

ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareDpi AS CASE 
		WHEN Source LIKE 'Preqin' THEN PreqinDpi 
		ELSE Dpi
	END PERSISTED 
GO
--==
--==
ALTER TABLE dbo.Organization DROP COLUMN GPScoutPhaseCompleted
--==
ALTER TABLE dbo.Organization ADD IsInProcess bit NOT NULL DEFAULT 0
