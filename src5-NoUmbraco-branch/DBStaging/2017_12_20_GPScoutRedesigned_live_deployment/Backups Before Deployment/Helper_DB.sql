IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_TruncateStagingTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_TruncateStagingTables]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostImport_FirmAddresses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_PostImport_FirmAddresses]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_PostImport]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostFullImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_PostFullImport]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportTrackRecords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportTrackRecords]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportStrenghtsAndConcerns]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportStrenghtsAndConcerns]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportHelper]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportHelper]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportGPScoutScorecards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportGPScoutScorecards]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFurtherFundMetrics_2016_03_12]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFurtherFundMetrics_2016_03_12]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFurtherFundMetrics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFurtherFundMetrics]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFunds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFunds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFundMetrics_2016_03_12]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFundMetrics_2016_03_12]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFundMetrics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFundMetrics]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFirmAddresses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportFirmAddresses]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportContacts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportContacts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportBenchmarks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportBenchmarks]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportAccounts_2017_05_09_before_Import_field_and_adv_search_changes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportAccounts_2017_05_09_before_Import_field_and_adv_search_changes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportAccounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_ImportAccounts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_GetEntityImportConfig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_GetEntityImportConfig]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_GetAdditionalAccountIdsClause]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_GetAdditionalAccountIdsClause]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_DeleteSFData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SF_DeleteSFData]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecords]') AND name = N'NonClusteredIndex-20150514-003440')
DROP INDEX [NonClusteredIndex-20150514-003440] ON [dbo].[TrackRecords]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[StrengthsAndConcerns]') AND name = N'NonClusteredIndex-20150514-003401')
DROP INDEX [NonClusteredIndex-20150514-003401] ON [dbo].[StrengthsAndConcerns]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GPScoutScorecards]') AND name = N'NonClusteredIndex-20150514-003127')
DROP INDEX [NonClusteredIndex-20150514-003127] ON [dbo].[GPScoutScorecards]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND name = N'NonClusteredIndex-20150514-005259')
DROP INDEX [NonClusteredIndex-20150514-005259] ON [dbo].[Funds]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND name = N'NonClusteredIndex-20150514-003027')
DROP INDEX [NonClusteredIndex-20150514-003027] ON [dbo].[Funds]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FundMetrics]') AND name = N'NonClusteredIndex-20150513-185321')
DROP INDEX [NonClusteredIndex-20150513-185321] ON [dbo].[FundMetrics]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FirmAddreses]') AND name = N'NonClusteredIndex-20150514-002854')
DROP INDEX [NonClusteredIndex-20150514-002854] ON [dbo].[FirmAddreses]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contacts]') AND name = N'NonClusteredIndex-20150514-002810')
DROP INDEX [NonClusteredIndex-20150514-002810] ON [dbo].[Contacts]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND name = N'NonClusteredIndex-20150514-023124')
DROP INDEX [NonClusteredIndex-20150514-023124] ON [dbo].[Accounts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecords]') AND type in (N'U'))
DROP TABLE [dbo].[TrackRecords]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StrengthsAndConcerns]') AND type in (N'U'))
DROP TABLE [dbo].[StrengthsAndConcerns]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MergeInternalEntities]') AND type in (N'U'))
DROP TABLE [dbo].[MergeInternalEntities]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ids]') AND type in (N'U'))
DROP TABLE [dbo].[Ids]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GPScoutScorecards]') AND type in (N'U'))
DROP TABLE [dbo].[GPScoutScorecards]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND type in (N'U'))
DROP TABLE [dbo].[Funds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FundMetrics]') AND type in (N'U'))
DROP TABLE [dbo].[FundMetrics]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FirmAddreses]') AND type in (N'U'))
DROP TABLE [dbo].[FirmAddreses]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contacts]') AND type in (N'U'))
DROP TABLE [dbo].[Contacts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks_Last]') AND type in (N'U'))
DROP TABLE [dbo].[Benchmarks_Last]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND type in (N'U'))
DROP TABLE [dbo].[Benchmarks]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
DROP TABLE [dbo].[Accounts]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_yn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_yn]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_ToPipeDelimitedValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_ToPipeDelimitedValue]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_Split]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetQuartile]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_GetQuartile]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetGradeWord]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_GetGradeWord]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetGrade]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[udf_GetGrade]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetGrade]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[udf_GetGrade]
(
	@CheckmarkNumber NVARCHAR(50)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN CASE 
		WHEN @CheckmarkNumber = ''1'' THEN ''Poor''
		WHEN @CheckmarkNumber = ''2'' THEN ''Weak''
		WHEN @CheckmarkNumber = ''3'' THEN ''Standard''
		WHEN @CheckmarkNumber = ''4'' THEN ''Strong''
		WHEN @CheckmarkNumber = ''5'' THEN ''Exceptional''
		ELSE @CheckmarkNumber
	END

END

' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetGradeWord]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		SFP ZK
-- Create date: 5/25/2015
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[udf_GetGradeWord]
(
	@GradeNumber decimal(18,2)
)
RETURNS nvarchar(50)
AS
BEGIN
	RETURN CASE 
		WHEN @GradeNumber = 5 THEN ''Ungraded''
		WHEN @GradeNumber > 5 AND @GradeNumber < 30 THEN ''Poor''
		WHEN @GradeNumber >= 30 AND @GradeNumber < 50 THEN ''Weak''
		WHEN @GradeNumber >= 50 AND @GradeNumber < 70 THEN ''Standard''
		WHEN @GradeNumber >= 70 AND @GradeNumber < 90 THEN ''Strong''
		WHEN @GradeNumber >= 90 AND @GradeNumber <= 100 THEN ''Exceptional''
		ELSE NULL
	END
END
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_GetQuartile]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[udf_GetQuartile] 
(
	@Value decimal(18,2),
	@First decimal(18,2),
	@Median decimal(18,2),
	@Third decimal(18,2)
)
RETURNS varchar(9)
AS
BEGIN
	IF @Value >= @First
		RETURN ''1st''
	ELSE IF @Value >= @Median
		RETURN ''2nd''
	ELSE IF @Value >= @Third
		RETURN ''3rd''
	ELSE IF @Value < @Third
		RETURN ''4th''

	RETURN ''N/A''
END


' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_Split]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[udf_Split] 
(
    @List nvarchar(max),
	@delimiter nvarchar
)
RETURNS @Table TABLE (Value nvarchar(max))
AS
BEGIN
    IF RIGHT(@List, 1) <> @delimiter
    SELECT @List = @List + @delimiter

    DECLARE @Pos int, @OldPos int, @n int = LEN(@List)
    SELECT  @Pos = 1, @OldPos = 1

    WHILE   @Pos < @n BEGIN
            SELECT  @Pos = CHARINDEX(@delimiter, @List, @OldPos)
            INSERT INTO @Table (Value)
            VALUES (SUBSTRING(@List, @OldPos, @Pos - @OldPos))

            SELECT  @OldPos = @Pos + 1
    END

    RETURN
END' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_ToPipeDelimitedValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[udf_ToPipeDelimitedValue]
(
	@val nvarchar(max),
	@delimiter nvarchar
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @result nvarchar(max)=''''

	SELECT @result = @result + ''|'' + Value
	FROM [dbo].[udf_Split](@val, @delimiter)
	WHERE Value != '''' AND CHARINDEX(''--'', Value)=0
		
	RETURN SUBSTRING(@result, 2, LEN(@result))
END
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udf_yn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[udf_yn]
(
	@value varchar(90)
)
RETURNS varchar(90)
AS
BEGIN
	RETURN CASE
		WHEN @value IN (''1'', ''true'') THEN ''Yes''
		WHEN @value IN (''0'', ''false'') THEN ''No''
		ELSE @value
	END
END
' 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Accounts](
	[Id] [varchar](18) NOT NULL,
	[Website] [nvarchar](255) NULL,
	[Most_Recent_Fund_Industry_Focus__c] [nvarchar](1300) NULL,
	[YearGPFounded__c] [nvarchar](4) NULL,
	[Most_Recent_Fund_Eff_Size__c] [decimal](18, 2) NULL,
	[Alias__c] [nvarchar](250) NULL,
	[AUM_Calc__c] [decimal](18, 2) NULL,
	[Emerging_Manager__c] [bit] NULL,
	[Co_Investment_Opportunities_for_Non_LPs__c] [nvarchar](50) NULL,
	[Overview__c] [nvarchar](max) NULL,
	[Investment_Thesis__c] [nvarchar](max) NULL,
	[Team_Overview__c] [nvarchar](max) NULL,
	[Key_Takeaway__c] [nvarchar](max) NULL,
	[Grade_Qualitative_Text__c] [nvarchar](max) NULL,
	[Grade_Quantitative_Text__c] [nvarchar](max) NULL,
	[Grade_Scatterchart_Text__c] [nvarchar](max) NULL,
	[Track_Record_Text__c] [nvarchar](max) NULL,
	[Evaluation_Text__c] [nvarchar](max) NULL,
	[Total_Closed_Funds__c] [decimal](5, 0) NULL,
	[Most_Recent_Fund_Market_Stage__c] [nvarchar](1300) NULL,
	[Region__c] [nvarchar](255) NULL,
	[Industry_Focus__c] [nvarchar](255) NULL,
	[Expected_Next_Fundraise__c] [date] NULL,
	[Co_Investment_Opportunities_for_Fund_LP__c] [nvarchar](50) NULL,
	[Firm_History__c] [nvarchar](max) NULL,
	[Most_Recent_Fund_Region__c] [nvarchar](1300) NULL,
	[Most_Recent_Fund_Regional_Specialist__c] [nvarchar](9) NULL,
	[Most_Recent_Fund_Primary_Strategy__c] [nvarchar](1300) NULL,
	[Publish_Level__c] [nvarchar](255) NULL,
	[Most_Recent_Fund_Secondary_Strategy__c] [nvarchar](1300) NULL,
	[GPScout_Last_Updated__c] [date] NULL,
	[Access_Constrained_Firm__c] [bit] NULL,
	[Name] [nvarchar](255) NULL,
	[ParentId] [varchar](18) NULL,
	[BillingCountry] [nvarchar](80) NULL,
	[Most_Recent_Fund_SBIC__c] [nvarchar](9) NULL,
	[Most_Recent_Fund_is_Sector_Specialist__c] [nvarchar](9) NULL,
	[Most_Recent_Fund_Sub_Region__c] [nvarchar](1300) NULL,
	[IsDeleted] [bit] NULL,
	[Most_Recent_Fund_Currency__c] [nvarchar](1300) NULL,
	[Most_Recent_Fund_Fundraising_Status__c] [nvarchar](1300) NULL,
	[MostRecentScorecardSFID] [varchar](18) NULL,
	[MostRecentFundCountries] [nvarchar](max) NULL,
	[GPScout_Phase_Assigned__c] [nvarchar](255) NULL,
	[GPScout_Phase_Completed__c] [nvarchar](255) NULL,
	[FirmGrade__c] [nvarchar](255) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[NewOrganizationId] [uniqueidentifier] NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Benchmarks](
	[Id] [varchar](18) NOT NULL,
	[Effective_Date__c] [date] NULL,
	[IRR_Median__c] [decimal](18, 2) NULL,
	[TVPI_Median__c] [decimal](18, 2) NULL,
	[DPI_Median__c] [decimal](18, 2) NULL,
	[IRR_Upper_Quartile__c] [decimal](18, 2) NULL,
	[TVPI_Upper_Quartile__c] [decimal](18, 2) NULL,
	[DPI_Upper_Quartile__c] [decimal](18, 2) NULL,
	[IRR_Lower_Quartile__c] [decimal](18, 2) NULL,
	[TVPI_Lower_Quartile__c] [decimal](18, 2) NULL,
	[DPI_Lower_Quartile__c] [decimal](18, 2) NULL,
	[Provided_By__c] [nvarchar](50) NULL,
	[Vintage_Year__c] [nvarchar](1300) NULL,
	[IsDeleted] [bit] NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Benchmarks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks_Last]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Benchmarks_Last](
	[Id] [uniqueidentifier] NOT NULL,
	[VintageYear] [int] NULL,
	[AsOf] [datetime] NULL,
	[DpiFirstQuartile] [decimal](18, 2) NULL,
	[DpiMedian] [decimal](18, 2) NULL,
	[DpiThirdQuartile] [decimal](18, 2) NULL,
	[NetIrrFirstQuartile] [decimal](18, 2) NULL,
	[NetIrrMedian] [decimal](18, 2) NULL,
	[NetIrrThirdQuartile] [decimal](18, 2) NULL,
	[TvpiFirstQuartile] [decimal](18, 2) NULL,
	[TvpiMedian] [decimal](18, 2) NULL,
	[TvpiThirdQuartile] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Benchmarks_Last] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Contacts](
	[Id] [varchar](18) NOT NULL,
	[AccountId] [varchar](18) NULL,
	[Phone] [nvarchar](40) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Year_Started_With_Current_Firm__c] [decimal](18, 0) NULL,
	[Bio__c] [nvarchar](max) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[MailingCountry] [nvarchar](80) NULL,
	[IsDeleted] [bit] NULL,
	[Publish_Contact__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](80) NULL,
	[Display_Order__c] [decimal](9, 0) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FirmAddreses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FirmAddreses](
	[Id] [varchar](18) NOT NULL,
	[Account__c] [varchar](18) NULL,
	[Street_Address__c] [nvarchar](180) NULL,
	[City__c] [nvarchar](50) NULL,
	[State__c] [nvarchar](50) NULL,
	[Country__c] [nvarchar](50) NULL,
	[Address_Description__c] [nvarchar](255) NULL,
	[Phone__c] [nvarchar](40) NULL,
	[Fax__c] [nvarchar](40) NULL,
	[Zip_Code__c] [nvarchar](20) NULL,
	[Address_2__c] [nvarchar](100) NULL,
	[IsDeleted] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[Firm_Location_Lat] [decimal](14, 9) NULL,
	[Firm_Location_Long] [decimal](14, 9) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Firm_Location] [geography] NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_FirmAddreses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FundMetrics]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FundMetrics](
	[Id] [varchar](18) NOT NULL,
	[AIMPortMgmt__Effective_Date__c] [date] NULL,
	[Preqin_Called__c] [decimal](16, 2) NULL,
	[Preqin_DPI__c] [decimal](16, 2) NULL,
	[Preqin_TVPI__c] [decimal](12, 2) NULL,
	[Preqin_Net_IRR__c] [decimal](10, 2) NULL,
	[Preqin_Fund_Size__c] [decimal](12, 2) NULL,
	[AIMPortMgmt__Account__c] [varchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Publish_Metric__c] [bit] NULL,
	[ParentAccount_Id] [varchar](18) NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[Invested_Capital__c] [decimal](18, 1) NULL,
	[Realized_Value__c] [decimal](18, 2) NULL,
	[Unrealized_Value__c] [decimal](18, 2) NULL,
	[Total_Value__c] [decimal](18, 2) NULL,
	[Fund_Total_IRR__c] [decimal](12, 2) NULL,
	[Fund_Total_Cash_on_Cash__c] [decimal](5, 2) NULL,
	[DPI__c] [decimal](18, 2) NULL,
	[Fund_Net_IRR__c] [decimal](10, 2) NULL,
	[Fund_Net_Cash_on_Cash__c] [decimal](7, 2) NULL,
	[AIMPortMgmt__Fund__c] [varchar](18) NULL,
	[ParentFund_IsDeleted] [bit] NULL,
	[ParentFund_Publish__c] [bit] NULL,
	[Public_Metric_Source__c] [varchar](50) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
	[IsMostRecent] [bit] NULL,
	[ImportStep] [tinyint] NULL,
 CONSTRAINT [PK_FundMetrics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Funds](
	[Id] [varchar](18) NOT NULL,
	[Name] [nvarchar](80) NULL,
	[AIM__Account__c] [varchar](18) NULL,
	[Currency__c] [nvarchar](255) NULL,
	[Fundraising_Status__c] [nvarchar](255) NULL,
	[First_Drawn_Capital__c] [date] NULL,
	[Fund_Total_Cash_on_Cash__c] [decimal](18, 2) NULL,
	[Fund_Total_IRR__c] [decimal](18, 2) NULL,
	[Total_Deal_Professionals__c] [nvarchar](1300) NULL,
	[FundTargetSize__c] [decimal](9, 2) NULL,
	[FundNumber__c] [decimal](3, 0) NULL,
	[FundLaunchDate__c] [date] NULL,
	[Vintage_Year__c] [nvarchar](50) NULL,
	[Eff_Size__c] [decimal](18, 1) NULL,
	[Deal_Table_URL__c] [nvarchar](255) NULL,
	[SBIC__c] [nvarchar](9) NULL,
	[Preqin_Fund_Name__c] [nvarchar](100) NULL,
	[Effective_Date__c] [datetime2](7) NULL,
	[IsDeleted] [bit] NULL,
	[Publish__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[MostRecentMetricSFID] [varchar](18) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[NewFundId] [uniqueidentifier] NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Funds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GPScoutScorecards]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GPScoutScorecards](
	[Id] [varchar](18) NOT NULL,
	[GPScout_Scorecard__c] [varchar](18) NULL,
	[Expertise_Aligned_with_Strategy__c] [nvarchar](255) NULL,
	[History_Together__c] [nvarchar](255) NULL,
	[Complementary_Skills__c] [nvarchar](255) NULL,
	[Team_Depth__c] [nvarchar](255) NULL,
	[Value_Add_Resources__c] [nvarchar](255) NULL,
	[Investment_Thesis__c] [nvarchar](255) NULL,
	[Competitive_Advantage__c] [nvarchar](255) NULL,
	[Portfolio_Construction__c] [nvarchar](255) NULL,
	[Appropriateness_of_Fund_Size__c] [nvarchar](255) NULL,
	[Consistency_of_Strategy__c] [nvarchar](255) NULL,
	[Sourcing__c] [nvarchar](255) NULL,
	[Due_Diligence__c] [nvarchar](255) NULL,
	[Decision_Making__c] [nvarchar](255) NULL,
	[Deal_Execution_Structure__c] [nvarchar](255) NULL,
	[Post_Investment_Value_add__c] [nvarchar](255) NULL,
	[Team_Stability__c] [nvarchar](255) NULL,
	[Ownership_and_Compensation__c] [nvarchar](255) NULL,
	[Culture__c] [nvarchar](255) NULL,
	[Alignment_with_LPs__c] [nvarchar](255) NULL,
	[Terms__c] [nvarchar](255) NULL,
	[Scaled_Score__c] [decimal](6, 2) NULL,
	[Absolute_Performance__c] [nvarchar](255) NULL,
	[Relative_Performance__c] [nvarchar](255) NULL,
	[Realized_Performance__c] [nvarchar](255) NULL,
	[Depth_of_Track_Record__c] [nvarchar](255) NULL,
	[Relevance_of_Track_Record__c] [nvarchar](255) NULL,
	[Loss_Ratio_Analysis__c] [nvarchar](255) NULL,
	[Unrealized_Portfolio__c] [nvarchar](255) NULL,
	[RCP_Value_Creation_Analysis__c] [nvarchar](255) NULL,
	[RCP_Hits_Misses__c] [nvarchar](255) NULL,
	[Deal_Size__c] [nvarchar](255) NULL,
	[Strategy__c] [nvarchar](255) NULL,
	[Time__c] [nvarchar](255) NULL,
	[Team__c] [nvarchar](255) NULL,
	[Qualitative_Final_Grade__c] [nvarchar](15) NULL,
	[Quantitative_Final_Grade__c] [nvarchar](15) NULL,
	[Quantitative_Final_Number__c] [decimal](18, 2) NULL,
	[IsDeleted] [bit] NULL,
	[Publish_Scorecard__c] [bit] NULL,
	[ParentScorecard_IsDeleted] [bit] NULL,
	[ParentScorecard_Publish_Level__c] [nvarchar](255) NULL,
	[Date_Reviewed__c] [datetime2](7) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_GPScoutScorecards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ids]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Ids](
	[UID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Ids_UID]  DEFAULT (newid()),
	[GPScout_Scorecard__c] [varchar](18) NULL,
	[AIMPortMgmt__Fund__c] [varchar](18) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Ids] ADD [MostRecentRCPFundMetricId] [varchar](18) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Ids] ADD [MostRecentPreqinFundMetricId] [varchar](18) NULL
ALTER TABLE [dbo].[Ids] ADD [OrgInProcessBeforeImport] [uniqueidentifier] NULL
 CONSTRAINT [PK_Ids] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MergeInternalEntities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MergeInternalEntities](
	[SFName] [nvarchar](200) NULL,
	[RecordType] [nvarchar](50) NULL,
	[Test] [nvarchar](50) NULL,
	[ID] [nvarchar](50) NULL,
	[OrganizationName] [nvarchar](200) NULL,
	[OrganizationId] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StrengthsAndConcerns]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StrengthsAndConcerns](
	[Id] [varchar](18) NOT NULL,
	[Display_Order__c] [decimal](3, 0) NULL,
	[Publish__c] [bit] NOT NULL,
	[Account__c] [varchar](18) NOT NULL,
	[Type__c] [nvarchar](255) NULL,
	[Name__c] [nvarchar](255) NULL,
	[External_Text__c] [nvarchar](max) NULL,
	[Internal_Notes__c] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_StrengthsAndConcerns] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecords]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackRecords](
	[Id] [varchar](18) NOT NULL,
	[Account__c] [varchar](18) NULL,
	[Name] [nvarchar](80) NULL,
	[Description__c] [nvarchar](max) NULL,
	[File_Name__c] [nvarchar](255) NULL,
	[Display_Order__c] [decimal](3, 0) NULL,
	[IsDeleted] [bit] NULL,
	[Publish__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_TrackRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Accounts]') AND name = N'NonClusteredIndex-20150514-023124')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-023124] ON [dbo].[Accounts]
(
	[Most_Recent_Fund_Currency__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contacts]') AND name = N'NonClusteredIndex-20150514-002810')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-002810] ON [dbo].[Contacts]
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FirmAddreses]') AND name = N'NonClusteredIndex-20150514-002854')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-002854] ON [dbo].[FirmAddreses]
(
	[Account__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FundMetrics]') AND name = N'NonClusteredIndex-20150513-185321')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150513-185321] ON [dbo].[FundMetrics]
(
	[AIMPortMgmt__Account__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND name = N'NonClusteredIndex-20150514-003027')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-003027] ON [dbo].[Funds]
(
	[AIM__Account__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Funds]') AND name = N'NonClusteredIndex-20150514-005259')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-005259] ON [dbo].[Funds]
(
	[Currency__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GPScoutScorecards]') AND name = N'NonClusteredIndex-20150514-003127')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-003127] ON [dbo].[GPScoutScorecards]
(
	[GPScout_Scorecard__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[StrengthsAndConcerns]') AND name = N'NonClusteredIndex-20150514-003401')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-003401] ON [dbo].[StrengthsAndConcerns]
(
	[Account__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecords]') AND name = N'NonClusteredIndex-20150514-003440')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150514-003440] ON [dbo].[TrackRecords]
(
	[Account__c] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_DeleteSFData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_DeleteSFData] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXEC [dbo].[SF_DeleteSFData] 1
ALTER PROCEDURE [dbo].[SF_DeleteSFData]
	@FullDelete bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM alexabe_01.dbo.SearchOptions
	DELETE FROM alexabe_01.dbo.EntityMultipleValues

	DELETE FROM alexabe_01.dbo.Benchmarks
	DELETE FROM alexabe_01.dbo.Evaluation
	DELETE FROM alexabe_01.dbo.Grade
	DELETE FROM alexabe_01.dbo.OrganizationMember
	DELETE FROM alexabe_01.dbo.OtherAddress
	DELETE FROM alexabe_01.dbo.TrackRecord
	DELETE FROM alexabe_01.dbo.Fund
	DELETE FROM alexabe_01.dbo.Entity WHERE Id IN (SELECT Id FROM alexabe_01.dbo.Fund) AND EntityType = 2

	IF @FullDelete = 1 BEGIN
		DELETE FROM alexabe_01.dbo.ClientRequest
		DELETE FROM alexabe_01.dbo.OrganizationRequest
		DELETE FROM alexabe_01.dbo.OrganizationViewing

		DELETE FROM alexabe_01.dbo.PortfolioCompany
		DELETE FROM alexabe_01.dbo.GroupOrganization 
		DELETE FROM alexabe_01.dbo.OrganizationNote 
		DELETE FROM alexabe_01.dbo.TrackUsersProfileView
		DELETE FROM alexabe_01.dbo.Organization
		DELETE FROM alexabe_01.dbo.Entity WHERE Id IN (SELECT Id FROM alexabe_01.dbo.Organization) AND EntityType = 1
	END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_GetAdditionalAccountIdsClause]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_GetAdditionalAccountIdsClause] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_GetAdditionalAccountIdsClause]
AS

SELECT AccountID Id FROM (
	SELECT DISTINCT AccountID FROM (
			SELECT AccountId FROM Contacts
			UNION ALL
			SELECT GPScout_Scorecard__c FROM GPScoutScorecards
			UNION ALL
			SELECT FA.Account__c FROM FirmAddreses FA
			UNION ALL 
			SELECT F.AIM__Account__c FROM Funds F
			UNION ALL
			SELECT TR.Account__c FROM TrackRecords TR
			UNION ALL 
			SELECT SC.Account__c FROM StrengthsAndConcerns SC
			--UNION ALL
			--SELECT M.AIMPortMgmt__Account__c FROM Metrics M
			UNION ALL 
			SELECT FM.AIMPortMgmt__Account__c FROM FundMetrics FM
			UNION ALL
			SELECT GPScout_Scorecard__c
			FROM Ids
			WHERE GPScout_Scorecard__c IS NOT NULL
		) B WHERE AccountID IS NOT NULL AND AccountID NOT IN (SELECT Id FROM Accounts)
	) IDs

--DECLARE @accountIDs varchar(max) = ''
--SELECT @accountIDs = STUFF((
--	SELECT ',''' + AccountId + '''' FROM (
--		SELECT DISTINCT AccountID FROM (
--			SELECT AccountId FROM Contacts
--			UNION ALL
--			SELECT GPScout_Scorecard__c FROM GPScoutScorecards
--			UNION ALL
--			SELECT FA.Account__c FROM FirmAddreses FA
--			UNION ALL 
--			SELECT F.AIM__Account__c FROM Funds F
--			UNION ALL
--			SELECT TR.Account__c FROM TrackRecords TR
--			UNION ALL 
--			SELECT SC.Account__c FROM StrengthsAndConcerns SC
--			UNION ALL
--			SELECT M.AIMPortMgmt__Account__c FROM Metrics M
--			UNION ALL 
--			SELECT FM.AIMPortMgmt__Account__c FROM FundMetrics FM
--		) B WHERE AccountID IS NOT NULL
--	) A FOR XML PATH(''))
--,1,1,'')

--SELECT CASE 
--  WHEN @accountIDs!='' THEN ' OR Id IN (' + @accountIDs + ')'
--  ELSE ''
--END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_GetEntityImportConfig]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_GetEntityImportConfig] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_GetEntityImportConfig]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM SF_EntityImportConfig WHERE Enabled=1 ORDER BY OrderBy
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportAccounts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportAccounts] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/06/2015 - LastUpdated field imported from GPScout_Last_Updated__c instead of Last_Modified_Date__c from now
--		- 08/08/2016 - Changed AUM__c to AUM_Calc__c
--		- 12/09/2016 - Changed Most_Recent_Fund_Regional_Specialist__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_is_Sector_Specialist__c from bit NOT NULL to nvarchar(9) Yes, No, or NULL
--		- 05/04/2017 - Most_Recent_Fund_Regional_Specialist__c added to the import logic
--					 - Access_Constrained__c replaced with Access_Constrained_Firm__c
--					 - Populate KeyTakeAway with Overview__c (KeyTakeAway not used anymore)
--					 - Industry_Focus__c removed, not used anymore
--					 - Most_Recent_Fund_Sub_Region__c removed, not used anymore
--		- 06/06/2017 - InactiveFirm logic added
--		- 06/28/2017 - adjustment (CASE WHEN Most_Recent_Fund_Fundraising_Status__c IN ('Pledge Fund - Active', 'Pledge Fund - Inactive') THEN 'Fundless Sponsor' ELSE Most_Recent_Fund_Fundraising_Status__c END) Most_Recent_Fund_Fundraising_Status__c, -- Most_Recent_Fund_Fundraising_Status__c moved to SP SF_PostImport
--		- 10/09/2017 - FundRaisingStatus = 'Assumed Closed / Unknown' will import as null
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportAccounts]
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_Regional_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_Regional_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_Regional_Specialist__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_SBIC__c NOT IN ('No','Yes') AND Most_Recent_Fund_SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_SBIC__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_is_Sector_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_is_Sector_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_is_Sector_Specialist__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE A
	SET A.Include = CASE WHEN IsDeleted = 0 AND Publish_Level__c IS NOT NULL THEN 1 ELSE 0 END
	FROM dbo.Accounts A

	UPDATE A SET NewOrganizationId = NEWID()
	FROM dbo.Accounts A
	WHERE NOT EXISTS (SELECT NULL FROM alexabe_01.dbo.Organization Target WHERE Target.SFID = A.Id)
		AND A.Include=1

	INSERT [alexabe_01].[dbo].[Entity] (Id, EntityType)
	SELECT Source.NewOrganizationId, 1
	FROM dbo.Accounts Source 
	WHERE Source.NewOrganizationId IS NOT NULL AND Source.NewOrganizationId NOT IN (SELECT Id FROM [alexabe_01].[dbo].[Entity] WHERE EntityType=1) --AND Include=1
	

	MERGE alexabe_01.dbo.Organization WITH (HOLDLOCK) AS Target
	USING (SELECT 
		ISNULL(Access_Constrained_Firm__c, 0) AS Access_Constrained_Firm__c,
		Alias__c,
		AUM_Calc__c,
		BillingCountry,
		Co_Investment_Opportunities_for_Fund_LP__c,
		Co_Investment_Opportunities_for_Non_LPs__c,
		DateImportedUtc,
		ISNULL(Emerging_Manager__c, 0) AS Emerging_Manager__c,
		Evaluation_Text__c,
		Expected_Next_Fundraise__c,
		Firm_History__c,
		--Grade_Qualitative_Text__c,
		--Grade_Quantitative_Text__c,
		--Grade_Scatterchart_Text__c,
		A.Id,
		--Industry_Focus__c,
		Investment_Thesis__c,
		--Key_Takeaway__c,
		GPScout_Last_Updated__c,
		Most_Recent_Fund_Eff_Size__c,
		NULLIF(Most_Recent_Fund_Fundraising_Status__c, 'Assumed Closed / Unknown') Most_Recent_Fund_Fundraising_Status__c,
		Most_Recent_Fund_Industry_Focus__c,
		(CASE Most_Recent_Fund_is_Sector_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_is_Sector_Specialist__c,
		Most_Recent_Fund_Market_Stage__c,
		Most_Recent_Fund_Primary_Strategy__c,
		Most_Recent_Fund_Region__c,
		(CASE Most_Recent_Fund_Regional_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_Regional_Specialist__c, -- Most_Recent_Fund_Regional_Specialist__c,
		(CASE Most_Recent_Fund_SBIC__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_SBIC__c,
		Most_Recent_Fund_Secondary_Strategy__c,
		--Most_Recent_Fund_Sub_Region__c,
		A.Name,
		NewOrganizationId,
		Overview__c,
		-- if Account has been deleted or unpublished in SF then we unpublish it in GPScout
		(CASE Include WHEN 1 THEN Publish_Level__c ELSE '' END) Publish_Level__c,
		--ISNULL(Radar_List__c, 0) AS Radar_List__c,
		(CAST(CASE WHEN FirmGrade__c='A' OR FirmGrade__c='B' THEN 1 ELSE 0 END AS BIT)) AS FirmGrade__c,
		(CASE FirmGrade__c WHEN 'Inactive' THEN 1 ELSE 0 END) InactiveFirm,
		Region__c,
		Team_Overview__c,
		Total_Closed_Funds__c,
		Track_Record_Text__c,
		Website,
		YearGPFounded__c,
		[Include], -- initialized in UPDATE above
		C.Id CurrencyId,
		GPScout_Phase_Assigned__c,
		GPScout_Phase_Completed__c
		--A.RecordTypeName,
		--A.ParentId
		FROM dbo.Accounts A
		LEFT JOIN alexabe_01.dbo.Currency C ON A.Most_Recent_Fund_Currency__c = C.Name
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED /*AND Source.[Include] = 1*/ THEN UPDATE SET 
		-- Grade.Scatterchart_Text__c = Source.Scatterchart_Text__c, -- Handled in SF_PostImport
		Target.RegionalSpecialist = Source.Most_Recent_Fund_Regional_Specialist__c, 
		Target.AccessConstrained = Source.Access_Constrained_Firm__c,
		Target.Address1Country = Source.BillingCountry,
		Target.AliasesPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Target.Aum = Source.AUM_Calc__c,
		Target.CoInvestWithExistingLPs = Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Target.CoInvestWithOtherLPs = Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Target.DateImportedUtc = Source.DateImportedUtc,
		Target.EmergingManager = Source.Emerging_Manager__c,
		Target.PublishLevelAsImported = Source.Publish_Level__c,
		Target.EvaluationText = Source.Evaluation_Text__c,
		Target.ExpectedNextFundRaise = Source.Expected_Next_Fundraise__c,
		--Target.FocusList = Source.Radar_List__c,
		Target.FocusList = Source.FirmGrade__c,
		Target.History = Source.Firm_History__c,
		Target.InvestmentRegionPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Target.InvestmentThesis = Source.Investment_Thesis__c,
		Target.KeyTakeAway = Source.Overview__c, --Source.Key_Takeaway__c,
		Target.LastFundSize = Source.Most_Recent_Fund_Eff_Size__c,
		Target.LastUpdated = Source.GPScout_Last_Updated__c,
		Target.MarketStage = Source.Most_Recent_Fund_Market_Stage__c,
		Target.Name = Source.Name,
		Target.NumberOfFunds = Source.Total_Closed_Funds__c,
		Target.OrganizationOverview = Source.Overview__c,
		--Target.QualitativeGrade = Source.Grade_Qualitative_Text__c,	-- Handled in SF_PostImport
		--Target.QuantitativeGrade = Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Target.SBICFund = Source.Most_Recent_Fund_SBIC__c,
		Target.SectorFocusPipeDelimited = dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Target.SectorSpecialist = Source.Most_Recent_Fund_is_Sector_Specialist__c,
		Target.StrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--Target.SubRegionsPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		Target.SubStrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Target.FundRaisingStatus = Source.Most_Recent_Fund_Fundraising_Status__c,
		Target.TeamOverview = Source.Team_Overview__c,
		Target.TrackRecordText = Source.Track_Record_Text__c,
		Target.WebsiteUrl = Source.Website,
		Target.YearFounded = Source.YearGPFounded__c,
		Target.CurrencyId = Source.CurrencyId,
		Target.GPScoutPhaseAssigned = Source.GPScout_Phase_Assigned__c,
		Target.GPScoutPhaseCompleted = Source.GPScout_Phase_Completed__c,
		--Target.RecordTypeName = Source.RecordTypeName,
		--Target.ParentSFID = Source.ParentId
		Target.InactiveFirm = Source.InactiveFirm

	WHEN NOT MATCHED BY TARGET AND (Source.[Include] = 1 /*OR Source.RecordTypeName = 'Fund Manager Parent'*/) THEN INSERT(
		SFID,
		RegionalSpecialist, 
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		AccessConstrained,
		Address1Country,
		AliasesPipeDelimited,
		Aum,
		CoInvestWithExistingLPs,
		CoInvestWithOtherLPs,
		DateImportedUtc,
		EmergingManager,
		PublishLevelAsImported,
		EvaluationText,
		ExpectedNextFundRaise,
		FocusList,
		History,
		Id,
		InvestmentRegionPipeDelimited,
		InvestmentThesis,
		KeyTakeAway,
		LastFundSize,
		LastUpdated,
		MarketStage,
		Name,
		NumberOfFunds,
		OrganizationOverview,
		--QualitativeGrade,
		--QuantitativeGrade,
		SBICFund,
		SectorFocusPipeDelimited,
		SectorSpecialist,
		StrategyPipeDelimited,
		--SubRegionsPipeDelimited,
		SubStrategyPipeDelimited,
		FundRaisingStatus,
		TeamOverview,
		TrackRecordText,
		WebsiteUrl,
		YearFounded,
		CurrencyId,
		GPScoutPhaseAssigned,
		GPScoutPhaseCompleted,
		--RecordTypeName,
		--ParentSFID
		InactiveFirm
	) 
	VALUES(
		Source.Id,
		Source.Most_Recent_Fund_Regional_Specialist__c, 
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		Source.Access_Constrained_Firm__c,
		Source.BillingCountry,
		dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Source.AUM_Calc__c,
		Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Source.DateImportedUtc,
		Source.Emerging_Manager__c,
		Source.Publish_Level__c,
		Source.Evaluation_Text__c,
		Source.Expected_Next_Fundraise__c,
		--Source.Radar_List__c,
		Source.FirmGrade__c,
		Source.Firm_History__c,
		Source.NewOrganizationId,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Source.Investment_Thesis__c,
		Source.Overview__c, --Source.Key_Takeaway__c,
		Source.Most_Recent_Fund_Eff_Size__c,
		Source.GPScout_Last_Updated__c,
		Source.Most_Recent_Fund_Market_Stage__c,
		Source.Name,
		Source.Total_Closed_Funds__c,
		Source.Overview__c,
		--Source.Grade_Qualitative_Text__c,		-- Handled in SF_PostImport
		--Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Source.Most_Recent_Fund_SBIC__c,
		dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Source.Most_Recent_Fund_is_Sector_Specialist__c,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Source.Most_Recent_Fund_Fundraising_Status__c,
		Source.Team_Overview__c,
		Source.Track_Record_Text__c,
		Source.Website,
		Source.YearGPFounded__c,
		Source.CurrencyId,
		Source.GPScout_Phase_Assigned__c,
		Source.GPScout_Phase_Completed__c,
		--Source.RecordTypeName,
		--Source.ParentId
		Source.InactiveFirm
	)
--	WHEN MATCHED AND Source.[Include] = 0 THEN -- Organizations are not deleted for now. They can have user created detail info attached
		--DELETE
		;

	UPDATE O
		SET PublishLevelID = CASE
			WHEN A.Include = 0 THEN 0
			WHEN A.Publish_Level__c LIKE '0 -%' THEN 1
			WHEN A.Publish_Level__c LIKE '1 -%' THEN 2
			WHEN A.Publish_Level__c LIKE '2 -%' THEN 3
			WHEN A.Publish_Level__c LIKE '3 -%' THEN 4
			WHEN A.Publish_Level__c LIKE '2.5 -%' THEN 5
			ELSE 0
		END
	FROM Accounts A
	JOIN alexabe_01.dbo.Organization O ON A.Id = O.SFID

	UPDATE O
		SET 
			EvaluationLevel = CASE PublishLevelID
				WHEN 2 THEN 'Initial Assessment'
				WHEN 3 THEN 'Preliminary Evaluation'
				WHEN 4 THEN 'Evaluation'
				WHEN 5 THEN 'Evaluation'
				ELSE ''
			END,
			DiligenceLevel = CASE PublishLevelID
				WHEN 2 THEN 33
				WHEN 3 THEN 66
				WHEN 4 THEN 100
				WHEN 5 THEN 66
				ELSE ''
			END,
			PublishSearch = CASE WHEN PublishLevelID>=1 THEN 1 ELSE 0 END,
			PublishOverviewAndTeam = CASE WHEN PublishLevelID>=2 THEN 1 ELSE 0 END,
			PublishTrackRecord = CASE WHEN PublishLevelID>=3 THEN 1 ELSE 0 END,
			PublishProfile = CASE WHEN PublishLevelID>=4 THEN 1 ELSE 0 END,
			FocusRadar = CASE WHEN O.FocusList=1 THEN 'Focus List' ELSE '' END--,
			--ParentId = NULL
	FROM Accounts A
	JOIN alexabe_01.dbo.Organization O ON A.Id = O.SFID

	-- set parent-child relationship
	--UPDATE ChildOrganization
	--SET ChildOrganization.ParentId = ParentOrganization.Id
	--FROM Accounts A
	--JOIN alexabe_01.dbo.Organization ChildOrganization ON A.Id = ChildOrganization.SFID
	--JOIN alexabe_01.dbo.Organization ParentOrganization ON A.ParentId = ParentOrganization.SFID

	-- remove child items of unpublished Organizations
	SELECT Id INTO #UnpublishedOrgs FROM alexabe_01.dbo.Organization WHERE PublishLevelID=0

	DELETE E FROM alexabe_01.dbo.Evaluation E JOIN #UnpublishedOrgs O ON E.OrganizationId=O.Id 
	DELETE G FROM alexabe_01.dbo.Grade G JOIN #UnpublishedOrgs O ON G.OrganizationId=O.Id 
	DELETE M FROM alexabe_01.dbo.OrganizationMember M JOIN #UnpublishedOrgs O ON M.OrganizationId=O.Id 
	--DELETE M FROM alexabe_01.dbo.OrganizationMember M WHERE NOT EXISTS (SELECT NULL FROM alexabe_01.dbo.Organization O WHERE O.Id=M.OrganizationId AND O.PublishLevelID>0)
	DELETE A FROM alexabe_01.dbo.OtherAddress A JOIN #UnpublishedOrgs O ON A.OrganizationId=O.Id 
	DELETE T FROM alexabe_01.dbo.TrackRecord T JOIN #UnpublishedOrgs O ON T.OrganizationId=O.Id
	DELETE F FROM alexabe_01.dbo.Fund F JOIN #UnpublishedOrgs O ON F.OrganizationId=O.Id

	DROP TABLE #UnpublishedOrgs

	TRUNCATE TABLE alexabe_01.dbo.ResearchPriorityOrganization
	INSERT alexabe_01.dbo.ResearchPriorityOrganization (
			[Id]
           ,[Name]
           ,[MarketStage]
           ,[StrategyPipeDelimited]
           ,[InvestmentRegionPipeDelimited])
     SELECT
           Id
           ,Name
           ,MarketStage
           ,StrategyPipeDelimited
           ,InvestmentRegionPipeDelimited
	FROM alexabe_01.dbo.Organization
	WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '') AND PublishLevelAsImported!=''

	COMMIT TRAN
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportAccounts_2017_05_09_before_Import_field_and_adv_search_changes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportAccounts_2017_05_09_before_Import_field_and_adv_search_changes] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/06/2015 - LastUpdated field imported from GPScout_Last_Updated__c instead of Last_Modified_Date__c from now
--		- 08/08/2016 - Changed AUM__c to AUM_Calc__c
--		- 12/09/2016 - Changed Most_Recent_Fund_Regional_Specialist__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_is_Sector_Specialist__c from bit NOT NULL to nvarchar(9) Yes, No, or NULL
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportAccounts_2017_05_09_before_Import_field_and_adv_search_changes]
AS
/*
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_Regional_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_Regional_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_Regional_Specialist__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_SBIC__c NOT IN ('No','Yes') AND Most_Recent_Fund_SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_SBIC__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_is_Sector_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_is_Sector_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_is_Sector_Specialist__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE A
	SET A.Include = CASE WHEN IsDeleted = 0 AND Publish_Level__c IS NOT NULL THEN 1 ELSE 0 END
	FROM dbo.Accounts A

	UPDATE A SET NewOrganizationId = NEWID()
	FROM dbo.Accounts A
	WHERE NOT EXISTS (SELECT NULL FROM alexabe_01.dbo.Organization Target WHERE Target.SFID = A.Id)
		AND A.Include=1

	INSERT [alexabe_01].[dbo].[Entity] (Id, EntityType)
	SELECT Source.NewOrganizationId, 1
	FROM dbo.Accounts Source 
	WHERE Source.NewOrganizationId IS NOT NULL AND Source.NewOrganizationId NOT IN (SELECT Id FROM [alexabe_01].[dbo].[Entity] WHERE EntityType=1) --AND Include=1
	

	MERGE alexabe_01.dbo.Organization WITH (HOLDLOCK) AS Target
	USING (SELECT 
		ISNULL(Access_Constrained__c, 0) AS Access_Constrained__c,
		Alias__c,
		AUM_Calc__c,
		BillingCountry,
		Co_Investment_Opportunities_for_Fund_LP__c,
		Co_Investment_Opportunities_for_Non_LPs__c,
		DateImportedUtc,
		ISNULL(Emerging_Manager__c, 0) AS Emerging_Manager__c,
		Evaluation_Text__c,
		Expected_Next_Fundraise__c,
		Firm_History__c,
		--Grade_Qualitative_Text__c,
		--Grade_Quantitative_Text__c,
		--Grade_Scatterchart_Text__c,
		A.Id,
		Industry_Focus__c,
		Investment_Thesis__c,
		Key_Takeaway__c,
		GPScout_Last_Updated__c,
		Most_Recent_Fund_Eff_Size__c,
		Most_Recent_Fund_Fundraising_Status__c,
		Most_Recent_Fund_Industry_Focus__c,
		(CASE Most_Recent_Fund_is_Sector_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_is_Sector_Specialist__c,
		Most_Recent_Fund_Market_Stage__c,
		Most_Recent_Fund_Primary_Strategy__c,
		Most_Recent_Fund_Region__c,
		Most_Recent_Fund_Regional_Specialist__c,
		(CASE Most_Recent_Fund_SBIC__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_SBIC__c,
		Most_Recent_Fund_Secondary_Strategy__c,
		Most_Recent_Fund_Sub_Region__c,
		A.Name,
		NewOrganizationId,
		Overview__c,
		-- if Account has been deleted or unpublished in SF then we unpublish it in GPScout
		(CASE Include WHEN 1 THEN Publish_Level__c ELSE '' END) Publish_Level__c,
		--ISNULL(Radar_List__c, 0) AS Radar_List__c,
		(CAST(CASE WHEN FirmGrade__c='A' OR FirmGrade__c='B' THEN 1 ELSE 0 END AS BIT)) AS FirmGrade__c,
		Region__c,
		Team_Overview__c,
		Total_Closed_Funds__c,
		Track_Record_Text__c,
		Website,
		YearGPFounded__c,
		[Include], -- initialized in UPDATE above
		C.Id CurrencyId,
		GPScout_Phase_Assigned__c,
		GPScout_Phase_Completed__c
		--A.RecordTypeName,
		--A.ParentId
		FROM dbo.Accounts A
		LEFT JOIN alexabe_01.dbo.Currency C ON A.Most_Recent_Fund_Currency__c = C.Name
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED /*AND Source.[Include] = 1*/ THEN UPDATE SET 
		-- Grade.Scatterchart_Text__c = Source.Scatterchart_Text__c, -- Handled in SF_PostImport
		-- Target. = Source.Most_Recent_Fund_Regional_Specialist__c, -- does not exist in current mapping
		Target.AccessConstrained = Source.Access_Constrained__c,
		Target.Address1Country = Source.BillingCountry,
		Target.AliasesPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Target.Aum = Source.AUM_Calc__c,
		Target.CoInvestWithExistingLPs = Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Target.CoInvestWithOtherLPs = Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Target.DateImportedUtc = Source.DateImportedUtc,
		Target.EmergingManager = Source.Emerging_Manager__c,
		Target.PublishLevelAsImported = Source.Publish_Level__c,
		Target.EvaluationText = Source.Evaluation_Text__c,
		Target.ExpectedNextFundRaise = Source.Expected_Next_Fundraise__c,
		--Target.FocusList = Source.Radar_List__c,
		Target.FocusList = Source.FirmGrade__c,
		Target.History = Source.Firm_History__c,
		Target.InvestmentRegionPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Target.InvestmentThesis = Source.Investment_Thesis__c,
		Target.KeyTakeAway = Source.Key_Takeaway__c,
		Target.LastFundSize = Source.Most_Recent_Fund_Eff_Size__c,
		Target.LastUpdated = Source.GPScout_Last_Updated__c,
		Target.MarketStage = Source.Most_Recent_Fund_Market_Stage__c,
		Target.Name = Source.Name,
		Target.NumberOfFunds = Source.Total_Closed_Funds__c,
		Target.OrganizationOverview = Source.Overview__c,
		--Target.QualitativeGrade = Source.Grade_Qualitative_Text__c,	-- Handled in SF_PostImport
		--Target.QuantitativeGrade = Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Target.SBICFund = Source.Most_Recent_Fund_SBIC__c,
		Target.SectorFocusPipeDelimited = dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Target.SectorSpecialist = Source.Most_Recent_Fund_is_Sector_Specialist__c,
		Target.StrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		Target.SubRegionsPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		Target.SubStrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Target.FundRaisingStatus = Source.Most_Recent_Fund_Fundraising_Status__c,
		Target.TeamOverview = Source.Team_Overview__c,
		Target.TrackRecordText = Source.Track_Record_Text__c,
		Target.WebsiteUrl = Source.Website,
		Target.YearFounded = Source.YearGPFounded__c,
		Target.CurrencyId = Source.CurrencyId,
		Target.GPScoutPhaseAssigned = Source.GPScout_Phase_Assigned__c,
		Target.GPScoutPhaseCompleted = Source.GPScout_Phase_Completed__c
		--Target.RecordTypeName = Source.RecordTypeName,
		--Target.ParentSFID = Source.ParentId

	WHEN NOT MATCHED BY TARGET AND (Source.[Include] = 1 /*OR Source.RecordTypeName = 'Fund Manager Parent'*/) THEN INSERT(
		SFID,
		-- Most_Recent_Fund_Regional_Specialist__c, -- does not exist in current mapping
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		AccessConstrained,
		Address1Country,
		AliasesPipeDelimited,
		Aum,
		CoInvestWithExistingLPs,
		CoInvestWithOtherLPs,
		DateImportedUtc,
		EmergingManager,
		PublishLevelAsImported,
		EvaluationText,
		ExpectedNextFundRaise,
		FocusList,
		History,
		Id,
		InvestmentRegionPipeDelimited,
		InvestmentThesis,
		KeyTakeAway,
		LastFundSize,
		LastUpdated,
		MarketStage,
		Name,
		NumberOfFunds,
		OrganizationOverview,
		--QualitativeGrade,
		--QuantitativeGrade,
		SBICFund,
		SectorFocusPipeDelimited,
		SectorSpecialist,
		StrategyPipeDelimited,
		SubRegionsPipeDelimited,
		SubStrategyPipeDelimited,
		FundRaisingStatus,
		TeamOverview,
		TrackRecordText,
		WebsiteUrl,
		YearFounded,
		CurrencyId,
		GPScoutPhaseAssigned,
		GPScoutPhaseCompleted
		--RecordTypeName,
		--ParentSFID
	) 
	VALUES(
		Source.Id,
		-- Most_Recent_Fund_Regional_Specialist__c, -- does not exist in current mapping
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		Source.Access_Constrained__c,
		Source.BillingCountry,
		dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Source.AUM_Calc__c,
		Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Source.DateImportedUtc,
		Source.Emerging_Manager__c,
		Source.Publish_Level__c,
		Source.Evaluation_Text__c,
		Source.Expected_Next_Fundraise__c,
		--Source.Radar_List__c,
		Source.FirmGrade__c,
		Source.Firm_History__c,
		Source.NewOrganizationId,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Source.Investment_Thesis__c,
		Source.Key_Takeaway__c,
		Source.Most_Recent_Fund_Eff_Size__c,
		Source.GPScout_Last_Updated__c,
		Source.Most_Recent_Fund_Market_Stage__c,
		Source.Name,
		Source.Total_Closed_Funds__c,
		Source.Overview__c,
		--Source.Grade_Qualitative_Text__c,		-- Handled in SF_PostImport
		--Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Source.Most_Recent_Fund_SBIC__c,
		dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Source.Most_Recent_Fund_is_Sector_Specialist__c,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Source.Most_Recent_Fund_Fundraising_Status__c,
		Source.Team_Overview__c,
		Source.Track_Record_Text__c,
		Source.Website,
		Source.YearGPFounded__c,
		Source.CurrencyId,
		Source.GPScout_Phase_Assigned__c,
		Source.GPScout_Phase_Completed__c
		--Source.RecordTypeName,
		--Source.ParentId
	)
--	WHEN MATCHED AND Source.[Include] = 0 THEN -- Organizations are not deleted for now. They can have user created detail info attached
		--DELETE
		;

	UPDATE O
		SET PublishLevelID = CASE
			WHEN A.Include = 0 THEN 0
			WHEN A.Publish_Level__c LIKE '0 -%' THEN 1
			WHEN A.Publish_Level__c LIKE '1 -%' THEN 2
			WHEN A.Publish_Level__c LIKE '2 -%' THEN 3
			WHEN A.Publish_Level__c LIKE '3 -%' THEN 4
			WHEN A.Publish_Level__c LIKE '2.5 -%' THEN 5
			ELSE 0
		END
	FROM Accounts A
	JOIN alexabe_01.dbo.Organization O ON A.Id = O.SFID

	UPDATE O
		SET 
			EvaluationLevel = CASE PublishLevelID
				WHEN 2 THEN 'Initial Assessment'
				WHEN 3 THEN 'Preliminary Evaluation'
				WHEN 4 THEN 'Evaluation'
				WHEN 5 THEN 'Evaluation'
				ELSE ''
			END,
			DiligenceLevel = CASE PublishLevelID
				WHEN 2 THEN 33
				WHEN 3 THEN 66
				WHEN 4 THEN 100
				WHEN 5 THEN 66
				ELSE ''
			END,
			PublishSearch = CASE WHEN PublishLevelID>=1 THEN 1 ELSE 0 END,
			PublishOverviewAndTeam = CASE WHEN PublishLevelID>=2 THEN 1 ELSE 0 END,
			PublishTrackRecord = CASE WHEN PublishLevelID>=3 THEN 1 ELSE 0 END,
			PublishProfile = CASE WHEN PublishLevelID>=4 THEN 1 ELSE 0 END,
			FocusRadar = CASE WHEN O.FocusList=1 THEN 'Focus List' ELSE '' END--,
			--ParentId = NULL
	FROM Accounts A
	JOIN alexabe_01.dbo.Organization O ON A.Id = O.SFID

	-- set parent-child relationship
	--UPDATE ChildOrganization
	--SET ChildOrganization.ParentId = ParentOrganization.Id
	--FROM Accounts A
	--JOIN alexabe_01.dbo.Organization ChildOrganization ON A.Id = ChildOrganization.SFID
	--JOIN alexabe_01.dbo.Organization ParentOrganization ON A.ParentId = ParentOrganization.SFID

	-- remove child items of unpublished Organizations
	SELECT Id INTO #UnpublishedOrgs FROM alexabe_01.dbo.Organization WHERE PublishLevelID=0

	DELETE E FROM alexabe_01.dbo.Evaluation E JOIN #UnpublishedOrgs O ON E.OrganizationId=O.Id 
	DELETE G FROM alexabe_01.dbo.Grade G JOIN #UnpublishedOrgs O ON G.OrganizationId=O.Id 
	DELETE M FROM alexabe_01.dbo.OrganizationMember M JOIN #UnpublishedOrgs O ON M.OrganizationId=O.Id 
	--DELETE M FROM alexabe_01.dbo.OrganizationMember M WHERE NOT EXISTS (SELECT NULL FROM alexabe_01.dbo.Organization O WHERE O.Id=M.OrganizationId AND O.PublishLevelID>0)
	DELETE A FROM alexabe_01.dbo.OtherAddress A JOIN #UnpublishedOrgs O ON A.OrganizationId=O.Id 
	DELETE T FROM alexabe_01.dbo.TrackRecord T JOIN #UnpublishedOrgs O ON T.OrganizationId=O.Id
	DELETE F FROM alexabe_01.dbo.Fund F JOIN #UnpublishedOrgs O ON F.OrganizationId=O.Id

	DROP TABLE #UnpublishedOrgs

	TRUNCATE TABLE alexabe_01.dbo.ResearchPriorityOrganization
	INSERT alexabe_01.dbo.ResearchPriorityOrganization (
			[Id]
           ,[Name]
           ,[MarketStage]
           ,[StrategyPipeDelimited]
           ,[InvestmentRegionPipeDelimited])
     SELECT
           Id
           ,Name
           ,MarketStage
           ,StrategyPipeDelimited
           ,InvestmentRegionPipeDelimited
	FROM alexabe_01.dbo.Organization
	WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '') AND PublishLevelAsImported!=''

	COMMIT TRAN
END
	*/

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportBenchmarks]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportBenchmarks] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportBenchmarks]

AS
BEGIN

	SET NOCOUNT ON;

	MERGE [alexabe_01].[dbo].[Benchmarks] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			[Vintage_Year__c]
			,ISNULL([Provided_by__c], '') AS [Provided_by__c]
			,[DPI_Lower_Quartile__c]
			,[DPI_Median__c]
			,[DPI_Upper_Quartile__c]
			,[IRR_Lower_Quartile__c]
			,[IRR_Median__c]
			,[IRR_Upper_Quartile__c]
			,[TVPI_Lower_Quartile__c]
			,[TVPI_Median__c]
			,[TVPI_Upper_Quartile__c]
			,[DateImportedUtc]
			,[Id]
			,[Include] = CASE WHEN IsDeleted = 0 THEN 1 ELSE 0 END
			,[Effective_Date__c]
	FROM [dbo].[Benchmarks]
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Name] = Source.[Provided_By__c],
		Target.[VintageYear] = Source.[Vintage_Year__c],
		Target.[DpiFirstQuartile] = Source.[DPI_Upper_Quartile__c],
		--Target.[DpiFirstQuartile] = Source.[DPI_Lower_Quartile__c],
		Target.[DpiMedian] = Source.[DPI_Median__c],
		--Target.[DpiThirdQuartile] = Source.[DPI_Upper_Quartile__c],
		Target.[DpiThirdQuartile] = Source.[DPI_Lower_Quartile__c],

		--Target.[NetIrrFirstQuartile] = Source.[IRR_Lower_Quartile__c],
		Target.[NetIrrFirstQuartile] = Source.[IRR_Upper_Quartile__c],

		Target.[NetIrrMedian] = Source.[IRR_Median__c],
		Target.[NetIrrThirdQuartile] = Source.[IRR_Lower_Quartile__c],
		--[TVPI_Upper_Quartile__c]
		Target.[TvpiFirstQuartile] = Source.[TVPI_Upper_Quartile__c],
		Target.[TvpiMedian] = Source.[TVPI_Median__c],
		Target.[TvpiThirdQuartile] = Source.[TVPI_Lower_Quartile__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],

		Target.[AsOf] = Source.[Effective_Date__c]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
				[Id]
			   ,[Name]
			   ,[VintageYear]
			   ,[DpiFirstQuartile]
			   ,[DpiMedian]
			   ,[DpiThirdQuartile]
			   ,[NetIrrFirstQuartile]
			   ,[NetIrrMedian]
			   ,[NetIrrThirdQuartile]
			   ,[TvpiFirstQuartile]
			   ,[TvpiMedian]
			   ,[TvpiThirdQuartile]
			   ,[DateImportedUtc]
			   ,[SFID]
			   ,[AsOf]
	) 
		VALUES(	
				newid()
				,Source.[Provided_By__c]
				,Source.[Vintage_Year__c]
				,Source.[DPI_Upper_Quartile__c]
				,Source.[DPI_Median__c]
				,Source.[DPI_Lower_Quartile__c]
				,Source.[IRR_Upper_Quartile__c]
				,Source.[IRR_Median__c]
				,Source.[IRR_Lower_Quartile__c]
				,Source.[TVPI_Upper_Quartile__c]
				,Source.[TVPI_Median__c]
				,Source.[TVPI_Lower_Quartile__c]
				,Source.[DateImportedUtc]
				,Id
				,Source.[Effective_Date__c]
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportContacts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportContacts] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportContacts]
AS
BEGIN

	SET NOCOUNT ON;

	MERGE [alexabe_01].[dbo].[OrganizationMember] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  C.[Phone]
		  ,CAST(C.[Email] AS NVARCHAR(50)) AS [Email]
		  ,C.[Title]
		  ,C.[Year_Started_With_Current_Firm__c]
		  ,C.[Bio__c]
		  ,CAST(C.MailingStreet AS NVARCHAR(128)) AS MailingStreet
		  ,C.MailingCity
		  ,CAST([MailingState] AS NVARCHAR(50)) AS MailingState
		  ,C.MailingPostalCode
		  ,CAST([MailingCountry] AS NVARCHAR(50)) AS MailingCountry
		  ,C.[DateImportedUtc]
		  ,C.[Id]
		  ,O.Id OrganizationId
		  ,[Include] = CASE WHEN IsDeleted = 0 AND Publish_Contact__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
		  ,C.FirstName
		  ,C.LastName
		  ,C.Display_Order__c
	  FROM [dbo].[Contacts] C
	  LEFT JOIN [alexabe_01].[dbo].[Organization] O ON C.AccountId = O.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Phone] = Source.[Phone],
		Target.[Email] = Source.[Email],
		Target.[JobTitle] = Source.[Title],
		Target.[YearsAtOrganization] = Source.[Year_Started_With_Current_Firm__c],
		Target.[Description] = Source.[Bio__c],
		Target.[Address1] = Source.MailingStreet,
		Target.[City] = Source.MailingCity,
		Target.[StateOrProvince] = Source.MailingState,
		Target.[PostalCode] = Source.MailingPostalCode,
		Target.[Country] = Source.MailingCountry,
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.OrganizationId = Source.OrganizationId,
		Target.FirstName = Source.FirstName,
		Target.LastName = Source.LastName,
		Target.DisplayOrder = Source.Display_Order__c

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id
			,[Phone]
			,[Email]
			,[JobTitle]
			,[YearsAtOrganization]
			,[Description]
			,[Address1]
			,[City]
			,[StateOrProvince]
			,[PostalCode]
			,[Country]
			,[DateImportedUtc]
			,[SFID]
			,OrganizationId
			,FirstName
			,LastName
			,DisplayOrder
	) 
		VALUES(	
				NEWID()
				,Source.[Phone]
				,Source.[Email]
				,Source.[Title]
				,Source.[Year_Started_With_Current_Firm__c]
				,Source.[Bio__c]
				,Source.MailingStreet
				,Source.MailingCity
				,Source.MailingState
				,Source.MailingPostalCode
				,Source.MailingCountry
				,Source.[DateImportedUtc]
				,Source.Id
				,Source.OrganizationId
				,Source.FirstName
				,Source.LastName
				,Source.Display_Order__c
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFirmAddresses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFirmAddresses] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFirmAddresses]

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE FirmAddreses
	SET [Firm_Location]=geography::STPointFromText('POINT('+ CAST(Firm_Location_Long AS varchar(99)) +' '+ CAST(Firm_Location_Lat AS varchar(99))+')', 4326)
	WHERE Firm_Location_Lat IS NOT NULL AND Firm_Location_Long IS NOT NULL

	MERGE [alexabe_01].[dbo].[OtherAddress] WITH (HOLDLOCK) AS Target
	USING (SELECT
				FA.[Street_Address__c]
				,FA.[City__c]
				,FA.[State__c]
				,FA.[Country__c]
				--,FA.[Firm_Location__c]
				,FA.[Firm_Location_Lat]
				,FA.[Firm_Location_Long]
				,FA.[Address_Description__c]
				,FA.[Phone__c]
				,FA.[Fax__c]
				,FA.[Zip_Code__c]
				,FA.[Address_2__c]
				,FA.[DateImportedUtc]
				,FA.[Id]
				,O.Id OrganizationId
				,[Include] = CASE WHEN ISNULL(FA.[Address_Description__c], '') != 'Billing Headquarters' AND IsDeleted = 0 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			FROM [dbo].[FirmAddreses] FA
			LEFT JOIN [alexabe_01].dbo.Organization O ON FA.Account__c = O.SFID
	) AS Source 
		ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.OrganizationId = Source.OrganizationId,
		Target.[Address1] = Source.[Street_Address__c],
		Target.[City] = Source.[City__c],
		Target.[StateProvince] = Source.[State__c],
		Target.[Country] = Source.[Country__c],
		Target.[Phone] = Source.[Phone__c],
		Target.[Fax] = Source.[Fax__c],
		Target.[PostalCode] = Source.[Zip_Code__c],
		Target.[Address2] = Source.[Address_2__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[Latitude] = Source.[Firm_Location_Lat],
		Target.[Longitude] = Source.[Firm_Location_Long]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id
			,[Address1]
			,[City]
			,[StateProvince]
			,[Country]
			,[Phone]
			,[Fax]
			,[PostalCode]
			,[Address2]
			,[DateImportedUtc]
			,[SFID]
			,OrganizationId
			,[Latitude]
			,[Longitude]
	) 
		VALUES(	
				NEWID()
				,Source.[Street_Address__c]
				,Source.[City__c]
				,Source.[State__c]
				,Source.[Country__c]
				,Source.[Phone__c]
				,Source.[Fax__c]
				,Source.[Zip_Code__c]
				,Source.[Address_2__c]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.OrganizationId
				,Source.[Firm_Location_Lat]
				,Source.[Firm_Location_Long]
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFundMetrics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFundMetrics] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFundMetrics]
AS
BEGIN

	SET NOCOUNT ON;

	--UPDATE FM
	--SET IsMostRecent = CASE WHEN LatestFMs.MaxEffectiveDate IS NOT NULL THEN 1 ELSE 0 END
	--FROM FundMetrics FM
	--LEFT JOIN (
	--	SELECT FM2.AIMPortMgmt__Fund__c, MAX(FM2.[AIMPortMgmt__Effective_Date__c]) MaxEffectiveDate
	--	FROM FundMetrics FM2 
	--	GROUP BY FM2.AIMPortMgmt__Fund__c
	--) LatestFMs
	--	ON FM.AIMPortMgmt__Fund__c = LatestFMs.AIMPortMgmt__Fund__c AND FM.AIMPortMgmt__Effective_Date__c = LatestFMs.MaxEffectiveDate AND FM.Publish_Metric__c=1
	--		AND FM.IsDeleted=0

	-- Metrics marked w/ ImportStep=1 means that these metrics have been imported as Most Recent Fund Metrics
	UPDATE FundMetrics
	SET 
		IsMostRecent = CASE WHEN EXISTS (SELECT NULL FROM Funds WHERE Funds.MostRecentMetricSFID=FundMetrics.Id AND FundMetrics.ImportStep = 1) THEN 1 ELSE 0 END,
		[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Public_Metric_Source__c, '') != ''
		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
	FROM FundMetrics

	MERGE [alexabe_01].[dbo].[Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  fm.[AIMPortMgmt__Effective_Date__c]
		  ,fm.[Preqin_Called__c]
		  ,fm.[Preqin_DPI__c]
		  ,fm.[Preqin_TVPI__c]
		  ,fm.[Preqin_Net_IRR__c]
		  ,fm.[Preqin_Fund_Size__c]
--		  ,fm.[DateImportedUtc]
--		  ,fm.[Id]
		  --,o.[Id] AS [OrganizationId]
		  --,[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Publish_Metric__c, '') != ''
--		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
--		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
		  ,fm.[Include]
		  ,fm.[Invested_Capital__c]
		  ,fm.[Realized_Value__c]
		  ,fm.[Unrealized_Value__c]
		  ,fm.[Total_Value__c]
		  ,fm.[Fund_Total_IRR__c]
		  ,fm.[Fund_Total_Cash_on_Cash__c]
		  ,fm.[DPI__c]
		  ,fm.[Fund_Net_IRR__c]
		  ,fm.[Fund_Net_Cash_on_Cash__c]
		  ,fm.AIMPortMgmt__Fund__c FundSFID
		  ,Public_Metric_Source__c
		  --,IsMoreRecentThanExisting = CASE WHEN F.Id IS NULL THEN 0 ELSE 1 END
	  FROM [FundMetrics] fm
	  --LEFT JOIN [alexabe_01].[dbo].Fund F 
		--ON fm.AIMPortMgmt__Fund__c = F.SFID AND fm.[AIMPortMgmt__Effective_Date__c] >= CASE WHEN ISNULL(F.AsOf, '1/1/1900')>=ISNULL(F.PreqinAsOf, '1/1/1900') THEN F.AsOf ELSE F.PreqinAsOf END
--	  LEFT JOIN [alexabe_01].[dbo].[Organization] o ON fm.AIMPortMgmt__Account__c = o.SFID
--	  WHERE o.Id IS NOT NULL
	  WHERE IsMostRecent = 1
	  ) AS Source
	ON Target.SFID = Source.FundSFID

	WHEN MATCHED THEN
		UPDATE SET 
			Target.PreqinAsOf = CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
			Target.PreqinCalled = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c]	END,
			Target.PreqinDpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
			Target.PreqinTvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
			Target.PreqinIrr = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
			Target.PreqinFundSize = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c]	END,
--			Target.[DateImportedUtc] = CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc]	END,
			Target.InvestedCapital = CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
			Target.RealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
			Target.UnrealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
			Target.TotalValue = CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
			Target.GrossIrr = CASE 
								WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
								WHEN Source.[Include] = 0 THEN NULL
							END,
			Target.Moic = CASE 
							WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Dpi = CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
			Target.Irr = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
			Target.Tvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END,
			Target.AsOf = CASE 
							WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Source = Source.Public_Metric_Source__c
		;
/*
		AND Source.[Include] = 1 THEN UPDATE SET 
		Target.PreqinAsOf = Source.[AIMPortMgmt__Effective_Date__c],
		Target.PreqinCalled = Source.[Preqin_Called__c],
		Target.PreqinDpi = Source.[Preqin_DPI__c],
		Target.PreqinTvpi = Source.[Preqin_TVPI__c],
		Target.PreqinIrr = Source.[Preqin_Net_IRR__c],
		Target.PreqinFundSize = Source.[Preqin_Fund_Size__c],
--		Target.[DateImportedUtc] = Source.[DateImportedUtc],
--		Target.[OrganizationId] = Source.[OrganizationId],
		Target.InvestedCapital = Source.[Invested_Capital__c],
		Target.RealizedValue = Source.[Realized_Value__c],
		Target.UnrealizedValue = Source.[Unrealized_Value__c],
		Target.TotalValue = Source.[Total_Value__c],
		Target.GrossIrr = Source.[Fund_Total_IRR__c],
		Target.Moic = Source.[Fund_Total_Cash_on_Cash__c],
		Target.Dpi = Source.[DPI__c],
		Target.Irr = Source.[Fund_Net_IRR__c],
		Target.Tvpi = Source.[Fund_Net_Cash_on_Cash__c],
		Target.AsOf = Source.[AIMPortMgmt__Effective_Date__c]
*/


-- Inserts and deletes should not occur b/c they already have been handled by SP [SF_ImportFunds]

	--WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
	--		Id
	--		,PreqinAsOf
	--		,PreqinCalled
	--		,PreqinDpi
	--		,PreqinTvpi
	--		,PreqinIrr
	--		,PreqinFundSize
--	--		,DateImportedUtc
	--		,SFID
	--		,[OrganizationId]
	--		,InvestedCapital
	--		,RealizedValue
	--		,UnrealizedValue
	--		,TotalValue
	--		,GrossIrr
	--		,Moic
	--		,Dpi
	--		,Irr
	--		,Tvpi
	--		,AsOf
	--) 
	--	VALUES(	NEWID()
	--			,Source.[AIMPortMgmt__Effective_Date__c]
	--			,Source.[Preqin_Called__c]
	--			,Source.[Preqin_DPI__c]
	--			,Source.[Preqin_TVPI__c]
	--			,Source.[Preqin_Net_IRR__c]
	--			,Source.[Preqin_Fund_Size__c]
--	--			,Source.[DateImportedUtc]
	--			,Source.FundSFID--[Id]
	--			,Source.[OrganizationId]
	--			,Source.[Invested_Capital__c]
	--			,Source.[Realized_Value__c]
	--			,Source.[Unrealized_Value__c]
	--			,Source.[Total_Value__c]
	--			,Source.[Fund_Total_IRR__c]
	--			,Source.[Fund_Total_Cash_on_Cash__c]
	--			,Source.[DPI__c]
	--			,Source.[Fund_Net_IRR__c]
	--			,Source.[Fund_Net_Cash_on_Cash__c]
	--			,Source.[AIMPortMgmt__Effective_Date__c]

	--	)
	--WHEN MATCHED AND Source.[Include] = 0 THEN 
	--	DELETE;

	/*
	UPDATE alexabe_01.dbo.Fund
	SET PerformanceDataSource = CASE
		WHEN 
			GrossIrr IS NULL 
			AND Moic IS NULL 
			AND Dpi IS NULL 
			AND Irr IS NULL 
			AND Tvpi IS NULL
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	*/

	UPDATE F
	SET PerformanceDataSource = CASE
		--WHEN fm.Publish_Metric__c = 'Preqin'
		WHEN fm.Public_Metric_Source__c = 'Preqin'
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	FROM alexabe_01.dbo.Fund F
	JOIN [FundMetrics] fm ON 
		F.SFID = fm.AIMPortMgmt__Fund__c -- fm.AIMPortMgmt__Fund__c is SFID of Fund
		AND fm.[IsMostRecent] = 1

	UPDATE Fund
	SET
		Fund.PreqinAsOf = NULL,
		Fund.PreqinCalled = NULL,
		Fund.PreqinDpi = NULL,
		Fund.PreqinTvpi = NULL,
		Fund.PreqinIrr = NULL,
		Fund.PreqinFundSize = NULL,
--		Fund.[DateImportedUtc] = NULL,
--		Fund.[OrganizationId] = Source.[OrganizationId],
		Fund.InvestedCapital = NULL,
		Fund.RealizedValue = NULL,
		Fund.UnrealizedValue = NULL,
		Fund.TotalValue = NULL,
		Fund.GrossIrr = NULL,
		Fund.Moic = NULL,
		Fund.Dpi = NULL,
		Fund.Irr = NULL,
		Fund.Tvpi = NULL,
		Fund.AsOf = NULL,
		Fund.Source = NULL,
		Fund.[BenchmarkAsOf] = NULL,
		Fund.[BenchmarkDpiFirstQuartile] = NULL,
		Fund.[BenchmarkDpiMedian] = NULL,
		Fund.[BenchmarkDpiThirdQuartile] = NULL,

		Fund.[BenchmarkNetIrrFirstQuartile] = NULL,
		Fund.[BenchmarkNetIrrMedian] = NULL,
		Fund.[BenchmarkNetIrrThirdQuartile] = NULL,

		Fund.[BenchmarkTvpiFirstQuartile] = NULL,
		Fund.[BenchmarkTvpiMedian] = NULL,
		Fund.[BenchmarkTvpiThirdQuartile] = NULL
	FROM alexabe_01.dbo.Fund
	JOIN Funds ON Fund.SFID = Funds.Id AND Funds.MostRecentMetricSFID IS NULL


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM alexabe_01.dbo.Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='RCP' 
	OR F.Source = '' OR F.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM alexabe_01.dbo.Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='Preqin'
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFundMetrics_2016_03_12]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFundMetrics_2016_03_12] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFundMetrics_2016_03_12]
AS
BEGIN

	SET NOCOUNT ON;

	--UPDATE FM
	--SET IsMostRecent = CASE WHEN LatestFMs.MaxEffectiveDate IS NOT NULL THEN 1 ELSE 0 END
	--FROM FundMetrics FM
	--LEFT JOIN (
	--	SELECT FM2.AIMPortMgmt__Fund__c, MAX(FM2.[AIMPortMgmt__Effective_Date__c]) MaxEffectiveDate
	--	FROM FundMetrics FM2 
	--	GROUP BY FM2.AIMPortMgmt__Fund__c
	--) LatestFMs
	--	ON FM.AIMPortMgmt__Fund__c = LatestFMs.AIMPortMgmt__Fund__c AND FM.AIMPortMgmt__Effective_Date__c = LatestFMs.MaxEffectiveDate AND FM.Publish_Metric__c=1
	--		AND FM.IsDeleted=0

	-- Metrics marked w/ ImportStep=1 means that these metrics have been imported as Most Recent Fund Metrics
	UPDATE FundMetrics
	SET 
		IsMostRecent = CASE WHEN EXISTS (SELECT NULL FROM Funds WHERE Funds.MostRecentMetricSFID=FundMetrics.Id AND FundMetrics.ImportStep = 1) THEN 1 ELSE 0 END,
		[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Public_Metric_Source__c, '') != ''
		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
	FROM FundMetrics

	MERGE [alexabe_01].[dbo].[Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  fm.[AIMPortMgmt__Effective_Date__c]
		  ,fm.[Preqin_Called__c]
		  ,fm.[Preqin_DPI__c]
		  ,fm.[Preqin_TVPI__c]
		  ,fm.[Preqin_Net_IRR__c]
		  ,fm.[Preqin_Fund_Size__c]
--		  ,fm.[DateImportedUtc]
--		  ,fm.[Id]
		  --,o.[Id] AS [OrganizationId]
		  --,[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Publish_Metric__c, '') != ''
--		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
--		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
		  ,fm.[Include]
		  ,fm.[Invested_Capital__c]
		  ,fm.[Realized_Value__c]
		  ,fm.[Unrealized_Value__c]
		  ,fm.[Total_Value__c]
		  ,fm.[Fund_Total_IRR__c]
		  ,fm.[Fund_Total_Cash_on_Cash__c]
		  ,fm.[DPI__c]
		  ,fm.[Fund_Net_IRR__c]
		  ,fm.[Fund_Net_Cash_on_Cash__c]
		  ,fm.AIMPortMgmt__Fund__c FundSFID
		  ,Public_Metric_Source__c
		  --,IsMoreRecentThanExisting = CASE WHEN F.Id IS NULL THEN 0 ELSE 1 END
	  FROM [FundMetrics] fm
	  --LEFT JOIN [alexabe_01].[dbo].Fund F 
		--ON fm.AIMPortMgmt__Fund__c = F.SFID AND fm.[AIMPortMgmt__Effective_Date__c] >= CASE WHEN ISNULL(F.AsOf, '1/1/1900')>=ISNULL(F.PreqinAsOf, '1/1/1900') THEN F.AsOf ELSE F.PreqinAsOf END
--	  LEFT JOIN [alexabe_01].[dbo].[Organization] o ON fm.AIMPortMgmt__Account__c = o.SFID
--	  WHERE o.Id IS NOT NULL
	  WHERE IsMostRecent = 1
	  ) AS Source
	ON Target.SFID = Source.FundSFID

	WHEN MATCHED THEN
		UPDATE SET 
			Target.PreqinAsOf = CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
			Target.PreqinCalled = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c]	END,
			Target.PreqinDpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
			Target.PreqinTvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
			Target.PreqinIrr = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
			Target.PreqinFundSize = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c]	END,
--			Target.[DateImportedUtc] = CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc]	END,
			Target.InvestedCapital = CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
			Target.RealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
			Target.UnrealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
			Target.TotalValue = CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
			Target.GrossIrr = CASE 
								WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
								WHEN Source.[Include] = 0 THEN NULL
							END,
			Target.Moic = CASE 
							WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Dpi = CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
			Target.Irr = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
			Target.Tvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END,
			Target.AsOf = CASE 
							WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Source = Source.Public_Metric_Source__c
		;
/*
		AND Source.[Include] = 1 THEN UPDATE SET 
		Target.PreqinAsOf = Source.[AIMPortMgmt__Effective_Date__c],
		Target.PreqinCalled = Source.[Preqin_Called__c],
		Target.PreqinDpi = Source.[Preqin_DPI__c],
		Target.PreqinTvpi = Source.[Preqin_TVPI__c],
		Target.PreqinIrr = Source.[Preqin_Net_IRR__c],
		Target.PreqinFundSize = Source.[Preqin_Fund_Size__c],
--		Target.[DateImportedUtc] = Source.[DateImportedUtc],
--		Target.[OrganizationId] = Source.[OrganizationId],
		Target.InvestedCapital = Source.[Invested_Capital__c],
		Target.RealizedValue = Source.[Realized_Value__c],
		Target.UnrealizedValue = Source.[Unrealized_Value__c],
		Target.TotalValue = Source.[Total_Value__c],
		Target.GrossIrr = Source.[Fund_Total_IRR__c],
		Target.Moic = Source.[Fund_Total_Cash_on_Cash__c],
		Target.Dpi = Source.[DPI__c],
		Target.Irr = Source.[Fund_Net_IRR__c],
		Target.Tvpi = Source.[Fund_Net_Cash_on_Cash__c],
		Target.AsOf = Source.[AIMPortMgmt__Effective_Date__c]
*/


-- Inserts and deletes should not occur b/c they already have been handled by SP [SF_ImportFunds]

	--WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
	--		Id
	--		,PreqinAsOf
	--		,PreqinCalled
	--		,PreqinDpi
	--		,PreqinTvpi
	--		,PreqinIrr
	--		,PreqinFundSize
--	--		,DateImportedUtc
	--		,SFID
	--		,[OrganizationId]
	--		,InvestedCapital
	--		,RealizedValue
	--		,UnrealizedValue
	--		,TotalValue
	--		,GrossIrr
	--		,Moic
	--		,Dpi
	--		,Irr
	--		,Tvpi
	--		,AsOf
	--) 
	--	VALUES(	NEWID()
	--			,Source.[AIMPortMgmt__Effective_Date__c]
	--			,Source.[Preqin_Called__c]
	--			,Source.[Preqin_DPI__c]
	--			,Source.[Preqin_TVPI__c]
	--			,Source.[Preqin_Net_IRR__c]
	--			,Source.[Preqin_Fund_Size__c]
--	--			,Source.[DateImportedUtc]
	--			,Source.FundSFID--[Id]
	--			,Source.[OrganizationId]
	--			,Source.[Invested_Capital__c]
	--			,Source.[Realized_Value__c]
	--			,Source.[Unrealized_Value__c]
	--			,Source.[Total_Value__c]
	--			,Source.[Fund_Total_IRR__c]
	--			,Source.[Fund_Total_Cash_on_Cash__c]
	--			,Source.[DPI__c]
	--			,Source.[Fund_Net_IRR__c]
	--			,Source.[Fund_Net_Cash_on_Cash__c]
	--			,Source.[AIMPortMgmt__Effective_Date__c]

	--	)
	--WHEN MATCHED AND Source.[Include] = 0 THEN 
	--	DELETE;

	/*
	UPDATE alexabe_01.dbo.Fund
	SET PerformanceDataSource = CASE
		WHEN 
			GrossIrr IS NULL 
			AND Moic IS NULL 
			AND Dpi IS NULL 
			AND Irr IS NULL 
			AND Tvpi IS NULL
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	*/

	UPDATE F
	SET PerformanceDataSource = CASE
		--WHEN fm.Publish_Metric__c = 'Preqin'
		WHEN fm.Public_Metric_Source__c = 'Preqin'
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	FROM alexabe_01.dbo.Fund F
	JOIN [FundMetrics] fm ON 
		F.SFID = fm.AIMPortMgmt__Fund__c -- fm.AIMPortMgmt__Fund__c is SFID of Fund
		AND fm.[IsMostRecent] = 1

	UPDATE Fund
	SET
		Fund.PreqinAsOf = NULL,
		Fund.PreqinCalled = NULL,
		Fund.PreqinDpi = NULL,
		Fund.PreqinTvpi = NULL,
		Fund.PreqinIrr = NULL,
		Fund.PreqinFundSize = NULL,
--		Fund.[DateImportedUtc] = NULL,
--		Fund.[OrganizationId] = Source.[OrganizationId],
		Fund.InvestedCapital = NULL,
		Fund.RealizedValue = NULL,
		Fund.UnrealizedValue = NULL,
		Fund.TotalValue = NULL,
		Fund.GrossIrr = NULL,
		Fund.Moic = NULL,
		Fund.Dpi = NULL,
		Fund.Irr = NULL,
		Fund.Tvpi = NULL,
		Fund.AsOf = NULL,
		Fund.Source = NULL
	FROM alexabe_01.dbo.Fund
	JOIN Funds ON Fund.SFID = Funds.Id AND Funds.MostRecentMetricSFID IS NULL


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile)
	FROM alexabe_01.dbo.Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='RCP' 
	OR F.Source = '' OR F.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile)
	FROM alexabe_01.dbo.Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='Preqin'
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFunds]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFunds] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/09/2017 - Status = 'Assumed Closed / Unknown' will import as null
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFunds]
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Funds WHERE SBIC__c NOT IN ('No','Yes') AND SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Funds.SBIC__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE F SET NewFundId = NEWID()
	FROM dbo.Funds F
	WHERE NOT EXISTS (SELECT NULL FROM alexabe_01.dbo.Fund Target WHERE Target.SFID = F.Id)

	INSERT [alexabe_01].[dbo].[Entity] (Id, EntityType)
	SELECT Source.NewFundId, 2
	FROM dbo.Funds Source 
	WHERE Source.NewFundId IS NOT NULL AND Source.NewFundId NOT IN (SELECT Id FROM [alexabe_01].[dbo].[Entity] WHERE EntityType=2)
	

	MERGE [alexabe_01].[dbo].[Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			f.[Name]
			--,f.[AIM__Account__c]
			,o.[Id] AS [OrganizationId]
			--,f.[Currency__c]
			,c.[Id] AS [CurrencyId]
			,NULLIF(f.[Fundraising_Status__c], 'Assumed Closed / Unknown') [Fundraising_Status__c]
			,f.[Fund_Total_Cash_on_Cash__c]
			,f.[Fund_Total_IRR__c]
			,f.[Total_Deal_Professionals__c]
			,f.[FundTargetSize__c]
			,f.[FundNumber__c]
			--,(CASE 
			--	WHEN NULLIF(f.[First_Drawn_Capital__c], '') IS NOT NULL THEN YEAR(f.[First_Drawn_Capital__c])
			--	ELSE YEAR(f.[FundLaunchDate__c])
			--	END) AS [VintageYear]
			,f.[Vintage_Year__c] AS [VintageYear]
			,f.[Eff_Size__c]
			,f.[Deal_Table_URL__c]
			,f.[Effective_Date__c]
			,f.[NewFundId]
			,[Include] = CASE WHEN IsDeleted = 0 AND Publish__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			,f.[DateImportedUtc]
			,f.[Id]
	  FROM	[dbo].[Funds] f
			LEFT JOIN [alexabe_01].[dbo].[Organization] o ON f.[AIM__Account__c] = o.[SFID]
			LEFT JOIN [alexabe_01].[dbo].[Currency] c ON f.[Currency__c] = c.[Name]
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
				Target.[Name] = Source.[Name],
				Target.[OrganizationId] = Source.[OrganizationId],
				Target.[CurrencyId] = Source.[CurrencyId],
				Target.[Status] = Source.[Fundraising_Status__c],
				Target.[Moic] = Source.[Fund_Total_Cash_on_Cash__c],
				Target.[GrossIrr] = Source.[Fund_Total_IRR__c],
				-- join with organization and set organization.NumberOfInvestmentProfessionals = Source.[Total_Deal_Professionals__c],
				Target.[TargetSize] = Source.[FundTargetSize__c],
				Target.[SortOrder] = Source.[FundNumber__c],
				Target.[VintageYear] = Source.[VintageYear],
				Target.[FundSize] = Source.[Eff_Size__c],
				Target.[AnalysisUrl] = Source.[Deal_Table_URL__c],
				Target.[AsOf] = Source.[Effective_Date__c],
				Target.[DateImportedUtc] = Source.[DateImportedUtc]
	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
				[Id]
				,[Name]
				,[OrganizationId]
				,[CurrencyId]
				,[Status]
				,[Moic]
				,[GrossIrr]
				,[TargetSize]
				,[SortOrder]
				,[VintageYear]
				,[FundSize]
				,[AnalysisUrl]
				,[AsOf]
				,[DateImportedUtc]
				,[SFID]
	) 
		VALUES(	
				Source.[NewFundId]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[CurrencyId]
				,Source.[Fundraising_Status__c]
				,Source.[Fund_Total_Cash_on_Cash__c]
				,Source.[Fund_Total_IRR__c]
				,Source.[FundTargetSize__c]
				,Source.[FundNumber__c]
				,Source.[VintageYear]
				,Source.[Eff_Size__c]
				,Source.[Deal_Table_URL__c]
				,Source.[Effective_Date__c]
				,Source.[DateImportedUtc]
				,Source.[Id])
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;	

	COMMIT TRAN

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFurtherFundMetrics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFurtherFundMetrics] AS' 
END
GO

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	This logic imports not most recent rcp fund metrics
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFurtherFundMetrics]
AS


DELETE fm
FROM alexabe_01.dbo.FurtherFundMetrics fm
JOIN alexabe_01.dbo.Fund f ON fm.FundId = f.Id
JOIN Funds stgFunds ON f.SFID = stgFunds.Id

INSERT alexabe_01.dbo.FurtherFundMetrics (
		Id, FundId, 
		PreqinAsOf, 
		[PreqinCalled], 
		[PreqinDpi],
		PreqinTvpi, 
		[PreqinIrr], 
		[PreqinFundSize], 
		[InvestedCapital],
		[RealizedValue], 
		[UnrealizedValue], 
		[TotalValue], 
		[GrossIrr], 
		[Moic], 
		[Dpi], 
		[Irr], 
		[Tvpi], 
		[AsOf], 
		[DateImportedUtc], 
		[SFID], 
		[Source]--, 
		--[DPIQuartile], 
		--[TVPIQuartile], 
		--[NetIrrQuartile]
)
SELECT NEWID(), F.Id FundID, 
	CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
	CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END, -- tvpi
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc] END,
	Source.Id, -- SFID
	Source.Public_Metric_Source__c
FROM FundMetrics Source 
JOIN Funds StgFunds ON StgFunds.Id = Source.[AIMPortMgmt__Fund__c]
JOIN alexabe_01.dbo.Fund F ON StgFunds.Id = F.SFID
WHERE Source.ImportStep IS NULL AND ISNULL(Source.IsMostRecent, 0) !=1 -- these 2 criterias for now are the same


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM alexabe_01.dbo.FurtherFundMetrics fm
LEFT JOIN alexabe_01.dbo.Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='RCP'
OR fm.Source = '' OR fm.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM alexabe_01.dbo.FurtherFundMetrics fm
LEFT JOIN alexabe_01.dbo.Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='Preqin'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportFurtherFundMetrics_2016_03_12]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportFurtherFundMetrics_2016_03_12] AS' 
END
GO

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	This logic imports not most recent rcp fund metrics
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFurtherFundMetrics_2016_03_12]
AS


DELETE fm
FROM alexabe_01.dbo.FurtherFundMetrics fm
JOIN alexabe_01.dbo.Fund f ON fm.FundId = f.Id
JOIN Funds stgFunds ON f.SFID = stgFunds.Id

INSERT alexabe_01.dbo.FurtherFundMetrics (
		Id, FundId, 
		PreqinAsOf, 
		[PreqinCalled], 
		[PreqinDpi],
		PreqinTvpi, 
		[PreqinIrr], 
		[PreqinFundSize], 
		[InvestedCapital],
		[RealizedValue], 
		[UnrealizedValue], 
		[TotalValue], 
		[GrossIrr], 
		[Moic], 
		[Dpi], 
		[Irr], 
		[Tvpi], 
		[AsOf], 
		[DateImportedUtc], 
		[SFID], 
		[Source]--, 
		--[DPIQuartile], 
		--[TVPIQuartile], 
		--[NetIrrQuartile]
)
SELECT NEWID(), F.Id FundID, 
	CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
	CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END, -- tvpi
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc] END,
	Source.Id, -- SFID
	Source.Public_Metric_Source__c
FROM FundMetrics Source 
JOIN Funds StgFunds ON StgFunds.Id = Source.[AIMPortMgmt__Fund__c]
JOIN alexabe_01.dbo.Fund F ON StgFunds.Id = F.SFID
WHERE Source.ImportStep IS NULL AND ISNULL(Source.IsMostRecent, 0) !=1 -- these 2 criterias for now are the same

UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile)
FROM alexabe_01.dbo.FurtherFundMetrics fm
LEFT JOIN alexabe_01.dbo.Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='RCP'
OR fm.Source = '' OR fm.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile)
FROM alexabe_01.dbo.FurtherFundMetrics fm
LEFT JOIN alexabe_01.dbo.Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN alexabe_01.dbo.Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='Preqin'



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportGPScoutScorecards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportGPScoutScorecards] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportGPScoutScorecards]
AS
BEGIN
	SET NOCOUNT ON;

	-- removing Grades/Scorecards from organizations new Scorecard data has been imported for
	DELETE G
	FROM [alexabe_01].[dbo].[Grade] G
	JOIN [alexabe_01].[dbo].Organization O ON G.OrganizationId = O.Id
	WHERE O.SFID IN (SELECT GPScout_Scorecard__c FROM Ids WHERE GPScout_Scorecard__c IS NOT NULL)


	MERGE [alexabe_01].[dbo].[Grade] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		dbo.udf_GetGrade([Expertise_Aligned_with_Strategy__c]) AS [Expertise_Aligned_with_Strategy__c]
		,dbo.udf_GetGrade([History_Together__c]) AS [History_Together__c]
		,dbo.udf_GetGrade([Complementary_Skills__c]) AS [Complementary_Skills__c]
		,dbo.udf_GetGrade([Team_Depth__c]) AS [Team_Depth__c]
		,dbo.udf_GetGrade([Value_Add_Resources__c]) AS [Value_Add_Resources__c]
		,dbo.udf_GetGrade([Investment_Thesis__c]) AS [Investment_Thesis__c]
		,dbo.udf_GetGrade([Competitive_Advantage__c]) AS [Competitive_Advantage__c]
		,dbo.udf_GetGrade([Portfolio_Construction__c]) AS [Portfolio_Construction__c]
		,dbo.udf_GetGrade([Appropriateness_of_Fund_Size__c]) AS [Appropriateness_of_Fund_Size__c]
		,dbo.udf_GetGrade([Consistency_of_Strategy__c]) AS [Consistency_of_Strategy__c]
		,dbo.udf_GetGrade([Sourcing__c]) AS [Sourcing__c]
		,dbo.udf_GetGrade([Due_Diligence__c]) AS [Due_Diligence__c]
		,dbo.udf_GetGrade([Decision_Making__c]) AS [Decision_Making__c]
		,dbo.udf_GetGrade([Deal_Execution_Structure__c]) AS [Deal_Execution_Structure__c]
		,dbo.udf_GetGrade([Post_Investment_Value_add__c]) AS [Post_Investment_Value_add__c]
		,dbo.udf_GetGrade([Team_Stability__c]) AS [Team_Stability__c]
		,dbo.udf_GetGrade([Ownership_and_Compensation__c]) AS [Ownership_and_Compensation__c]
		,dbo.udf_GetGrade([Culture__c]) AS [Culture__c]
		,dbo.udf_GetGrade([Alignment_with_LPs__c]) AS [Alignment_with_LPs__c]
		,dbo.udf_GetGrade([Terms__c]) AS [Terms__c]
		,s.[Scaled_Score__c]
		,dbo.udf_GetGrade([Absolute_Performance__c]) AS [Absolute_Performance__c]
		,dbo.udf_GetGrade([Relative_Performance__c]) AS [Relative_Performance__c]
		,dbo.udf_GetGrade([Realized_Performance__c]) AS [Realized_Performance__c]
		,dbo.udf_GetGrade([Depth_of_Track_Record__c]) AS [Depth_of_Track_Record__c]
		,dbo.udf_GetGrade([Relevance_of_Track_Record__c]) AS [Relevance_of_Track_Record__c]
		,dbo.udf_GetGrade([Loss_Ratio_Analysis__c]) AS [Loss_Ratio_Analysis__c]
		,dbo.udf_GetGrade([Unrealized_Portfolio__c]) AS [Unrealized_Portfolio__c]
		,dbo.udf_GetGrade([RCP_Value_Creation_Analysis__c]) AS [RCP_Value_Creation_Analysis__c]
		,dbo.udf_GetGrade([RCP_Hits_Misses__c]) AS [RCP_Hits_Misses__c]
		,dbo.udf_GetGrade([Deal_Size__c]) AS [Deal_Size__c]
		,dbo.udf_GetGrade([Strategy__c]) AS [Strategy__c]
		,dbo.udf_GetGrade([Time__c]) AS [Time__c]
		,dbo.udf_GetGrade([Team__c]) AS [Team__c]
		  --,s.[Qualitative_Final_Grade__c]
		  --,s.[Quantitative_Final_Grade__c]
		  --,s.[Quantitative_Final_Number__c]
		  ,s.[Date_Reviewed__c]
		  ,s.[DateImportedUtc]
		  ,s.[Id]
		  ,o.[Id] AS [OrganizationId]
		  ,[Include] = CASE WHEN IsDeleted = 0 AND Publish_Scorecard__c = 1 AND ParentScorecard_Publish_Level__c IS NOT NULL AND ParentScorecard_IsDeleted = 0 THEN 1 ELSE 0 END
	  FROM [dbo].[GPScoutScorecards] s
	  LEFT JOIN [alexabe_01].[dbo].[Organization] o ON s.GPScout_Scorecard__c = o.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.ExpertiseAlignedWithStrategy = Source.[Expertise_Aligned_with_Strategy__c],
		Target.HistoryTogether = Source.[History_Together__c],
		Target.ComplementarySkills = Source.[Complementary_Skills__c],
		Target.TeamDepth = Source.[Team_Depth__c],
		Target.ValueAddResources = Source.[Value_Add_Resources__c],
		Target.InvestmentThesis = Source.[Investment_Thesis__c],
		Target.CompetitiveAdvantage = Source.[Competitive_Advantage__c],
		Target.PortfolioConstruction = Source.[Portfolio_Construction__c],
		Target.AppropriatenessOfFundSize = Source.[Appropriateness_of_Fund_Size__c],
		Target.ConsistencyOfStrategy = Source.[Consistency_of_Strategy__c],
		Target.Sourcing = Source.[Sourcing__c],
		Target.DueDiligence = Source.[Due_Diligence__c],
		Target.DecisionMaking = Source.[Decision_Making__c],
		Target.DealExecutionStructure = Source.[Deal_Execution_Structure__c],
		Target.PostInvestmentValueAdd = Source.[Post_Investment_Value_add__c],
		Target.TeamStability = Source.[Team_Stability__c],
		Target.OwnershipAndCompensation = Source.[Ownership_and_Compensation__c],
		Target.Culture = Source.[Culture__c],
		Target.AlignmentWithLPs = Source.[Alignment_with_LPs__c],
		Target.Terms = Source.[Terms__c],
		--Target. = Source.[Scaled_Score__c], -- Organization.QualitativeGradeNumber = Source.[Scaled_Score__c] -- Handled in SF_PostImport 
		Target.AbsolutePerformance = Source.[Absolute_Performance__c],
		Target.RelativePerformance = Source.[Relative_Performance__c],
		Target.RealizedPerformance = Source.[Realized_Performance__c],
		Target.DepthOfTrackRecord = Source.[Depth_of_Track_Record__c],
		Target.RelevanceOfTrackRecord = Source.[Relevance_of_Track_Record__c],
		Target.LossRatioAnalysis = Source.[Loss_Ratio_Analysis__c],
		Target.UnrealizedPortfolio = Source.[Unrealized_Portfolio__c],
		Target.AtlasValueCreationAnalysis = Source.[RCP_Value_Creation_Analysis__c],
		Target.AtlasHitsAndMisses = Source.[RCP_Hits_Misses__c],
		Target.DealSize = Source.[Deal_Size__c],
		Target.Strategy = Source.[Strategy__c],
		Target.[Time] = Source.[Time__c],
		Target.Team = Source.[Team__c],
		--Target. = Source.[Qualitative_Final_Grade__c], -- seems to be Organization.QualitativeGrade			-- Handled in SF_PostImport
		--Target. = Source.[Quantitative_Final_Grade__c], -- seems to be Organization.QuantitativeGrade			-- Handled in SF_PostImport
		--Target. = Source.[Quantitative_Final_Number__c]  -- seems to be Organization.QuantitativeGradeNumber	-- Handled in SF_PostImport
		Target.[DateReviewed] = Source.[Date_Reviewed__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[OrganizationId] = Source.[OrganizationId]
	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id,
			ExpertiseAlignedWithStrategy,
			HistoryTogether,
			ComplementarySkills,
			TeamDepth,
			ValueAddResources,
			InvestmentThesis,
			CompetitiveAdvantage,
			PortfolioConstruction,
			AppropriatenessOfFundSize,
			ConsistencyOfStrategy,
			Sourcing,
			DueDiligence,
			DecisionMaking,
			DealExecutionStructure,
			PostInvestmentValueAdd,
			TeamStability,
			OwnershipAndCompensation,
			Culture,
			AlignmentWithLPs,
			Terms,
			AbsolutePerformance,
			RelativePerformance,
			RealizedPerformance,
			DepthOfTrackRecord,
			RelevanceOfTrackRecord,
			LossRatioAnalysis,
			UnrealizedPortfolio,
			AtlasValueCreationAnalysis,
			AtlasHitsAndMisses,
			DealSize,
			Strategy,
			[Time],
			Team,
			[DateReviewed],
			[DateImportedUtc],
			[SFID],
			[OrganizationId]
	) 
		VALUES(	NEWID(),
				Source.[Expertise_Aligned_with_Strategy__c],
				Source.[History_Together__c],
				Source.[Complementary_Skills__c],
				Source.[Team_Depth__c],
				Source.[Value_Add_Resources__c],
				Source.[Investment_Thesis__c],
				Source.[Competitive_Advantage__c],
				Source.[Portfolio_Construction__c],
				Source.[Appropriateness_of_Fund_Size__c],
				Source.[Consistency_of_Strategy__c],
				Source.[Sourcing__c],
				Source.[Due_Diligence__c],
				Source.[Decision_Making__c],
				Source.[Deal_Execution_Structure__c],
				Source.[Post_Investment_Value_add__c],
				Source.[Team_Stability__c],
				Source.[Ownership_and_Compensation__c],
				Source.[Culture__c],
				Source.[Alignment_with_LPs__c],
				Source.[Terms__c],
				Source.[Absolute_Performance__c],
				Source.[Relative_Performance__c],
				Source.[Realized_Performance__c],
				Source.[Depth_of_Track_Record__c],
				Source.[Relevance_of_Track_Record__c],
				Source.[Loss_Ratio_Analysis__c],
				Source.[Unrealized_Portfolio__c],
				Source.[RCP_Value_Creation_Analysis__c],
				Source.[RCP_Hits_Misses__c],
				Source.[Deal_Size__c],
				Source.[Strategy__c],
				Source.[Time__c],
				Source.[Team__c],
				Source.[Date_Reviewed__c],
				Source.[DateImportedUtc],
				Source.[Id],
				Source.[OrganizationId])
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportHelper]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportHelper] AS' 
END
GO

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	Db level actions called during the data import process
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportHelper]
	@Mode varchar(50),
	@ImportStep tinyint = NULL
AS

IF @Mode = 'SetFundMetricsImportStep' 
	UPDATE [dbo].[FundMetrics] SET ImportStep = @ImportStep

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportStrenghtsAndConcerns]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportStrenghtsAndConcerns] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportStrenghtsAndConcerns]

AS
BEGIN

	SET NOCOUNT ON;

	MERGE [alexabe_01].[dbo].[Evaluation] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			CONVERT(int, ISNULL(sc.[Display_Order__c], 999)) Display_Order__c
			,sc.[Account__c]
			,(CASE 
				WHEN sc.[Type__c] LIKE '%Strength%' OR sc.[Type__c] IS NULL THEN 0
				WHEN sc.[Type__c] LIKE '%Concern%' THEN 1
			END) AS [Type]
			,sc.[Name__c]
			,sc.[External_Text__c]
			,sc.[DateImportedUtc]
			,sc.[Id]
			,o.[Id] AS [OrganizationId]
			,[Include] = CASE WHEN IsDeleted = 0 AND Publish__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
	  FROM [dbo].[StrengthsAndConcerns] sc
	  LEFT JOIN [alexabe_01].[dbo].[Organization] o ON sc.Account__c = o.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[SortOrder] = Source.[Display_Order__c],
		Target.[OrganizationId] = Source.[OrganizationId],
		Target.[Type] = Source.[Type],
		Target.[Description] = Source.[Name__c],
		Target.[Note] = Source.[External_Text__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc]
	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			[Id]
			,[SortOrder]
			,[OrganizationId]
			,[Type]
			,[Description]
			,[Note]
			,[DateImportedUtc]
			,[SFID]
	) 
		VALUES(	NEWID()
				,Source.[Display_Order__c]
				,Source.[OrganizationId]
				,Source.[Type]
				,Source.[Name__c]
				,Source.[External_Text__c]
				,Source.[DateImportedUtc]
				,Source.Id)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_ImportTrackRecords]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_ImportTrackRecords] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportTrackRecords]
AS
	SET NOCOUNT ON;

	MERGE [alexabe_01].[dbo].[TrackRecord] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			[Description__c],
			[File_Name__c],
			[Display_Order__c],
			tr.[Name],
			[Account__c],
			tr.[DateImportedUtc],
			tr.[Id],
			o.[Id] AS [OrganizationId],
			[Include] = CASE WHEN IsDeleted = 0 AND Publish__c= 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
	  FROM [dbo].[TrackRecords] tr
	  LEFT JOIN [alexabe_01].[dbo].[Organization] o ON tr.Account__c = o.SFID

	  ) AS Source 
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Description] = Source.[Description__c],
		Target.[FileUrl] = Source.[File_Name__c],
		Target.[Order] = Source.[Display_Order__c],
		Target.[Title] = Source.[Name],
		Target.[OrganizationId] = Source.[OrganizationId],
		Target.[DateImportedUtc] = Source.[DateImportedUtc]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
		Id,
		[Description],
		[FileUrl],
		[Order],
		[Title],
		[OrganizationId],
		[DateImportedUtc],
		SFID
		) 
		VALUES(	NEWID()
				,Source.[Description__c]
				,Source.[File_Name__c]
				,Source.[Display_Order__c]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[DateImportedUtc]
				,Source.[Id]
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostFullImport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_PostFullImport] AS' 
END
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_PostFullImport]
AS
BEGIN
	SET NOCOUNT ON;

BEGIN TRY 

BEGIN TRAN

	--FolderOrganization
	INSERT INTO alexabe_01.dbo.FolderOrganization(FolderId,OrganizationId)
	select	old.FolderId,neworg.Id
	from	alexabe_01_before_SF.dbo.FolderOrganization old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name

	--GroupOrganization
	INSERT INTO alexabe_01.dbo.GroupOrganization(GroupId,OrganizationId)
	select	old.GroupId,neworg.Id
	from	alexabe_01_before_SF.dbo.GroupOrganization old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name
		

	--OrganizationNote
	INSERT INTO alexabe_01.dbo.OrganizationNote(Id,UserId,OrganizationId,Note,DateEntered,GroupId,EditedByUserId,EditedByDate)
	select	old.Id,old.UserId,neworg.Id,old.Note,old.DateEntered,old.GroupId,old.EditedByUserId,old.EditedByDate
	from	alexabe_01_before_SF.dbo.OrganizationNote old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name


	--OrganizationRequest
	INSERT INTO alexabe_01.dbo.OrganizationRequest(Id,OrganizationId,OrganizationName,Comment,UserId,CreatedOn,IsActive,[Type])
	select	old.Id,neworg.Id,old.OrganizationName,old.Comment,old.UserId,old.CreatedOn,old.IsActive,old.[Type]
	from	alexabe_01_before_SF.dbo.OrganizationRequest old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name


	--OrganizationViewing
	INSERT INTO alexabe_01.dbo.OrganizationViewing(Id,OrganizationId,UserId,[TimeStamp])
	select	old.Id,neworg.Id,old.UserId,old.[TimeStamp]
	from	alexabe_01_before_SF.dbo.OrganizationViewing old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name


	--TrackUsersProfileView
	INSERT INTO alexabe_01.dbo.TrackUsersProfileView(Id,TrackUsersId,OrganizationId)
	select	old.Id,old.TrackUsersId,neworg.Id
	from	alexabe_01_before_SF.dbo.TrackUsersProfileView old
			join alexabe_01_before_SF.dbo.MergeInternalEntities org on old.OrganizationId = org.OrganizationId
			join alexabe_01.dbo.Organization neworg on org.OrganizationName = neworg.Name
	where	old.TrackUsersId NOT IN (SELECT Id FROM alexabe_01.dbo.TrackUsers) --TODO: this is not needed in production

COMMIT TRAN

END TRY
BEGIN CATCH
	ROLLBACK TRAN
	-- THROW
    -- if THROW statement is not available:    

    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostImport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_PostImport] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		05/04/2017 SFP ZK	- MostRecentFundCountries update not used anymore
-- =============================================
-- EXEC [SF_PostImport] '5/27/2015 3:36:05 PM'  
ALTER PROCEDURE [dbo].[SF_PostImport]
	@DateImportedUtc datetime
AS


DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;


BEGIN TRY 

BEGIN TRAN

UPDATE Accounts SET DateImportedUtc = @DateImportedUtc
UPDATE Benchmarks SET DateImportedUtc = @DateImportedUtc
UPDATE Contacts SET DateImportedUtc = @DateImportedUtc
UPDATE FirmAddreses SET DateImportedUtc = @DateImportedUtc
UPDATE TrackRecords SET DateImportedUtc = @DateImportedUtc
UPDATE Funds SET DateImportedUtc = @DateImportedUtc
UPDATE FundMetrics SET DateImportedUtc = @DateImportedUtc
--UPDATE Metrics SET DateImportedUtc = @DateImportedUtc
UPDATE StrengthsAndConcerns SET DateImportedUtc = @DateImportedUtc
UPDATE GPScoutScorecards SET DateImportedUtc = @DateImportedUtc

--UPDATE Accounts SET Most_Recent_Fund_Currency__c = 'US Dollar' WHERE Most_Recent_Fund_Currency__c IS NULL OR Most_Recent_Fund_Currency__c=''
--UPDATE Funds SET Currency__c = 'US Dollar' WHERE Currency__c IS NULL OR Currency__c=''
UPDATE Accounts SET Most_Recent_Fund_Currency__c = 'USD' WHERE Most_Recent_Fund_Currency__c IS NULL OR Most_Recent_Fund_Currency__c=''
UPDATE Funds SET Currency__c = 'USD' WHERE Currency__c IS NULL OR Currency__c=''

-- store Org Id-s being in In Process mode before data import
DELETE Ids WHERE OrgInProcessBeforeImport IS NOT NULL
INSERT IDs (OrgInProcessBeforeImport)
SELECT Id FROM [alexabe_01].[dbo].Organization WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '')


INSERT INTO [alexabe_01].[dbo].Currency(Id, Code, Name, Symbol)
SELECT NEWID(), 'n/a', CurrencyName, 'n/a'
FROM (
	SELECT DISTINCT ISNULL(CurrencyName, '') CurrencyName
	FROM (
	SELECT Currency__c CurrencyName
	FROM Funds
	UNION ALL
	SELECT Most_Recent_Fund_Currency__c 
	FROM Accounts
	) C2
) C
WHERE C.CurrencyName NOT IN (SELECT C3.Name FROM [alexabe_01].[dbo].Currency C3)

PRINT 'EXEC '
EXEC SF_ImportBenchmarks

/*
TRUNCATE TABLE Benchmarks_Last

INSERT INTO Benchmarks_Last (Id, VintageYear, AsOf, 
	[DpiFirstQuartile], [DpiMedian], [DpiThirdQuartile],
	[NetIrrFirstQuartile], [NetIrrMedian], [NetIrrThirdQuartile],
	[TvpiFirstQuartile], [TvpiMedian], [TvpiThirdQuartile])
SELECT 
	B.Id, B.VintageYear, B.AsOf,
	B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile,
	B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile,
	B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile
FROM (
	SELECT MAX(B2.AsOf) MaxAsOf, VintageYear 
	FROM alexabe_01.dbo.Benchmarks B2 
	GROUP BY B2.VintageYear
) A
JOIN alexabe_01.dbo.Benchmarks B ON A.VintageYear = B.VintageYear AND A.MaxAsOf = B.AsOf
*/

PRINT 'EXEC SF_ImportAccounts:'
EXEC SF_ImportAccounts

PRINT 'EXEC SF_ImportFunds:'
EXEC SF_ImportFunds
--EXEC SF_ImportMetrics

PRINT 'EXEC SF_ImportFundMetrics:'
EXEC SF_ImportFundMetrics

PRINT 'EXEC SF_ImportFurtherFundMetrics:'
EXEC SF_ImportFurtherFundMetrics

PRINT 'EXEC SF_ImportContacts:'
EXEC SF_ImportContacts

PRINT 'EXEC SF_ImportFirmAddresses:'
EXEC SF_ImportFirmAddresses

PRINT 'EXEC SF_ImportTrackRecords:'
EXEC SF_ImportTrackRecords

PRINT 'EXEC SF_ImportStrenghtsAndConcerns:'
EXEC SF_ImportStrenghtsAndConcerns

PRINT 'EXEC SF_ImportGPScoutScorecards:'
EXEC SF_ImportGPScoutScorecards

PRINT 'EXEC [SF_PostImport_FirmAddresses]:'
EXEC [dbo].[SF_PostImport_FirmAddresses]

-- Handled in SP [SF_ImportGPScoutScorecards] instead
--DELETE	G
--FROM	(
--		SELECT	ROW_NUMBER() OVER (PARTITION BY OrganizationId ORDER BY [DateReviewed] DESC) AS RecID 
--		FROM	alexabe_01.dbo.Grade
--		) AS G
--WHERE	RecID > 1 


-- insert Grade record for Organizations not having at least one Grade record. This is required b/c we need to store some Organization level info at Grade level anyway. See next update statement.
INSERT [alexabe_01].[dbo].Grade (Id, OrganizationId, DateImportedUtc, SFID)
SELECT NEWID(), O.Id, @DateImportedUtc, ''
FROM [alexabe_01].[dbo].Organization O
WHERE O.Id NOT IN (SELECT G.OrganizationId FROM [alexabe_01].[dbo].Grade G) 


UPDATE G
SET
	G.ScatterchartText = A.Grade_Scatterchart_Text__c,
	G.AQScorecardText = A.Grade_Qualitative_Text__c,
	G.QoRScorecardText = A.Grade_Quantitative_Text__c
FROM [alexabe_01].[dbo].[Grade] G
JOIN [alexabe_01].[dbo].Organization O ON G.OrganizationId = O.Id
JOIN dbo.Accounts A ON O.SFID = A.Id


UPDATE O
SET 
	O.QualitativeGradeNumber = SC.Scaled_Score__c,
	O.QualitativeGrade = dbo.udf_GetGradeWord(SC.Scaled_Score__c), --  SC.Qualitative_Final_Grade__c,
	O.QuantitativeGrade = dbo.udf_GetGradeWord(SC.Quantitative_Final_Number__c), -- SC.Quantitative_Final_Grade__c,
	O.QuantitativeGradeNumber = SC.Quantitative_Final_Number__c
FROM [alexabe_01].[dbo].Organization O
--JOIN GPScoutScoreCards SC ON O.SFID = SC.[GPScout_Scorecard__c]
JOIN Accounts A ON O.SFID = A.Id
LEFT JOIN GPScoutScoreCards SC ON A.[MostRecentScorecardSFID] = SC.[Id] AND SC.IsDeleted = 0 AND SC.Publish_Scorecard__c = 1


UPDATE [alexabe_01].[dbo].Organization
SET FundRaisingStatus = 'Fundless Sponsor' WHERE FundRaisingStatus IN ('Pledge Fund - Active', 'Pledge Fund - Inactive')


-- set currency to USD for Organizations whose CurrencyId is null
DECLARE @CurrencyId UNIQUEIDENTIFIER
SET		@CurrencyId = (SELECT Id FROM [alexabe_01].[dbo].[Currency] WHERE Name = 'USD')
UPDATE	[alexabe_01].[dbo].[Organization]
SET		CurrencyId = @CurrencyId
WHERE	CurrencyId IS NULL


-- set list of countries assigned to each Organization through their list of firm addresses
--UPDATE O
--SET CountriesPipeDelimited = STUFF((
--	SELECT '|' + Country FROM (
--		SELECT DISTINCT Country FROM (
--			SELECT DISTINCT OA.Country FROM alexabe_01.dbo.OtherAddress OA WHERE OA.OrganizationId = O.Id
--			UNION ALL
--			SELECT O.Address1Country
--		) B
--	) A FOR XML PATH(''))
--,1,1,'')
--FROM alexabe_01.dbo.Organization O 
--WHERE O.DateImportedUtc = @DateImportedUtc


---- set list of countries assigned to each Organization from list of countries of most recent fund of the Organization
--UPDATE O
--SET O.CountriesPipeDelimited = dbo.udf_ToPipeDelimitedValue(A.MostRecentFundCountries, ';')
--FROM Accounts A 
--JOIN alexabe_01.dbo.Organization O ON A.Id = O.SFID AND A.Include=1


-- delete entity multiple values for Organizations and Funds that were imported. They will be (re)created by post-import c# code
DELETE EV
FROM [alexabe_01].[dbo].Organization O
JOIN [alexabe_01].[dbo].EntityMultipleValues EV ON O.SFID IS NOT NULL AND O.DateImportedUtc = @DateImportedUtc AND O.Id = EV.EntityId AND EV.Field = 1

DELETE EV
FROM [alexabe_01].[dbo].Fund F
JOIN [alexabe_01].[dbo].EntityMultipleValues EV ON F.SFID IS NOT NULL AND F.DateImportedUtc = @DateImportedUtc AND F.Id = EV.EntityId AND EV.Field = 2

-- drop orphan Entities belonging to Funds
SELECT E.Id INTO #EntitiesToDelete
FROM alexabe_01.dbo.Entity E
WHERE E.EntityType = 2 AND E.Id NOT IN (SELECT F.Id FROM alexabe_01.dbo.Fund F)

DELETE alexabe_01.dbo.EntityMultipleValues WHERE EntityId IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)
DELETE alexabe_01.dbo.Entity WHERE Id IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)

DROP TABLE #EntitiesToDelete

-- backend code will re-populate
TRUNCATE TABLE alexabe_01.dbo.SearchOptions

COMMIT TRAN

END TRY
BEGIN CATCH
	-- ROLLBACK TRAN;
	-- THROW
    -- if THROW statement is not available:    

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
ROLLBACK TRAN
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH


BEGIN TRY
	EXEC alexabe_01.dbo.[usp_Util_ReIndexDatabase_UpdateStats]
END TRY
BEGIN CATCH
	-- THROW
    -- if THROW statement is not available:    

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_PostImport_FirmAddresses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_PostImport_FirmAddresses] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_PostImport_FirmAddresses] 
AS
	UPDATE	O
	SET		Address1City = NULL,
			-- Address1Country = NULL,
			Address1 = NULL,
			Address2 = NULL,
			PostalCode = NULL,
			StateProvince = NULL,
			PrimaryOffice = NULL,
			Latitude = NULL,
			Longitude = NULL
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.PrimaryOfficeSFID = FA.Id
	WHERE ISNULL(FA.[Address_Description__c], '') NOT IN ('Headquarters', 'Billing Headquarters')

	UPDATE	O
	SET		Address1City = FA.City__c,
			Address1Country = FA.Country__c,
			Address1 = FA.Street_Address__c,
			Address2 = FA.Address_2__c,
			PostalCode = FA.Zip_Code__c,
			StateProvince = FA.State__c,
			PrimaryOffice = FA.City__c + IIF(FA.State__c IS NOT NULL OR FA.State__c <> '', ', ' + FA.State__c, ''),
			Latitude = FA.Firm_Location_Lat,
			Longitude = FA.Firm_Location_Long,
			PrimaryOfficeSFID = FA.Id
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.SFID = FA.Account__c
	WHERE	FA.[Address_Description__c] = 'Billing Headquarters'

	UPDATE	O
	SET		Address1City = FA.City__c,
			Address1Country = FA.Country__c,
			Address1 = FA.Street_Address__c,
			Address2 = FA.Address_2__c,
			PostalCode = FA.Zip_Code__c,
			StateProvince = FA.State__c,
			PrimaryOffice = FA.City__c + IIF(FA.State__c IS NOT NULL OR FA.State__c <> '', ', ' + FA.State__c, ''),
			Latitude = FA.Firm_Location_Lat,
			Longitude = FA.Firm_Location_Long,
			PrimaryOfficeSFID = FA.Id
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.SFID = FA.Account__c
	WHERE	FA.[Address_Description__c] = 'Headquarters' AND O.PrimaryOffice IS NULL

	-- delete addresses selected as Primary Office the OtherAddresses
	DELETE OA
	FROM alexabe_01.dbo.OtherAddress OA
	JOIN alexabe_01.dbo.Organization O ON OA.SFID = O.PrimaryOfficeSFID AND O.PrimaryOffice IS NOT NULL

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_TruncateStagingTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SF_TruncateStagingTables] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_TruncateStagingTables]
AS
BEGIN

	SET NOCOUNT ON;

	--delete [alexabe_01].[dbo].[SystemStatus] where property = 'DataImportFromDateUtc' -- for debug only. Remove this line in production
	TRUNCATE TABLE Accounts
	TRUNCATE TABLE Benchmarks
	TRUNCATE TABLE Contacts
	TRUNCATE TABLE FirmAddreses
	TRUNCATE TABLE FundMetrics
	TRUNCATE TABLE Funds
	TRUNCATE TABLE GPScoutScorecards
	--TRUNCATE TABLE Metrics
	TRUNCATE TABLE StrengthsAndConcerns
	TRUNCATE TABLE TrackRecords
	TRUNCATE TABLE Ids
END

GO
