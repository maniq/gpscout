IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Group_DefaultProductType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [CK_Group_DefaultProductType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserRssEmail_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserRssEmail]'))
ALTER TABLE [dbo].[UserRssEmail] DROP CONSTRAINT [FK_UserRssEmail_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [FK_UserExt_Group]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [FK_UserExt_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserEulaAudit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserEulaAudit]'))
ALTER TABLE [dbo].[UserEulaAudit] DROP CONSTRAINT [FK_UserEulaAudit_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_TrackUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView] DROP CONSTRAINT [FK_TrackUsersProfileView_TrackUsers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView] DROP CONSTRAINT [FK_TrackUsersProfileView_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsers_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsers]'))
ALTER TABLE [dbo].[TrackUsers] DROP CONSTRAINT [FK_TrackUsers_UserExt]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackRecord_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackRecord]'))
ALTER TABLE [dbo].[TrackRecord] DROP CONSTRAINT [FK_TrackRecord_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SegmentFilter_Segment]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegmentFilter]'))
ALTER TABLE [dbo].[SegmentFilter] DROP CONSTRAINT [FK_SegmentFilter_Segment]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTerms_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTerms]'))
ALTER TABLE [dbo].[SearchTerms] DROP CONSTRAINT [FK_SearchTerms_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplateFilter_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]'))
ALTER TABLE [dbo].[SearchTemplateFilter] DROP CONSTRAINT [FK_SearchTemplateFilter_SearchTemplate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplate_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplate]'))
ALTER TABLE [dbo].[SearchTemplate] DROP CONSTRAINT [FK_SearchTemplate_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SavedSearches_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SavedSearches]'))
ALTER TABLE [dbo].[SavedSearches] DROP CONSTRAINT [FK_SavedSearches_SearchTemplate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PortfolioCompany_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PortfolioCompany]'))
ALTER TABLE [dbo].[PortfolioCompany] DROP CONSTRAINT [FK_PortfolioCompany_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OtherAddress_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OtherAddress]'))
ALTER TABLE [dbo].[OtherAddress] DROP CONSTRAINT [FK_OtherAddress_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing] DROP CONSTRAINT [FK_OrganaizationViewing_UserExt]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing] DROP CONSTRAINT [FK_OrganaizationViewing_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [FK_OrganizationRequest_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [FK_OrganizationRequest_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] DROP CONSTRAINT [FK_OrganizationNote_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] DROP CONSTRAINT [FK_OrganizationNote_Group]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] DROP CONSTRAINT [FK_OrganizationNote_aspnet_Users1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] DROP CONSTRAINT [FK_OrganizationNote_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationMember_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationMember]'))
ALTER TABLE [dbo].[OrganizationMember] DROP CONSTRAINT [FK_OrganizationMember_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_Entity]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [FK_Organization_Currency]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NewsEntity_News]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewsEntity]'))
ALTER TABLE [dbo].[NewsEntity] DROP CONSTRAINT [FK_NewsEntity_News]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LimitedPartner_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[LimitedPartner]'))
ALTER TABLE [dbo].[LimitedPartner] DROP CONSTRAINT [FK_LimitedPartner_Fund]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_SegmentTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment] DROP CONSTRAINT [FK_GroupSegment_SegmentTemplate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment] DROP CONSTRAINT [FK_GroupSegment_Group]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted] DROP CONSTRAINT [FK_GroupOrganizationRestricted_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted] DROP CONSTRAINT [FK_GroupOrganizationRestricted_Group]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization] DROP CONSTRAINT [FK_GroupOrganization_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization] DROP CONSTRAINT [FK_GroupOrganization_Group]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Group_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [FK_Group_UserExt]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Grade_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Grade]'))
ALTER TABLE [dbo].[Grade] DROP CONSTRAINT [FK_Grade_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] DROP CONSTRAINT [FK_Fund_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] DROP CONSTRAINT [FK_Fund_Entity]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] DROP CONSTRAINT [FK_Fund_Currency]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization] DROP CONSTRAINT [FK_FolderOrganization_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization] DROP CONSTRAINT [FK_FolderOrganization_Folder]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick] DROP CONSTRAINT [FK_FolderClick_Folder]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick] DROP CONSTRAINT [FK_FolderClick_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Folder_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Folder]'))
ALTER TABLE [dbo].[Folder] DROP CONSTRAINT [FK_Folder_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Evaluation_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Evaluation]'))
ALTER TABLE [dbo].[Evaluation] DROP CONSTRAINT [FK_Evaluation_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntityMultipleValues_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]'))
ALTER TABLE [dbo].[EntityMultipleValues] DROP CONSTRAINT [FK_EntityMultipleValues_Entity]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmploymentHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmploymentHistory]'))
ALTER TABLE [dbo].[EmploymentHistory] DROP CONSTRAINT [FK_EmploymentHistory_OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmailDistributionListEmail_EmailDistributionList]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]'))
ALTER TABLE [dbo].[EmailDistributionListEmail] DROP CONSTRAINT [FK_EmailDistributionListEmail_EmailDistributionList]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EducationHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EducationHistory]'))
ALTER TABLE [dbo].[EducationHistory] DROP CONSTRAINT [FK_EducationHistory_OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DueDiligenceItem_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[DueDiligenceItem]'))
ALTER TABLE [dbo].[DueDiligenceItem] DROP CONSTRAINT [FK_DueDiligenceItem_Organization]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BoardSeat_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[BoardSeat]'))
ALTER TABLE [dbo].[BoardSeat] DROP CONSTRAINT [FK_BoardSeat_OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Audit]'))
ALTER TABLE [dbo].[Audit] DROP CONSTRAINT [FK_Audit_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__UserI__7A672E12]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__RoleI__797309D9]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__3E52440B]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [FK__aspnet_Us__Appli__3E52440B]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__46E78A0C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [FK__aspnet_Ro__Appli__46E78A0C]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__75A278F5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile] DROP CONSTRAINT [FK__aspnet_Pr__UserI__75A278F5]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__UserI__68487DD7]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__PathI__6754599E]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__6383C8BA]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] DROP CONSTRAINT [FK__aspnet_Pe__PathI__6383C8BA]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__5EBF139D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [FK__aspnet_Pa__Appli__5EBF139D]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__4316F928]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__UserI__4316F928]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__Appli__4222D4EF]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdvisoryBoard_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[AdvisoryBoard]'))
ALTER TABLE [dbo].[AdvisoryBoard] DROP CONSTRAINT [FK_AdvisoryBoard_Fund]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_IsValid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] DROP CONSTRAINT [DF_UserNotification_IsValid]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_HideTimeoutMS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] DROP CONSTRAINT [DF_UserNotification_HideTimeoutMS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_Hidden]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] DROP CONSTRAINT [DF_UserNotification_Hidden]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_CheckStatusIntervalMS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] DROP CONSTRAINT [DF_UserNotification_CheckStatusIntervalMS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RequestAccess]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [DF_UserExt_RequestAccess]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_DiligenceAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [DF_UserExt_DiligenceAllowed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RssSearchAliases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [DF_UserExt_RssSearchAliases]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RssAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] DROP CONSTRAINT [DF_UserExt_RssAllowed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserEulaAudit_AgreedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserEulaAudit] DROP CONSTRAINT [DF_UserEulaAudit_AgreedOn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TrackRecord_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TrackRecord] DROP CONSTRAINT [DF_TrackRecord_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Task_IsCompleted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Task] DROP CONSTRAINT [DF_Task_IsCompleted]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SearchTemplateFilter_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SearchTemplateFilter] DROP CONSTRAINT [DF_SearchTemplateFilter_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SearchTemplate_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SearchTemplate] DROP CONSTRAINT [DF_SearchTemplate_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SavedSearches_SavedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SavedSearches] DROP CONSTRAINT [DF_SavedSearches_SavedOn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganaizationViewing_Timestamp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationViewing] DROP CONSTRAINT [DF_OrganaizationViewing_Timestamp]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [DF_OrganizationRequest_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [DF_OrganizationRequest_IsActive]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_CreatedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [DF_OrganizationRequest_CreatedOn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] DROP CONSTRAINT [DF_OrganizationRequest_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_InactiveFirm]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_InactiveFirm]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Organizat__Publi__57A801BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF__Organizat__Publi__57A801BA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishLevelAsImported]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishLevelAsImported]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishProfile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishProfile]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Organizat__SBICF__6BAEFA67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF__Organizat__SBICF__6BAEFA67]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishSearch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishSearch]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishTrackRecord]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishTrackRecord]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishStratProcessFirmAndFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishStratProcessFirmAndFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishOverviewAndTeam]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_PublishOverviewAndTeam]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_NumberOfInvestmentProfessionals]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_NumberOfInvestmentProfessionals]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_AccessConstrained]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_AccessConstrained]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_FocusList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_FocusList]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_EmergingManager]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_EmergingManager]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Group_WatermarkEnabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [DF_Group_WatermarkEnabled]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Group__Organizat__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [DF__Group__Organizat__0E04126B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Group_OrganizationProfilePdfAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [DF_Group_OrganizationProfilePdfAllowed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Group__DefaultPr__24285DB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] DROP CONSTRAINT [DF__Group__DefaultPr__24285DB4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Folder_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Folder] DROP CONSTRAINT [DF_Folder_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Evaluatio__SortO__373B3228]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Evaluation] DROP CONSTRAINT [DF__Evaluatio__SortO__373B3228]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EmailDistributionListEmail_Active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EmailDistributionListEmail] DROP CONSTRAINT [DF_EmailDistributionListEmail_Active]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EmailDistributionList_Active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EmailDistributionList] DROP CONSTRAINT [DF_EmailDistributionList_Active]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Currency_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Currency_Id]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__IsAno__3D5E1FD2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__3C69FB99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__Mobil__3C69FB99]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__UserI__3B75D760]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [DF__aspnet_Ro__RoleI__45F365D3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [DF__aspnet_Perso__Id__66603565]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [DF__aspnet_Pa__PathI__5DCAEF64]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [DF__aspnet_Me__Passw__412EB0B6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] DROP CONSTRAINT [DF__aspnet_Ap__Appli__38996AB5]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND name = N'IX_TrackUsersProfileView_TrackUsersID_Include')
DROP INDEX [IX_TrackUsersProfileView_TrackUsersID_Include] ON [dbo].[TrackUsersProfileView]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND name = N'IX_TrackUsersProfileView_TrackUsersID')
DROP INDEX [IX_TrackUsersProfileView_TrackUsersID] ON [dbo].[TrackUsersProfileView]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_UserIDType')
DROP INDEX [IX_TrackUsers_UserIDType] ON [dbo].[TrackUsers]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_UserID')
DROP INDEX [IX_TrackUsers_UserID] ON [dbo].[TrackUsers]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_Type')
DROP INDEX [IX_TrackUsers_Type] ON [dbo].[TrackUsers]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord]') AND name = N'IX_Evaluation_TrackRecord_Unique')
DROP INDEX [IX_Evaluation_TrackRecord_Unique] ON [dbo].[TrackRecord]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId_Include2')
DROP INDEX [IX_SearchTemplateFilter_SearchTemplateId_Include2] ON [dbo].[SearchTemplateFilter]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId_Include_Id')
DROP INDEX [IX_SearchTemplateFilter_SearchTemplateId_Include_Id] ON [dbo].[SearchTemplateFilter]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId')
DROP INDEX [IX_SearchTemplateFilter_SearchTemplateId] ON [dbo].[SearchTemplateFilter]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplate_SearchTemplateId_Include2')
DROP INDEX [IX_SearchTemplate_SearchTemplateId_Include2] ON [dbo].[SearchTemplateFilter]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND name = N'IX_SearchTemplate_UserID2')
DROP INDEX [IX_SearchTemplate_UserID2] ON [dbo].[SearchTemplate]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND name = N'IX_SearchTemplate_UserID')
DROP INDEX [IX_SearchTemplate_UserID] ON [dbo].[SearchTemplate]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND name = N'IX_OtherAddress_SFID_Unique')
DROP INDEX [IX_OtherAddress_SFID_Unique] ON [dbo].[OtherAddress]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND name = N'IX_OtherAddress_OrganizationId')
DROP INDEX [IX_OtherAddress_OrganizationId] ON [dbo].[OtherAddress]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_UserID_Include')
DROP INDEX [IX_OrganizationViewing_UserID_Include] ON [dbo].[OrganizationViewing]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_UserID')
DROP INDEX [IX_OrganizationViewing_UserID] ON [dbo].[OrganizationViewing]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_OrganizationId')
DROP INDEX [IX_OrganizationViewing_OrganizationId] ON [dbo].[OrganizationViewing]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND name = N'IX_OrganizationMember_SFID_Unique')
DROP INDEX [IX_OrganizationMember_SFID_Unique] ON [dbo].[OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND name = N'IX_OrganizationMember_OrganizationId')
DROP INDEX [IX_OrganizationMember_OrganizationId] ON [dbo].[OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_SFID_Unique')
DROP INDEX [IX_Organization_SFID_Unique] ON [dbo].[Organization]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_PublishLevelAsImported')
DROP INDEX [IX_Organization_PublishLevelAsImported] ON [dbo].[Organization]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_PrimaryOfficeSFID')
DROP INDEX [IX_Organization_PrimaryOfficeSFID] ON [dbo].[Organization]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_CurrencyId')
DROP INDEX [IX_Organization_CurrencyId] ON [dbo].[Organization]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]') AND name = N'IX_GroupOrganizationRestricted')
DROP INDEX [IX_GroupOrganizationRestricted] ON [dbo].[GroupOrganizationRestricted]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND name = N'IX_Grade_SFID_Unique')
DROP INDEX [IX_Grade_SFID_Unique] ON [dbo].[Grade]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND name = N'IX_Grade_OrganizationId')
DROP INDEX [IX_Grade_OrganizationId] ON [dbo].[Grade]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND name = N'IX_Fund_SFID_Unique')
DROP INDEX [IX_Fund_SFID_Unique] ON [dbo].[Fund]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND name = N'IX_Fund_OrganizationId')
DROP INDEX [IX_Fund_OrganizationId] ON [dbo].[Fund]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND name = N'IX_Evaluation_SFID_Unique')
DROP INDEX [IX_Evaluation_SFID_Unique] ON [dbo].[Evaluation]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND name = N'IX_Evaluation_OrganizationId')
DROP INDEX [IX_Evaluation_OrganizationId] ON [dbo].[Evaluation]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_Field')
DROP INDEX [IX_EntityMultipleValues_Field] ON [dbo].[EntityMultipleValues]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_EntityIdField')
DROP INDEX [IX_EntityMultipleValues_EntityIdField] ON [dbo].[EntityMultipleValues]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_EntityId')
DROP INDEX [IX_EntityMultipleValues_EntityId] ON [dbo].[EntityMultipleValues]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Entity]') AND name = N'IX_Entity_EntityType')
DROP INDEX [IX_Entity_EntityType] ON [dbo].[Entity]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]') AND name = N'IX_EmailDistributionListEmail_Member_Unique')
DROP INDEX [IX_EmailDistributionListEmail_Member_Unique] ON [dbo].[EmailDistributionListEmail]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND name = N'NonClusteredIndex-20150514-005429')
DROP INDEX [NonClusteredIndex-20150514-005429] ON [dbo].[Currency]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND name = N'IX_Benchmarks_SFID_Unique')
DROP INDEX [IX_Benchmarks_SFID_Unique] ON [dbo].[Benchmarks]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND name = N'aspnet_UsersInRoles_index')
DROP INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index2')
DROP INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_ncindex2')
DROP INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index')
DROP INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND name = N'aspnet_Roles_index1')
DROP INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_index1')
DROP INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND name = N'aspnet_Paths_index')
DROP INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND name = N'aspnet_Membership_index')
DROP INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND name = N'aspnet_Applications_Index')
DROP INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_User]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Shared]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Paths]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
DROP VIEW [dbo].[vw_aspnet_UsersInRoles]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
DROP VIEW [dbo].[vw_aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
DROP VIEW [dbo].[vw_aspnet_Roles]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
DROP VIEW [dbo].[vw_aspnet_Profiles]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
DROP VIEW [dbo].[vw_aspnet_MembershipUsers]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
DROP VIEW [dbo].[vw_aspnet_Applications]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRssEmail]') AND type in (N'U'))
DROP TABLE [dbo].[UserRssEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserNotification]') AND type in (N'U'))
DROP TABLE [dbo].[UserNotification]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserExt]') AND type in (N'U'))
DROP TABLE [dbo].[UserExt]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserEulaAudit]') AND type in (N'U'))
DROP TABLE [dbo].[UserEulaAudit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView_2015_07_21]') AND type in (N'U'))
DROP TABLE [dbo].[TrackUsersProfileView_2015_07_21]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND type in (N'U'))
DROP TABLE [dbo].[TrackUsersProfileView]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND type in (N'U'))
DROP TABLE [dbo].[TrackUsers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord_2016_04_04_pre_domain_migration]') AND type in (N'U'))
DROP TABLE [dbo].[TrackRecord_2016_04_04_pre_domain_migration]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord]') AND type in (N'U'))
DROP TABLE [dbo].[TrackRecord]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_searchoptions_2015_02_11]') AND type in (N'U'))
DROP TABLE [dbo].[temp_searchoptions_2015_02_11]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_searchoptions_2015_02_08]') AND type in (N'U'))
DROP TABLE [dbo].[temp_searchoptions_2015_02_08]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_OrganizationRequest]') AND type in (N'U'))
DROP TABLE [dbo].[temp_OrganizationRequest]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_organization_2015_02_11]') AND type in (N'U'))
DROP TABLE [dbo].[temp_organization_2015_02_11]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_organization_2015_02_08]') AND type in (N'U'))
DROP TABLE [dbo].[temp_organization_2015_02_08]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Task]') AND type in (N'U'))
DROP TABLE [dbo].[Task]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemStatus]') AND type in (N'U'))
DROP TABLE [dbo].[SystemStatus]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SegmentFilter]') AND type in (N'U'))
DROP TABLE [dbo].[SegmentFilter]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segment]') AND type in (N'U'))
DROP TABLE [dbo].[Segment]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTerms]') AND type in (N'U'))
DROP TABLE [dbo].[SearchTerms]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND type in (N'U'))
DROP TABLE [dbo].[SearchTemplateFilter]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[SearchTemplate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchOptions]') AND type in (N'U'))
DROP TABLE [dbo].[SearchOptions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SavedSearches]') AND type in (N'U'))
DROP TABLE [dbo].[SavedSearches]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RSSFeedsRaw]') AND type in (N'U'))
DROP TABLE [dbo].[RSSFeedsRaw]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RssFeeds]') AND type in (N'U'))
DROP TABLE [dbo].[RssFeeds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ResearchPriorityOrganization]') AND type in (N'U'))
DROP TABLE [dbo].[ResearchPriorityOrganization]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PortfolioCompany]') AND type in (N'U'))
DROP TABLE [dbo].[PortfolioCompany]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND type in (N'U'))
DROP TABLE [dbo].[OtherAddress]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND type in (N'U'))
DROP TABLE [dbo].[OrganizationViewing]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]') AND type in (N'U'))
DROP TABLE [dbo].[OrganizationRequest]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationNote]') AND type in (N'U'))
DROP TABLE [dbo].[OrganizationNote]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND type in (N'U'))
DROP TABLE [dbo].[OrganizationMember]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization_temp_2016_04_21]') AND type in (N'U'))
DROP TABLE [dbo].[Organization_temp_2016_04_21]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND type in (N'U'))
DROP TABLE [dbo].[Organization]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewsEntity]') AND type in (N'U'))
DROP TABLE [dbo].[NewsEntity]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News]') AND type in (N'U'))
DROP TABLE [dbo].[News]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LimitedPartner]') AND type in (N'U'))
DROP TABLE [dbo].[LimitedPartner]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupSegment]') AND type in (N'U'))
DROP TABLE [dbo].[GroupSegment]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]') AND type in (N'U'))
DROP TABLE [dbo].[GroupOrganizationRestricted]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganization]') AND type in (N'U'))
DROP TABLE [dbo].[GroupOrganization]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Group]') AND type in (N'U'))
DROP TABLE [dbo].[Group]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND type in (N'U'))
DROP TABLE [dbo].[Grade]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FurtherFundMetrics]') AND type in (N'U'))
DROP TABLE [dbo].[FurtherFundMetrics]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fund_2016_04_04_pre_domain_migration]') AND type in (N'U'))
DROP TABLE [dbo].[Fund_2016_04_04_pre_domain_migration]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND type in (N'U'))
DROP TABLE [dbo].[Fund]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderOrganization]') AND type in (N'U'))
DROP TABLE [dbo].[FolderOrganization]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderClick]') AND type in (N'U'))
DROP TABLE [dbo].[FolderClick]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Folder]') AND type in (N'U'))
DROP TABLE [dbo].[Folder]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND type in (N'U'))
DROP TABLE [dbo].[Evaluation]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Eula]') AND type in (N'U'))
DROP TABLE [dbo].[Eula]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND type in (N'U'))
DROP TABLE [dbo].[EntityMultipleValues]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Entity]') AND type in (N'U'))
DROP TABLE [dbo].[Entity]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmploymentHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EmploymentHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]') AND type in (N'U'))
DROP TABLE [dbo].[EmailDistributionListEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionList]') AND type in (N'U'))
DROP TABLE [dbo].[EmailDistributionList]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EducationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EducationHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DueDiligenceItem]') AND type in (N'U'))
DROP TABLE [dbo].[DueDiligenceItem]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency_Updates_2017_11_09]') AND type in (N'U'))
DROP TABLE [dbo].[Currency_Updates_2017_11_09]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
DROP TABLE [dbo].[Currency]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientRequest]') AND type in (N'U'))
DROP TABLE [dbo].[ClientRequest]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BoardSeat]') AND type in (N'U'))
DROP TABLE [dbo].[BoardSeat]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND type in (N'U'))
DROP TABLE [dbo].[Benchmarks]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
DROP TABLE [dbo].[Audit]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AssociatedTerms]') AND type in (N'U'))
DROP TABLE [dbo].[AssociatedTerms]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_WebEvent_Events]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_UsersInRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Users]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_SchemaVersions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Roles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Profile]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationPerUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationAllUsers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Paths]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Membership]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Applications]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdvisoryBoard]') AND type in (N'U'))
DROP TABLE [dbo].[AdvisoryBoard]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WipeEulaDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WipeEulaDates]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Util_ReIndexDatabase_UpdateStats]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Util_ReIndexDatabase_UpdateStats]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udp_DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[udp_DeleteUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecentlyViewedOrganizations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RecentlyViewedOrganizations]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News_PopulateRssFeeds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[News_PopulateRssFeeds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_DeleteUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_CreateUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_RoleExists]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_GetAllRoles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_CreateRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_SetProperties]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProperties]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Paths_CreatePath]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_SetPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPassword]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_CreateUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_AnyDataInTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N''aspnet_Membership''
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Roles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N''aspnet_Roles''
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N''aspnet_Profile''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N''aspnet_PersonalizationPerUser''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''aspnet_WebEvent_LogEvent'') AND (type = ''P''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N''aspnet_WebEvent_Events''
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N''aspnet_Users''
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N''aspnet_Applications''
            RETURN
        END
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
    WHERE @UserId = UserId

    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = ''EXEC sp_droprolemember '' + '''''''' + @name + '''''', '''''' + USER_NAME(@user_id) + ''''''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + '' '' + @action + '' on '' + @object + '' TO ['' + @grantee + '']''
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_UsersInRoles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'''', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News_PopulateRssFeeds]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		SFP ZK
-- Create date: 05/29/2015
-- Description:	Populate RssFeeds table w. list of rss feeds provided by the client
-- =============================================
CREATE PROCEDURE [dbo].[News_PopulateRssFeeds]
AS

	DECLARE @DateImported datetime = GETDATE()

	TRUNCATE TABLE RssFeeds

	INSERT INTO RssFeeds (Id, Url, SourceOrganizationId, SourceOrganizationName, [Enabled], DateImported)
	SELECT NEWID(), RawFeeds.FeedAddress, RawSources.SourceId, RawSources.Source, 1, @DateImported
	FROM (
		SELECT DISTINCT RF.FeedAddress, RF.Source
		FROM RSSFeedsRaw RF
	) RawFeeds
	JOIN (
		SELECT Source, NEWID() SourceId
		FROM (
			SELECT DISTINCT RF.Source
			FROM RSSFeedsRaw RF
		) A
	) RawSources ON RawFeeds.Source = RawSources.Source
  ' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecentlyViewedOrganizations]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RecentlyViewedOrganizations] 
	-- Add the parameters for the stored procedure here
	@userId uniqueidentifier,
	@take int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select top(@take) OrganizationId
, o.Name
, o.MarketStage
, replace(o.InvestmentRegionPipeDelimited, ''|'', '', '') as InvestmentRegions
, Replace(o.StrategyPipeDelimited, ''|'', '', '') as Strategies
, o.FocusRadar
, o.EmergingManager
, o.AccessConstrained
, o.KeyTakeAway
, max(ov.timestamp) as [Timestamp]

from organizationviewing ov inner join organization o on ov.organizationid = o.id
	where ov.userid = @userId
	group by ov.organizationid
	, o.Name
	, o.MarketStage
	,o.InvestmentRegionPipeDelimited
	, o.StrategyPipeDelimited
	, o.FocusRadar
	, o.EmergingManager
	, o.AccessConstrained
	, o.KeyTakeAway
	order by [Timestamp] desc
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[udp_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[udp_DeleteUser]
	@UserID uniqueidentifier
AS

print ''begin deletion tran:''
BEGIN TRAN

delete trupw from TrackUsersProfileView trupw
join TrackUsers tu on trupw.TrackUsersId = tu.Id and tu.UserId = @UserID

delete from TrackUsers where userid=@userid

print ''delete user rss email data:''
delete from UserRssEmail where UserId = @UserID
delete from UserNotification where UserId = @UserID
delete from UserEulaAudit where UserId = @UserID
delete from Task where UserId = @UserID
delete from SearchTerms where UserId = @UserID

delete ss from SavedSearches ss join SearchTemplate st ON ss.SearchTemplateId = st.Id and st.UserId = @UserID
delete from SearchTemplate where UserId = @UserID

delete from [dbo].[OrganizationRequest] where UserId = @UserID
delete from [dbo].[OrganizationViewing] where UserId = @UserID
delete from [dbo].[OrganizationNote] where UserId = @UserID

delete from [dbo].FolderClick where UserId = @UserID
delete from [dbo].Folder where UserId = @UserID

print ''group leader update:''
update [group] set GroupLeaderId = null where GroupLeaderId = @UserID

delete from aspnet_PersonalizationPerUser where userid=@userid
delete from aspnet_UsersInRoles where userid=@userid
delete from aspnet_Profile where userid=@userid
delete from aspnet_Membership where userid=@userid
delete from UserExt where UserId = @UserID
delete from aspnet_Users where UserId = @UserID

COMMIT TRAN
--rollback tran

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Util_ReIndexDatabase_UpdateStats]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_Util_ReIndexDatabase_UpdateStats]
AS
SET NOCOUNT ON;

-- Rebuild or reorganize indexes based on their fragmentation levels
DECLARE @work_to_do TABLE 
(
    objectid INT,
    indexid INT,
    pagedensity FLOAT,
    fragmentation FLOAT,
    numrows INT 
)

DECLARE @objectid int;
DECLARE @indexid int;
DECLARE @schemaname nvarchar(130); 
DECLARE @objectname nvarchar(130); 
DECLARE @indexname nvarchar(130); 
DECLARE @numrows int
DECLARE @density float;
DECLARE @fragmentation float;
DECLARE @command nvarchar(4000); 
DECLARE @fillfactorset bit
DECLARE @numpages int

-- Select indexes that need to be defragmented based on the following
-- * Page density is low
-- * External fragmentation is high in relation to index size
PRINT ''Estimating fragmentation: Begin. '' + convert(nvarchar, getdate(), 121) 

INSERT @work_to_do
SELECT
    f.object_id,
    index_id,
    avg_page_space_used_in_percent,
    avg_fragmentation_in_percent,
    record_count
FROM 
    sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, ''SAMPLED'') AS f
WHERE
     (f.page_count > 50 and f.avg_fragmentation_in_percent > 15.0)
    or (f.page_count > 10 and f.avg_fragmentation_in_percent > 80.0)

PRINT ''Number of indexes to rebuild: '' + cast(@@ROWCOUNT as nvarchar(20))

PRINT ''Estimating fragmentation: End. '' + convert(nvarchar, getdate(), 121)

SELECT @numpages = sum(ps.used_page_count)
FROM
    @work_to_do AS fi
    INNER JOIN sys.indexes AS i ON fi.objectid = i.object_id and fi.indexid = i.index_id
    INNER JOIN sys.dm_db_partition_stats AS ps on i.object_id = ps.object_id and i.index_id = ps.index_id

-- Declare the cursor for the list of indexes to be processed.
DECLARE curIndexes CURSOR FOR SELECT * FROM @work_to_do

-- Open the cursor.
OPEN curIndexes

-- Loop through the indexes
WHILE (1=1)
BEGIN
    FETCH NEXT FROM curIndexes
    INTO @objectid, @indexid, @density, @fragmentation, @numrows;
    IF @@FETCH_STATUS < 0 BREAK;

    SELECT 
        @objectname = QUOTENAME(o.name)
        , @schemaname = QUOTENAME(s.name)
    FROM 
        sys.objects AS o
        INNER JOIN sys.schemas as s ON s.schema_id = o.schema_id
    WHERE 
        o.object_id = @objectid;

    SELECT 
        @indexname = QUOTENAME(name)
        , @fillfactorset = CASE fill_factor WHEN 0 THEN 0 ELSE 1 END
    FROM 
        sys.indexes
    WHERE
        object_id = @objectid AND index_id = @indexid;

    IF ((@density BETWEEN 75.0 AND 85.0) AND @fillfactorset = 1) OR (@fragmentation < 30.0)
        SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REORGANIZE'';
    ELSE IF @numrows >= 5000 AND @fillfactorset = 0
        SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REBUILD WITH (FILLFACTOR = 90)'';
    ELSE
        SET @command = N''ALTER INDEX '' + @indexname + N'' ON '' + @schemaname + N''.'' + @objectname + N'' REBUILD'';
    PRINT convert(nvarchar, getdate(), 121) + N'' Executing: '' + @command;
    EXEC (@command);
    PRINT convert(nvarchar, getdate(), 121) + N'' Done.'';
END

-- Close and deallocate the cursor.
CLOSE curIndexes;
DEALLOCATE curIndexes;


IF EXISTS (SELECT * FROM @work_to_do)
BEGIN
    PRINT ''Estimated number of pages in fragmented indexes: '' + cast(@numpages as nvarchar(20))
    SELECT @numpages = @numpages - sum(ps.used_page_count)
    FROM
        @work_to_do AS fi
        INNER JOIN sys.indexes AS i ON fi.objectid = i.object_id and fi.indexid = i.index_id
        INNER JOIN sys.dm_db_partition_stats AS ps on i.object_id = ps.object_id and i.index_id = ps.index_id

    PRINT ''Estimated number of pages freed: '' + cast(@numpages as nvarchar(20))
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WipeEulaDates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[WipeEulaDates]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update userext set eulasigned = null
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdvisoryBoard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AdvisoryBoard](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Type] [varchar](50) NULL,
	[Description] [varchar](2000) NULL,
	[FundId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_AdvisoryBoard] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) NOT NULL,
	[LoweredPath] [nvarchar](256) NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] NOT NULL,
	[PropertyValuesString] [ntext] NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[LoweredRoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) NULL,
	[ApplicationPath] [nvarchar](256) NULL,
	[ApplicationVirtualPath] [nvarchar](256) NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[RequestUrl] [nvarchar](1024) NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AssociatedTerms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AssociatedTerms](
	[Id] [uniqueidentifier] NOT NULL,
	[SourceTerm] [varchar](255) NOT NULL,
	[AssociatedTerm] [varchar](255) NOT NULL,
 CONSTRAINT [PK_AssociatedTerms_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Audit](
	[Id] [uniqueidentifier] NOT NULL,
	[Modified] [datetime] NOT NULL,
	[ModifiedBy] [uniqueidentifier] NOT NULL,
	[ModifiedTable] [varchar](255) NOT NULL,
	[ModifiedEntity] [uniqueidentifier] NOT NULL,
	[AuditType] [int] NOT NULL,
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Benchmarks](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[VintageYear] [int] NULL,
	[AsOf] [datetime] NULL,
	[FundCount] [int] NULL,
	[MarketStage] [nvarchar](100) NULL,
	[Region] [nvarchar](100) NULL,
	[Strategy] [nvarchar](100) NULL,
	[PreqinDownloadDate] [datetime] NULL,
	[DpiFirstQuartile] [decimal](18, 2) NULL,
	[DpiMedian] [decimal](18, 2) NULL,
	[DpiThirdQuartile] [decimal](18, 2) NULL,
	[NetIrrFirstQuartile] [decimal](18, 2) NULL,
	[NetIrrMedian] [decimal](18, 2) NULL,
	[NetIrrThirdQuartile] [decimal](18, 2) NULL,
	[TvpiFirstQuartile] [decimal](18, 2) NULL,
	[TvpiMedian] [decimal](18, 2) NULL,
	[TvpiThirdQuartile] [decimal](18, 2) NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Benchmarks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BoardSeat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BoardSeat](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationMemberId] [uniqueidentifier] NOT NULL,
	[YearJoined] [datetime] NULL,
	[OrganizationName] [varchar](200) NULL,
	[PastPresent] [varchar](50) NULL,
 CONSTRAINT [PK_BoardSeat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClientRequest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClientRequest](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[FundId] [uniqueidentifier] NULL,
	[OrganizationName] [varchar](255) NULL,
	[FundName] [varchar](255) NULL,
	[PlacementAgent] [varchar](255) NULL,
	[MarketStage] [varchar](100) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[Source] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[DateReceived] [datetime] NULL,
 CONSTRAINT [PK_ClientRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Currency](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [varchar](50) NULL,
	[Symbol] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Currency_Updates_2017_11_09]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Currency_Updates_2017_11_09](
	[Name] [varchar](50) NOT NULL,
	[APIName] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DueDiligenceItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DueDiligenceItem](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [varchar](200) NULL,
	[Description] [varchar](2000) NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[TabName] [varchar](50) NULL,
 CONSTRAINT [PK_DueDiligenceItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EducationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EducationHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationMemberId] [uniqueidentifier] NOT NULL,
	[School] [varchar](200) NULL,
	[Year] [datetime] NULL,
	[Degree] [varchar](200) NULL,
	[Notes] [varchar](500) NULL,
 CONSTRAINT [PK_EducationHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmailDistributionList](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](90) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_EmailDistributionList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmailDistributionListEmail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](254) NOT NULL,
	[DistributionListID] [int] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_EmailDistributionListEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmploymentHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmploymentHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationMemberId] [uniqueidentifier] NOT NULL,
	[OrganizationName] [varchar](200) NULL,
	[YearsAtOrganization] [varchar](200) NULL,
	[JobTitle] [varchar](200) NULL,
	[Role] [varchar](2000) NULL,
 CONSTRAINT [PK_EmploymentHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Entity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Entity](
	[Id] [uniqueidentifier] NOT NULL,
	[EntityType] [int] NOT NULL,
 CONSTRAINT [PK_Entity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EntityMultipleValues](
	[Id] [uniqueidentifier] NOT NULL,
	[EntityId] [uniqueidentifier] NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[Field] [int] NOT NULL,
 CONSTRAINT [PK_EntityMultipleValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Eula]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Eula](
	[Id] [uniqueidentifier] NOT NULL,
	[Text] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Eula] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Evaluation](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[Note] [varchar](max) NULL,
	[Description] [varchar](2000) NULL,
	[Type] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Evaluation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Folder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Folder](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Shared] [bit] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderClick]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FolderClick](
	[UserId] [uniqueidentifier] NOT NULL,
	[FolderId] [uniqueidentifier] NOT NULL,
	[LastOpened] [datetime] NOT NULL,
 CONSTRAINT [PK_FolderClick] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[FolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderOrganization]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FolderOrganization](
	[FolderId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_FolderOrganization] PRIMARY KEY CLUSTERED 
(
	[FolderId] ASC,
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Fund](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[GeographicFocus] [varchar](100) NULL,
	[MarketStage] [varchar](100) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[AnalysisUrl] [varchar](255) NULL,
	[AdvisoryBoard] [bit] NULL,
	[Clawback] [bit] NULL,
	[AsOf] [datetime] NULL,
	[FinalClose] [datetime] NULL,
	[FirstClose] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
	[CarriedInterest] [decimal](18, 2) NULL,
	[DebtMax] [decimal](18, 2) NULL,
	[DebtMin] [decimal](18, 2) NULL,
	[Dpi] [decimal](18, 2) NULL,
	[EbitdaMax] [decimal](18, 2) NULL,
	[EbitdaMin] [decimal](18, 2) NULL,
	[EquityCheckSizeMax] [decimal](18, 2) NULL,
	[EquityCheckSizeMin] [decimal](18, 2) NULL,
	[EvMax] [decimal](18, 2) NULL,
	[EvMin] [decimal](18, 2) NULL,
	[FundSize] [decimal](18, 2) NULL,
	[GpCatchUp] [decimal](18, 2) NULL,
	[GpCommitment] [decimal](18, 2) NULL,
	[GpRemovalVote] [decimal](18, 2) NULL,
	[HardCapSize] [decimal](18, 2) NULL,
	[InvestedCapital] [decimal](18, 2) NULL,
	[Irr] [decimal](18, 2) NULL,
	[ManagementFee] [decimal](18, 2) NULL,
	[Moic] [decimal](18, 2) NULL,
	[NoFaultDivorceVote] [decimal](18, 2) NULL,
	[OtherDealFees] [decimal](18, 2) NULL,
	[PreferredReturn] [decimal](18, 2) NULL,
	[RealizedValue] [decimal](18, 2) NULL,
	[TargetSize] [decimal](18, 2) NULL,
	[TieredCarryLevel] [decimal](18, 2) NULL,
	[TotalValue] [decimal](18, 2) NULL,
	[TransFeeOffset] [decimal](18, 2) NULL,
	[Tvpi] [decimal](18, 2) NULL,
	[TypicalLeverageMax] [decimal](18, 2) NULL,
	[TypicalLeverageMin] [decimal](18, 2) NULL,
	[UnrealizedValue] [decimal](18, 2) NULL,
	[FundTerm] [int] NULL,
	[InvestmentPeriod] [int] NULL,
	[AdvisoryBoardNotes] [varchar](2000) NULL,
	[CarriedInterestNotes] [varchar](2000) NULL,
	[ClawbackNotes] [varchar](2000) NULL,
	[Description] [varchar](2000) NULL,
	[EvaluationOfKeyTerms] [varchar](2000) NULL,
	[EvaluationOfKeyTermsFlag] [varchar](50) NULL,
	[FinalCloseNote] [varchar](2000) NULL,
	[FirstCloseNote] [varchar](2000) NULL,
	[FundraisingStatusNote] [varchar](255) NULL,
	[FundSizeNote] [varchar](255) NULL,
	[FundTermNotes] [varchar](2000) NULL,
	[GeneralNotesOnWaterfall] [varchar](2000) NULL,
	[Geography] [varchar](255) NULL,
	[GpCatchUpNotes] [varchar](2000) NULL,
	[GpCommitmentNote] [varchar](2000) NULL,
	[GpRemovalNotes] [varchar](2000) NULL,
	[InterimClawback] [varchar](255) NULL,
	[InterimClawbackNotes] [varchar](255) NULL,
	[InvestmentPeriodNotes] [varchar](2000) NULL,
	[InvestmentRestrictions] [varchar](2000) NULL,
	[KeyMan] [varchar](2000) NULL,
	[KeyManTrigger] [varchar](255) NULL,
	[KeyManTriggerNotes] [varchar](2000) NULL,
	[ManagementFeeNotes] [varchar](2000) NULL,
	[Mfn] [varchar](255) NULL,
	[MfnNotes] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[NoFaultNotes] [varchar](2000) NULL,
	[NumberOfPortfolioCompanies] [varchar](50) NULL,
	[OtherFeeNotes] [varchar](2000) NULL,
	[PlacementAgent] [varchar](255) NULL,
	[PreferredReturnNotes] [varchar](2000) NULL,
	[SectorFocusPipeDelimited] [varchar](2000) NULL,
	[Status] [varchar](50) NULL,
	[Strategy] [varchar](255) NULL,
	[SubSector] [varchar](1000) NULL,
	[SubStrategy] [varchar](255) NULL,
	[TargetFundSizeNote] [varchar](255) NULL,
	[TieredCarryNotes] [varchar](255) NULL,
	[TransFeeNotes] [varchar](2000) NULL,
	[VintageYear] [varchar](255) NULL,
	[RevenueMax] [decimal](18, 2) NULL,
	[RevenueMin] [decimal](18, 2) NULL,
	[SortOrder] [int] NULL,
	[GrossIrr] [decimal](18, 2) NULL,
	[PerformanceDatasource] [varchar](50) NULL,
	[PreqinAsOf] [datetime] NULL,
	[PreqinTvpi] [decimal](18, 2) NULL,
	[PreqinDpi] [decimal](18, 2) NULL,
	[PreqinIrr] [decimal](18, 2) NULL,
	[PreqinFundSize] [decimal](18, 2) NULL,
	[PreqinCalled] [decimal](18, 2) NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Fund] ADD [Source] [varchar](50) NULL
ALTER TABLE [dbo].[Fund] ADD [DPIQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund] ADD [TVPIQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund] ADD [NetIrrQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkAsOf] [datetime] NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkDpiFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkDpiMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkDpiThirdQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkNetIrrFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkNetIrrMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkNetIrrThirdQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkTvpiFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkTvpiMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund] ADD [BenchmarkTvpiThirdQuartile] [decimal](18, 2) NULL
 CONSTRAINT [PK_Fund] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fund_2016_04_04_pre_domain_migration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Fund_2016_04_04_pre_domain_migration](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[GeographicFocus] [varchar](100) NULL,
	[MarketStage] [varchar](100) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[AnalysisUrl] [varchar](255) NULL,
	[AdvisoryBoard] [bit] NULL,
	[Clawback] [bit] NULL,
	[AsOf] [datetime] NULL,
	[FinalClose] [datetime] NULL,
	[FirstClose] [datetime] NULL,
	[ModifiedOn] [datetime] NULL,
	[CarriedInterest] [decimal](18, 2) NULL,
	[DebtMax] [decimal](18, 2) NULL,
	[DebtMin] [decimal](18, 2) NULL,
	[Dpi] [decimal](18, 2) NULL,
	[EbitdaMax] [decimal](18, 2) NULL,
	[EbitdaMin] [decimal](18, 2) NULL,
	[EquityCheckSizeMax] [decimal](18, 2) NULL,
	[EquityCheckSizeMin] [decimal](18, 2) NULL,
	[EvMax] [decimal](18, 2) NULL,
	[EvMin] [decimal](18, 2) NULL,
	[FundSize] [decimal](18, 2) NULL,
	[GpCatchUp] [decimal](18, 2) NULL,
	[GpCommitment] [decimal](18, 2) NULL,
	[GpRemovalVote] [decimal](18, 2) NULL,
	[HardCapSize] [decimal](18, 2) NULL,
	[InvestedCapital] [decimal](18, 2) NULL,
	[Irr] [decimal](18, 2) NULL,
	[ManagementFee] [decimal](18, 2) NULL,
	[Moic] [decimal](18, 2) NULL,
	[NoFaultDivorceVote] [decimal](18, 2) NULL,
	[OtherDealFees] [decimal](18, 2) NULL,
	[PreferredReturn] [decimal](18, 2) NULL,
	[RealizedValue] [decimal](18, 2) NULL,
	[TargetSize] [decimal](18, 2) NULL,
	[TieredCarryLevel] [decimal](18, 2) NULL,
	[TotalValue] [decimal](18, 2) NULL,
	[TransFeeOffset] [decimal](18, 2) NULL,
	[Tvpi] [decimal](18, 2) NULL,
	[TypicalLeverageMax] [decimal](18, 2) NULL,
	[TypicalLeverageMin] [decimal](18, 2) NULL,
	[UnrealizedValue] [decimal](18, 2) NULL,
	[FundTerm] [int] NULL,
	[InvestmentPeriod] [int] NULL,
	[AdvisoryBoardNotes] [varchar](2000) NULL,
	[CarriedInterestNotes] [varchar](2000) NULL,
	[ClawbackNotes] [varchar](2000) NULL,
	[Description] [varchar](2000) NULL,
	[EvaluationOfKeyTerms] [varchar](2000) NULL,
	[EvaluationOfKeyTermsFlag] [varchar](50) NULL,
	[FinalCloseNote] [varchar](2000) NULL,
	[FirstCloseNote] [varchar](2000) NULL,
	[FundraisingStatusNote] [varchar](255) NULL,
	[FundSizeNote] [varchar](255) NULL,
	[FundTermNotes] [varchar](2000) NULL,
	[GeneralNotesOnWaterfall] [varchar](2000) NULL,
	[Geography] [varchar](255) NULL,
	[GpCatchUpNotes] [varchar](2000) NULL,
	[GpCommitmentNote] [varchar](2000) NULL,
	[GpRemovalNotes] [varchar](2000) NULL,
	[InterimClawback] [varchar](255) NULL,
	[InterimClawbackNotes] [varchar](255) NULL,
	[InvestmentPeriodNotes] [varchar](2000) NULL,
	[InvestmentRestrictions] [varchar](2000) NULL,
	[KeyMan] [varchar](2000) NULL,
	[KeyManTrigger] [varchar](255) NULL,
	[KeyManTriggerNotes] [varchar](2000) NULL,
	[ManagementFeeNotes] [varchar](2000) NULL,
	[Mfn] [varchar](255) NULL,
	[MfnNotes] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[NoFaultNotes] [varchar](2000) NULL,
	[NumberOfPortfolioCompanies] [varchar](50) NULL,
	[OtherFeeNotes] [varchar](2000) NULL,
	[PlacementAgent] [varchar](255) NULL,
	[PreferredReturnNotes] [varchar](2000) NULL,
	[SectorFocusPipeDelimited] [varchar](2000) NULL,
	[Status] [varchar](50) NULL,
	[Strategy] [varchar](255) NULL,
	[SubSector] [varchar](1000) NULL,
	[SubStrategy] [varchar](255) NULL,
	[TargetFundSizeNote] [varchar](255) NULL,
	[TieredCarryNotes] [varchar](255) NULL,
	[TransFeeNotes] [varchar](2000) NULL,
	[VintageYear] [varchar](255) NULL,
	[RevenueMax] [decimal](18, 2) NULL,
	[RevenueMin] [decimal](18, 2) NULL,
	[SortOrder] [int] NULL,
	[GrossIrr] [decimal](18, 2) NULL,
	[PerformanceDatasource] [varchar](50) NULL,
	[PreqinAsOf] [datetime] NULL,
	[PreqinTvpi] [decimal](18, 2) NULL,
	[PreqinDpi] [decimal](18, 2) NULL,
	[PreqinIrr] [decimal](18, 2) NULL,
	[PreqinFundSize] [decimal](18, 2) NULL,
	[PreqinCalled] [decimal](18, 2) NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [Source] [varchar](50) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [DPIQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [TVPIQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [NetIrrQuartile] [varchar](9) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkAsOf] [datetime] NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkDpiFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkDpiMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkDpiThirdQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkNetIrrFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkNetIrrMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkNetIrrThirdQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkTvpiFirstQuartile] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkTvpiMedian] [decimal](18, 2) NULL
ALTER TABLE [dbo].[Fund_2016_04_04_pre_domain_migration] ADD [BenchmarkTvpiThirdQuartile] [decimal](18, 2) NULL
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FurtherFundMetrics]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FurtherFundMetrics](
	[Id] [uniqueidentifier] NOT NULL,
	[FundId] [uniqueidentifier] NOT NULL,
	[PreqinAsOf] [datetime] NULL,
	[PreqinTvpi] [decimal](18, 2) NULL,
	[PreqinDpi] [decimal](18, 2) NULL,
	[PreqinIrr] [decimal](18, 2) NULL,
	[PreqinFundSize] [decimal](18, 2) NULL,
	[PreqinCalled] [decimal](18, 2) NULL,
	[InvestedCapital] [decimal](18, 2) NULL,
	[RealizedValue] [decimal](18, 2) NULL,
	[UnrealizedValue] [decimal](18, 2) NULL,
	[TotalValue] [decimal](18, 2) NULL,
	[GrossIrr] [decimal](18, 2) NULL,
	[Moic] [decimal](18, 2) NULL,
	[Dpi] [decimal](18, 2) NULL,
	[Tvpi] [decimal](18, 2) NULL,
	[Irr] [decimal](18, 2) NULL,
	[AsOf] [datetime] NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[SFID] [varchar](18) NOT NULL,
	[Source] [varchar](50) NULL,
	[DPIQuartile] [varchar](9) NULL,
	[TVPIQuartile] [varchar](9) NULL,
	[NetIrrQuartile] [varchar](9) NULL,
	[BenchmarkAsOf] [datetime] NULL,
	[BenchmarkDpiFirstQuartile] [decimal](18, 2) NULL,
	[BenchmarkDpiMedian] [decimal](18, 2) NULL,
	[BenchmarkDpiThirdQuartile] [decimal](18, 2) NULL,
	[BenchmarkNetIrrFirstQuartile] [decimal](18, 2) NULL,
	[BenchmarkNetIrrMedian] [decimal](18, 2) NULL,
	[BenchmarkNetIrrThirdQuartile] [decimal](18, 2) NULL,
	[BenchmarkTvpiFirstQuartile] [decimal](18, 2) NULL,
	[BenchmarkTvpiMedian] [decimal](18, 2) NULL,
	[BenchmarkTvpiThirdQuartile] [decimal](18, 2) NULL,
 CONSTRAINT [PK_FurtherFundMetrics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_FurtherFundMetrics_FundId] UNIQUE NONCLUSTERED 
(
	[FundId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Grade](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[ExpertiseAlignedWithStrategy] [varchar](50) NULL,
	[HistoryTogether] [varchar](50) NULL,
	[ComplementarySkills] [varchar](50) NULL,
	[TeamDepth] [varchar](50) NULL,
	[ValueAddResources] [varchar](50) NULL,
	[InvestmentThesis] [varchar](50) NULL,
	[CompetitiveAdvantage] [varchar](50) NULL,
	[PortfolioConstruction] [varchar](50) NULL,
	[AppropriatenessOfFundSize] [varchar](50) NULL,
	[ConsistencyOfStrategy] [varchar](50) NULL,
	[Sourcing] [varchar](50) NULL,
	[DueDiligence] [varchar](50) NULL,
	[DecisionMaking] [varchar](50) NULL,
	[DealExecutionStructure] [varchar](50) NULL,
	[PostInvestmentValueAdd] [varchar](50) NULL,
	[TeamStability] [varchar](50) NULL,
	[OwnershipAndCompensation] [varchar](50) NULL,
	[Culture] [varchar](50) NULL,
	[AlignmentWithLPs] [varchar](50) NULL,
	[Terms] [varchar](50) NULL,
	[AbsolutePerformance] [varchar](50) NULL,
	[RelativePerformance] [varchar](50) NULL,
	[RealizedPerformance] [varchar](50) NULL,
	[DepthOfTrackRecord] [varchar](50) NULL,
	[RelevanceOfTrackRecord] [varchar](50) NULL,
	[LossRatioAnalysis] [varchar](50) NULL,
	[UnrealizedPortfolio] [varchar](50) NULL,
	[AtlasValueCreationAnalysis] [varchar](50) NULL,
	[AtlasHitsAndMisses] [varchar](50) NULL,
	[DealSize] [varchar](50) NULL,
	[Strategy] [varchar](50) NULL,
	[Time] [varchar](50) NULL,
	[Team] [varchar](50) NULL,
	[ScatterchartText] [varchar](4000) NULL,
	[AQScorecardText] [varchar](4000) NULL,
	[QoRScorecardText] [varchar](4000) NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
	[DateReviewed] [datetime2](7) NULL,
 CONSTRAINT [PK_Grade] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Group]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Group](
	[Id] [uniqueidentifier] NOT NULL,
	[MaxUsers] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[GroupLeaderId] [uniqueidentifier] NULL,
	[DefaultProductType] [int] NULL,
	[OrganizationProfilePdfAllowed] [bit] NOT NULL,
	[OrganizationProfilePdfQuota] [int] NULL,
	[OrganizationProfilePdfQuotaDays] [int] NULL,
	[OrganizationProfilePdfQuotaActionID] [tinyint] NOT NULL,
	[WatermarkEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganization]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GroupOrganization](
	[GroupId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_GroupOrganization] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GroupOrganizationRestricted](
	[Id] [uniqueidentifier] NOT NULL,
	[GroupId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_GroupOrganizationRestricted] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupSegment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GroupSegment](
	[GroupId] [uniqueidentifier] NOT NULL,
	[SegmentId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_GroupSegment] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC,
	[SegmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LimitedPartner]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LimitedPartner](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Type] [varchar](50) NULL,
	[FundId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_LimitedPartner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[News]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[News](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [varchar](200) NULL,
	[PublishedDate] [datetime] NOT NULL,
	[Url] [varchar](500) NOT NULL,
	[OrganizationSourceId] [uniqueidentifier] NULL,
	[Description] [varchar](max) NULL,
	[OrganizationSourceName] [varchar](255) NULL,
	[Summary] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_NewsItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewsEntity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NewsEntity](
	[NewsId] [uniqueidentifier] NOT NULL,
	[EntityId] [uniqueidentifier] NOT NULL,
	[EntityType] [smallint] NOT NULL,
 CONSTRAINT [PK_NewsEntity] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[EntityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Organization](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NULL,
	[PrimaryOffice] [varchar](255) NULL,
	[Category] [varchar](100) NULL,
	[YearFounded] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MarketStage] [varchar](100) NULL,
	[GeographicFocus] [varchar](100) NULL,
	[Address1Country] [varchar](50) NULL,
	[Address1City] [varchar](50) NULL,
	[LastFundSize] [decimal](18, 2) NULL,
	[FundRaisingStatus] [varchar](50) NULL,
	[NumberOfFunds] [int] NULL,
	[EmergingManager] [bit] NOT NULL,
	[FocusList] [bit] NOT NULL,
	[AccessConstrained] [bit] NOT NULL,
	[DiligenceLevel] [int] NULL,
	[Aum] [decimal](18, 2) NULL,
	[KeyTakeAway] [varchar](max) NULL,
	[History] [varchar](max) NULL,
	[InvestmentThesis] [varchar](max) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[SubSector] [varchar](50) NULL,
	[SubStrategyPipeDelimited] [varchar](max) NULL,
	[SectorFocusPipeDelimited] [varchar](255) NULL,
	[SubSectorFocusPipeDelimited] [varchar](4000) NULL,
	[LastFundClosed] [int] NULL,
	[NumberOfInvestmentProfessionals] [varchar](100) NULL,
	[NumberOfGeneralPartners] [varchar](100) NULL,
	[OrganizationOverview] [varchar](max) NULL,
	[TeamOverview] [varchar](max) NULL,
	[OgranizationColorCode] [varchar](50) NULL,
	[PerformanceColorCode] [varchar](50) NULL,
	[WhatMakesThemUnique] [varchar](4000) NULL,
	[WhatMakesThemUniqueFlag] [varchar](50) NULL,
	[WhatMakesThemUniqueDoc] [varchar](255) NULL,
	[InvestmentStrategyDetails] [varchar](4000) NULL,
	[InvestmentStrategyDetailsFlag] [varchar](50) NULL,
	[InvestmentStrategyDetailsDoc] [varchar](200) NULL,
	[AppropriatenessOfFundSize] [varchar](4000) NULL,
	[AppropriatenessOfFundSizeFlag] [varchar](50) NULL,
	[AppropriatenessOfFundSizeDoc] [varchar](200) NULL,
	[PortfolioConstruction] [varchar](4000) NULL,
	[PortfolioConstructionFlag] [varchar](50) NULL,
	[PortfolioConstructionDoc] [varchar](200) NULL,
	[DealStructuring] [varchar](4000) NULL,
	[DealStructuringFlag] [varchar](50) NULL,
	[DealStructuringDoc] [varchar](200) NULL,
	[RiskManagement] [varchar](4000) NULL,
	[RiskManagementFlag] [varchar](50) NULL,
	[RiskManagementDoc] [varchar](200) NULL,
	[CompetitionAndCompetitiveAdvantage] [varchar](4000) NULL,
	[CompetitionAndCompetitiveAdvantageFlag] [varchar](50) NULL,
	[CompetitionAndCompetitiveAdvantageDoc] [varchar](200) NULL,
	[ExitStrategy] [varchar](4000) NULL,
	[ExitStrategyFlag] [varchar](50) NULL,
	[ExitStrategyDoc] [varchar](200) NULL,
	[StrategyDrift] [varchar](4000) NULL,
	[StrategyDriftFlag] [varchar](50) NULL,
	[StrategyDriftDoc] [varchar](200) NULL,
	[Style] [varchar](50) NULL,
	[EnterpriseMinValue] [decimal](18, 2) NULL,
	[EnterpriseMaxValue] [decimal](18, 2) NULL,
	[RevenueMinValue] [decimal](18, 2) NULL,
	[RevenueMaxValue] [decimal](18, 2) NULL,
	[EbitdaMinValue] [decimal](18, 2) NULL,
	[EbitdaMaxValue] [decimal](18, 2) NULL,
	[EquityCheckMinValue] [decimal](18, 2) NULL,
	[EquityCheckMaxValue] [decimal](18, 2) NULL,
	[TypicalLeverageMinValue] [decimal](18, 2) NULL,
	[TypicalLeverageMaxValue] [decimal](18, 2) NULL,
	[SourcingStrategy] [varchar](4000) NULL,
	[StaffingModel] [varchar](4000) NULL,
	[DueDiligenceProcess] [varchar](4000) NULL,
	[DecisionMakingProcess] [varchar](4000) NULL,
	[DealExecution] [varchar](4000) NULL,
	[ValueAddedApproach] [varchar](4000) NULL,
	[Monitoring] [varchar](4000) NULL,
	[ExitExecution] [varchar](4000) NULL,
	[DealPipeline] [varchar](4000) NULL,
	[SourcingStrategyFlag] [varchar](50) NULL,
	[StaffingModelFlag] [varchar](50) NULL,
	[DueDiligenceProcessFlag] [varchar](50) NULL,
	[DecisionMakingProcessFlag] [varchar](50) NULL,
	[DealExecutionFlag] [varchar](50) NULL,
	[ValueAddedApproachFlag] [varchar](50) NULL,
	[MonitoringFlag] [varchar](50) NULL,
	[ExitExecutionFlag] [varchar](50) NULL,
	[DealPipelineFlag] [varchar](50) NULL,
	[SourcingStrategyDoc] [varchar](200) NULL,
	[StaffingModelDoc] [varchar](200) NULL,
	[DueDiligenceProcessDoc] [varchar](200) NULL,
	[DecisionMakingProcessDoc] [varchar](200) NULL,
	[DealExecutionDoc] [varchar](200) NULL,
	[ValueAddedApproachDoc] [varchar](200) NULL,
	[MonitoringDoc] [varchar](200) NULL,
	[ExitExecutionDoc] [varchar](200) NULL,
	[DealPipelineDoc] [varchar](200) NULL,
	[RecentAdditionsAndDepartures] [varchar](4000) NULL,
	[AdditionalFirmResources] [varchar](4000) NULL,
	[Culture] [varchar](4000) NULL,
	[FirmCapabilitiesAndResources] [varchar](4000) NULL,
	[CompensationStructure] [varchar](4000) NULL,
	[ReportingAndInvestorRelations] [varchar](4000) NULL,
	[CoInvestmentOpportunities] [varchar](4000) NULL,
	[OperationalDiligence] [varchar](4000) NULL,
	[LegalReview] [varchar](4000) NULL,
	[FirmValuationPolicy] [varchar](4000) NULL,
	[RecentAdditionsAndDeparturesFlag] [varchar](50) NULL,
	[AdditionalFirmResourcesFlag] [varchar](50) NULL,
	[CultureFlag] [varchar](50) NULL,
	[FirmCapabilitiesAndResourcesFlag] [varchar](50) NULL,
	[CompensationStructureFlag] [varchar](50) NULL,
	[ReportingAndInvestorRelationsFlag] [varchar](50) NULL,
	[CoInvestmentOpportunitiesFlag] [varchar](50) NULL,
	[OperationalDiligenceFlag] [varchar](50) NULL,
	[LegalReviewFlag] [varchar](50) NULL,
	[FirmValuationPolicyFlag] [varchar](50) NULL,
	[RecentAdditionsAndDeparturesDoc] [varchar](200) NULL,
	[CoInvestmentOpportunitiesDoc] [varchar](200) NULL,
	[OperationalDiligenceDoc] [varchar](200) NULL,
	[LegalReviewDoc] [varchar](200) NULL,
	[FirmValuationPolicyDoc] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[PortfolioDoc] [varchar](200) NULL,
	[WebsiteUrl] [varchar](255) NULL,
	[TrackRecordOverviewFlag] [varchar](50) NULL,
	[TrackRecordOverview] [varchar](max) NULL,
	[TrackRecordOverviewDoc] [varchar](200) NULL,
	[PublishOverviewAndTeam] [bit] NOT NULL,
	[PublishStratProcessFirmAndFund] [bit] NOT NULL,
	[PublishTrackRecord] [bit] NOT NULL,
	[ExpectedNextFundRaise] [datetime] NULL,
	[OrganizationColorCode] [varchar](50) NULL,
	[ReportingAndInvestorRelationsDoc] [varchar](200) NULL,
	[SubCategory] [varchar](255) NULL,
	[FirmCapabilitiesAndResourcesDoc] [varchar](200) NULL,
	[CultureDoc] [varchar](200) NULL,
	[CompensationStructureDoc] [varchar](200) NULL,
	[AdditionalFirmResourcesDoc] [varchar](200) NULL,
	[AliasesPipeDelimited] [varchar](4000) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[CountriesPipeDelimited] [varchar](4000) NULL,
	[SubRegionsPipeDelimited] [varchar](4000) NULL,
	[PublishSearch] [bit] NOT NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[QuantitativeGrade] [varchar](50) NULL,
	[QualitativeGrade] [varchar](50) NULL,
	[SectorSpecialist] [bit] NULL,
	[QualitativeGradeNumber] [float] NULL,
	[QuantitativeGradeNumber] [float] NULL,
	[EvaluationLevel] [varchar](50) NULL,
	[FirmHighlights] [varchar](4000) NULL,
	[EvaluationText] [varchar](4000) NULL,
	[TrackRecordText] [varchar](4000) NULL,
	[FocusRadar] [varchar](50) NULL,
	[CoInvestWithExistingLPs] [varchar](50) NULL,
	[CoInvestWithOtherLPs] [varchar](50) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Address3] [varchar](200) NULL,
	[StateProvince] [varchar](200) NULL,
	[PostalCode] [varchar](50) NULL,
	[SBICFund] [bit] NOT NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
	[PublishProfile] [bit] NOT NULL,
	[PublishLevelAsImported] [varchar](50) NOT NULL,
	[PublishLevelID] [tinyint] NOT NULL,
	[GPScoutPhaseAssigned] [nvarchar](255) NULL,
	[GPScoutPhaseCompleted] [nvarchar](255) NULL,
	[PrimaryOfficeSFID] [varchar](18) NULL,
	[RegionalSpecialist] [bit] NULL,
	[InactiveFirm] [bit] NOT NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Organization_temp_2016_04_21]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Organization_temp_2016_04_21](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NULL,
	[PrimaryOffice] [varchar](255) NULL,
	[Category] [varchar](100) NULL,
	[YearFounded] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MarketStage] [varchar](100) NULL,
	[GeographicFocus] [varchar](100) NULL,
	[Address1Country] [varchar](50) NULL,
	[Address1City] [varchar](50) NULL,
	[LastFundSize] [decimal](18, 2) NULL,
	[FundRaisingStatus] [varchar](50) NULL,
	[NumberOfFunds] [int] NULL,
	[EmergingManager] [bit] NOT NULL,
	[FocusList] [bit] NOT NULL,
	[AccessConstrained] [bit] NOT NULL,
	[DiligenceLevel] [int] NULL,
	[Aum] [decimal](18, 2) NULL,
	[KeyTakeAway] [varchar](max) NULL,
	[History] [varchar](max) NULL,
	[InvestmentThesis] [varchar](max) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[SubSector] [varchar](50) NULL,
	[SubStrategyPipeDelimited] [varchar](max) NULL,
	[SectorFocusPipeDelimited] [varchar](255) NULL,
	[SubSectorFocusPipeDelimited] [varchar](4000) NULL,
	[LastFundClosed] [int] NULL,
	[NumberOfInvestmentProfessionals] [varchar](100) NULL,
	[NumberOfGeneralPartners] [varchar](100) NULL,
	[OrganizationOverview] [varchar](max) NULL,
	[TeamOverview] [varchar](max) NULL,
	[OgranizationColorCode] [varchar](50) NULL,
	[PerformanceColorCode] [varchar](50) NULL,
	[WhatMakesThemUnique] [varchar](4000) NULL,
	[WhatMakesThemUniqueFlag] [varchar](50) NULL,
	[WhatMakesThemUniqueDoc] [varchar](255) NULL,
	[InvestmentStrategyDetails] [varchar](4000) NULL,
	[InvestmentStrategyDetailsFlag] [varchar](50) NULL,
	[InvestmentStrategyDetailsDoc] [varchar](200) NULL,
	[AppropriatenessOfFundSize] [varchar](4000) NULL,
	[AppropriatenessOfFundSizeFlag] [varchar](50) NULL,
	[AppropriatenessOfFundSizeDoc] [varchar](200) NULL,
	[PortfolioConstruction] [varchar](4000) NULL,
	[PortfolioConstructionFlag] [varchar](50) NULL,
	[PortfolioConstructionDoc] [varchar](200) NULL,
	[DealStructuring] [varchar](4000) NULL,
	[DealStructuringFlag] [varchar](50) NULL,
	[DealStructuringDoc] [varchar](200) NULL,
	[RiskManagement] [varchar](4000) NULL,
	[RiskManagementFlag] [varchar](50) NULL,
	[RiskManagementDoc] [varchar](200) NULL,
	[CompetitionAndCompetitiveAdvantage] [varchar](4000) NULL,
	[CompetitionAndCompetitiveAdvantageFlag] [varchar](50) NULL,
	[CompetitionAndCompetitiveAdvantageDoc] [varchar](200) NULL,
	[ExitStrategy] [varchar](4000) NULL,
	[ExitStrategyFlag] [varchar](50) NULL,
	[ExitStrategyDoc] [varchar](200) NULL,
	[StrategyDrift] [varchar](4000) NULL,
	[StrategyDriftFlag] [varchar](50) NULL,
	[StrategyDriftDoc] [varchar](200) NULL,
	[Style] [varchar](50) NULL,
	[EnterpriseMinValue] [decimal](18, 2) NULL,
	[EnterpriseMaxValue] [decimal](18, 2) NULL,
	[RevenueMinValue] [decimal](18, 2) NULL,
	[RevenueMaxValue] [decimal](18, 2) NULL,
	[EbitdaMinValue] [decimal](18, 2) NULL,
	[EbitdaMaxValue] [decimal](18, 2) NULL,
	[EquityCheckMinValue] [decimal](18, 2) NULL,
	[EquityCheckMaxValue] [decimal](18, 2) NULL,
	[TypicalLeverageMinValue] [decimal](18, 2) NULL,
	[TypicalLeverageMaxValue] [decimal](18, 2) NULL,
	[SourcingStrategy] [varchar](4000) NULL,
	[StaffingModel] [varchar](4000) NULL,
	[DueDiligenceProcess] [varchar](4000) NULL,
	[DecisionMakingProcess] [varchar](4000) NULL,
	[DealExecution] [varchar](4000) NULL,
	[ValueAddedApproach] [varchar](4000) NULL,
	[Monitoring] [varchar](4000) NULL,
	[ExitExecution] [varchar](4000) NULL,
	[DealPipeline] [varchar](4000) NULL,
	[SourcingStrategyFlag] [varchar](50) NULL,
	[StaffingModelFlag] [varchar](50) NULL,
	[DueDiligenceProcessFlag] [varchar](50) NULL,
	[DecisionMakingProcessFlag] [varchar](50) NULL,
	[DealExecutionFlag] [varchar](50) NULL,
	[ValueAddedApproachFlag] [varchar](50) NULL,
	[MonitoringFlag] [varchar](50) NULL,
	[ExitExecutionFlag] [varchar](50) NULL,
	[DealPipelineFlag] [varchar](50) NULL,
	[SourcingStrategyDoc] [varchar](200) NULL,
	[StaffingModelDoc] [varchar](200) NULL,
	[DueDiligenceProcessDoc] [varchar](200) NULL,
	[DecisionMakingProcessDoc] [varchar](200) NULL,
	[DealExecutionDoc] [varchar](200) NULL,
	[ValueAddedApproachDoc] [varchar](200) NULL,
	[MonitoringDoc] [varchar](200) NULL,
	[ExitExecutionDoc] [varchar](200) NULL,
	[DealPipelineDoc] [varchar](200) NULL,
	[RecentAdditionsAndDepartures] [varchar](4000) NULL,
	[AdditionalFirmResources] [varchar](4000) NULL,
	[Culture] [varchar](4000) NULL,
	[FirmCapabilitiesAndResources] [varchar](4000) NULL,
	[CompensationStructure] [varchar](4000) NULL,
	[ReportingAndInvestorRelations] [varchar](4000) NULL,
	[CoInvestmentOpportunities] [varchar](4000) NULL,
	[OperationalDiligence] [varchar](4000) NULL,
	[LegalReview] [varchar](4000) NULL,
	[FirmValuationPolicy] [varchar](4000) NULL,
	[RecentAdditionsAndDeparturesFlag] [varchar](50) NULL,
	[AdditionalFirmResourcesFlag] [varchar](50) NULL,
	[CultureFlag] [varchar](50) NULL,
	[FirmCapabilitiesAndResourcesFlag] [varchar](50) NULL,
	[CompensationStructureFlag] [varchar](50) NULL,
	[ReportingAndInvestorRelationsFlag] [varchar](50) NULL,
	[CoInvestmentOpportunitiesFlag] [varchar](50) NULL,
	[OperationalDiligenceFlag] [varchar](50) NULL,
	[LegalReviewFlag] [varchar](50) NULL,
	[FirmValuationPolicyFlag] [varchar](50) NULL,
	[RecentAdditionsAndDeparturesDoc] [varchar](200) NULL,
	[CoInvestmentOpportunitiesDoc] [varchar](200) NULL,
	[OperationalDiligenceDoc] [varchar](200) NULL,
	[LegalReviewDoc] [varchar](200) NULL,
	[FirmValuationPolicyDoc] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[PortfolioDoc] [varchar](200) NULL,
	[WebsiteUrl] [varchar](255) NULL,
	[TrackRecordOverviewFlag] [varchar](50) NULL,
	[TrackRecordOverview] [varchar](max) NULL,
	[TrackRecordOverviewDoc] [varchar](200) NULL,
	[PublishOverviewAndTeam] [bit] NOT NULL,
	[PublishStratProcessFirmAndFund] [bit] NOT NULL,
	[PublishTrackRecord] [bit] NOT NULL,
	[ExpectedNextFundRaise] [datetime] NULL,
	[OrganizationColorCode] [varchar](50) NULL,
	[ReportingAndInvestorRelationsDoc] [varchar](200) NULL,
	[SubCategory] [varchar](255) NULL,
	[FirmCapabilitiesAndResourcesDoc] [varchar](200) NULL,
	[CultureDoc] [varchar](200) NULL,
	[CompensationStructureDoc] [varchar](200) NULL,
	[AdditionalFirmResourcesDoc] [varchar](200) NULL,
	[AliasesPipeDelimited] [varchar](4000) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[CountriesPipeDelimited] [varchar](4000) NULL,
	[SubRegionsPipeDelimited] [varchar](4000) NULL,
	[PublishSearch] [bit] NOT NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[QuantitativeGrade] [varchar](50) NULL,
	[QualitativeGrade] [varchar](50) NULL,
	[SectorSpecialist] [bit] NULL,
	[QualitativeGradeNumber] [float] NULL,
	[QuantitativeGradeNumber] [float] NULL,
	[EvaluationLevel] [varchar](50) NULL,
	[FirmHighlights] [varchar](4000) NULL,
	[EvaluationText] [varchar](4000) NULL,
	[TrackRecordText] [varchar](4000) NULL,
	[FocusRadar] [varchar](50) NULL,
	[CoInvestWithExistingLPs] [varchar](50) NULL,
	[CoInvestWithOtherLPs] [varchar](50) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Address3] [varchar](200) NULL,
	[StateProvince] [varchar](200) NULL,
	[PostalCode] [varchar](50) NULL,
	[SBICFund] [bit] NOT NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
	[PublishProfile] [bit] NOT NULL,
	[PublishLevelAsImported] [varchar](50) NOT NULL,
	[PublishLevelID] [tinyint] NOT NULL,
	[GPScoutPhaseAssigned] [nvarchar](255) NULL,
	[GPScoutPhaseCompleted] [nvarchar](255) NULL,
	[PrimaryOfficeSFID] [varchar](18) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganizationMember](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[JobTitle] [varchar](255) NULL,
	[YearsAtOrganization] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](128) NULL,
	[Address3] [varchar](128) NULL,
	[City] [varchar](50) NULL,
	[StateOrProvince] [varchar](80) NULL,
	[PostalCode] [varchar](50) NULL,
	[Country] [varchar](80) NULL,
	[Email] [varchar](80) NULL,
	[ImageUrl] [varchar](200) NULL,
	[Description] [varchar](max) NULL,
	[DisplayOrder] [int] NULL,
	[AliasesPipeDelimited] [varchar](200) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[SFID] [varchar](18) NULL,
 CONSTRAINT [PK_OrganizationMember] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationNote]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganizationNote](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[Note] [varchar](8000) NULL,
	[DateEntered] [datetime] NOT NULL,
	[GroupId] [uniqueidentifier] NULL,
	[EditedByUserId] [uniqueidentifier] NULL,
	[EditedByDate] [datetime] NULL,
 CONSTRAINT [PK_OrganizationNote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganizationRequest](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[OrganizationName] [varchar](500) NULL,
	[Comment] [varchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Type] [int] NOT NULL,
	[RequestTypeDisplayName]  AS (case [Type] when (2) then 'Research' when (3) then 'Introduction' when (4) then 'Restricted GP Access' when (1) then 'Priority' when (5) then 'Profile Update Completed' when (0) then 'Queue' else '' end),
 CONSTRAINT [PK_OrganizationRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OrganizationViewing](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_OrganaizationViewing] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OtherAddress](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Address3] [varchar](200) NULL,
	[City] [varchar](200) NULL,
	[StateProvince] [varchar](200) NULL,
	[Country] [varchar](100) NULL,
	[PostalCode] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_OtherAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PortfolioCompany]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PortfolioCompany](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[CompanyId] [uniqueidentifier] NOT NULL,
	[CompanyName] [varchar](200) NULL,
	[Description] [varchar](2000) NULL,
 CONSTRAINT [PK_PortfolioCompany] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ResearchPriorityOrganization]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ResearchPriorityOrganization](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[MarketStage] [varchar](100) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[Status] [varchar](100) NULL,
 CONSTRAINT [PK_ResearchPriorityOrganization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RssFeeds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RssFeeds](
	[Id] [uniqueidentifier] NOT NULL,
	[Url] [varchar](max) NOT NULL,
	[SourceOrganizationId] [uniqueidentifier] NULL,
	[SourceOrganizationName] [nvarchar](255) NULL,
	[Enabled] [bit] NOT NULL,
	[DateImported] [datetime] NOT NULL,
 CONSTRAINT [PK_RssFeeds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RSSFeedsRaw]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RSSFeedsRaw](
	[FeedAddress] [nvarchar](max) NULL,
	[Source] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SavedSearches]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SavedSearches](
	[SearchTemplateId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[SavedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_SavedSearches] PRIMARY KEY CLUSTERED 
(
	[SearchTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchOptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SearchOptions](
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SearchOptions_1] PRIMARY KEY CLUSTERED 
(
	[Field] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SearchTemplate](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Name] [varchar](100) NULL,
	[KeywordField] [varchar](255) NULL,
 CONSTRAINT [PK_SearchTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SearchTemplateFilter](
	[Id] [uniqueidentifier] NOT NULL,
	[SearchTemplateId] [uniqueidentifier] NOT NULL,
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NULL,
 CONSTRAINT [PK_SearchTemplateFilter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchTerms]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SearchTerms](
	[UserId] [uniqueidentifier] NOT NULL,
	[SearchTerm] [varchar](200) NOT NULL,
 CONSTRAINT [PK_SearchTerms] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[SearchTerm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Segment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Segment](
	[Id] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Name] [varchar](100) NULL,
	[KeywordField] [varchar](255) NULL,
 CONSTRAINT [PK_SegmentTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SegmentFilter]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SegmentFilter](
	[Id] [uniqueidentifier] NOT NULL,
	[SegmentId] [uniqueidentifier] NOT NULL,
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NULL,
 CONSTRAINT [PK_SegmentFilter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SystemStatus](
	[Property] [varchar](30) NOT NULL,
	[Value] [nvarchar](90) NULL,
 CONSTRAINT [PK_SystemStatus] PRIMARY KEY CLUSTERED 
(
	[Property] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Task]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Task](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](50) NULL,
	[TaskTypeId] [int] NOT NULL,
	[Data] [varchar](max) NULL,
	[StatusId] [int] NOT NULL,
	[ReferenceId] [varchar](50) NULL,
	[UserId] [uniqueidentifier] NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_organization_2015_02_08]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_organization_2015_02_08](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NULL,
	[PrimaryOffice] [varchar](255) NULL,
	[Category] [varchar](100) NULL,
	[YearFounded] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MarketStage] [varchar](100) NULL,
	[GeographicFocus] [varchar](100) NULL,
	[Address1Country] [varchar](50) NULL,
	[Address1City] [varchar](50) NULL,
	[LastFundSize] [decimal](18, 2) NULL,
	[FundRaisingStatus] [varchar](50) NULL,
	[NumberOfFunds] [int] NULL,
	[EmergingManager] [bit] NOT NULL,
	[FocusList] [bit] NOT NULL,
	[AccessConstrained] [bit] NOT NULL,
	[DiligenceLevel] [int] NULL,
	[Aum] [decimal](18, 2) NULL,
	[KeyTakeAway] [varchar](max) NULL,
	[History] [varchar](max) NULL,
	[InvestmentThesis] [varchar](max) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[SubSector] [varchar](50) NULL,
	[SubStrategyPipeDelimited] [varchar](max) NULL,
	[SectorFocusPipeDelimited] [varchar](255) NULL,
	[SubSectorFocusPipeDelimited] [varchar](4000) NULL,
	[LastFundClosed] [int] NULL,
	[NumberOfInvestmentProfessionals] [varchar](100) NULL,
	[NumberOfGeneralPartners] [varchar](100) NULL,
	[OrganizationOverview] [varchar](max) NULL,
	[TeamOverview] [varchar](max) NULL,
	[OgranizationColorCode] [varchar](50) NULL,
	[PerformanceColorCode] [varchar](50) NULL,
	[WhatMakesThemUnique] [varchar](4000) NULL,
	[WhatMakesThemUniqueFlag] [varchar](50) NULL,
	[WhatMakesThemUniqueDoc] [varchar](255) NULL,
	[InvestmentStrategyDetails] [varchar](4000) NULL,
	[InvestmentStrategyDetailsFlag] [varchar](50) NULL,
	[InvestmentStrategyDetailsDoc] [varchar](200) NULL,
	[AppropriatenessOfFundSize] [varchar](4000) NULL,
	[AppropriatenessOfFundSizeFlag] [varchar](50) NULL,
	[AppropriatenessOfFundSizeDoc] [varchar](200) NULL,
	[PortfolioConstruction] [varchar](4000) NULL,
	[PortfolioConstructionFlag] [varchar](50) NULL,
	[PortfolioConstructionDoc] [varchar](200) NULL,
	[DealStructuring] [varchar](4000) NULL,
	[DealStructuringFlag] [varchar](50) NULL,
	[DealStructuringDoc] [varchar](200) NULL,
	[RiskManagement] [varchar](4000) NULL,
	[RiskManagementFlag] [varchar](50) NULL,
	[RiskManagementDoc] [varchar](200) NULL,
	[CompetitionAndCompetitiveAdvantage] [varchar](4000) NULL,
	[CompetitionAndCompetitiveAdvantageFlag] [varchar](50) NULL,
	[CompetitionAndCompetitiveAdvantageDoc] [varchar](200) NULL,
	[ExitStrategy] [varchar](4000) NULL,
	[ExitStrategyFlag] [varchar](50) NULL,
	[ExitStrategyDoc] [varchar](200) NULL,
	[StrategyDrift] [varchar](4000) NULL,
	[StrategyDriftFlag] [varchar](50) NULL,
	[StrategyDriftDoc] [varchar](200) NULL,
	[Style] [varchar](50) NULL,
	[EnterpriseMinValue] [decimal](18, 2) NULL,
	[EnterpriseMaxValue] [decimal](18, 2) NULL,
	[RevenueMinValue] [decimal](18, 2) NULL,
	[RevenueMaxValue] [decimal](18, 2) NULL,
	[EbitdaMinValue] [decimal](18, 2) NULL,
	[EbitdaMaxValue] [decimal](18, 2) NULL,
	[EquityCheckMinValue] [decimal](18, 2) NULL,
	[EquityCheckMaxValue] [decimal](18, 2) NULL,
	[TypicalLeverageMinValue] [decimal](18, 2) NULL,
	[TypicalLeverageMaxValue] [decimal](18, 2) NULL,
	[SourcingStrategy] [varchar](4000) NULL,
	[StaffingModel] [varchar](4000) NULL,
	[DueDiligenceProcess] [varchar](4000) NULL,
	[DecisionMakingProcess] [varchar](4000) NULL,
	[DealExecution] [varchar](4000) NULL,
	[ValueAddedApproach] [varchar](4000) NULL,
	[Monitoring] [varchar](4000) NULL,
	[ExitExecution] [varchar](4000) NULL,
	[DealPipeline] [varchar](4000) NULL,
	[SourcingStrategyFlag] [varchar](50) NULL,
	[StaffingModelFlag] [varchar](50) NULL,
	[DueDiligenceProcessFlag] [varchar](50) NULL,
	[DecisionMakingProcessFlag] [varchar](50) NULL,
	[DealExecutionFlag] [varchar](50) NULL,
	[ValueAddedApproachFlag] [varchar](50) NULL,
	[MonitoringFlag] [varchar](50) NULL,
	[ExitExecutionFlag] [varchar](50) NULL,
	[DealPipelineFlag] [varchar](50) NULL,
	[SourcingStrategyDoc] [varchar](200) NULL,
	[StaffingModelDoc] [varchar](200) NULL,
	[DueDiligenceProcessDoc] [varchar](200) NULL,
	[DecisionMakingProcessDoc] [varchar](200) NULL,
	[DealExecutionDoc] [varchar](200) NULL,
	[ValueAddedApproachDoc] [varchar](200) NULL,
	[MonitoringDoc] [varchar](200) NULL,
	[ExitExecutionDoc] [varchar](200) NULL,
	[DealPipelineDoc] [varchar](200) NULL,
	[RecentAdditionsAndDepartures] [varchar](4000) NULL,
	[AdditionalFirmResources] [varchar](4000) NULL,
	[Culture] [varchar](4000) NULL,
	[FirmCapabilitiesAndResources] [varchar](4000) NULL,
	[CompensationStructure] [varchar](4000) NULL,
	[ReportingAndInvestorRelations] [varchar](4000) NULL,
	[CoInvestmentOpportunities] [varchar](4000) NULL,
	[OperationalDiligence] [varchar](4000) NULL,
	[LegalReview] [varchar](4000) NULL,
	[FirmValuationPolicy] [varchar](4000) NULL,
	[RecentAdditionsAndDeparturesFlag] [varchar](50) NULL,
	[AdditionalFirmResourcesFlag] [varchar](50) NULL,
	[CultureFlag] [varchar](50) NULL,
	[FirmCapabilitiesAndResourcesFlag] [varchar](50) NULL,
	[CompensationStructureFlag] [varchar](50) NULL,
	[ReportingAndInvestorRelationsFlag] [varchar](50) NULL,
	[CoInvestmentOpportunitiesFlag] [varchar](50) NULL,
	[OperationalDiligenceFlag] [varchar](50) NULL,
	[LegalReviewFlag] [varchar](50) NULL,
	[FirmValuationPolicyFlag] [varchar](50) NULL,
	[RecentAdditionsAndDeparturesDoc] [varchar](200) NULL,
	[CoInvestmentOpportunitiesDoc] [varchar](200) NULL,
	[OperationalDiligenceDoc] [varchar](200) NULL,
	[LegalReviewDoc] [varchar](200) NULL,
	[FirmValuationPolicyDoc] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[PortfolioDoc] [varchar](200) NULL,
	[WebsiteUrl] [varchar](255) NULL,
	[TrackRecordOverviewFlag] [varchar](50) NULL,
	[TrackRecordOverview] [varchar](max) NULL,
	[TrackRecordOverviewDoc] [varchar](200) NULL,
	[PublishOverviewAndTeam] [bit] NOT NULL,
	[PublishStratProcessFirmAndFund] [bit] NOT NULL,
	[PublishTrackRecord] [bit] NOT NULL,
	[PublishEvaluation] [bit] NOT NULL,
	[ExpectedNextFundRaise] [datetime] NULL,
	[OrganizationColorCode] [varchar](50) NULL,
	[ReportingAndInvestorRelationsDoc] [varchar](200) NULL,
	[SubCategory] [varchar](255) NULL,
	[FirmCapabilitiesAndResourcesDoc] [varchar](200) NULL,
	[CultureDoc] [varchar](200) NULL,
	[CompensationStructureDoc] [varchar](200) NULL,
	[AdditionalFirmResourcesDoc] [varchar](200) NULL,
	[AliasesPipeDelimited] [varchar](4000) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[CountriesPipeDelimited] [varchar](4000) NULL,
	[SubRegionsPipeDelimited] [varchar](4000) NULL,
	[PublishSearch] [bit] NOT NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[QuantitativeGrade] [varchar](50) NULL,
	[QualitativeGrade] [varchar](50) NULL,
	[SectorSpecialist] [bit] NULL,
	[QualitativeGradeNumber] [float] NULL,
	[QuantitativeGradeNumber] [float] NULL,
	[EvaluationLevel] [varchar](50) NULL,
	[FirmHighlights] [varchar](4000) NULL,
	[EvaluationText] [varchar](4000) NULL,
	[TrackRecordText] [varchar](4000) NULL,
	[FocusRadar] [varchar](50) NULL,
	[CoInvestWithExistingLPs] [varchar](50) NULL,
	[CoInvestWithOtherLPs] [varchar](50) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Address3] [varchar](200) NULL,
	[StateProvince] [varchar](200) NULL,
	[PostalCode] [varchar](50) NULL,
	[SBICFund] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_organization_2015_02_11]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_organization_2015_02_11](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](255) NULL,
	[PrimaryOffice] [varchar](255) NULL,
	[Category] [varchar](100) NULL,
	[YearFounded] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[MarketStage] [varchar](100) NULL,
	[GeographicFocus] [varchar](100) NULL,
	[Address1Country] [varchar](50) NULL,
	[Address1City] [varchar](50) NULL,
	[LastFundSize] [decimal](18, 2) NULL,
	[FundRaisingStatus] [varchar](50) NULL,
	[NumberOfFunds] [int] NULL,
	[EmergingManager] [bit] NOT NULL,
	[FocusList] [bit] NOT NULL,
	[AccessConstrained] [bit] NOT NULL,
	[DiligenceLevel] [int] NULL,
	[Aum] [decimal](18, 2) NULL,
	[KeyTakeAway] [varchar](max) NULL,
	[History] [varchar](max) NULL,
	[InvestmentThesis] [varchar](max) NULL,
	[StrategyPipeDelimited] [varchar](255) NULL,
	[SubSector] [varchar](50) NULL,
	[SubStrategyPipeDelimited] [varchar](max) NULL,
	[SectorFocusPipeDelimited] [varchar](255) NULL,
	[SubSectorFocusPipeDelimited] [varchar](4000) NULL,
	[LastFundClosed] [int] NULL,
	[NumberOfInvestmentProfessionals] [varchar](100) NULL,
	[NumberOfGeneralPartners] [varchar](100) NULL,
	[OrganizationOverview] [varchar](max) NULL,
	[TeamOverview] [varchar](max) NULL,
	[OgranizationColorCode] [varchar](50) NULL,
	[PerformanceColorCode] [varchar](50) NULL,
	[WhatMakesThemUnique] [varchar](4000) NULL,
	[WhatMakesThemUniqueFlag] [varchar](50) NULL,
	[WhatMakesThemUniqueDoc] [varchar](255) NULL,
	[InvestmentStrategyDetails] [varchar](4000) NULL,
	[InvestmentStrategyDetailsFlag] [varchar](50) NULL,
	[InvestmentStrategyDetailsDoc] [varchar](200) NULL,
	[AppropriatenessOfFundSize] [varchar](4000) NULL,
	[AppropriatenessOfFundSizeFlag] [varchar](50) NULL,
	[AppropriatenessOfFundSizeDoc] [varchar](200) NULL,
	[PortfolioConstruction] [varchar](4000) NULL,
	[PortfolioConstructionFlag] [varchar](50) NULL,
	[PortfolioConstructionDoc] [varchar](200) NULL,
	[DealStructuring] [varchar](4000) NULL,
	[DealStructuringFlag] [varchar](50) NULL,
	[DealStructuringDoc] [varchar](200) NULL,
	[RiskManagement] [varchar](4000) NULL,
	[RiskManagementFlag] [varchar](50) NULL,
	[RiskManagementDoc] [varchar](200) NULL,
	[CompetitionAndCompetitiveAdvantage] [varchar](4000) NULL,
	[CompetitionAndCompetitiveAdvantageFlag] [varchar](50) NULL,
	[CompetitionAndCompetitiveAdvantageDoc] [varchar](200) NULL,
	[ExitStrategy] [varchar](4000) NULL,
	[ExitStrategyFlag] [varchar](50) NULL,
	[ExitStrategyDoc] [varchar](200) NULL,
	[StrategyDrift] [varchar](4000) NULL,
	[StrategyDriftFlag] [varchar](50) NULL,
	[StrategyDriftDoc] [varchar](200) NULL,
	[Style] [varchar](50) NULL,
	[EnterpriseMinValue] [decimal](18, 2) NULL,
	[EnterpriseMaxValue] [decimal](18, 2) NULL,
	[RevenueMinValue] [decimal](18, 2) NULL,
	[RevenueMaxValue] [decimal](18, 2) NULL,
	[EbitdaMinValue] [decimal](18, 2) NULL,
	[EbitdaMaxValue] [decimal](18, 2) NULL,
	[EquityCheckMinValue] [decimal](18, 2) NULL,
	[EquityCheckMaxValue] [decimal](18, 2) NULL,
	[TypicalLeverageMinValue] [decimal](18, 2) NULL,
	[TypicalLeverageMaxValue] [decimal](18, 2) NULL,
	[SourcingStrategy] [varchar](4000) NULL,
	[StaffingModel] [varchar](4000) NULL,
	[DueDiligenceProcess] [varchar](4000) NULL,
	[DecisionMakingProcess] [varchar](4000) NULL,
	[DealExecution] [varchar](4000) NULL,
	[ValueAddedApproach] [varchar](4000) NULL,
	[Monitoring] [varchar](4000) NULL,
	[ExitExecution] [varchar](4000) NULL,
	[DealPipeline] [varchar](4000) NULL,
	[SourcingStrategyFlag] [varchar](50) NULL,
	[StaffingModelFlag] [varchar](50) NULL,
	[DueDiligenceProcessFlag] [varchar](50) NULL,
	[DecisionMakingProcessFlag] [varchar](50) NULL,
	[DealExecutionFlag] [varchar](50) NULL,
	[ValueAddedApproachFlag] [varchar](50) NULL,
	[MonitoringFlag] [varchar](50) NULL,
	[ExitExecutionFlag] [varchar](50) NULL,
	[DealPipelineFlag] [varchar](50) NULL,
	[SourcingStrategyDoc] [varchar](200) NULL,
	[StaffingModelDoc] [varchar](200) NULL,
	[DueDiligenceProcessDoc] [varchar](200) NULL,
	[DecisionMakingProcessDoc] [varchar](200) NULL,
	[DealExecutionDoc] [varchar](200) NULL,
	[ValueAddedApproachDoc] [varchar](200) NULL,
	[MonitoringDoc] [varchar](200) NULL,
	[ExitExecutionDoc] [varchar](200) NULL,
	[DealPipelineDoc] [varchar](200) NULL,
	[RecentAdditionsAndDepartures] [varchar](4000) NULL,
	[AdditionalFirmResources] [varchar](4000) NULL,
	[Culture] [varchar](4000) NULL,
	[FirmCapabilitiesAndResources] [varchar](4000) NULL,
	[CompensationStructure] [varchar](4000) NULL,
	[ReportingAndInvestorRelations] [varchar](4000) NULL,
	[CoInvestmentOpportunities] [varchar](4000) NULL,
	[OperationalDiligence] [varchar](4000) NULL,
	[LegalReview] [varchar](4000) NULL,
	[FirmValuationPolicy] [varchar](4000) NULL,
	[RecentAdditionsAndDeparturesFlag] [varchar](50) NULL,
	[AdditionalFirmResourcesFlag] [varchar](50) NULL,
	[CultureFlag] [varchar](50) NULL,
	[FirmCapabilitiesAndResourcesFlag] [varchar](50) NULL,
	[CompensationStructureFlag] [varchar](50) NULL,
	[ReportingAndInvestorRelationsFlag] [varchar](50) NULL,
	[CoInvestmentOpportunitiesFlag] [varchar](50) NULL,
	[OperationalDiligenceFlag] [varchar](50) NULL,
	[LegalReviewFlag] [varchar](50) NULL,
	[FirmValuationPolicyFlag] [varchar](50) NULL,
	[RecentAdditionsAndDeparturesDoc] [varchar](200) NULL,
	[CoInvestmentOpportunitiesDoc] [varchar](200) NULL,
	[OperationalDiligenceDoc] [varchar](200) NULL,
	[LegalReviewDoc] [varchar](200) NULL,
	[FirmValuationPolicyDoc] [varchar](200) NULL,
	[Type] [varchar](50) NULL,
	[PortfolioDoc] [varchar](200) NULL,
	[WebsiteUrl] [varchar](255) NULL,
	[TrackRecordOverviewFlag] [varchar](50) NULL,
	[TrackRecordOverview] [varchar](max) NULL,
	[TrackRecordOverviewDoc] [varchar](200) NULL,
	[PublishOverviewAndTeam] [bit] NOT NULL,
	[PublishStratProcessFirmAndFund] [bit] NOT NULL,
	[PublishTrackRecord] [bit] NOT NULL,
	[PublishEvaluation] [bit] NOT NULL,
	[ExpectedNextFundRaise] [datetime] NULL,
	[OrganizationColorCode] [varchar](50) NULL,
	[ReportingAndInvestorRelationsDoc] [varchar](200) NULL,
	[SubCategory] [varchar](255) NULL,
	[FirmCapabilitiesAndResourcesDoc] [varchar](200) NULL,
	[CultureDoc] [varchar](200) NULL,
	[CompensationStructureDoc] [varchar](200) NULL,
	[AdditionalFirmResourcesDoc] [varchar](200) NULL,
	[AliasesPipeDelimited] [varchar](4000) NULL,
	[CurrencyId] [uniqueidentifier] NULL,
	[CountriesPipeDelimited] [varchar](4000) NULL,
	[SubRegionsPipeDelimited] [varchar](4000) NULL,
	[PublishSearch] [bit] NOT NULL,
	[InvestmentRegionPipeDelimited] [varchar](4000) NULL,
	[QuantitativeGrade] [varchar](50) NULL,
	[QualitativeGrade] [varchar](50) NULL,
	[SectorSpecialist] [bit] NULL,
	[QualitativeGradeNumber] [float] NULL,
	[QuantitativeGradeNumber] [float] NULL,
	[EvaluationLevel] [varchar](50) NULL,
	[FirmHighlights] [varchar](4000) NULL,
	[EvaluationText] [varchar](4000) NULL,
	[TrackRecordText] [varchar](4000) NULL,
	[FocusRadar] [varchar](50) NULL,
	[CoInvestWithExistingLPs] [varchar](50) NULL,
	[CoInvestWithOtherLPs] [varchar](50) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Address1] [varchar](200) NULL,
	[Address2] [varchar](200) NULL,
	[Address3] [varchar](200) NULL,
	[StateProvince] [varchar](200) NULL,
	[PostalCode] [varchar](50) NULL,
	[SBICFund] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_OrganizationRequest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_OrganizationRequest](
	[Id] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[OrganizationName] [varchar](500) NULL,
	[Comment] [varchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Type] [int] NOT NULL,
	[RequestTypeDisplayName] [varchar](20) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_searchoptions_2015_02_08]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_searchoptions_2015_02_08](
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_searchoptions_2015_02_11]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[temp_searchoptions_2015_02_11](
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackRecord](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[FileUrl] [varchar](255) NULL,
	[Order] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TrackRecord] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord_2016_04_04_pre_domain_migration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackRecord_2016_04_04_pre_domain_migration](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[FileUrl] [varchar](255) NULL,
	[Order] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
	[SFID] [varchar](18) NOT NULL,
	[DateImportedUtc] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackUsers](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Type] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_TrackUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackUsersProfileView](
	[Id] [uniqueidentifier] NOT NULL,
	[TrackUsersId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TrackUsersProfileView] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView_2015_07_21]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrackUsersProfileView_2015_07_21](
	[Id] [uniqueidentifier] NOT NULL,
	[TrackUsersId] [uniqueidentifier] NOT NULL,
	[OrganizationId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserEulaAudit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserEulaAudit](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[AgreedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_UserEulaAudit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserExt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserExt](
	[UserId] [uniqueidentifier] NOT NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[GroupId] [uniqueidentifier] NULL,
	[RssAllowed] [bit] NOT NULL,
	[RssNextEmailDate] [datetime] NULL,
	[RssNextEmailOffsetEnum] [smallint] NULL,
	[RssSearchAliases] [bit] NOT NULL,
	[DiligenceAllowed] [bit] NOT NULL,
	[AccessGranted] [bit] NOT NULL,
	[Title] [varchar](255) NULL,
	[Phone] [varchar](50) NULL,
	[OrganizationName] [varchar](100) NULL,
	[OrganizationType] [int] NULL,
	[AUM] [varchar](100) NULL,
	[ReferredBy] [varchar](255) NULL,
	[PreviousLogin] [datetime] NULL,
	[EulaSigned] [datetime] NULL,
 CONSTRAINT [PK_UserExt] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserNotification]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserNotification](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NULL,
	[NotificationTypeId] [tinyint] NOT NULL,
	[Data] [nvarchar](max) NULL,
	[CheckStatusIntervalMS] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Hidden] [bit] NOT NULL,
	[HideTimeoutMS] [int] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[ReferenceId] [varchar](50) NULL,
	[PollingExpirationDate] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_UserNotification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserRssEmail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserRssEmail](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[RssEmailAddress] [varchar](255) NOT NULL,
 CONSTRAINT [PK_UserRssEmail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
  ' 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
  ' 
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND name = N'aspnet_Applications_Index')
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications]
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND name = N'aspnet_Membership_index')
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership]
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND name = N'aspnet_Paths_index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths]
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser]
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND name = N'aspnet_Roles_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles]
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users]
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_ncindex2')
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser]
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index2')
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users]
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND name = N'aspnet_UsersInRoles_index')
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Benchmarks]') AND name = N'IX_Benchmarks_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Benchmarks_SFID_Unique] ON [dbo].[Benchmarks]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Currency]') AND name = N'NonClusteredIndex-20150514-005429')
CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-20150514-005429] ON [dbo].[Currency]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]') AND name = N'IX_EmailDistributionListEmail_Member_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_EmailDistributionListEmail_Member_Unique] ON [dbo].[EmailDistributionListEmail]
(
	[Email] ASC,
	[DistributionListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Entity]') AND name = N'IX_Entity_EntityType')
CREATE NONCLUSTERED INDEX [IX_Entity_EntityType] ON [dbo].[Entity]
(
	[EntityType] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_EntityId')
CREATE NONCLUSTERED INDEX [IX_EntityMultipleValues_EntityId] ON [dbo].[EntityMultipleValues]
(
	[EntityId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_EntityIdField')
CREATE NONCLUSTERED INDEX [IX_EntityMultipleValues_EntityIdField] ON [dbo].[EntityMultipleValues]
(
	[EntityId] ASC,
	[Field] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]') AND name = N'IX_EntityMultipleValues_Field')
CREATE NONCLUSTERED INDEX [IX_EntityMultipleValues_Field] ON [dbo].[EntityMultipleValues]
(
	[Field] ASC
)
INCLUDE ( 	[Id],
	[EntityId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND name = N'IX_Evaluation_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_Evaluation_OrganizationId] ON [dbo].[Evaluation]
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Evaluation]') AND name = N'IX_Evaluation_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Evaluation_SFID_Unique] ON [dbo].[Evaluation]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND name = N'IX_Fund_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_Fund_OrganizationId] ON [dbo].[Fund]
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND name = N'IX_Fund_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Fund_SFID_Unique] ON [dbo].[Fund]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND name = N'IX_Grade_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_Grade_OrganizationId] ON [dbo].[Grade]
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Grade]') AND name = N'IX_Grade_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Grade_SFID_Unique] ON [dbo].[Grade]
(
	[SFID] ASC,
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]') AND name = N'IX_GroupOrganizationRestricted')
CREATE UNIQUE NONCLUSTERED INDEX [IX_GroupOrganizationRestricted] ON [dbo].[GroupOrganizationRestricted]
(
	[OrganizationId] ASC,
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_CurrencyId')
CREATE NONCLUSTERED INDEX [IX_Organization_CurrencyId] ON [dbo].[Organization]
(
	[CurrencyId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_PrimaryOfficeSFID')
CREATE NONCLUSTERED INDEX [IX_Organization_PrimaryOfficeSFID] ON [dbo].[Organization]
(
	[PrimaryOfficeSFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_PublishLevelAsImported')
CREATE NONCLUSTERED INDEX [IX_Organization_PublishLevelAsImported] ON [dbo].[Organization]
(
	[PublishLevelAsImported] ASC
)
INCLUDE ( 	[Id],
	[Name],
	[SFID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organization]') AND name = N'IX_Organization_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Organization_SFID_Unique] ON [dbo].[Organization]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND name = N'IX_OrganizationMember_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_OrganizationMember_OrganizationId] ON [dbo].[OrganizationMember]
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationMember]') AND name = N'IX_OrganizationMember_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrganizationMember_SFID_Unique] ON [dbo].[OrganizationMember]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_OrganizationViewing_OrganizationId] ON [dbo].[OrganizationViewing]
(
	[OrganizationId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_UserID')
CREATE NONCLUSTERED INDEX [IX_OrganizationViewing_UserID] ON [dbo].[OrganizationViewing]
(
	[UserId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]') AND name = N'IX_OrganizationViewing_UserID_Include')
CREATE NONCLUSTERED INDEX [IX_OrganizationViewing_UserID_Include] ON [dbo].[OrganizationViewing]
(
	[UserId] ASC
)
INCLUDE ( 	[OrganizationId],
	[Timestamp]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND name = N'IX_OtherAddress_OrganizationId')
CREATE NONCLUSTERED INDEX [IX_OtherAddress_OrganizationId] ON [dbo].[OtherAddress]
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OtherAddress]') AND name = N'IX_OtherAddress_SFID_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_OtherAddress_SFID_Unique] ON [dbo].[OtherAddress]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND name = N'IX_SearchTemplate_UserID')
CREATE NONCLUSTERED INDEX [IX_SearchTemplate_UserID] ON [dbo].[SearchTemplate]
(
	[UserId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplate]') AND name = N'IX_SearchTemplate_UserID2')
CREATE NONCLUSTERED INDEX [IX_SearchTemplate_UserID2] ON [dbo].[SearchTemplate]
(
	[UserId] ASC
)
INCLUDE ( 	[Id],
	[CreatedDate],
	[Name],
	[KeywordField]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplate_SearchTemplateId_Include2')
CREATE NONCLUSTERED INDEX [IX_SearchTemplate_SearchTemplateId_Include2] ON [dbo].[SearchTemplateFilter]
(
	[SearchTemplateId] ASC
)
INCLUDE ( 	[Id],
	[Field],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId')
CREATE NONCLUSTERED INDEX [IX_SearchTemplateFilter_SearchTemplateId] ON [dbo].[SearchTemplateFilter]
(
	[SearchTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId_Include_Id')
CREATE NONCLUSTERED INDEX [IX_SearchTemplateFilter_SearchTemplateId_Include_Id] ON [dbo].[SearchTemplateFilter]
(
	[SearchTemplateId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]') AND name = N'IX_SearchTemplateFilter_SearchTemplateId_Include2')
CREATE NONCLUSTERED INDEX [IX_SearchTemplateFilter_SearchTemplateId_Include2] ON [dbo].[SearchTemplateFilter]
(
	[SearchTemplateId] ASC
)
INCLUDE ( 	[Id],
	[Field],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackRecord]') AND name = N'IX_Evaluation_TrackRecord_Unique')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Evaluation_TrackRecord_Unique] ON [dbo].[TrackRecord]
(
	[SFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_Type')
CREATE NONCLUSTERED INDEX [IX_TrackUsers_Type] ON [dbo].[TrackUsers]
(
	[Type] ASC
)
INCLUDE ( 	[Id],
	[UserId],
	[Date]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_UserID')
CREATE NONCLUSTERED INDEX [IX_TrackUsers_UserID] ON [dbo].[TrackUsers]
(
	[UserId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsers]') AND name = N'IX_TrackUsers_UserIDType')
CREATE NONCLUSTERED INDEX [IX_TrackUsers_UserIDType] ON [dbo].[TrackUsers]
(
	[UserId] ASC,
	[Type] ASC
)
INCLUDE ( 	[Date]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND name = N'IX_TrackUsersProfileView_TrackUsersID')
CREATE NONCLUSTERED INDEX [IX_TrackUsersProfileView_TrackUsersID] ON [dbo].[TrackUsersProfileView]
(
	[TrackUsersId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]') AND name = N'IX_TrackUsersProfileView_TrackUsersID_Include')
CREATE NONCLUSTERED INDEX [IX_TrackUsersProfileView_TrackUsersID_Include] ON [dbo].[TrackUsersProfileView]
(
	[TrackUsersId] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] ADD  DEFAULT (newid()) FOR [ApplicationId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__412EB0B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] ADD  DEFAULT ((0)) FOR [PasswordFormat]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] ADD  DEFAULT (newid()) FOR [PathId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__66603565]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] ADD  DEFAULT (newid()) FOR [RoleId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (newid()) FOR [UserId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__3C69FB99]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (NULL) FOR [MobileAlias]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT ((0)) FOR [IsAnonymous]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Currency_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EmailDistributionList_Active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EmailDistributionList] ADD  CONSTRAINT [DF_EmailDistributionList_Active]  DEFAULT ((1)) FOR [Active]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EmailDistributionListEmail_Active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EmailDistributionListEmail] ADD  CONSTRAINT [DF_EmailDistributionListEmail_Active]  DEFAULT ((1)) FOR [Active]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Evaluatio__SortO__373B3228]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Evaluation] ADD  DEFAULT ((0)) FOR [SortOrder]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Folder_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF_Folder_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Group__DefaultPr__24285DB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] ADD  CONSTRAINT [DF__Group__DefaultPr__24285DB4]  DEFAULT ((0)) FOR [DefaultProductType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Group_OrganizationProfilePdfAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] ADD  CONSTRAINT [DF_Group_OrganizationProfilePdfAllowed]  DEFAULT ((0)) FOR [OrganizationProfilePdfAllowed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Group__Organizat__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] ADD  DEFAULT ((1)) FOR [OrganizationProfilePdfQuotaActionID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Group_WatermarkEnabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Group] ADD  CONSTRAINT [DF_Group_WatermarkEnabled]  DEFAULT ((0)) FOR [WatermarkEnabled]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_EmergingManager]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_EmergingManager]  DEFAULT ((0)) FOR [EmergingManager]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_FocusList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_FocusList]  DEFAULT ((0)) FOR [FocusList]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_AccessConstrained]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_AccessConstrained]  DEFAULT ((0)) FOR [AccessConstrained]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_NumberOfInvestmentProfessionals]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_NumberOfInvestmentProfessionals]  DEFAULT ('') FOR [NumberOfInvestmentProfessionals]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishOverviewAndTeam]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishOverviewAndTeam]  DEFAULT ((0)) FOR [PublishOverviewAndTeam]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishStratProcessFirmAndFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishStratProcessFirmAndFund]  DEFAULT ((0)) FOR [PublishStratProcessFirmAndFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishTrackRecord]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishTrackRecord]  DEFAULT ((0)) FOR [PublishTrackRecord]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishSearch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishSearch]  DEFAULT ((0)) FOR [PublishSearch]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Organizat__SBICF__6BAEFA67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF__Organizat__SBICF__6BAEFA67]  DEFAULT ((0)) FOR [SBICFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishProfile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishProfile]  DEFAULT ((0)) FOR [PublishProfile]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_PublishLevelAsImported]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_PublishLevelAsImported]  DEFAULT ('') FOR [PublishLevelAsImported]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Organizat__Publi__57A801BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  DEFAULT ((0)) FOR [PublishLevelID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Organization_InactiveFirm]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_InactiveFirm]  DEFAULT ((0)) FOR [InactiveFirm]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] ADD  CONSTRAINT [DF_OrganizationRequest_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_CreatedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] ADD  CONSTRAINT [DF_OrganizationRequest_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] ADD  CONSTRAINT [DF_OrganizationRequest_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganizationRequest_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationRequest] ADD  CONSTRAINT [DF_OrganizationRequest_Type]  DEFAULT ((0)) FOR [Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_OrganaizationViewing_Timestamp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OrganizationViewing] ADD  CONSTRAINT [DF_OrganaizationViewing_Timestamp]  DEFAULT (getdate()) FOR [Timestamp]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SavedSearches_SavedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SavedSearches] ADD  CONSTRAINT [DF_SavedSearches_SavedOn]  DEFAULT (getdate()) FOR [SavedOn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SearchTemplate_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SearchTemplate] ADD  CONSTRAINT [DF_SearchTemplate_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SearchTemplateFilter_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SearchTemplateFilter] ADD  CONSTRAINT [DF_SearchTemplateFilter_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Task_IsCompleted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Task] ADD  CONSTRAINT [DF_Task_IsCompleted]  DEFAULT ((0)) FOR [StatusId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_TrackRecord_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TrackRecord] ADD  CONSTRAINT [DF_TrackRecord_Id]  DEFAULT (newid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserEulaAudit_AgreedOn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserEulaAudit] ADD  CONSTRAINT [DF_UserEulaAudit_AgreedOn]  DEFAULT (getdate()) FOR [AgreedOn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RssAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] ADD  CONSTRAINT [DF_UserExt_RssAllowed]  DEFAULT ((0)) FOR [RssAllowed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RssSearchAliases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] ADD  CONSTRAINT [DF_UserExt_RssSearchAliases]  DEFAULT ((0)) FOR [RssSearchAliases]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_DiligenceAllowed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] ADD  CONSTRAINT [DF_UserExt_DiligenceAllowed]  DEFAULT ((0)) FOR [DiligenceAllowed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserExt_RequestAccess]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserExt] ADD  CONSTRAINT [DF_UserExt_RequestAccess]  DEFAULT ((0)) FOR [AccessGranted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_CheckStatusIntervalMS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] ADD  CONSTRAINT [DF_UserNotification_CheckStatusIntervalMS]  DEFAULT ((0)) FOR [CheckStatusIntervalMS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_Hidden]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] ADD  CONSTRAINT [DF_UserNotification_Hidden]  DEFAULT ((0)) FOR [Hidden]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_HideTimeoutMS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] ADD  CONSTRAINT [DF_UserNotification_HideTimeoutMS]  DEFAULT ((0)) FOR [HideTimeoutMS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_UserNotification_IsValid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UserNotification] ADD  CONSTRAINT [DF_UserNotification_IsValid]  DEFAULT ((1)) FOR [IsValid]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdvisoryBoard_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[AdvisoryBoard]'))
ALTER TABLE [dbo].[AdvisoryBoard]  WITH CHECK ADD  CONSTRAINT [FK_AdvisoryBoard_Fund] FOREIGN KEY([FundId])
REFERENCES [dbo].[Fund] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdvisoryBoard_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[AdvisoryBoard]'))
ALTER TABLE [dbo].[AdvisoryBoard] CHECK CONSTRAINT [FK_AdvisoryBoard_Fund]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH NOCHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__4316F928]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH NOCHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__5EBF139D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__6383C8BA]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__6754599E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__68487DD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__75A278F5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__46E78A0C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles]  WITH NOCHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__3E52440B]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users]  WITH NOCHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH NOCHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH NOCHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Audit]'))
ALTER TABLE [dbo].[Audit]  WITH CHECK ADD  CONSTRAINT [FK_Audit_aspnet_Users] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Audit]'))
ALTER TABLE [dbo].[Audit] CHECK CONSTRAINT [FK_Audit_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BoardSeat_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[BoardSeat]'))
ALTER TABLE [dbo].[BoardSeat]  WITH NOCHECK ADD  CONSTRAINT [FK_BoardSeat_OrganizationMember] FOREIGN KEY([OrganizationMemberId])
REFERENCES [dbo].[OrganizationMember] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BoardSeat_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[BoardSeat]'))
ALTER TABLE [dbo].[BoardSeat] CHECK CONSTRAINT [FK_BoardSeat_OrganizationMember]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DueDiligenceItem_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[DueDiligenceItem]'))
ALTER TABLE [dbo].[DueDiligenceItem]  WITH CHECK ADD  CONSTRAINT [FK_DueDiligenceItem_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DueDiligenceItem_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[DueDiligenceItem]'))
ALTER TABLE [dbo].[DueDiligenceItem] CHECK CONSTRAINT [FK_DueDiligenceItem_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EducationHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EducationHistory]'))
ALTER TABLE [dbo].[EducationHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_EducationHistory_OrganizationMember] FOREIGN KEY([OrganizationMemberId])
REFERENCES [dbo].[OrganizationMember] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EducationHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EducationHistory]'))
ALTER TABLE [dbo].[EducationHistory] CHECK CONSTRAINT [FK_EducationHistory_OrganizationMember]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmailDistributionListEmail_EmailDistributionList]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]'))
ALTER TABLE [dbo].[EmailDistributionListEmail]  WITH CHECK ADD  CONSTRAINT [FK_EmailDistributionListEmail_EmailDistributionList] FOREIGN KEY([DistributionListID])
REFERENCES [dbo].[EmailDistributionList] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmailDistributionListEmail_EmailDistributionList]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmailDistributionListEmail]'))
ALTER TABLE [dbo].[EmailDistributionListEmail] CHECK CONSTRAINT [FK_EmailDistributionListEmail_EmailDistributionList]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmploymentHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmploymentHistory]'))
ALTER TABLE [dbo].[EmploymentHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_EmploymentHistory_OrganizationMember] FOREIGN KEY([OrganizationMemberId])
REFERENCES [dbo].[OrganizationMember] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmploymentHistory_OrganizationMember]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmploymentHistory]'))
ALTER TABLE [dbo].[EmploymentHistory] CHECK CONSTRAINT [FK_EmploymentHistory_OrganizationMember]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntityMultipleValues_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]'))
ALTER TABLE [dbo].[EntityMultipleValues]  WITH NOCHECK ADD  CONSTRAINT [FK_EntityMultipleValues_Entity] FOREIGN KEY([EntityId])
REFERENCES [dbo].[Entity] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntityMultipleValues_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntityMultipleValues]'))
ALTER TABLE [dbo].[EntityMultipleValues] CHECK CONSTRAINT [FK_EntityMultipleValues_Entity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Evaluation_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Evaluation]'))
ALTER TABLE [dbo].[Evaluation]  WITH NOCHECK ADD  CONSTRAINT [FK_Evaluation_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Evaluation_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Evaluation]'))
ALTER TABLE [dbo].[Evaluation] CHECK CONSTRAINT [FK_Evaluation_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Folder_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Folder]'))
ALTER TABLE [dbo].[Folder]  WITH NOCHECK ADD  CONSTRAINT [FK_Folder_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Folder_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[Folder]'))
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick]  WITH NOCHECK ADD  CONSTRAINT [FK_FolderClick_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick] CHECK CONSTRAINT [FK_FolderClick_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick]  WITH NOCHECK ADD  CONSTRAINT [FK_FolderClick_Folder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderClick_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderClick]'))
ALTER TABLE [dbo].[FolderClick] CHECK CONSTRAINT [FK_FolderClick_Folder]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization]  WITH NOCHECK ADD  CONSTRAINT [FK_FolderOrganization_Folder] FOREIGN KEY([FolderId])
REFERENCES [dbo].[Folder] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Folder]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization] CHECK CONSTRAINT [FK_FolderOrganization_Folder]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization]  WITH NOCHECK ADD  CONSTRAINT [FK_FolderOrganization_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FolderOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[FolderOrganization]'))
ALTER TABLE [dbo].[FolderOrganization] CHECK CONSTRAINT [FK_FolderOrganization_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund]  WITH NOCHECK ADD  CONSTRAINT [FK_Fund_Currency] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currency] ([Id])
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] CHECK CONSTRAINT [FK_Fund_Currency]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund]  WITH NOCHECK ADD  CONSTRAINT [FK_Fund_Entity] FOREIGN KEY([Id])
REFERENCES [dbo].[Entity] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] CHECK CONSTRAINT [FK_Fund_Entity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund]  WITH NOCHECK ADD  CONSTRAINT [FK_Fund_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Fund_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Fund]'))
ALTER TABLE [dbo].[Fund] CHECK CONSTRAINT [FK_Fund_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Grade_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Grade]'))
ALTER TABLE [dbo].[Grade]  WITH NOCHECK ADD  CONSTRAINT [FK_Grade_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Grade_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[Grade]'))
ALTER TABLE [dbo].[Grade] CHECK CONSTRAINT [FK_Grade_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Group_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group]  WITH NOCHECK ADD  CONSTRAINT [FK_Group_UserExt] FOREIGN KEY([GroupLeaderId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON UPDATE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Group_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_UserExt]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupOrganization_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization] CHECK CONSTRAINT [FK_GroupOrganization_Group]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupOrganization_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganization_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganization]'))
ALTER TABLE [dbo].[GroupOrganization] CHECK CONSTRAINT [FK_GroupOrganization_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted]  WITH CHECK ADD  CONSTRAINT [FK_GroupOrganizationRestricted_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted] CHECK CONSTRAINT [FK_GroupOrganizationRestricted_Group]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted]  WITH CHECK ADD  CONSTRAINT [FK_GroupOrganizationRestricted_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupOrganizationRestricted_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupOrganizationRestricted]'))
ALTER TABLE [dbo].[GroupOrganizationRestricted] CHECK CONSTRAINT [FK_GroupOrganizationRestricted_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupSegment_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment] CHECK CONSTRAINT [FK_GroupSegment_Group]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_SegmentTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupSegment_SegmentTemplate] FOREIGN KEY([SegmentId])
REFERENCES [dbo].[Segment] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GroupSegment_SegmentTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[GroupSegment]'))
ALTER TABLE [dbo].[GroupSegment] CHECK CONSTRAINT [FK_GroupSegment_SegmentTemplate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LimitedPartner_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[LimitedPartner]'))
ALTER TABLE [dbo].[LimitedPartner]  WITH CHECK ADD  CONSTRAINT [FK_LimitedPartner_Fund] FOREIGN KEY([FundId])
REFERENCES [dbo].[Fund] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LimitedPartner_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[LimitedPartner]'))
ALTER TABLE [dbo].[LimitedPartner] CHECK CONSTRAINT [FK_LimitedPartner_Fund]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NewsEntity_News]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewsEntity]'))
ALTER TABLE [dbo].[NewsEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_NewsEntity_News] FOREIGN KEY([NewsId])
REFERENCES [dbo].[News] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NewsEntity_News]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewsEntity]'))
ALTER TABLE [dbo].[NewsEntity] CHECK CONSTRAINT [FK_NewsEntity_News]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization]  WITH NOCHECK ADD  CONSTRAINT [FK_Organization_Currency] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currency] ([Id])
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization] CHECK CONSTRAINT [FK_Organization_Currency]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization]  WITH NOCHECK ADD  CONSTRAINT [FK_Organization_Entity] FOREIGN KEY([Id])
REFERENCES [dbo].[Entity] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_Entity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]'))
ALTER TABLE [dbo].[Organization] CHECK CONSTRAINT [FK_Organization_Entity]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationMember_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationMember]'))
ALTER TABLE [dbo].[OrganizationMember]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationMember_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationMember_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationMember]'))
ALTER TABLE [dbo].[OrganizationMember] CHECK CONSTRAINT [FK_OrganizationMember_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationNote_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] CHECK CONSTRAINT [FK_OrganizationNote_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationNote_aspnet_Users1] FOREIGN KEY([EditedByUserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_aspnet_Users1]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] CHECK CONSTRAINT [FK_OrganizationNote_aspnet_Users1]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationNote_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
ON DELETE SET NULL
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] CHECK CONSTRAINT [FK_OrganizationNote_Group]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationNote_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationNote_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationNote]'))
ALTER TABLE [dbo].[OrganizationNote] CHECK CONSTRAINT [FK_OrganizationNote_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationRequest_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest] CHECK CONSTRAINT [FK_OrganizationRequest_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganizationRequest_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationRequest_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationRequest]'))
ALTER TABLE [dbo].[OrganizationRequest] CHECK CONSTRAINT [FK_OrganizationRequest_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganaizationViewing_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing] CHECK CONSTRAINT [FK_OrganaizationViewing_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganaizationViewing_UserExt] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserExt] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganaizationViewing_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationViewing]'))
ALTER TABLE [dbo].[OrganizationViewing] CHECK CONSTRAINT [FK_OrganaizationViewing_UserExt]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OtherAddress_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OtherAddress]'))
ALTER TABLE [dbo].[OtherAddress]  WITH NOCHECK ADD  CONSTRAINT [FK_OtherAddress_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OtherAddress_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[OtherAddress]'))
ALTER TABLE [dbo].[OtherAddress] CHECK CONSTRAINT [FK_OtherAddress_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PortfolioCompany_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PortfolioCompany]'))
ALTER TABLE [dbo].[PortfolioCompany]  WITH NOCHECK ADD  CONSTRAINT [FK_PortfolioCompany_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PortfolioCompany_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[PortfolioCompany]'))
ALTER TABLE [dbo].[PortfolioCompany] CHECK CONSTRAINT [FK_PortfolioCompany_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SavedSearches_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SavedSearches]'))
ALTER TABLE [dbo].[SavedSearches]  WITH NOCHECK ADD  CONSTRAINT [FK_SavedSearches_SearchTemplate] FOREIGN KEY([SearchTemplateId])
REFERENCES [dbo].[SearchTemplate] ([Id])
ON UPDATE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SavedSearches_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SavedSearches]'))
ALTER TABLE [dbo].[SavedSearches] CHECK CONSTRAINT [FK_SavedSearches_SearchTemplate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplate_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplate]'))
ALTER TABLE [dbo].[SearchTemplate]  WITH NOCHECK ADD  CONSTRAINT [FK_SearchTemplate_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplate_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplate]'))
ALTER TABLE [dbo].[SearchTemplate] CHECK CONSTRAINT [FK_SearchTemplate_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplateFilter_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]'))
ALTER TABLE [dbo].[SearchTemplateFilter]  WITH NOCHECK ADD  CONSTRAINT [FK_SearchTemplateFilter_SearchTemplate] FOREIGN KEY([SearchTemplateId])
REFERENCES [dbo].[SearchTemplate] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTemplateFilter_SearchTemplate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTemplateFilter]'))
ALTER TABLE [dbo].[SearchTemplateFilter] CHECK CONSTRAINT [FK_SearchTemplateFilter_SearchTemplate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTerms_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTerms]'))
ALTER TABLE [dbo].[SearchTerms]  WITH NOCHECK ADD  CONSTRAINT [FK_SearchTerms_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SearchTerms_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[SearchTerms]'))
ALTER TABLE [dbo].[SearchTerms] CHECK CONSTRAINT [FK_SearchTerms_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SegmentFilter_Segment]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegmentFilter]'))
ALTER TABLE [dbo].[SegmentFilter]  WITH NOCHECK ADD  CONSTRAINT [FK_SegmentFilter_Segment] FOREIGN KEY([SegmentId])
REFERENCES [dbo].[Segment] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SegmentFilter_Segment]') AND parent_object_id = OBJECT_ID(N'[dbo].[SegmentFilter]'))
ALTER TABLE [dbo].[SegmentFilter] CHECK CONSTRAINT [FK_SegmentFilter_Segment]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackRecord_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackRecord]'))
ALTER TABLE [dbo].[TrackRecord]  WITH NOCHECK ADD  CONSTRAINT [FK_TrackRecord_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackRecord_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackRecord]'))
ALTER TABLE [dbo].[TrackRecord] CHECK CONSTRAINT [FK_TrackRecord_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsers_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsers]'))
ALTER TABLE [dbo].[TrackUsers]  WITH CHECK ADD  CONSTRAINT [FK_TrackUsers_UserExt] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserExt] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsers_UserExt]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsers]'))
ALTER TABLE [dbo].[TrackUsers] CHECK CONSTRAINT [FK_TrackUsers_UserExt]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView]  WITH CHECK ADD  CONSTRAINT [FK_TrackUsersProfileView_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_Organization]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView] CHECK CONSTRAINT [FK_TrackUsersProfileView_Organization]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_TrackUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView]  WITH CHECK ADD  CONSTRAINT [FK_TrackUsersProfileView_TrackUsers] FOREIGN KEY([TrackUsersId])
REFERENCES [dbo].[TrackUsers] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrackUsersProfileView_TrackUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrackUsersProfileView]'))
ALTER TABLE [dbo].[TrackUsersProfileView] CHECK CONSTRAINT [FK_TrackUsersProfileView_TrackUsers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserEulaAudit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserEulaAudit]'))
ALTER TABLE [dbo].[UserEulaAudit]  WITH NOCHECK ADD  CONSTRAINT [FK_UserEulaAudit_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserEulaAudit_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserEulaAudit]'))
ALTER TABLE [dbo].[UserEulaAudit] CHECK CONSTRAINT [FK_UserEulaAudit_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt]  WITH NOCHECK ADD  CONSTRAINT [FK_UserExt_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt] CHECK CONSTRAINT [FK_UserExt_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt]  WITH NOCHECK ADD  CONSTRAINT [FK_UserExt_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserExt_Group]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserExt]'))
ALTER TABLE [dbo].[UserExt] CHECK CONSTRAINT [FK_UserExt_Group]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserRssEmail_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserRssEmail]'))
ALTER TABLE [dbo].[UserRssEmail]  WITH NOCHECK ADD  CONSTRAINT [FK_UserRssEmail_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserRssEmail_aspnet_Users]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserRssEmail]'))
ALTER TABLE [dbo].[UserRssEmail] CHECK CONSTRAINT [FK_UserRssEmail_aspnet_Users]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Group_DefaultProductType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group]  WITH NOCHECK ADD  CONSTRAINT [CK_Group_DefaultProductType] CHECK  (([DefaultProductType]>=(0) OR [DefaultProductType]<=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Group_DefaultProductType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Group]'))
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [CK_Group_DefaultProductType]
GO
