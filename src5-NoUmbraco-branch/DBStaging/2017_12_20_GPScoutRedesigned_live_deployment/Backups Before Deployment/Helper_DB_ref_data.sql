IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_help_SF_Mapping_Enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[help_SF_Mapping] DROP CONSTRAINT [DF_help_SF_Mapping_Enabled]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig]') AND type in (N'U'))
DROP TABLE [dbo].[SF_EntityImportConfig]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[help_SF_Mapping]') AND type in (N'U'))
DROP TABLE [dbo].[help_SF_Mapping]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[help_SF_Mapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[help_SF_Mapping](
	[TargetTable] [varchar](200) NULL,
	[TargetField] [varchar](200) NULL,
	[SourceTable] [varchar](200) NULL,
	[SourceField] [varchar](200) NULL,
	[TargetFieldLength] [int] NULL,
	[SourceFieldLength] [int] NULL,
	[Enabled] [bit] NULL,
	[SourceFieldFound] [bit] NULL,
	[TargetFieldFound] [bit] NULL,
	[TargetFieldType] [varchar](50) NULL,
	[SourceFieldType] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig](
	[ID] [int] NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL CONSTRAINT [DF_SF_EntityImportConfig_SelectPageSize]  DEFAULT ((100)),
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL CONSTRAINT [DF_SF_EntityImportConfig_Enabled]  DEFAULT ((1)),
	[OrderBy] [int] NULL,
 CONSTRAINT [PK_SF_EntityImportConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix](
	[ID] [int] NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update](
	[ID] [int] NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c](
	[ID] [int] NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included](
	[ID] [int] NOT NULL,
	[EntityCode] [varchar](200) NOT NULL,
	[EntityFriendlyName] [nvarchar](90) NULL,
	[EntityCodeInSource] [varchar](200) NULL,
	[SourceUri] [varchar](500) NULL,
	[SelectQuery] [nvarchar](max) NULL,
	[FirstSelectQuery] [nvarchar](max) NULL,
	[SelectPageSize] [int] NOT NULL,
	[TargetDTO] [varchar](90) NULL,
	[Enabled] [bit] NOT NULL,
	[OrderBy] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND Provided_By__c=''Preqin North American Buyout''', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported
', N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL

', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, 
Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, 
Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, 
Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, 
Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, 
Track_Record_Text__c, Website, YearGPFounded__c, Most_Recent_Fund_Regional_Specialist__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND Public_Metric_Source__c!='''' 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (17, N'SFFundMetricIDsRCP', N'Fund Metrics IDs of RCP Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (18, N'SFFundMetricIDsPreqin', N'Fund Metrics IDs of Preqin Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (19, N'SFFundMetricsPreqinAndRCPNotMostRecent', N'Further Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', 100, N'dbo.FundMetrics', 1, 500)
GO
SET IDENTITY_INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ON 

GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND IsDeleted=false and Provided_By__c=''Preqin North American Buyout''
', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, Last_Modified_Date__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, Last_Modified_Date__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, Last_Modified_Date__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
SET IDENTITY_INSERT [dbo].[SF_EntityImportConfig_2015_10_05_before__Last_Modified_Date__update] OFF
GO
SET IDENTITY_INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ON 

GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND IsDeleted=false and Provided_By__c=''Preqin North American Buyout''
', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Publish_Metric__c=true AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
SET IDENTITY_INSERT [dbo].[SF_EntityImportConfig_2015_11_17_Before_TR_Changes] OFF
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND Provided_By__c=''Preqin North American Buyout''', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true ORDER BY Date_Reviewed__c DESC LIMIT 1),(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true ORDER BY Effective_Date__c DESC LIMIT 1),Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND Public_Metric_Source__c!='''' 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (17, N'SFFundMetricIDsRCP', N'Fund Metrics IDs of RCP Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (18, N'SFFundMetricIDsPreqin', N'Fund Metrics IDs of Preqin Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_04_21_before__LastScoreCard_of_Account_fix] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (19, N'SFFundMetricsPreqinAndRCPNotMostRecent', N'Further Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', 100, N'dbo.FundMetrics', 1, 500)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND Provided_By__c=''Preqin North American Buyout''', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1)
,Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND Public_Metric_Source__c!='''' 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (17, N'SFFundMetricIDsRCP', N'Fund Metrics IDs of RCP Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (18, N'SFFundMetricIDsPreqin', N'Fund Metrics IDs of Preqin Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_08_10_before__AUM_Calc__c___update] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (19, N'SFFundMetricsPreqinAndRCPNotMostRecent', N'Further Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', 100, N'dbo.FundMetrics', 1, 500)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND Provided_By__c=''Preqin North American Buyout''', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1)
,Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, Radar_List__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND Public_Metric_Source__c!='''' 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (17, N'SFFundMetricIDsRCP', N'Fund Metrics IDs of RCP Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (18, N'SFFundMetricIDsPreqin', N'Fund Metrics IDs of Preqin Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2016_10_31_before_SFAccount_FormGrade__c] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (19, N'SFFundMetricsPreqinAndRCPNotMostRecent', N'Further Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', 100, N'dbo.FundMetrics', 1, 500)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (1, N'Sample', N'Sample', N'sObject API Name here', N'/services/blabla/account/describe', N'', N'', 100, N'Sample', 0, NULL)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (2, N'SFBenchmark', N'Benchmarks', NULL, NULL, N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c  
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported AND Provided_By__c=''Preqin North American Buyout''', N'SELECT  DPI_Lower_Quartile__c,DPI_Median__c,DPI_Upper_Quartile__c,Effective_Date__c,Id,IRR_Lower_Quartile__c,IRR_Median__c,IRR_Upper_Quartile__c,Provided_By__c,TVPI_Lower_Quartile__c,TVPI_Median__c,TVPI_Upper_Quartile__c,Vintage_Year__c ,IsDeleted 
FROM Benchmark_Metric__c 
WHERE IsDeleted = false AND LastModifiedDate < @DateImported and Provided_By__c=''Preqin North American Buyout''', 100, N'dbo.Benchmarks', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (3, N'SFAccount', N'Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported', N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1)
,Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL', 100, N'dbo.Accounts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (4, N'SFFirmAddress', N'FirmAddresses', NULL, NULL, N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id, Account__c, Street_Address__c, City__c,State__c, Country__c, Firm_Location__c, Address_Description__c, Phone__c, Fax__c, Zip_Code__c, Address_2__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Firm_Addresses__c WHERE IsDeleted = false AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.FirmAddreses', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (5, N'SFScorecard', N'GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', N'SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c FROM GPScout_Scorecard__c WHERE Id IN (@MostRecentScorecardIds)', 100, N'dbo.GPScoutScorecards', 1, 5100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (6, N'SFContact', N'Contacts', NULL, NULL, N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)', N'SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c,Account.IsDeleted FROM contact WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported', 100, N'dbo.Contacts', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (7, N'SFFund', N'Funds', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1),Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
', 100, N'dbo.Funds', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (8, N'SFFundMetric', N'Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)', 100, N'dbo.FundMetrics', 1, 300)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (10, N'SFStrengthAndConcern', N'Strength And Concerns', NULL, NULL, N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Id,Display_Order__c,Publish__c,Account__c,Type__c,Name__c,External_Text__c,Internal_Notes__c,IsDeleted,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Strengths__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.StrengthsAndConcerns', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (11, N'SFTrackRecord', N'Track Records', NULL, NULL, N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)', N'SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted FROM Track_Record_Item__c WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false', 100, N'dbo.TrackRecords', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (13, N'SFAdditionalAccounts', N'Additional Accounts', NULL, NULL, N'
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
(SELECT Country__c FROM AIM__Funds__r WHERE Publish__c=true AND IsDeleted=false ORDER BY Effective_Date__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, Industry_Focus__c, Investment_Thesis__c, Key_Takeaway__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Secondary_Strategy__c, Most_Recent_Fund_Sub_Region__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)', NULL, 100, N'dbo.Accounts', 1, 5000)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (14, N'SFGPScoutScorecardRefAccount', N'Account IDs of updated GPScout Scorecards', NULL, NULL, N'SELECT GPScout_Scorecard__c 
FROM GPScout_Scorecard__c 
WHERE GPScout_Scorecard__r.Publish_Level__c!=NULL AND GPScout_Scorecard__r.IsDeleted=false 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (GPScout_Scorecard__r.LastModifiedDate >= @LastModifiedDate AND GPScout_Scorecard__r.LastModifiedDate < @DateImported)) 
GROUP BY GPScout_Scorecard__c', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (15, N'SFFundMetricRefFund', N'Fund IDs of updated Fund Metrics', NULL, NULL, N'SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND Public_Metric_Source__c!='''' 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
', NULL, 100, N'dbo.Ids', 1, 100)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (16, N'SFAdditionalFunds', N'Funds (induced by updated Fund Metrics)', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c!='''' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c,FundTargetSize__c,FundNumber__c
,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)', NULL, 100, N'dbo.Funds', 1, 200)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (17, N'SFFundMetricIDsRCP', N'Fund Metrics IDs of RCP Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''RCP'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentPreqinMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (18, N'SFFundMetricIDsPreqin', N'Fund Metrics IDs of Preqin Metrics', NULL, NULL, N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', N'SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE Public_Metric_Source__c=''Preqin'' AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
FROM AIM__Fund__c 
WHERE Id IN (@FundIDsOfFundsWithMostRecentRCPMetrics)', 100, N'dbo.Ids', 1, 400)
GO
INSERT [dbo].[SF_EntityImportConfig_2017_04_17_before__Most_Recent_Fund_Regional_Specialist__c__included] ([ID], [EntityCode], [EntityFriendlyName], [EntityCodeInSource], [SourceUri], [SelectQuery], [FirstSelectQuery], [SelectPageSize], [TargetDTO], [Enabled], [OrderBy]) VALUES (19, N'SFFundMetricsPreqinAndRCPNotMostRecent', N'Further Fund Metrics', NULL, NULL, N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', N'SELECT Public_Metric_Source__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)', 100, N'dbo.FundMetrics', 1, 500)
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_help_SF_Mapping_Enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[help_SF_Mapping] ADD  CONSTRAINT [DF_help_SF_Mapping_Enabled]  DEFAULT ((1)) FOR [Enabled]
END

GO
