DROP TABLE [dbo].[TrackRecords]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[TrackRecords]
GO

CREATE TABLE [dbo].[TrackRecords](
	[Id] [varchar](18) NOT NULL,
	[Account__c] [varchar](18) NULL,
	[Name] [nvarchar](80) NULL,
	[Description__c] [nvarchar](max) NULL,
	[File_Name__c] [nvarchar](255) NULL,
	[Display_Order__c] [decimal](3, 0) NULL,
	[IsDeleted] [bit] NULL,
	[Publish__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[TopGraphDisplayOrder__c] [nvarchar](30) NULL,
	[PublishDetail__c] [nvarchar](90) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_TrackRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE TrackRecord ADD [PublishDetail] int NULL


--==
--==
ALTER TABLE Funds DROP COLUMN [DateImportedUtc]
ALTER TABLE Funds DROP COLUMN [NewFundId]
ALTER TABLE Funds DROP COLUMN [Include]

ALTER TABLE dbo.Funds ADD Personnel_Senior_Professional__c float NULL
ALTER TABLE dbo.Funds ADD Personnel_Mid_Level__c float NULL
ALTER TABLE dbo.Funds ADD Personnel_Junior_Staff__c float NULL
ALTER TABLE dbo.Funds ADD Personnel_Operating_Partner_Professional__c float NULL

ALTER TABLE dbo.Funds ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.Funds ADD [NewFundId] [uniqueidentifier] NULL
ALTER TABLE dbo.Funds ADD [Include] [bit] NULL

--==
--==
ALTER TABLE Accounts DROP COLUMN [DateImportedUtc]
ALTER TABLE Accounts DROP COLUMN NewOrganizationId
ALTER TABLE Accounts DROP COLUMN [Include]

ALTER TABLE dbo.Accounts ADD GPScoutLevelCompleted__c nvarchar(50) NULL
ALTER TABLE dbo.Accounts ADD Level_of_Disclosure__c nvarchar(50) NULL

ALTER TABLE dbo.Accounts ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.Accounts ADD NewOrganizationId uniqueidentifier NULL
ALTER TABLE dbo.Accounts ADD [Include] [bit] NULL


ALTER TABLE GPScoutScorecards DROP COLUMN [DateImportedUtc]
ALTER TABLE GPScoutScorecards DROP COLUMN [Include]

ALTER TABLE dbo.GPScoutScorecards ADD FirmWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD ProcessWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD StrategyWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD TeamWeightedGrade__c float NULL

ALTER TABLE dbo.GPScoutScorecards ADD PerformanceWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD TrackRecordWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD PerformanceConsistencyWeightedGrade__c float NULL

ALTER TABLE dbo.GPScoutScorecards ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.GPScoutScorecards ADD [Include] [bit] NULL
--==
--==

DROP TABLE [dbo].[Contacts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Contacts](
	[Id] [varchar](18) NOT NULL,
	[AccountId] [varchar](18) NULL,
	[Phone] [nvarchar](40) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Year_Started_With_Current_Firm__c] [decimal](18, 0) NULL,
	[Bio__c] [nvarchar](max) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[MailingCountry] [nvarchar](80) NULL,
	[IsDeleted] [bit] NULL,
	[Publish_Contact__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](80) NULL,
	[Display_Order__c] [decimal](9, 0) NULL,
	[PublishDetail__c] [nvarchar](90) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--===============================
--==
EXEC sp_RENAME 'FundMetrics.Public_Metric_Source__c' , 'GPScoutMetricSource__c', 'COLUMN'

ALTER TABLE Accounts DROP COLUMN [DateImportedUtc]
ALTER TABLE Accounts DROP COLUMN NewOrganizationId
ALTER TABLE Accounts DROP COLUMN [Include]

ALTER TABLE dbo.Accounts ADD GPS_Current_Record_Date__c datetime NULL

ALTER TABLE dbo.Accounts ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.Accounts ADD NewOrganizationId uniqueidentifier NULL
ALTER TABLE dbo.Accounts ADD [Include] [bit] NULL

ALTER TABLE dbo.Ids DROP COLUMN OrgInProcessBeforeImport
ALTER TABLE dbo.Ids ADD IdOfMetricRefByAccountCurrentRecordDate varchar(18) NULL
ALTER TABLE dbo.Ids ADD IdOfUpdatedFundMetric varchar(18) NULL
ALTER TABLE dbo.Ids ADD OrgInProcessBeforeImport uniqueidentifier NULL
--==
--==
ALTER TABLE Funds DROP COLUMN [DateImportedUtc]
ALTER TABLE Funds DROP COLUMN [NewFundId]
ALTER TABLE Funds DROP COLUMN [Include]
GO
ALTER TABLE dbo.Funds ADD Personnel_Sourcing_Professional__c float NULL
GO
ALTER TABLE dbo.Funds ADD [DateImportedUtc] [datetime2](7) NULL
GO
ALTER TABLE dbo.Funds ADD [NewFundId] [uniqueidentifier] NULL
GO
ALTER TABLE dbo.Funds ADD [Include] [bit] NULL
--==
--==
ALTER TABLE FundMetrics DROP COLUMN Publish_Metric__c
GO
--==
--==
ALTER TABLE dbo.Accounts DROP COLUMN GPScout_Phase_Completed__c
--==
--==
EXEC sp_RENAME 'Accounts.GPScout_Last_Updated__c' , 'ProfileUpdateNeeded__c', 'COLUMN'

--==
--===============================
Import SF_EntityImportConfig
Import udf-s 
Import SP-s
[SF_PostImport]: check database name referenced in line containing call to dbo.[usp_Util_ReIndexDatabase_UpdateStats] if SP is propagated into different environment

--==
-- Helper Db
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SFP ZK
-- Create date: 12/18/2017
-- Description:	
-- =============================================
ALTER FUNCTION dbo.udf_IsInProcess 
(
	@GPScoutPhaseAssigned nvarchar(99),
	@GPScoutLevelCompleted nvarchar(99)
)
RETURNS int
AS
BEGIN
	RETURN 
		CASE WHEN @GPScoutPhaseAssigned != 'Not Applicable'
		AND ISNULL(@GPScoutPhaseAssigned, '') != ISNULL(@GPScoutLevelCompleted, '')
		AND (ISNULL(@GPScoutPhaseAssigned, '') != 'Phase 3: Profile' OR ISNULL(@GPScoutLevelCompleted, '') != 'Phase 3: Profile (No Quant)')
			THEN 1
			ELSE 0
		END
END
GO
