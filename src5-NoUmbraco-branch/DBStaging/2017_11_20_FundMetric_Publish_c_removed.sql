﻿-- Publish_Metric__c removed from FundMetrics
SELECT *
INTO [SF_EntityImportConfig_2017_11_19_before_removing_FundMetrics_Publish__c]
FROM [SF_EntityImportConfig]

UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)
',
FirstSelectQuery='
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)
'
WHERE ID=8 AND EntityCode='SFFundMetric'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)
',
FirstSelectQuery='
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)
'
WHERE ID=22 AND EntityCode='SFMetricsFromMetricIDsReferencedByAccountCurrentDate'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND GPScoutMetricSource__c NOT IN ('''',''N/A'') 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
',
FirstSelectQuery=NULL
WHERE ID=15 AND EntityCode='SFFundMetricRefFund'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Effective_Date__c IN (@AccountCurrentRecordDate)
AND IsDeleted=false
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL
',
FirstSelectQuery='
SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Effective_Date__c IN (@AccountCurrentRecordDate)
AND IsDeleted=false
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL
'
WHERE ID=20 AND EntityCode='SFFundMetricIDsReferencedByAccountCurrentDate'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported)
AND AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Fund__r.id!=null
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL
',
FirstSelectQuery=NULL
WHERE ID=21 AND EntityCode='SFIDsOfUpdatedFundMetrics'

ALTER TABLE FundMetrics DROP COLUMN Publish_Metric__c
GO

