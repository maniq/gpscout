﻿DROP TABLE [dbo].[RssFeeds]
DROP TABLE [dbo].[RSSFeedsRaw]
DROP TABLE [dbo].[UserRssEmail]

ALTER TABLE UserExt DROP CONSTRAINT DF_UserExt_RssAllowed, DF_UserExt_RssSearchAliases, DF_UserExt_DiligenceAllowed

ALTER TABLE [dbo].[UserExt] DROP COLUMN [RssAllowed], [RssNextEmailDate], [RssNextEmailOffsetEnum], [RssSearchAliases], [DiligenceAllowed]

exec sp_rename '[dbo].[News_PopulateRssFeeds]','[dbo].[delete_News_PopulateRssFeeds]'

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--	12/06/2018 - SFP ZK - delete from UserRssEmail commented out
-- =============================================
ALTER PROCEDURE [dbo].[udp_DeleteUser]
	@UserID uniqueidentifier
AS

print 'begin deletion tran:'
BEGIN TRAN

delete trupw from TrackUsersProfileView trupw
join TrackUsers tu on trupw.TrackUsersId = tu.Id and tu.UserId = @UserID

delete from TrackUsers where userid=@userid

print 'delete user email data:'
--delete from UserRssEmail where UserId = @UserID
delete from UserNotification where UserId = @UserID
delete from UserEulaAudit where UserId = @UserID
delete from Task where UserId = @UserID
delete from SearchTerms where UserId = @UserID

delete ss from SavedSearches ss join SearchTemplate st ON ss.SearchTemplateId = st.Id and st.UserId = @UserID
delete from SearchTemplate where UserId = @UserID

delete from [dbo].[OrganizationRequest] where UserId = @UserID
delete from [dbo].[OrganizationViewing] where UserId = @UserID
delete from [dbo].[OrganizationNote] where UserId = @UserID

delete from [dbo].FolderClick where UserId = @UserID
delete from [dbo].Folder where UserId = @UserID

print 'group leader update:'
update [group] set GroupLeaderId = null where GroupLeaderId = @UserID

delete from aspnet_PersonalizationPerUser where userid=@userid
delete from aspnet_UsersInRoles where userid=@userid
delete from aspnet_Profile where userid=@userid
delete from aspnet_Membership where userid=@userid
delete from UserExt where UserId = @UserID
delete from aspnet_Users where UserId = @UserID

COMMIT TRAN
--rollback tran


GO