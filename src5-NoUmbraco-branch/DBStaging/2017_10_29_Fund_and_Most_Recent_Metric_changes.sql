﻿UPDATE SF_EntityImportConfig 
  SET Enabled = 0
  WHERE (ID=17 AND EntityCode='SFFundMetricIDsRCP') OR (ID=18 AND EntityCode='SFFundMetricIDsPreqin') OR (ID=19 AND EntityCode='SFFundMetricsPreqinAndRCPNotMostRecent')

UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1)
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted
,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)',

FirstSelectQuery='SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1)
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c
,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false'
WHERE ID=7 AND EntityCode='SFFund'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)',
FirstSelectQuery='SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@MostRecentMetricIds)',
EntityFriendlyName = 'Most Recent Fund Metrics'
WHERE ID=8 AND EntityCode='SFFundMetric'

EXEC sp_RENAME 'FundMetrics.Public_Metric_Source__c' , 'GPScoutMetricSource__c', 'COLUMN'

UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND GPScoutMetricSource__c NOT IN ('''',''N/A'') 
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c',
FirstSelectQuery=NULL
WHERE ID=15 AND EntityCode='SFFundMetricRefFund'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c
,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)
',
FirstSelectQuery=NULL
WHERE ID=16 AND EntityCode='SFAdditionalFunds'

--=================================================================
UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported
',
FirstSelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL
'
WHERE ID=3 AND EntityCode='SFAccount'


UPDATE [SF_EntityImportConfig]
SET SelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, 
Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, 
Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, 
Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, 
Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, 
Track_Record_Text__c, Website, YearGPFounded__c, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)
',
FirstSelectQuery=NULL
WHERE ID=13 AND EntityCode='SFAdditionalAccounts'


INSERT [SF_EntityImportConfig] (ID, EntityCode, EntityFriendlyName, SelectQuery, FirstSelectQuery, SelectPageSize, TargetDTO, Enabled, OrderBy)
SELECT 20, 'SFFundMetricIDsReferencedByAccountCurrentDate','IDs of Fund Metrics Referenced by Accounts via GPS_Current_Record_Date__c','
SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Effective_Date__c IN (@AccountCurrentRecordDate)
',
'SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Effective_Date__c IN (@AccountCurrentRecordDate)
AND IsDeleted=false AND Publish_Metric__c=true 
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL',
100, 'dbo.Ids',1,400


INSERT [SF_EntityImportConfig] (ID, EntityCode, EntityFriendlyName, SelectQuery, FirstSelectQuery, SelectPageSize, TargetDTO, Enabled, OrderBy)
SELECT 21, 'SFIDsOfUpdatedFundMetrics','IDs of Updated Fund Metrics','
SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported)
AND AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Fund__r.id!=null
AND IsDeleted=false AND Publish_Metric__c=true 
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL
',
'SELECT Id
FROM AIMPortMgmt__Investment_Metric__c 
WHERE AIMPortMgmt__Account__r.id!=null AND AIMPortMgmt__Fund__r.id!=null
AND IsDeleted=false AND Publish_Metric__c=true 
AND AIMPortMgmt__Fund__r.IsDeleted=false AND AIMPortMgmt__Fund__r.Publish__c=true
AND AIMPortMgmt__Account__r.IsDeleted=false AND AIMPortMgmt__Account__r.Publish_Level__c!=NULL',
100, 'dbo.Ids',1,400


INSERT [SF_EntityImportConfig] (ID, EntityCode, EntityFriendlyName, SelectQuery, FirstSelectQuery, SelectPageSize, TargetDTO, Enabled, OrderBy)
SELECT 22, 'SFMetricsFromMetricIDsReferencedByAccountCurrentDate','Updated Metrics and Metrics Referenced by Accounts via GPS_Current_Record_Date__c','
SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)',
'SELECT GPScoutMetricSource__c,AIMPortMgmt__Account__r.Id,AIMPortMgmt__Fund__c,Id,AIMPortMgmt__Effective_Date__c,Preqin_Called__c,Preqin_DPI__c,Preqin_TVPI__c
,Preqin_Net_IRR__c,Preqin_Fund_Size__c,IsDeleted,Publish_Metric__c,AIMPortMgmt__Account__r.Publish_Level__c,AIMPortMgmt__Account__r.IsDeleted
,AIMPortMgmt__Fund__r.Publish__c, AIMPortMgmt__Fund__r.IsDeleted,Invested_Capital__c,Realized_Value__c,Unrealized_Value__c,Total_Value__c,Fund_Total_IRR__c
,Fund_Total_Cash_on_Cash__c,DPI__c,Fund_Net_IRR__c,Fund_Net_Cash_on_Cash__c 
FROM AIMPortMgmt__Investment_Metric__c 
WHERE Id IN (@FurtherFundMetricIds)',
100, 'dbo.FundMetrics',1,500

ALTER TABLE Accounts DROP COLUMN [DateImportedUtc]
ALTER TABLE Accounts DROP COLUMN NewOrganizationId
ALTER TABLE Accounts DROP COLUMN [Include]

ALTER TABLE dbo.Accounts ADD GPS_Current_Record_Date__c datetime NULL

ALTER TABLE dbo.Accounts ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.Accounts ADD NewOrganizationId uniqueidentifier NULL
ALTER TABLE dbo.Accounts ADD [Include] [bit] NULL

ALTER TABLE dbo.Ids DROP COLUMN OrgInProcessBeforeImport
ALTER TABLE dbo.Ids ADD IdOfMetricRefByAccountCurrentRecordDate varchar(18) NULL
ALTER TABLE dbo.Ids ADD IdOfUpdatedFundMetric varchar(18) NULL
ALTER TABLE dbo.Ids ADD OrgInProcessBeforeImport uniqueidentifier NULL

ALTER TABLE Organization ADD GPScoutCurrentRecordDate datetime NULL

ALTER TABLE [dbo].[FurtherFundMetrics] DROP CONSTRAINT [IX_FurtherFundMetrics_FundId]
GO
CREATE NONCLUSTERED INDEX [IX_FurtherFundMetrics_FundId] ON [dbo].[FurtherFundMetrics]
(
	[FundId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--===========================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		  05/04/2017 - Most_Recent_Fund_Regional_Specialist__c added to the import logic
--					 - Access_Constrained__c replaced with Access_Constrained_Firm__c
--					 - Populate KeyTakeAway with Overview__c (KeyTakeAway not used anymore)
--					 - Industry_Focus__c removed, not used anymore
--					 - Most_Recent_Fund_Sub_Region__c removed, not used anymore
--		- 06/06/2017 - (CASE Most_Recent_Fund_Fundraising_Status__c WHEN 'Pledge Fund - Active' THEN 'Fundless Sponsor'
--					 - InactiveFirm logic added
--		- 06/28/2017 - adjustment (CASE WHEN Most_Recent_Fund_Fundraising_Status__c IN ('Pledge Fund - Active', 'Pledge Fund - Inactive') THEN 'Fundless Sponsor' ELSE Most_Recent_Fund_Fundraising_Status__c END) Most_Recent_Fund_Fundraising_Status__c, -- Most_Recent_Fund_Fundraising_Status__c moved to SP SF_PostImport
--		- 10/04/2017 - GPScoutLevelCompleted and LevelOfDisclosure fields included
--		- 10/09/2017 - FundRaisingStatus = 'Assumed Closed / Unknown' will import as null
--		- 10/29/2017 - GPScoutCurrentRecordDate datetime NULL column added
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportAccounts]
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_Regional_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_Regional_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_Regional_Specialist__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_SBIC__c NOT IN ('No','Yes') AND Most_Recent_Fund_SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_SBIC__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_is_Sector_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_is_Sector_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_is_Sector_Specialist__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE A
	SET A.Include = CASE WHEN IsDeleted = 0 AND Publish_Level__c IS NOT NULL THEN 1 ELSE 0 END
	FROM dbo.Accounts A

	UPDATE A SET NewOrganizationId = NEWID()
	FROM dbo.Accounts A
	WHERE NOT EXISTS (SELECT NULL FROM main_Organization Target WHERE Target.SFID = A.Id)
		AND A.Include=1

	INSERT main_Entity (Id, EntityType)
	SELECT Source.NewOrganizationId, 1
	FROM dbo.Accounts Source 
	WHERE Source.NewOrganizationId IS NOT NULL AND Source.NewOrganizationId NOT IN (SELECT Id FROM main_Entity WHERE EntityType=1) --AND Include=1
	

	MERGE main_Organization WITH (HOLDLOCK) AS Target
	USING (SELECT 
		ISNULL(Access_Constrained_Firm__c, 0) AS Access_Constrained_Firm__c,
		Alias__c,
		AUM_Calc__c,
		BillingCountry,
		Co_Investment_Opportunities_for_Fund_LP__c,
		Co_Investment_Opportunities_for_Non_LPs__c,
		DateImportedUtc,
		ISNULL(Emerging_Manager__c, 0) AS Emerging_Manager__c,
		Evaluation_Text__c,
		Expected_Next_Fundraise__c,
		Firm_History__c,
		--Grade_Qualitative_Text__c,
		--Grade_Quantitative_Text__c,
		--Grade_Scatterchart_Text__c,
		A.Id,
		--Industry_Focus__c,
		Investment_Thesis__c,
		--Key_Takeaway__c,
		GPScout_Last_Updated__c,
		Most_Recent_Fund_Eff_Size__c,
		NULLIF(Most_Recent_Fund_Fundraising_Status__c, 'Assumed Closed / Unknown') Most_Recent_Fund_Fundraising_Status__c,
		Most_Recent_Fund_Industry_Focus__c,
		(CASE Most_Recent_Fund_is_Sector_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_is_Sector_Specialist__c,
		Most_Recent_Fund_Market_Stage__c,
		Most_Recent_Fund_Primary_Strategy__c,
		Most_Recent_Fund_Region__c,
		(CASE Most_Recent_Fund_Regional_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_Regional_Specialist__c,
		(CASE Most_Recent_Fund_SBIC__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_SBIC__c,
		Most_Recent_Fund_Secondary_Strategy__c,
		--Most_Recent_Fund_Sub_Region__c,
		A.Name,
		NewOrganizationId,
		Overview__c,
		-- if Account has been deleted or unpublished in SF then we unpublish it in GPScout
		(CASE Include WHEN 1 THEN Publish_Level__c ELSE '' END) Publish_Level__c,
		--ISNULL(Radar_List__c, 0) AS Radar_List__c,
		(CAST(CASE WHEN FirmGrade__c='A' OR FirmGrade__c='B' THEN 1 ELSE 0 END AS BIT)) AS FirmGrade__c,
		(CASE FirmGrade__c WHEN 'Inactive' THEN 1 ELSE 0 END) InactiveFirm,
		Region__c,
		Team_Overview__c,
		Total_Closed_Funds__c,
		Track_Record_Text__c,
		Website,
		YearGPFounded__c,
		[Include], -- initialized in UPDATE above
		C.Id CurrencyId,
		GPScout_Phase_Assigned__c,
		GPScout_Phase_Completed__c,
		GPScoutLevelCompleted__c GPScoutLevelCompleted,
		Level_of_Disclosure__c LevelOfDisclosure,
		GPS_Current_Record_Date__c GPScoutCurrentRecordDate
		--A.RecordTypeName,
		--A.ParentId
		FROM dbo.Accounts A
		LEFT JOIN main_Currency C ON A.Most_Recent_Fund_Currency__c = C.Name
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED /*AND Source.[Include] = 1*/ THEN UPDATE SET 
		-- Grade.Scatterchart_Text__c = Source.Scatterchart_Text__c, -- Handled in SF_PostImport
		Target.RegionalSpecialist = Source.Most_Recent_Fund_Regional_Specialist__c,
		Target.AccessConstrained = Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Target.Address1Country = Source.BillingCountry,
		Target.AliasesPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Target.Aum = Source.AUM_Calc__c,
		Target.CoInvestWithExistingLPs = Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Target.CoInvestWithOtherLPs = Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Target.DateImportedUtc = Source.DateImportedUtc,
		Target.EmergingManager = Source.Emerging_Manager__c,
		Target.PublishLevelAsImported = Source.Publish_Level__c,
		Target.EvaluationText = Source.Evaluation_Text__c,
		Target.ExpectedNextFundRaise = Source.Expected_Next_Fundraise__c,
		--Target.FocusList = Source.Radar_List__c,
		Target.FocusList = Source.FirmGrade__c,
		Target.History = Source.Firm_History__c,
		Target.InvestmentRegionPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Target.InvestmentThesis = Source.Investment_Thesis__c,
		Target.KeyTakeAway = Source.Overview__c, --Source.Key_Takeaway__c,
		Target.LastFundSize = Source.Most_Recent_Fund_Eff_Size__c,
		Target.LastUpdated = Source.GPScout_Last_Updated__c,
		Target.MarketStage = Source.Most_Recent_Fund_Market_Stage__c,
		Target.Name = Source.Name,
		Target.NumberOfFunds = Source.Total_Closed_Funds__c,
		Target.OrganizationOverview = Source.Overview__c,
		--Target.QualitativeGrade = Source.Grade_Qualitative_Text__c,	-- Handled in SF_PostImport
		--Target.QuantitativeGrade = Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Target.SBICFund = Source.Most_Recent_Fund_SBIC__c,
		Target.SectorFocusPipeDelimited = dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Target.SectorSpecialist = Source.Most_Recent_Fund_is_Sector_Specialist__c,
		Target.StrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--Target.SubRegionsPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		Target.SubStrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Target.FundRaisingStatus = Source.Most_Recent_Fund_Fundraising_Status__c,
		Target.TeamOverview = Source.Team_Overview__c,
		Target.TrackRecordText = Source.Track_Record_Text__c,
		Target.WebsiteUrl = Source.Website,
		Target.YearFounded = Source.YearGPFounded__c,
		Target.CurrencyId = Source.CurrencyId,
		Target.GPScoutPhaseAssigned = Source.GPScout_Phase_Assigned__c,
		Target.GPScoutPhaseCompleted = Source.GPScout_Phase_Completed__c,
		--Target.RecordTypeName = Source.RecordTypeName,
		--Target.ParentSFID = Source.ParentId,
		Target.InactiveFirm = Source.InactiveFirm,
		Target.GPScoutLevelCompleted = Source.GPScoutLevelCompleted,
		Target.LevelOfDisclosure = Source.LevelOfDisclosure,
		Target.GPScoutCurrentRecordDate = Source.GPScoutCurrentRecordDate

	WHEN NOT MATCHED BY TARGET AND (Source.[Include] = 1 /*OR Source.RecordTypeName = 'Fund Manager Parent'*/) THEN INSERT(
		SFID,
		RegionalSpecialist,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		AccessConstrained,
		Address1Country,
		AliasesPipeDelimited,
		Aum,
		CoInvestWithExistingLPs,
		CoInvestWithOtherLPs,
		DateImportedUtc,
		EmergingManager,
		PublishLevelAsImported,
		EvaluationText,
		ExpectedNextFundRaise,
		FocusList,
		History,
		Id,
		InvestmentRegionPipeDelimited,
		InvestmentThesis,
		KeyTakeAway,
		LastFundSize,
		LastUpdated,
		MarketStage,
		Name,
		NumberOfFunds,
		OrganizationOverview,
		--QualitativeGrade,
		--QuantitativeGrade,
		SBICFund,
		SectorFocusPipeDelimited,
		SectorSpecialist,
		StrategyPipeDelimited,
		--SubRegionsPipeDelimited,
		SubStrategyPipeDelimited,
		FundRaisingStatus,
		TeamOverview,
		TrackRecordText,
		WebsiteUrl,
		YearFounded,
		CurrencyId,
		GPScoutPhaseAssigned,
		GPScoutPhaseCompleted,
		--RecordTypeName,
		--ParentSFID
		InactiveFirm,
		GPScoutLevelCompleted,
		LevelOfDisclosure,
		GPScoutCurrentRecordDate
	) 
	VALUES(
		Source.Id,
		Source.Most_Recent_Fund_Regional_Specialist__c,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Source.BillingCountry,
		dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Source.AUM_Calc__c,
		Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Source.DateImportedUtc,
		Source.Emerging_Manager__c,
		Source.Publish_Level__c,
		Source.Evaluation_Text__c,
		Source.Expected_Next_Fundraise__c,
		--Source.Radar_List__c,
		Source.FirmGrade__c,
		Source.Firm_History__c,
		Source.NewOrganizationId,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Source.Investment_Thesis__c,
		Source.Overview__c, -- Source.Key_Takeaway__c,
		Source.Most_Recent_Fund_Eff_Size__c,
		Source.GPScout_Last_Updated__c,
		Source.Most_Recent_Fund_Market_Stage__c,
		Source.Name,
		Source.Total_Closed_Funds__c,
		Source.Overview__c,
		--Source.Grade_Qualitative_Text__c,		-- Handled in SF_PostImport
		--Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Source.Most_Recent_Fund_SBIC__c,
		dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Source.Most_Recent_Fund_is_Sector_Specialist__c,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Source.Most_Recent_Fund_Fundraising_Status__c,
		Source.Team_Overview__c,
		Source.Track_Record_Text__c,
		Source.Website,
		Source.YearGPFounded__c,
		Source.CurrencyId,
		Source.GPScout_Phase_Assigned__c,
		Source.GPScout_Phase_Completed__c,
		--Source.RecordTypeName,
		--Source.ParentId
		Source.InactiveFirm,
		Source.GPScoutLevelCompleted,
		Source.LevelOfDisclosure,
		Source.GPScoutCurrentRecordDate
	)
--	WHEN MATCHED AND Source.[Include] = 0 THEN -- Organizations are not deleted for now. They can have user created detail info attached
		--DELETE
		;

	UPDATE O
		SET PublishLevelID = CASE
			WHEN A.Include = 0 THEN 0
			WHEN A.Publish_Level__c LIKE '0 -%' THEN 1
			WHEN A.Publish_Level__c LIKE '1 -%' THEN 2
			WHEN A.Publish_Level__c LIKE '2 -%' THEN 3
			WHEN A.Publish_Level__c LIKE '3 -%' THEN 4
			WHEN A.Publish_Level__c LIKE '2.5 -%' THEN 5
			ELSE 0
		END
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	UPDATE O
		SET 
			EvaluationLevel = CASE PublishLevelID
				WHEN 2 THEN 'Initial Assessment'
				WHEN 3 THEN 'Preliminary Evaluation'
				WHEN 4 THEN 'Evaluation'
				WHEN 5 THEN 'Evaluation'
				ELSE ''
			END,
			DiligenceLevel = CASE PublishLevelID
				WHEN 2 THEN 33
				WHEN 3 THEN 66
				WHEN 4 THEN 100
				WHEN 5 THEN 66
				ELSE ''
			END,
			PublishSearch = CASE WHEN PublishLevelID>=1 THEN 1 ELSE 0 END,
			PublishOverviewAndTeam = CASE WHEN PublishLevelID>=2 THEN 1 ELSE 0 END,
			PublishTrackRecord = CASE WHEN PublishLevelID>=3 THEN 1 ELSE 0 END,
			PublishProfile = CASE WHEN PublishLevelID>=4 THEN 1 ELSE 0 END,
			FocusRadar = CASE WHEN O.FocusList=1 THEN 'Focus List' ELSE '' END--,
			--ParentId = NULL
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	-- set parent-child relationship
	--UPDATE ChildOrganization
	--SET ChildOrganization.ParentId = ParentOrganization.Id
	--FROM Accounts A
	--JOIN alexabe_01_stage.dbo.Organization ChildOrganization ON A.Id = ChildOrganization.SFID
	--JOIN alexabe_01_stage.dbo.Organization ParentOrganization ON A.ParentId = ParentOrganization.SFID

	-- remove child items of unpublished Organizations
	SELECT Id INTO #UnpublishedOrgs FROM main_Organization WHERE PublishLevelID=0

	DELETE E FROM main_Evaluation E JOIN #UnpublishedOrgs O ON E.OrganizationId=O.Id 
	DELETE G FROM main_Grade G JOIN #UnpublishedOrgs O ON G.OrganizationId=O.Id 
	DELETE M FROM main_OrganizationMember M JOIN #UnpublishedOrgs O ON M.OrganizationId=O.Id 
	--DELETE M FROM main_OrganizationMember M WHERE NOT EXISTS (SELECT NULL FROM main_Organization O WHERE O.Id=M.OrganizationId AND O.PublishLevelID>0)
	DELETE A FROM main_OtherAddress A JOIN #UnpublishedOrgs O ON A.OrganizationId=O.Id 
	DELETE T FROM main_TrackRecord T JOIN #UnpublishedOrgs O ON T.OrganizationId=O.Id
	DELETE F FROM main_Fund F JOIN #UnpublishedOrgs O ON F.OrganizationId=O.Id

	DROP TABLE #UnpublishedOrgs

	DELETE main_ResearchPriorityOrganization
	INSERT main_ResearchPriorityOrganization (
			[Id]
           ,[Name]
           ,[MarketStage]
           ,[StrategyPipeDelimited]
           ,[InvestmentRegionPipeDelimited])
     SELECT
           Id
           ,Name
           ,MarketStage
           ,StrategyPipeDelimited
           ,InvestmentRegionPipeDelimited
	FROM main_Organization
	WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '') AND PublishLevelAsImported!=''

	COMMIT TRAN
END


SET ANSI_NULLS ON



--============================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	This logic imports not most recent rcp fund metrics
-- History:
--	10/29/2017 - Logic change related to GPScoutMetricSource__c
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFurtherFundMetrics]
AS

DELETE fm
FROM main_FurtherFundMetrics fm
--JOIN main_Fund f ON fm.FundId = f.Id
--JOIN Funds stgFunds ON f.SFID = stgFunds.Id
JOIN FundMetrics stgFundMetrics ON fm.SFID = stgFundMetrics.Id

INSERT main_FurtherFundMetrics (
		Id, FundId, 
		PreqinAsOf, 
		[PreqinCalled], 
		[PreqinDpi],
		PreqinTvpi, 
		[PreqinIrr], 
		[PreqinFundSize], 
		[InvestedCapital],
		[RealizedValue], 
		[UnrealizedValue], 
		[TotalValue], 
		[GrossIrr], 
		[Moic], 
		[Dpi], 
		[Irr], 
		[Tvpi], 
		[AsOf], 
		[DateImportedUtc], 
		[SFID], 
		[Source]--, 
		--[DPIQuartile], 
		--[TVPIQuartile], 
		--[NetIrrQuartile]
)
SELECT NEWID(), F.Id FundID, 
	CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
	CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END, -- tvpi
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc] END,
	Source.Id, -- SFID
	Source.GPScoutMetricSource__c -- Source.Public_Metric_Source__c
FROM FundMetrics Source 
--JOIN Funds StgFunds ON StgFunds.Id = Source.[AIMPortMgmt__Fund__c]
JOIN main_Fund F ON Source.AIMPortMgmt__Fund__c = F.SFID
--WHERE Source.ImportStep IS NULL AND ISNULL(Source.IsMostRecent, 0) !=1 -- these 2 criterias for now are the same


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM main_FurtherFundMetrics fm
LEFT JOIN main_Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN main_Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source LIKE 'RCP%'
OR fm.Source = '' OR fm.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM main_FurtherFundMetrics fm
LEFT JOIN main_Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN main_Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='Preqin'

GO

--============================================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--	10/29/2017 - Public_Metric_Source__c replaced with GPScoutMetricSource__c and additional logic change
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFundMetrics]
AS
BEGIN

	SET NOCOUNT ON;

	--UPDATE FM
	--SET IsMostRecent = CASE WHEN LatestFMs.MaxEffectiveDate IS NOT NULL THEN 1 ELSE 0 END
	--FROM FundMetrics FM
	--LEFT JOIN (
	--	SELECT FM2.AIMPortMgmt__Fund__c, MAX(FM2.[AIMPortMgmt__Effective_Date__c]) MaxEffectiveDate
	--	FROM FundMetrics FM2 
	--	GROUP BY FM2.AIMPortMgmt__Fund__c
	--) LatestFMs
	--	ON FM.AIMPortMgmt__Fund__c = LatestFMs.AIMPortMgmt__Fund__c AND FM.AIMPortMgmt__Effective_Date__c = LatestFMs.MaxEffectiveDate AND FM.Publish_Metric__c=1
	--		AND FM.IsDeleted=0

	-- Metrics marked w/ ImportStep=1 means that these metrics have been imported as Most Recent Fund Metrics
	UPDATE FundMetrics
	SET 
		IsMostRecent = CASE WHEN EXISTS (SELECT NULL FROM Funds WHERE Funds.MostRecentMetricSFID=FundMetrics.Id AND FundMetrics.ImportStep = 1) THEN 1 ELSE 0 END,
		--[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Public_Metric_Source__c, '') != ''
		[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(GPScoutMetricSource__c, '') NOT IN ('','N/A')
		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
	FROM FundMetrics

	MERGE main_Fund WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  fm.[AIMPortMgmt__Effective_Date__c]
		  ,fm.[Preqin_Called__c]
		  ,fm.[Preqin_DPI__c]
		  ,fm.[Preqin_TVPI__c]
		  ,fm.[Preqin_Net_IRR__c]
		  ,fm.[Preqin_Fund_Size__c]
--		  ,fm.[DateImportedUtc]
--		  ,fm.[Id]
		  --,o.[Id] AS [OrganizationId]
		  --,[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Publish_Metric__c, '') != ''
--		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
--		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
		  ,fm.[Include]
		  ,fm.[Invested_Capital__c]
		  ,fm.[Realized_Value__c]
		  ,fm.[Unrealized_Value__c]
		  ,fm.[Total_Value__c]
		  ,fm.[Fund_Total_IRR__c]
		  ,fm.[Fund_Total_Cash_on_Cash__c]
		  ,fm.[DPI__c]
		  ,fm.[Fund_Net_IRR__c]
		  ,fm.[Fund_Net_Cash_on_Cash__c]
		  ,fm.AIMPortMgmt__Fund__c FundSFID
		  --,Public_Metric_Source__c
		  ,fm.GPScoutMetricSource__c
		  --,IsMoreRecentThanExisting = CASE WHEN F.Id IS NULL THEN 0 ELSE 1 END
	  FROM [FundMetrics] fm
	  --LEFT JOIN main_Fund F 
		--ON fm.AIMPortMgmt__Fund__c = F.SFID AND fm.[AIMPortMgmt__Effective_Date__c] >= CASE WHEN ISNULL(F.AsOf, '1/1/1900')>=ISNULL(F.PreqinAsOf, '1/1/1900') THEN F.AsOf ELSE F.PreqinAsOf END
--	  LEFT JOIN main_Organization o ON fm.AIMPortMgmt__Account__c = o.SFID
--	  WHERE o.Id IS NOT NULL
	  WHERE IsMostRecent = 1
	  ) AS Source
	ON Target.SFID = Source.FundSFID

	WHEN MATCHED THEN
		UPDATE SET 
			Target.PreqinAsOf = CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
			Target.PreqinCalled = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c]	END,
			Target.PreqinDpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
			Target.PreqinTvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
			Target.PreqinIrr = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
			Target.PreqinFundSize = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c]	END,
--			Target.[DateImportedUtc] = CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc]	END,
			Target.InvestedCapital = CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
			Target.RealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
			Target.UnrealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
			Target.TotalValue = CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
			Target.GrossIrr = CASE 
								WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
								WHEN Source.[Include] = 0 THEN NULL
							END,
			Target.Moic = CASE 
							WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Dpi = CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
			Target.Irr = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
			Target.Tvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END,
			Target.AsOf = CASE 
							WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Source = Source.GPScoutMetricSource__c -- Source.Public_Metric_Source__c
		;
/*
		AND Source.[Include] = 1 THEN UPDATE SET 
		Target.PreqinAsOf = Source.[AIMPortMgmt__Effective_Date__c],
		Target.PreqinCalled = Source.[Preqin_Called__c],
		Target.PreqinDpi = Source.[Preqin_DPI__c],
		Target.PreqinTvpi = Source.[Preqin_TVPI__c],
		Target.PreqinIrr = Source.[Preqin_Net_IRR__c],
		Target.PreqinFundSize = Source.[Preqin_Fund_Size__c],
--		Target.[DateImportedUtc] = Source.[DateImportedUtc],
--		Target.[OrganizationId] = Source.[OrganizationId],
		Target.InvestedCapital = Source.[Invested_Capital__c],
		Target.RealizedValue = Source.[Realized_Value__c],
		Target.UnrealizedValue = Source.[Unrealized_Value__c],
		Target.TotalValue = Source.[Total_Value__c],
		Target.GrossIrr = Source.[Fund_Total_IRR__c],
		Target.Moic = Source.[Fund_Total_Cash_on_Cash__c],
		Target.Dpi = Source.[DPI__c],
		Target.Irr = Source.[Fund_Net_IRR__c],
		Target.Tvpi = Source.[Fund_Net_Cash_on_Cash__c],
		Target.AsOf = Source.[AIMPortMgmt__Effective_Date__c]
*/


-- Inserts and deletes should not occur b/c they already have been handled by SP [SF_ImportFunds]

	--WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
	--		Id
	--		,PreqinAsOf
	--		,PreqinCalled
	--		,PreqinDpi
	--		,PreqinTvpi
	--		,PreqinIrr
	--		,PreqinFundSize
--	--		,DateImportedUtc
	--		,SFID
	--		,[OrganizationId]
	--		,InvestedCapital
	--		,RealizedValue
	--		,UnrealizedValue
	--		,TotalValue
	--		,GrossIrr
	--		,Moic
	--		,Dpi
	--		,Irr
	--		,Tvpi
	--		,AsOf
	--) 
	--	VALUES(	NEWID()
	--			,Source.[AIMPortMgmt__Effective_Date__c]
	--			,Source.[Preqin_Called__c]
	--			,Source.[Preqin_DPI__c]
	--			,Source.[Preqin_TVPI__c]
	--			,Source.[Preqin_Net_IRR__c]
	--			,Source.[Preqin_Fund_Size__c]
--	--			,Source.[DateImportedUtc]
	--			,Source.FundSFID--[Id]
	--			,Source.[OrganizationId]
	--			,Source.[Invested_Capital__c]
	--			,Source.[Realized_Value__c]
	--			,Source.[Unrealized_Value__c]
	--			,Source.[Total_Value__c]
	--			,Source.[Fund_Total_IRR__c]
	--			,Source.[Fund_Total_Cash_on_Cash__c]
	--			,Source.[DPI__c]
	--			,Source.[Fund_Net_IRR__c]
	--			,Source.[Fund_Net_Cash_on_Cash__c]
	--			,Source.[AIMPortMgmt__Effective_Date__c]

	--	)
	--WHEN MATCHED AND Source.[Include] = 0 THEN 
	--	DELETE;

	/*
	UPDATE main_Fund
	SET PerformanceDataSource = CASE
		WHEN 
			GrossIrr IS NULL 
			AND Moic IS NULL 
			AND Dpi IS NULL 
			AND Irr IS NULL 
			AND Tvpi IS NULL
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	*/

	UPDATE F
	SET PerformanceDataSource = CASE
		--WHEN fm.Publish_Metric__c = 'Preqin'
		--WHEN fm.Public_Metric_Source__c = 'Preqin'
		WHEN fm.GPScoutMetricSource__c = 'Preqin'
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	FROM main_Fund F
	JOIN [FundMetrics] fm ON 
		F.SFID = fm.AIMPortMgmt__Fund__c -- fm.AIMPortMgmt__Fund__c is SFID of Fund
		AND fm.[IsMostRecent] = 1

	UPDATE Fund
	SET
		Fund.PreqinAsOf = NULL,
		Fund.PreqinCalled = NULL,
		Fund.PreqinDpi = NULL,
		Fund.PreqinTvpi = NULL,
		Fund.PreqinIrr = NULL,
		Fund.PreqinFundSize = NULL,
--		Fund.[DateImportedUtc] = NULL,
--		Fund.[OrganizationId] = Source.[OrganizationId],
		Fund.InvestedCapital = NULL,
		Fund.RealizedValue = NULL,
		Fund.UnrealizedValue = NULL,
		Fund.TotalValue = NULL,
		Fund.GrossIrr = NULL,
		Fund.Moic = NULL,
		Fund.Dpi = NULL,
		Fund.Irr = NULL,
		Fund.Tvpi = NULL,
		Fund.AsOf = NULL,
		Fund.Source = NULL,
		Fund.[BenchmarkAsOf] = NULL,
		Fund.[BenchmarkDpiFirstQuartile] = NULL,
		Fund.[BenchmarkDpiMedian] = NULL,
		Fund.[BenchmarkDpiThirdQuartile] = NULL,

		Fund.[BenchmarkNetIrrFirstQuartile] = NULL,
		Fund.[BenchmarkNetIrrMedian] = NULL,
		Fund.[BenchmarkNetIrrThirdQuartile] = NULL,

		Fund.[BenchmarkTvpiFirstQuartile] = NULL,
		Fund.[BenchmarkTvpiMedian] = NULL,
		Fund.[BenchmarkTvpiThirdQuartile] = NULL
	FROM main_Fund Fund
	JOIN Funds ON Fund.SFID = Funds.Id AND Funds.MostRecentMetricSFID IS NULL

	
	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.Dpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.Tvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.Irr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM main_Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN main_Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source LIKE 'RCP%' -- F.Source = 'RCP' 10/29/2017
	OR F.Source = '' OR F.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM main_Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN main_Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='Preqin'
	

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	
--	Note: check database name referenced in line containing call to dbo.[usp_Util_ReIndexDatabase_UpdateStats] if SP is propagated into different environment
-- History:
--		05/04/2017 SFP ZK	- MostRecentFundCountries update not used anymore
--		06/28/2017 SFP ZK	- Organization.FundraisingStatus IN ('Pledge Fund - Active', 'Pledge Fund - Inactive') set to 'Fundless Sponsor'
-- =============================================
-- EXEC [SF_PostImport] '10/2/2017 5:07:31 PM'  
ALTER PROCEDURE [dbo].[SF_PostImport]
	@DateImportedUtc datetime
AS

DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

BEGIN TRY 

BEGIN TRAN

UPDATE Accounts SET DateImportedUtc = @DateImportedUtc
UPDATE Benchmarks SET DateImportedUtc = @DateImportedUtc
UPDATE Contacts SET DateImportedUtc = @DateImportedUtc
UPDATE FirmAddreses SET DateImportedUtc = @DateImportedUtc
UPDATE TrackRecords SET DateImportedUtc = @DateImportedUtc
UPDATE Funds SET DateImportedUtc = @DateImportedUtc
UPDATE FundMetrics SET DateImportedUtc = @DateImportedUtc
--UPDATE Metrics SET DateImportedUtc = @DateImportedUtc
UPDATE StrengthsAndConcerns SET DateImportedUtc = @DateImportedUtc
UPDATE GPScoutScorecards SET DateImportedUtc = @DateImportedUtc

UPDATE Accounts SET Most_Recent_Fund_Currency__c = 'US Dollar' WHERE Most_Recent_Fund_Currency__c IS NULL OR Most_Recent_Fund_Currency__c=''
UPDATE Funds SET Currency__c = 'US Dollar' WHERE Currency__c IS NULL OR Currency__c=''

-- store Org Id-s being in In Process mode before data import
DELETE Ids WHERE OrgInProcessBeforeImport IS NOT NULL
INSERT IDs (OrgInProcessBeforeImport)
SELECT Id FROM main_Organization WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '')

INSERT INTO main_Currency(Id, Code, Name, Symbol)
SELECT NEWID(), 'n/a', CurrencyName, 'n/a'
FROM (
	SELECT DISTINCT ISNULL(CurrencyName, '') CurrencyName
	FROM (
	SELECT Currency__c CurrencyName
	FROM Funds
	UNION ALL
	SELECT Most_Recent_Fund_Currency__c 
	FROM Accounts
	) C2
) C
WHERE C.CurrencyName NOT IN (SELECT C3.Name FROM main_Currency C3)


EXEC SF_ImportBenchmarks

/*
TRUNCATE TABLE Benchmarks_Last

INSERT INTO Benchmarks_Last (Id, VintageYear, AsOf, 
	[DpiFirstQuartile], [DpiMedian], [DpiThirdQuartile],
	[NetIrrFirstQuartile], [NetIrrMedian], [NetIrrThirdQuartile],
	[TvpiFirstQuartile], [TvpiMedian], [TvpiThirdQuartile])
SELECT 
	B.Id, B.VintageYear, B.AsOf,
	B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile,
	B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile,
	B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile
FROM (
	SELECT MAX(B2.AsOf) MaxAsOf, VintageYear 
	FROM alexabe_01_stage.dbo.Benchmarks B2 
	GROUP BY B2.VintageYear
) A
JOIN alexabe_01_stage.dbo.Benchmarks B ON A.VintageYear = B.VintageYear AND A.MaxAsOf = B.AsOf
*/

PRINT 'EXEC SF_ImportAccounts:'
EXEC SF_ImportAccounts
PRINT 'EXEC SF_ImportFunds:'
EXEC SF_ImportFunds
--EXEC SF_ImportMetrics
PRINT 'EXEC SF_ImportFundMetrics:'
EXEC SF_ImportFundMetrics
PRINT 'EXEC SF_ImportFurtherFundMetrics:'
EXEC SF_ImportFurtherFundMetrics
PRINT 'EXEC SF_ImportContacts:'
EXEC SF_ImportContacts
PRINT 'EXEC SF_ImportFirmAddresses:'
EXEC SF_ImportFirmAddresses
PRINT 'EXEC SF_ImportTrackRecords:'
EXEC SF_ImportTrackRecords
PRINT 'EXEC SF_ImportStrenghtsAndConcerns:'
EXEC SF_ImportStrenghtsAndConcerns
PRINT 'EXEC SF_ImportGPScoutScorecards:'
EXEC SF_ImportGPScoutScorecards

EXEC [dbo].[SF_PostImport_FirmAddresses]

-- Handled in SP [SF_ImportGPScoutScorecards] instead
--DELETE	G
--FROM	(
--		SELECT	ROW_NUMBER() OVER (PARTITION BY OrganizationId ORDER BY [DateReviewed] DESC) AS RecID 
--		FROM	alexabe_01_stage.dbo.Grade
--		) AS G
--WHERE	RecID > 1 


-- insert Grade record for Organizations not having at least one Grade record. This is required b/c we need to store some Organization level info at Grade level anyway. See next update statement.
INSERT main_Grade (Id, OrganizationId, DateImportedUtc, SFID)
SELECT NEWID(), O.Id, @DateImportedUtc, ''
FROM main_Organization O
WHERE O.Id NOT IN (SELECT G.OrganizationId FROM main_Grade G) 


UPDATE G
SET
	G.ScatterchartText = A.Grade_Scatterchart_Text__c,
	G.AQScorecardText = A.Grade_Qualitative_Text__c,
	G.QoRScorecardText = A.Grade_Quantitative_Text__c
FROM [main_Grade] G
JOIN main_Organization O ON G.OrganizationId = O.Id
JOIN dbo.Accounts A ON O.SFID = A.Id


UPDATE O
SET 
	O.QualitativeGradeNumber = SC.Scaled_Score__c,
	O.QualitativeGrade = dbo.udf_GetGradeWord(SC.Scaled_Score__c), --  SC.Qualitative_Final_Grade__c,
	O.QuantitativeGrade = dbo.udf_GetGradeWord(SC.Quantitative_Final_Number__c), -- SC.Quantitative_Final_Grade__c,
	O.QuantitativeGradeNumber = SC.Quantitative_Final_Number__c
FROM main_Organization O
--JOIN GPScoutScoreCards SC ON O.SFID = SC.[GPScout_Scorecard__c]
JOIN Accounts A ON O.SFID = A.Id
LEFT JOIN GPScoutScoreCards SC ON A.[MostRecentScorecardSFID] = SC.[Id] AND SC.IsDeleted = 0 AND SC.Publish_Scorecard__c = 1


UPDATE main_Organization
SET FundRaisingStatus = 'Fundless Sponsor' WHERE FundRaisingStatus IN ('Pledge Fund - Active', 'Pledge Fund - Inactive')


-- set currency to USD for Organizations whose CurrencyId is null
DECLARE @CurrencyId UNIQUEIDENTIFIER
SET		@CurrencyId = (SELECT Id FROM [main_Currency] WHERE Code = 'USD')
UPDATE	[main_Organization]
SET		CurrencyId = @CurrencyId
WHERE	CurrencyId IS NULL


-- set list of countries assigned to each Organization through their list of firm addresses
--UPDATE O
--SET CountriesPipeDelimited = STUFF((
--	SELECT '|' + Country FROM (
--		SELECT DISTINCT Country FROM (
--			SELECT DISTINCT OA.Country FROM alexabe_01_stage.dbo.OtherAddress OA WHERE OA.OrganizationId = O.Id
--			UNION ALL
--			SELECT O.Address1Country
--		) B
--	) A FOR XML PATH(''))
--,1,1,'')
--FROM alexabe_01_stage.dbo.Organization O 
--WHERE O.DateImportedUtc = @DateImportedUtc


-- set list of countries assigned to each Organization from list of countries of most recent fund of the Organization
/*
UPDATE O
SET O.CountriesPipeDelimited = dbo.udf_ToPipeDelimitedValue(A.MostRecentFundCountries, ';')
FROM Accounts A 
JOIN alexabe_01_stage.dbo.Organization O ON A.Id = O.SFID AND A.Include=1
*/

-- delete entity multiple values for Organizations and Funds that were imported. They will be (re)created by post-import c# code
DELETE EV
FROM main_Organization O
JOIN main_EntityMultipleValues EV ON O.SFID IS NOT NULL AND O.DateImportedUtc = @DateImportedUtc AND O.Id = EV.EntityId AND EV.Field = 1

DELETE EV
FROM main_Fund F
JOIN main_EntityMultipleValues EV ON F.SFID IS NOT NULL AND F.DateImportedUtc = @DateImportedUtc AND F.Id = EV.EntityId AND EV.Field = 2

-- drop orphan Entities belonging to Funds
SELECT E.Id INTO #EntitiesToDelete
FROM main_Entity E
WHERE E.EntityType = 2 AND E.Id NOT IN (SELECT F.Id FROM main_Fund F)

DELETE main_EntityMultipleValues WHERE EntityId IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)
DELETE main_Entity WHERE Id IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)

DROP TABLE #EntitiesToDelete

-- backend code will re-populate
DELETE main_SearchOptions

COMMIT TRAN

END TRY
BEGIN CATCH
	ROLLBACK TRAN;
	-- THROW
    -- if THROW statement is not available:    



    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH


BEGIN TRY
	RETURN
	EXEC alexabe_01_dev_new.dbo.[usp_Util_ReIndexDatabase_UpdateStats]
END TRY
BEGIN CATCH
	-- THROW
    -- if THROW statement is not available:    

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH


GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXEC [dbo].[SF_DeleteSFData] 1
ALTER PROCEDURE [dbo].[SF_DeleteSFData]
	@FullDelete bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM main_SearchOptions
	DELETE FROM main_EntityMultipleValues

	DELETE FROM main_Benchmarks
	DELETE FROM main_Evaluation
	DELETE FROM main_Grade
	DELETE FROM main_OrganizationMember
	DELETE FROM main_OtherAddress
	DELETE FROM main_TrackRecord
	DELETE FROM main_FurtherFundMetrics
	DELETE FROM main_Fund
	DELETE FROM main_Entity WHERE Id IN (SELECT Id FROM main_Fund) AND EntityType = 2

	IF @FullDelete = 1 BEGIN
		DELETE FROM main_ClientRequest
		DELETE FROM main_OrganizationRequest
		DELETE FROM main_OrganizationViewing

		DELETE FROM main_PortfolioCompany
		DELETE FROM main_GroupOrganization 
		DELETE FROM main_OrganizationNote 
		DELETE FROM main_TrackUsersProfileView
		DELETE FROM main_Organization
		DELETE FROM main_Entity WHERE Id IN (SELECT Id FROM main_Organization) AND EntityType = 1
	END
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/01/2017 - PersonnelSeniorProfessionals, PersonnelMidLevelProfessionals ,PersonnelJuniorLevelProfessionals, PersonnelOperatingPartnerProfessionals,
--					   PersonnelTotalDealProfessionals, FundLaunchDate fields added
--		- 10/09/2017 - Status = 'Assumed Closed / Unknown' will import as null
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFunds]
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Funds WHERE SBIC__c NOT IN ('No','Yes') AND SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Funds.SBIC__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE F SET NewFundId = NEWID()
	FROM dbo.Funds F
	WHERE NOT EXISTS (SELECT NULL FROM main_Fund Target WHERE Target.SFID = F.Id)

	INSERT [main_Entity] (Id, EntityType)
	SELECT Source.NewFundId, 2
	FROM dbo.Funds Source 
	WHERE Source.NewFundId IS NOT NULL AND Source.NewFundId NOT IN (SELECT Id FROM [main_Entity] WHERE EntityType=2)
	

	MERGE [main_Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			f.[Name]
			--,f.[AIM__Account__c]
			,o.[Id] AS [OrganizationId]
			--,f.[Currency__c]
			,c.[Id] AS [CurrencyId]
			,NULLIF(f.[Fundraising_Status__c], 'Assumed Closed / Unknown') [Fundraising_Status__c]
			,f.[Fund_Total_Cash_on_Cash__c]
			,f.[Fund_Total_IRR__c]
			,NULLIF(TRY_CONVERT(int, f.[Total_Deal_Professionals__c]), 0) PersonnelTotalDealProfessionals
			,f.[FundTargetSize__c]
			,f.[FundNumber__c]
			--,(CASE 
			--	WHEN NULLIF(f.[First_Drawn_Capital__c], '') IS NOT NULL THEN YEAR(f.[First_Drawn_Capital__c])
			--	ELSE YEAR(f.[FundLaunchDate__c])
			--	END) AS [VintageYear]
			,f.FundLaunchDate__c FundLaunchDate
			,f.[Vintage_Year__c] AS [VintageYear]
			,f.[Eff_Size__c]
			,(CASE WHEN f.[Deal_Table_URL__c] NOT LIKE 'http://%' AND f.[Deal_Table_URL__c] NOT LIKE 'https://%' THEN 'https://' + f.[Deal_Table_URL__c] ELSE f.[Deal_Table_URL__c] END) [Deal_Table_URL__c]
			,f.[Effective_Date__c]
			,f.Personnel_Senior_Professional__c PersonnelSeniorProfessionals
			,f.Personnel_Mid_Level__c PersonnelMidLevelProfessionals
			,f.Personnel_Junior_Staff__c PersonnelJuniorLevelProfessionals
			,f.Personnel_Operating_Partner_Professional__c PersonnelOperatingPartnerProfessionals
			,f.[NewFundId]
			,[Include] = CASE WHEN IsDeleted = 0 AND Publish__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			,f.[DateImportedUtc]
			,f.[Id]
	  FROM	[dbo].[Funds] f
			LEFT JOIN [main_Organization] o ON f.[AIM__Account__c] = o.[SFID]
			LEFT JOIN [main_Currency] c ON f.[Currency__c] = c.[Name]
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
				Target.[Name] = Source.[Name],
				Target.[OrganizationId] = Source.[OrganizationId],
				Target.[CurrencyId] = Source.[CurrencyId],
				Target.[Status] = Source.[Fundraising_Status__c],
				Target.[Moic] = Source.[Fund_Total_Cash_on_Cash__c],
				Target.[GrossIrr] = Source.[Fund_Total_IRR__c],
				-- join with organization and set organization.NumberOfInvestmentProfessionals = Source.[Total_Deal_Professionals__c],
				Target.[TargetSize] = Source.[FundTargetSize__c],
				Target.[SortOrder] = Source.[FundNumber__c],
				Target.[VintageYear] = Source.[VintageYear],
				Target.[FundSize] = Source.[Eff_Size__c],
				Target.[AnalysisUrl] = Source.[Deal_Table_URL__c],
				Target.[AsOf] = Source.[Effective_Date__c],
				Target.[DateImportedUtc] = Source.[DateImportedUtc],
				Target.PersonnelSeniorProfessionals = Source.PersonnelSeniorProfessionals,
				Target.PersonnelMidLevelProfessionals = Source.PersonnelMidLevelProfessionals,
				Target.PersonnelJuniorLevelProfessionals = Source.PersonnelJuniorLevelProfessionals,
				Target.PersonnelOperatingPartnerProfessionals = Source.PersonnelOperatingPartnerProfessionals,
				Target.PersonnelTotalDealProfessionals = Source.PersonnelTotalDealProfessionals,
				Target.FundLaunchDate = Source.FundLaunchDate

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
				[Id]
				,[Name]
				,[OrganizationId]
				,[CurrencyId]
				,[Status]
				,[Moic]
				,[GrossIrr]
				,[TargetSize]
				,[SortOrder]
				,[VintageYear]
				,[FundSize]
				,[AnalysisUrl]
				,[AsOf]
				,[DateImportedUtc]
				,[SFID]
				,PersonnelSeniorProfessionals
				,PersonnelMidLevelProfessionals
				,PersonnelJuniorLevelProfessionals
				,PersonnelOperatingPartnerProfessionals
				,PersonnelTotalDealProfessionals
				,FundLaunchDate
	) 
		VALUES(	
				Source.[NewFundId]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[CurrencyId]
				,Source.[Fundraising_Status__c]
				,Source.[Fund_Total_Cash_on_Cash__c]
				,Source.[Fund_Total_IRR__c]
				,Source.[FundTargetSize__c]
				,Source.[FundNumber__c]
				,Source.[VintageYear]
				,Source.[Eff_Size__c]
				,Source.[Deal_Table_URL__c]
				,Source.[Effective_Date__c]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.PersonnelSeniorProfessionals
				,Source.PersonnelMidLevelProfessionals
				,Source.PersonnelJuniorLevelProfessionals
				,Source.PersonnelOperatingPartnerProfessionals
				,Source.PersonnelTotalDealProfessionals
				,Source.FundLaunchDate
			)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;	

	COMMIT TRAN

END
