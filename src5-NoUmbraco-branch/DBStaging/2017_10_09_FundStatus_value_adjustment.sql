﻿USE [GPScoutStagingStage_New]
GO
/****** Object:  StoredProcedure [dbo].[SF_ImportAccounts]    Script Date: 10/10/2017 12:47:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		  05/04/2017 - Most_Recent_Fund_Regional_Specialist__c added to the import logic
--					 - Access_Constrained__c replaced with Access_Constrained_Firm__c
--					 - Populate KeyTakeAway with Overview__c (KeyTakeAway not used anymore)
--					 - Industry_Focus__c removed, not used anymore
--					 - Most_Recent_Fund_Sub_Region__c removed, not used anymore
--		- 06/06/2017 - (CASE Most_Recent_Fund_Fundraising_Status__c WHEN 'Pledge Fund - Active' THEN 'Fundless Sponsor'
--					 - InactiveFirm logic added
--		- 06/28/2017 - adjustment (CASE WHEN Most_Recent_Fund_Fundraising_Status__c IN ('Pledge Fund - Active', 'Pledge Fund - Inactive') THEN 'Fundless Sponsor' ELSE Most_Recent_Fund_Fundraising_Status__c END) Most_Recent_Fund_Fundraising_Status__c, -- Most_Recent_Fund_Fundraising_Status__c moved to SP SF_PostImport
--		- 10/04/2017 - GPScoutLevelCompleted and LevelOfDisclosure fields included
--		- 10/09/2017 - FundRaisingStatus = 'Assumed Closed / Unknown' will import as null
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportAccounts]
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_Regional_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_Regional_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_Regional_Specialist__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_SBIC__c NOT IN ('No','Yes') AND Most_Recent_Fund_SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_SBIC__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_is_Sector_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_is_Sector_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_is_Sector_Specialist__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE A
	SET A.Include = CASE WHEN IsDeleted = 0 AND Publish_Level__c IS NOT NULL THEN 1 ELSE 0 END
	FROM dbo.Accounts A

	UPDATE A SET NewOrganizationId = NEWID()
	FROM dbo.Accounts A
	WHERE NOT EXISTS (SELECT NULL FROM main_Organization Target WHERE Target.SFID = A.Id)
		AND A.Include=1

	INSERT main_Entity (Id, EntityType)
	SELECT Source.NewOrganizationId, 1
	FROM dbo.Accounts Source 
	WHERE Source.NewOrganizationId IS NOT NULL AND Source.NewOrganizationId NOT IN (SELECT Id FROM main_Entity WHERE EntityType=1) --AND Include=1
	

	MERGE main_Organization WITH (HOLDLOCK) AS Target
	USING (SELECT 
		ISNULL(Access_Constrained_Firm__c, 0) AS Access_Constrained_Firm__c,
		Alias__c,
		AUM_Calc__c,
		BillingCountry,
		Co_Investment_Opportunities_for_Fund_LP__c,
		Co_Investment_Opportunities_for_Non_LPs__c,
		DateImportedUtc,
		ISNULL(Emerging_Manager__c, 0) AS Emerging_Manager__c,
		Evaluation_Text__c,
		Expected_Next_Fundraise__c,
		Firm_History__c,
		--Grade_Qualitative_Text__c,
		--Grade_Quantitative_Text__c,
		--Grade_Scatterchart_Text__c,
		A.Id,
		--Industry_Focus__c,
		Investment_Thesis__c,
		--Key_Takeaway__c,
		GPScout_Last_Updated__c,
		Most_Recent_Fund_Eff_Size__c,
		NULLIF(Most_Recent_Fund_Fundraising_Status__c, 'Assumed Closed / Unknown') Most_Recent_Fund_Fundraising_Status__c,
		Most_Recent_Fund_Industry_Focus__c,
		(CASE Most_Recent_Fund_is_Sector_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_is_Sector_Specialist__c,
		Most_Recent_Fund_Market_Stage__c,
		Most_Recent_Fund_Primary_Strategy__c,
		Most_Recent_Fund_Region__c,
		(CASE Most_Recent_Fund_Regional_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_Regional_Specialist__c,
		(CASE Most_Recent_Fund_SBIC__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_SBIC__c,
		Most_Recent_Fund_Secondary_Strategy__c,
		--Most_Recent_Fund_Sub_Region__c,
		A.Name,
		NewOrganizationId,
		Overview__c,
		-- if Account has been deleted or unpublished in SF then we unpublish it in GPScout
		(CASE Include WHEN 1 THEN Publish_Level__c ELSE '' END) Publish_Level__c,
		--ISNULL(Radar_List__c, 0) AS Radar_List__c,
		(CAST(CASE WHEN FirmGrade__c='A' OR FirmGrade__c='B' THEN 1 ELSE 0 END AS BIT)) AS FirmGrade__c,
		(CASE FirmGrade__c WHEN 'Inactive' THEN 1 ELSE 0 END) InactiveFirm,
		Region__c,
		Team_Overview__c,
		Total_Closed_Funds__c,
		Track_Record_Text__c,
		Website,
		YearGPFounded__c,
		[Include], -- initialized in UPDATE above
		C.Id CurrencyId,
		GPScout_Phase_Assigned__c,
		GPScout_Phase_Completed__c,
		GPScoutLevelCompleted__c GPScoutLevelCompleted,
		Level_of_Disclosure__c LevelOfDisclosure
		--A.RecordTypeName,
		--A.ParentId
		FROM dbo.Accounts A
		LEFT JOIN main_Currency C ON A.Most_Recent_Fund_Currency__c = C.Name
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED /*AND Source.[Include] = 1*/ THEN UPDATE SET 
		-- Grade.Scatterchart_Text__c = Source.Scatterchart_Text__c, -- Handled in SF_PostImport
		Target.RegionalSpecialist = Source.Most_Recent_Fund_Regional_Specialist__c,
		Target.AccessConstrained = Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Target.Address1Country = Source.BillingCountry,
		Target.AliasesPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Target.Aum = Source.AUM_Calc__c,
		Target.CoInvestWithExistingLPs = Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Target.CoInvestWithOtherLPs = Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Target.DateImportedUtc = Source.DateImportedUtc,
		Target.EmergingManager = Source.Emerging_Manager__c,
		Target.PublishLevelAsImported = Source.Publish_Level__c,
		Target.EvaluationText = Source.Evaluation_Text__c,
		Target.ExpectedNextFundRaise = Source.Expected_Next_Fundraise__c,
		--Target.FocusList = Source.Radar_List__c,
		Target.FocusList = Source.FirmGrade__c,
		Target.History = Source.Firm_History__c,
		Target.InvestmentRegionPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Target.InvestmentThesis = Source.Investment_Thesis__c,
		Target.KeyTakeAway = Source.Overview__c, --Source.Key_Takeaway__c,
		Target.LastFundSize = Source.Most_Recent_Fund_Eff_Size__c,
		Target.LastUpdated = Source.GPScout_Last_Updated__c,
		Target.MarketStage = Source.Most_Recent_Fund_Market_Stage__c,
		Target.Name = Source.Name,
		Target.NumberOfFunds = Source.Total_Closed_Funds__c,
		Target.OrganizationOverview = Source.Overview__c,
		--Target.QualitativeGrade = Source.Grade_Qualitative_Text__c,	-- Handled in SF_PostImport
		--Target.QuantitativeGrade = Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Target.SBICFund = Source.Most_Recent_Fund_SBIC__c,
		Target.SectorFocusPipeDelimited = dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Target.SectorSpecialist = Source.Most_Recent_Fund_is_Sector_Specialist__c,
		Target.StrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--Target.SubRegionsPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		Target.SubStrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Target.FundRaisingStatus = Source.Most_Recent_Fund_Fundraising_Status__c,
		Target.TeamOverview = Source.Team_Overview__c,
		Target.TrackRecordText = Source.Track_Record_Text__c,
		Target.WebsiteUrl = Source.Website,
		Target.YearFounded = Source.YearGPFounded__c,
		Target.CurrencyId = Source.CurrencyId,
		Target.GPScoutPhaseAssigned = Source.GPScout_Phase_Assigned__c,
		Target.GPScoutPhaseCompleted = Source.GPScout_Phase_Completed__c,
		--Target.RecordTypeName = Source.RecordTypeName,
		--Target.ParentSFID = Source.ParentId,
		Target.InactiveFirm = Source.InactiveFirm,
		Target.GPScoutLevelCompleted = Source.GPScoutLevelCompleted,
		Target.LevelOfDisclosure = Source.LevelOfDisclosure

	WHEN NOT MATCHED BY TARGET AND (Source.[Include] = 1 /*OR Source.RecordTypeName = 'Fund Manager Parent'*/) THEN INSERT(
		SFID,
		RegionalSpecialist,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		AccessConstrained,
		Address1Country,
		AliasesPipeDelimited,
		Aum,
		CoInvestWithExistingLPs,
		CoInvestWithOtherLPs,
		DateImportedUtc,
		EmergingManager,
		PublishLevelAsImported,
		EvaluationText,
		ExpectedNextFundRaise,
		FocusList,
		History,
		Id,
		InvestmentRegionPipeDelimited,
		InvestmentThesis,
		KeyTakeAway,
		LastFundSize,
		LastUpdated,
		MarketStage,
		Name,
		NumberOfFunds,
		OrganizationOverview,
		--QualitativeGrade,
		--QuantitativeGrade,
		SBICFund,
		SectorFocusPipeDelimited,
		SectorSpecialist,
		StrategyPipeDelimited,
		--SubRegionsPipeDelimited,
		SubStrategyPipeDelimited,
		FundRaisingStatus,
		TeamOverview,
		TrackRecordText,
		WebsiteUrl,
		YearFounded,
		CurrencyId,
		GPScoutPhaseAssigned,
		GPScoutPhaseCompleted,
		--RecordTypeName,
		--ParentSFID
		InactiveFirm,
		GPScoutLevelCompleted,
		LevelOfDisclosure
	) 
	VALUES(
		Source.Id,
		Source.Most_Recent_Fund_Regional_Specialist__c,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Source.BillingCountry,
		dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Source.AUM_Calc__c,
		Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Source.DateImportedUtc,
		Source.Emerging_Manager__c,
		Source.Publish_Level__c,
		Source.Evaluation_Text__c,
		Source.Expected_Next_Fundraise__c,
		--Source.Radar_List__c,
		Source.FirmGrade__c,
		Source.Firm_History__c,
		Source.NewOrganizationId,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Source.Investment_Thesis__c,
		Source.Overview__c, -- Source.Key_Takeaway__c,
		Source.Most_Recent_Fund_Eff_Size__c,
		Source.GPScout_Last_Updated__c,
		Source.Most_Recent_Fund_Market_Stage__c,
		Source.Name,
		Source.Total_Closed_Funds__c,
		Source.Overview__c,
		--Source.Grade_Qualitative_Text__c,		-- Handled in SF_PostImport
		--Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Source.Most_Recent_Fund_SBIC__c,
		dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Source.Most_Recent_Fund_is_Sector_Specialist__c,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Source.Most_Recent_Fund_Fundraising_Status__c,
		Source.Team_Overview__c,
		Source.Track_Record_Text__c,
		Source.Website,
		Source.YearGPFounded__c,
		Source.CurrencyId,
		Source.GPScout_Phase_Assigned__c,
		Source.GPScout_Phase_Completed__c,
		--Source.RecordTypeName,
		--Source.ParentId
		Source.InactiveFirm,
		Source.GPScoutLevelCompleted,
		Source.LevelOfDisclosure
	)
--	WHEN MATCHED AND Source.[Include] = 0 THEN -- Organizations are not deleted for now. They can have user created detail info attached
		--DELETE
		;

	UPDATE O
		SET PublishLevelID = CASE
			WHEN A.Include = 0 THEN 0
			WHEN A.Publish_Level__c LIKE '0 -%' THEN 1
			WHEN A.Publish_Level__c LIKE '1 -%' THEN 2
			WHEN A.Publish_Level__c LIKE '2 -%' THEN 3
			WHEN A.Publish_Level__c LIKE '3 -%' THEN 4
			WHEN A.Publish_Level__c LIKE '2.5 -%' THEN 5
			ELSE 0
		END
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	UPDATE O
		SET 
			EvaluationLevel = CASE PublishLevelID
				WHEN 2 THEN 'Initial Assessment'
				WHEN 3 THEN 'Preliminary Evaluation'
				WHEN 4 THEN 'Evaluation'
				WHEN 5 THEN 'Evaluation'
				ELSE ''
			END,
			DiligenceLevel = CASE PublishLevelID
				WHEN 2 THEN 33
				WHEN 3 THEN 66
				WHEN 4 THEN 100
				WHEN 5 THEN 66
				ELSE ''
			END,
			PublishSearch = CASE WHEN PublishLevelID>=1 THEN 1 ELSE 0 END,
			PublishOverviewAndTeam = CASE WHEN PublishLevelID>=2 THEN 1 ELSE 0 END,
			PublishTrackRecord = CASE WHEN PublishLevelID>=3 THEN 1 ELSE 0 END,
			PublishProfile = CASE WHEN PublishLevelID>=4 THEN 1 ELSE 0 END,
			FocusRadar = CASE WHEN O.FocusList=1 THEN 'Focus List' ELSE '' END--,
			--ParentId = NULL
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	-- set parent-child relationship
	--UPDATE ChildOrganization
	--SET ChildOrganization.ParentId = ParentOrganization.Id
	--FROM Accounts A
	--JOIN alexabe_01_stage.dbo.Organization ChildOrganization ON A.Id = ChildOrganization.SFID
	--JOIN alexabe_01_stage.dbo.Organization ParentOrganization ON A.ParentId = ParentOrganization.SFID

	-- remove child items of unpublished Organizations
	SELECT Id INTO #UnpublishedOrgs FROM main_Organization WHERE PublishLevelID=0

	DELETE E FROM main_Evaluation E JOIN #UnpublishedOrgs O ON E.OrganizationId=O.Id 
	DELETE G FROM main_Grade G JOIN #UnpublishedOrgs O ON G.OrganizationId=O.Id 
	DELETE M FROM main_OrganizationMember M JOIN #UnpublishedOrgs O ON M.OrganizationId=O.Id 
	--DELETE M FROM main_OrganizationMember M WHERE NOT EXISTS (SELECT NULL FROM main_Organization O WHERE O.Id=M.OrganizationId AND O.PublishLevelID>0)
	DELETE A FROM main_OtherAddress A JOIN #UnpublishedOrgs O ON A.OrganizationId=O.Id 
	DELETE T FROM main_TrackRecord T JOIN #UnpublishedOrgs O ON T.OrganizationId=O.Id
	DELETE F FROM main_Fund F JOIN #UnpublishedOrgs O ON F.OrganizationId=O.Id

	DROP TABLE #UnpublishedOrgs

	DELETE main_ResearchPriorityOrganization
	INSERT main_ResearchPriorityOrganization (
			[Id]
           ,[Name]
           ,[MarketStage]
           ,[StrategyPipeDelimited]
           ,[InvestmentRegionPipeDelimited])
     SELECT
           Id
           ,Name
           ,MarketStage
           ,StrategyPipeDelimited
           ,InvestmentRegionPipeDelimited
	FROM main_Organization
	WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '') AND PublishLevelAsImported!=''

	COMMIT TRAN
END


SET ANSI_NULLS ON


USE [GPScoutStagingStage_New]
GO
/****** Object:  StoredProcedure [dbo].[SF_ImportFunds]    Script Date: 10/10/2017 12:47:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/01/2017 - PersonnelSeniorProfessionals, PersonnelMidLevelProfessionals ,PersonnelJuniorLevelProfessionals, PersonnelOperatingPartnerProfessionals,
--					   PersonnelTotalDealProfessionals, FundLaunchDate fields added
--		- 10/09/2017 - Status = 'Assumed Closed / Unknown' will import as null
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFunds]
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Funds WHERE SBIC__c NOT IN ('No','Yes') AND SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Funds.SBIC__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE F SET NewFundId = NEWID()
	FROM dbo.Funds F
	WHERE NOT EXISTS (SELECT NULL FROM main_Fund Target WHERE Target.SFID = F.Id)

	INSERT [main_Entity] (Id, EntityType)
	SELECT Source.NewFundId, 2
	FROM dbo.Funds Source 
	WHERE Source.NewFundId IS NOT NULL AND Source.NewFundId NOT IN (SELECT Id FROM [main_Entity] WHERE EntityType=2)
	

	MERGE [main_Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			f.[Name]
			--,f.[AIM__Account__c]
			,o.[Id] AS [OrganizationId]
			--,f.[Currency__c]
			,c.[Id] AS [CurrencyId]
			,NULLIF(f.[Fundraising_Status__c], 'Assumed Closed / Unknown') [Fundraising_Status__c]
			,f.[Fund_Total_Cash_on_Cash__c]
			,f.[Fund_Total_IRR__c]
			,NULLIF(TRY_CONVERT(int, f.[Total_Deal_Professionals__c]), 0) PersonnelTotalDealProfessionals
			,f.[FundTargetSize__c]
			,f.[FundNumber__c]
			--,(CASE 
			--	WHEN NULLIF(f.[First_Drawn_Capital__c], '') IS NOT NULL THEN YEAR(f.[First_Drawn_Capital__c])
			--	ELSE YEAR(f.[FundLaunchDate__c])
			--	END) AS [VintageYear]
			,f.FundLaunchDate__c FundLaunchDate
			,f.[Vintage_Year__c] AS [VintageYear]
			,f.[Eff_Size__c]
			,f.[Deal_Table_URL__c]
			,f.[Effective_Date__c]
			,f.Personnel_Senior_Professional__c PersonnelSeniorProfessionals
			,f.Personnel_Mid_Level__c PersonnelMidLevelProfessionals
			,f.Personnel_Junior_Staff__c PersonnelJuniorLevelProfessionals
			,f.Personnel_Operating_Partner_Professional__c PersonnelOperatingPartnerProfessionals
			,f.[NewFundId]
			,[Include] = CASE WHEN IsDeleted = 0 AND Publish__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			,f.[DateImportedUtc]
			,f.[Id]
	  FROM	[dbo].[Funds] f
			LEFT JOIN [main_Organization] o ON f.[AIM__Account__c] = o.[SFID]
			LEFT JOIN [main_Currency] c ON f.[Currency__c] = c.[Name]
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
				Target.[Name] = Source.[Name],
				Target.[OrganizationId] = Source.[OrganizationId],
				Target.[CurrencyId] = Source.[CurrencyId],
				Target.[Status] = Source.[Fundraising_Status__c],
				Target.[Moic] = Source.[Fund_Total_Cash_on_Cash__c],
				Target.[GrossIrr] = Source.[Fund_Total_IRR__c],
				-- join with organization and set organization.NumberOfInvestmentProfessionals = Source.[Total_Deal_Professionals__c],
				Target.[TargetSize] = Source.[FundTargetSize__c],
				Target.[SortOrder] = Source.[FundNumber__c],
				Target.[VintageYear] = Source.[VintageYear],
				Target.[FundSize] = Source.[Eff_Size__c],
				Target.[AnalysisUrl] = Source.[Deal_Table_URL__c],
				Target.[AsOf] = Source.[Effective_Date__c],
				Target.[DateImportedUtc] = Source.[DateImportedUtc],
				Target.PersonnelSeniorProfessionals = Source.PersonnelSeniorProfessionals,
				Target.PersonnelMidLevelProfessionals = Source.PersonnelMidLevelProfessionals,
				Target.PersonnelJuniorLevelProfessionals = Source.PersonnelJuniorLevelProfessionals,
				Target.PersonnelOperatingPartnerProfessionals = Source.PersonnelOperatingPartnerProfessionals,
				Target.PersonnelTotalDealProfessionals = Source.PersonnelTotalDealProfessionals,
				Target.FundLaunchDate = Source.FundLaunchDate

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
				[Id]
				,[Name]
				,[OrganizationId]
				,[CurrencyId]
				,[Status]
				,[Moic]
				,[GrossIrr]
				,[TargetSize]
				,[SortOrder]
				,[VintageYear]
				,[FundSize]
				,[AnalysisUrl]
				,[AsOf]
				,[DateImportedUtc]
				,[SFID]
				,PersonnelSeniorProfessionals
				,PersonnelMidLevelProfessionals
				,PersonnelJuniorLevelProfessionals
				,PersonnelOperatingPartnerProfessionals
				,PersonnelTotalDealProfessionals
				,FundLaunchDate
	) 
		VALUES(	
				Source.[NewFundId]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[CurrencyId]
				,Source.[Fundraising_Status__c]
				,Source.[Fund_Total_Cash_on_Cash__c]
				,Source.[Fund_Total_IRR__c]
				,Source.[FundTargetSize__c]
				,Source.[FundNumber__c]
				,Source.[VintageYear]
				,Source.[Eff_Size__c]
				,Source.[Deal_Table_URL__c]
				,Source.[Effective_Date__c]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.PersonnelSeniorProfessionals
				,Source.PersonnelMidLevelProfessionals
				,Source.PersonnelJuniorLevelProfessionals
				,Source.PersonnelOperatingPartnerProfessionals
				,Source.PersonnelTotalDealProfessionals
				,Source.FundLaunchDate
			)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;	

	COMMIT TRAN

END
