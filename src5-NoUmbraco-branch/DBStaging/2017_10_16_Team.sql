﻿UPDATE SF_EntityImportConfig
SET SelectQuery='
SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c
,Account.IsDeleted, PublishDetail__c
FROM contact 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) OR (Account.LastModifiedDate >= @LastModifiedDate AND Account.LastModifiedDate < @DateImported)
',
FirstSelectQuery='
SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Publish_Contact__c,Account.Publish_Level__c
,Account.IsDeleted, PublishDetail__c
FROM contact 
WHERE IsDeleted = false AND Publish_Contact__c=true AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported
'
WHERE ID=6 AND EntityCode = 'SFContact'


DROP TABLE [dbo].[Contacts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Contacts](
	[Id] [varchar](18) NOT NULL,
	[AccountId] [varchar](18) NULL,
	[Phone] [nvarchar](40) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Year_Started_With_Current_Firm__c] [decimal](18, 0) NULL,
	[Bio__c] [nvarchar](max) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[MailingCountry] [nvarchar](80) NULL,
	[IsDeleted] [bit] NULL,
	[Publish_Contact__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](80) NULL,
	[Display_Order__c] [decimal](9, 0) NULL,
	[PublishDetail__c] [nvarchar](90) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


--==========================================================================
ALTER TABLE OrganizationMember ADD [PublishDetail] int NULL

--==========================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/16/2017 - [PublishDetail__c] added
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportContacts]
AS
BEGIN

	SET NOCOUNT ON;

	MERGE [main_OrganizationMember] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  C.[Phone]
		  ,CAST(C.[Email] AS NVARCHAR(50)) AS [Email]
		  ,C.[Title]
		  ,C.[Year_Started_With_Current_Firm__c]
		  ,C.[Bio__c]
		  ,CAST(C.MailingStreet AS NVARCHAR(128)) AS MailingStreet
		  ,C.MailingCity
		  ,CAST([MailingState] AS NVARCHAR(50)) AS MailingState
		  ,C.MailingPostalCode
		  ,CAST([MailingCountry] AS NVARCHAR(50)) AS MailingCountry
		  ,C.[DateImportedUtc]
		  ,C.[Id]
		  ,O.Id OrganizationId
		  ,[Include] = CASE WHEN IsDeleted = 0 AND Publish_Contact__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
		  ,C.FirstName
		  ,C.LastName
		  ,C.Display_Order__c
		  ,CASE C.[PublishDetail__c]
				WHEN 'Senior - Key' THEN 10
				WHEN 'Senior - Lead' THEN 20
				WHEN 'Mid Level' THEN 30
				WHEN 'Operating' THEN 40
				ELSE NULL
			END [PublishDetail]
	  FROM [dbo].[Contacts] C
	  LEFT JOIN [main_Organization] O ON C.AccountId = O.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Phone] = Source.[Phone],
		Target.[Email] = Source.[Email],
		Target.[JobTitle] = Source.[Title],
		Target.[YearsAtOrganization] = Source.[Year_Started_With_Current_Firm__c],
		Target.[Description] = Source.[Bio__c],
		Target.[Address1] = Source.MailingStreet,
		Target.[City] = Source.MailingCity,
		Target.[StateOrProvince] = Source.MailingState,
		Target.[PostalCode] = Source.MailingPostalCode,
		Target.[Country] = Source.MailingCountry,
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.OrganizationId = Source.OrganizationId,
		Target.FirstName = Source.FirstName,
		Target.LastName = Source.LastName,
		Target.DisplayOrder = Source.Display_Order__c,
		Target.PublishDetail = Source.PublishDetail

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id
			,[Phone]
			,[Email]
			,[JobTitle]
			,[YearsAtOrganization]
			,[Description]
			,[Address1]
			,[City]
			,[StateOrProvince]
			,[PostalCode]
			,[Country]
			,[DateImportedUtc]
			,[SFID]
			,OrganizationId
			,FirstName
			,LastName
			,DisplayOrder
			,PublishDetail
	) 
		VALUES(	
				NEWID()
				,Source.[Phone]
				,Source.[Email]
				,Source.[Title]
				,Source.[Year_Started_With_Current_Firm__c]
				,Source.[Bio__c]
				,Source.MailingStreet
				,Source.MailingCity
				,Source.MailingState
				,Source.MailingPostalCode
				,Source.MailingCountry
				,Source.[DateImportedUtc]
				,Source.Id
				,Source.OrganizationId
				,Source.FirstName
				,Source.LastName
				,Source.Display_Order__c
				,PublishDetail
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END


