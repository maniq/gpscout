﻿-- Removed from SF queries for now.
-- Removal of remaining sql and app references still TO DO:.
-- Helper DB:
UPDATE SF_EntityImportConfig SET
SelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported
',
FirstSelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL
'
WHERE ID=3 AND EntityCode = 'SFAccount'


UPDATE SF_EntityImportConfig SET
SelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, 
Emerging_Manager__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id, 
Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, 
Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, 
Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, 
Track_Record_Text__c, Website, YearGPFounded__c, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)
'
WHERE ID=13 AND EntityCode = 'SFAdditionalAccounts'