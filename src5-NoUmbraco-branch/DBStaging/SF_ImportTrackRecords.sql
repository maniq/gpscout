﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/10/2017 - [File_Name__c] prefixed with url protocol if required
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportTrackRecords]
AS
	SET NOCOUNT ON;

	MERGE dbo.[main_TrackRecord] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			[Description__c],
			(CASE WHEN [File_Name__c] NOT LIKE 'http://%' AND [File_Name__c] NOT LIKE 'https://%' THEN 'https://' + [File_Name__c] ELSE [File_Name__c] END) [File_Name__c],
			[Display_Order__c],
			tr.[Name],
			[Account__c],
			tr.[DateImportedUtc],
			tr.[Id],
			NULLIF(TRY_CONVERT(int, tr.TopGraphDisplayOrder__c), 0) TopGraphDisplayOrder__c,
			o.[Id] AS [OrganizationId],
			[Include] = CASE WHEN IsDeleted = 0 AND Publish__c= 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
	  FROM [dbo].[TrackRecords] tr
	  LEFT JOIN dbo.[main_Organization] o ON tr.Account__c = o.SFID

	  ) AS Source 
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Description] = Source.[Description__c],
		Target.[FileUrl] = Source.[File_Name__c],
		Target.[Order] = Source.[Display_Order__c],
		Target.[Title] = Source.[Name],
		Target.[OrganizationId] = Source.[OrganizationId],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[TopGraphDisplayOrder] = Source.[TopGraphDisplayOrder__c]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
		Id,
		[Description],
		[FileUrl],
		[Order],
		[Title],
		[OrganizationId],
		[DateImportedUtc],
		SFID,
		[TopGraphDisplayOrder]
		) 
		VALUES(	NEWID()
				,Source.[Description__c]
				,Source.[File_Name__c]
				,Source.[Display_Order__c]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.[TopGraphDisplayOrder__c]
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;
