﻿UPDATE SF_EntityImportConfig
SET FirstSelectQuery = '	
SELECT Display_Order__c,FirstName,LastName,Id, AccountId, Phone,Email,Title,Year_Started_With_Current_Firm__c,Bio__c,MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry,IsDeleted,Account.Publish_Level__c
,Account.IsDeleted, PublishDetail__c
FROM contact 
WHERE IsDeleted = false AND PublishDetail__c!=NULL AND PublishDetail__c!='''' AND  Display_Order__c>0 AND Account.Publish_Level__c!=NULL AND Account.IsDeleted=false AND LastModifiedDate < @DateImported'
Where EntityCode = 'SFContact' and ID=6