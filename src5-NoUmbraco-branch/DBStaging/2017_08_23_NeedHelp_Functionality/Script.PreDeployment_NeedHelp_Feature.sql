﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
ALTER TABLE dbo.OrganizationRequest
ALTER COLUMN UserId uniqueidentifier NULL

ALTER TABLE dbo.OrganizationRequest
DROP COLUMN [RequestTypeDisplayName]

ALTER TABLE dbo.OrganizationRequest
ADD [RequestTypeDisplayName] AS (case [Type] when (2) then 'Research' when (3) then 'Introduction' when (4) then 'Restricted GP Access' when (1) then 'Priority' when (5) then 'Profile Update Completed' when (6) then 'Need Help' when (0) then 'Queue' else '' end)

/****** Object:  Table [dbo].[RequestAdditionalInfo]    Script Date: 8/23/2017 5:06:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RequestAdditionalInfo](
	[Id] [uniqueidentifier] NOT NULL,
	[UserEmail] [nvarchar](320) NULL,
 CONSTRAINT [PK_RequestAdditionalInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RequestAdditionalInfo]  WITH CHECK ADD  CONSTRAINT [FK_RequestAdditionalInfo_OrganizationRequest] FOREIGN KEY([Id])
REFERENCES [dbo].[OrganizationRequest] ([Id])
GO

ALTER TABLE [dbo].[RequestAdditionalInfo] CHECK CONSTRAINT [FK_RequestAdditionalInfo_OrganizationRequest]
GO


