﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
UPDATE SF_EntityImportConfig
SET SelectQuery='
SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted, TopGraphDisplayOrder__c 
,PublishDetail__c
FROM Track_Record_Item__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (Account__r.LastModifiedDate >= @LastModifiedDate AND Account__r.LastModifiedDate < @DateImported)',
FirstSelectQuery='
SELECT Account__c, Description__c, Display_Order__c, File_Name__c, Id, Name,IsDeleted,Publish__c,Account__r.Publish_Level__c,Account__r.IsDeleted, TopGraphDisplayOrder__c
,PublishDetail__c
FROM Track_Record_Item__c 
WHERE IsDeleted = false AND Publish__c=true AND LastModifiedDate < @DateImported AND Account__r.Publish_Level__c!=NULL AND Account__r.IsDeleted=false
'
WHERE ID=11 AND EntityCode = 'SFTrackRecord'

DROP TABLE [dbo].[TrackRecords]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE [dbo].[TrackRecords]
GO

CREATE TABLE [dbo].[TrackRecords](
	[Id] [varchar](18) NOT NULL,
	[Account__c] [varchar](18) NULL,
	[Name] [nvarchar](80) NULL,
	[Description__c] [nvarchar](max) NULL,
	[File_Name__c] [nvarchar](255) NULL,
	[Display_Order__c] [decimal](3, 0) NULL,
	[IsDeleted] [bit] NULL,
	[Publish__c] [bit] NULL,
	[ParentAccount_IsDeleted] [bit] NULL,
	[ParentAccount_Publish_Level__c] [nvarchar](255) NULL,
	[TopGraphDisplayOrder__c] [nvarchar](30) NULL,
	[PublishDetail__c] [nvarchar](90) NULL,
	[DateImportedUtc] [datetime2](7) NULL,
	[Include] [bit] NULL,
 CONSTRAINT [PK_TrackRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE TrackRecord ADD [TopGraphDisplayOrder] int NULL
ALTER TABLE TrackRecord ADD [PublishDetail] int NULL


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/10/2017 - [File_Name__c] prefixed with url protocol if required
--		- 10/13/2017 - [PublishDetail__c] added
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportTrackRecords]
AS
	SET NOCOUNT ON;

	MERGE dbo.[main_TrackRecord] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			[Description__c],
			(CASE WHEN [File_Name__c] NOT LIKE 'http://%' AND [File_Name__c] NOT LIKE 'https://%' THEN 'https://' + [File_Name__c] ELSE [File_Name__c] END) [File_Name__c],
			[Display_Order__c],
			tr.[Name],
			[Account__c],
			tr.[DateImportedUtc],
			tr.[Id],
			CASE tr.[PublishDetail__c]
				WHEN 'Strategy - Exposure' THEN 1
				WHEN 'Strategy - Size' THEN 2
				WHEN 'Strategy - Pricing / Sourcing' THEN 3
				WHEN 'Strategy - Other' THEN 4
				WHEN 'Team - Attribution' THEN 10
				WHEN 'Team - Size' THEN 11
				WHEN 'Team - Other' THEN 12
				WHEN 'Track - Risk / Returns' THEN 30
				WHEN 'Track - Operating Metrics' THEN 31
				WHEN 'Track - Pace' THEN 32
				WHEN 'Track - Benchmark' THEN 33
				WHEN 'Track - Other' THEN 34
				ELSE NULL
			END [PublishDetail],
			NULLIF(TRY_CONVERT(int, tr.TopGraphDisplayOrder__c), 0) TopGraphDisplayOrder__c,
			o.[Id] AS [OrganizationId],
			[Include] = CASE WHEN IsDeleted = 0 AND Publish__c= 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
	  FROM [dbo].[TrackRecords] tr
	  LEFT JOIN dbo.[main_Organization] o ON tr.Account__c = o.SFID

	  ) AS Source 
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Description] = Source.[Description__c],
		Target.[FileUrl] = Source.[File_Name__c],
		Target.[Order] = Source.[Display_Order__c],
		Target.[Title] = Source.[Name],
		Target.[OrganizationId] = Source.[OrganizationId],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[TopGraphDisplayOrder] = Source.[TopGraphDisplayOrder__c],
		Target.[PublishDetail] = Source.[PublishDetail]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
		Id,
		[Description],
		[FileUrl],
		[Order],
		[Title],
		[OrganizationId],
		[DateImportedUtc],
		SFID,
		[TopGraphDisplayOrder],
		[PublishDetail]
	) 
	VALUES(	NEWID()
			,Source.[Description__c]
			,Source.[File_Name__c]
			,Source.[Display_Order__c]
			,Source.[Name]
			,Source.[OrganizationId]
			,Source.[DateImportedUtc]
			,Source.[Id]
			,Source.[TopGraphDisplayOrder__c]
			,Source.[PublishDetail]
	)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;