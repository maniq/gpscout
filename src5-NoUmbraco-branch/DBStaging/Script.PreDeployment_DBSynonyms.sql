﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/****** Object:  Synonym [dbo].[main_usp_Util_ReIndexDatabase_UpdateStats]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_usp_Util_ReIndexDatabase_UpdateStats]
GO

/****** Object:  Synonym [dbo].[main_TrackUsersProfileView]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_TrackUsersProfileView]
GO

/****** Object:  Synonym [dbo].[main_TrackUsers]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_TrackUsers]
GO

/****** Object:  Synonym [dbo].[main_TrackRecord]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_TrackRecord]
GO

/****** Object:  Synonym [dbo].[main_SearchOptions]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_SearchOptions]
GO

/****** Object:  Synonym [dbo].[main_ResearchPriorityOrganization]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_ResearchPriorityOrganization]
GO

/****** Object:  Synonym [dbo].[main_PortfolioCompany]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_PortfolioCompany]
GO

/****** Object:  Synonym [dbo].[main_OtherAddress]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_OtherAddress]
GO

/****** Object:  Synonym [dbo].[main_OrganizationViewing]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_OrganizationViewing]
GO

/****** Object:  Synonym [dbo].[main_OrganizationRequest]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_OrganizationRequest]
GO

/****** Object:  Synonym [dbo].[main_OrganizationNote]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_OrganizationNote]
GO

/****** Object:  Synonym [dbo].[main_OrganizationMember]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_OrganizationMember]
GO

/****** Object:  Synonym [dbo].[main_Organization]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Organization]
GO

/****** Object:  Synonym [dbo].[main_GroupOrganization]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_GroupOrganization]
GO

/****** Object:  Synonym [dbo].[main_Grade]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Grade]
GO

/****** Object:  Synonym [dbo].[main_FurtherFundMetrics]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_FurtherFundMetrics]
GO

/****** Object:  Synonym [dbo].[main_Fund]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Fund]
GO

/****** Object:  Synonym [dbo].[main_FolderOrganization]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_FolderOrganization]
GO

/****** Object:  Synonym [dbo].[main_Evaluation]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Evaluation]
GO

/****** Object:  Synonym [dbo].[main_EntityMultipleValues]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_EntityMultipleValues]
GO

/****** Object:  Synonym [dbo].[main_Entity]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Entity]
GO

/****** Object:  Synonym [dbo].[main_Currency]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Currency]
GO

/****** Object:  Synonym [dbo].[main_ClientRequest]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_ClientRequest]
GO

/****** Object:  Synonym [dbo].[main_Benchmarks]    Script Date: 9/18/2017 11:04:26 PM ******/
DROP SYNONYM [dbo].[main_Benchmarks]
GO

/****** Object:  Synonym [dbo].[main_Benchmarks]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_Benchmarks] FOR [alexabe_01_stage_new].[dbo].[Benchmarks]
GO

/****** Object:  Synonym [dbo].[main_ClientRequest]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_ClientRequest] FOR [alexabe_01_stage_new].[dbo].[ClientRequest]
GO

/****** Object:  Synonym [dbo].[main_Currency]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_Currency] FOR [alexabe_01_stage_new].[dbo].[Currency]
GO

/****** Object:  Synonym [dbo].[main_Entity]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_Entity] FOR [alexabe_01_stage_new].[dbo].[Entity]
GO

/****** Object:  Synonym [dbo].[main_EntityMultipleValues]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_EntityMultipleValues] FOR [alexabe_01_stage_new].[dbo].[EntityMultipleValues]
GO

/****** Object:  Synonym [dbo].[main_Evaluation]    Script Date: 9/18/2017 11:04:26 PM ******/
CREATE SYNONYM [dbo].[main_Evaluation] FOR [alexabe_01_stage_new].[dbo].[Evaluation]
GO

/****** Object:  Synonym [dbo].[main_FolderOrganization]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_FolderOrganization] FOR [alexabe_01_stage_new].[dbo].[FolderOrganization]
GO

/****** Object:  Synonym [dbo].[main_Fund]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_Fund] FOR [alexabe_01_stage_new].[dbo].[Fund]
GO

/****** Object:  Synonym [dbo].[main_FurtherFundMetrics]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_FurtherFundMetrics] FOR [alexabe_01_stage_new].[dbo].[FurtherFundMetrics]
GO

/****** Object:  Synonym [dbo].[main_Grade]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_Grade] FOR [alexabe_01_stage_new].[dbo].[Grade]
GO

/****** Object:  Synonym [dbo].[main_GroupOrganization]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_GroupOrganization] FOR [alexabe_01_stage_new].[dbo].[GroupOrganization]
GO

/****** Object:  Synonym [dbo].[main_Organization]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_Organization] FOR [alexabe_01_stage_new].[dbo].[Organization]
GO

/****** Object:  Synonym [dbo].[main_OrganizationMember]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_OrganizationMember] FOR [alexabe_01_stage_new].[dbo].[OrganizationMember]
GO

/****** Object:  Synonym [dbo].[main_OrganizationNote]    Script Date: 9/18/2017 11:04:27 PM ******/
CREATE SYNONYM [dbo].[main_OrganizationNote] FOR [alexabe_01_stage_new].[dbo].[OrganizationNote]
GO

/****** Object:  Synonym [dbo].[main_OrganizationRequest]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_OrganizationRequest] FOR [alexabe_01_stage_new].[dbo].[OrganizationRequest]
GO

/****** Object:  Synonym [dbo].[main_OrganizationViewing]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_OrganizationViewing] FOR [alexabe_01_stage_new].[dbo].[OrganizationViewing]
GO

/****** Object:  Synonym [dbo].[main_OtherAddress]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_OtherAddress] FOR [alexabe_01_stage_new].[dbo].[OtherAddress]
GO

/****** Object:  Synonym [dbo].[main_PortfolioCompany]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_PortfolioCompany] FOR [alexabe_01_stage_new].[dbo].[PortfolioCompany]
GO

/****** Object:  Synonym [dbo].[main_ResearchPriorityOrganization]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_ResearchPriorityOrganization] FOR [alexabe_01_stage_new].[dbo].[ResearchPriorityOrganization]
GO

/****** Object:  Synonym [dbo].[main_SearchOptions]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_SearchOptions] FOR [alexabe_01_stage_new].[dbo].[SearchOptions]
GO

/****** Object:  Synonym [dbo].[main_TrackRecord]    Script Date: 9/18/2017 11:04:28 PM ******/
CREATE SYNONYM [dbo].[main_TrackRecord] FOR [alexabe_01_stage_new].[dbo].[TrackRecord]
GO

/****** Object:  Synonym [dbo].[main_TrackUsers]    Script Date: 9/18/2017 11:04:29 PM ******/
CREATE SYNONYM [dbo].[main_TrackUsers] FOR [alexabe_01_stage_new].[dbo].[TrackUsers]
GO

/****** Object:  Synonym [dbo].[main_TrackUsersProfileView]    Script Date: 9/18/2017 11:04:29 PM ******/
CREATE SYNONYM [dbo].[main_TrackUsersProfileView] FOR [alexabe_01_stage_new].[dbo].[TrackUsersProfileView]
GO

/****** Object:  Synonym [dbo].[main_usp_Util_ReIndexDatabase_UpdateStats]    Script Date: 9/18/2017 11:04:29 PM ******/
CREATE SYNONYM [dbo].[main_usp_Util_ReIndexDatabase_UpdateStats] FOR [alexabe_01_stage_new].[dbo].[usp_Util_ReIndexDatabase_UpdateStats]
GO
