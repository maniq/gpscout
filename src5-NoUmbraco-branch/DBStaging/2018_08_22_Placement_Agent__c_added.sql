﻿-- Helper DB:

select * INTO SF_EntityImportConfig_2018_08_21_before_adding_PlacementAgent
FROM SF_EntityImportConfig

UPDATE [SF_EntityImportConfig] 
SET [SelectQuery]='
SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1)
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted
,Publish__c,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
,Personnel_Sourcing_Professional__c,PlacementAgent__r.Name
FROM AIM__Fund__c 
WHERE (LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIM__Account__r.LastModifiedDate >= @LastModifiedDate AND AIM__Account__r.LastModifiedDate < @DateImported)
',
[FirstSelectQuery]='
SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1)
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Vintage_Year__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c
,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
,Personnel_Sourcing_Professional__c,PlacementAgent__r.Name
FROM AIM__Fund__c WHERE IsDeleted = false AND Publish__c = true AND LastModifiedDate < @DateImported AND AIM__Account__r.Publish_Level__c!=NULL AND AIM__Account__r.IsDeleted=false
'
WHERE ID=7 AND [EntityCode] = 'SFFund'


UPDATE [SF_EntityImportConfig] 
SET [SelectQuery]='
SELECT (SELECT Id FROM AIMPortMgmt__Investment_Metrics__r WHERE GPScoutMetricSource__c NOT IN ('''',''N/A'') AND IsDeleted=false ORDER BY AIMPortMgmt__Effective_Date__c DESC LIMIT 1) 
,Id,Name,AIM__Account__c,Currency__c,Fundraising_Status__c,First_Drawn_Capital__c,Fund_Total_Cash_on_Cash__c,Fund_Total_IRR__c,Total_Deal_Professionals__c
,FundTargetSize__c,FundNumber__c,FundLaunchDate__c,Eff_Size__c,Deal_Table_URL__c,SBIC__c,Preqin_Fund_Name__c,Effective_Date__c,IsDeleted,Publish__c
,AIM__Account__r.Publish_Level__c,AIM__Account__r.IsDeleted 
,Personnel_Senior_Professional__c, Personnel_Mid_Level__c, Personnel_Junior_Staff__c, Personnel_Operating_Partner_Professional__c
,Personnel_Sourcing_Professional__c,PlacementAgent__r.Name
FROM AIM__Fund__c 
WHERE Id IN (@AdditionalFundIds)
',
[FirstSelectQuery]=NULL
WHERE ID=16 AND [EntityCode] = 'SFAdditionalFunds'


ALTER TABLE dbo.Funds DROP COLUMN Include, NewFundId, DateImportedUtc
GO

ALTER TABLE dbo.Funds ADD 
	PlacementAgentName nvarchar(255) NULL,
	DateImportedUtc datetime2 NULL,
	NewFundId uniqueidentifier NULL,
	Include bit NULL
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/01/2017 - PersonnelSeniorProfessionals, PersonnelMidLevelProfessionals, PersonnelJuniorLevelProfessionals, PersonnelOperatingPartnerProfessionals,
--					   PersonnelTotalDealProfessionals, FundLaunchDate fields added
--		- 10/09/2017 - Status = 'Assumed Closed / Unknown' will import as null
--		- 11/10/2017 - Personnel_Sourcing_Professional__c field added
--		- 08/22/2018 - PlacementAgentName field added
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFunds]
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Funds WHERE SBIC__c NOT IN ('No','Yes') AND SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Funds.SBIC__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE F SET NewFundId = NEWID()
	FROM dbo.Funds F
	WHERE NOT EXISTS (SELECT NULL FROM main_Fund Target WHERE Target.SFID = F.Id)

	INSERT [main_Entity] (Id, EntityType)
	SELECT Source.NewFundId, 2
	FROM dbo.Funds Source 
	WHERE Source.NewFundId IS NOT NULL AND Source.NewFundId NOT IN (SELECT Id FROM [main_Entity] WHERE EntityType=2)
	

	MERGE [main_Fund] WITH (HOLDLOCK) AS Target
	USING (SELECT 
			f.[Name]
			--,f.[AIM__Account__c]
			,o.[Id] AS [OrganizationId]
			--,f.[Currency__c]
			,c.[Id] AS [CurrencyId]
			,NULLIF(f.[Fundraising_Status__c], 'Assumed Closed / Unknown') [Fundraising_Status__c]
			,f.[Fund_Total_Cash_on_Cash__c]
			,f.[Fund_Total_IRR__c]
			,NULLIF(TRY_CONVERT(int, f.[Total_Deal_Professionals__c]), 0) PersonnelTotalDealProfessionals
			,f.[FundTargetSize__c]
			,f.[FundNumber__c]
			--,(CASE 
			--	WHEN NULLIF(f.[First_Drawn_Capital__c], '') IS NOT NULL THEN YEAR(f.[First_Drawn_Capital__c])
			--	ELSE YEAR(f.[FundLaunchDate__c])
			--	END) AS [VintageYear]
			,f.FundLaunchDate__c FundLaunchDate
			,f.[Vintage_Year__c] AS [VintageYear]
			,f.[Eff_Size__c]
			,(CASE WHEN f.[Deal_Table_URL__c] NOT LIKE 'http://%' AND f.[Deal_Table_URL__c] NOT LIKE 'https://%' THEN 'https://' + f.[Deal_Table_URL__c] ELSE f.[Deal_Table_URL__c] END) [Deal_Table_URL__c]
			,f.[Effective_Date__c]
			,f.Personnel_Senior_Professional__c PersonnelSeniorProfessionals
			,f.Personnel_Mid_Level__c PersonnelMidLevelProfessionals
			,f.Personnel_Junior_Staff__c PersonnelJuniorLevelProfessionals
			,f.Personnel_Operating_Partner_Professional__c PersonnelOperatingPartnerProfessionals
			,f.Personnel_Sourcing_Professional__c PersonnelSourcingProfessionals
			,f.PlacementAgentName
			,f.[NewFundId]
			,[Include] = CASE WHEN IsDeleted = 0 AND Publish__c = 1 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			,f.[DateImportedUtc]
			,f.[Id]
	  FROM	[dbo].[Funds] f
			LEFT JOIN [main_Organization] o ON f.[AIM__Account__c] = o.[SFID]
			LEFT JOIN [main_Currency] c ON f.[Currency__c] = c.[Name]
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
				Target.[Name] = Source.[Name],
				Target.[OrganizationId] = Source.[OrganizationId],
				Target.[CurrencyId] = Source.[CurrencyId],
				Target.[Status] = Source.[Fundraising_Status__c],
				Target.[Moic] = Source.[Fund_Total_Cash_on_Cash__c],
				Target.[GrossIrr] = Source.[Fund_Total_IRR__c],
				-- join with organization and set organization.NumberOfInvestmentProfessionals = Source.[Total_Deal_Professionals__c],
				Target.[TargetSize] = Source.[FundTargetSize__c],
				Target.[SortOrder] = Source.[FundNumber__c],
				Target.[VintageYear] = Source.[VintageYear],
				Target.[FundSize] = Source.[Eff_Size__c],
				Target.[AnalysisUrl] = Source.[Deal_Table_URL__c],
				Target.[AsOf] = Source.[Effective_Date__c],
				Target.[DateImportedUtc] = Source.[DateImportedUtc],
				Target.PersonnelSeniorProfessionals = Source.PersonnelSeniorProfessionals,
				Target.PersonnelMidLevelProfessionals = Source.PersonnelMidLevelProfessionals,
				Target.PersonnelJuniorLevelProfessionals = Source.PersonnelJuniorLevelProfessionals,
				Target.PersonnelOperatingPartnerProfessionals = Source.PersonnelOperatingPartnerProfessionals,
				Target.PersonnelTotalDealProfessionals = Source.PersonnelTotalDealProfessionals,
				Target.FundLaunchDate = Source.FundLaunchDate,
				Target.PersonnelSourcingProfessionals = Source.PersonnelSourcingProfessionals,
				Target.PlacementAgentName = Source.PlacementAgentName

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
				[Id]
				,[Name]
				,[OrganizationId]
				,[CurrencyId]
				,[Status]
				,[Moic]
				,[GrossIrr]
				,[TargetSize]
				,[SortOrder]
				,[VintageYear]
				,[FundSize]
				,[AnalysisUrl]
				,[AsOf]
				,[DateImportedUtc]
				,[SFID]
				,PersonnelSeniorProfessionals
				,PersonnelMidLevelProfessionals
				,PersonnelJuniorLevelProfessionals
				,PersonnelOperatingPartnerProfessionals
				,PersonnelTotalDealProfessionals
				,FundLaunchDate
				,PersonnelSourcingProfessionals
				,PlacementAgentName
	) 
		VALUES(	
				Source.[NewFundId]
				,Source.[Name]
				,Source.[OrganizationId]
				,Source.[CurrencyId]
				,Source.[Fundraising_Status__c]
				,Source.[Fund_Total_Cash_on_Cash__c]
				,Source.[Fund_Total_IRR__c]
				,Source.[FundTargetSize__c]
				,Source.[FundNumber__c]
				,Source.[VintageYear]
				,Source.[Eff_Size__c]
				,Source.[Deal_Table_URL__c]
				,Source.[Effective_Date__c]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.PersonnelSeniorProfessionals
				,Source.PersonnelMidLevelProfessionals
				,Source.PersonnelJuniorLevelProfessionals
				,Source.PersonnelOperatingPartnerProfessionals
				,Source.PersonnelTotalDealProfessionals
				,Source.FundLaunchDate
				,Source.PersonnelSourcingProfessionals
				,Source.PlacementAgentName
			)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;	

	COMMIT TRAN

END

-- main DB:
ALTER TABLE dbo.Fund ADD PlacementAgentName nvarchar(255)
GO