﻿USE [GPScoutSFImportStagingStage]
GO
/****** Object:  StoredProcedure [dbo].[SF_PostImport]    Script Date: 2/13/2016 8:20:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXEC [SF_PostImport] '5/27/2015 3:36:05 PM'  
ALTER PROCEDURE [dbo].[SF_PostImport]
	@DateImportedUtc datetime
AS

BEGIN TRY 

BEGIN TRAN

UPDATE Accounts SET DateImportedUtc = @DateImportedUtc
UPDATE Benchmarks SET DateImportedUtc = @DateImportedUtc
UPDATE Contacts SET DateImportedUtc = @DateImportedUtc
UPDATE FirmAddreses SET DateImportedUtc = @DateImportedUtc
UPDATE TrackRecords SET DateImportedUtc = @DateImportedUtc
UPDATE Funds SET DateImportedUtc = @DateImportedUtc
UPDATE FundMetrics SET DateImportedUtc = @DateImportedUtc
--UPDATE Metrics SET DateImportedUtc = @DateImportedUtc
UPDATE StrengthsAndConcerns SET DateImportedUtc = @DateImportedUtc
UPDATE GPScoutScorecards SET DateImportedUtc = @DateImportedUtc

-- store Org Id-s being in In Process mode before data import
DELETE Ids WHERE OrgInProcessBeforeImport IS NOT NULL
INSERT IDs (OrgInProcessBeforeImport)
SELECT Id FROM [alexabe_01_stage].[dbo].Organization WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '')


UPDATE Accounts SET Most_Recent_Fund_Currency__c = 'US Dollar' WHERE Most_Recent_Fund_Currency__c IS NULL OR Most_Recent_Fund_Currency__c=''
UPDATE Funds SET Currency__c = 'US Dollar' WHERE Currency__c IS NULL OR Currency__c=''

INSERT INTO [alexabe_01_stage].[dbo].Currency(Id, Code, Name, Symbol)
SELECT NEWID(), 'n/a', CurrencyName, 'n/a'
FROM (
	SELECT DISTINCT ISNULL(CurrencyName, '') CurrencyName
	FROM (
	SELECT Currency__c CurrencyName
	FROM Funds
	UNION ALL
	SELECT Most_Recent_Fund_Currency__c 
	FROM Accounts
	) C2
) C
WHERE C.CurrencyName NOT IN (SELECT C3.Name FROM [alexabe_01_stage].[dbo].Currency C3)


EXEC SF_ImportBenchmarks

/*
TRUNCATE TABLE Benchmarks_Last

INSERT INTO Benchmarks_Last (Id, VintageYear, AsOf, 
	[DpiFirstQuartile], [DpiMedian], [DpiThirdQuartile],
	[NetIrrFirstQuartile], [NetIrrMedian], [NetIrrThirdQuartile],
	[TvpiFirstQuartile], [TvpiMedian], [TvpiThirdQuartile])
SELECT 
	B.Id, B.VintageYear, B.AsOf,
	B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile,
	B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile,
	B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile
FROM (
	SELECT MAX(B2.AsOf) MaxAsOf, VintageYear 
	FROM alexabe_01_stage.dbo.Benchmarks B2 
	GROUP BY B2.VintageYear
) A
JOIN alexabe_01_stage.dbo.Benchmarks B ON A.VintageYear = B.VintageYear AND A.MaxAsOf = B.AsOf
*/

EXEC SF_ImportAccounts
EXEC SF_ImportFunds
--EXEC SF_ImportMetrics
EXEC SF_ImportFundMetrics
EXEC SF_ImportFurtherFundMetrics
EXEC SF_ImportContacts
EXEC SF_ImportFirmAddresses
EXEC SF_ImportTrackRecords
EXEC SF_ImportStrenghtsAndConcerns
EXEC SF_ImportGPScoutScorecards

EXEC [dbo].[SF_PostImport_FirmAddresses]

-- Handled in SP [SF_ImportGPScoutScorecards] instead
--DELETE	G
--FROM	(
--		SELECT	ROW_NUMBER() OVER (PARTITION BY OrganizationId ORDER BY [DateReviewed] DESC) AS RecID 
--		FROM	alexabe_01_stage.dbo.Grade
--		) AS G
--WHERE	RecID > 1 


-- insert Grade record for Organizations not having at least one Grade record. This is required b/c we need to store some Organization level info at Grade level anyway. See next update statement.
INSERT [alexabe_01_stage].[dbo].Grade (Id, OrganizationId, DateImportedUtc, SFID)
SELECT NEWID(), O.Id, @DateImportedUtc, ''
FROM [alexabe_01_stage].[dbo].Organization O
WHERE O.Id NOT IN (SELECT G.OrganizationId FROM [alexabe_01_stage].[dbo].Grade G) 


UPDATE G
SET
	G.ScatterchartText = A.Grade_Scatterchart_Text__c,
	G.AQScorecardText = A.Grade_Qualitative_Text__c,
	G.QoRScorecardText = A.Grade_Quantitative_Text__c
FROM [alexabe_01_stage].[dbo].[Grade] G
JOIN [alexabe_01_stage].[dbo].Organization O ON G.OrganizationId = O.Id
JOIN dbo.Accounts A ON O.SFID = A.Id


UPDATE O
SET 
	O.QualitativeGradeNumber = SC.Scaled_Score__c,
	O.QualitativeGrade = dbo.udf_GetGradeWord(SC.Scaled_Score__c), --  SC.Qualitative_Final_Grade__c,
	O.QuantitativeGrade = dbo.udf_GetGradeWord(SC.Quantitative_Final_Number__c), -- SC.Quantitative_Final_Grade__c,
	O.QuantitativeGradeNumber = SC.Quantitative_Final_Number__c
FROM [alexabe_01_stage].[dbo].Organization O
--JOIN GPScoutScoreCards SC ON O.SFID = SC.[GPScout_Scorecard__c]
JOIN Accounts A ON O.SFID = A.Id
LEFT JOIN GPScoutScoreCards SC ON A.[MostRecentScorecardSFID] = SC.[Id] AND SC.IsDeleted = 0 AND SC.Publish_Scorecard__c = 1


-- set currency to USD for Organizations whose CurrencyId is null
DECLARE @CurrencyId UNIQUEIDENTIFIER
SET		@CurrencyId = (SELECT Id FROM [alexabe_01_stage].[dbo].[Currency] WHERE Code = 'USD')
UPDATE	[alexabe_01_stage].[dbo].[Organization]
SET		CurrencyId = @CurrencyId
WHERE	CurrencyId IS NULL


-- set list of countries assigned to each Organization through their list of firm addresses
--UPDATE O
--SET CountriesPipeDelimited = STUFF((
--	SELECT '|' + Country FROM (
--		SELECT DISTINCT Country FROM (
--			SELECT DISTINCT OA.Country FROM alexabe_01_stage.dbo.OtherAddress OA WHERE OA.OrganizationId = O.Id
--			UNION ALL
--			SELECT O.Address1Country
--		) B
--	) A FOR XML PATH(''))
--,1,1,'')
--FROM alexabe_01_stage.dbo.Organization O 
--WHERE O.DateImportedUtc = @DateImportedUtc


-- set list of countries assigned to each Organization from list of countries of most recent fund of the Organization
UPDATE O
SET O.CountriesPipeDelimited = dbo.udf_ToPipeDelimitedValue(A.MostRecentFundCountries, ';')
FROM Accounts A 
JOIN alexabe_01_stage.dbo.Organization O ON A.Id = O.SFID AND A.Include=1


-- delete entity multiple values for Organizations and Funds that were imported. They will be (re)created by post-import c# code
DELETE EV
FROM [alexabe_01_stage].[dbo].Organization O
JOIN [alexabe_01_stage].[dbo].EntityMultipleValues EV ON O.SFID IS NOT NULL AND O.DateImportedUtc = @DateImportedUtc AND O.Id = EV.EntityId AND EV.Field = 1

DELETE EV
FROM [alexabe_01_stage].[dbo].Fund F
JOIN [alexabe_01_stage].[dbo].EntityMultipleValues EV ON F.SFID IS NOT NULL AND F.DateImportedUtc = @DateImportedUtc AND F.Id = EV.EntityId AND EV.Field = 2

-- drop orphan Entities belonging to Funds
SELECT E.Id INTO #EntitiesToDelete
FROM alexabe_01_stage.dbo.Entity E
WHERE E.EntityType = 2 AND E.Id NOT IN (SELECT F.Id FROM alexabe_01_stage.dbo.Fund F)

DELETE alexabe_01_stage.dbo.EntityMultipleValues WHERE EntityId IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)
DELETE alexabe_01_stage.dbo.Entity WHERE Id IN (SELECT E2D.Id FROM #EntitiesToDelete E2D)

DROP TABLE #EntitiesToDelete

-- backend code will re-populate
TRUNCATE TABLE alexabe_01_stage.dbo.SearchOptions

COMMIT TRAN

END TRY
BEGIN CATCH
	ROLLBACK TRAN
	-- THROW
    -- if THROW statement is not available:    

    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH