USE [alexabe_01_redesign]
GO

/****** Object:  Table [dbo].[temp_searchoptions_2019_04_11]    Script Date: 4/11/2019 11:13:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[temp_searchoptions_2019_04_11](
	[Field] [int] NOT NULL,
	[Value] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SearchOptions_temp_searchoptions_2019_04_11] PRIMARY KEY CLUSTERED 
(
	[Field] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

Insert into [temp_searchoptions_2019_04_11] Select * from dbo.[SearchOptions] 