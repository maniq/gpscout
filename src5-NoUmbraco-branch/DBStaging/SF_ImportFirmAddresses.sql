﻿USE [GPScoutSFImportStagingStage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFirmAddresses]

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE FirmAddreses
	SET [Firm_Location]=geography::STPointFromText('POINT('+ CAST(Firm_Location_Long AS varchar(99)) +' '+ CAST(Firm_Location_Lat AS varchar(99))+')', 4326)
	WHERE Firm_Location_Lat IS NOT NULL AND Firm_Location_Long IS NOT NULL

	MERGE [alexabe_01].[dbo].[OtherAddress] WITH (HOLDLOCK) AS Target
	USING (SELECT
				FA.[Street_Address__c]
				,FA.[City__c]
				,FA.[State__c]
				,FA.[Country__c]
				--,FA.[Firm_Location__c]
				,FA.[Firm_Location_Lat]
				,FA.[Firm_Location_Long]
				,FA.[Address_Description__c]
				,FA.[Phone__c]
				,FA.[Fax__c]
				,FA.[Zip_Code__c]
				,FA.[Address_2__c]
				,FA.[DateImportedUtc]
				,FA.[Id]
				,O.Id OrganizationId
				,[Include] = CASE WHEN ISNULL(FA.[Address_Description__c], '') != 'Billing Headquarters' AND IsDeleted = 0 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
			FROM [dbo].[FirmAddreses] FA
			LEFT JOIN [alexabe_01].dbo.Organization O ON FA.Account__c = O.SFID
	) AS Source 
		ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.OrganizationId = Source.OrganizationId,
		Target.[Address1] = Source.[Street_Address__c],
		Target.[City] = Source.[City__c],
		Target.[StateProvince] = Source.[State__c],
		Target.[Country] = Source.[Country__c],
		Target.[Phone] = Source.[Phone__c],
		Target.[Fax] = Source.[Fax__c],
		Target.[PostalCode] = Source.[Zip_Code__c],
		Target.[Address2] = Source.[Address_2__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[Latitude] = Source.[Firm_Location_Lat],
		Target.[Longitude] = Source.[Firm_Location_Long]

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id
			,[Address1]
			,[City]
			,[StateProvince]
			,[Country]
			,[Phone]
			,[Fax]
			,[PostalCode]
			,[Address2]
			,[DateImportedUtc]
			,[SFID]
			,OrganizationId
			,[Latitude]
			,[Longitude]
	) 
		VALUES(	
				NEWID()
				,Source.[Street_Address__c]
				,Source.[City__c]
				,Source.[State__c]
				,Source.[Country__c]
				,Source.[Phone__c]
				,Source.[Fax__c]
				,Source.[Zip_Code__c]
				,Source.[Address_2__c]
				,Source.[DateImportedUtc]
				,Source.[Id]
				,Source.OrganizationId
				,Source.[Firm_Location_Lat]
				,Source.[Firm_Location_Long]
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END
