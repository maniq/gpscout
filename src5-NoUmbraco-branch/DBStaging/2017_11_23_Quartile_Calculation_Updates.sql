﻿ALTER TABLE dbo.Fund ADD
	SourceAwareTvpi AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN Moic 
		WHEN Source LIKE 'RCP' THEN Tvpi 
		ELSE PreqinTvpi 
	END PERSISTED 
GO

ALTER TABLE dbo.Fund ADD
	SourceAwareIrr AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN GrossIrr 
		WHEN Source LIKE 'RCP' THEN Irr 
		ELSE PreqinIrr 
	END PERSISTED 
GO

ALTER TABLE dbo.Fund ADD
	SourceAwareDpi AS CASE 
		WHEN Source LIKE 'Preqin' THEN PreqinDpi 
		ELSE Dpi
	END PERSISTED 
GO

-- =================================================
ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareTvpi AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN Moic 
		WHEN Source LIKE 'RCP' THEN Tvpi 
		ELSE PreqinTvpi 
	END PERSISTED 
GO

ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareIrr AS CASE 
		WHEN Source LIKE 'RCP (Gross)' THEN GrossIrr 
		WHEN Source LIKE 'RCP' THEN Irr 
		ELSE PreqinIrr 
	END PERSISTED 
GO

ALTER TABLE dbo.FurtherFundMetrics ADD
	SourceAwareDpi AS CASE 
		WHEN Source LIKE 'Preqin' THEN PreqinDpi 
		ELSE Dpi
	END PERSISTED 
GO

--======
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SFP ZK
-- Create date: 10/21/2015
-- Description:	This logic imports not most recent rcp fund metrics
-- History:
--	10/29/2017 - Logic change related to GPScoutMetricSource__c
--  11/23/2017 - SourceAware Dpi, Tvpi and Irr added to quartile calculations
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFurtherFundMetrics]
AS

DELETE fm
FROM main_FurtherFundMetrics fm
--JOIN main_Fund f ON fm.FundId = f.Id
--JOIN Funds stgFunds ON f.SFID = stgFunds.Id
JOIN FundMetrics stgFundMetrics ON fm.SFID = stgFundMetrics.Id

INSERT main_FurtherFundMetrics (
		Id, FundId, 
		PreqinAsOf, 
		[PreqinCalled], 
		[PreqinDpi],
		PreqinTvpi, 
		[PreqinIrr], 
		[PreqinFundSize], 
		[InvestedCapital],
		[RealizedValue], 
		[UnrealizedValue], 
		[TotalValue], 
		[GrossIrr], 
		[Moic], 
		[Dpi], 
		[Irr], 
		[Tvpi], 
		[AsOf], 
		[DateImportedUtc], 
		[SFID], 
		[Source]--, 
		--[DPIQuartile], 
		--[TVPIQuartile], 
		--[NetIrrQuartile]
)
SELECT NEWID(), F.Id FundID, 
	CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
	CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
	CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END, -- tvpi
	CASE 
		WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
		WHEN Source.[Include] = 0 THEN NULL
	END,
	CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc] END,
	Source.Id, -- SFID
	Source.GPScoutMetricSource__c -- Source.Public_Metric_Source__c
FROM FundMetrics Source 
--JOIN Funds StgFunds ON StgFunds.Id = Source.[AIMPortMgmt__Fund__c]
JOIN main_Fund F ON Source.AIMPortMgmt__Fund__c = F.SFID
--WHERE Source.ImportStep IS NULL AND ISNULL(Source.IsMostRecent, 0) !=1 -- these 2 criterias for now are the same


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.SourceAwareDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.SourceAwareTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.SourceAwareIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM main_FurtherFundMetrics fm
LEFT JOIN main_Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN main_Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source LIKE 'RCP%'
OR fm.Source = '' OR fm.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


UPDATE fm
SET
	[DPIQuartile] = dbo.udf_GetQuartile(fm.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
	[TVPIQuartile] = dbo.udf_GetQuartile(fm.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
	[NetIrrQuartile] = dbo.udf_GetQuartile(fm.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
	[BenchmarkAsOf] = B.AsOf,
	BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
	BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
	BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
FROM main_FurtherFundMetrics fm
LEFT JOIN main_Fund F ON fm.FundId = F.Id
--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
LEFT JOIN main_Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
WHERE fm.Source='Preqin'

--=====
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--	10/29/2017 - Public_Metric_Source__c replaced with GPScoutMetricSource__c and additional logic change
--  11/23/2017 - SourceAware Dpi, Tvpi and Irr added to quartile calculations
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportFundMetrics]
AS
BEGIN

	SET NOCOUNT ON;

	--UPDATE FM
	--SET IsMostRecent = CASE WHEN LatestFMs.MaxEffectiveDate IS NOT NULL THEN 1 ELSE 0 END
	--FROM FundMetrics FM
	--LEFT JOIN (
	--	SELECT FM2.AIMPortMgmt__Fund__c, MAX(FM2.[AIMPortMgmt__Effective_Date__c]) MaxEffectiveDate
	--	FROM FundMetrics FM2 
	--	GROUP BY FM2.AIMPortMgmt__Fund__c
	--) LatestFMs
	--	ON FM.AIMPortMgmt__Fund__c = LatestFMs.AIMPortMgmt__Fund__c AND FM.AIMPortMgmt__Effective_Date__c = LatestFMs.MaxEffectiveDate AND FM.Publish_Metric__c=1
	--		AND FM.IsDeleted=0

	-- Metrics marked w/ ImportStep=1 means that these metrics have been imported as Most Recent Fund Metrics
	UPDATE FundMetrics
	SET 
		IsMostRecent = CASE WHEN EXISTS (SELECT NULL FROM Funds WHERE Funds.MostRecentMetricSFID=FundMetrics.Id AND FundMetrics.ImportStep = 1) THEN 1 ELSE 0 END,
		--[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Public_Metric_Source__c, '') != ''
		[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(GPScoutMetricSource__c, '') NOT IN ('','N/A')
		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
	FROM FundMetrics

	MERGE main_Fund WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  fm.[AIMPortMgmt__Effective_Date__c]
		  ,fm.[Preqin_Called__c]
		  ,fm.[Preqin_DPI__c]
		  ,fm.[Preqin_TVPI__c]
		  ,fm.[Preqin_Net_IRR__c]
		  ,fm.[Preqin_Fund_Size__c]
--		  ,fm.[DateImportedUtc]
--		  ,fm.[Id]
		  --,o.[Id] AS [OrganizationId]
		  --,[Include] = CASE WHEN IsDeleted = 0 AND ISNULL(Publish_Metric__c, '') != ''
--		  AND (ParentAccount_Id IS NULL OR (ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0))
--		  AND ParentFund_IsDeleted=0 AND ParentFund_Publish__c=1 THEN 1 ELSE 0 END
		  ,fm.[Include]
		  ,fm.[Invested_Capital__c]
		  ,fm.[Realized_Value__c]
		  ,fm.[Unrealized_Value__c]
		  ,fm.[Total_Value__c]
		  ,fm.[Fund_Total_IRR__c]
		  ,fm.[Fund_Total_Cash_on_Cash__c]
		  ,fm.[DPI__c]
		  ,fm.[Fund_Net_IRR__c]
		  ,fm.[Fund_Net_Cash_on_Cash__c]
		  ,fm.AIMPortMgmt__Fund__c FundSFID
		  --,Public_Metric_Source__c
		  ,fm.GPScoutMetricSource__c
		  --,IsMoreRecentThanExisting = CASE WHEN F.Id IS NULL THEN 0 ELSE 1 END
	  FROM [FundMetrics] fm
	  --LEFT JOIN main_Fund F 
		--ON fm.AIMPortMgmt__Fund__c = F.SFID AND fm.[AIMPortMgmt__Effective_Date__c] >= CASE WHEN ISNULL(F.AsOf, '1/1/1900')>=ISNULL(F.PreqinAsOf, '1/1/1900') THEN F.AsOf ELSE F.PreqinAsOf END
--	  LEFT JOIN main_Organization o ON fm.AIMPortMgmt__Account__c = o.SFID
--	  WHERE o.Id IS NOT NULL
	  WHERE IsMostRecent = 1
	  ) AS Source
	ON Target.SFID = Source.FundSFID

	WHEN MATCHED THEN
		UPDATE SET 
			Target.PreqinAsOf = CASE WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c]	END,
			Target.PreqinCalled = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Called__c]	END,
			Target.PreqinDpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_DPI__c]	END,
			Target.PreqinTvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_TVPI__c]	END,
			Target.PreqinIrr = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Net_IRR__c]	END,
			Target.PreqinFundSize = CASE WHEN Source.[Include] = 1 THEN Source.[Preqin_Fund_Size__c]	END,
--			Target.[DateImportedUtc] = CASE WHEN Source.[Include] = 1 THEN Source.[DateImportedUtc]	END,
			Target.InvestedCapital = CASE WHEN Source.[Include] = 1 THEN Source.[Invested_Capital__c] END, 
			Target.RealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Realized_Value__c] END,
			Target.UnrealizedValue = CASE WHEN Source.[Include] = 1 THEN Source.[Unrealized_Value__c] END,
			Target.TotalValue = CASE WHEN Source.[Include] = 1 THEN Source.[Total_Value__c] END,
			Target.GrossIrr = CASE 
								WHEN Source.[Include] = 1 THEN Source.[Fund_Total_IRR__c] 
								WHEN Source.[Include] = 0 THEN NULL
							END,
			Target.Moic = CASE 
							WHEN Source.[Include] = 1 THEN Source.[Fund_Total_Cash_on_Cash__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Dpi = CASE WHEN Source.[Include] = 1 THEN Source.[DPI__c] END,
			Target.Irr = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_IRR__c] END,
			Target.Tvpi = CASE WHEN Source.[Include] = 1 THEN Source.[Fund_Net_Cash_on_Cash__c] END,
			Target.AsOf = CASE 
							WHEN Source.[Include] = 1 THEN Source.[AIMPortMgmt__Effective_Date__c] 
							WHEN Source.[Include] = 0 THEN NULL
						END,
			Target.Source = Source.GPScoutMetricSource__c -- Source.Public_Metric_Source__c
		;
/*
		AND Source.[Include] = 1 THEN UPDATE SET 
		Target.PreqinAsOf = Source.[AIMPortMgmt__Effective_Date__c],
		Target.PreqinCalled = Source.[Preqin_Called__c],
		Target.PreqinDpi = Source.[Preqin_DPI__c],
		Target.PreqinTvpi = Source.[Preqin_TVPI__c],
		Target.PreqinIrr = Source.[Preqin_Net_IRR__c],
		Target.PreqinFundSize = Source.[Preqin_Fund_Size__c],
--		Target.[DateImportedUtc] = Source.[DateImportedUtc],
--		Target.[OrganizationId] = Source.[OrganizationId],
		Target.InvestedCapital = Source.[Invested_Capital__c],
		Target.RealizedValue = Source.[Realized_Value__c],
		Target.UnrealizedValue = Source.[Unrealized_Value__c],
		Target.TotalValue = Source.[Total_Value__c],
		Target.GrossIrr = Source.[Fund_Total_IRR__c],
		Target.Moic = Source.[Fund_Total_Cash_on_Cash__c],
		Target.Dpi = Source.[DPI__c],
		Target.Irr = Source.[Fund_Net_IRR__c],
		Target.Tvpi = Source.[Fund_Net_Cash_on_Cash__c],
		Target.AsOf = Source.[AIMPortMgmt__Effective_Date__c]
*/


-- Inserts and deletes should not occur b/c they already have been handled by SP [SF_ImportFunds]

	--WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
	--		Id
	--		,PreqinAsOf
	--		,PreqinCalled
	--		,PreqinDpi
	--		,PreqinTvpi
	--		,PreqinIrr
	--		,PreqinFundSize
--	--		,DateImportedUtc
	--		,SFID
	--		,[OrganizationId]
	--		,InvestedCapital
	--		,RealizedValue
	--		,UnrealizedValue
	--		,TotalValue
	--		,GrossIrr
	--		,Moic
	--		,Dpi
	--		,Irr
	--		,Tvpi
	--		,AsOf
	--) 
	--	VALUES(	NEWID()
	--			,Source.[AIMPortMgmt__Effective_Date__c]
	--			,Source.[Preqin_Called__c]
	--			,Source.[Preqin_DPI__c]
	--			,Source.[Preqin_TVPI__c]
	--			,Source.[Preqin_Net_IRR__c]
	--			,Source.[Preqin_Fund_Size__c]
--	--			,Source.[DateImportedUtc]
	--			,Source.FundSFID--[Id]
	--			,Source.[OrganizationId]
	--			,Source.[Invested_Capital__c]
	--			,Source.[Realized_Value__c]
	--			,Source.[Unrealized_Value__c]
	--			,Source.[Total_Value__c]
	--			,Source.[Fund_Total_IRR__c]
	--			,Source.[Fund_Total_Cash_on_Cash__c]
	--			,Source.[DPI__c]
	--			,Source.[Fund_Net_IRR__c]
	--			,Source.[Fund_Net_Cash_on_Cash__c]
	--			,Source.[AIMPortMgmt__Effective_Date__c]

	--	)
	--WHEN MATCHED AND Source.[Include] = 0 THEN 
	--	DELETE;

	/*
	UPDATE main_Fund
	SET PerformanceDataSource = CASE
		WHEN 
			GrossIrr IS NULL 
			AND Moic IS NULL 
			AND Dpi IS NULL 
			AND Irr IS NULL 
			AND Tvpi IS NULL
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	*/

	UPDATE F
	SET PerformanceDataSource = CASE
		--WHEN fm.Publish_Metric__c = 'Preqin'
		--WHEN fm.Public_Metric_Source__c = 'Preqin'
		WHEN fm.GPScoutMetricSource__c = 'Preqin'
		THEN 'Preqin'
		ELSE 'General Partner'
	END
	FROM main_Fund F
	JOIN [FundMetrics] fm ON 
		F.SFID = fm.AIMPortMgmt__Fund__c -- fm.AIMPortMgmt__Fund__c is SFID of Fund
		AND fm.[IsMostRecent] = 1

	UPDATE Fund
	SET
		Fund.PreqinAsOf = NULL,
		Fund.PreqinCalled = NULL,
		Fund.PreqinDpi = NULL,
		Fund.PreqinTvpi = NULL,
		Fund.PreqinIrr = NULL,
		Fund.PreqinFundSize = NULL,
--		Fund.[DateImportedUtc] = NULL,
--		Fund.[OrganizationId] = Source.[OrganizationId],
		Fund.InvestedCapital = NULL,
		Fund.RealizedValue = NULL,
		Fund.UnrealizedValue = NULL,
		Fund.TotalValue = NULL,
		Fund.GrossIrr = NULL,
		Fund.Moic = NULL,
		Fund.Dpi = NULL,
		Fund.Irr = NULL,
		Fund.Tvpi = NULL,
		Fund.AsOf = NULL,
		Fund.Source = NULL,
		Fund.[BenchmarkAsOf] = NULL,
		Fund.[BenchmarkDpiFirstQuartile] = NULL,
		Fund.[BenchmarkDpiMedian] = NULL,
		Fund.[BenchmarkDpiThirdQuartile] = NULL,

		Fund.[BenchmarkNetIrrFirstQuartile] = NULL,
		Fund.[BenchmarkNetIrrMedian] = NULL,
		Fund.[BenchmarkNetIrrThirdQuartile] = NULL,

		Fund.[BenchmarkTvpiFirstQuartile] = NULL,
		Fund.[BenchmarkTvpiMedian] = NULL,
		Fund.[BenchmarkTvpiThirdQuartile] = NULL
	FROM main_Fund Fund
	JOIN Funds ON Fund.SFID = Funds.Id AND Funds.MostRecentMetricSFID IS NULL

	
	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.SourceAwareDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.SourceAwareTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.SourceAwareIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM main_Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN main_Benchmarks B ON F.AsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source LIKE 'RCP%' -- F.Source = 'RCP' 10/29/2017
	OR F.Source = '' OR F.Source IS NULL -- for these udf_GetQuartile will return 'N/A'


	UPDATE F
	SET
		[DPIQuartile] = dbo.udf_GetQuartile(F.PreqinDpi, B.DpiFirstQuartile, B.DpiMedian, B.DpiThirdQuartile),
		[TVPIQuartile] = dbo.udf_GetQuartile(F.PreqinTvpi, B.TvpiFirstQuartile, B.TvpiMedian, B.TvpiThirdQuartile),
		[NetIrrQuartile] = dbo.udf_GetQuartile(F.PreqinIrr, B.NetIrrFirstQuartile, B.NetIrrMedian, B.NetIrrThirdQuartile),
		[BenchmarkAsOf] = B.AsOf,
		BenchmarkDpiFirstQuartile = B.DpiFirstQuartile, BenchmarkDpiMedian = B.DpiMedian, BenchmarkDpiThirdQuartile = B.DpiThirdQuartile,
		BenchmarkNetIrrFirstQuartile = B.NetIrrFirstQuartile, BenchmarkNetIrrMedian = B.NetIrrMedian, BenchmarkNetIrrThirdQuartile = B.NetIrrThirdQuartile,
		BenchmarkTvpiFirstQuartile = B.TvpiFirstQuartile, BenchmarkTvpiMedian = B.TvpiMedian, BenchmarkTvpiThirdQuartile = B.TvpiThirdQuartile
	FROM main_Fund F
	--LEFT JOIN Benchmarks_Last B ON F.VintageYear = B.VintageYear
	LEFT JOIN main_Benchmarks B ON F.PreqinAsOf = B.AsOf AND F.VintageYear = B.VintageYear
	WHERE F.Source='Preqin'
	

END
