﻿UPDATE SF_EntityImportConfig
SET SelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c
FROM Account 
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported
',
FirstSelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL
'
WHERE ID=3 AND EntityCode = 'SFAccount'


UPDATE SF_EntityImportConfig
SET SelectQuery='
SELECT GPScout_Phase_Assigned__c,GPScout_Phase_Completed__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, 
Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Qualitative_Text__c, Grade_Quantitative_Text__c, Grade_Scatterchart_Text__c, Id, 
Investment_Thesis__c, GPScout_Last_Updated__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, 
Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, 
Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, 
Track_Record_Text__c, Website, YearGPFounded__c, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, Level_of_Disclosure__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)
',
FirstSelectQuery=NULL
WHERE ID=13 AND EntityCode = 'SFAdditionalAccounts'

-- =========================================================================
UPDATE SF_EntityImportConfig
SET SelectQuery='
SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c
,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c
,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c
,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c
,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted
,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c
,FirmWeightedGrade__c,ProcessWeightedGrade__c,StrategyWeightedGrade__c,TeamWeightedGrade__c,PerformanceWeightedGrade__c,TrackRecordWeightedGrade__c,PerformanceConsistencyWeightedGrade__c
FROM GPScout_Scorecard__c 
WHERE Id IN (@MostRecentScorecardIds)
',
FirstSelectQuery='
SELECT GPScout_Scorecard__c, Id,Expertise_Aligned_with_Strategy__c,History_Together__c,Complementary_Skills__c,Team_Depth__c,Value_Add_Resources__c,Investment_Thesis__c
,Competitive_Advantage__c,Portfolio_Construction__c,Appropriateness_of_Fund_Size__c,Consistency_of_Strategy__c,Sourcing__c,Due_Diligence__c,Decision_Making__c
,Deal_Execution_Structure__c,Post_Investment_Value_add__c,Team_Stability__c,Ownership_and_Compensation__c,Culture__c,Alignment_with_LPs__c,Terms__c,Scaled_Score__c
,Absolute_Performance__c,Relative_Performance__c,Realized_Performance__c,Depth_of_Track_Record__c,Relevance_of_Track_Record__c,Loss_Ratio_Analysis__c,Unrealized_Portfolio__c
,RCP_Value_Creation_Analysis__c,RCP_Hits_Misses__c,Deal_Size__c,Strategy__c,Time__c,Team__c,Qualitative_Final_Grade__c,Quantitative_Final_Grade__c,Quantitative_Final_Number__c,IsDeleted
,Publish_Scorecard__c,GPScout_Scorecard__r.Publish_Level__c,GPScout_Scorecard__r.IsDeleted,Date_Reviewed__c
,FirmWeightedGrade__c,ProcessWeightedGrade__c,StrategyWeightedGrade__c,TeamWeightedGrade__c,PerformanceWeightedGrade__c,TrackRecordWeightedGrade__c,PerformanceConsistencyWeightedGrade__c
FROM GPScout_Scorecard__c 
WHERE Id IN (@MostRecentScorecardIds)
'
WHERE ID=5 AND EntityCode = 'SFScorecard'


ALTER TABLE Accounts DROP COLUMN [DateImportedUtc]
ALTER TABLE Accounts DROP COLUMN NewOrganizationId
ALTER TABLE Accounts DROP COLUMN [Include]

ALTER TABLE dbo.Accounts ADD GPScoutLevelCompleted__c nvarchar(50) NULL
ALTER TABLE dbo.Accounts ADD Level_of_Disclosure__c nvarchar(50) NULL

ALTER TABLE dbo.Accounts ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.Accounts ADD NewOrganizationId uniqueidentifier NULL
ALTER TABLE dbo.Accounts ADD [Include] [bit] NULL


ALTER TABLE GPScoutScorecards DROP COLUMN [DateImportedUtc]
ALTER TABLE GPScoutScorecards DROP COLUMN [Include]

ALTER TABLE dbo.GPScoutScorecards ADD FirmWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD ProcessWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD StrategyWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD TeamWeightedGrade__c float NULL

ALTER TABLE dbo.GPScoutScorecards ADD PerformanceWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD TrackRecordWeightedGrade__c float NULL
ALTER TABLE dbo.GPScoutScorecards ADD PerformanceConsistencyWeightedGrade__c float NULL

ALTER TABLE dbo.GPScoutScorecards ADD [DateImportedUtc] [datetime2](7) NULL
ALTER TABLE dbo.GPScoutScorecards ADD [Include] [bit] NULL

--==========================================================================
ALTER TABLE Organization ADD GPScoutLevelCompleted nvarchar(50) NULL
ALTER TABLE Organization ADD LevelOfDisclosure nvarchar(50) NULL

ALTER TABLE Grade ADD FirmWeightedGrade float NULL
ALTER TABLE Grade ADD ProcessWeightedGrade float NULL
ALTER TABLE Grade ADD StrategyWeightedGrade float NULL
ALTER TABLE Grade ADD TeamWeightedGrade float NULL
ALTER TABLE Grade ADD PerformanceWeightedGrade float NULL
ALTER TABLE Grade ADD TrackRecordWeightedGrade float NULL
ALTER TABLE Grade ADD PerformanceConsistencyWeightedGrade float NULL
--==========================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		  05/04/2017 - Most_Recent_Fund_Regional_Specialist__c added to the import logic
--					 - Access_Constrained__c replaced with Access_Constrained_Firm__c
--					 - Populate KeyTakeAway with Overview__c (KeyTakeAway not used anymore)
--					 - Industry_Focus__c removed, not used anymore
--					 - Most_Recent_Fund_Sub_Region__c removed, not used anymore
--		- 06/06/2017 - (CASE Most_Recent_Fund_Fundraising_Status__c WHEN 'Pledge Fund - Active' THEN 'Fundless Sponsor'
--					 - InactiveFirm logic added
--		- 06/28/2017 - adjustment (CASE WHEN Most_Recent_Fund_Fundraising_Status__c IN ('Pledge Fund - Active', 'Pledge Fund - Inactive') THEN 'Fundless Sponsor' ELSE Most_Recent_Fund_Fundraising_Status__c END) Most_Recent_Fund_Fundraising_Status__c, -- Most_Recent_Fund_Fundraising_Status__c moved to SP SF_PostImport
--		- 10/04/2017 - GPScoutLevelCompleted and LevelOfDisclosure fields included
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportAccounts]
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_Regional_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_Regional_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_Regional_Specialist__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_SBIC__c NOT IN ('No','Yes') AND Most_Recent_Fund_SBIC__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_SBIC__c column.', 16, 1);
	END
	IF EXISTS(SELECT NULL FROM Accounts WHERE Most_Recent_Fund_is_Sector_Specialist__c NOT IN ('No','Yes') AND Most_Recent_Fund_is_Sector_Specialist__c IS NOT NULL) BEGIN
		RAISERROR(N'Value other than ''No'' or ''Yes'' or NULL detected in Accounts.Most_Recent_Fund_is_Sector_Specialist__c column.', 16, 1);
	END

	BEGIN TRAN

	UPDATE A
	SET A.Include = CASE WHEN IsDeleted = 0 AND Publish_Level__c IS NOT NULL THEN 1 ELSE 0 END
	FROM dbo.Accounts A

	UPDATE A SET NewOrganizationId = NEWID()
	FROM dbo.Accounts A
	WHERE NOT EXISTS (SELECT NULL FROM main_Organization Target WHERE Target.SFID = A.Id)
		AND A.Include=1

	INSERT main_Entity (Id, EntityType)
	SELECT Source.NewOrganizationId, 1
	FROM dbo.Accounts Source 
	WHERE Source.NewOrganizationId IS NOT NULL AND Source.NewOrganizationId NOT IN (SELECT Id FROM main_Entity WHERE EntityType=1) --AND Include=1
	

	MERGE main_Organization WITH (HOLDLOCK) AS Target
	USING (SELECT 
		ISNULL(Access_Constrained_Firm__c, 0) AS Access_Constrained_Firm__c,
		Alias__c,
		AUM_Calc__c,
		BillingCountry,
		Co_Investment_Opportunities_for_Fund_LP__c,
		Co_Investment_Opportunities_for_Non_LPs__c,
		DateImportedUtc,
		ISNULL(Emerging_Manager__c, 0) AS Emerging_Manager__c,
		Evaluation_Text__c,
		Expected_Next_Fundraise__c,
		Firm_History__c,
		--Grade_Qualitative_Text__c,
		--Grade_Quantitative_Text__c,
		--Grade_Scatterchart_Text__c,
		A.Id,
		--Industry_Focus__c,
		Investment_Thesis__c,
		--Key_Takeaway__c,
		GPScout_Last_Updated__c,
		Most_Recent_Fund_Eff_Size__c,
		Most_Recent_Fund_Fundraising_Status__c,
		Most_Recent_Fund_Industry_Focus__c,
		(CASE Most_Recent_Fund_is_Sector_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_is_Sector_Specialist__c,
		Most_Recent_Fund_Market_Stage__c,
		Most_Recent_Fund_Primary_Strategy__c,
		Most_Recent_Fund_Region__c,
		(CASE Most_Recent_Fund_Regional_Specialist__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_Regional_Specialist__c,
		(CASE Most_Recent_Fund_SBIC__c WHEN 'Yes' THEN 1 ELSE 0 END) Most_Recent_Fund_SBIC__c,
		Most_Recent_Fund_Secondary_Strategy__c,
		--Most_Recent_Fund_Sub_Region__c,
		A.Name,
		NewOrganizationId,
		Overview__c,
		-- if Account has been deleted or unpublished in SF then we unpublish it in GPScout
		(CASE Include WHEN 1 THEN Publish_Level__c ELSE '' END) Publish_Level__c,
		--ISNULL(Radar_List__c, 0) AS Radar_List__c,
		(CAST(CASE WHEN FirmGrade__c='A' OR FirmGrade__c='B' THEN 1 ELSE 0 END AS BIT)) AS FirmGrade__c,
		(CASE FirmGrade__c WHEN 'Inactive' THEN 1 ELSE 0 END) InactiveFirm,
		Region__c,
		Team_Overview__c,
		Total_Closed_Funds__c,
		Track_Record_Text__c,
		Website,
		YearGPFounded__c,
		[Include], -- initialized in UPDATE above
		C.Id CurrencyId,
		GPScout_Phase_Assigned__c,
		GPScout_Phase_Completed__c,
		GPScoutLevelCompleted__c GPScoutLevelCompleted,
		Level_of_Disclosure__c LevelOfDisclosure
		--A.RecordTypeName,
		--A.ParentId
		FROM dbo.Accounts A
		LEFT JOIN main_Currency C ON A.Most_Recent_Fund_Currency__c = C.Name
	) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED /*AND Source.[Include] = 1*/ THEN UPDATE SET 
		-- Grade.Scatterchart_Text__c = Source.Scatterchart_Text__c, -- Handled in SF_PostImport
		Target.RegionalSpecialist = Source.Most_Recent_Fund_Regional_Specialist__c,
		Target.AccessConstrained = Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Target.Address1Country = Source.BillingCountry,
		Target.AliasesPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Target.Aum = Source.AUM_Calc__c,
		Target.CoInvestWithExistingLPs = Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Target.CoInvestWithOtherLPs = Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Target.DateImportedUtc = Source.DateImportedUtc,
		Target.EmergingManager = Source.Emerging_Manager__c,
		Target.PublishLevelAsImported = Source.Publish_Level__c,
		Target.EvaluationText = Source.Evaluation_Text__c,
		Target.ExpectedNextFundRaise = Source.Expected_Next_Fundraise__c,
		--Target.FocusList = Source.Radar_List__c,
		Target.FocusList = Source.FirmGrade__c,
		Target.History = Source.Firm_History__c,
		Target.InvestmentRegionPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Target.InvestmentThesis = Source.Investment_Thesis__c,
		Target.KeyTakeAway = Source.Overview__c, --Source.Key_Takeaway__c,
		Target.LastFundSize = Source.Most_Recent_Fund_Eff_Size__c,
		Target.LastUpdated = Source.GPScout_Last_Updated__c,
		Target.MarketStage = Source.Most_Recent_Fund_Market_Stage__c,
		Target.Name = Source.Name,
		Target.NumberOfFunds = Source.Total_Closed_Funds__c,
		Target.OrganizationOverview = Source.Overview__c,
		--Target.QualitativeGrade = Source.Grade_Qualitative_Text__c,	-- Handled in SF_PostImport
		--Target.QuantitativeGrade = Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Target.SBICFund = Source.Most_Recent_Fund_SBIC__c,
		Target.SectorFocusPipeDelimited = dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Target.SectorSpecialist = Source.Most_Recent_Fund_is_Sector_Specialist__c,
		Target.StrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--Target.SubRegionsPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		Target.SubStrategyPipeDelimited = dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Target.FundRaisingStatus = Source.Most_Recent_Fund_Fundraising_Status__c,
		Target.TeamOverview = Source.Team_Overview__c,
		Target.TrackRecordText = Source.Track_Record_Text__c,
		Target.WebsiteUrl = Source.Website,
		Target.YearFounded = Source.YearGPFounded__c,
		Target.CurrencyId = Source.CurrencyId,
		Target.GPScoutPhaseAssigned = Source.GPScout_Phase_Assigned__c,
		Target.GPScoutPhaseCompleted = Source.GPScout_Phase_Completed__c,
		--Target.RecordTypeName = Source.RecordTypeName,
		--Target.ParentSFID = Source.ParentId,
		Target.InactiveFirm = Source.InactiveFirm,
		Target.GPScoutLevelCompleted = Source.GPScoutLevelCompleted,
		Target.LevelOfDisclosure = Source.LevelOfDisclosure

	WHEN NOT MATCHED BY TARGET AND (Source.[Include] = 1 /*OR Source.RecordTypeName = 'Fund Manager Parent'*/) THEN INSERT(
		SFID,
		RegionalSpecialist,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		AccessConstrained,
		Address1Country,
		AliasesPipeDelimited,
		Aum,
		CoInvestWithExistingLPs,
		CoInvestWithOtherLPs,
		DateImportedUtc,
		EmergingManager,
		PublishLevelAsImported,
		EvaluationText,
		ExpectedNextFundRaise,
		FocusList,
		History,
		Id,
		InvestmentRegionPipeDelimited,
		InvestmentThesis,
		KeyTakeAway,
		LastFundSize,
		LastUpdated,
		MarketStage,
		Name,
		NumberOfFunds,
		OrganizationOverview,
		--QualitativeGrade,
		--QuantitativeGrade,
		SBICFund,
		SectorFocusPipeDelimited,
		SectorSpecialist,
		StrategyPipeDelimited,
		--SubRegionsPipeDelimited,
		SubStrategyPipeDelimited,
		FundRaisingStatus,
		TeamOverview,
		TrackRecordText,
		WebsiteUrl,
		YearFounded,
		CurrencyId,
		GPScoutPhaseAssigned,
		GPScoutPhaseCompleted,
		--RecordTypeName,
		--ParentSFID
		InactiveFirm,
		GPScoutLevelCompleted,
		LevelOfDisclosure
	) 
	VALUES(
		Source.Id,
		Source.Most_Recent_Fund_Regional_Specialist__c,
		-- Grade_Scatterchart_Text__c, -- Handled in SF_PostImport
		Source.Access_Constrained_Firm__c, --Source.Access_Constrained__c,
		Source.BillingCountry,
		dbo.udf_ToPipeDelimitedValue(Source.Alias__c, ';'),
		Source.AUM_Calc__c,
		Source.Co_Investment_Opportunities_for_Fund_LP__c,
		Source.Co_Investment_Opportunities_for_Non_LPs__c,
		Source.DateImportedUtc,
		Source.Emerging_Manager__c,
		Source.Publish_Level__c,
		Source.Evaluation_Text__c,
		Source.Expected_Next_Fundraise__c,
		--Source.Radar_List__c,
		Source.FirmGrade__c,
		Source.Firm_History__c,
		Source.NewOrganizationId,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Region__c, ','),
		Source.Investment_Thesis__c,
		Source.Overview__c, -- Source.Key_Takeaway__c,
		Source.Most_Recent_Fund_Eff_Size__c,
		Source.GPScout_Last_Updated__c,
		Source.Most_Recent_Fund_Market_Stage__c,
		Source.Name,
		Source.Total_Closed_Funds__c,
		Source.Overview__c,
		--Source.Grade_Qualitative_Text__c,		-- Handled in SF_PostImport
		--Source.Grade_Quantitative_Text__c,	-- Handled in SF_PostImport
		Source.Most_Recent_Fund_SBIC__c,
		dbo.udf_ToPipeDelimitedValue( Source.Most_Recent_Fund_Industry_Focus__c, ';'),
		Source.Most_Recent_Fund_is_Sector_Specialist__c,
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Primary_Strategy__c, ','),
		--dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Sub_Region__c, ','),
		dbo.udf_ToPipeDelimitedValue(Source.Most_Recent_Fund_Secondary_Strategy__c, ','),
		Source.Most_Recent_Fund_Fundraising_Status__c,
		Source.Team_Overview__c,
		Source.Track_Record_Text__c,
		Source.Website,
		Source.YearGPFounded__c,
		Source.CurrencyId,
		Source.GPScout_Phase_Assigned__c,
		Source.GPScout_Phase_Completed__c,
		--Source.RecordTypeName,
		--Source.ParentId
		Source.InactiveFirm,
		Source.GPScoutLevelCompleted,
		Source.LevelOfDisclosure
	)
--	WHEN MATCHED AND Source.[Include] = 0 THEN -- Organizations are not deleted for now. They can have user created detail info attached
		--DELETE
		;

	UPDATE O
		SET PublishLevelID = CASE
			WHEN A.Include = 0 THEN 0
			WHEN A.Publish_Level__c LIKE '0 -%' THEN 1
			WHEN A.Publish_Level__c LIKE '1 -%' THEN 2
			WHEN A.Publish_Level__c LIKE '2 -%' THEN 3
			WHEN A.Publish_Level__c LIKE '3 -%' THEN 4
			WHEN A.Publish_Level__c LIKE '2.5 -%' THEN 5
			ELSE 0
		END
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	UPDATE O
		SET 
			EvaluationLevel = CASE PublishLevelID
				WHEN 2 THEN 'Initial Assessment'
				WHEN 3 THEN 'Preliminary Evaluation'
				WHEN 4 THEN 'Evaluation'
				WHEN 5 THEN 'Evaluation'
				ELSE ''
			END,
			DiligenceLevel = CASE PublishLevelID
				WHEN 2 THEN 33
				WHEN 3 THEN 66
				WHEN 4 THEN 100
				WHEN 5 THEN 66
				ELSE ''
			END,
			PublishSearch = CASE WHEN PublishLevelID>=1 THEN 1 ELSE 0 END,
			PublishOverviewAndTeam = CASE WHEN PublishLevelID>=2 THEN 1 ELSE 0 END,
			PublishTrackRecord = CASE WHEN PublishLevelID>=3 THEN 1 ELSE 0 END,
			PublishProfile = CASE WHEN PublishLevelID>=4 THEN 1 ELSE 0 END,
			FocusRadar = CASE WHEN O.FocusList=1 THEN 'Focus List' ELSE '' END--,
			--ParentId = NULL
	FROM Accounts A
	JOIN main_Organization O ON A.Id = O.SFID

	-- set parent-child relationship
	--UPDATE ChildOrganization
	--SET ChildOrganization.ParentId = ParentOrganization.Id
	--FROM Accounts A
	--JOIN alexabe_01_stage.dbo.Organization ChildOrganization ON A.Id = ChildOrganization.SFID
	--JOIN alexabe_01_stage.dbo.Organization ParentOrganization ON A.ParentId = ParentOrganization.SFID

	-- remove child items of unpublished Organizations
	SELECT Id INTO #UnpublishedOrgs FROM main_Organization WHERE PublishLevelID=0

	DELETE E FROM main_Evaluation E JOIN #UnpublishedOrgs O ON E.OrganizationId=O.Id 
	DELETE G FROM main_Grade G JOIN #UnpublishedOrgs O ON G.OrganizationId=O.Id 
	DELETE M FROM main_OrganizationMember M JOIN #UnpublishedOrgs O ON M.OrganizationId=O.Id 
	--DELETE M FROM main_OrganizationMember M WHERE NOT EXISTS (SELECT NULL FROM main_Organization O WHERE O.Id=M.OrganizationId AND O.PublishLevelID>0)
	DELETE A FROM main_OtherAddress A JOIN #UnpublishedOrgs O ON A.OrganizationId=O.Id 
	DELETE T FROM main_TrackRecord T JOIN #UnpublishedOrgs O ON T.OrganizationId=O.Id
	DELETE F FROM main_Fund F JOIN #UnpublishedOrgs O ON F.OrganizationId=O.Id

	DROP TABLE #UnpublishedOrgs

	DELETE main_ResearchPriorityOrganization
	INSERT main_ResearchPriorityOrganization (
			[Id]
           ,[Name]
           ,[MarketStage]
           ,[StrategyPipeDelimited]
           ,[InvestmentRegionPipeDelimited])
     SELECT
           Id
           ,Name
           ,MarketStage
           ,StrategyPipeDelimited
           ,InvestmentRegionPipeDelimited
	FROM main_Organization
	WHERE ISNULL(GPScoutPhaseAssigned, '') != ISNULL(GPScoutPhaseCompleted, '') AND PublishLevelAsImported!=''

	COMMIT TRAN
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/04/2017 - FirmWeightedGrade, ProcessWeightedGrade, StrategyWeightedGrade, TeamWeightedGrade,
--					   PerformanceWeightedGrade, TrackRecordWeightedGrade, PerformanceConsistencyWeightedGrade fields included
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportGPScoutScorecards]
AS
BEGIN
	SET NOCOUNT ON;

	-- removing Grades/Scorecards from organizations new Scorecard data has been imported for
	DELETE G
	FROM main_Grade G
	JOIN main_Organization O ON G.OrganizationId = O.Id
	WHERE O.SFID IN (SELECT GPScout_Scorecard__c FROM Ids WHERE GPScout_Scorecard__c IS NOT NULL)


	MERGE main_Grade WITH (HOLDLOCK) AS Target
	USING (SELECT 
		dbo.udf_GetGrade([Expertise_Aligned_with_Strategy__c]) AS [Expertise_Aligned_with_Strategy__c]
		,dbo.udf_GetGrade([History_Together__c]) AS [History_Together__c]
		,dbo.udf_GetGrade([Complementary_Skills__c]) AS [Complementary_Skills__c]
		,dbo.udf_GetGrade([Team_Depth__c]) AS [Team_Depth__c]
		,dbo.udf_GetGrade([Value_Add_Resources__c]) AS [Value_Add_Resources__c]
		,dbo.udf_GetGrade([Investment_Thesis__c]) AS [Investment_Thesis__c]
		,dbo.udf_GetGrade([Competitive_Advantage__c]) AS [Competitive_Advantage__c]
		,dbo.udf_GetGrade([Portfolio_Construction__c]) AS [Portfolio_Construction__c]
		,dbo.udf_GetGrade([Appropriateness_of_Fund_Size__c]) AS [Appropriateness_of_Fund_Size__c]
		,dbo.udf_GetGrade([Consistency_of_Strategy__c]) AS [Consistency_of_Strategy__c]
		,dbo.udf_GetGrade([Sourcing__c]) AS [Sourcing__c]
		,dbo.udf_GetGrade([Due_Diligence__c]) AS [Due_Diligence__c]
		,dbo.udf_GetGrade([Decision_Making__c]) AS [Decision_Making__c]
		,dbo.udf_GetGrade([Deal_Execution_Structure__c]) AS [Deal_Execution_Structure__c]
		,dbo.udf_GetGrade([Post_Investment_Value_add__c]) AS [Post_Investment_Value_add__c]
		,dbo.udf_GetGrade([Team_Stability__c]) AS [Team_Stability__c]
		,dbo.udf_GetGrade([Ownership_and_Compensation__c]) AS [Ownership_and_Compensation__c]
		,dbo.udf_GetGrade([Culture__c]) AS [Culture__c]
		,dbo.udf_GetGrade([Alignment_with_LPs__c]) AS [Alignment_with_LPs__c]
		,dbo.udf_GetGrade([Terms__c]) AS [Terms__c]
		,s.[Scaled_Score__c]
		,dbo.udf_GetGrade([Absolute_Performance__c]) AS [Absolute_Performance__c]
		,dbo.udf_GetGrade([Relative_Performance__c]) AS [Relative_Performance__c]
		,dbo.udf_GetGrade([Realized_Performance__c]) AS [Realized_Performance__c]
		,dbo.udf_GetGrade([Depth_of_Track_Record__c]) AS [Depth_of_Track_Record__c]
		,dbo.udf_GetGrade([Relevance_of_Track_Record__c]) AS [Relevance_of_Track_Record__c]
		,dbo.udf_GetGrade([Loss_Ratio_Analysis__c]) AS [Loss_Ratio_Analysis__c]
		,dbo.udf_GetGrade([Unrealized_Portfolio__c]) AS [Unrealized_Portfolio__c]
		,dbo.udf_GetGrade([RCP_Value_Creation_Analysis__c]) AS [RCP_Value_Creation_Analysis__c]
		,dbo.udf_GetGrade([RCP_Hits_Misses__c]) AS [RCP_Hits_Misses__c]
		,dbo.udf_GetGrade([Deal_Size__c]) AS [Deal_Size__c]
		,dbo.udf_GetGrade([Strategy__c]) AS [Strategy__c]
		,dbo.udf_GetGrade([Time__c]) AS [Time__c]
		,dbo.udf_GetGrade([Team__c]) AS [Team__c]
		  --,s.[Qualitative_Final_Grade__c]
		  --,s.[Quantitative_Final_Grade__c]
		  --,s.[Quantitative_Final_Number__c]
		  ,s.[Date_Reviewed__c]
		  ,s.[DateImportedUtc]
		  ,s.[Id]
		  ,o.[Id] AS [OrganizationId]
		  ,[Include] = CASE WHEN IsDeleted = 0 AND Publish_Scorecard__c = 1 AND ParentScorecard_Publish_Level__c IS NOT NULL AND ParentScorecard_IsDeleted = 0 THEN 1 ELSE 0 END
		  ,s.FirmWeightedGrade__c		FirmWeightedGrade
		  ,s.ProcessWeightedGrade__c	ProcessWeightedGrade
		  ,s.StrategyWeightedGrade__c	StrategyWeightedGrade
		  ,s.TeamWeightedGrade__c		TeamWeightedGrade
		  ,s.PerformanceWeightedGrade__c PerformanceWeightedGrade
		  ,s.TrackRecordWeightedGrade__c TrackRecordWeightedGrade
		  ,s.PerformanceConsistencyWeightedGrade__c PerformanceConsistencyWeightedGrade
	  FROM [dbo].[GPScoutScorecards] s
	  LEFT JOIN main_Organization o ON s.GPScout_Scorecard__c = o.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.ExpertiseAlignedWithStrategy = Source.[Expertise_Aligned_with_Strategy__c],
		Target.HistoryTogether = Source.[History_Together__c],
		Target.ComplementarySkills = Source.[Complementary_Skills__c],
		Target.TeamDepth = Source.[Team_Depth__c],
		Target.ValueAddResources = Source.[Value_Add_Resources__c],
		Target.InvestmentThesis = Source.[Investment_Thesis__c],
		Target.CompetitiveAdvantage = Source.[Competitive_Advantage__c],
		Target.PortfolioConstruction = Source.[Portfolio_Construction__c],
		Target.AppropriatenessOfFundSize = Source.[Appropriateness_of_Fund_Size__c],
		Target.ConsistencyOfStrategy = Source.[Consistency_of_Strategy__c],
		Target.Sourcing = Source.[Sourcing__c],
		Target.DueDiligence = Source.[Due_Diligence__c],
		Target.DecisionMaking = Source.[Decision_Making__c],
		Target.DealExecutionStructure = Source.[Deal_Execution_Structure__c],
		Target.PostInvestmentValueAdd = Source.[Post_Investment_Value_add__c],
		Target.TeamStability = Source.[Team_Stability__c],
		Target.OwnershipAndCompensation = Source.[Ownership_and_Compensation__c],
		Target.Culture = Source.[Culture__c],
		Target.AlignmentWithLPs = Source.[Alignment_with_LPs__c],
		Target.Terms = Source.[Terms__c],
		--Target. = Source.[Scaled_Score__c], -- Organization.QualitativeGradeNumber = Source.[Scaled_Score__c] -- Handled in SF_PostImport 
		Target.AbsolutePerformance = Source.[Absolute_Performance__c],
		Target.RelativePerformance = Source.[Relative_Performance__c],
		Target.RealizedPerformance = Source.[Realized_Performance__c],
		Target.DepthOfTrackRecord = Source.[Depth_of_Track_Record__c],
		Target.RelevanceOfTrackRecord = Source.[Relevance_of_Track_Record__c],
		Target.LossRatioAnalysis = Source.[Loss_Ratio_Analysis__c],
		Target.UnrealizedPortfolio = Source.[Unrealized_Portfolio__c],
		Target.AtlasValueCreationAnalysis = Source.[RCP_Value_Creation_Analysis__c],
		Target.AtlasHitsAndMisses = Source.[RCP_Hits_Misses__c],
		Target.DealSize = Source.[Deal_Size__c],
		Target.Strategy = Source.[Strategy__c],
		Target.[Time] = Source.[Time__c],
		Target.Team = Source.[Team__c],
		--Target. = Source.[Qualitative_Final_Grade__c], -- seems to be Organization.QualitativeGrade			-- Handled in SF_PostImport
		--Target. = Source.[Quantitative_Final_Grade__c], -- seems to be Organization.QuantitativeGrade			-- Handled in SF_PostImport
		--Target. = Source.[Quantitative_Final_Number__c]  -- seems to be Organization.QuantitativeGradeNumber	-- Handled in SF_PostImport
		Target.[DateReviewed] = Source.[Date_Reviewed__c],
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.[OrganizationId] = Source.[OrganizationId],
		Target.FirmWeightedGrade = Source.FirmWeightedGrade,
		Target.ProcessWeightedGrade = Source.ProcessWeightedGrade,
		Target.StrategyWeightedGrade = Source.StrategyWeightedGrade,
		Target.TeamWeightedGrade = Source.TeamWeightedGrade,
		Target.PerformanceWeightedGrade = Source.PerformanceWeightedGrade,
		Target.TrackRecordWeightedGrade = Source.TrackRecordWeightedGrade,
		Target.PerformanceConsistencyWeightedGrade = Source.PerformanceConsistencyWeightedGrade

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id,
			ExpertiseAlignedWithStrategy,
			HistoryTogether,
			ComplementarySkills,
			TeamDepth,
			ValueAddResources,
			InvestmentThesis,
			CompetitiveAdvantage,
			PortfolioConstruction,
			AppropriatenessOfFundSize,
			ConsistencyOfStrategy,
			Sourcing,
			DueDiligence,
			DecisionMaking,
			DealExecutionStructure,
			PostInvestmentValueAdd,
			TeamStability,
			OwnershipAndCompensation,
			Culture,
			AlignmentWithLPs,
			Terms,
			AbsolutePerformance,
			RelativePerformance,
			RealizedPerformance,
			DepthOfTrackRecord,
			RelevanceOfTrackRecord,
			LossRatioAnalysis,
			UnrealizedPortfolio,
			AtlasValueCreationAnalysis,
			AtlasHitsAndMisses,
			DealSize,
			Strategy,
			[Time],
			Team,
			[DateReviewed],
			[DateImportedUtc],
			[SFID],
			[OrganizationId],
			FirmWeightedGrade,
			ProcessWeightedGrade,
			StrategyWeightedGrade,
			TeamWeightedGrade,
			PerformanceWeightedGrade,
			TrackRecordWeightedGrade,
			PerformanceConsistencyWeightedGrade
	) 
		VALUES(	NEWID(),
				Source.[Expertise_Aligned_with_Strategy__c],
				Source.[History_Together__c],
				Source.[Complementary_Skills__c],
				Source.[Team_Depth__c],
				Source.[Value_Add_Resources__c],
				Source.[Investment_Thesis__c],
				Source.[Competitive_Advantage__c],
				Source.[Portfolio_Construction__c],
				Source.[Appropriateness_of_Fund_Size__c],
				Source.[Consistency_of_Strategy__c],
				Source.[Sourcing__c],
				Source.[Due_Diligence__c],
				Source.[Decision_Making__c],
				Source.[Deal_Execution_Structure__c],
				Source.[Post_Investment_Value_add__c],
				Source.[Team_Stability__c],
				Source.[Ownership_and_Compensation__c],
				Source.[Culture__c],
				Source.[Alignment_with_LPs__c],
				Source.[Terms__c],
				Source.[Absolute_Performance__c],
				Source.[Relative_Performance__c],
				Source.[Realized_Performance__c],
				Source.[Depth_of_Track_Record__c],
				Source.[Relevance_of_Track_Record__c],
				Source.[Loss_Ratio_Analysis__c],
				Source.[Unrealized_Portfolio__c],
				Source.[RCP_Value_Creation_Analysis__c],
				Source.[RCP_Hits_Misses__c],
				Source.[Deal_Size__c],
				Source.[Strategy__c],
				Source.[Time__c],
				Source.[Team__c],
				Source.[Date_Reviewed__c],
				Source.[DateImportedUtc],
				Source.[Id],
				Source.[OrganizationId],
				Source.FirmWeightedGrade,
				Source.ProcessWeightedGrade,
				Source.StrategyWeightedGrade,
				Source.TeamWeightedGrade,
				Source.PerformanceWeightedGrade,
				Source.TrackRecordWeightedGrade,
				Source.PerformanceConsistencyWeightedGrade
		)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END
