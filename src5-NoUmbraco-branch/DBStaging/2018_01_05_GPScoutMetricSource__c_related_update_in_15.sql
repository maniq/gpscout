﻿SELECT *
INTO SF_EntityImportConfig_2018_01_05_before_GPScoutMetricSource__c_related_update_in_15
FROM SF_EntityImportConfig
--AND GPScoutMetricSource__c NOT IN ('''',''N/A'') 

UPDATE SF_EntityImportConfig
SET SelectQuery = '
SELECT AIMPortMgmt__Fund__c
FROM AIMPortMgmt__Investment_Metric__c 
WHERE (AIMPortMgmt__Account__r.Id=NULL OR (AIMPortMgmt__Account__r.Publish_Level__c!=NULL AND AIMPortMgmt__Account__r.IsDeleted=false)) 
AND AIMPortMgmt__Fund__r.Publish__c=true AND AIMPortMgmt__Fund__r.IsDeleted=false
AND ((LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported) 
OR (AIMPortMgmt__Account__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Account__r.LastModifiedDate < @DateImported) OR (AIMPortMgmt__Fund__r.LastModifiedDate >= @LastModifiedDate AND AIMPortMgmt__Fund__r.LastModifiedDate < @DateImported))
GROUP BY AIMPortMgmt__Fund__c
',
FirstSelectQuery = NULL
WHERE ID=15 AND EntityCode='SFFundMetricRefFund'