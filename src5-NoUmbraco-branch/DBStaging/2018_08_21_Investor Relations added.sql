﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- History:
--		- 10/16/2017 - [PublishDetail__c] added
--		- 04/30/2018 - [Publish_Contact__c] removed and [PublishDetail__c] related logic updated
--		- 08/21/2018 - PublishDetail__c = 'Investor Relations' option added
-- =============================================
ALTER PROCEDURE [dbo].[SF_ImportContacts]
AS
BEGIN

	SET NOCOUNT ON;

	MERGE [main_OrganizationMember] WITH (HOLDLOCK) AS Target
	USING (SELECT 
		  C.[Phone]
		  ,CAST(C.[Email] AS NVARCHAR(50)) AS [Email]
		  ,C.[Title]
		  ,C.[Year_Started_With_Current_Firm__c]
		  ,C.[Bio__c]
		  ,CAST(C.MailingStreet AS NVARCHAR(128)) AS MailingStreet
		  ,C.MailingCity
		  ,CAST([MailingState] AS NVARCHAR(50)) AS MailingState
		  ,C.MailingPostalCode
		  ,CAST([MailingCountry] AS NVARCHAR(50)) AS MailingCountry
		  ,C.[DateImportedUtc]
		  ,C.[Id]
		  ,O.Id OrganizationId
		  ,[Include] = CASE WHEN IsDeleted = 0 AND PublishDetail__c IS NOT NULL AND PublishDetail__c!='' AND  Display_Order__c>0 AND ParentAccount_Publish_Level__c IS NOT NULL AND ParentAccount_IsDeleted = 0 THEN 1 ELSE 0 END
		  ,C.FirstName
		  ,C.LastName
		  ,C.Display_Order__c
		  ,CASE C.[PublishDetail__c]
				WHEN 'Senior - Key' THEN 10
				WHEN 'Senior - Lead' THEN 20
				WHEN 'Mid Level' THEN 30
				WHEN 'Operating' THEN 40
				WHEN 'Investor Relations' THEN 50
				ELSE NULL
			END [PublishDetail]
	  FROM [dbo].[Contacts] C
	  LEFT JOIN [main_Organization] O ON C.AccountId = O.SFID
	  ) AS Source
	ON Target.SFID = Source.Id

	WHEN MATCHED AND Source.[Include] = 1 THEN UPDATE SET 
		Target.[Phone] = Source.[Phone],
		Target.[Email] = Source.[Email],
		Target.[JobTitle] = Source.[Title],
		Target.[YearsAtOrganization] = Source.[Year_Started_With_Current_Firm__c],
		Target.[Description] = Source.[Bio__c],
		Target.[Address1] = Source.MailingStreet,
		Target.[City] = Source.MailingCity,
		Target.[StateOrProvince] = Source.MailingState,
		Target.[PostalCode] = Source.MailingPostalCode,
		Target.[Country] = Source.MailingCountry,
		Target.[DateImportedUtc] = Source.[DateImportedUtc],
		Target.OrganizationId = Source.OrganizationId,
		Target.FirstName = Source.FirstName,
		Target.LastName = Source.LastName,
		Target.DisplayOrder = Source.Display_Order__c,
		Target.PublishDetail = Source.PublishDetail

	WHEN NOT MATCHED AND Source.[Include] = 1 THEN INSERT(
			Id
			,[Phone]
			,[Email]
			,[JobTitle]
			,[YearsAtOrganization]
			,[Description]
			,[Address1]
			,[City]
			,[StateOrProvince]
			,[PostalCode]
			,[Country]
			,[DateImportedUtc]
			,[SFID]
			,OrganizationId
			,FirstName
			,LastName
			,DisplayOrder
			,PublishDetail
	) 
		VALUES(	
				NEWID()
				,Source.[Phone]
				,Source.[Email]
				,Source.[Title]
				,Source.[Year_Started_With_Current_Firm__c]
				,Source.[Bio__c]
				,Source.MailingStreet
				,Source.MailingCity
				,Source.MailingState
				,Source.MailingPostalCode
				,Source.MailingCountry
				,Source.[DateImportedUtc]
				,Source.Id
				,Source.OrganizationId
				,Source.FirstName
				,Source.LastName
				,Source.Display_Order__c
				,PublishDetail
				)
	WHEN MATCHED AND Source.[Include] = 0 THEN 
		DELETE;

END