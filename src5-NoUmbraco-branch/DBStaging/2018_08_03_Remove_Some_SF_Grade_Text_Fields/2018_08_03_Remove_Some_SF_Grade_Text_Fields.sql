﻿-- In main DB:
-- Remove Grade.AQScorecardText and Grade.QoRScorecardText
ALTER TABLE dbo.Grade DROP COLUMN AQScorecardText
ALTER TABLE dbo.Grade DROP COLUMN QoRScorecardText
GO

-- Helper DB:
-- Removing Grade_Qualitative_Text__c and Grade_Quantitative_Text__c 
select * from SF_EntityImportConfig where SelectQuery like '%Grade_Qualitative_Text__c%' or FirstSelectQuery like '%Grade_Qualitative_Text__c%'
or SelectQuery like '%Grade_Quantitative_Text__c%' or FirstSelectQuery like '%Grade_Quantitative_Text__c%'

select * INTO SF_EntityImportConfig_2018_08_02_before_Grade_Quantitative_Text__c_removal
FROM SF_EntityImportConfig

UPDATE SF_EntityImportConfig SET
SelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c
, Total_Closed_Funds__C, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE LastModifiedDate >= @LastModifiedDate AND LastModifiedDate < @DateImported
',
FirstSelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c
, Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id
, Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Industry_Focus__c, Most_Recent_Fund_is_Sector_Specialist__c
, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, Most_Recent_Fund_Fundraising_Status__c
, Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C
, Track_Record_Text__c, Website, YearGPFounded__c,IsDeleted, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c
, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL
'
WHERE ID=3 AND EntityCode = 'SFAccount'


UPDATE SF_EntityImportConfig SET
SelectQuery = '
SELECT GPScout_Phase_Assigned__c,
(SELECT Id FROM GPScout_Qualitative_Scorecards__r WHERE Publish_Scorecard__c=true AND IsDeleted=false ORDER BY Date_Reviewed__c DESC LIMIT 1),
Most_Recent_Fund_Currency__c,Access_Constrained_Firm__c, Alias__c, AUM_Calc__c, BillingCountry, Co_Investment_Opportunities_for_Fund_LP__c, Co_Investment_Opportunities_for_Non_LPs__c, 
Emerging_Manager__c, Evaluation_Text__c, Expected_Next_Fundraise__c, Firm_History__c, Grade_Scatterchart_Text__c, Id, 
Investment_Thesis__c, ProfileUpdateNeeded__c, Most_Recent_Fund_Eff_Size__c, Most_Recent_Fund_Fundraising_Status__c, Most_Recent_Fund_Industry_Focus__c, 
Most_Recent_Fund_is_Sector_Specialist__c, Most_Recent_Fund_Market_Stage__c, Most_Recent_Fund_Primary_Strategy__c, Most_Recent_Fund_Region__c, Most_Recent_Fund_SBIC__c, 
Most_Recent_Fund_Secondary_Strategy__c, Name, Overview__c, ParentId, Publish_Level__c, FirmGrade__c, Region__c, Team_Overview__c, Total_Closed_Funds__C, 
Track_Record_Text__c, Website, YearGPFounded__c, Most_Recent_Fund_Regional_Specialist__c, GPScoutLevelCompleted__c, LevelofDisclosureContent__c, GPS_Current_Record_Date__c
FROM Account 
WHERE IsDeleted = false AND Publish_Level__c!=NULL AND Id IN (@AdditionalIds)
'
WHERE ID=13 AND EntityCode = 'SFAdditionalAccounts'


ALTER TABLE dbo.Accounts DROP COLUMN Grade_Qualitative_Text__c
ALTER TABLE dbo.Accounts DROP COLUMN Grade_Quantitative_Text__c
GO

-- UPDATE SP [dbo].[SF_PostImport]:
--		08/03/2018 SFP ZK	- Removing columns AQScorecardText and QoRScorecardText from Grade table
UPDATE G
SET
	G.ScatterchartText = A.Grade_Scatterchart_Text__c
	-- G.AQScorecardText = A.Grade_Qualitative_Text__c,
	-- G.QoRScorecardText = A.Grade_Quantitative_Text__c
FROM [main_Grade] G
JOIN main_Organization O ON G.OrganizationId = O.Id
JOIN dbo.Accounts A ON O.SFID = A.Id





