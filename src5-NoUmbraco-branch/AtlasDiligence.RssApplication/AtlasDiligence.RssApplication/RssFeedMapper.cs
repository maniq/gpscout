﻿using System;
using System.Web;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.RssApplication
{
    public static class RssFeedMapper
    {
        public static NewsItem NewsItemFromSyndicationItem(SyndicationItem item, string contentNode, Guid? sourceId, string sourceName, DateTime dateCreated)
        {
            var content = string.Empty;
            var summary = string.Empty;
            if (item.Content != null)
            {
                content = ((TextSyndicationContent)item.Content).Text;
            }
            if (item.Summary != null)
            {
                summary = item.Summary.Text;
            }
            if (String.IsNullOrEmpty(content))
            {
                foreach (var extension in item.ElementExtensions)
                {
                    if (extension.OuterName == contentNode)
                    {
                        content = extension.GetObject<XElement>().Value;
                    }
                }
            }
            var result = new NewsItem
                             {
                                 PublishedDate = item.PublishDate.DateTime >= DateTime.Now ? DateTime.Now : item.PublishDate.DateTime,
                                 Subject = item.Title.Text,
                                 Content = StripHtml(content),
                                 Summary = StripHtml(summary),
                                 //Url = item.Id ?? (item.Links.Count > 0 ? item.Links.FirstOrDefault().Uri.ToString() : "No URL provided"),
                                 Url = (item != null && item.Links != null && item.Links.Any()) ? item.Links.FirstOrDefault().Uri.ToString() : "No URL provided",
                                 OrganizationSourceId = sourceId,
                                 OrganizationSourceName = sourceName,
                                 DateCreated = dateCreated
                             };

            return result;
        }

        private static string StripHtml(string text)
        {
            var result = HttpUtility.HtmlDecode(Regex.Replace(text, @"<(.|\n)*?>", string.Empty))
                .Replace("\r", "")
                .Replace("\n", "")
                .Replace("\t", "")
                .Replace(Environment.NewLine, "");
            //.Replace("&#8211;", "-")
            //.Replace("&#8212;", "--")
            //.Replace("&#8216;", "'")
            //.Replace("&#8217;", "'")
            //.Replace("&#8218;", ",")
            //.Replace("&#8220;", "\"")
            //.Replace("&#8221;", "\"")
            //.Replace("&#8222;", ",,")
            //.Replace("&#8230;", "...")
            //.Replace("&#8482;", "tm")
            //.Replace("&amp;", "&");
            return result;
        }
    }
}
