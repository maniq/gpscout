﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Xml;
using AtlasDiligence.Common.Data.Repositories;
using log4net;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Implementation.Services;

namespace AtlasDiligence.RssApplication
{
    public static class RssReaderFacade
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RssReaderFacade));
        private class FeedItemContentHolder
        {
            public Guid? FeedSource { get; set; }
            public string FeedSourceName { get; set; }
            public string ContentNode { get; set; }
            public string FeedAddress { get; set; }
            public SyndicationItem FeedItem { get; set; }
        } 

        /// <summary>
        /// Returns news items from RSS feeds.
        /// </summary>
        /// <returns></returns>
        public static IList<NewsItem> GetNewsItems()
        {
            Log.Debug("Getting news feeds.");
            var result = new List<NewsItem>();
            var rssService = new RssFeedService();
            List<FeedItemContentHolder> rssFeeds;
            try
            {
                var rssFeedsList = rssService.GetAllFeeds();
                rssFeeds = rssFeedsList
                    .Select(m => new FeedItemContentHolder
                                     {
                                         FeedSource = m.SourceOrganizationId,
                                         FeedSourceName = m.SourceOrganizationName,
                                         //ContentNode = m.Content,
                                         FeedAddress = m.Url
                                     })
                    .ToList();
            }
            catch (Exception)
            {
                Log.Fatal("Error retrieving RSS feeds.");
                throw;
            }

            DateTime now = DateTime.Now;
            foreach (var newsItem in AggregateFeedItems(rssFeeds))
            {
                result.Add(RssFeedMapper.NewsItemFromSyndicationItem(newsItem.FeedItem, newsItem.ContentNode,
                                                                     newsItem.FeedSource, newsItem.FeedSourceName, now));
            }
            return result;
        }

        private static IEnumerable<FeedItemContentHolder> AggregateFeedItems(List<FeedItemContentHolder> rssFeeds)
        {
            var lastPublishedItem = new NewsRepository().GetMostRecentPublishedDate();
            if(lastPublishedItem >= DateTime.Now || lastPublishedItem == DateTime.MinValue)
            {
                lastPublishedItem = DateTime.Now.AddDays(-1);
                Log.Debug("Setting last published date to yesterday.");
            }
            var items = new List<FeedItemContentHolder>();
            Log.Debug("Reading " + rssFeeds.Where(f => !String.IsNullOrEmpty(f.FeedAddress)).Count() + " feeds and aggregating.");
            foreach (var f in rssFeeds.Where(f => !String.IsNullOrEmpty(f.FeedAddress)))
            {
                Log.Debug("    Reading feed " + f.FeedAddress);
                try
                {
                    using (XmlReader feedReader = XmlReader.Create(new Uri(f.FeedAddress).AbsoluteUri))
                    {
                        SyndicationFeed feed = SyndicationFeed.Load(feedReader);
                        if (feed != null)
                        {
                            items =
                                items.Union(
                                    feed.Items.Where(m=>m.PublishDate > lastPublishedItem).Select(
                                        m =>
                                        new FeedItemContentHolder
                                            {
                                                ContentNode = f.ContentNode, 
                                                FeedAddress = f.FeedAddress, 
                                                FeedItem = m,
                                                FeedSource = f.FeedSource,
                                                FeedSourceName = f.FeedSourceName
                                            })).
                                    ToList();
                        }
                    }
                } 
                catch(Exception ex)
                {
                    Log.Error("-----Error reading feed " + f.FeedAddress);
                }
            }
            return items.OrderByDescending(i => i.FeedItem.PublishDate).ToList();
        }
    }
}
