﻿using GPScout.Salesforce.Contracts;
using RestSharp;
using SalesforceSharp;
using SalesforceSharp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using _SalesforceClient = SalesforceSharp.SalesforceClient;

namespace GPScout.Salesforce.Services
{
    public class SalesforceClient: ISalesforceClient
    {
        #region Private
        private _SalesforceClient _client;
        private readonly ISalesforceInstanceInfo _instanceInfo;
        private readonly ISalesforceAuthenticationInfo _authenticationInfo;
        #endregion

        #region Protected
        protected _SalesforceClient Client
        {
            get
            {
                if (_client == null || !_client.IsAuthenticated)
                {
                    _client = new _SalesforceClient();
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                        if (!string.IsNullOrWhiteSpace(_instanceInfo.ApiVersion)) _client.ApiVersion = _instanceInfo.ApiVersion;
                        else _instanceInfo.ApiVersion = _client.ApiVersion;

                        var authFlow = new UsernamePasswordAuthenticationFlow(_authenticationInfo.ConsumerKey, _authenticationInfo.ConsumerSecret, _authenticationInfo.UserName, _authenticationInfo.Password);
                        authFlow.TokenRequestEndpointUrl = _instanceInfo.TokenRequestUrl;

                        //authFlow.TokenRequestEndpointUrl = "https://test.salesforce.com";
                        _client.Authenticate(authFlow);
                    }
                    catch (SalesforceException sfex)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                return _client;
            }
        }
        #endregion

        #region Ctor
        public SalesforceClient(ISalesforceInstanceInfo instanceInfo, ISalesforceAuthenticationInfo authenticationInfo)
        {
            _instanceInfo = instanceInfo;
            _authenticationInfo = authenticationInfo;
        }
        #endregion

        public IEnumerable<T> Query<T>(string query, string altUrl = "", RestRequest request = null) where T: new()
        {
            return Client.Query<T>(query, altUrl, request);
        }

        public ISalesforceInstanceInfo SalesforceInstanceInfo
        {
            get { return _instanceInfo; }
        }

        public ISalesforceInstanceInfo InstanceInfo
        {
            get {
                if (_client == null) _client = Client;
                return _instanceInfo;
            }
        }
    }
}
