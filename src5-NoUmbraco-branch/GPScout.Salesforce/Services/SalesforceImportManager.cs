﻿using AtlasDiligence.Common.Data.General;
using GPScout.DataImport.Contracts.Models;
using GPScout.DataImport.Contracts.Repositories;
using GPScout.DataImport.Contracts.Services;
using GPScout.Domain.Contracts.Enums;
using GPScout.Domain.Contracts.Models;
using GPScout.Salesforce.Contracts;
using GPScout.Salesforce.Data;
using GPScout.Salesforce.DTOs;
using RestSharp;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Account = GPScout.Salesforce.DTOs.Account;
using Fund = GPScout.Salesforce.DTOs.Fund;

//12/12/2019
namespace GPScout.Salesforce.Services
{
    public enum SFEntityType : int
    {
        SFAccount,
        SFBenchmark,
        SFFirmAddress,
        SFScorecard,
        SFContact,
        SFFund,
        SFFundMetric,
        SFStrengthAndConcern,
        SFTrackRecord,
        SFAdditionalAccounts,
        SFGPScoutScorecardRefAccount,
        SFFundMetricRefFund,
        SFAdditionalFunds,
        //SFFundMetricIDsRCP,
        //SFFundMetricIDsPreqin,
        //SFFundMetricsPreqinAndRCPNotMostRecent,
        SFFundMetricIDsReferencedByAccountCurrentDate,
        SFIDsOfUpdatedFundMetrics,
        SFMetricsFromMetricIDsReferencedByAccountCurrentDate
    }

    public class SalesforceImportManager : ISalesforceImportManager
    {
        #region Private members
        private readonly ISalesforceClient _salesforceClient;
        private readonly IAppContext _appContext;
        private readonly IRepository _targetRepository;
        private readonly ILog _log;
        private readonly IResults _messages;
        private DateTime _dateImported;
        private DateTime _importFromDate;
        private string _importFromDateStr;
        private string _dateImportedStr;
        private GPScoutSFImportStagingEntities _stagingContext;

        #endregion

        #region CTor

        public SalesforceImportManager(
            ILog log,
            ISalesforceClient salesforceClient,
            IRepository targetRepository,
            IAppContext appContext
            )
        {
            _log = log;
            _salesforceClient = salesforceClient;
            _targetRepository = targetRepository;
            _appContext = appContext;
            _messages = new Results();
        }
        #endregion

        #region SalesforceImportManager
        public DateTime ImportFromDate
        {
            get { return _importFromDate; }
            set
            {
                _importFromDate = value;
                _importFromDateStr = _importFromDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
        }

        public DateTime DateImported
        {
            get { return _dateImported; }
            set
            {
                _dateImported = value;
                _dateImportedStr = _dateImported.ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
        }


        public GPScoutSFImportStagingEntities StagingDb
        {
            get
            {
                if (_stagingContext == null) _stagingContext = new GPScoutSFImportStagingEntities();
                return _stagingContext;
            }
        }

        public bool Enabled()
        {
            return true;
        }


        public IResults StartImport(bool forceStart, bool fullImport, out bool dataImported, bool onlyImportToStage)
        {
            bool __dataImported = false;

            //Aniq Test
            //forceStart = true;

            try
            {
                LogStart(fullImport);

                _targetRepository.TruncateStagingTables();

                _targetRepository.Select<SFEntityImportConfig>().OrderBy(x => x.OrderBy).ToList().ForEach(entity =>
                {
                    bool _dataImported;
                    Import(entity, fullImport, out _dataImported);
                    if (!__dataImported) __dataImported = _dataImported;
                });

                Log(ResultCode.DataImport_Salesforce_to_GPScout_Staging_Finished);

                if (!onlyImportToStage)
                {
                    if (__dataImported)
                    {
                        Log(ResultCode.DataImport_Starting_Import_Into_GPScout);
                        PostImport(1);
                    }
                    else
                    {
                        Log(ResultCode.DataImport_No_Data_Imported_From_Salesforce);
                    }
                }
            }
            catch (Exception ex)
            {
                _messages.Add(ex);
                _log.Write(ex);
            }
            dataImported = __dataImported;
            return _messages;
        }
        #endregion

        #region Helper Methods

        void PostImport(int n)
        {
            try
            {
                _log.Write(string.Format("Calling PostImport SP, attempt {0}...", n));
                _targetRepository.PostImport(DateImported);
                _log.Write("PostImport SP execution completed successfully.");
            }
            catch (Exception ex)
            {
                _log.Write(string.Format("PostImport SP call attempt {0} failed.", n));
                _log.Write(ex.ToString());
                if (n <= _appContext.SalesForceMaxRetryAttempts)
                {
                    Thread.Sleep(999);
                    PostImport(++n);
                }
            }
        }

        delegate void ProcessDelegate(object inputData);

        private void Import(SFEntityImportConfig entityInfo, bool fullImport, out bool dataImported)
        {
            //Aniq Test
            bool debug1 = false;

            dataImported = false;
            string query = "", _selectQuery = null, idsParameterName = "";
            string[] Ids = null;
            var entityFriendlyName = entityInfo.EntityFriendlyName;
            ProcessDelegate preProcessSourceData = null;
            RestRequest request = null;
            try
            {
                if (!fullImport && !string.IsNullOrWhiteSpace(entityInfo.SelectQuery))
                {
                    // Injecting actual parameters into the query here and/or below in the loop
                    _selectQuery = entityInfo.SelectQuery
                    .Replace("@LastModifiedDate", _importFromDateStr)
                    .Replace("@DateImported", _dateImportedStr);
                }
                else if (!string.IsNullOrWhiteSpace(entityInfo.FirstSelectQuery))
                {
                    _selectQuery = entityInfo.FirstSelectQuery
                        .Replace("@DateImported", _dateImportedStr);
                }

                // no query to execute
                if (_selectQuery == null)
                    return;

                SFEntityType entityType = (SFEntityType)Enum.Parse(typeof(SFEntityType), entityInfo.EntityCode, true);

                Type entityTypeInfo;

                switch (entityType)
                {
                    case SFEntityType.SFAccount:
                        entityTypeInfo = typeof(Account);
                        break;
                    case SFEntityType.SFAdditionalAccounts:
                        Ids = _targetRepository.GetAdditionalAccountIdsClause().ToArray();
                        idsParameterName = "@AdditionalIds";
                        entityTypeInfo = typeof(Account);
                        break;
                    case SFEntityType.SFBenchmark:
                        entityTypeInfo = typeof(Benchmark);
                        break;
                    case SFEntityType.SFFirmAddress:
                        entityTypeInfo = typeof(FirmAddress);
                        break;
                    case SFEntityType.SFScorecard:
                        Ids = StagingDb.Accounts.Where(x => x.MostRecentScorecardSFID != null).Select(x => x.MostRecentScorecardSFID).ToArray();
                        idsParameterName = "@MostRecentScorecardIds";
                        entityTypeInfo = typeof(GPScoutScorecard);
                        break;
                    case SFEntityType.SFContact:
                        entityTypeInfo = typeof(Contact);
                        break;
                    case SFEntityType.SFFund:
                        entityTypeInfo = typeof(Fund);
                        break;
                    case SFEntityType.SFFundMetric:
                        Ids = StagingDb.Funds.Where(x => x.MostRecentMetricSFID != null).Select(x => x.MostRecentMetricSFID).ToArray();
                        idsParameterName = "@MostRecentMetricIds";
                        entityTypeInfo = typeof(GPScout.Salesforce.DTOs.FundMetric);
                        break;
                    case SFEntityType.SFStrengthAndConcern:
                        entityTypeInfo = typeof(StrengthAndConcern);
                        break;
                    case SFEntityType.SFTrackRecord:
                        entityTypeInfo = typeof(TrackRecord);
                        break;
                    case SFEntityType.SFGPScoutScorecardRefAccount:
                    case SFEntityType.SFFundMetricRefFund:
                        entityTypeInfo = typeof(ID);
                        break;
                    case SFEntityType.SFAdditionalFunds:
                        Ids = StagingDb.Ids.Where(x => x.AIMPortMgmt__Fund__c != null).Select(x => x.AIMPortMgmt__Fund__c)
                            .Except(StagingDb.Funds.Select(f => f.Id))
                            .ToArray();
                        entityTypeInfo = typeof(Fund);
                        idsParameterName = "@AdditionalFundIds";
                        break;
                    //case SFEntityType.SFFundMetricIDsRCP:
                    //    // will return IDs of Most Recent Metrics of publish type RCP, for funds having Most Recent Metric of publish type Preqin
                    //    var lastMetricsOfTypePrequin = StagingDb.FundMetrics.Where(fm => fm.Public_Metric_Source__c == FundMetricSource.Preqin).Select(fm => fm.Id).ToArray();
                    //    Ids = StagingDb.Funds.Where(x => x.MostRecentMetricSFID != null && lastMetricsOfTypePrequin.Contains(x.MostRecentMetricSFID)).Select(x => x.Id).ToArray();
                    //    idsParameterName = "@FundIDsOfFundsWithMostRecentPreqinMetrics";
                    //    entityTypeInfo = typeof(ID);
                    //    break;
                    //case SFEntityType.SFFundMetricIDsPreqin:
                    //    // will return IDs of Most Recent Metrics of publish type Preqin, for funds having Most Recent Metric of publish type RCP
                    //    var lastMetricsOfTypeRCP = StagingDb.FundMetrics.Where(fm => fm.Public_Metric_Source__c == FundMetricSource.RCP).Select(fm => fm.Id).ToArray();
                    //    Ids = StagingDb.Funds.Where(x => x.MostRecentMetricSFID != null && lastMetricsOfTypeRCP.Contains(x.MostRecentMetricSFID)).Select(x => x.Id).ToArray();
                    //    idsParameterName = "@FundIDsOfFundsWithMostRecentRCPMetrics";
                    //    entityTypeInfo = typeof(ID);

                    //    // Give a hint which property AIMPortMgmt__Investment_Metrics__r data should be translated into
                    //    preProcessSourceData = (srcData) => {
                    //        (srcData as IEnumerable<ID>).ToList().ForEach(id => id.FieldSelectionHint = FieldSelectionHint.MostRecentPreqinFundMetricId);
                    //    };
                    //    break;
                    //case SFEntityType.SFFundMetricsPreqinAndRCPNotMostRecent:
                    //    Ids = StagingDb.Ids.Where(x => x.MostRecentRCPFundMetricId != null).Select(x => x.MostRecentRCPFundMetricId)
                    //            .Union(StagingDb.Ids.Where(x => x.MostRecentPreqinFundMetricId != null).Select(x => x.MostRecentPreqinFundMetricId))
                    //            .ToArray();
                    //    idsParameterName = "@FurtherFundMetricIds";
                    //    entityTypeInfo = typeof(GPScout.Salesforce.DTOs.FundMetric);
                    //    break;
                    case SFEntityType.SFFundMetricIDsReferencedByAccountCurrentDate:
                        var dates = StagingDb.Accounts.Where(a => a.GPS_Current_Record_Date__c != null).Select(a => a.GPS_Current_Record_Date__c.Value).Distinct().ToArray();
                        Ids = dates.Select(x => x.ToString("yyyy-MM-dd")).ToArray();
                        idsParameterName = "@AccountCurrentRecordDate";
                        entityTypeInfo = typeof(GPScout.Salesforce.DTOs.ID);
                        // Give a hint which property AIMPortMgmt__Investment_Metrics__r data should be translated into
                        preProcessSourceData = (srcData) => {
                            (srcData as IEnumerable<ID>).ToList().ForEach(id => id.FieldSelectionHint = FieldSelectionHint.IdOfMetricRefByAccountCurrentRecordDate);
                        };
                        break;
                    case SFEntityType.SFIDsOfUpdatedFundMetrics:
                        entityTypeInfo = typeof(GPScout.Salesforce.DTOs.ID);
                        // Give a hint which property AIMPortMgmt__Investment_Metrics__r data should be translated into
                        preProcessSourceData = (srcData) => {
                            (srcData as IEnumerable<ID>).ToList().ForEach(id => id.FieldSelectionHint = FieldSelectionHint.IdOfUpdatedFundMetric);
                        };
                        break;
                    case SFEntityType.SFMetricsFromMetricIDsReferencedByAccountCurrentDate:
                        Ids = ((StagingDb.Ids.Where(x => x.IdOfMetricRefByAccountCurrentRecordDate != null).Select(x => x.IdOfMetricRefByAccountCurrentRecordDate)
                                .Union(StagingDb.Ids.Where(x => x.IdOfUpdatedFundMetric != null).Select(x => x.IdOfUpdatedFundMetric))
                                )
                                .Except(StagingDb.FundMetrics.Select(fm => fm.Id)))
                                .Distinct()
                                .ToArray();
                        idsParameterName = "@FurtherFundMetricIds";
                        entityTypeInfo = typeof(GPScout.Salesforce.DTOs.FundMetric);
                        break;
                    default:
                        throw new Exception(string.Format("Unknown entity type: {0}", entityType));
                }

                if (Ids != null && !Ids.Any()) return;


                LogInfo(string.Format("Going to read data from SF for {0}...", entityFriendlyName));
                

                var queryMethod = _salesforceClient.GetType().GetMethod("Query");
                queryMethod = queryMethod.MakeGenericMethod(entityTypeInfo);

                var bulkInsertMethod = _targetRepository.GetType().GetMethod("BulkInsert");
                bulkInsertMethod = bulkInsertMethod.MakeGenericMethod(entityTypeInfo);

                var syncResults = new DataSyncResult();
                int total = Ids == null ? 1 : Ids.Count();
                int skip = 0, rowsRead = 0;
                int SFRequestTimeoutMS = _appContext.SalesForceRequestTimeoutSeconds * 1000;
                //syncResults.Total = total;
                while (skip < total)
                {
                    // Inject actual parameters into the query here too, if needed
                    if (Ids != null)
                    {
                        if (entityInfo.SelectPageSize < 1)
                            throw new Exception(string.Format("SelectPageSize cannot be less than 1 for {0}", entityFriendlyName));

                        query = _selectQuery.Replace(idsParameterName, GetINClause(Ids, skip, entityInfo.SelectPageSize, entityType != SFEntityType.SFFundMetricIDsReferencedByAccountCurrentDate));
                    }
                    else
                        query = _selectQuery;

                    if (debug1)
                        LogInfo(string.Format("Query to debug {0}", query));

                    string uri = entityInfo.SourceUri;
                    if (String.IsNullOrEmpty(uri))
                        // services/data/v29.0/queryAll?q=
                        uri = string.Format("services/data/{0}/queryAll", _salesforceClient.InstanceInfo.ApiVersion);

                    request = new RestRequest();
                    request.ReadWriteTimeout = request.Timeout = SFRequestTimeoutMS;

                    IEnumerable<object> sourceData = null;
                    int n = 1;
                    while (true)
                    {
                        try
                        {
                            sourceData = queryMethod.Invoke(_salesforceClient, new object[] { query, uri, request }) as IEnumerable<object>;
                            break;
                        }
                        catch (Exception exRead)
                        {
                            if (n > _appContext.SalesForceMaxRetryAttempts)
                                throw;
                            LogStagingError(exRead, entityType.ToString(), "Reading attempt from SF " + n, request, false);
                            Thread.Sleep(n * 1000);
                            n++;
                        }
                    }

                    int _rowsRead = (sourceData != null) ? sourceData.Count() : 0;
                    dataImported = _rowsRead > 0;
                    rowsRead += _rowsRead;

                    LogCloudRead(entityFriendlyName, rowsRead);

                    if (Ids == null)
                    {
                        if (!dataImported) break;
                        skip += _rowsRead;
                    }
                    else
                    {
                        skip += entityInfo.SelectPageSize;
                    }

                    if (preProcessSourceData != null)
                        preProcessSourceData(sourceData);

                    object _syncResults = null;
                    n = 1;
                    while (true)
                    {
                        try
                        {
                            _syncResults = bulkInsertMethod.Invoke(_targetRepository, new object[] { sourceData, entityInfo.TargetDTO });
                            break;
                        }
                        catch (Exception exRead)
                        {
                            if (n > _appContext.SalesForceMaxRetryAttempts)
                                throw;
                            LogStagingError(exRead, entityType.ToString(), "Writing attempt to Staging DB " + n, request, false);
                            Thread.Sleep(n * 1000);
                            n++;
                        }
                    }

                    syncResults = syncResults + (_syncResults as DataSyncResult);

                    LogStagingWrite(entityFriendlyName, syncResults);
                }

                switch (entityType)
                {
                    case SFEntityType.SFFundMetric:
                        // after the most recent fund metrics have been imported, we mark these fund metrics
                        // as ImportStep = 1.
                        // This way we can distinct between metrics imported during this step vs further 
                        // metrics imported later
                        StagingDb.SF_ImportHelper(ImportHelperSteps.SetFundMetricsImportStep, 1);
                        break;
                }
            }
            catch (Exception ex)
            {
                LogStagingError(ex, entityFriendlyName, /*sourceData*/null, request, true);
                throw;
            }
        }

        private string GetINClause(IEnumerable<string> Ids, int skip, int pageSize, bool addQuotes)
        {
            List<string> ids = Ids.Skip(skip).Take(pageSize).ToList();
            string format = addQuotes ? "'{0}'"  : "{0}";
            return ids.Any() ? string.Join(",", ids.Select(id => string.Format(format, id))) : string.Empty;
        }

        /// <summary>
        /// if considerAsError is true then the Result will be added to the ResultSet as a real error otherwise just as a warning.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="entityName"></param>
        /// <param name="source"></param>
        /// <param name="restRequest"></param>
        /// <param name="considerAsError"></param>
        private void LogStagingError(Exception ex, string entityName, object source, RestRequest restRequest, bool considerAsError)
        {
            if (considerAsError)
                _messages.Add(ResultCode.DataImport_StagingError, entityName);
            else
                _messages.Add(ResultCode.InfoMsg, source);
                //TO DO: _messages.Add(ResultCode.WarningMsg, source);

            _log.Write(ex);
            if (source == null)
            {
                var r = _messages.Add(ResultCode.DataImport_EntityNotTouchedInStaging, entityName);
                _log.Write(r.Message, r.ResultType.ToString());
            }
            if (restRequest != null)
            {
                var r = _messages.Add(ResultCode.InfoMsg, string.Format("RestClient Get Attempts made: {0}", restRequest.Attempts));
                _log.Write(r.Message, r.ResultType.ToString());
            }
        }


        private void LogStagingWrite(string entityName, DataSyncResult syncResults)
        {
            //var r = _messages.Add(ResultCode.DataImport_DataWrittenToStaging, syncResults.Updated, entityName, syncResults.Inserted);
            var r = _messages.Add(ResultCode.DataImport_DataWrittenToStagingInsertsOnly, entityName, syncResults.Inserted);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        private void LogCloudRead(string entityName, int rowsRead)
        {
            var r = _messages.Add(ResultCode.DataImport_DataReadFromSource, rowsRead, entityName);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        private void LogInfo(string message)
        {
            var r = _messages.Add(ResultCode.InfoMsg, message);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        private void Log(ResultCode code)
        {
            var r = _messages.Add(code);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        private void LogStart(bool fullImport)
        {
            IResult result = fullImport ?
                _messages.Add(ResultCode.DataImport_FullImport_From_Salesforce_Started, this.DateImported.ToUniversalTime(), this.DateImported.ToLocalTime())
                : _messages.Add(ResultCode.DataImport_From_Salesforce_Started, this.ImportFromDate, this.DateImported.ToUniversalTime(), this.ImportFromDate.ToLocalTime(), this.DateImported.ToLocalTime());

            _log.Write(result.Message, result.ResultType.ToString());
        }
        #endregion
    }


    struct ImportHelperSteps
    {
        public const string SetFundMetricsImportStep = "SetFundMetricsImportStep";
    }
}
