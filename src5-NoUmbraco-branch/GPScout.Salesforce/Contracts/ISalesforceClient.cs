﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.Contracts
{
    public interface ISalesforceClient
    {
        ISalesforceInstanceInfo InstanceInfo { get; }
        IEnumerable<T> Query<T>(string query, string altUrl = "", RestRequest request = null)
            where T: new();
    }
}
