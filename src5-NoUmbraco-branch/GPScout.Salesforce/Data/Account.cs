//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPScout.Salesforce.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Account
    {
        public string Id { get; set; }
        public string Website { get; set; }
        public string Most_Recent_Fund_Industry_Focus__c { get; set; }
        public string YearGPFounded__c { get; set; }
        public Nullable<decimal> Most_Recent_Fund_Eff_Size__c { get; set; }
        public string Alias__c { get; set; }
        public Nullable<decimal> AUM_Calc__c { get; set; }
        public Nullable<bool> Emerging_Manager__c { get; set; }
        public string Co_Investment_Opportunities_for_Non_LPs__c { get; set; }
        public string Overview__c { get; set; }
        public string Investment_Thesis__c { get; set; }
        public string Team_Overview__c { get; set; }
        public string Key_Takeaway__c { get; set; }
        public string Grade_Scatterchart_Text__c { get; set; }
        public string Track_Record_Text__c { get; set; }
        public string Evaluation_Text__c { get; set; }
        public Nullable<decimal> Total_Closed_Funds__c { get; set; }
        public string Most_Recent_Fund_Market_Stage__c { get; set; }
        public string Region__c { get; set; }
        public string Industry_Focus__c { get; set; }
        public Nullable<System.DateTime> Expected_Next_Fundraise__c { get; set; }
        public string Co_Investment_Opportunities_for_Fund_LP__c { get; set; }
        public string Firm_History__c { get; set; }
        public string Most_Recent_Fund_Region__c { get; set; }
        public string Most_Recent_Fund_Regional_Specialist__c { get; set; }
        public string Most_Recent_Fund_Primary_Strategy__c { get; set; }
        public string Publish_Level__c { get; set; }
        public string Most_Recent_Fund_Secondary_Strategy__c { get; set; }
        public Nullable<System.DateTime> ProfileUpdateNeeded__c { get; set; }
        public Nullable<bool> Access_Constrained_Firm__c { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public string BillingCountry { get; set; }
        public string Most_Recent_Fund_SBIC__c { get; set; }
        public string Most_Recent_Fund_is_Sector_Specialist__c { get; set; }
        public string Most_Recent_Fund_Sub_Region__c { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string Most_Recent_Fund_Currency__c { get; set; }
        public string Most_Recent_Fund_Fundraising_Status__c { get; set; }
        public string MostRecentScorecardSFID { get; set; }
        public string MostRecentFundCountries { get; set; }
        public string GPScout_Phase_Assigned__c { get; set; }
        public string FirmGrade__c { get; set; }
        public string GPScoutLevelCompleted__c { get; set; }
        public string LevelofDisclosureContent__c { get; set; }
        public Nullable<System.DateTime> GPS_Current_Record_Date__c { get; set; }
        public Nullable<System.DateTime> DateImportedUtc { get; set; }
        public Nullable<System.Guid> NewOrganizationId { get; set; }
        public Nullable<bool> Include { get; set; }
    }
}
