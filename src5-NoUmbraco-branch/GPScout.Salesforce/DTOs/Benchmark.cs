﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Benchmark
    {
        public string Id { get; set; }
        public DateTime? Effective_Date__c { get; set; }
        public decimal? IRR_Median__c { get; set; }
        public decimal? TVPI_Median__c { get; set; }
        public decimal? DPI_Median__c { get; set; }
        public decimal? IRR_Upper_Quartile__c { get; set; }
        public decimal? TVPI_Upper_Quartile__c { get; set; }
        public decimal? DPI_Upper_Quartile__c { get; set; }
        public decimal? IRR_Lower_Quartile__c { get; set; }
        public decimal? TVPI_Lower_Quartile__c { get; set; }
        public decimal? DPI_Lower_Quartile__c { get; set; }
        public string Provided_By__c { get; set; }
        public string Vintage_Year__c { get; set; }
        public bool IsDeleted { get; set; }
        
        //public DateTime? DateImportedUtc { get; set; }

    }
}
