﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    // Not used right now
    // Could be a possible improvement to use this class instead of dynamic casting of data deserialized by Newtonsoft from Salesforce fields accessed via Salesforce relationships. 
    // E.g. MostRecentScorecardSFID property of the Account DTO class

    //public class IDRecords
    //{
    //    public ID[] records { get; set; }
    //}
}
