﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class GPScoutScorecardRefAccount
    {
        public string GPScout_Scorecard__c { get; set; }
    }
}
