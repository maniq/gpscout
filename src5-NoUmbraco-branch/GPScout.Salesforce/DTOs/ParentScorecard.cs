﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class ParentScorecard
    {
        public bool IsDeleted { get; set; }
        public string Publish_Level__c { get; set; }
    }
}
