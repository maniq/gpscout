﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class FundMetricRefFund
    {
        public string AIMPortMgmt__Fund__c { get; set; }
    }
}
