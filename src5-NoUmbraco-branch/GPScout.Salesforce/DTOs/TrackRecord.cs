﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class TrackRecord
    {
        public string Id { get; set; }
        public string Account__c { get; set; }
        public string Name { get; set; }
        public string Description__c { get; set; }
        public string File_Name__c { get; set; }
        public decimal? Display_Order__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool Publish__c { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return Account__r != null ? Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return Account__r != null ? Account__r.Publish_Level__c : "";
            }
        }

        public string TopGraphDisplayOrder__c { get; set; }

        public string PublishDetail__c { get; set; }

        public ParentAccount Account__r { get; set; }

    }
}
