﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public class YuiDataTableColumn<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        public YuiDataTableColumn(string fieldName)
        {
            this.Settings = new YuiDataTableColumnSettings();
            //default Settings
            this.Settings.Key(fieldName);
            this.Settings.Label(fieldName);

            this.FieldName = fieldName;
        }

        #region Properties

        /// <summary>
        /// Gets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        public string FieldName { get; private set; }

        public YuiDataTableColumnSettings Settings { get; private set; }
        #endregion
    }
}
