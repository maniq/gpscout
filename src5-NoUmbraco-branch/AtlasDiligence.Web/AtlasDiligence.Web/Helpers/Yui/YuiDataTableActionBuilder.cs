﻿using System.Collections.Generic;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public class YuiDataTableActionBuilder<T> where T : class
    {
        private readonly List<YuiDataTableAction<T>> _tableActions = new List<YuiDataTableAction<T>>();

        public YuiDataTableAction<T> AddAction()
        {
            var item = new YuiDataTableAction<T>();
            _tableActions.Add(item);
            return item;
        }

        public YuiDataTableAction<T> AddAction(string id)
        {
            var item = new YuiDataTableAction<T>(id);
            _tableActions.Add(item);
            return item;
        }

        public List<YuiDataTableAction<T>> TableActions
        {
            get
            {
                return this._tableActions;
            }
        }
    }
}
