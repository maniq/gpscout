﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.General
{
    public class PerformanceMonitorModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PreRequestHandlerExecute += delegate(object sender, EventArgs e)
            {
                //Set Page Timer Star
                var requestContext = ((HttpApplication)sender).Context;
                var timer = new Stopwatch();
                requestContext.Items["Timer"] = timer;
                timer.Start();
            };
            context.PostRequestHandlerExecute += delegate(object sender, EventArgs e)
            {

                var httpContext = ((HttpApplication)sender).Context;
                var response = httpContext.Response;
                var timer = (Stopwatch)httpContext.Items["Timer"];
                timer.Stop();

                // Don't interfere with non-HTML responses
                if (response.ContentType == "text/html")
                {
                    var seconds = (double)timer.ElapsedTicks / Stopwatch.Frequency;
                    var resultTime = string.Format("{0:F4} sec ", seconds);
                    RenderQueriesToResponse(response, resultTime);
                }
            };
        }
        void RenderQueriesToResponse(HttpResponse response, string resultTime)
        {
            response.Write("<script>");
            response.Write("if(console){console.log('Page Generation Time - " + resultTime + "');}");
            response.Write("</script>");
            //response.Write("<div style=\"margin: 5px; background-color: #FFFF00; color: #000;\">");
            //response.Write(string.Format("<b>Page Generated in " + result_time));
            //response.Write("</div>");
        }
        public void Dispose() { /* Not needed */ }
    }
}