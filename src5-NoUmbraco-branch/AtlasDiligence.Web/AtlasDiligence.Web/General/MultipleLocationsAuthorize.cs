﻿using AtlanticBT.Common.Types;
using AtlasDiligence.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.General
{
    class MultipleLocationsAuthorize : AuthorizeAttribute
    {
        public bool IsAuthorize(HttpContextBase httpContext)
        {
            return AuthorizeCore(httpContext);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (!AccessHelper.AcessAllowed(filterContext.HttpContext))
            {
                AccessHelper.SignOut(filterContext.HttpContext.Response);
                // auth failed, redirect to login page
                filterContext.Result = new RedirectResult("/Account/Logon");
            }
        }
    }
}
