﻿using System;

namespace AtlasDiligence.Web.General
{
    public class AtlasDiligenceContstants
    {
        public const string UserCache = "USER_ENTITY_CACHE";
        public const string CurrentCompany = "CURRENT_COMPANY_CACHE";

        public const int PagesPerSet = 10;
        public const int RecordsPerPage = 10;
    }
}

