﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace AtlasDiligence.Web.General
{
    public static class JsonExtensions
    {
        public static IHtmlString ToJsonString(this HtmlHelper htmlHelper, object x)
        {
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            string json = JsonConvert.SerializeObject(x, camelCaseFormatter);
            return htmlHelper.Raw(json);
        }
    }
}