﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Repositories;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Web.General
{
    public class EulaAuthorizeAttribute : AuthorizeAttribute
    {
        private const string EulaSessionKey = "eulaSigned";
        public const string EulaRedirectSessionKey = "eulaRedirect";
        public bool IsAuthorize(HttpContextBase httpContext)
        {
            return this.AuthorizeCore(httpContext);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
            {
                return false;
            }
            if (Convert.ToBoolean(httpContext.Session[EulaSessionKey]))
            {
                return true;
            }
            if (ComponentBrokerInstance.RetrieveComponent<IEulaRepository>().HasUserSignedCurrentEula(httpContext.User.Identity.Name))
            {
                httpContext.Session[EulaSessionKey] = true;
                return true;
            }
            var user = EntityCache.Get<AtlasDiligence.Web.Models.UserEntity>(AtlasDiligence.Web.Resources.Params.UserEntityCache);
            if (user == null)
            {
                AccessHelper.SignOut(httpContext.Response);
                httpContext.Response.Redirect("/Account/Logon");
                httpContext.Response.End();
                return true;
            }
            httpContext.Session[EulaRedirectSessionKey] = httpContext.Request.Url.ToString();
            httpContext.Response.Redirect("/Account/TermsAndConditions");
            httpContext.Response.End();
            return true;
        }
    }
}