﻿<%@ Application Codebehind="Global.asax.cs" Inherits="AtlasDiligence.Web.GPScoutApplication" Language="C#" %>
<script runat="server">
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AtlasDiligence.Web.GPScoutApplication));

    //#if DEBUG
    void Session_OnStart() {
        //new AtlasDiligence.Common.DTO.Services.IndexService().ReIndex();
        AccessHelper.OnSessionStart();
    }
    //#endif


    void Application_Error(Object sender, EventArgs e)
    {
        if (Common.IsPrint)
        {
            Exception ex = Server.GetLastError();
            Log.Error("Application_Error IsPrint", ex);
            Server.ClearError();
            return;
        }

        try
        {
            Exception ex = Server.GetLastError();
            Log.Error("Application_Error", ex);

            //Context.Response.Write(ex.ToString());

            foreach (log4net.Appender.IAppender appender in Log.Logger.Repository.GetAppenders())
            {
                var buffered = appender as log4net.Appender.BufferingAppenderSkeleton;
                if (buffered != null)
                {
                    buffered.Flush();
                }
            }

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Error");
            routeData.Values.Add("message", ex.Message);
            IController controller = new AtlasDiligence.Web.Controllers.ErrorController();
            var reqContext = new RequestContext(new HttpContextWrapper(Context), routeData);
            controller.Execute(reqContext);
            Server.ClearError();
        }
        catch (Exception ex) {
            //ex.ToString();
        }
    }
</script>

