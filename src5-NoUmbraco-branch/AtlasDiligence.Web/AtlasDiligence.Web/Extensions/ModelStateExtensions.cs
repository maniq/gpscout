﻿using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Extensions
{
    public static class ModelStateExtensions
    {
        public static void AddModelErrorUI(this ModelStateDictionary modelState, string key, IResults results)
        {
            if (results != null)
            {
                foreach (IResult result in results)
                    modelState.AddModelError(key, UIMessage(result.Message));
            }
        }

        private static string UIMessage(string message)
        {
            if (string.IsNullOrEmpty(message)) return message;

            int n = message.IndexOf(" - ");
            if (n >= 0)
                return message.Substring(n + 3);

            return message;
        }
    }
}
