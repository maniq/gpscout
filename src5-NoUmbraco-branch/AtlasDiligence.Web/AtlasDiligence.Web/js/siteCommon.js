﻿function SiteCommon() {
    var self = this;
    self.checkWidgetContent = function () {
        $('[data-widget-wrapper]:not(:has([data-widget-content]:first))').hide();
    }
}

var siteCommon = new SiteCommon();