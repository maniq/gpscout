﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Web.Config;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.Cors;
using GPScout.Domain.Contracts.Services;
using GPScout.Domain.Implementation.Common;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AtlasDiligence.Web
{
    public class GPScoutApplication : System.Web.HttpApplication
    {
        void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            //GCS Updates
            //System.Configuration.ConfigurationManager.AppSettings["InlineNonce"] = Guid.NewGuid().ToString("N");
            RegisterGlobalFilters(GlobalFilters.Filters);

            RegisterRoutes(RouteTable.Routes);
            ApplicationHelper.RegisterTypes();
            RegisterTypesForWebAppOnly();
            ComponentBrokerInstance.RetrieveComponent<IOrganizationService>().RemoveObsoleteDataItems();
            BundleConfig.RegisterBundles(BundleTable.Bundles);


#if Release
            GlobalFilters.Filters.Add(new RequireHttpsAttribute());
#endif
        }


        void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Basic", action = "Index", id = UrlParameter.Optional }  // Parameter defaults
            );
        }


        void RegisterTypesForWebAppOnly()
        {
            ComponentBrokerInstance.RegisterType<IContactService, ContactService>();
            ComponentBrokerInstance.RegisterType<IFormsAuthenticationService, FormsAuthenticationService>();
            ComponentBrokerInstance.RegisterType<AtlasDiligence.Web.Controllers.Services.IUserService, AtlasDiligence.Web.Controllers.Services.UserService>();
        }

        //GCS Updates
        [AttributeUsage(AttributeTargets.Method)]
        public class ExcludeMySecurityAttribute : Attribute
        {
        }

        //GCS Updates
        // Global CSP filter
        [AllowCrossSite]
        public class ContentSecurityPolicyFilterAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                if (!filterContext.IsChildAction)
                {
                    string actionName = filterContext.ActionDescriptor.ActionName;
                    string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                    if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(ExcludeMySecurityAttribute), true).Any())
                    {
                        // The controller action is decorated with the exclude attribute
                        // so you should probably do nothing here
                    }
                    else if (controllerName != "Organization")
                    {
                        var response = filterContext.HttpContext.Response;


                        string unsafeEval = "";
                        if (controllerName == "Search")
                            unsafeEval = "'unsafe-eval' ";
                        if (controllerName == "Admin" && actionName == "Index")
                            unsafeEval = "'unsafe-eval' ";                        
                        if (controllerName == "Request" && actionName == "RequestOrganization")
                            unsafeEval = "'unsafe-eval' ";
                        if (controllerName == "Organization" && actionName == "Index")
                            unsafeEval = "'unsafe-eval' ";
                        if (controllerName == "CompareTables")
                            unsafeEval = "'unsafe-eval' ";
                        if (controllerName == "Group" && (actionName == "Create" || actionName == "Edit"))
                            unsafeEval = "'unsafe-eval' ";
                        /*
                        if (controllerName == "Account" && actionName == "TermsAndConditions")
                            unsafeEval = "'unsafe-eval' ";
                        */

                        string unsafeInlineJS = "";
                        if (controllerName == "Admin" && (actionName == "Index" || actionName == "UpdateEula" || actionName == "UpdateScoutCategoryDefinitions"))
                            unsafeInlineJS = "'unsafe-inline' ";
                        if (controllerName == "Organization" && actionName == "Index")
                            unsafeInlineJS = "'unsafe-inline' ";
                        if (controllerName == "Search")
                            unsafeInlineJS = "'unsafe-inline' ";
                        if (controllerName == "GroupAccess" && (actionName == "Create" || actionName == "Edit"))
                            unsafeInlineJS = "'unsafe-inline' ";
                        if (controllerName == "Group" && (actionName == "Create" || actionName == "Edit"))
                            unsafeInlineJS = "'unsafe-inline' ";
                        if (controllerName == "Segment" && (actionName == "Create" || actionName == "Edit"))
                            unsafeInlineJS = "'unsafe-inline' ";

                        // By default allow unsafe css
                        string unsafeInlineCSS = "'unsafe-inline' ";
                        if (controllerName == "")
                            unsafeInlineCSS = "";
                        if (controllerName == "Index")
                            unsafeInlineCSS = "";
                        if (controllerName == "Account" && actionName == "LogOn")
                            unsafeInlineCSS = "";
                        

                        string cspGPScout = "default-src 'none'; frame-src 'self' *.rcpadvisors.com *.gpscoutnavigator.com *.google.com; ";
                        cspGPScout += "script-src 'self' ";
                        cspGPScout += unsafeEval;
                        cspGPScout += "*.rcpadvisors.com *.gpscoutnavigator.com *.googleapis.com *.google.com *.gstatic.com *.gtm.com *.github.com *.googletagmanager.com *.analytics.com *.google-analytics.com *.tynt.com *.typekit.net *.mapbox.com ";

                        cspGPScout += unsafeInlineJS;
                        if (unsafeInlineJS == "")
                        {
                            cspGPScout += "'sha256-pBd2D1oUCC/nnW0xv0icKT/R1cmMx1+EW1RyBQMHyv8=' 'sha256-Xy6CuXfIdw8wouMXokv/WeS40wg9DJrivrCNlqhgFEo=' 'sha256-a/lrUTx3w3vLUfnKFDTNrcSx/mmfsCWcRc0rqqFjboQ=' 'sha256-RLkmNbn23MgPL+YM1ZODHiitfhu0wWOGr4WVJbVcPHc=' 'sha256-1xtiB6mV1iIKZ5iz9CxA5lEnfEg8d0XEH3FL9L8NBqo=' 'sha256-BBFgL74HAd/if8MSO8qnRdYp+lC6anj61hiFyn3svco=' 'sha256-Nyy2vSyqh/BZUo5i+uSb41RJ2GcmlC15DkdiO46hyac=' 'sha256-rbZxQnc02mkiuhe1PPRzuHYAFzU3tCfjjGY8kLrWs4Y=' 'sha256-oqL09Rude1cdKBCwc2VXSOpj6APPVcUb1KeTK/iRxCQ=' 'sha256-Z7l0Xqn+ym3k9jMZFZ6tCEnMb3axq1HLz739EgJ3hM4=' 'sha256-hvRztloauADHFqq+8QuGlYvnbgOzvVwjZyDBU/80V7o=' ";

                            if (controllerName == "Account")
                                cspGPScout += "'sha256-tXqrp7jaGQVABN5eMiLeUJ2JftMKyyHQd18Ir69+sAM=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-FNIuARgiFUMyE7NfW1SJW+BLCNRj2ikt/QREV2TdD28=' 'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' 'sha256-Ulho7XKDjcF/xm9coqv5zdJg4gkwm7KE3bSettT9T0c=' 'sha256-gbN97FA3tAXc18Xlrw70PNhxFVMPLhC3qW2ip/BacZc=' 'sha256-deUlMM1KxcuRQlUOX+0R0Zt3cb8Rxu65sUWhJU2dUL0=' 'sha256-5tDZ9NLZ2dJ79VwVWdE8zf/Ujj0RhOBYBXsBbeGGHkE=' 'sha256-m3X/HOjY/AanPOxh1xaRd4t2YFJEHe/V95fVH4XoeU4=' 'sha256-+igmMZwASuopunjMmhJ4sphs0f0ATV+TJ8UO7hDf8wU=' ";
                            else if (controllerName == "Search")
                                cspGPScout += "'sha256-Dqh3AqeEq9q3yJUWFFvsh8mT2EoS6AF4DFuxnYtag7c=' 'sha256-wkWKbs/JvtCXAsotD5p8JuSdxnEIVoxQmDQiGgYCEV0='  'sha256-matwEc6givhWX0+jiSfM1+E5UMk8/UGLdl902bjFBmY=' 'sha256-ZdHxw9eWtnxUb3mk6tBS+gIiVUPE3pGM470keHPDFlE=' 'sha256-pcnxk1mT7NzMD58Smnh3lWwngs8hkn38M8N6LWwGZ3o=' 'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' 'sha256-RaYN8y52zmw5vemlsptIyoLmYT4miU5jw8Fkg4A35ss=' 'sha256-urcVgBi7BLJ4kH38hMZw57qokoCxOlU8vboCM/b8zLE=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-pBd2D1oUCC/nnW0xv0icKT/R1cmMx1+EW1RyBQMHyv8=' 'sha256-oqL09Rude1cdKBCwc2VXSOpj6APPVcUb1KeTK/iRxCQ=' 'sha256-Z7l0Xqn+ym3k9jMZFZ6tCEnMb3axq1HLz739EgJ3hM4=' 'sha256-J6d5gwcxLRTjpYG1Iyh+tDa5xzqP28SfxcwTdY4NEUw=' 'sha256-QGTDW24MFBhAZRv9tHpMqbjL/0VC/0W/zMwVKjlAYz8=' 'sha256-ZL/yk+qKbWSJ93ptnDpzhRPsneVMNzlOZQWsehePuZA=' 'sha256-51BNzX1MFqJWaXjcJfPnrbobyn1b99phMskwipvp1v4=' 'sha256-yRDrrUqqEvxOcNhDRfT4yz6nP/rULobBJbUiirflPZ4=' ";
                            else if (controllerName == "Admin")
                                cspGPScout += "'sha256-YVnn7ZaxpAr9oq8D8D415V6RntuOfrr2pAJtauiyiWQ=' 'sha256-tSTbBaSSbmjmfdETpzxoZmwNqD0fyBWMoTn8O8pfzfI=' 'sha256-QoXj0Ms/3eNCtQeEOiqXRDThBUaLATev9+M2jhPQ6eI=' 'sha256-mcXzQjdJuMVEEezR05MUTkBDUAIq/TvkbU6skiAzI1U=' 'sha256-FNIFr5ua0pMsg9XmqTjsqPvzFBLRRwapBwTery3rk2Y=' 'sha256-XruZdN5KrAsYoMmVdo8z+6S3Eu9lonHIaY8zph28U7A=' 'sha256-NOpqplcoRcD2rNfug++3BBjtUTD9thdk46zbgCBZQX0=' 'sha256-13I75QXb3sBHZ/UaJLFHWNS5uwwjDXiBqElvBTwsKI4=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-hlHCQcTcnzAftiAoHlzCJHUU3Jm1ejN4Gchx60vah6M=' 'sha256-6pynYNIGG+6evyTSJJsNvQ1xKrJgAgo7/5vTwM12q2E=' 'sha256-Ut79aLjs3fC5UtVv26l2r+kyv/4DhifGEM6YG3xXOyo=' 'sha256-xohV/pQB03OWaiilhtRambfR6S8Mb4x11CL8wXA6LkY=' ";
                            else if (controllerName == "Organization")
                                cspGPScout += "   'sha256-mO9VWkmH0srfDKna+WCkjpdEeqDN2uhIY4BERbXdQUI=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-gwo8wY7zWifmmxOhrwmf7jLd0cOEU4/EkfW6s9SHceQ=' 'sha256-cGwhv0TPVlbsc0tTMo8KIAubtAtItRWv1JExGrQQmj0=' 'sha256-TuWVGRLcd3VURkp4ZVHA+oKVyM+jrxWz1RP4dH8a0Ws=' 'sha256-tAw3AusdMzZ/Btr7/YdnV2y4GkD+Q840pWj6MO/6qvQ=' ";
                            else if (controllerName == "Group")
                                cspGPScout += "'sha256-2j150wTQF95K5GU2uKGxfr4Vl5whhSNAqiK2FaYhviU=' ";
                            else if (controllerName == "User")
                                cspGPScout += "'sha256-8umls+9P3OfvvKSsjSm6egfDnDpQQUvKQOSZXtN72hk=' 'sha256-4mebXYLSjDG+P/DfzMa3YOoI80mGSaEL4o1vkWuTXac=' ";
                            else if (controllerName == "Segment")
                                cspGPScout += "";
                            else if (controllerName == "GroupAccess")
                                cspGPScout += "'sha256-NOpqplcoRcD2rNfug++3BBjtUTD9thdk46zbgCBZQX0=' 'sha256-13I75QXb3sBHZ/UaJLFHWNS5uwwjDXiBqElvBTwsKI4=' ";
                            else if (controllerName == "MarketData")
                                cspGPScout += "'sha256-+9a0jl6w6n7X9s9GkpiMcuX0VhN8lvNg+ja4QuBMQU8=' 'sha256-n11fQeI0JhA7dJ3pEEDW0sHD6VpQndVQfMPWv/ECn2I='  'sha256-1bRhvP1obQCW/sQ4UhnuthpeNLRWzsKNbv+v8d0JduY=' 'sha256-ph9YEUuu1jvwFH4k1zfLnsuMIB4MM/l4LlvAmRFtXo0=' 'sha256-xohV/pQB03OWaiilhtRambfR6S8Mb4x11CL8wXA6LkY=' sha256-oouCB+mzzAalym850ikJ6V/KmLz4M8X78lN0dMjHcSE=' ";
                            else if (controllerName == "Folder" || controllerName == "folder")
                                cspGPScout += "'sha256-T21BDMa6c94WrGFQemw6bu8SLsqTf4AyAVXiV+HY3yk=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-XbG2cI9l8bK4Vapot9ysrkvxEhx4iQYm4ItEuTYxO6c=' 'sha256-EogW27EcBQQMXO3e7P0sb4WFsZIjJwy+fLByedG0vnc=' 'sha256-QlXd2u5lfA8iIOtOyg6Sj3XryIzxKsBRW8/SLZGzNqM=' 'sha256-nUFthDQCKQxKyfZrRMBAvvyvFX+0UR7dmE6N8rKi6WE=' 'sha256-ECPea4R2IYYAs7oWpBfqDTPKU6p6QPegNKypOQPWhM8=' 'sha256-owHeo14qy8qbijIFAAYP5mhHl5YSJQJgVs8TYjmqatI='    ";
                            else if (controllerName == "CompareTables")
                                cspGPScout += "'sha256-5nDz4CF9iCgPp7kRMXdf1QO1tpVqaBtRQav3OpWcocc='    'sha256-SWOCEgnU0a68M1YW/hYZS3nAr/eghdCsD5tRbSMqUj0=' 'sha256-FhmXeglzVE98x3mXMRJ+H+0z/wGYD6+PjHYiNnGtZak=' ";
                            else if (controllerName == "Error")
                                cspGPScout += "'sha256-46CRavxrOrUk47JF64ImP1/gXEvth8G4a6CV0iexB/8=' 'sha256-5tDZ9NLZ2dJ79VwVWdE8zf/Ujj0RhOBYBXsBbeGGHkE=' ";
                            else if (controllerName == "Map")
                                cspGPScout += "'sha256-mVyFc7ZvUjEeRV8xBjYzr4pPyVZEwUgQGxReliQm2eA=' ";
                            else if (controllerName == "Navigation")
                                cspGPScout += "'sha256-tLBOozgunq3kFlKfz3y82ZWQSa1TE8Yt1t/r6Tw/b1E=' ";
                            else if (controllerName == "Request")
                                cspGPScout += "'sha256-H1ZQTMHvHgzLxojCdcb7SMYp8DnXpq93qPL5NduX+hA=' ";
                        }
                        /*
                        if (controllerName == "Group")
                            cspGPScout += "'sha256-2j150wTQF95K5GU2uKGxfr4Vl5whhSNAqiK2FaYhviU=' 'sha256-hvRztloauADHFqq+8QuGlYvnbgOzvVwjZyDBU/80V7o=' ";
                        */
                        cspGPScout += "; ";

                        cspGPScout += "style-src 'self' *.googleapis.com *.google.com *.mapbox.com *.rcpadvisors.com ";
                        cspGPScout += unsafeInlineCSS;
                        if (unsafeInlineCSS == "")
                        {
                            //cspGPScout += "'sha256-ZdHxw9eWtnxUb3mk6tBS+gIiVUPE3pGM470keHPDFlE=' 'sha256-LXMayLPvtaIAoKCymuE2A/cqc6AgfkrQHUQkh1j7+3E=' 'sha256-0rYNbyAMybvCV8kYCisggiSMb8LiRfN/kL357rro+64=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-lWayBDGc6OnzhN7/7jXugZtfvxZKmljA/QF7sobduNI=' 'sha256-tphxjFMX6/+Cq1DlOk8NX2MnaXU9YWtG4KgRst9dUwE=' ";
                            cspGPScout += "'sha256-ZdHxw9eWtnxUb3mk6tBS+gIiVUPE3pGM470keHPDFlE=' 'sha256-LXMayLPvtaIAoKCymuE2A/cqc6AgfkrQHUQkh1j7+3E=' 'sha256-0rYNbyAMybvCV8kYCisggiSMb8LiRfN/kL357rro+64=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-lWayBDGc6OnzhN7/7jXugZtfvxZKmljA/QF7sobduNI=' 'sha256-tphxjFMX6/+Cq1DlOk8NX2MnaXU9YWtG4KgRst9dUwE=' 'sha256-Rilei2X8YIvp5aIpDmT37tsxcdGrjZEFgUGyS7TE4t0=' 'sha256-GvQswZwEq38JZEeEujAkLSY3lJ4EolItnIilLM8mcjg=' 'sha256-zwESNVJJkquAnPBc/beoySOWslTCZbRyEeTRXaqy8As=' 'sha256-l9c2dlLekQUQsmDCboiCEGo9Zm2FQRuZPvOBZ7yktA8=' 'sha256-5HTZthS/l1fra4ED0NALLThxOvXU7zs1ctlA9nY1cas=' 'sha256-XBqXNz/0VWoeNPEvqlGBr1pwkHclUNkT5ENMpG/u2Io=' 'sha256-IMecqjfi02mCrqotCiHlQ5RAl/+IaymSE6/jd9HQmY8=' 'sha256-q00ZWid1TRdn/JkI3pHzsF5Ln36qY1Y481tNQCF+tZk=' 'sha256-C0aToWbN60AU4qJknl6Z1CZXPFQlkT044P7HeW0bE2w=' 'sha256-1Xp74O1X6JXVyRoorPEcGWZLETBgGX+Xs9YR3Ft3A6A=' 'sha256-r1g/i5zvPbmcMEVR9xGfQYaKeZHkC3fHI5OubBM26wM=' 'sha256-TU8jbAd1uHi/6bfV+bn2MtfDvd4UPhHvvuHe4XKign0=' 'sha256-SSLCy86hAQ+mzWcEWbidumN322lDLxVOdi6SQz2kNeU=' 'sha256-0EZqoz+oBhx7gF4nvY2bSqoGyy4zLjNF+SDQXGp/ZrY=' 'sha256-P00lvee8x6xjuGjxNoLhzbHkMqe2tqptXV5iGqqgNn4=' 'sha256-kv5tIGzYbczh//M02PlcZlQkubLuoZCyHyR5Fp8mwSk=' 'sha256-zivMMlDSyfOXWMdS5KB1SnKYOG4mNT0cZyZFagixWJw=' 'sha256-5gz2c1mG+rwxmYnSeeebLAR8qCrXnUfXX6WitG8+IzU=' 'sha256-tXn7erlRn1YWySL+RQ0PNqn+6NVAh+OrhDg4eA94Nno=' 'sha256-64mcSQVXen0ozr47xSkKV1HYsyhyGdqiyaDRzn1HIW4=' 'sha256-YruATgF3AjfcS6xQRQUQxrmhKkI/+g+I5ljgqNOVbK4=' 'sha256-wrrlQ5WoMK4k+3RPCfbmJHVkPzOkuiWWFugH86/TZxU=' 'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' 'sha256-IpwVQ3db1LicKlxlrkwQiaPTERdkIXJT0mqftHTDQrw=' 'sha256-Ut79aLjs3fC5UtVv26l2r+kyv/4DhifGEM6YG3xXOyo=' 'sha256-ycdgw0TgMXayRj/XS5iAOoIz1N1dAvspOIV839in6lA=' 'sha256-9H47lw3Ov9TU1oBsW0UMwIWgV17s1lHP7l7rcjmQhCs=' 'sha256-G6UYgfMAt96bwmNO3tZUQ/8oJIcQTGjHXe4xmZIcQH0=' 'sha256-pTF7jaW71Jt8Ib+7SNG3iPRM/bfSL8wdrX84iJzdmxw=' 'sha256-pC5REm07RkpelE/wKKlg88w/Ejtd65aYb53L2vMA/Tg=' 'sha256-V5qjqpzF5RXK7ia+GRW41bgCA1RCxTHFTLseInoGqSg=' 'sha256-qd+Nhc9yA8AHpeBWfcIrlo6n2kt+8yFCuBqBOgxc9WY=' 'sha256-PEo9RAB0C7Pw0EzsgS+HuVOHFPZRr93dZNJ9mYzz95A=' 'sha256-FMl2ia/jtZ/ski0K6lZcncz7Fr32vYNAD+T+5cXBfgI=' 'sha256-keJo6HXi51GdhFi6WSR3KvZrTSkPooX7kk/CxhOKS+k=' 'sha256-7g31PdKdPoCsP4LdqZlHvDg9P9nGJfjAzo/EF/hzsq8=' 'sha256-kBlEOa+c6w+xK2z7DfI4ddXv5cEnbCF4TrrmOwEGsqs=' 'sha256-Xb6VsMsUW5jBy8HAXlMcrIeEC0qAgR5OuKvwp+fJWi0=' 'sha256-T2RZ46gL1swN4CUQhcAIe37+DNaY7n2B1P/wdLl7/kY=' 'sha256-bV45uSPJR4RuskvJebSjEo5CWGHSsgpATMiBYy+9MUk=' 'sha256-6UMW9dlwpXwXk1Hw2G/xCD+Ou8yYBXCn7332fwVTwMI=' 'sha256-NA/L7veIqc5kZWbgS9e/2ax4TvwWTx993L6x5F2qFCk=' 'sha256-vuKj6mP7ufT6gXLyJM81iZyC5M5Pyzg+CSuaHWstGHI=' 'sha256-62/SCjXZeYCKasNqbCW69d6mUYlIBUklroUTBoK/0Lo=' 'sha256-+9a0jl6w6n7X9s9GkpiMcuX0VhN8lvNg+ja4QuBMQU8=' 'sha256-urcVgBi7BLJ4kH38hMZw57qokoCxOlU8vboCM/b8zLE=' ";

                            /*
                            if (controllerName == "Account")
                                cspGPScout += "'sha256-Rilei2X8YIvp5aIpDmT37tsxcdGrjZEFgUGyS7TE4t0=' ";
                            else if (controllerName == "Search")
                                cspGPScout += "'sha256-GvQswZwEq38JZEeEujAkLSY3lJ4EolItnIilLM8mcjg=' 'sha256-zwESNVJJkquAnPBc/beoySOWslTCZbRyEeTRXaqy8As=' 'sha256-l9c2dlLekQUQsmDCboiCEGo9Zm2FQRuZPvOBZ7yktA8=' 'sha256-5HTZthS/l1fra4ED0NALLThxOvXU7zs1ctlA9nY1cas=' 'sha256-XBqXNz/0VWoeNPEvqlGBr1pwkHclUNkT5ENMpG/u2Io=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-IMecqjfi02mCrqotCiHlQ5RAl/+IaymSE6/jd9HQmY8=' 'sha256-q00ZWid1TRdn/JkI3pHzsF5Ln36qY1Y481tNQCF+tZk=' 'sha256-C0aToWbN60AU4qJknl6Z1CZXPFQlkT044P7HeW0bE2w=' 'sha256-1Xp74O1X6JXVyRoorPEcGWZLETBgGX+Xs9YR3Ft3A6A=' 'sha256-r1g/i5zvPbmcMEVR9xGfQYaKeZHkC3fHI5OubBM26wM=' 'sha256-TU8jbAd1uHi/6bfV+bn2MtfDvd4UPhHvvuHe4XKign0=' 'sha256-SSLCy86hAQ+mzWcEWbidumN322lDLxVOdi6SQz2kNeU=' 'sha256-0EZqoz+oBhx7gF4nvY2bSqoGyy4zLjNF+SDQXGp/ZrY=' 'sha256-P00lvee8x6xjuGjxNoLhzbHkMqe2tqptXV5iGqqgNn4=' 'sha256-kv5tIGzYbczh//M02PlcZlQkubLuoZCyHyR5Fp8mwSk=' 'sha256-zivMMlDSyfOXWMdS5KB1SnKYOG4mNT0cZyZFagixWJw=' 'sha256-5gz2c1mG+rwxmYnSeeebLAR8qCrXnUfXX6WitG8+IzU=' 'sha256-tXn7erlRn1YWySL+RQ0PNqn+6NVAh+OrhDg4eA94Nno=' 'sha256-64mcSQVXen0ozr47xSkKV1HYsyhyGdqiyaDRzn1HIW4=' 'sha256-YruATgF3AjfcS6xQRQUQxrmhKkI/+g+I5ljgqNOVbK4=' 'sha256-wrrlQ5WoMK4k+3RPCfbmJHVkPzOkuiWWFugH86/TZxU=' ";
                            else if (controllerName == "Account")
                                cspGPScout += "'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' ";
                            else if (controllerName == "Admin")
                                cspGPScout += "'sha256-IpwVQ3db1LicKlxlrkwQiaPTERdkIXJT0mqftHTDQrw=' 'sha256-Ut79aLjs3fC5UtVv26l2r+kyv/4DhifGEM6YG3xXOyo=' ";
                            else if (controllerName == "Organization")
                                cspGPScout += "'sha256-ycdgw0TgMXayRj/XS5iAOoIz1N1dAvspOIV839in6lA=' 'sha256-9H47lw3Ov9TU1oBsW0UMwIWgV17s1lHP7l7rcjmQhCs=' 'sha256-G6UYgfMAt96bwmNO3tZUQ/8oJIcQTGjHXe4xmZIcQH0=' 'sha256-pTF7jaW71Jt8Ib+7SNG3iPRM/bfSL8wdrX84iJzdmxw=' 'sha256-pC5REm07RkpelE/wKKlg88w/Ejtd65aYb53L2vMA/Tg=' 'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' 'sha256-V5qjqpzF5RXK7ia+GRW41bgCA1RCxTHFTLseInoGqSg=' 'sha256-qd+Nhc9yA8AHpeBWfcIrlo6n2kt+8yFCuBqBOgxc9WY=' 'sha256-PEo9RAB0C7Pw0EzsgS+HuVOHFPZRr93dZNJ9mYzz95A=' 'sha256-FMl2ia/jtZ/ski0K6lZcncz7Fr32vYNAD+T+5cXBfgI=' 'sha256-keJo6HXi51GdhFi6WSR3KvZrTSkPooX7kk/CxhOKS+k=' 'sha256-7g31PdKdPoCsP4LdqZlHvDg9P9nGJfjAzo/EF/hzsq8=' 'sha256-kBlEOa+c6w+xK2z7DfI4ddXv5cEnbCF4TrrmOwEGsqs=' 'sha256-Xb6VsMsUW5jBy8HAXlMcrIeEC0qAgR5OuKvwp+fJWi0=' 'sha256-T2RZ46gL1swN4CUQhcAIe37+DNaY7n2B1P/wdLl7/kY=' 'sha256-bV45uSPJR4RuskvJebSjEo5CWGHSsgpATMiBYy+9MUk=' 'sha256-6UMW9dlwpXwXk1Hw2G/xCD+Ou8yYBXCn7332fwVTwMI=' 'sha256-NA/L7veIqc5kZWbgS9e/2ax4TvwWTx993L6x5F2qFCk=' 'sha256-vuKj6mP7ufT6gXLyJM81iZyC5M5Pyzg+CSuaHWstGHI=' 'sha256-62/SCjXZeYCKasNqbCW69d6mUYlIBUklroUTBoK/0Lo=' ";
                            else if (controllerName == "MarketData")
                                cspGPScout += "'sha256-+9a0jl6w6n7X9s9GkpiMcuX0VhN8lvNg+ja4QuBMQU8=' ";
                            */
                        }
                        cspGPScout += "; ";

                        cspGPScout += "connect-src 'self' *.doubleclick.net *.typekit.net *.mapbox.com *.google-analytics.com *.rcpadvisors.com; ";
                        cspGPScout += "img-src 'self' http: data: blob: *.google-analytics.com *.doubleclick.net *.typekit.net *.rcpadvisors.com *.gpscoutnavigator.com *.tynt.com *.eyeota.net; ";
                        cspGPScout += "font-src 'self' *.googleapis.com *.gstatic.com *.typekit.net *.rcpadvisors.com; ";
                        cspGPScout += "worker-src 'self' blob:;";

                        response.AddHeader("Content-Security-Policy", cspGPScout);
                        response.AddHeader("X-WebKit-CSP", cspGPScout);
                        response.AddHeader("X-Content-Security-Policy", cspGPScout);
                        base.OnActionExecuting(filterContext);
                    }
                }

            }
        }

        //GCS Updates
        // Search?showAdvancedOptions=1
        public class ContentSecurityPolicyFilterAdvanceSearchAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var response = filterContext.HttpContext.Response;

                string cspGPScout = "default-src 'none'; frame-src 'self' *.rcpadvisors.com *.gpscoutnavigator.com; ";
                cspGPScout += "script-src 'self' ";
                cspGPScout += "*.rcpadvisors.com *.gpscoutnavigator.com *.googleapis.com *.google.com *.gstatic.com *.gtm.com *.github.com *.googletagmanager.com *.analytics.com *.google-analytics.com *.tynt.com *.typekit.net *.mapbox.com ";
                cspGPScout += "'sha256-Nyy2vSyqh/BZUo5i+uSb41RJ2GcmlC15DkdiO46hyac=' 'sha256-Dqh3AqeEq9q3yJUWFFvsh8mT2EoS6AF4DFuxnYtag7c=' 'sha256-rbZxQnc02mkiuhe1PPRzuHYAFzU3tCfjjGY8kLrWs4Y=' 'sha256-1xtiB6mV1iIKZ5iz9CxA5lEnfEg8d0XEH3FL9L8NBqo=' 'sha256-wkWKbs/JvtCXAsotD5p8JuSdxnEIVoxQmDQiGgYCEV0=' 'sha256-Xy6CuXfIdw8wouMXokv/WeS40wg9DJrivrCNlqhgFEo=' 'sha256-matwEc6givhWX0+jiSfM1+E5UMk8/UGLdl902bjFBmY=' 'sha256-ZdHxw9eWtnxUb3mk6tBS+gIiVUPE3pGM470keHPDFlE=' 'sha256-pcnxk1mT7NzMD58Smnh3lWwngs8hkn38M8N6LWwGZ3o=' 'sha256-biLFinpqYMtWHmXfkA1BPeCY0/fNt46SAZ+BBk5YUog=' 'sha256-RaYN8y52zmw5vemlsptIyoLmYT4miU5jw8Fkg4A35ss=' 'sha256-urcVgBi7BLJ4kH38hMZw57qokoCxOlU8vboCM/b8zLE=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-pBd2D1oUCC/nnW0xv0icKT/R1cmMx1+EW1RyBQMHyv8=' 'sha256-oqL09Rude1cdKBCwc2VXSOpj6APPVcUb1KeTK/iRxCQ=' 'sha256-Z7l0Xqn+ym3k9jMZFZ6tCEnMb3axq1HLz739EgJ3hM4=' 'sha256-J6d5gwcxLRTjpYG1Iyh+tDa5xzqP28SfxcwTdY4NEUw=' 'sha256-QGTDW24MFBhAZRv9tHpMqbjL/0VC/0W/zMwVKjlAYz8=' 'sha256-ZL/yk+qKbWSJ93ptnDpzhRPsneVMNzlOZQWsehePuZA=' 'sha256-51BNzX1MFqJWaXjcJfPnrbobyn1b99phMskwipvp1v4=' 'sha256-yRDrrUqqEvxOcNhDRfT4yz6nP/rULobBJbUiirflPZ4=' ";
                cspGPScout += "; ";

                cspGPScout += "style-src 'self' *.googleapis.com *.google.com *.mapbox.com 'unsafe-inline' ";
                //cspGPScout += "'sha256-GvQswZwEq38JZEeEujAkLSY3lJ4EolItnIilLM8mcjg=' 'sha256-zwESNVJJkquAnPBc/beoySOWslTCZbRyEeTRXaqy8As=' 'sha256-l9c2dlLekQUQsmDCboiCEGo9Zm2FQRuZPvOBZ7yktA8=' 'sha256-5HTZthS/l1fra4ED0NALLThxOvXU7zs1ctlA9nY1cas=' 'sha256-XBqXNz/0VWoeNPEvqlGBr1pwkHclUNkT5ENMpG/u2Io=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-IMecqjfi02mCrqotCiHlQ5RAl/+IaymSE6/jd9HQmY8=' 'sha256-q00ZWid1TRdn/JkI3pHzsF5Ln36qY1Y481tNQCF+tZk=' 'sha256-C0aToWbN60AU4qJknl6Z1CZXPFQlkT044P7HeW0bE2w=' 'sha256-1Xp74O1X6JXVyRoorPEcGWZLETBgGX+Xs9YR3Ft3A6A=' 'sha256-r1g/i5zvPbmcMEVR9xGfQYaKeZHkC3fHI5OubBM26wM=' 'sha256-TU8jbAd1uHi/6bfV+bn2MtfDvd4UPhHvvuHe4XKign0=' 'sha256-SSLCy86hAQ+mzWcEWbidumN322lDLxVOdi6SQz2kNeU=' 'sha256-0EZqoz+oBhx7gF4nvY2bSqoGyy4zLjNF+SDQXGp/ZrY=' 'sha256-P00lvee8x6xjuGjxNoLhzbHkMqe2tqptXV5iGqqgNn4=' 'sha256-kv5tIGzYbczh//M02PlcZlQkubLuoZCyHyR5Fp8mwSk=' 'sha256-zivMMlDSyfOXWMdS5KB1SnKYOG4mNT0cZyZFagixWJw=' 'sha256-5gz2c1mG+rwxmYnSeeebLAR8qCrXnUfXX6WitG8+IzU=' 'sha256-tXn7erlRn1YWySL+RQ0PNqn+6NVAh+OrhDg4eA94Nno=' 'sha256-64mcSQVXen0ozr47xSkKV1HYsyhyGdqiyaDRzn1HIW4=' 'sha256-YruATgF3AjfcS6xQRQUQxrmhKkI/+g+I5ljgqNOVbK4=' 'sha256-wrrlQ5WoMK4k+3RPCfbmJHVkPzOkuiWWFugH86/TZxU=' ";
                cspGPScout += "; ";
                cspGPScout += "connect-src 'self' *.doubleclick.net *.typekit.net *.mapbox.com *.google-analytics.com; ";
                cspGPScout += "img-src 'self' data: blob: *.google-analytics.com *.doubleclick.net *.typekit.net *.rcpadvisors.com *.gpscoutnavigator.com *.tynt.com *.eyeota.net; ";
                cspGPScout += "font-src 'self' *.googleapis.com *.gstatic.com *.typekit.net; ";
                cspGPScout += "worker-src 'self' blob:;";

                response.AddHeader("Content-Security-Policy", cspGPScout);
                response.AddHeader("X-WebKit-CSP", cspGPScout);
                response.AddHeader("X-Content-Security-Policy", cspGPScout);
                base.OnActionExecuting(filterContext);
            }
        }

        //GCS Updates
        // Search/SaveTemplate
        public class ContentSecurityPolicyFilterSearchAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                var response = filterContext.HttpContext.Response;

                string cspGPScout = "default-src 'none'; frame-src 'self' *.rcpadvisors.com *.gpscoutnavigator.com; ";
                cspGPScout += "script-src 'self' 'unsafe-eval' ";
                cspGPScout += "'unsafe-inline' *.rcpadvisors.com *.gpscoutnavigator.com *.googleapis.com *.google.com *.gstatic.com *.gtm.com *.github.com *.googletagmanager.com *.analytics.com *.google-analytics.com *.tynt.com *.typekit.net *.mapbox.com ";
                //cspGPScout += "'sha256-pBd2D1oUCC/nnW0xv0icKT/R1cmMx1+EW1RyBQMHyv8=' 'sha256-oqL09Rude1cdKBCwc2VXSOpj6APPVcUb1KeTK/iRxCQ=' 'sha256-Z7l0Xqn+ym3k9jMZFZ6tCEnMb3axq1HLz739EgJ3hM4=' ";
                //cspGPScout += "'sha256-Nyy2vSyqh/BZUo5i+uSb41RJ2GcmlC15DkdiO46hyac=' 'sha256-rbZxQnc02mkiuhe1PPRzuHYAFzU3tCfjjGY8kLrWs4Y=' 'sha256-1xtiB6mV1iIKZ5iz9CxA5lEnfEg8d0XEH3FL9L8NBqo=' 'sha256-tXqrp7jaGQVABN5eMiLeUJ2JftMKyyHQd18Ir69+sAM=' 'sha256-J6d5gwcxLRTjpYG1Iyh+tDa5xzqP28SfxcwTdY4NEUw=' 'sha256-Xy6CuXfIdw8wouMXokv/WeS40wg9DJrivrCNlqhgFEo=' 'sha256-wht6SSBjTZBw01yvHP+WqiWveA5z3HMM0FUkFaPX96E=' 'sha256-ZL/yk+qKbWSJ93ptnDpzhRPsneVMNzlOZQWsehePuZA=' 'sha256-44WWzMfS0ivLYhQU+/1tQ1z0INg3g1ntfds8Uyilex8=' 'sha256-2LLlvfU7xh9DPivi+EtzzYekow7FdNsYuiV7S1UDzo8=' 'sha256-QGTDW24MFBhAZRv9tHpMqbjL/0VC/0W/zMwVKjlAYz8=' 'sha256-yRDrrUqqEvxOcNhDRfT4yz6nP/rULobBJbUiirflPZ4=' ";
                //cspGPScout += "'nonce-" + System.Configuration.ConfigurationManager.AppSettings["InlineNonce"] + "' ";
                cspGPScout += "; ";

                cspGPScout += "style-src 'self' *.googleapis.com *.google.com *.mapbox.com ";
                cspGPScout += "'unsafe-inline' ";
                //cspGPScout += "'sha256-ZdHxw9eWtnxUb3mk6tBS+gIiVUPE3pGM470keHPDFlE=' 'sha256-LXMayLPvtaIAoKCymuE2A/cqc6AgfkrQHUQkh1j7+3E=' 'sha256-0rYNbyAMybvCV8kYCisggiSMb8LiRfN/kL357rro+64=' 'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=' 'sha256-lWayBDGc6OnzhN7/7jXugZtfvxZKmljA/QF7sobduNI=' 'sha256-tphxjFMX6/+Cq1DlOk8NX2MnaXU9YWtG4KgRst9dUwE=' ";
                //cspGPScout += "'sha256-GvQswZwEq38JZEeEujAkLSY3lJ4EolItnIilLM8mcjg=' 'sha256-YruATgF3AjfcS6xQRQUQxrmhKkI/+g+I5ljgqNOVbK4=' 'sha256-kv5tIGzYbczh//M02PlcZlQkubLuoZCyHyR5Fp8mwSk=' 'sha256-1Xp74O1X6JXVyRoorPEcGWZLETBgGX+Xs9YR3Ft3A6A=' 'sha256-zivMMlDSyfOXWMdS5KB1SnKYOG4mNT0cZyZFagixWJw=' 'sha256-5gz2c1mG+rwxmYnSeeebLAR8qCrXnUfXX6WitG8+IzU=' 'sha256-KE/YeycP+iyQ39dYjkuC/UUeEoO+ZE8cN3etPt+JZMo=' 'sha256-MCztXC9r35FrCDpf+V1yUgS9a+CT7wASkjD99WCISnk=' 'sha256-aqNNdDLnnrDOnTNdkJpYlAxKVJtLt9CtFLklmInuUAE=' 'sha256-t1iFLmY/8oXncmapZaCzoqGUnJ7fSxoLpYMf/2mu+hc=' 'sha256-4emgXpMbxOTIklxXPHPcq70Tyr7fARTCiYLyQC6Sb2s=' ";
                cspGPScout += "; ";
                cspGPScout += "connect-src 'self' *.doubleclick.net *.typekit.net *.mapbox.com *.google-analytics.com; ";
                cspGPScout += "img-src 'self' data: blob: *.google-analytics.com *.doubleclick.net *.typekit.net *.rcpadvisors.com *.gpscoutnavigator.com *.tynt.com *.eyeota.net; ";
                cspGPScout += "font-src 'self' *.googleapis.com *.gstatic.com *.typekit.net; ";
                cspGPScout += "worker-src 'self' blob:;";

                response.AddHeader("Content-Security-Policy", cspGPScout);
                response.AddHeader("X-WebKit-CSP", cspGPScout);
                response.AddHeader("X-Content-Security-Policy", cspGPScout);
                base.OnActionExecuting(filterContext);
            }
        }



        //GCS Updates
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // filters.Add(new HandleErrorAttribute());
            filters.Add(new ContentSecurityPolicyFilterAttribute());
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Headers.Remove("X-Powered-By");
            HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
            HttpContext.Current.Response.Headers.Remove("X-AspNetMvc-Version");
            HttpContext.Current.Response.Headers.Remove("Server");
        }
    }
}