﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class NoteController : ControllerBase
    {
        public JsonResult CreateFromWidget(string text, string organizationId, bool? isGroup)
        {
            // Construct the note
            var note = new OrganizationNote
            {
                Id = Guid.NewGuid(),
                UserId = UserEntity.Id,
                OrganizationId = new Guid(organizationId),
                Note = text,
                DateEntered = DateTime.Now,
                GroupId = isGroup.GetValueOrDefault(false) ? UserEntity.GroupId : null,
                EditedByDate = DateTime.Now,
                EditedByUserId = UserEntity.Id
            };

            // Add it to the db
            NoteRepository.AddNote(note);

            // Return success
            var retval = new MessageViewModel
            {
                Status = MessageStatus.Success,
                RenderView = this.RenderPartialViewToString("NotesPartial", NoteRepository.GetByUserIdAndOrganizationId(UserEntity.Id, new Guid(organizationId)))
            };

            return Json(retval, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(Guid? id)
        {
            var note = NoteRepository.FindAll(m => m.Id == id).SingleOrDefault();
            var retval = new MessageViewModel();
            if (note == null || !(note.UserId == UserEntity.Id || (note.GroupId.HasValue && note.GroupId == UserEntity.GroupId)))
            {
                retval.Status = MessageStatus.Failure;
            }
            else
            {
                NoteRepository.DeleteOnSubmit(note);
                NoteRepository.SubmitChanges();
                retval.Status = MessageStatus.Success;
            }
            return Json(retval, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Modify(string text, Guid? noteId)
        {
            var note = NoteRepository.FindAll(m =>m.Id == noteId).SingleOrDefault();
            var retval = new MessageViewModel();
            if (note == null || !(note.UserId == UserEntity.Id || (note.GroupId.HasValue && note.GroupId == UserEntity.GroupId)))
            {
                retval.Status = MessageStatus.Failure;
            }
            else
            {
                note.Note = text;
                note.EditedByDate = DateTime.Now;
                note.EditedByUserId = UserEntity.Id;
                NoteRepository.SubmitChanges();
                retval.Status = MessageStatus.Success;
            }
            return Json(retval, JsonRequestBehavior.AllowGet);
        }
    }
}
