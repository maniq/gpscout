﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Common.Data.Models;
using System.Linq.Expressions;
using System.IO;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using GPScout.Domain.Contracts.Models;
using AtlasDiligence.Web.Models;

namespace AtlasDiligence.Web.Controllers
{
    public class RequestController : ControllerBase
    {
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public ActionResult Index()
        {
            return View();
        }


        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult RequestGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        {
            var results = new GridList<RequestDataGrid>();
            Expression<Func<OrganizationRequest, string>> orderBy = null;
            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "CreatedOn";
                sortAscending = true;
            }
            else if (sortColumn == "UserName")
            {
                //sortColumn = "aspnet_User.LoweredUserName";
                orderBy = x => x.UserEmail;
            }
            Expression<Func<OrganizationRequest, bool>> predicate =
                m => (m.IsActive) &&
                     (string.IsNullOrEmpty(organizationName) || m.OrganizationName.StartsWith(organizationName)) &&
                     (string.IsNullOrEmpty(userName) || (m.aspnet_User != null && m.aspnet_User.LoweredUserName.StartsWith(userName)) || (m.RequestAdditionalInfo != null && m.RequestAdditionalInfo.UserEmail.StartsWith(userName)));

            var allRequests = RequestRepository.FindAll(predicate);

            IQueryable<OrganizationRequest> users = orderBy == null ?
                users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage())
            :
                users = RequestRepository.Paginate(allRequests, orderBy, sortAscending, pageStart, DataGridRowsPerPage());

            results.Total = allRequests.Count();
            results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

            return Json(results);
        }

        //[AtlasAuthorize(Roles = RoleNames.Admin)]
        //public JsonResult OrganizationRequestGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        //{
        //    var results = new GridList<RequestDataGrid>();
        //    if (string.IsNullOrEmpty(sortColumn))
        //    {
        //        sortColumn = "CreatedOn";
        //        sortAscending = true;
        //    }
        //    if (sortColumn == "UserName")
        //    {
        //        sortColumn = "aspnet_User.LoweredUserName";
        //    }
        //    Expression<Func<OrganizationRequest, bool>> predicate =
        //        m => (m.OrganizationId.HasValue) &&
        //             (m.IsActive) &&
        //             (m.Type != (int)RequestType.Introduction) &&
        //             (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
        //             (m.OrganizationName.StartsWith(organizationName) || string.IsNullOrEmpty(organizationName));
        //    var allRequests = RequestRepository.FindAll(predicate);
        //    var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

        //    results.Total = allRequests.Count();
        //    results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

        //    return Json(results);
        //}

        //[AtlasAuthorize(Roles = RoleNames.Admin)]
        //public JsonResult OrganizationIntroductionGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        //{
        //    var results = new GridList<RequestDataGrid>();
        //    if (string.IsNullOrEmpty(sortColumn))
        //    {
        //        sortColumn = "CreatedOn";
        //        sortAscending = true;
        //    }
        //    if (sortColumn == "UserName")
        //    {
        //        sortColumn = "aspnet_User.LoweredUserName";
        //    }
        //    Expression<Func<OrganizationRequest, bool>> predicate =
        //        m => (m.OrganizationId.HasValue) &&
        //             (m.IsActive) &&
        //             (m.Type == (int)RequestType.Introduction) &&
        //             (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
        //             (m.OrganizationName.StartsWith(organizationName) || string.IsNullOrEmpty(organizationName));
        //    var allRequests = RequestRepository.FindAll(predicate);
        //    var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

        //    results.Total = allRequests.Count();
        //    results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

        //    return Json(results);
        //}

        //[AtlasAuthorize(Roles = RoleNames.Admin)]
        //public JsonResult AnonymousOrganizationRequestGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string organizationName)
        //{
        //    var results = new GridList<RequestDataGrid>();
        //    if (string.IsNullOrEmpty(sortColumn))
        //    {
        //        sortColumn = "CreatedOn";
        //        sortAscending = true;
        //    }
        //    if (sortColumn == "UserName")
        //    {
        //        sortColumn = "aspnet_User.LoweredUserName";
        //    }
        //    Expression<Func<OrganizationRequest, bool>> predicate =
        //        m => (!m.OrganizationId.HasValue) &&
        //             (m.IsActive) &&
        //             (m.aspnet_User.LoweredUserName.StartsWith(userName) || string.IsNullOrEmpty(userName)) &&
        //             (m.OrganizationName.Contains(organizationName) || string.IsNullOrEmpty(organizationName));
        //    var allRequests = RequestRepository.FindAll(predicate);
        //    var users = RequestRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

        //    results.Total = allRequests.Count();
        //    results.Result = users.Select(x => new RequestDataGrid(x)).ToArray();

        //    return Json(results);
        //}

        /// <summary>
        /// Request Information for given Org.
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public JsonResult RequestOrganization(RequestType requestType, Guid organizationId, string comment)
        {
            IResults results = RequestService.AddRequest(requestType, organizationId, UserEntity.Id, comment, base.MailFrom);
            return Json(new MessageViewModel { Status = !results.HasError ? MessageStatus.Success : MessageStatus.Failure }, JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// Request Information for an organization with name parameter.  No requirement that this is in the database
        /// </summary>
        /// <param name="name"></param>
        /// <param name="comment"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public JsonResult RequestOrganizationName(RequestType requestType, string name, string comment)
        {
            if (!string.IsNullOrWhiteSpace(comment))
            {
                IResults results = RequestService.AddRequest(requestType, name, UserEntity.Id, comment, base.MailFrom);
                return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
            }
            return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "Please specify your request in the Specific Request/Comment/Question field." }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Sets Request as inactive
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult HideRequest(Guid id)
        {
            var request = RequestRepository.FindAll(m => m.Id == id).SingleOrDefault();
            if (request == null) throw new ArgumentException("Invalid Id.");
            request.IsActive = false;
            RequestRepository.Edit(request);
            return Json(new MessageViewModel{Status = MessageStatus.Success}, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PrintFullReport(string instanceId)
        {
            var task = TaskService.GetById(Guid.Parse(instanceId));
            if (task!=null && !task.IsCompleted()) {
                var user = UserService.GetUserById(task.UserId);
                if (user != null)
                {
                    HttpContext.Session.RemoveAll();
                    var _formsService = ComponentBrokerInstance.RetrieveComponent<IFormsAuthenticationService>();
                    _formsService.SignOut();
                    _formsService.SignIn(user.UserName, false);
                    //RedirectToAction("PrintAll", "Organization", new { instanceId = instanceId });
                }
            }
            return new EmptyResult();
        }

        //public ActionResult PrintAllHeader(string instanceId)
        //{
        //    string fileName = Path.Combine(AppSettings.TempPath, OrganizationHelperService.GetFullReportHeaderFileName(instanceId));
        //    return File(fileName, "text/html");
        //}

        //public ActionResult PrintAllFooter(string instanceId)
        //{
        //    string fileName = Path.Combine(AppSettings.TempPath, OrganizationHelperService.GetFullReportFooterFileName(instanceId));
        //    return File(fileName, "text/html");
        //}

        public ActionResult RequestOrganizationPopup(Guid orgId, bool add)
        {
            OrganizationRequest model = new OrganizationRequest();
            if (UserEntity.AvailableOrganizationIDs.Contains(orgId))
            {
                var org = OrganizationService.GetById(orgId);
                if (org != null)
                {
                    ViewBag.Add = add;
                    model.Organization = org;
                }
            }
            return PartialView("OrganizationRequest", model);
        }


        [HttpPost]
        [MultipleLocationsAuthorize]
        [AtlasAuthorize(Roles = RoleNames.CollectedInfo)]
        public ActionResult UpdatePUCNRequest(Guid orgId, bool add)
        {
            bool accessible = true;
            IResults results = null;
            if (add)
            {
                if (UserEntity.AvailableOrganizationIDs.Contains(orgId))
                    results = RequestService.AddRequest(RequestType.ProfileUpdateCompletedNotification, orgId, UserEntity.Id, string.Empty, base.MailFrom);
                else
                    accessible = false;
            }
            else
                results = RequestService.RemoveRequest(RequestType.ProfileUpdateCompletedNotification, orgId, UserEntity.Id, string.Empty, base.MailFrom);

            if (accessible && !results.HasError)
            {
                if (add)
                    UserEntity.PUCNOrganizationIDs.Add(orgId);
                else
                    UserEntity.PUCNOrganizationIDs.Remove(orgId);
                return Json(new { Status = "success", Added = add, OrgId = orgId });
            }
            else
            {
                return Json(new { Status = "error", IsOrgAccessible = accessible });
            }
        }


        public ActionResult NeedHelpPopup()
        {
            NeedHelpMessageViewModel model = new NeedHelpMessageViewModel();
            if (UserEntity != null) model.Email = UserEntity.Email;
            return PartialView("NeedHelp", model);
        }


        [HttpPost]
        public ActionResult SendNeedHelpMessage(NeedHelpMessageViewModel model)
        {
            var messageViewModel = new MessageViewModel();
            if (ModelState.IsValid)
            {
                Guid? userId = User != null && User.Identity != null && User.Identity.IsAuthenticated && string.Compare(User.Identity.Name, model.Email, true) == 0 ? (Guid?)UserEntity.Id : null;

                // if email is identical w user account email then there is no need to store the email separately
                if (userId.HasValue) model.Email = null;
                var results = RequestService.AddRequest(RequestType.NeedHelp, string.Empty, userId, model.Message, model.Email, base.MailFrom);
                if (!results.HasError) messageViewModel.Status = MessageStatus.Success;
            }
            else
                messageViewModel.Message = "Please provide the required information.";

            if (messageViewModel.Status != MessageStatus.Success)
                messageViewModel.RenderView = RenderPartialViewToString("NeedHelp", model);
            return Json(messageViewModel);
        }
    }
}
