﻿using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Folder = AtlasDiligence.Common.Data.Models.Folder;

namespace AtlasDiligence.Web.Controllers
{
	[Authorize]
	[EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class FolderController : ControllerBase
	{
		public ActionResult Index(Guid? id)
		{
            ViewBag.ExpandFolderId = id;
			return View(BuildFolderViewModelList(FolderRepository.GetByUserId(UserEntity.Id)));
		}

		public JsonResult OrganizationsAutoComplete(string term)
		{
			var segments = new List<Segment>();
			var purchasedOrgs = new List<Guid>();
			if (UserEntity.GroupId.HasValue)
			{
				var group = this.GroupRepository.GetById(UserEntity.GroupId.Value);
				segments = group.GroupSegments.Select(m => m.Segment).ToList();
				purchasedOrgs = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
			}
			var orgNames = SearchService.GetOrganizationsContainingTerm(segments, purchasedOrgs, term);
			//var orgs = FolderRepository.GetOrganizationsStartsWith(term);
			return Json(orgNames.Select(m => new { Id = m.Id, Name = m.Name, IsPublished = m.IsPublished }).Distinct(), JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Add an organization by name to a folder and return a FolderOrganization Row
		/// </summary>
		/// <param name="organizationName"></param>
		/// <param name="folderId"></param>
		/// <returns></returns>
		public JsonResult AddOrganizationByName(string organizationName, Guid folderId)
		{
			var retval = new MessageViewModel();

			var organization = FolderRepository.GetOrganizationByName(organizationName).FirstOrDefault();
			var availableOrgs = GetAvailableOrganizations();

			if (organization == null || !availableOrgs.Any(m => m.Id == organization.Id))
			{
				retval.Status = MessageStatus.Failure;
				retval.Message = "There were no Organizations with the name entered.";
			}
			else
			{
				FolderRepository.AddOrganizationToFolder(folderId, organization.Id);
				retval.Status = MessageStatus.Success;
				retval.RenderView = RenderPartialViewToString("FolderOrganization", organization);
				retval.Data = new { Id = organization.Id };
			}

			return Json(retval, JsonRequestBehavior.AllowGet);
		}

		public JsonResult AddOrganization(Guid organizationId, Guid folderId)
		{
			var retval = new MessageViewModel();
			retval.Status = MessageStatus.Success;
			FolderRepository.AddOrganizationToFolder(folderId, organizationId);
			return Json(retval);
		}

		public JsonResult AddOrganizations(IEnumerable<Guid> organizationIds, Guid folderId)
		{
			var retval = new MessageViewModel();
			retval.Status = MessageStatus.Success;
			FolderRepository.AddOrganizationsToFolder(folderId, organizationIds);
			return Json(retval);
		}

		public JsonResult Delete(Guid folderId)
		{
			var retval = new MessageViewModel();
			retval.Status = MessageStatus.Success;
			FolderRepository.RemoveFolder(folderId);
            RenderFoldersSideBarWidget(retval);
			return Json(retval);
		}


		public JsonResult Rename(Guid folderId, string name)
		{
			var retval = new MessageViewModel();
			if (!String.IsNullOrWhiteSpace(name))
			{
				retval.Status = MessageStatus.Success;
				var folder = FolderRepository.GetById(folderId);
				folder.Name = name;
				FolderRepository.SubmitChanges();
                RenderFoldersSideBarWidget(retval);
			}
			else
			{
				retval.Status = MessageStatus.Failure;
			}
			return Json(retval, JsonRequestBehavior.AllowGet);
		}

		public JsonResult RemoveOrganization(Guid folderId, Guid organizationId)
		{
			var retval = new MessageViewModel();
			retval.Status = MessageStatus.Success;
			FolderRepository.RemoveOrganizationFromFolder(folderId, organizationId);
			return Json(retval);
		}

		public JsonResult Create(string name, bool? shared)
		{
			var folder = new Folder { Name = name, Id = Guid.NewGuid(), UserId = UserEntity.Id, Shared = shared };
			FolderRepository.InsertOnSubmit(folder);
			FolderRepository.SubmitChanges();
			var retval = new
			{
				Status = MessageStatus.Success,
				Message = folder.Id.ToString(),
				RenderView = RenderPartialViewToString("Index", BuildFolderViewModelList(FolderRepository.GetByUserId(UserEntity.Id))),
                SideBarFoldersWidgetHtml = RenderFoldersSideBarWidget()
			};
			return Json(retval);
		}

		public JsonResult CreateFromWidget(string name, string organizationId, bool? shared)
		{
			var folder = new Folder { Name = name, Id = Guid.NewGuid(), UserId = UserEntity.Id, Shared = shared };
			FolderRepository.InsertOnSubmit(folder);
			FolderRepository.SubmitChanges();
			AddOrganization(new Guid(organizationId), folder.Id);
			var allFolders = FolderRepository.GetByUserId(UserEntity.Id).ToList();
			var retval = new
			{
				Status = MessageStatus.Success,
				RenderView = RenderPartialViewToString("FolderWidgetPartial", new KeyValuePair<string, IEnumerable<Folder>>(organizationId, allFolders)),
				SelectedRenderView = RenderPartialViewToString("SelectedFolderWidgetPartial", allFolders.Select(m => new FolderViewModel { Id = m.Id, Name = m.Name, Shared = m.Shared.GetValueOrDefault() })),
                SideBarFoldersWidgetHtml = RenderFoldersSideBarWidget()
            };
			return Json(retval, JsonRequestBehavior.AllowGet);
		}

        public JsonResult ManyCreateFromWidget(string name, IEnumerable<Guid> organizationIds, bool? shared)
		{
			if (organizationIds == null)
				return Json(new { Status = MessageStatus.Failure });
			var folder = new Folder { Name = name, Id = Guid.NewGuid(), UserId = UserEntity.Id, Shared = shared };
			FolderRepository.InsertOnSubmit(folder);
			FolderRepository.SubmitChanges();
			AddOrganizations(organizationIds, folder.Id);
			var allFolders = FolderRepository.GetByUserId(UserEntity.Id).ToList();

			return Json(new
			{
				Status = MessageStatus.Success,
				SelectedRenderView = RenderPartialViewToString("SelectedFolderWidgetPartial", allFolders.Select(m => new FolderViewModel { Id = m.Id, Name = m.Name, Shared = m.Shared.GetValueOrDefault() })),
        		RenderView = RenderPartialViewToString("FolderWidgetPartial", new KeyValuePair<string, IEnumerable<Folder>>(string.Empty, allFolders)),
                SideBarFoldersWidgetHtml = RenderFoldersSideBarWidget()
            }, JsonRequestBehavior.AllowGet);
		}

		public void RegisterUserClick(Guid folderId)
		{
			FolderRepository.RegisterUserClick(UserEntity.Id, folderId);
            var dbUser = UserService.GetUserById(UserEntity.Id);
            if (dbUser != null)
                UserEntity.Folders = dbUser.Folders;
		}


        public JsonResult SetShared(Guid folderId, bool? shared)
        {
            var retval = new MessageViewModel();
            var folder = FolderRepository.GetById(folderId);
            if (folder != null)
            {
                folder.Shared = shared;
                FolderRepository.SubmitChanges();
                retval.Status = MessageStatus.Success;
            }
            else
            {
                retval.Status = MessageStatus.Failure;
            }
            return Json(retval, JsonRequestBehavior.AllowGet);
		}

		private List<FolderViewModel> BuildFolderViewModelList(IEnumerable<Folder> folders)
		{
			var viewModel = new List<FolderViewModel>();
			foreach (var folder in folders)
			{
				var folderViewModel = new FolderViewModel();
				folderViewModel.Build(folder);
				viewModel.Add(folderViewModel);
			}
			return viewModel;
		}


        public ActionResult GetSharedFolders()
        {
            if (UserEntity != null && UserEntity.Group != null && UserEntity.Group.UserExts != null) {
                var userIDs = UserEntity.Group.UserExts.Where(u => u.aspnet_User != null && u.aspnet_User.aspnet_Membership != null && u.aspnet_User.aspnet_Membership.IsApproved).Select(u => u.UserId).Except(new Guid[] { UserEntity.Id });
                var sharedFolders = FolderRepository.GetAll().Where(f => f.Shared == true && userIDs.Contains(f.UserId));
                var model = BuildFolderViewModelList(sharedFolders);
                return PartialView("SharedFolders", model);
            }
            return new EmptyResult();
        }



        void RenderFoldersSideBarWidget(MessageViewModel retval)
        {
            retval.RenderView = RenderFoldersSideBarWidget();
        }


        string RenderFoldersSideBarWidget()
        {
            if (User.Identity.IsAuthenticated)
            {
                UserEntity.Folders = UserService.GetUserById(UserEntity.Id).Folders;
                return RenderPartialViewToString("SideBarNavigationFoldersWidget", ((UserProfile)(HttpContext.Profile)).NavigationStatus.SideBarStatus.FoldersExpanded);
            }
            return null;
        }

	}
}
