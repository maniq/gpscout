﻿using System;
using System.Linq;
using System.Web.Mvc;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using System.Web;
using AtlanticBT.Common.ComponentBroker;
using GPScout.Domain.Contracts.Services;
using AtlasDiligence.Web.Models.ViewModels.EmailDistributionList;
using AtlasDiligence.Web.Extensions;
using System.Globalization;
using System.Net.Mail;
using System.Web.Configuration;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    [MultipleLocationsAuthorize]
    public class DebuggerController : ControllerBase
    {
        [HttpPost, ValidateInput(false)]
        public ActionResult SendBody(string html)
        {
            string from = WebConfigurationManager.AppSettings["MailFrom"].ToString();
            string to = WebConfigurationManager.AppSettings["NotificationsEmail"].ToString();

            var mail = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = string.Format("Body sent from {0}, user {1}.", Request.UserHostAddress, base.UserEntity.UserName),
                Body = string.Format("User agent: {0}\r\n\r\n{1}", Request.UserAgent, html),
                IsBodyHtml = false
            };
            mail.To.Add(to);

            SmtpClient smtp = new SmtpClient();
            smtp.Send(mail);

            return Json(string.Format("Debug info sent to {0}.", to), JsonRequestBehavior.AllowGet);
        }
    }
}
