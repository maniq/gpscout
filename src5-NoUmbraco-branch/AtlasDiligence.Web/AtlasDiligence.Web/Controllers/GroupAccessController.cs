﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class GroupAccessController : ControllerBase
    {
        private IRepository<Organization> _orgRepository;
        private IRepository<Organization> OrganizationRepository
        {
            get
            {
                if (_orgRepository == null)
                    _orgRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<Organization>>();

                return _orgRepository;
            }
        }

        public ActionResult Index()
        {
            string term = TempData["Term"] as string;
            return View("Index", null, term);
        }


        //public ActionResult Search(string term)
        //{
        //    TempData["Term"] = term;
        //    return Redirect(Url.Action("Index", "Admin", new { lt = 8 }));
        //}


        public JsonResult OrganizationGrid(int pageStart, string sortColumn, bool sortAscending, int rowsPerPage, string term)
        {
            if (rowsPerPage > 100) rowsPerPage = 100;

            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "Name";
                sortAscending = true;
            }

            Expression<Func<Organization, bool>> predicate = o => term == null || (o.Name != null && o.Name.Contains(term));

            var results = new GridList<OrgGroupAccessDataGrid>();
            results.Total = OrganizationRepository.FindAll(predicate).Count();
            results.Result = OrganizationRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, rowsPerPage)
                .Select(x => new OrgGroupAccessDataGrid(x))
                .ToArray();

            return Json(results);
        }


        public JsonResult OrganizationsAutoComplete(string term)
        {
            var orgNames = OrganizationService.SearchByNameWildcard(term);
            return Json(orgNames.Select(o => new { Id = o.Id, Name = o.Name, Url = "/GroupAccess/Edit/" + o.Id }), JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(Guid id, string term)
        {
            var model = this.InitModel(null, id);
            model.Term = term;
            var excludedGroups = this.OrgGroupRestrictedRepository.FindAll(gar => gar.OrganizationId == id).SelectMany(gar => gar.Groups).Distinct().ToArray();
            model.IncludedGroups = GroupRepository.FindAll(g => !excludedGroups.Contains(g)).Select(g => g.Id).ToList();

            return View(model);
        }


        [HttpPost]
        public ActionResult Update(OrgGroupAccessVM model)
        {
            if (ModelState.IsValid)
            {
                var excludedGroups = GroupRepository.FindAll(g => !model.IncludedGroups.Contains(g.Id)).ToArray();

                foreach (var entity in OrgGroupRestrictedRepository.FindAll(gar=>gar.OrganizationId == model.OrganizationID))
                {
                    OrgGroupRestrictedRepository.DeleteOnSubmit(entity);
                }
                OrgGroupRestrictedRepository.SubmitChanges();

                foreach (var group in excludedGroups)
                {
                    var orgGroupRestricted = new GroupOrganizationRestricted()
                    {
                        OrganizationId = model.OrganizationID,
                        GroupId = group.Id,
                        Id = Guid.NewGuid()
                    };
                    OrgGroupRestrictedRepository.InsertOnSubmit(orgGroupRestricted);
                }
                OrgGroupRestrictedRepository.SubmitChanges();
                TempData["Term"] = model.Term;
                return Redirect(Url.Action("Index", "Admin"/*, new { lt = 8 }*/));
            }
            InitModel(model, model.OrganizationID);
            return View("Edit", model);
        }


        private OrgGroupAccessVM InitModel(OrgGroupAccessVM model, Guid orgId)
        {
            if (model == null) model = new OrgGroupAccessVM();
            var org = OrganizationRepository.GetById(orgId);
            if (org != null) model.OrganizationName = org.Name;

            model.OrganizationID = orgId;
            model.AllGroups = GroupRepository.GetAll().OrderBy(g => g.Name).ToList();
            return model;
        }
    }
}