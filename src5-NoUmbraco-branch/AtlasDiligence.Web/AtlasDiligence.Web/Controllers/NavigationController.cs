﻿using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.Models.ViewModels.Navigation;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    public class NavigationController : ControllerBase
    {
        public ActionResult SideBarNavigation()
        {
            UserProfile profile = (UserProfile)HttpContext.Profile;
            var model = new SideBarNavigationViewModel();
            model.Build(profile.NavigationStatus.SideBarStatus);
            return PartialView("_SideBarNavigation", model);
        }


        public ActionResult UpdateSideBarStatus(bool? SideBarExpanded, bool? HistoryExpanded, bool? SearchExpanded, bool? FoldersExpanded)
        {
            UserProfile profile = (UserProfile)HttpContext.Profile;
            if (SideBarExpanded.HasValue) profile.NavigationStatus.SideBarStatus.SideBarExpanded = SideBarExpanded.Value;
            else if (HistoryExpanded.HasValue) profile.NavigationStatus.SideBarStatus.HistoryExpanded = HistoryExpanded.Value;
            else if (SearchExpanded.HasValue) profile.NavigationStatus.SideBarStatus.SearchExpanded = SearchExpanded.Value;
            else if (FoldersExpanded.HasValue) profile.NavigationStatus.SideBarStatus.FoldersExpanded = FoldersExpanded.Value;
            profile.Save();
            return new EmptyResult();
        }

    }
}
