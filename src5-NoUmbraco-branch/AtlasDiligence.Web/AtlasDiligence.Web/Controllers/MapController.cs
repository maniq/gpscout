﻿using AtlasDiligence.Web.Cors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    [AllowCrossSite]
    public class MapController : ControllerBase
    {
        //
        // GET: /Map/

        public ActionResult Index()
        {
            return View();
        }
    }
}
