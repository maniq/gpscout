﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Web.Models.ViewModels;

namespace AtlasDiligence.Web.Controllers
{
    public class LandingController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.Request.IsAuthenticated)
                return RedirectToAction("Index", "Basic");

            var model = new LandingViewModel();
            return View(model);
        }

    }
}
