﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Web.Resources;
using log4net;

namespace AtlasDiligence.Web.Controllers
{
    [AtlasAuthorize]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class UserController : AccountBaseController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(UserController));

        /// <summary>
        /// User grid for managing users admin
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="userName"></param>
        /// <param name="email"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AdminUserGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string email, string firstName, string lastName)
        {
            var excludedUsers = new List<Guid>() { GetLoggedInUserId() };
            GridList<UserDataGrid> results = UserService.GetUserGridListResults(pageStart, sortColumn, sortAscending, DataGridRowsPerPage(),
                email, firstName, lastName, excludedUsers, true, null);

            return Json(results);
        }

        /// <summary>
        /// User grid for managing user requests
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="userName"></param>
        /// <param name="email"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AdminUserRequestsGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string email, string firstName, string lastName)
        {
            var excludedUsers = new List<Guid>() { GetLoggedInUserId() };
            GridList<UserDataGrid> results = UserService.GetUserGridListResults(pageStart, sortColumn, sortAscending, DataGridRowsPerPage(),
                email, firstName, lastName, excludedUsers, false, null);

            return Json(results);
        }

        /// <summary>
        /// User grid for group leader
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="userName"></param>
        /// <param name="email"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        public JsonResult UserGrid(int pageStart, string sortColumn, bool sortAscending, string userName, string email, string firstName, string lastName, Guid? groupId)
        {
            var excludedUsers = new List<Guid>() { GetLoggedInUserId() };
            GridList<UserDataGrid> results = UserService.GetUserGridListResults(pageStart, sortColumn, sortAscending, DataGridRowsPerPage(),
                email, firstName, lastName, excludedUsers, true, groupId);

            return Json(results);
        }

        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        public ActionResult Create(Guid? groupId)
        {
            var model = UserViewModel.Build(UserEntity, groupId, UserService);

            return View(model);
        }

        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        [HttpPost]
        public ActionResult Create(UserViewModel model)
        {
            //group validate
            if (model.GroupId.HasValue && !CanAccessGroup(model.GroupId.Value))
            {
                ViewData["Message"] = ErrorMessages.IncorrectGroup;
                return View();
            }

            if (ModelState.IsValid)
            {
                if (!User.IsInRole(RoleNames.Admin)) // if not an admin, then a group leader
                {
                    //model.RssAllowed = UserEntity.RssAllowed;
                    //model.DiligenceAllowed = UserEntity.DiligenceAllowed;
                    model.IsApproved = true;
                }
                var returnMessage = UserService.CreateUser(model, User.IsInRole(RoleNames.Admin));
                if (returnMessage == MembershipCreateStatus.Success)
                {
                    TempData["Message"] = string.Format("{0} has been created.", model.Email);
                    return model.GroupId.HasValue
                        ? User.IsInRole(RoleNames.Admin)
                            ? RedirectToAction("Edit", "Group", new { id = model.GroupId })
                            : RedirectToAction("View", "Group", new { id = model.GroupId })
                        : RedirectToAction("Index", "Admin");
                }
                if (returnMessage == MembershipCreateStatus.DuplicateEmail || returnMessage == MembershipCreateStatus.DuplicateUserName)
                {
                    ModelState.AddModelError("Email", ErrorMessages.DuplicateEmail);
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(returnMessage));
                }
            }

            UserViewModel.Build(UserEntity, model, UserService);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnRoute"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(Guid id, string returnRoute)
        {
            var modelEntity = UserService.GetUserEntityByUserId(id);
            if (modelEntity == null)
            {
                ViewData["Message"] = string.Format(ErrorMessages.InvalidId, "User");
                return View();
            }
            if (!CanAccessUser(modelEntity))
            {
                ViewData["Message"] = ErrorMessages.CannotAccessUser;
                return View();
            }

            UserViewModel model = UserViewModel.Build(UserEntity, modelEntity, UserService);
            model.ReturnRoute = returnRoute;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel model)
        {
            //group validate
            if (model.GroupId.HasValue && !CanAccessGroup(model.GroupId.Value))
            {
                ViewData["Message"] = ErrorMessages.IncorrectGroup;
                return View();
            }

            if (ViewData.ModelState.IsValid)
            {
                var modelEntity = UserService.GetUserEntityByUserId(model.Id ?? Guid.Empty);
                //validation checks
                if (modelEntity == null)
                {
                    ViewData["Message"] = string.Format(ErrorMessages.InvalidId, "User");
                    return View();
                }
                if (!CanAccessUser(modelEntity))
                {
                    ViewData["Message"] = ErrorMessages.CannotAccessUser;
                    return View();
                }
                //map
                UserEntity.Map(modelEntity, model, User.IsInRole(RoleNames.Admin));
                //update
                var returnMessage = UserService.UpdateUser(modelEntity);
                if (returnMessage == UpdateUserMessage.Success)
                {
                    TempData["Message"] = string.Format("The User \"{0}\" has been edited.", model.Email);

                    //repopulate UserEntity user being edited is logged in user
                    if (model.Id == UserEntity.Id)
                        SetUserEntity(UserService.GetUserEntityByUserId(UserEntity.Id));

                    //redirect for different referral sources
                    if (model.ReturnRoute == "Group")
                    {
                        return this.Redirect(model.ReturnRoute + "?groupId=" + model.GroupId);
                    }
                    else
                    {
                        return !string.IsNullOrWhiteSpace(model.ReturnRoute)
                            ? this.Redirect(model.ReturnRoute.StartsWith("/") ? model.ReturnRoute : "/" + model.ReturnRoute)
                                   : this.Redirect("/Basic");
                    }
                }
                else if (returnMessage == UpdateUserMessage.DeniedDuplicateEmail)
                {
                    ModelState.AddModelError("Email", AccountValidation.ErrorCodeToString(returnMessage));
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(returnMessage));
                }
            }

            UserViewModel.Build(UserEntity, model, UserService);
            return View(model);
        }

        /// <summary>
        /// Unlocks a locked user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        public JsonResult Unlock(Guid id)
        {
            var model = new MessageViewModel();

            if (UserService.UnlockUser(UserService.GetUserEntityByUserId(id)))
            {
                model.Status = MessageStatus.Success;
                model.Message = Resources.ErrorMessages.UserIsUnlocked;
            }
            else
            {
                model.Status = MessageStatus.Failure;
                model.Message = Resources.ErrorMessages.UserUnlockError;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Reset given user password.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        public JsonResult ResetPassword(Guid id, string password)
        {
            var model = new MessageViewModel();

            if (!passwordStrengthRegexp.IsMatch(password))
            {
                model.Status = MessageStatus.Failure;
                model.Message = "The password you entered does not meet the security requirements.";
            }
            else
            {
                var user = UserService.GetUserEntityByUserId(id);

                if (user != null && user.IsApproved && UserService.ResetPassword(user, password))
                {
                    model.Status = MessageStatus.Success;
                    model.Message = string.Format(Resources.ErrorMessages.PasswordIsReset, user.Email);
                }
                else
                {
                    model.Status = MessageStatus.Failure;
                    model.Message = string.Format(Resources.ErrorMessages.PasswordResetError,
                                                  user != null ? user.Email : string.Empty);
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Toggle on/off user active status.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns> 
        [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
        public JsonResult ChangeUserActiveStatus(Guid id)
        {
            var model = new MessageViewModel();
            var userEntity = UserService.GetUserEntityByUserId(id);

            if (userEntity != null && CanAccessUser(userEntity))
            {
                var returnMessage = UserService.ChangeActiveStatus(userEntity);

                switch (returnMessage)
                {
                    case ActivateUserMessage.Success:
                        model.Status = MessageStatus.Success;
                        break;
                    case ActivateUserMessage.DeniedMaxUsers:
                        model.Status = MessageStatus.Failure;
                        model.Message = ErrorMessages.ActivateGroupMaxUsersError;
                        break;
                    default:
                        model.Status = MessageStatus.Failure;
                        model.Message = Resources.ErrorMessages.DefaultError;
                        break;
                }
            }
            else
            {
                model.Status = MessageStatus.Failure;
                model.Message = string.Format(ErrorMessages.InvalidId, "Group");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Grant access request to a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult GrantAccessRequest(Guid id)
        {
            var model = new MessageViewModel();
            var userEntity = UserService.GetUserEntityByUserId(id);

            if (userEntity != null)
            {
                if (UserService.GrantAccessRequest(userEntity))
                {
                    model.Status = MessageStatus.Success;
                    model.Data = new { RedirectUrl = Url.Action("Edit", "User", new { id = id, returnRoute = "Admin" }) };
                }
                else
                {
                    model.Status = MessageStatus.Failure;
                    model.Message = Resources.ErrorMessages.DefaultError;
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Only allow Delete for RequestAccess Users (AccessGranted == false)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        [HttpGet]
        public JsonResult Delete(Guid id)
        {
            var model = new MessageViewModel();
            var userEntity = UserService.GetUserEntityByUserId(id);
            if (userEntity == null)
            {
                model.Message = string.Format(Resources.ErrorMessages.InvalidId, "User");
                model.Status = MessageStatus.Failure;
            }
            else if (userEntity.Id == UserEntity.Id)
            {
                model.Message = string.Format(Resources.ErrorMessages.CannotDeleteCurrentUser, "User");
                model.Status = MessageStatus.Failure;
            }
            else
            {
                UserService.DeleteUser(userEntity);

                model.Message = Resources.ErrorMessages.DeletedRow;
                model.Status = MessageStatus.Success;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult GetUserEmails(string term)
        {
            return Json(this.UserService.GetUserEmails(term).ToArray(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Determines whether current logged in user can access given user.
        /// </summary>
        /// <param name="userToModify"></param>
        /// <returns></returns>
        private bool CanAccessUser(UserEntity userToModify)
        {
            //logged in user Admin?
            if (User.IsInRole(RoleNames.Admin))
                return true;
            //is logged in user a Group Leader?
            if (UserEntity.IsGroupLeader && userToModify.GroupId == UserEntity.GroupId)
                return true;

            //is logged in user also the user being modified?
            return UserEntity.Id == userToModify.Id;
        }

    }
}
