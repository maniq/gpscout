﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels.TrackUsers;
using AtlasDiligence.Web.Models.ViewModels;

namespace AtlasDiligence.Web.Controllers
{
	[Authorize(Roles = "Admin,Employee")]
    [MultipleLocationsAuthorize]
    public class TrackUsersController : ControllerBase
	{
		private IUserService _userService
		{
			get
			{
				return ComponentBrokerInstance.RetrieveComponent<IUserService>();
			}
		}


        private GPScout.Domain.Contracts.Services.IUserService __userService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<GPScout.Domain.Contracts.Services.IUserService>();
            }
        }


        public ActionResult Index()
		{
            base.CopyModelState();
            var model = TempData["Model"] as TrackUsersViewModel;
            return View(model);
		}

		[HttpPost]
		public ActionResult Index(TrackUsersViewModel model)
		{
			if (ModelState.IsValid)
			{
				if (model.EndDate < model.StartDate)
				{
					ModelState.AddModelError("EndDate", "End Date must be less than the Start Date");
				}
				else
				{
					return GenerateExcelExport(model);
				}
			} else
            {
                ModelState.AddModelError(String.Empty, "Please specify a valid date range");
            }

            TempData["AdminViewModel"] = new AdminViewModel() { DefaultTabUrl = "/TrackUsers" };
            TempData["ModelState"] = ModelState;
            TempData["Model"] = model;
            return RedirectToAction("Index", "Admin");
        }

		private ExcelResult GenerateExcelExport(TrackUsersViewModel model)
		{
            var headers = new List<string> { "Date", "Time", "First Name", "Last Name", "Email", "Group" };
            if (model.TrackType == TrackUsersType.ProfileVisit || model.TrackType == TrackUsersType.ProfileFullPdfReport)
            {
                headers.Add("Organization Name");
            }

            var excelResult = new ExcelResult
            {
                Headers = headers,
                DeleteFile = true,
                FileName = "Trackusers.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"],
                Rows = __userService.GetExcelData(model.TrackType, model.StartDate.GetValueOrDefault(), model.EndDate.GetValueOrDefault(), headers)
            };

            return excelResult;
        }
	}
}
