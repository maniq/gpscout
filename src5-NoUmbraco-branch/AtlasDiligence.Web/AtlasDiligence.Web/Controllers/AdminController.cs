﻿using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.Data;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Extensions;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.EmailDistributionList;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Models.Tasks;
using GPScout.Domain.Contracts.Models.UserNotifications;
using GPScout.Domain.Contracts.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    [MultipleLocationsAuthorize]
    public class AdminController : ControllerBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AdminController));
        private IIndexService IndexService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IIndexService>();
            }
        }

        private IEmailDistributionListService EmailDistributionListService
         {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEmailDistributionListService>();
            }
        }

        public ActionResult ReIndex()
        {
            IndexService.ReIndex();
            return Content("Re-indexed");
        }

        public ActionResult Index()
        {
            AdminViewModel model = TempData["AdminViewModel"] as AdminViewModel;
            if (model == null) model = new AdminViewModel();
            model.UserEntity = UserEntity;
            return View(model);
        }

        public ActionResult UpdateEula()
        {
            var viewModel = new EulaViewModel();
            viewModel.Build(EulaRepository);
            return this.View(viewModel);
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult UpdateEula(EulaViewModel viewModel)
        {
            if (String.IsNullOrWhiteSpace(viewModel.Text))
            {
                ModelState.AddModelError("Text", "You must enter at least 1 character.");
            }
            if (ModelState.IsValid)
            {
                EulaRepository.InsertEula(new Eula
                    {
                        Id = Guid.NewGuid(),
                        Text = HttpUtility.HtmlDecode(viewModel.Text)
                    });
                ModelState.Remove("Text");
                viewModel.Build(EulaRepository);
            }
            return this.View(viewModel);
        }


        public ActionResult UpdateScoutCategoryDefinitions()
        {
            var viewModel = new UpdateScoutCategoryDefinitionsVM();
            viewModel.Build(ScoutCategoryDefinitionRepository);
            return this.View(viewModel);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateScoutCategoryDefinitions(UpdateScoutCategoryDefinitionsVM viewModel)
        {
            if (String.IsNullOrWhiteSpace(viewModel.Text))
            {
                ModelState.AddModelError("Text", "You must enter at least 1 character.");
            }
            if (ModelState.IsValid)
            {
                var currentDefinitions = ScoutCategoryDefinitionRepository.GetAll().SingleOrDefault();
                ScoutCategoryDefinitionRepository.InsertOnSubmit(new ScoutCategoryDefinition
                {
                    Id = Guid.NewGuid(),
                    Text = HttpUtility.HtmlDecode(viewModel.Text)
                });
                if (currentDefinitions != null)
                    ScoutCategoryDefinitionRepository.DeleteOnSubmit(currentDefinitions);
                ScoutCategoryDefinitionRepository.SubmitChanges();
                ModelState.Remove("Text");
                viewModel.Build(ScoutCategoryDefinitionRepository);
            }
            return this.View(viewModel);
        }


        public ExcelResult ExportUserSearch()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Search Term" },
                DeleteFile = true,
                FileName = "UserSearchTerms.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.SearchTerms != null && m.SearchTerms.Count() > 0))
            {
                rows.AddRange(user.SearchTerms.Select(searchTerm => new Dictionary<string, string>
                                                                        {
                                                                            {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                                            {"Email", user.aspnet_Membership.LoweredEmail},
                                                                            {"Search Term", searchTerm.SearchTerm}
                                                                        }));
            }
            retval.Rows = rows;
            return retval;
        }

        public ExcelResult ExportUserInformation()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Phone", "Title", "Organization Name", "Organization Type", "Referred By", "Group", "Active" },
                DeleteFile = true,
                FileName = "UserInformation.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.UserExt != null))
            {
                rows.Add(new Dictionary<string, string>{
                                                        {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                        {"Email", user.aspnet_Membership.LoweredEmail},
                                                        {"Phone", user.UserExt.Phone},
                                                        {"Title",  user.UserExt.Title},
                                                        {"Organization Name",  user.UserExt.OrganizationName},
                                                        {"Organization Type",  user.UserExt.OrganizationType.HasValue && user.UserExt.OrganizationType.Value != 0 ? ((OrganizationType) user.UserExt.OrganizationType).GetDescription() : string.Empty},
                                                        {"Referred By",  user.UserExt.ReferredBy},
                                                        {"Group", user.UserExt.Group != null ? user.UserExt.Group.Name : String.Empty},
                                                        {"Active", user.aspnet_Membership != null && user.aspnet_Membership.IsApproved ? "Yes" : "No" }
                                                    });
            }
            retval.Rows = rows;
            return retval;
        }


        #region Email Distribution Lists - EDL

        public ActionResult EmailDistributionLists()
        {
            var lists = EmailDistributionListService.GetAllActiveLists().Select(x => new EmailDistributionListViewModel()
            {
                Active = x.Active,
                Emails = x.Emails,
                ID = x.ID,
                Name = x.Name
            });
            return PartialView(lists);
        }

        [HttpPost]
        public ActionResult EDLAddEmail(EmailDistributionListViewModel model)
        {
            if (ModelState.IsValid)
            {
                var results = EmailDistributionListService.AddEmail(new Common.Data.EmailDistributionListEmail { Email = model.EmailToAdd, Active = true, DistributionListID = model.ID });
                ModelState.AddModelErrorUI("EmailToAdd", results);
            }
            else
                ModelState.AddModelError("EmailToAdd", "Invalid data.");

            var distriblist = EmailDistributionListService.GetAllActiveLists().Single(x => x.ID == model.ID);
            //model.EmailToAdd = string.Empty;
            ModelState.SetModelValue("EmailToAdd",  new ValueProviderResult("", "", CultureInfo.CurrentCulture));
            model.Emails = distriblist.Emails;
            model.Name = distriblist.Name;
            return PartialView("EmailDistributionList", model);
        }


        [HttpPost]
        public ActionResult EDLRemoveEmail(int edlListID, int emailID)
        {
            if (ModelState.IsValid)
            {
                var results = EmailDistributionListService.DeleteEmail(emailID);
                ModelState.AddModelErrorUI(String.Empty, results);
            }
            else
                ModelState.AddModelError(String.Empty, "Invalid data.");

            var distriblist = EmailDistributionListService.GetAllActiveLists().Single(x => x.ID == edlListID);
            var model = new EmailDistributionListViewModel();
            model.ID = distriblist.ID;
            model.Emails = distriblist.Emails;
            model.Name = distriblist.Name;
            return PartialView("EmailDistributionList", model);
        }
        #endregion


        #region Data Import
        public ActionResult DataImport()
        {
            var mainModel = TempData["AdminViewModel"] as AdminViewModel;
            return PartialView(mainModel != null ? mainModel.DataImportResults : null);
        }


        public ActionResult DataImportStatus(Guid taskId)
        {
            var task = TaskService.GetById(taskId) as DataImportTask;
            ResultMessages dataImportResults = (task != null) ? task.Results : new ResultMessages();
            TempData["AdminViewModel"] = new AdminViewModel() { DefaultTabUrl = "#tab-data-import", DataImportResults = dataImportResults };
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public string SalesforceDataImport()
        {
            var notificationsHtml = new StringBuilder();
            try
            {
                Guid taskId = Guid.NewGuid();

                var dataImportTask = new DataImportTask()
                {
                    Id = taskId,
                    ReferenceId = taskId.ToString(),
                    UserId = UserEntity.Id,
                    Status = DownloadableItemLifeCycle.Unknown,
                    DateExpired = DateTime.Now.AddSeconds(AppSettings.DataImportRespawnSeconds)
                };
                TaskService.Save(dataImportTask);

                var notification = new DataImportNotification()
                {
                    Id = Guid.NewGuid(),
                    PollingExpirationDate = dataImportTask.DateExpired,
                    UserId = UserEntity.Id,
                    ReferenceId = taskId.ToString(),
                    Data = new NotificationData()
                };

                UserNotificationService.Save(notification);
                this.UserEntity.Notifications.AddOrUpdate(notification);

                notificationsHtml.AppendLine(base.RenderPartialViewToString("UserNotification", notification));
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return notificationsHtml.ToString();
        }


        public string StartDataImportTask(Guid taskId)
        {
            var task = TaskService.GetById(taskId) as DataImportTask;
            if (task != null && task.Status == DownloadableItemLifeCycle.Unknown)
            {
                // starting new task
                task.Status = DownloadableItemLifeCycle.Started;
                TaskService.Save(task);
                StartDataImportTask(task);
            }
            return "ok";
        }


        private void StartDataImportTask(DataImportTask dataImportTask)
        {
            try
            {
                Guid id = Guid.Parse(dataImportTask.ReferenceId);
                var task = new Task<int>(() =>
                {
                    StringBuilder output = null;
                    StringBuilder errors = null;
                    try
                    {
                        Utils.RunProcess(String.Empty, AppSettings.DataImportRespawnSeconds * 1000, AppSettings.DataImportApplication, AppSettings.TempPath, out output, out errors);
                        ProcessResults(ref dataImportTask, null, output, errors);
                    }
                    catch (Exception ex)
                    {
                        ProcessResults(ref dataImportTask, ex, output, errors);
                    }
                    return 0;
                });
                task.Start();
            }
            catch (Exception ex)
            {
                ProcessResults(ref dataImportTask, ex, null, null);
            }
        }


        private void ProcessResults(ref DataImportTask task, Exception ex, StringBuilder output, StringBuilder errors)
        {
            if (ex != null)
            {
                Log.Error(ex);
                string errMsg = ex.ToString();
                if (errors == null) errors = new StringBuilder(errMsg);
                else if (!errors.ToString().Contains(errMsg)) errors.AppendLine(errMsg);
            }

            var results = new ResultMessages();
            if (errors != null)
                results.Error = errors.ToString();

            if (output != null)
                results.Output = output.ToString();

            // ignore log4net cannot roll file warning and consider just real error entries that contains the word ERROR from the logger
            task.Status = (results.HasError && results.Error.Contains("ERROR ")) ? DownloadableItemLifeCycle.Error : DownloadableItemLifeCycle.GenerationCompleted;
            task.Results = results;
            TaskService.Save(task);
        }
        #endregion
    }
}
