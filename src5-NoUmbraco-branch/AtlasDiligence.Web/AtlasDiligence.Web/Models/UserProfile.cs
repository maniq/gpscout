﻿using AtlasDiligence.Web.Models.ViewModels.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;

namespace AtlasDiligence.Web.Models
{
    public class UserProfile : ProfileBase
    {
        public NavigationStatus NavigationStatus
        {
            get { return this.GetPropertyValue("NavigationStatus") as NavigationStatus; }
            set { this.SetPropertyValue("NavigationStatus", value); }
        }

    }
}