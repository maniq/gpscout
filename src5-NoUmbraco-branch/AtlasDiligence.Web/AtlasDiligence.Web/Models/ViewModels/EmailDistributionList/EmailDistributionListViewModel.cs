﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlasDiligence.Web.Models.ViewModels.EmailDistributionList
{
    public class EmailDistributionListViewModel : AtlasDiligence.Common.Data.EmailDistributionList
    {
        [Display(Description = "Email", Name="Email")]
        [Required]
        [AtlanticBT.Common.Validation.Email(ErrorMessage = "Email is not in correct format.")]
        [DataType(DataType.EmailAddress)]
        public string EmailToAdd { get; set; }
    }
}
