﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Navigation
{
    [Serializable]
    public class SideBarNavigationStatus
    {
        public bool SideBarExpanded { get; set; }
        public bool HistoryExpanded { get; set; }

        public bool SearchExpanded { get; set; }
        public bool FoldersExpanded { get; set; }

        public SideBarNavigationStatus()
        {
            SideBarExpanded = HistoryExpanded = SearchExpanded = FoldersExpanded = true;
        }
    }
}