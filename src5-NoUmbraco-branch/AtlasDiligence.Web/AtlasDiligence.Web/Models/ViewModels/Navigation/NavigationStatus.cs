﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Navigation
{
    [Serializable]
    public class NavigationStatus
    {
        public SideBarNavigationStatus SideBarStatus { get; set; }

        public NavigationStatus ()
        {
            SideBarStatus = new SideBarNavigationStatus();
        }

    }
}