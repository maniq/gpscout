﻿using AtlasDiligence.Web.Models.ViewModels.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Navigation
{
    public class SideBarNavigationViewModel
    {
        public SideBarNavigationStatus SideBarStatus { get; set; }

        internal void Build(SideBarNavigationStatus sideBarStatus)
        {
            this.SideBarStatus = sideBarStatus;
        }
    }
}