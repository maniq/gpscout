﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Navigation
{
    public class NavigationLinkViewModel
    {
        public string Url { get; set; }
        public string ViewName { get; set; }
        public object Model { get; set; }
    }
}