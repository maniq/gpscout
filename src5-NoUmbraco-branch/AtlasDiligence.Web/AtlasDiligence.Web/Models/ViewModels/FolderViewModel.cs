﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class FolderViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IList<AtlasDiligence.Common.Data.Models.Organization> Organizations { get; set; }

        public bool Shared { get; set; }
        public Guid UserId { get; set; }
        public void Build(Folder folder)
        {
            this.Id = folder.Id;
            this.Name = folder.Name;
            this.Shared = folder.Shared.GetValueOrDefault();
            this.UserId = folder.UserId;
            this.Organizations = folder.FolderOrganizations == null
                ? new List<Common.Data.Models.Organization>()
                : folder.FolderOrganizations.Select(m => m.Organization).ToList();
        }

        public FolderViewModel(Folder folder)
        {
            Build(folder);
        }

        public FolderViewModel() { }
    }
}