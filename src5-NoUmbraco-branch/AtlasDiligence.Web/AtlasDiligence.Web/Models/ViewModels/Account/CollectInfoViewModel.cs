﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AtlasDiligence.Web.General;
using System.ComponentModel;

namespace AtlasDiligence.Web.Models.ViewModels.Account
{
    public class CollectInfoViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Organization Name")]
        public string OrganizationName { get; set; }

        [Required]
        [DisplayName("Organization Type")]
        public OrganizationType OrganizationType { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Referred By")]
        public string ReferredBy { get; set; }
    }
}