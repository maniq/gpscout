﻿namespace AtlasDiligence.Web.Models.ViewModels
{
    public class SectionContentViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Flag { get; set; }
        public string DocumentName { get; set; }
        public string DocumentUrl { get; set; }
        public string BookmarkName { get; set; }
    }
}