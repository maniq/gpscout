﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class TermsAndConditionsViewModel
    {
        [Display(Name = "Terms and Conditions")]
        public string Text { get; set; }
        [Display(Name = "I Agree to the Terms and Conditions")]
        public bool IsChecked { get; set; }

        public void Build(IEulaRepository eulaRepository)
        {
            this.Text = eulaRepository.GetCurrentEula().Text;
        }
    }
}