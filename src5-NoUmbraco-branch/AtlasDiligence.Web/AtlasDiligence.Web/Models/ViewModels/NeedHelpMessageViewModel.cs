﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class NeedHelpMessageViewModel
    {
        [DisplayName(@"Email:")]
        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        [DisplayName(@"Message:")]
        [Required]
        [AllowHtml]
        public string Message { get; set; }

    }
}