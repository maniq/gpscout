﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class OrganizationHTViewModel
    {
        public IList<OrganizationHTEventViewModel> Events;

        public void Build(Organization org)
        {
            Events = new List<OrganizationHTEventViewModel>();
            if (org.YearFounded.HasValue)
                Events.Add(new OrganizationHTEventViewModel { EventDate = new DateTime(org.YearFounded.Value, 1, 1), EventType = OrganizationHistoryEventType.FirmFounded, EventName = org.Name });

            if (org.Funds != null)
                foreach (Fund fund in org.Funds.Where(f => f.VintageYear.HasValue || f.FundLaunchDate.HasValue))
                {
                    DateTime? fundDate = fund.VintageYear.HasValue ? new DateTime(fund.VintageYear.Value, 1, 1) : fund.FundLaunchDate;
                    if (fundDate.HasValue)
                    {
                        Events.Add(new OrganizationHTFundEventViewModel
                        {
                            EventDate = fundDate.Value,
                            EventType = OrganizationHistoryEventType.FundRaise,
                            EventName = fund.Name,
                            FundSize = fund.IsOpen ? fund.TargetSize : fund.FundSize,
                            FundCurrency = fund.Currency,
                            IsOpenFund = fund.IsOpen
                        });
                    }
                }


            if (org.ExpectedNextFundRaise.HasValue && !string.Equals(org.FundRaisingStatus, "open", StringComparison.InvariantCultureIgnoreCase))
                Events.Add(new OrganizationHTFundEventViewModel {
                    EventDate = org.ExpectedNextFundRaise.Value, EventType = OrganizationHistoryEventType.ExpectedNextFundRaise, FundSize = org.LastFundSize,
                    FundCurrency = org.Currency
                });


            Events = Events.OrderBy(e => e, new OrganizationHTEventComparer()).ToList();
        }

    }


    public class OrganizationHTEventComparer : IComparer<OrganizationHTEventViewModel>
    {
        public int Compare(OrganizationHTEventViewModel x, OrganizationHTEventViewModel y)
        {
            if (x == null) return -1;
            if (y == null) return 1;
            if (x.EventDate.Year != y.EventDate.Year)
                return DateTime.Compare(x.EventDate, y.EventDate);

            if (x.EventType == OrganizationHistoryEventType.FirmFounded && y.EventType != OrganizationHistoryEventType.FirmFounded)
                return -1;

            if (y.EventType == OrganizationHistoryEventType.FirmFounded && x.EventType != OrganizationHistoryEventType.FirmFounded)
                return 1;
        

            if (x.EventType == OrganizationHistoryEventType.FundRaise && y.EventType != OrganizationHistoryEventType.FundRaise)
                return -1;

            if (y.EventType == OrganizationHistoryEventType.FundRaise && x.EventType != OrganizationHistoryEventType.FundRaise)
                return 1;

            if (x.EventDate == y.EventDate)
                return string.Compare(x.EventName, y.EventName);
            else
                return DateTime.Compare(x.EventDate, y.EventDate);
        }
    }
}