﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public enum OrganizationHistoryEventType : int
    {
        FirmFounded = 1,
        FundRaise = 2,
        ExpectedNextFundRaise = 3
    }

    public class OrganizationHTEventViewModel
    {
        public OrganizationHistoryEventType EventType { get; set; }
        public DateTime EventDate;
        public string EventName;
        public bool RenderDateOnLeft;
    }
}