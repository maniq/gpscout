﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class OrganizationHTFundEventViewModel : OrganizationHTEventViewModel
    {
        public decimal? FundSize;
        public Currency FundCurrency;
        public bool IsOpenFund;
    }
}