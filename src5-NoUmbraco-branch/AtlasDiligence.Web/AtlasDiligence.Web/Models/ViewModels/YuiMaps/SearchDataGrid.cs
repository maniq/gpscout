﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.General;
using GPScout.Domain.Contracts.Enums;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class SearchDataGrid
    {
        public SearchDataGrid(AtlasDiligence.Common.Data.Models.Organization backingOrg, string FPVHtml)
        {
            this.Id = backingOrg.Id;

            this.AccessConstrained = backingOrg.AccessConstrained;
            this.EmergingManager = backingOrg.EmergingManager;
            this.FocusRadar = backingOrg.FocusRadar;
            this.KeyTakeAway = backingOrg.KeyTakeAway;
            string name = backingOrg.Name.HtmlHardBreaks(20, "(");
            name = backingOrg.PublishOverviewAndTeam ? string.Format("<a href=\"{0}\" class=\"org-name\">{1}</a>", VirtualPathUtility.ToAbsolute("~/Organization/Index/" + backingOrg.Id), name)
                : "<span class=\"org-name\">" + name + "</span>";
            //var emptyDiv = "<div class=\"empty\">&nbsp;</div>";
            //var rightEmptyDiv = "<div class=\"empty\" style=\"width:16px;\">&nbsp;</div>";
            ////this.Name = string.Format("{0} {1}",
            ////                          name,
            ////                          !string.IsNullOrWhiteSpace(backingOrg.KeyTakeAway)
            ////                            ? string.Format("<div class='key-takeaway simple-tooltip' title='{0}'>{0}</div>", backingOrg.KeyTakeAway)
            ////                            : string.Empty);

            ////var lastFundSize = (backingOrg.LastFundSize != null) ? (backingOrg.LastFundSize.Value > 0 ? backingOrg.Currency.FormattedSymbol + Math.Round(backingOrg.LastFundSize.Value, 0, MidpointRounding.AwayFromZero) : "") : "";
            ////var expectedNextFundRaise = (!String.IsNullOrEmpty(backingOrg.FundRaisingStatus) ? backingOrg.FundRaisingStatus.ToLower() == "open" || backingOrg.FundRaisingStatus.ToLower() == "pre-marketing" ? "N/A" : String.Format("{0:y}", backingOrg.ExpectedNextFundRaise) : "");
            ////var tooltipText = String.Format("<strong>Primary Strategy: </strong>{0}<br />"
            ////    + "<strong>Sector: </strong>{1}<br />"
            ////    + "<strong>Market/Stage: </strong>{2}<br />"
            ////    + "<strong>Fundraising Status: </strong>{3}<br />"
            ////    + "<strong>Expected Next Fundraise: </strong>{4}<br />"
            ////    + "<strong>Last Fund Size (mm): </strong>{5}"
            ////    + "{6}{7}"
            ////    , (!String.IsNullOrEmpty(backingOrg.StrategyPipeDelimited) ? backingOrg.StrategyPipeDelimited.Replace("|", ",") : "")
            ////    , (!String.IsNullOrEmpty(backingOrg.SectorFocusPipeDelimited) ? backingOrg.SectorFocusPipeDelimited.Replace("|", ",") : "")
            ////    , backingOrg.MarketStage
            ////    , backingOrg.FundRaisingStatus
            ////    , expectedNextFundRaise
            ////    , lastFundSize
            ////    , (!String.IsNullOrEmpty(backingOrg.OrganizationOverview)
            ////        ? "<hr class='horizontalRule' /><strong>Overview:  </strong>" + HttpContext.Current.Server.HtmlEncode(backingOrg.OrganizationOverview.NewlineToBreak())
            ////        : "")
            ////    , HttpContext.Current.Server.HtmlEncode(FPVHtml)
            ////    );


            //string requestOrgIconHtml = !backingOrg.PublishOverviewAndTeam ? "<a href=\"/Request/RequestOrganization\" class=\"request-information simple-tooltip\" data-id=\"" + backingOrg.Id + "\" data-name=\"" + backingOrg.Name + "\">Request Information</a>" : String.Empty;
            string isInProcessOrgIconHtml = backingOrg.IsInProcess ? "<div class=\"under-construction\" data-org-id=\"" + backingOrg.Id + "\" data-user-registered-for-PCN=\"" + (backingOrg.IsUserRegisteredForPUCN.HasValue ? backingOrg.IsUserRegisteredForPUCN.Value ? "1" : "2" : "") + "\"></div>" : String.Empty;

            string iconDiv = string.Format("<div class=\"grid-icons\">{0}{1}</div>", FPVHtml, isInProcessOrgIconHtml);
            this.Name = string.Format("{0} {1}",
                                      "<div class=\"divParent\"><div>" + name + "</div>",
                                      iconDiv);

            //var focusRadar = String.IsNullOrWhiteSpace(this.FocusRadar)
            //                     ? string.Format(emptyDiv)
            //                     : string.Format("<div class=\"{0} icon\" title=\"{1}\">{1}</div>",
            //                                     this.FocusRadar.ToLower().Replace(' ', '-'), this.FocusRadar);

            //var emergingManager = this.EmergingManager ?
            //    string.Format("<div class=\"emerging-manager-true icon\" title=\"Emerging Manager\">{0}</div>",
            //    this.EmergingManager.ToString().ToLower()) : string.Format(emptyDiv);

            //var accessConstrained = this.AccessConstrained ?
            //    string.Format("<div class=\"access-constrained-true icon\" title=\"Access Constrained\">{0}</div>",
            //    this.AccessConstrained.ToString().ToLower()) : string.Format(rightEmptyDiv);

            //if (backingOrg.InactiveFirm)
            //    this.Icons = "Inactive";
            //else
            //    this.Icons = string.Format("{0} {1} {2}", focusRadar, emergingManager, accessConstrained);
            this.Icons = OrganizationViewModel.ScoutCategoryIcons(backingOrg);
            this.PrimaryOffice = backingOrg.PrimaryOffice;
            this.InvestmentRegion = String.Join(",<br/>", backingOrg.InvestmentRegions.OrderBy(m => m));
            this.Strategy = string.Join(",<br/>", backingOrg.Strategy);
            this.Category = backingOrg.Category;
            this.SubCategory = backingOrg.MarketStage;
            this.YearFounded = backingOrg.YearFounded.HasValue ? backingOrg.YearFounded.ToString() : string.Empty;
            this.FundraisingStatus = backingOrg.FundRaisingStatus;
            //this.LastUpdated = backingOrg.LastUpdated.HasValue
            //                       ? backingOrg.LastUpdated.Value.ToShortDateString()
            //                       : string.Empty;
            this.LastUpdated = DateDiffDays(backingOrg.LastUpdated);
            this.DiligenceLevel = backingOrg.DiligenceLevel.GetValueOrDefault(0).ToString();
            this.FolderIds = backingOrg.FolderOrganizations.Aggregate("[", (seed, current) =>
            {
                seed += String.Format("'{0}',", current.FolderId);
                return seed;
            }).TrimEnd(',') + "]";
            if (backingOrg.QualitativeGradeNumber.HasValue)
            {
                decimal value = Math.Round(backingOrg.QualitativeGradeStars.Value, 0);
                this.AtlasQualitative = string.Format(@"<div class=""score-level-result"">
                                            <div class=""score-level score-level-{0}"" style=""width: {1}%;"">
                                                &nbsp;
                                            </div>
                                          </div>", value, Convert.ToInt32(value * 20));
            }
            if (backingOrg.QuantitativeGradeNumber.GetValueOrDefault(0) > GradeRangeLimit.Ungraded)
            {
                decimal value = Math.Round(backingOrg.QuantitativeGradeStars.Value, 0);
                this.QoRPerformance = string.Format(@"<div class=""score-level-result"">
                                            <div class=""score-level score-level-{0}"" style=""width: {1}%;"">
                                                &nbsp;
                                            </div>
                                          </div>", value, Convert.ToInt32(value * 20));
            }
            this.InactiveFirm = backingOrg.InactiveFirm;
            this.SectorFocus = backingOrg.SectorFocusUI;
        }

        public Guid Id { get; set; }

        public string Icons { get; set; }
        public String Name { get; set; }

        public bool AccessConstrained { get; set; }
        public bool EmergingManager { get; set; }
        public string FocusRadar { get; set; }

        public String PrimaryOffice { get; set; }

        public String KeyTakeAway { get; set; }

        public String InvestmentRegion { get; set; }

        public String GeographicFocus { get; set; }

        public String Category { get; set; }

        public String Strategy { get; set; }

        /// <summary>
        /// MarketStage
        /// </summary>
        public string SubCategory { get; set; }

        public string YearFounded { get; set; }

        public string FundraisingStatus { get; set; }

        public string LastUpdated { get; set; }

        public string DiligenceLevel { get; set; }

        /// <summary>
        /// Returns Guids in a json array
        /// </summary>
        public string FolderIds { get; set; }

        public string AtlasQualitative { get; set; }

        public string QoRPerformance { get; set; }
        public bool InactiveFirm { get; set; }

        public string SectorFocus { get; set; }

        private string _FPVHtml;

        public string DateDiffDays(DateTime? date)
        {
            if (date.HasValue)
            {
                double days = DateTime.Now.Date.Subtract(date.Value.Date).TotalDays;
                if (days > 90) return "90+ days";
                if (days > 30) return "Last 90 days";
                if (days > 0) return "Last 30 days";
            }
            return string.Empty;
        }

    }
}