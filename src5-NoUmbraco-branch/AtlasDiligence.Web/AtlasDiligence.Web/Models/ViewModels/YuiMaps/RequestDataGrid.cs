﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class RequestDataGrid
    {
        public RequestDataGrid(OrganizationRequest backingRequest)
        {
            Id = backingRequest.Id;
            Comment = backingRequest.Comment;
            CreatedOn = backingRequest.CreatedOn.ToShortDateString();
            OrganizationId = backingRequest.OrganizationId ?? new Guid();
            OrganizationName = backingRequest.OrganizationName;
            UserName = backingRequest.UserEmail; //  .aspnet_User.LoweredUserName;
            RequestTypeDisplayName = backingRequest.RequestTypeDisplayName;

            //  EditLink = String.Format("<a href=\"{0}\" class=\"grid-row-action edit\">Edit</a>",
            //VirtualPathUtility.ToAbsolute("~/Group/Edit/" + backingGroup.Id)
            //);

            //  ViewLink = String.Format("<a href=\"{0}\" class=\"grid-row-action view\">View</a>",
            //VirtualPathUtility.ToAbsolute("~/Group/View/" + backingGroup.Id)
            //);
        }

        public Guid Id { get; set; }
        public string Comment { get; set; }
        public string CreatedOn { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string UserName { get; set; }
        public string RequestTypeDisplayName { get; set; }
    }
}