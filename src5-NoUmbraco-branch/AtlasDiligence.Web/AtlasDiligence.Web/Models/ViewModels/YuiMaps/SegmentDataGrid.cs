﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class SegmentDataGrid
    {
        public SegmentDataGrid(Common.Data.Models.Segment backingSegment)
        {
            Id = backingSegment.Id;

            Name = backingSegment.Name;

            EditLink = String.Format(
                "<a href=\"{0}\" class=\"grid-row-action edit\">Edit</a>", 
                VirtualPathUtility.ToAbsolute("~/Segment/Edit/" + backingSegment.Id));
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Name { get; set; }

        public string EditLink { get; set; }
    }
}