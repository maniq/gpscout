﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class GridList<T>
    {
        protected T[] _result = new T[0];

        public T[] Result
        {
            get
            {
                return this._result;
            }
            set
            {
                this._result = value;
            }
        }

        /// <summary>
        /// Indicates how many rows are available, varies independantly to the number in the "Results" array.
        /// </summary>
        public int Total
        {
            get;
            set;
        }

        public object MetaData { get; set; }
    }

}
