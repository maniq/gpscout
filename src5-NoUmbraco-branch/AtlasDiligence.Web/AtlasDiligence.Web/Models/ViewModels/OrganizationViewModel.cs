﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models.ViewModels.Search;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Enums;
using System.Linq;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.Models.ViewModels.Widgets;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class OrganizationViewModel
    {
        public bool IsForTooltip { get; set; }
        public Guid Id { get; set; }

        public IEnumerable<Common.Data.Models.Folder> Folders { get; set; }

        public Common.Data.Models.Organization Organization { get; set; }

        public IEnumerable<OrganizationMember> OrganizationMembers { get; set; }

        public IEnumerable<OrganizationNote> OrganizationNotes { get; set; } 

        public IEnumerable<FirmNote> FirmNotes { get; set; }
        
        public IEnumerable<NewsItem> NewsItems { get; set; }

        public ProductTypes GroupProductType { get; set; }

        public List<LuceneSearchResult> OrganizationsInSameSegment { get; set; }

        public SearchOptionsViewModel SearchOptions { get; set; }

        public bool Preview { get; set; }

        public bool HasPreqinMostRecentMetric { get; set; }

        public bool HasRCPPriorMetric { get; set; }

        public bool IsFundDetailViewTabAccessible { get; set; }


        private List<Fund> _fundsForTrackRecordsTab;
        public List<Fund> FundsForTrackRecordsTab
        {
            get
            {
                if (_fundsForTrackRecordsTab == null)
                {
                    _fundsForTrackRecordsTab = Organization.Funds/*.Where(m => m.PublishTrackRecord)*/.OrderBy(m => m.SortOrder.GetValueOrDefault(0)).ThenBy(m => m.Name).ToList();
                    // calculate further helper data
                    Fund[] fundWithMostRecentPreqinMetrics = _fundsForTrackRecordsTab.Where(f => f.IsPreqinMostRecentMetric).ToArray();
                    this.HasPreqinMostRecentMetric = fundWithMostRecentPreqinMetrics.Any();
                    if (this.HasPreqinMostRecentMetric)
                    {
                        this.HasRCPPriorMetric = this.FundsForTrackRecordsTab.Where(f => f.Source == FundMetricSource.RCP).Any();

                        if (!this.HasRCPPriorMetric)
                        {
                            this.HasRCPPriorMetric = fundWithMostRecentPreqinMetrics.SelectMany(f => f.FurtherFundMetrics).Where(frm => frm.Source == FundMetricSource.RCP && frm.AsOf < frm.Fund.PreqinAsOf).Any();
                        }
                    }
                    else
                    {
                        this.HasRCPPriorMetric = false;
                    }
                    this.IsFundDetailViewTabAccessible = (!this.HasPreqinMostRecentMetric && _fundsForTrackRecordsTab.Any(x => x.Source != null)) || this.HasRCPPriorMetric;
                }
                return _fundsForTrackRecordsTab;
            }
        }


        public Fund GetFundWithAvailableRCPData(Fund fund)
        {
            Fund _fund = new Fund()
            {
                Status = fund.Status,
                AnalysisUrl = fund.AnalysisUrl,
                Name = fund.Name,
                VintageYear = fund.VintageYear,
                TargetSize = fund.TargetSize,
                FundSize = fund.FundSize,
                Currency = fund.Currency,
            };
            var mostRecentRCPMetric = fund.FurtherFundMetrics.Where(f => f.Source == FundMetricSource.RCP).FirstOrDefault();
            if (mostRecentRCPMetric != null)
            {
                _fund.InvestedCapital = mostRecentRCPMetric.InvestedCapital;
                _fund.RealizedValue = mostRecentRCPMetric.RealizedValue;
                _fund.UnrealizedValue = mostRecentRCPMetric.UnrealizedValue;
                _fund.TotalValue = mostRecentRCPMetric.TotalValue;
                _fund.GrossIrr = mostRecentRCPMetric.GrossIrr;
                _fund.Moic = mostRecentRCPMetric.Moic;
                _fund.Dpi = mostRecentRCPMetric.Dpi;
                _fund.Tvpi = mostRecentRCPMetric.Tvpi;
                _fund.Irr = mostRecentRCPMetric.Irr;
                _fund.AsOf = mostRecentRCPMetric.AsOf;
                _fund.Source = mostRecentRCPMetric.Source;
                _fund.TVPIQuartile = mostRecentRCPMetric.TVPIQuartile;
                _fund.DPIQuartile = mostRecentRCPMetric.DPIQuartile;
                _fund.NetIrrQuartile = mostRecentRCPMetric.NetIrrQuartile;
                _fund.BenchmarkAsOf = mostRecentRCPMetric.BenchmarkAsOf;
                _fund.BenchmarkDpiFirstQuartile = mostRecentRCPMetric.BenchmarkDpiFirstQuartile;
                _fund.BenchmarkDpiMedian = mostRecentRCPMetric.BenchmarkDpiMedian;
                _fund.BenchmarkDpiThirdQuartile = mostRecentRCPMetric.BenchmarkDpiThirdQuartile;
                _fund.BenchmarkNetIrrFirstQuartile = mostRecentRCPMetric.BenchmarkNetIrrFirstQuartile;
                _fund.BenchmarkNetIrrMedian = mostRecentRCPMetric.BenchmarkNetIrrMedian;
                _fund.BenchmarkNetIrrThirdQuartile = mostRecentRCPMetric.BenchmarkNetIrrThirdQuartile;
                _fund.BenchmarkTvpiFirstQuartile = mostRecentRCPMetric.BenchmarkTvpiFirstQuartile;
                _fund.BenchmarkTvpiMedian = mostRecentRCPMetric.BenchmarkTvpiMedian;
                _fund.BenchmarkTvpiThirdQuartile = mostRecentRCPMetric.BenchmarkTvpiThirdQuartile;
            }
            return _fund;
        }


        public Fund GetFundWithAvailablePreqinData(Fund fund)
        {
            Fund _fund = new Fund()
            {
                Status = fund.Status,
                AnalysisUrl = fund.AnalysisUrl,
                Name = fund.Name,
                VintageYear = fund.VintageYear,
                TargetSize = fund.TargetSize,
                FundSize = fund.FundSize,
                Currency = fund.Currency,
            };
            var mostRecentPreqinMetric = fund.FurtherFundMetrics.Where(f => f.Source == FundMetricSource.Preqin).FirstOrDefault();
            _fund.HasPreqinMetric = mostRecentPreqinMetric != null;
            if (mostRecentPreqinMetric != null)
            {
                _fund.InvestedCapital = mostRecentPreqinMetric.InvestedCapital;
                _fund.RealizedValue = mostRecentPreqinMetric.RealizedValue;
                _fund.UnrealizedValue = mostRecentPreqinMetric.UnrealizedValue;
                _fund.TotalValue = mostRecentPreqinMetric.TotalValue;
                _fund.GrossIrr = mostRecentPreqinMetric.GrossIrr;
                _fund.Moic = mostRecentPreqinMetric.Moic;
                _fund.PreqinDpi = mostRecentPreqinMetric.PreqinDpi;
                _fund.PreqinTvpi = mostRecentPreqinMetric.PreqinTvpi;
                _fund.PreqinIrr = mostRecentPreqinMetric.PreqinIrr;
                _fund.PreqinAsOf = mostRecentPreqinMetric.PreqinAsOf;
                _fund.Source = mostRecentPreqinMetric.Source;
                _fund.TVPIQuartile = mostRecentPreqinMetric.TVPIQuartile;
                _fund.DPIQuartile = mostRecentPreqinMetric.DPIQuartile;
                _fund.NetIrrQuartile = mostRecentPreqinMetric.NetIrrQuartile;
                _fund.BenchmarkAsOf = mostRecentPreqinMetric.BenchmarkAsOf;
                _fund.BenchmarkDpiFirstQuartile = mostRecentPreqinMetric.BenchmarkDpiFirstQuartile;
                _fund.BenchmarkDpiMedian = mostRecentPreqinMetric.BenchmarkDpiMedian;
                _fund.BenchmarkDpiThirdQuartile = mostRecentPreqinMetric.BenchmarkDpiThirdQuartile;
                _fund.BenchmarkNetIrrFirstQuartile = mostRecentPreqinMetric.BenchmarkNetIrrFirstQuartile;
                _fund.BenchmarkNetIrrMedian = mostRecentPreqinMetric.BenchmarkNetIrrMedian;
                _fund.BenchmarkNetIrrThirdQuartile = mostRecentPreqinMetric.BenchmarkNetIrrThirdQuartile;
                _fund.BenchmarkTvpiFirstQuartile = mostRecentPreqinMetric.BenchmarkTvpiFirstQuartile;
                _fund.BenchmarkTvpiMedian = mostRecentPreqinMetric.BenchmarkTvpiMedian;
                _fund.BenchmarkTvpiThirdQuartile = mostRecentPreqinMetric.BenchmarkTvpiThirdQuartile;
            }
            return _fund;
        }


        IEnumerable<TrackRecord> topGraphTrackRecords;
        public IEnumerable<TrackRecord> TopGraphsModels
        {
            get
            {
                if (topGraphTrackRecords == null && this.Organization != null)
                {
                    topGraphTrackRecords = this.Organization.TrackRecords.Where(tr => tr.TopGraphDisplayOrder.HasValue).OrderBy(tr => tr.TopGraphDisplayOrder);
                }
                return topGraphTrackRecords;
            }
        }


        public string OrganizationStrategyWidgetHtml { get; set; }
        //public string TeamOverviewWidgetHtml { get; set; }

        public static string ScoutCategoryIcons(Organization backingOrg)
        {
            if (backingOrg.InactiveFirm)
                return "Inactive";

            var emptyDiv = "<div class=\"empty\">&nbsp;</div>";
            var rightEmptyDiv = "<div class=\"empty\" style=\"width:16px;\">&nbsp;</div>";
            var focusRadar = String.IsNullOrWhiteSpace(backingOrg.FocusRadar)
                             ? string.Format(emptyDiv)
                             : string.Format("<div class=\"{0} icon\" title=\"{1}\">{1}</div>",
                                              backingOrg.FocusRadar.ToLower().Replace(' ', '-'), backingOrg.FocusRadar);

            var emergingManager = backingOrg.EmergingManager ?
                string.Format("<div class=\"emerging-manager-true icon\" title=\"Emerging Manager\">{0}</div>",
                backingOrg.EmergingManager.ToString().ToLower()) : string.Format(emptyDiv);

            var accessConstrained = backingOrg.AccessConstrained ?
                string.Format("<div class=\"access-constrained-true icon\" title=\"Access Constrained\">{0}</div>",
                backingOrg.AccessConstrained.ToString().ToLower()) : string.Format(rightEmptyDiv);

            return string.Format("{0} {1} {2}", focusRadar, emergingManager, accessConstrained);
        }


        public bool HasScorecardTab
        {
            get
            {
                return (Organization != null && Organization.PublishProfile) || Preview;
            }
        }


        public bool HasTrackRecordTab
        {
            get
            {
                return (Organization != null && Organization.PublishTrackRecord) || Preview;
            }
        }

    }
}