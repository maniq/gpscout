﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class CollapsibleWidgetViewModel : WidgetViewModel
    {
        public string Key { get; set; }
        public bool? Expanded { get; set; }
    }
}