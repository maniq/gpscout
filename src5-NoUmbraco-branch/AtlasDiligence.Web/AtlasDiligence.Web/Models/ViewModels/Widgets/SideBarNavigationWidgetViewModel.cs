﻿using AtlasDiligence.Web.Models.ViewModels.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class SideBarNavigationWidgetViewModel : CollapsibleWidgetViewModel
    {
        public IEnumerable<NavigationLinkViewModel> Items { get; set; }

        public bool HasItems
        {
            get { return Items != null && Items.Any(); }
        }
    }
}