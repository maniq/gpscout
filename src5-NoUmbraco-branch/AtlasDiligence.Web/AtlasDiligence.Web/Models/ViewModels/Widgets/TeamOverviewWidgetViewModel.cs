﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class TeamOverviewWidgetViewModel
    {
        public bool IsOnProfileTab { get; set; }
        public OrganizationViewModel OrganizationViewModel { get; set; }
    }
}