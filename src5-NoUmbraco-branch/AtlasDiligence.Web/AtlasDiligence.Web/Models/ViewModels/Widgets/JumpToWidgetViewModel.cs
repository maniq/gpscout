﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class JumpToWidgetViewModel : WidgetViewModel
    {
        public string JumpToUrl { get; set; }
        public string ClickOnSelector { get; set; }
        public string ContentHtml { get; set; }
        public string ContentViewName { get; set; }
        public object ContentModel { get; set; }
        public bool HideIfNoContent { get; set; }
        public bool? ShowMoreInfoIcon { get; set; }
    }
}