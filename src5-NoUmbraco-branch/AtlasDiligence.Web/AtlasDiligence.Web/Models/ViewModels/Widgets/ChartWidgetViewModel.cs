﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class ChartWidgetViewModel : WidgetViewModel
    {
        public string ContentUrl { get; set; }
        public string ThumbnaiImagelUrl { get; set; }
        public string Description { get; set; }
        public string GroupTitle { get; set; }

        public bool HasContent
        {
            get { return !string.IsNullOrEmpty(ContentUrl); }
        }
    }
}