﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Widgets
{
    public class WidgetViewModel
    {
        public string Title { get; set; }
    }
}