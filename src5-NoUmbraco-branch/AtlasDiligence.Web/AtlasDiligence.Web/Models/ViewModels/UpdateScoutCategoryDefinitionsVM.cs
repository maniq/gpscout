﻿using System;
using System.Linq;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class UpdateScoutCategoryDefinitionsVM
    {
        public string Text { get; set; }

        //public DateTime Date { get; set; }
        public string CurrentText { get; set; }

        public void Build(IRepository<ScoutCategoryDefinition> scoutCategoryDefinitionRepository)
        {
            var definitions = scoutCategoryDefinitionRepository.GetAll().SingleOrDefault();
            Text = definitions == null ? string.Empty : definitions.Text;
        }
    }
}