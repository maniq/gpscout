﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Web.Models.ViewModels.Account;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class LandingViewModel
    {
        public LandingViewModel()
        {
            LogOnViewModel = new LogOnViewModel();
            RequestAccessViewModel = new RequestAccessViewModel();
        }

        public LogOnViewModel LogOnViewModel { get; set; }

        public RequestAccessViewModel RequestAccessViewModel { get; set; }
    }
}