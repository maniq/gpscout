﻿using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    /// <summary>
    /// Transport object for passing detailed message and status from json requests.
    /// </summary>
    public class MessageViewModel : TimeStamped
    {
        public string Message { get; set; }

        public string Status { get; set; }

        public string RenderView { get; set; }

        public object Data { get; set; }
        
    }
}
