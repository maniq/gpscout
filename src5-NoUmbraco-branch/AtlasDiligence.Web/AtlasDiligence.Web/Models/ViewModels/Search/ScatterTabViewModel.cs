﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Web.Controllers.Services;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
	public class ScatterTabViewModel
	{
		public SearchOptionsViewModel SearchOptions { get; set; }

		public ScatterChartViewModel ScatterChart { get; set; }

		public void Build(IEnumerable<LuceneSearchResult> availableOrgs, Guid? searchId)
		{
			// build scatter chart
			if (ScatterChart == null)
			{
				ScatterChart = new ScatterChartViewModel();
			}

			ScatterChart.Build(availableOrgs.ToList(), 500, 1000, 125, 50, 20, 20, 40);

            // build search options
            if (SearchOptions == null)
            {
                SearchOptions = new SearchOptionsViewModel(searchId);
            }
            else
                SearchOptions.SearchId = searchId;
			SearchOptions.Build();
		}
	}
}