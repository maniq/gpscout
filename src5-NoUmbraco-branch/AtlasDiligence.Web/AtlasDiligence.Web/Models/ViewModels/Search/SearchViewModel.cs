﻿using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Models.ViewModels.Map;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using FirmFilters = GPScout.Domain.Contracts.Models.FirmFilter;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    public class SearchViewModel
    {
        public SearchOptionsViewModel SearchOptions { get; set; }

        public IList<RecentSearchViewModel> RecentSearches { get; set; }

        public IEnumerable<FolderViewModel> Folders { get; set; }

        public IEnumerable<FolderViewModel> AllFolders { get; set; }

        public IEnumerable<Common.Data.Models.Organization> FirmsRecentlyUpdated { get; set; }

        public IEnumerable<ResearchPriorityOrganization> FirmsInProgress { get; set; }

        public SnapshotViewModel Snapshot { get; set; }

        public MapViewModel Map { get; set; }

        public ScatterChartViewModel ScatterChart { get; set; }

        public ProximitySearch ProximitySearch { get; set; }


        public bool? ShowMap { get; set; }

        /// <summary>
        /// Build new search template options.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="getAvailableOrganizations"></param>
        /// <param name="getFirmsRecentlyUpdated"></param>
        public void Build(Guid userId)
        {
            var userRepository = ComponentBrokerInstance.RetrieveComponent<IUserRepository>();
            var researchPriorityRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<ResearchPriorityOrganization>>();
            var searchService = ComponentBrokerInstance.RetrieveComponent<ISearchService>();

            var group = userRepository.GetById(userId).UserExt.Group;
            var segments = group != null ? group.GroupSegments.Select(m => m.Segment).ToList() : new List<Common.Data.Models.Segment>();
            var purchasedOrgs = group != null ? group.GroupOrganizations.Select(m => m.OrganizationId) : new List<Guid>();

            int total;
            var availableOrgs = searchService.GetOrganizations(new SearchFilter { Segments = segments, ProximitySearch = ProximitySearch, PurchasedOrganizationIds = purchasedOrgs }, out total).ToList();

            Snapshot = new SnapshotViewModel(availableOrgs, segments.Select(m => m.Name), researchPriorityRepository.GetAll().Count());

            Build(userId, null);
        }

        /// <summary>
        /// Build from existing Search Template options.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="previousSearchId"></param>
        public void Build(Guid userId, Guid? previousSearchId)
        {
            // recent searches
            var searchRepository = ComponentBrokerInstance.RetrieveComponent<ISearchRepository>();
            var recentSearches = searchRepository.GetSearchTemplatesByUserId(userId).Take(5);
            RecentSearches = recentSearches.Select(m => new RecentSearchViewModel { IsSavedSearch = m.SavedSearch != null, Date = m.CreatedDate, Name = m.SavedSearch != null ? m.SavedSearch.Name : m.Name, Id = m.Id, Detail = m.SearchFields }).ToList();

            // all folders
            var folderRepository = ComponentBrokerInstance.RetrieveComponent<IFolderRepository>();
            AllFolders = folderRepository.GetByUserId(userId).Select(m => new FolderViewModel(m));

            // search options
            if (SearchOptions == null)
            {
                SearchOptions = new SearchOptionsViewModel(previousSearchId);
            }
            else
                SearchOptions.SearchId = previousSearchId;

            SearchOptions.Build();

            if (previousSearchId.HasValue)
            {
                var template = searchRepository.GetSearchTemplateById(userId, previousSearchId.Value);
                this.ShowMap = template.ShowOnMap;
                SearchOptions.Search = template.KeywordField;
                SearchOptions.AccessConstrained = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.AccessConstrained).Select(m => m.Value).FirstOrDefault());
                SearchOptions.DiligenceStatusMaximum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.DiligenceStatusMax).Select(m => m.Value).FirstOrDefault());
                SearchOptions.DiligenceStatusMinimum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.DiligenceStatusMin).Select(m => m.Value).FirstOrDefault());
                SearchOptions.EmergingManager = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.EmergingManager).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SectorSpecialist = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SectorSpecialist).Select(m => m.Value).FirstOrDefault());
                SearchOptions.RegionalSpecialist = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.RegionalSpecialist).Select(m => m.Value).FirstOrDefault());
                SearchOptions.FundSizeMaximum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.FundSizeMax).Select(m => m.Value).FirstOrDefault());
                SearchOptions.FundSizeMinimum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.FundSizeMin).Select(m => m.Value).FirstOrDefault());
                SearchOptions.NumberOfFundsClosedMaximum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.NumberOfFundsClosedMax).Select(m => m.Value).FirstOrDefault());
                SearchOptions.NumberOfFundsClosedMinimum = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.NumberOfFundsClosedMin).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedFundraisingStatus = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.FundraisingStatus).Select(m => m.Value).ToList();
                SearchOptions.SelectedInvestmentRegions = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.InvestmentRegion).Select(m => m.Value).ToList();
                //SearchOptions.SelectedInvestmentSubRegion = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SubRegion).Select(m => m.Value).ToList();
                //SearchOptions.SelectedCountry = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.Country).Select(m => m.Value).ToList();
                SearchOptions.SelectedSubStrategy = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SubStrategy).Select(m => m.Value).ToList();
                SearchOptions.SelectedQualitativeGradeID = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.QualitativeGrade).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedQuantitativeGradeID = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.QuantitativeGrade).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedFocusList = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.FocusList).Select(m => m.Value).ToList();
                SearchOptions.FocusListSelectedFlag = SearchOptions.SelectedFocusList.Any(x => x == SearchFilter.FocusListStr);
                SearchOptions.SelectedMarketStage = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.MarketStage).Select(m => m.Value).ToList();
                SearchOptions.SelectedSector = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SectorFocus).Select(m => m.Value).ToList();
                SearchOptions.SelectedStrategy = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.Strategy).Select(m => m.Value).ToList();
                SearchOptions.SelectedNextFundraiseStartDate = Maybe.ToDateTime(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ExpectedNextFundraiseStartDate).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedNextFundraiseEndDate = Maybe.ToDateTime(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ExpectedNextFundraiseEndDate).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedNextFundraiseValue = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SelectedNextFundraiseValue).Select(s => s.Value).FirstOrDefault());
                SearchOptions.SelectedCurrencies = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.Currency).Select(m => m.Value).ToList();
                //SearchOptions.ShowOnlyPublished = Convert.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ShowOnlyPublished).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SBICFund = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.SBICFund).Select(m => m.Value).FirstOrDefault());
                SearchOptions.SelectedProximitySearchMiles = Maybe.ToInt32(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ProximitySearchMiles).Select(m => m.Value).FirstOrDefault());
                SearchOptions.ProximitySearchAddress = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ProximitySearchAddress).Select(m => m.Value).FirstOrDefault();
                SearchOptions.ProximitySearchLat = Maybe.ToDouble(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ProximitySearchLat).Select(m => m.Value).FirstOrDefault());
                SearchOptions.ProximitySearchLng = Maybe.ToDouble(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ProximitySearchLng).Select(m => m.Value).FirstOrDefault());
                SearchOptions.ShowInactiveFirms = Maybe.ToBoolean(template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.ShowInactiveFirms).Select(m => m.Value).FirstOrDefault()) != false;
                SearchOptions.FirmFilter = template.SearchTemplateFilters.Where(m => m.Field == (int)FieldType.FirmFilter).Select(m => m.Value).FirstOrDefault();
            }
            if (SearchOptions.DiligenceStatusMinimum.HasValue)
            {
                SearchOptions.SelectedDiligenceLevelID = SearchOptions.DiligenceStatusMinimum;
            }

            if (SearchOptions.FirmFilter == FirmFilters.InMarket)
            {
                if (SearchOptions.SelectedFundraisingStatus == null)
                    SearchOptions.SelectedFundraisingStatus = new List<string> { "open" };
                else if (!SearchOptions.SelectedFundraisingStatus.Where(f => f!=null && f.ToLower() == "open").Any())
                    SearchOptions.SelectedFundraisingStatus.Add("open");
            }
            else if (SearchOptions.FirmFilter == FirmFilters.FocusList)
            {
                SearchOptions.FocusListSelectedFlag = true;
            }

        }
    }
}