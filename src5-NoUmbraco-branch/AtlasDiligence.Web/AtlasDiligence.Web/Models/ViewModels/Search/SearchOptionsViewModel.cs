﻿using AtlanticBT.Common.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using System.Linq;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    public class SearchOptionsViewModel
    {
        public SearchOptionsViewModel()
        {
            this.ShowInactiveFirms = true;
        }

        public SearchOptionsViewModel (Guid? searchId)
        {
            this.ShowInactiveFirms = true;
            this.SearchId = searchId;
        }


        public Guid? SearchId { get; set; }

        [SearchField]
        [DisplayName(@"Search:")]
        public string Search { get; set; }

        public IList<string> Strategies { get; set; }

        [SearchField]
        [DisplayName(@"Primary Strategy:")]
        public IList<string> SelectedStrategy { get; set; }

        public IList<string> MarketStages { get; set; }

        [SearchField]
        [DisplayName(@"Market/Stage:")]
        public IList<string> SelectedMarketStage { get; set; }

        public IList<string> Sectors { get; set; }

        [SearchField]
        [DisplayName(@"Sector:")]
        public IList<string> SelectedSector { get; set; }

        public IList<string> InvestmentRegions { get; set; }

        [SearchField]
        [DisplayName(@"Region:")]
        public IList<string> SelectedInvestmentRegions { get; set; }

        public IList<string> FundraisingStatuses { get; set; }

        [SearchField]
        [DisplayName(@"Fundraising Status:")]
        public IList<string> SelectedFundraisingStatus { get; set; }

        [SearchField]
        [DisplayName(@"Fund Size Minimum:")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public double? FundSizeMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Fund Size Maximum:")]
        [DisplayFormat(DataFormatString = "{0:#,0}")]
        public double? FundSizeMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Number of Funds Closed Minimum:")]
        public int? NumberOfFundsClosedMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Number of Funds Closed Maximum:")]
        public int? NumberOfFundsClosedMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Emerging Manager:")]
        public bool? EmergingManager { get; set; }

        public IList<string> FocusLists { get; set; }

        [SearchField]
        [DisplayName(@"Focus List:")]
        public IList<string> SelectedFocusList { get; set; }
        public bool FocusListSelectedFlag { get; set; }

        [SearchField]
        [DisplayName(@"Access Constrained:")]
        public bool? AccessConstrained { get; set; }

        [SearchField]
        [DisplayName(@"Diligence Status Minimum:")]
        public int? DiligenceStatusMinimum { get; set; }

        [SearchField]
        [DisplayName(@"Diligence Status Maximum:")]
        public int? DiligenceStatusMaximum { get; set; }

        [SearchField]
        [DisplayName(@"Diligence Level:")]
        public int? SelectedDiligenceLevelID { get; set; }

        //[SearchField]
        //[DisplayName(@"Only Firms with Profiles:")]
        //public bool ShowOnlyPublished { get; set; }

        public IList<string> SubStrategies { get; set; }

        [SearchField]
        [DisplayName(@"Secondary Strategy:")]
        public IList<string> SelectedSubStrategy { get; set; }

        [SearchField]
        [DisplayName(@"Sector Specialist:")]
        public bool? SectorSpecialist { get; set; }

        [SearchField]
        [DisplayName(@"Regional Specialist:")]
        public bool? RegionalSpecialist { get; set; }

        [SearchField]
        [DisplayName(@"Include Inactive Firms:")]
        public bool ShowInactiveFirms { get; set; }
        

        //public IList<string> InvestmentSubRegions { get; set; }

        //[SearchField]
        //[DisplayName(@"Sub-Region:")]
        //public IList<string> SelectedInvestmentSubRegion { get; set; }

        //public IList<string> Countries { get; set; }

        //[SearchField]
        //[DisplayName(@"Country:")]
        //public IList<string> SelectedCountry { get; set; }

        public IList<string> Currencies { get; set; }

        [SearchField]
        [DisplayName(@"Currency:")]
        public IList<string> SelectedCurrencies { get; set; }

        [SearchField]
        [DisplayName(@"SBIC Fund:")]
        public bool? SBICFund { get; set; }

        // Expected Next Fundraise variables
        [SearchField]
        [DisplayName(@"Expected Next Fundraise Start Date:")]
        public DateTime? SelectedNextFundraiseStartDate { get; set; }

        [SearchField]
        [DisplayName(@"Expected Next Fundraise End Date:")]
        public DateTime? SelectedNextFundraiseEndDate { get; set; }

        [SearchField]
        [DisplayName(@"Expected Next Fundraise:")]
        public int? SelectedNextFundraiseValue { get; set; }

        public IEnumerable<SelectListItem> FundRaiseMonthOptions
        {
            get
            {
                string[] months = new string[] { "3", "6", "9", "12", "18", "24" };
                return months.Select(s => new SelectListItem { Text = String.Format("{0} months", s), Value = s });
            }
        }

        // Grade Fields variables
        public IList<string> QualitativeGrades { get; set; }

        public IList<string> QuantitativeGrades { get; set; }

        [SearchField]
        [DisplayName(@"Qualitative Score:")]
        public int? SelectedQualitativeGradeID { get; set; }

        [SearchField]
        [DisplayName(@"QoR Score:")]
        public int? SelectedQuantitativeGradeID { get; set; }

        public IList<string> SelectedQualitativeGrades { get; set; }

        public IList<string> SelectedQuantitativeGrades { get; set; }

        public IEnumerable<SelectListItem> SelectedGradesOptions
        {
            get
            {
                var types = from Grades type in Enum.GetValues(typeof(Grades))
                            select new { ID = (int)type, Name = (int)type < (int)Grades.Exceptional ? string.Format("{0} and Above", type.ToString()) : type.ToString() };
                return new SelectList(types, "ID", "Name");
            }
        }


        public IEnumerable<SelectListItem> DiligenceLevelOptions
        {
            get
            {
                var types = from DiligenceLevelUI type in Enum.GetValues(typeof(DiligenceLevelUI))
                            select new { ID = (int)type, Name = type.GetDescription() };
                return new SelectList(types, "ID", "Name");
            }
        }

        //public List<string> InputSubRegions
        //{
        //    get
        //    {
        //        string[] subRegions = new string[] { "US - Midwest", "US - Northwest", "US - Southwest", "US - Southeast", "US - Northeast", "US - Rocky Mountain", "US - West" };
        //        return subRegions.Cast<string>().ToList();
        //    }
        //}

        public List<string> InputCountries
        {
            get
            {
                string[] countries = new string[] { "USA", "Canada" };
                return countries.Cast<string>().ToList();
            }
        }

        /// <summary>
        /// Returns a dictionary of the search option properties and values. Used to pass params into Search grid.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> SearchOptionsKvp()
        {
            var retval = new Dictionary<string, object>();

            foreach (var property in typeof(SearchOptionsViewModel).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(SearchFieldAttribute), false).Any()
                    && property.GetValue(this, null) != null)
                {
                    // HACK: collections are not serialized correctly for the YUI data
                    if (property.PropertyType == typeof(IList<string>))
                    {
                        if (((IList<string>)property.GetValue(this, null)).Any())
                        {
                            retval.Add(property.Name,
                                       HttpUtility.UrlEncode(
                                           ((IList<string>)property.GetValue(this, null)).Aggregate(
                                               (a, b) => a + "," + b)));
                        }
                    }
                    else
                    {
                        retval.Add(property.Name, HttpUtility.UrlEncode(property.GetValue(this, null).ToString()));
                    }
                }
            }
            retval.Add("SearchId", SearchId);
            return retval;
        }

        public Dictionary<string, object> SearchOptionsReadable()
        {
            var retval = new Dictionary<string, object>();

            foreach (var property in typeof(SearchOptionsViewModel).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(SearchFieldAttribute), false).Any()
                    && property.GetValue(this, null) != null)
                {
                    var displayName =
                        property.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast
                            <DisplayNameAttribute>().FirstOrDefault();
                    if (property.PropertyType == typeof(IList<string>))
                    {
                        if (((IList<string>)property.GetValue(this, null)).Any())
                        {
                            retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                                       String.Join(", ", ((IList<string>)property.GetValue(this, null)).OrderBy(m => m)));
                        }
                    }
                    else if (property.PropertyType == typeof(bool?))
                    {
                        var value = (bool?)property.GetValue(this, null);
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                            value.HasValue && value.Value ? "Yes" : "No");
                    }
                    else if (property.PropertyType == typeof(bool))
                    {
                        var value = (bool)property.GetValue(this, null);
                        if (value)
                        {
                            retval.Add(displayName == null ? property.Name : displayName.DisplayName, "Yes");
                        }
                    }
                    else if (property.PropertyType == typeof(DateTime?))
                    {
                        var value = (DateTime?)property.GetValue(this, null);
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                            value.HasValue ? value.Value.ToShortDateString() : string.Empty);
                    }
                    else
                    {
                        retval.Add(displayName == null ? property.Name : displayName.DisplayName,
                                   property.GetValue(this, null).ToString());
                    }
                }
            }

            return retval;

        }

        [DisplayName(@"From City or Zip:")]
        public string ProximitySearchAddress { get; set; }

        [DisplayName(@"Miles:")]
        [SearchField]
        public int? SelectedProximitySearchMiles { get; set; }

        [SearchField]
        public double? ProximitySearchLat { get; set; }

        [SearchField]
        public double? ProximitySearchLng { get; set; }

        [DisplayName(@"Additional Filter:")]
        [SearchField]
        public string FirmFilter { get; set; }

        public bool? ShowMap { get; set; }


        public bool IsHeaderWidget { get; set; }


        public void Build()
        {
            var searchRepository = ComponentBrokerInstance.RetrieveComponent<ISearchRepository>();
            var options = searchRepository.GetDistinctOptions().ToList();
            this.Strategies = this.GetSearchOptionsByFieldType(options, FieldType.Strategy);
            this.MarketStages = this.GetSearchOptionsByFieldType(options, FieldType.MarketStage);
            this.Sectors = this.GetSearchOptionsByFieldType(options, FieldType.SectorFocus);
            //this.InvestmentRegions = SearchOptionsViewModel.GetInvestmentRegionsSearchOptions(options);
            this.InvestmentRegions = this.GetSearchOptionsByFieldType(options, FieldType.InvestmentRegion);
            if (this.SelectedInvestmentRegions == null) this.SelectedInvestmentRegions = new string[] { };
            this.SubStrategies = this.GetSearchOptionsByFieldType(options, FieldType.SubStrategy);
            //this.InvestmentSubRegions = this.GetSearchOptionsByFieldType(options, FieldType.InvestmentSubRegion);
            //this.Countries = this.GetSearchOptionsByFieldType(options, FieldType.Country);
            this.QualitativeGrades = this.GetSearchOptionsByFieldType(options, FieldType.QualitativeGrade);
            this.QuantitativeGrades = this.GetSearchOptionsByFieldType(options, FieldType.QuantitativeGrade);

            // Get fund raising statuses
            List<string> frsList = this.GetSearchOptionsByFieldType(options, FieldType.FundraisingStatus);
            // Keep only 'Open', 'Closed' and 'Fundless Sponsor' in the list
            this.FundraisingStatuses = frsList.Where(u => u.ToLower().Equals("open") || u.ToLower().Equals("closed")).ToList();
            var fsOption = frsList.FirstOrDefault(u => u.ToLower().Equals("fundless sponsor"));
            if (fsOption != null)
                this.FundraisingStatuses.Add(fsOption);

            this.Currencies = this.GetSearchOptionsByFieldType(options, FieldType.Currency);
            this.FocusLists = this.GetSearchOptionsByFieldType(options, FieldType.FocusList);
        }

        public SearchFilter ToSearchFilter()
        {
            return new SearchFilter
            {
                AccessConstrained = AccessConstrained,
                DiligenceStatusMaximum = DiligenceStatusMaximum,
                DiligenceStatusMinimum = DiligenceStatusMinimum,
                EmergingManager = EmergingManager,
                SectorSpecialist = SectorSpecialist,
                RegionalSpecialist = RegionalSpecialist,
                FocusList = SelectedFocusList,
                FundSizeMaximum = FundSizeMaximum,
                FundSizeMinimum = FundSizeMinimum,
                FundraisingStatus = SelectedFundraisingStatus,
                InvestmentRegions = SelectedInvestmentRegions,
                //InvestmentSubRegions = SelectedInvestmentSubRegion,
                SubStrategies = SelectedSubStrategy,
                //Countries = SelectedCountry,
                QualitativeGrades = SelectedQualitativeGrades,
                QuantitativeGrades = SelectedQuantitativeGrades,
                MarketStage = SelectedMarketStage,
                NumberOfFundsClosedMaximum = NumberOfFundsClosedMaximum,
                NumberOfFundsClosedMinimum = NumberOfFundsClosedMinimum,
                Sector = SelectedSector,
                Strategies = SelectedStrategy,
                Term = Search,
                ExpectedNextFundraiseEndDate = SelectedNextFundraiseEndDate,
                ExpectedNextFundraiseStartDate = SelectedNextFundraiseStartDate,
                //ShowOnlyPublished = ShowOnlyPublished,
                Currencies = SelectedCurrencies,
                SBICFund = SBICFund,
                ShowInactiveFirms = ShowInactiveFirms,
                FirmFilter = FirmFilter
            };
        }


        private List<string> GetSearchOptionsByFieldType(IEnumerable<SearchOption> options, FieldType fieldType)
        {
            var opts = options
           .Where(m => m.Field == (int)fieldType)
           .Select(m => m.Value)
           .ToList();

            return opts;

            //if (fieldType == FieldType.InvestmentSubRegion)
            //{
            //    results = opts
            //        .Where(m => InputSubRegions.Contains(m.Trim().ToString()))
            //        .Select(m => m.Trim().ToString()).Distinct().ToList();
            //}
            //else 
            //if (fieldType == FieldType.Country)
            //{
            //    results = opts
            //        .Where(m => InputCountries.Contains(m.Trim().ToString()))
            //        .Select(m => m.Trim().ToString()).Distinct().OrderByDescending(m => m).ToList();
            //}
            //else 
            //{
            //    results = opts;
            //}

            //return results;
            //return opts;
        } 

    }
}