﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    public class RecentSearchViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public Dictionary<string, string> Detail { get; set; }
        public bool IsSavedSearch { get; set; }
    }
}