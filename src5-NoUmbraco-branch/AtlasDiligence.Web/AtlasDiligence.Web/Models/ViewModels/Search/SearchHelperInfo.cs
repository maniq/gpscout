﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    [Serializable]
    internal class SearchHelperInfo
    {
        public SearchHelperInfo(Guid searchId)
        {
            SearchId = searchId;
        }

        public string QueryString { get; internal set; }

        //public List<Organization> ResultOrganizations { get; internal set; }
        public Guid SearchId { get; protected set; }
        public ProximitySearch LocationSearch { get; internal set; }
    }
}