﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlanticBT.Common.Validation;
using AtlasDiligence.Web.General;

namespace AtlasDiligence.Web.Models.ViewModels.Segment
{
    /// <summary>
    /// TODO: add country (primary office?)
    /// </summary>
    public class SegmentOptionsViewModel
    {
        public SegmentOptionsViewModel()
        {
            Strategies = new List<string>();
            SelectedStrategies = new List<string>();
            MarketStages = new List<string>();
            SelectedMarketStages = new List<string>();
            //InvestmentRegions = new List<KeyValuePair<string, string>>();
            InvestmentRegions = new List<string>();
            SelectedInvestmentRegions = new List<string>();
            //Countries = new List<string>();
            //SelectedCountries = new List<string>();
        }

        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string ReturnRoute { get; set; }

        public IList<string> Strategies { get; set; }

        [SegmentField]
        [DisplayName(@"Primary Strategy:")]
        public IList<string> SelectedStrategies { get; set; }

        public IList<string> MarketStages { get; set; }

        [SegmentField]
        [DisplayName(@"Market/Stage:")]
        public IList<string> SelectedMarketStages { get; set; }

        public IList<string> InvestmentRegions { get; set; }

        [SegmentField]
        [DisplayName(@"Investment Region:")]
        public IList<string> SelectedInvestmentRegions { get; set; }

        //public IList<string> Countries { get; set; }

        //[SegmentField]
        //[DisplayName(@"Country:")]
        //public IList<string> SelectedCountries { get; set; }

        [DisplayName(@"SBIC Fund:")]
        public bool? SbicFund { get; set; }

        /// <summary>
        /// Returns a dictionary of the search option properties and values. Used to pass params into Search grid.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> SegmentOptionsKvp()
        {
            var retval = new Dictionary<string, object>();

            foreach (var property in typeof(SegmentOptionsViewModel).GetProperties())
            {
                if (property.GetCustomAttributes(typeof(SegmentFieldAttribute), false).Any()
                    && property.GetValue(this, null) != null)
                    retval.Add(property.Name, HttpUtility.UrlEncode(property.GetValue(this, null).ToString()));
            }

            return retval;
        }
    }
}