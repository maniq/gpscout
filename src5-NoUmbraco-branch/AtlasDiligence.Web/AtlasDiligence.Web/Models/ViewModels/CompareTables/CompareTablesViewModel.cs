﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AtlasDiligence.Web.Models.ViewModels.CompareTables;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class CompareTablesViewModel
    {
        public Dictionary<Guid, string> OrganizationItems { get; set; } 

        public IEnumerable<Guid> SelectedOrganizationIds { get; set; }

        public IEnumerable<Common.Data.Models.Organization> SelectedOrganizations { get; set; }

        public CompareTablesViewModel()
        {
        }
    }
}