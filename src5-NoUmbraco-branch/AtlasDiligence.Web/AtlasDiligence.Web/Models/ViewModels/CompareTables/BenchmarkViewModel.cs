﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.Models;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.Web.Models.ViewModels.CompareTables
{
	public class BenchmarkViewModel
	{
		#region properties

		public int SelectedVintageYear { get; set; }

		public IEnumerable<ChartData> SelectedChartData { get; set; }

		public IEnumerable<ChartData> OtherChartData { get; set; }

		public IEnumerable<Benchmark> Benchmarks { get; set; }

		public IEnumerable<int> AllVintageYears { get; set; }

		public IEnumerable<int> AvailableVintageYears { get; set; }

		public decimal MinNetIrr { get; set; }

		public decimal MaxNetIrr { get; set; }

		public decimal MaxTvpi { get; set; }

		#endregion

		#region ctors

		public BenchmarkViewModel()
		{
			SelectedChartData = new List<ChartData>();
			OtherChartData = new List<ChartData>();
			Benchmarks = new List<Benchmark>();
		}

		#endregion

		#region helpers

		public void Build(IEnumerable<int> vintageYears, IEnumerable<Common.Data.Models.Organization> selectedOrgs)
		{
			SelectedChartData = selectedOrgs.Select(s => new ChartData {OrganizationName = s.Name});
			OtherChartData = new List<ChartData>();
			Benchmarks = new List<Benchmark>();
			AllVintageYears = vintageYears;
		}

        public void Build(IEnumerable<Common.Data.Models.Organization> orgs, IEnumerable<Common.Data.Models.Organization> selectedOrgs, IEnumerable<Benchmark> benchmarks, int selectedVintageYear, IEnumerable<int> vintageYears)
		{
			// set variables
			var selectedChartData = new List<ChartData>();
			var otherChartData = new List<ChartData>();
			var vintageYearRange = new string[] { (selectedVintageYear - 1).ToString(), selectedVintageYear.ToString(), (selectedVintageYear + 1).ToString() };
			
			// sort organizations

			orgs.ToList().ForEach(fe => fe.Funds
                                            .Where(w => /*w.PublishTrackRecord &&*/
                                                        !String.IsNullOrEmpty(w.VintageYearStr) &&
                                                        vintageYearRange.Contains(w.VintageYearStr))
											.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
											.ThenBy(t => t.Name)
											.ToList().ForEach(fd =>
											{
												var chartData = new ChartData();
												switch (fd.PerformanceDatasource)
												{
													case "General Partner":
														if (fd.AsOf.HasValue && (fd.Tvpi.HasValue || fd.Irr.HasValue))
														{
															chartData.OrganizationName = fd.Organization.Name;
															chartData.FundName = fd.Name;
															chartData.FundSize =
																fd.FundSize != null ? String.Format("{0}{1}", fd.FundSize.Value.ToString("#,#"), "mm") : String.Empty;
															chartData.TVPI = fd.Tvpi.HasValue ? fd.Tvpi.Value.ToString("0.00") : String.Empty;
															chartData.NetIRR = fd.Irr.HasValue ? fd.Irr.Value.ToString("0.0") : String.Empty;
															chartData.VintageYear = fd.VintageYear.GetValueOrDefault();
															chartData.AsOf = fd.AsOf.Value.ToShortDateString();
                                                            chartData.AsOfDate = fd.AsOf.HasValue ? fd.AsOf.Value.ToString("M/dd/yyyy") : ""; 
															chartData.DPI = fd.Dpi.HasValue ? String.Format("{0}{1}", fd.Dpi.Value.ToString("0.00"), "x") : String.Empty;
															chartData.MOIC = fd.Moic.HasValue ? String.Format("{0}{1}", fd.Moic.Value.ToString("0.00"), "x") : String.Empty;
															chartData.GrossIRR = fd.GrossIrr.HasValue ? String.Format("{0}{1}", fd.GrossIrr.Value.ToString("0.0"), "%") : String.Empty;
															chartData.Source = fd.PerformanceDatasource;
                                                            
															if (selectedOrgs.Select(s => s.Id).Contains(fe.Id))
															{
																selectedChartData.Add(chartData);
															}
															else
															{
																otherChartData.Add(chartData);
															}
														}
														break;
													case "Preqin":
														if (fd.PreqinAsOf.HasValue && (fd.PreqinTvpi.HasValue || fd.PreqinIrr.HasValue))
														{
															chartData.OrganizationName = fd.Organization.Name;
															chartData.FundName = fd.Name;
															chartData.FundSize = fd.FundSize != null ? String.Format("{0}{1}", fd.FundSize.Value.ToString("#,#"), "mm") : String.Empty;
															chartData.TVPI = fd.PreqinTvpi.HasValue ? fd.PreqinTvpi.Value.ToString("0.00") : String.Empty;
															chartData.NetIRR = fd.PreqinIrr.HasValue ? fd.PreqinIrr.Value.ToString("0.0") : String.Empty;
															chartData.VintageYear = fd.VintageYear.GetValueOrDefault();
															chartData.AsOf = fd.PreqinAsOf.Value.ToShortDateString();
                                                            chartData.AsOfDate = fd.PreqinAsOf.HasValue ? fd.PreqinAsOf.Value.ToString("M/dd/yyyy") : ""; 
															chartData.DPI = fd.PreqinDpi.HasValue ? String.Format("{0}{1}", fd.PreqinDpi.Value.ToString("0.00"), "x") : String.Empty;
															chartData.MOIC = String.Empty;
															chartData.GrossIRR = String.Empty;
															chartData.Source = fd.PerformanceDatasource;

															if (selectedOrgs.Select(s => s.Id).Contains(fe.Id))
															{
																selectedChartData.Add(chartData);
															}
															else
															{
																otherChartData.Add(chartData);
															}
														}
														break;
												}
											}));
			
			// set properties
			SelectedChartData = selectedChartData;
			OtherChartData = otherChartData;
            Benchmarks = benchmarks;
			AllVintageYears = vintageYears;

			// get min and max values
			var selectAndOtherData = selectedChartData.Union(otherChartData).ToList();
			MinNetIrr = selectAndOtherData.Any() ? selectAndOtherData.Where(w=> !String.IsNullOrEmpty(w.NetIRR)).Select(s => decimal.Parse(s.NetIRR)).Min() : 0;
            MaxNetIrr = selectAndOtherData.Any() ? selectAndOtherData.Where(w=> !String.IsNullOrEmpty(w.NetIRR)).Select(s => decimal.Parse(s.NetIRR)).Max() : 0;
			MaxTvpi = selectAndOtherData.Any() ? selectAndOtherData.Where(w=> !String.IsNullOrEmpty(w.TVPI)).Select(s => decimal.Parse(s.TVPI)).Max() : 0;
            var v = selectAndOtherData.Where(w => !String.IsNullOrEmpty(w.TVPI)).Select(s => s.TVPI).Distinct().ToList();
		}
		
        //private List<Benchmark> GetBenchmarks(IEnumerable<Benchmark> benchmarks)
        //{

        //}

		#endregion
	}
}