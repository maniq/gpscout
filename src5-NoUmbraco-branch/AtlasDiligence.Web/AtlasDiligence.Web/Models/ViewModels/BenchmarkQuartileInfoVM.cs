﻿using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public enum BenchmarkQuartileInfoType : byte
    {
        Dpi = 1,
        NetIrr = 2,
        Tvpi = 3
    }

    public class BenchmarkQuartileInfoVM
    {
        public string QuartileType { get; set; }
        public string FirstQuartileValue { get; set; }
        public string SecondQuartileValue { get; set; }
        public string ThirdQuartileValue { get; set; }
        public string AsOf { get; set; }
        public string Source { get; set; }

        public BenchmarkQuartileInfoVM(Fund fund, BenchmarkQuartileInfoType quartileType)
        {
            switch (quartileType)
            {
                case BenchmarkQuartileInfoType.Dpi:
                    this.QuartileType = "DPI";
                    this.FirstQuartileValue = fund.BenchmarkDpiFirstQuartile.AsQuartileString();
                    this.SecondQuartileValue = fund.BenchmarkDpiMedian.AsQuartileString();
                    this.ThirdQuartileValue = fund.BenchmarkDpiThirdQuartile.AsQuartileString();
                    break;
                case BenchmarkQuartileInfoType.NetIrr:
                    this.QuartileType = "Net IRR";
                    this.FirstQuartileValue = fund.BenchmarkNetIrrFirstQuartile.AsQuartileNetIRRString();
                    this.SecondQuartileValue = fund.BenchmarkNetIrrMedian.AsQuartileNetIRRString();
                    this.ThirdQuartileValue = fund.BenchmarkNetIrrThirdQuartile.AsQuartileNetIRRString();
                    break;
                case BenchmarkQuartileInfoType.Tvpi:
                    this.QuartileType = "TVPI";
                    this.FirstQuartileValue = fund.BenchmarkTvpiFirstQuartile.AsQuartileString();
                    this.SecondQuartileValue = fund.BenchmarkTvpiMedian.AsQuartileString();
                    this.ThirdQuartileValue = fund.BenchmarkTvpiThirdQuartile.AsQuartileString();
                    break;
            }
            this.AsOf = fund.BenchmarkAsOf.AsDateOrNA();
            this.Source = "Preqin";
        }
    }
}