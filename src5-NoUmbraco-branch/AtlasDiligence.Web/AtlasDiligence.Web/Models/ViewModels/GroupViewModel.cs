﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.Domain.Contracts.Enums;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class GroupViewModel
    {
        public GroupViewModel()
        {
            Organizations = new Dictionary<Guid, string>();
            Segments = new Dictionary<Guid, string>();
            OrganizationProfilePdfQuota = 10;
            OrganizationProfilePdfQuotaDays = 30;
            OrganizationProfilePdfQuotaActionID = Group.ProfilePdfQuotaActionEnum.NotifyRCPOnQuotaAchieved;
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [Range(1, 999999, ErrorMessage = "Max Users must be at least 1.")]
        [DisplayName("Max Users")]
        public int MaxUsers { get; set; }

        [DisplayName("Default Product Type")]
        public ProductTypes DefaultProductType { get; set; }

        public Dictionary<Guid, string> Organizations { get; set; }

        public Dictionary<Guid, string> Segments { get; set; }

        /// <summary>
        /// number of active users for this group
        /// </summary>
        public int ActiveUsers { get; set; }

        [DisplayName("Allow Organization Pdf Generation")]
        public bool OrganizationProfilePdfAllowed { get; set; }

        [DisplayName("Organization Pdf Generation Quota")]
        [Range(0, int.MaxValue, ErrorMessage = "Quota cannot be negative.")]
        public int? OrganizationProfilePdfQuota { get; set; }

        [DisplayName("Organization Pdf Generation Quota Days")]
        public int? OrganizationProfilePdfQuotaDays { get; set; }

        // This is current status of Profile Pdf Generation (Yes, No, Quota Achieved)
        public string OrganizationProfilePdfGeneration { get; set; }


        [DisplayName("When Organization Pdf Generation Quota Achieved")]
        [Required]
        public Group.ProfilePdfQuotaActionEnum OrganizationProfilePdfQuotaActionID { get; set; }


        [DisplayName("Show Watermark on Profile Screen")]
        public bool WatermarkEnabled { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="_groupRepository"></param>
        /// <returns></returns>
        public static object Build(Group group, IGroupRepository _groupRepository)
        {
            var organizationIds = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
            var organizations = _groupRepository.GetGroupOrganizations(organizationIds).ToDictionary(organization => organization.Id, organization => organization.Name);
            var segments = group.GroupSegments.ToDictionary(x => x.SegmentId, x => x.Segment.Name);

            return new GroupViewModel
            {
                Id = group.Id,
                MaxUsers = group.MaxUsers,
                Name = group.Name,
                DefaultProductType = (ProductTypes)group.DefaultProductType,
                ActiveUsers = _groupRepository.GetActiveUsersByGroupId(group.Id).Count(),
                Organizations = organizations,
                Segments = segments,
                OrganizationProfilePdfAllowed = group.OrganizationProfilePdfAllowed,
                OrganizationProfilePdfQuota = group.OrganizationProfilePdfQuota,
                OrganizationProfilePdfQuotaDays = group.OrganizationProfilePdfQuotaDays,
                OrganizationProfilePdfGeneration = group.OrganizationProfilePdfGeneration,
                OrganizationProfilePdfQuotaActionID = group.OrganizationProfilePdfQuotaActionID,
                WatermarkEnabled = group.WatermarkEnabled
            };
        }

        public static void PopulateDefaults(GroupViewModel model, IGroupRepository _groupRepository)
        {
            var group = _groupRepository.GetById(model.Id);
            var organizationIds = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
            var organizations = _groupRepository.GetGroupOrganizations(organizationIds).ToDictionary(organization => organization.Id, organization => organization.Name);
            var segments = group.GroupSegments.ToDictionary(x => x.SegmentId, x => x.Segment.Name);
            model.ActiveUsers = _groupRepository.GetActiveUsersByGroupId(model.Id).Count();
            model.Organizations = organizations;
            model.Segments = segments;
            model.OrganizationProfilePdfAllowed = group.OrganizationProfilePdfAllowed;
            model.OrganizationProfilePdfQuota = group.OrganizationProfilePdfQuota;
            model.OrganizationProfilePdfQuotaDays = group.OrganizationProfilePdfQuotaDays;
            model.OrganizationProfilePdfQuotaActionID = group.OrganizationProfilePdfQuotaActionID;
            model.WatermarkEnabled = group.WatermarkEnabled;
        }

    }
}