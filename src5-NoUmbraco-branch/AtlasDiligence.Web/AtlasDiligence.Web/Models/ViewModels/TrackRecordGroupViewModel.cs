﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class TrackRecordGroupViewModel
    {
        public OrganizationViewModel OrganizationViewModel { get; set; }
        public string GroupTitle { get; set; }
        public TrackRecordCategory Category { get; set; }
        public string GroupIconImageUrl { get; set; }
    }
}