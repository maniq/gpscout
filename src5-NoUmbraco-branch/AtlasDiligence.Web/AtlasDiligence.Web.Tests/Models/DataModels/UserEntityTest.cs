﻿using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using AtlasDiligence.Common.DTO.Model;
using Ploeh.AutoFixture;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using Group = AtlasDiligence.Common.Data.Models.Group;

namespace AtlasDiligence.Web.Tests.Models.DataModels
{


    /// <summary>
    ///This is a test class for UserEntityTest and is intended
    ///to contain all UserEntityTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UserEntityTest : TestBase
    {
        [TestMethod()]
        public void GetInstanceTest()
        {
            //Assign
            aspnet_User user = CreateTransientAspNetUser();

            //Act
            UserEntity actual = UserEntity.GetInstance(user);

            //Assert
            Assert.AreEqual(user.UserId, actual.Id);
            Assert.AreEqual(user.aspnet_Membership.Email, actual.Email);
            Assert.AreEqual(user.aspnet_Membership.IsApproved, actual.IsApproved);
            Assert.AreEqual(user.UserExt.FirstName, actual.FirstName);
            Assert.AreEqual(user.UserExt.LastName, actual.LastName);
            Assert.AreEqual(user.UserExt.AccessGranted, actual.AccessGranted);
            Assert.AreEqual(user.UserExt.CrmId, actual.CrmId);
            Assert.AreEqual(user.UserExt.DiligenceAllowed, actual.DiligenceAllowed);
            Assert.AreEqual(user.UserExt.GroupId, actual.GroupId);
            Assert.AreEqual(user.UserExt.RssAllowed, actual.RssAllowed);
            Assert.AreEqual(user.UserExt.RssNextEmailDate, actual.RssNextEmailDate);
            Assert.AreEqual(user.UserExt.RssSearchAliases, actual.RssSearchAliases);
            Assert.AreEqual((EmailFrequency)user.UserExt.RssNextEmailOffsetEnum, actual.RssEmailFrequency);
            Assert.AreEqual(user.UserExt.UserId, actual.Id);
            //additional info fields
            Assert.AreEqual(user.UserExt.Title, actual.Title);
            Assert.AreEqual(user.UserExt.Phone, actual.Phone);
            Assert.AreEqual(user.UserExt.OrganizationName, actual.OrganizationName);
            Assert.AreEqual((OrganizationType)user.UserExt.OrganizationType, actual.OrganizationType);
            Assert.AreEqual(user.UserExt.AUM, actual.AUM);
            Assert.AreEqual(user.UserExt.ReferredBy, actual.ReferredBy);

        }

        /// <summary>
        /// User being updated by a non-admin (cannot change certain permissions or group leader flag)
        /// </summary>
        [TestMethod]
        public void MapFromUserViewModelNonAdminUpdaterTest()
        {
            //Arrange
            Fixture fixture = new Fixture();
            UserViewModel userViewModel = fixture.Build<UserViewModel>().Without(m => m.UserEntity).CreateAnonymous();
            userViewModel.RssAllowed = userViewModel.IsApproved = userViewModel.DiligenceAllowed = userViewModel.IsGroupLeader = false;

            UserEntity userEntity = UserEntity.GetInstance(CreateTransientAspNetUser());
            userEntity.RssAllowed = userEntity.IsApproved = userEntity.DiligenceAllowed = true;

            //Act
            var actual = UserEntity.Map(userEntity, userViewModel, false);

            //Assert
            //regular fields equal model
            Assert.AreEqual(userViewModel.Email, actual.Email);
            Assert.AreEqual(userViewModel.FirstName, actual.FirstName);
            Assert.AreEqual(userViewModel.LastName, actual.LastName);
            Assert.AreEqual(userViewModel.Phone, actual.Phone);
            Assert.AreEqual(userViewModel.Title, actual.Title);
            Assert.AreEqual(userViewModel.OrganizationName, actual.OrganizationName);
            Assert.AreEqual(userViewModel.OrganizationType, actual.OrganizationType);
            Assert.AreEqual(userViewModel.ReferredBy, actual.ReferredBy);
            //admin only assign fields equal userEntity (cannot be chenged is isAdmin is false in Map method)
            Assert.AreNotEqual(userViewModel.RssAllowed, actual.RssAllowed);
            Assert.AreNotEqual(userViewModel.IsApproved, actual.IsApproved);
            Assert.AreNotEqual(userViewModel.DiligenceAllowed, actual.DiligenceAllowed);
            Assert.AreNotEqual(userViewModel.IsGroupLeader, actual.IsGroupLeader);
        }

        /// <summary>
        /// Model should be able to update additional fields and trump userEntity
        /// </summary>
        [TestMethod]
        public void MapFromUserViewModelAdminUpdaterTest()
        {
            //Arrange
            Fixture fixture = new Fixture();
            UserViewModel userViewModel = fixture.Build<UserViewModel>().Without(m => m.UserEntity).CreateAnonymous();
            userViewModel.RssAllowed = userViewModel.IsApproved = userViewModel.DiligenceAllowed = userViewModel.IsGroupLeader = false;

            UserEntity userEntity = UserEntity.GetInstance(CreateTransientAspNetUser());
            userEntity.RssAllowed = userEntity.IsApproved = userEntity.DiligenceAllowed = true;

            //Act
            var actual = UserEntity.Map(userEntity, userViewModel, true);

            //Assert
            //regular fields equal model
            Assert.AreEqual(userViewModel.Email, actual.Email);
            Assert.AreEqual(userViewModel.FirstName, actual.FirstName);
            Assert.AreEqual(userViewModel.LastName, actual.LastName);
            Assert.AreEqual(userViewModel.Phone, actual.Phone);
            Assert.AreEqual(userViewModel.Title, actual.Title);
            Assert.AreEqual(userViewModel.OrganizationName, actual.OrganizationName);
            Assert.AreEqual(userViewModel.OrganizationType, actual.OrganizationType);
            Assert.AreEqual(userViewModel.ReferredBy, actual.ReferredBy);
            //admin only assign fields equal userEntity (cannot be chenged is isAdmin is false in Map method)
            Assert.AreEqual(userViewModel.RssAllowed, actual.RssAllowed);
            Assert.AreEqual(userViewModel.IsApproved, actual.IsApproved);
            Assert.AreEqual(userViewModel.DiligenceAllowed, actual.DiligenceAllowed);
            Assert.AreEqual(userViewModel.IsGroupLeader, actual.IsGroupLeader);
        }
    }
}
