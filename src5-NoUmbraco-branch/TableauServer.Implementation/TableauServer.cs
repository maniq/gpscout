﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TableauServer.Contracts;

namespace TableauServer.Implementation
{
    public class TableauServer : ITableauServer
    {
        string serverUrl;

        public ITableauServer Create(string url)
        {
            serverUrl = url ?? ConfigurationManager.AppSettings["TableauServerUrl"];
            return this;
        }

        public string GetTrustedAuthenticationTicket(string userName)
        {
            try {

                // -- Start of Misbah changes
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                | SecurityProtocolType.Tls11
                | SecurityProtocolType.Tls12
                | SecurityProtocolType.Ssl3;
                // End of Misbah changes

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/trusted?username={1}", serverUrl, userName));
                request.Method = "POST";
                request.ContentType = "application/json";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream responseStream = response.GetResponseStream())
                using (var streamReader = new StreamReader(responseStream))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                        return streamReader.ReadToEnd();
                }
            } catch (Exception exp) { 
            
            }

            return string.Empty;
        }


        public bool HasAccess(string userName)
        {
            string key = GetTrustedAuthenticationTicket(userName);
            return key != "-1" && !string.IsNullOrWhiteSpace(key);
        }
    }
}
