﻿using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPScout.DataImport.Contracts.Models;

namespace GPScout.DataImport.Contracts.Services
{
    public interface IDataImportManager
    {
        bool IsRunning();
        //IResults StartImport();
        IResults StartImport(bool forceStart, bool fullImport, bool reindexOnly, bool onlyImportToStage);
        //IAppContext GetSettings(string dataImportConfigFilePath);
    }
}
