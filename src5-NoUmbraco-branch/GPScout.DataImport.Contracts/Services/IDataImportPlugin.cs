﻿using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.DataImport.Contracts.Services
{
    public interface IDataImportPlugin
    {
        DateTime DateImported { get; set; }
        DateTime ImportFromDate { get; set; }
        IResults StartImport(bool forceStart, bool fullImport, out bool dataImported, bool lonlyImportToStage);
        bool Enabled();
    }
}
