﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.DataImport.Contracts.Models
{
    public interface IAppContext
    {
        int DataImportRespawnSeconds { get; set; }
        string SSPN_DataImportLastStartDate { get; set; }
        bool SalesforceImportEnabled { get; set; }
        string SSPN_DataImportFromDateUtc { get; set; }
        DateTime DefaultDataImportFromDateUtc { get; set; }
        string FromEmail { get; set; }
        string PUCNEmailSubject { get; set; }
        int SalesForceRequestTimeoutSeconds { get; set; }
        int SalesForceMaxRetryAttempts { get; set; }
    }
}
