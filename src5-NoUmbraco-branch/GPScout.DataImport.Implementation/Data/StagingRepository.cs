﻿using AtlasDiligence.Common.Data.Models;
using GPScout.DataImport.Contracts.Repositories;
using GPScout.Domain.Contracts.Models;
using GPScout.Salesforce.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.DataImport.Implementation.Data
{
    public class StagingRepository : IRepository
    {
        //private static SqlBulkCopy _bulkCopy;
        //protected static SqlBulkCopy bulkCopy
        //{
        //    get
        //    {
        //        if (_bulkCopy == null)
        //        {
        //            string connectionString = ConfigurationManager.ConnectionStrings["atlasdilig_01ConnectionString"].ConnectionString;
        //            _bulkCopy = new SqlBulkCopy(connectionString);
        //        }
        //        return _bulkCopy;
        //    }
        //}


        public DataSyncResult BulkInsert<T>(IEnumerable<T> data, string targetTableName)
        {
            //var results = new DataSyncResult();
            //if (data != null)
            //{
            //    bulkCopy.DestinationTableName = targetTableName;
            //    bulkCopy.BatchSize = data.Count();
            //    bulkCopy.WriteToServer(data.AsDataReader());
            //    results.Inserted = bulkCopy.BatchSize;
            //}
            //return results;
            throw new NotImplementedException();
        }        

        
        public IEnumerable<T> Select<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void TruncateStagingTables()
        {
            throw new NotImplementedException();
        }

        public T Delete<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public T Add<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }


        public void PostImport(DateTime DateImportedUtc)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<string> GetAdditionalAccountIdsClause()
        {
            throw new NotImplementedException();
        }


        public void SetFundMetricImportStep(string mode, byte step)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guid?> GetOrgsInProcessBeforeDataImport()
        {
            throw new NotImplementedException();
        }
    }
}
