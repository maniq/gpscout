﻿using AtlanticBT.Common.GeoLocation;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.DataImport.Contracts.Models;
using GPScout.DataImport.Contracts.Repositories;
using GPScout.DataImport.Contracts.Services;
using GPScout.DataImport.Implementation.Models;
using GPScout.Domain.Contracts.Enums;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using SFP.Infrastructure.Email.Contracts;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using Utility;

namespace GPScout.DataImport.Implementation.Services
{
    public class DataImportManager : IDataImportManager
    {
        #region Private members
        private static readonly object _locker = new object();
        private readonly IAppContext _appContext;
        private readonly IRepository<SystemStatus> _systemStatusRepository;
        private readonly IRepository _dataImportRepository;
        private readonly DateTime DateImported;
        private readonly ILog _log;
        private readonly IResults _messages;
        private readonly ISalesforceImportManager _sfImportManager;
        private readonly IIndexService _indexService;
        private readonly ISearchService _searchService;
        private readonly IEmail _email;

        private string _pucnBodyTemplate;
        private string _pucnBodyTemplateFirmRow;
        #endregion

        #region Properties
        string PUCNBodyTemplate {
            get
            {
                if (_pucnBodyTemplate == null)
                    _pucnBodyTemplate = Utils.LoadShortFileFromBaseDirectory("PUCNEmailBody.html");

                return _pucnBodyTemplate;
            }
        }


        string PUCNBodyTemplateFirmRow
        {
            get
            {
                if (_pucnBodyTemplateFirmRow == null)
                    _pucnBodyTemplateFirmRow = Utils.LoadShortFileFromBaseDirectory("PUCNEmailBodyFirm.html");

                return _pucnBodyTemplateFirmRow;
            }
        }

        #endregion

        #region CTor

        public DataImportManager(
            IAppContext appContext,
            ILog log,
            IRepository<SystemStatus> systemStatusRepository,
            IRepository dataImportRepository,
            ISalesforceImportManager salesforceImportManager,
            IIndexService indexService,
            ISearchService searchService,
            IEmail email 
            )
        {
            DateImported = DateTime.UtcNow;
            _appContext = appContext;
            _log = log;
            _systemStatusRepository = systemStatusRepository;
            _dataImportRepository = dataImportRepository;
            _messages = new Results();
            _sfImportManager = salesforceImportManager;
            _indexService = indexService;
            _searchService  = searchService;
            _email = email;
        }

        #endregion

        #region IDataImportManager

        public bool IsRunning()
        {
            return IsRunning(null);
        }


        public IResults StartImport(bool forceStart, bool fullImport, bool reindexOnly, bool onlyImportToStage)
        {
            try
            {
                _messages.Clear();
                if (IsRunning(true, forceStart))
                {
                    Log(ResultCode.Dataimport_IsRunningAlready);
                    return _messages;
                }

                if (forceStart)
                    Log(ResultCode.DataImport_Starting_Forcibly);

                bool dataImported = false;
                if (!reindexOnly)
                {
                    Log(ResultCode.DataImport_Started);
                    if (_sfImportManager != null && _sfImportManager.Enabled())
                    {
                        _sfImportManager.DateImported = DateImported;
                        _sfImportManager.ImportFromDate = GetImportFromDate();
                        var sfImportMessages = _sfImportManager.StartImport(forceStart, fullImport, out dataImported, onlyImportToStage);
                        _messages.Add(sfImportMessages);
                    }
                }

                if (!_messages.HasError && !onlyImportToStage)
                {
                    if (dataImported)
                    {
                        ProcessSectorAndStrategy();
                        ProcessSearchOptions();
                        GetGeocodes();
                        Reindex();
                        ProcessProfileUpdateCompletedNotifications();

                        SetImportFromDate();

                        Log(ResultCode.DataImport_Finished);
                    }
                    else if (reindexOnly)
                        Reindex();
                }
                IsRunning(false);
            }
            catch (Exception ex)
            {
                var r = _messages.Add(ex);
                _log.Write(ex);
                IsRunning(false);
            }

            return _messages;
        }

        private void ProcessProfileUpdateCompletedNotifications()
        {
            Log(ResultCode.PUCN_SendingMailsStarted);
            var results = new Results();
            
            using (var context = new AtlasWebDbDataContext())
            {
                Guid?[] inProcessOrganizationsBeforeDataImport = _dataImportRepository.GetOrgsInProcessBeforeDataImport().ToArray();
                //var testList = inProcessOrganizationsBeforeDataImport.ToList(); testList.Add(Guid.Parse("3147887c-0e5b-432b-aa90-0a6be4754b1d")); inProcessOrganizationsBeforeDataImport = testList.ToArray();

                var requests = context.OrganizationRequests.Where(o =>
                    o.Type == (int)RequestType.ProfileUpdateCompletedNotification && o.IsActive && o.Organization != null //&& /*o.Organization.Active*/
                    /*&& !Organization.IsInProcessOrg(o.Organization)*/
                    && o.aspnet_User != null && o.aspnet_User.UserExt != null && o.aspnet_User.UserExt.EulaSigned.HasValue
                    && o.aspnet_User.aspnet_Membership != null && o.aspnet_User.aspnet_Membership.IsApproved
                    && inProcessOrganizationsBeforeDataImport.Contains(o.OrganizationId)
                    //&& o.aspnet_User.aspnet_Membership.Email == "zkovacs@smallfootprint.com"
                ).Select(o => new OrganizationRequestNotification
                {
                    UserId = o.UserId,
                    Email = o.aspnet_User.aspnet_Membership.Email,
                    Organization = o.Organization
                }).ToArray();

                requests = requests.Where(r => r.Organization.Active && !Organization.IsInProcessOrg(r.Organization)).ToArray();

                requests.GroupBy(r => r.UserId).ToList().ForEach(r =>
                    SendProfileUpdateCompletedNotification(r.ToList(), ref results)
                );
            }
            _messages.Add(results);
            int sent, errors;
            sent = errors = 0;
            foreach (var r in results)
            {
                if (r.IsError) errors++;
                else sent++;
                _log.Write(r.Message);
            }
            Log(ResultCode.PUCN_SendingMailsCompleted, sent, errors);
        }

        private void SendProfileUpdateCompletedNotification(List<OrganizationRequestNotification> list, ref Results results)
        {
            if (list != null && list.Any())
            {
                string sentTo = list.Select(r => r.Email).FirstOrDefault();
                try
                {
                    var userID = list.Select(r => r.UserId).FirstOrDefault();
                    IEnumerable<Guid?> availableOrganizationIDs = _searchService.GetAvailableOrganizations(userID.GetValueOrDefault()).Select(x => (Guid?)x.Id);

                    var sb = new StringBuilder();
                    var orgsProcessed = new List<Guid?>();
                    foreach (var request in list.Where(r => availableOrganizationIDs.Contains(r.OrganizationId)))
                    {
                        if (orgsProcessed.Contains(request.Organization.Id)) continue;
                        sb.AppendLine(this.PUCNBodyTemplateFirmRow
                            .Replace("__FIRMID__", request.Organization.Id.ToString())
                            .Replace("__FIRMNAME__", request.Organization.Name)
                        );
                        orgsProcessed.Add(request.Organization.Id);
                    }

                    if (sb.Length > 0)
                    {
                        var firmList = sb.ToString();
                        string body = this.PUCNBodyTemplate
                            .Replace("__FIRMSLIST__", firmList)
                            .Replace("__CURRENTYEAR__", DateTime.Now.Year.ToString());
                        _email.Send(_appContext.PUCNEmailSubject, body, _appContext.FromEmail, sentTo, null);
                        results.Add(ResultCode.PUCN_MailSent, sentTo, firmList);
                    }
                }
                catch (Exception ex)
                {
                    results.Add(new Exception(string.Format("Error sending PUCN mail to {0}.", sentTo), ex));
                }
            }
        }
        #endregion

        #region Helper Methods

        private void ProcessSearchOptions()
        {
            Log(ResultCode.DataImport_Inserting_SearchOptions_Started);
            _searchService.InsertSearchOptions();
            Log(ResultCode.DataImport_Inserting_SearchOptions_Finished);
        }


        private void ProcessSectorAndStrategy()
        {
            _log.Write("Processing Organization Sectors and Strategies...", "INFO");

            var context = new AtlasWebDbDataContext();
            var orgsToProcess = context.Organizations.Where(o => o.DateImportedUtc == DateImported);
            foreach (var org in orgsToProcess)
            {
                // sector
                if (!string.IsNullOrEmpty(org.SectorFocusPipeDelimited))
                {
                    foreach (string sectorFocus in org.SectorFocusPipeDelimited.Split('|').Where(x => !string.IsNullOrWhiteSpace(x)))
                    {
                        var entityMultiple = MapEntityMultiple(org.Id, FieldType.SectorFocus, sectorFocus);
                        context.EntityMultipleValues.InsertOnSubmit(entityMultiple);
                    }
                }

                // strategy
                if (!string.IsNullOrEmpty(org.StrategyPipeDelimited))
                {
                    foreach (string strategy in org.StrategyPipeDelimited.Split('|').Where(x => !string.IsNullOrWhiteSpace(x)))
                    {
                        var entityMultiple = MapEntityMultiple(org.Id, FieldType.Strategy, strategy);
                        context.EntityMultipleValues.InsertOnSubmit(entityMultiple);
                    }
                }
            }
            context.SubmitChanges();
            _log.Write("Done.", "INFO");
        }


        private EntityMultipleValue MapEntityMultiple(Guid entityId, FieldType fieldTypeName, string sectorFocus)
        {
            return new EntityMultipleValue()
            {
                Id = Guid.NewGuid(),
                EntityId = entityId,
                Field = (int)fieldTypeName,
                Value = sectorFocus
            };
        }


        private void GetGeocodes()
        {
            Log(ResultCode.DataImport_Getting_Geocode_Info_Started);

            var context = new AtlasWebDbDataContext();
            var allOrgs = context.Organizations.Where(w => (w.PublishOverviewAndTeam || w.PublishSearch) && (!w.Latitude.HasValue || !w.Longitude.HasValue));
            foreach (var org in allOrgs)
            {
                var retval = GetAddress(org.Address1, org.Address2, org.Address3, org.Address1City, org.PostalCode, org.Address1Country);
                if (retval != null)
                {
                    org.Latitude = Convert.ToDouble(retval.Geometry.Location.Latitude);
                    org.Longitude = Convert.ToDouble(retval.Geometry.Location.Longitude);
                }
            }

            var otherAddresses = context.OtherAddresses.Where(w => !w.Latitude.HasValue || !w.Longitude.HasValue);
            foreach (var org in otherAddresses)
            {
                var retval = GetAddress(org.Address1, org.Address2, org.Address3, org.City, org.PostalCode, org.Country);
                if (retval != null)
                {
                    org.Latitude = Convert.ToDouble(retval.Geometry.Location.Latitude);
                    org.Longitude = Convert.ToDouble(retval.Geometry.Location.Longitude);
                }
            }

            context.SubmitChanges();
            Log(ResultCode.DataImport_Getting_Geocode_Info_Finished);
        }


        private GeocodeAddress GetAddress(string address1, string address2, string address3, string city, string postalCode, string country)
        {
            // we are doing geocoding if we have at least city level address data 
            if (!string.IsNullOrEmpty(city))
            {
                try
                {
                    var geocodeService = new GeocodingService();

                    // first attempt
                    var firstQuery = String.Format("{0} {1} {2} {3} {4} {5}", address1, address2, address3, city, postalCode, country).Trim();

                    if (!String.IsNullOrWhiteSpace(firstQuery))
                    {
                        var retval = geocodeService.GetAddress(firstQuery).Results;
                        if (retval.Any())
                        {
                            return retval.FirstOrDefault();
                        }
                    }

                    // second attempt
                    var secondQuery = String.Format("{0} {1} {2} {3}", address1, city, postalCode, country).Trim();
                    if (!String.IsNullOrWhiteSpace(secondQuery))
                    {
                        var retval = geocodeService.GetAddress(secondQuery).Results;
                        if (retval.Any())
                        {
                            return retval.FirstOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Write(ex);
                }
            }
            return null;
        }


        private void Reindex()
        {
            Log(ResultCode.DataImport_ReIndex_Started);
            _indexService.ReIndex();
            Log(ResultCode.DataImport_ReIndex_Finished);
        }


        private DateTime GetImportFromDate()
        {
            var importStartFromDate = _systemStatusRepository.FindAll(x => x.Property == _appContext.SSPN_DataImportFromDateUtc).FirstOrDefault();
            DateTime dt;
            if (importStartFromDate != null)
            {
                dt = DateTime.Parse(importStartFromDate.Value);
            }
            else
                dt = _appContext.DefaultDataImportFromDateUtc.ToUniversalTime();

            return dt;
        }


        private void SetImportFromDate()
        {
            var importStartFromDate = _systemStatusRepository.FindAll(x => x.Property == _appContext.SSPN_DataImportFromDateUtc).FirstOrDefault();
            if (importStartFromDate == null)
            {
                importStartFromDate = new SystemStatus() { Property = _appContext.SSPN_DataImportFromDateUtc };
                _systemStatusRepository.InsertOnSubmit(importStartFromDate);
            }
            importStartFromDate.Value = DateImported.ToString();
            _systemStatusRepository.SubmitChanges();

            Log(ResultCode.DataImport_NewImportFromDateSet, DateImported.ToUniversalTime(), DateImported.ToLocalTime());
        }


        /// <summary>
        /// Checks, sets or resets the info about running state of the import process
        /// </summary>
        /// <param name="setRunningFlag">
        ///     no value = just check if the process is running
        ///     true     = mark as running
        ///     false    = mark as stopped
        /// </param>
        /// <returns></returns>
        protected bool IsRunning(bool? setRunningFlag, bool forceStart = false)
        {
            lock (_locker)
            {
                var dataImportLastStartDate = _systemStatusRepository.FindAll(x => x.Property == _appContext.SSPN_DataImportLastStartDate).FirstOrDefault();
                DateTime dt;
                if (!forceStart && (!setRunningFlag.HasValue || setRunningFlag.Value) && dataImportLastStartDate != null && DateTime.TryParse(dataImportLastStartDate.Value, out dt) && DateTime.Now.Subtract(dt).TotalSeconds < _appContext.DataImportRespawnSeconds)
                    return true;

                if (setRunningFlag.HasValue)
                {
                    if (dataImportLastStartDate != null && !setRunningFlag.Value)
                        _systemStatusRepository.DeleteOnSubmit(dataImportLastStartDate);
                    else if (dataImportLastStartDate != null && setRunningFlag.Value)
                        dataImportLastStartDate.Value = DateTime.Now.ToString();
                    else if (setRunningFlag.Value)
                        _systemStatusRepository.InsertOnSubmit(new SystemStatus { Property = _appContext.SSPN_DataImportLastStartDate, Value = DateTime.Now.ToString() });

                    _systemStatusRepository.SubmitChanges();
                }
                return false;
            }
        }


        private void Log(ResultCode resultCode, params object[] paramss)
        {
            var result = _messages.Add(resultCode, paramss);
            _log.Write(result.Message, result.ResultType.ToString());
        }


        private void Log(IResults messages)
        {
            if (messages != null)
            {
                foreach (var m in messages)
                {
                    _log.Write(m.Message, m.ResultType.ToString());
                }
            }
        }

        //public IAppContext GetSettings(string dataImportConfigFilePath)
        //{
        //    Configuration configuration = ConfigurationManager.OpenExeConfiguration(dataImportConfigFilePath);
        //    AppContext settings = new AppContext(kvConfColl2nvColl(configuration.AppSettings.Settings));
        //    return settings;
        //}


        //NameValueCollection kvConfColl2nvColl(KeyValueConfigurationCollection settings)
        //{
        //    var nv = new NameValueCollection();
        //    if (settings != null)
        //    {
        //        foreach (var key in settings.AllKeys)
        //        {
        //            nv.Add(settings[key].Key, settings[key].Value);
        //        }
        //    }
        //    return nv;
        //}

        #endregion
    }
}
