﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableauServer.Contracts
{
    public interface ITableauServer
    {
        string GetTrustedAuthenticationTicket(string userName);
        ITableauServer Create(string url);
        bool HasAccess(string userName);
    }
}
