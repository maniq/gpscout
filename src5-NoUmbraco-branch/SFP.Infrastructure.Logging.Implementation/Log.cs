﻿using log4net;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Infrastructure.Logging.Implementation
{
    public class Log : SFP.Infrastructure.Logging.Contracts.ILog
    {
        private static readonly log4net.ILog Logger = LogManager.GetLogger(typeof(Log));

        public Log()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public void Write(string msg)
        {
            Logger.Info(msg);
        }

        public void Write(string msg, string category)
        {
            if (string.Compare(category, "error", true) == 0)
                Logger.Error(msg);
            else if (string.Compare(category, "warning", true) == 0)
                Logger.Warn(msg);
            else
                Logger.Info(msg);
        }

        public void Write(Exception ex)
        {
            //var sb = new StringBuilder();
            //Exception ex2 = ex;
            //while (ex2 != null)
            //{
            //    FormatErrorInfo(ex2, ref sb);
            //    ex2 = ex2.InnerException;
            //}
            //Logger.Error(sb.ToString());
            if (ex != null)
                Logger.Error(ex.ToString());
        }

        public string FormatErrorInfo(Exception ex)
        {
            var sb = new StringBuilder();
            if (ex != null)
            {
                sb.AppendLine(ex.Message)
                .Append("Source: ").AppendLine(ex.Source)
                .Append("StackTrace: ").AppendLine(ex.StackTrace);
            }
            return sb.ToString();
        }

        public void FormatErrorInfo(Exception ex, ref StringBuilder message)
        {
            if (ex != null)
            {
                message.AppendLine(ex.Message)
                .Append("Source: ").AppendLine(ex.Source)
                .Append("StackTrace: ").AppendLine(ex.StackTrace);
            }
        }
    }
}
