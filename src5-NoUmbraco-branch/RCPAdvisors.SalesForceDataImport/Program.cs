﻿using GPScout.DataImport.Contracts.Services;
using GPScout.Domain.Contracts.Models;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.SalesForceDataImport.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            container.LoadConfiguration();

            IDataImportManager dataImportManager = container.Resolve<IDataImportManager>();
            IResults results = dataImportManager.StartImport();
            SendNotifications(results);

            //var sfAuthInfo = new SFAuthenticationInfo()
            //{
            //    ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"],
            //    ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"],
            //    UserName = ConfigurationManager.AppSettings["UserName"],
            //    Password = ConfigurationManager.AppSettings["Password"]
            //};
            //var sfClient = new SalesforceClient(sfAuthInfo);
            //sfClient.Test();
        }

        private static void SendNotifications(IResults results)
        {
            try
            {

            }
            catch { }
        }
    }
}
