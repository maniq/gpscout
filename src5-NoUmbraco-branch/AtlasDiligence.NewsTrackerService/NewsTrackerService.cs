﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Diagnostics;
//using System.Linq;
//using System.Net.Mail;
//using System.ServiceProcess;
//using System.Text;
//using System.Timers;
//using AtlasDiligence.Common.Data.General;
//using AtlasDiligence.Common.Data.Models;
//using AtlasDiligence.Common.Data.Repositories;
//using AtlasDiligence.NewsTrackerService.Properties;

//namespace AtlasDiligence.NewsTrackerService
//{
//    public partial class NewsTrackerService : ServiceBase
//    {
//        #region service methods and Timer

//        private Timer _timer;
//        private static string[] allowedRecipientEmails;
//        private static bool isConsoleMode;

//        public NewsTrackerService()
//        {
//            InitializeComponent();
//        }

//        protected override void OnStart(string[] args)
//        {
//            LogEvents.WriteLog("Started service.", EventLogEntryType.Information);

//            _timer = new Timer(3600000);

//            _timer.Elapsed += new ElapsedEventHandler(TimerElapsed);

//            _timer.Enabled = true;

//            TimerElapsed(null, null);
//        }

//        protected override void OnStop()
//        {
//            _timer.Stop();
//            _timer.Dispose();
//            _timer = null;
//        }

//        private void TimerElapsed(object sender, ElapsedEventArgs e)
//        {
//            _timer.Stop();

//            DoWork();

//            _timer.Start();
//        }

//        public static void Main(string[] args)
//        {
//            var newsTracker = new NewsTrackerService();

//            if (!args.Contains("-production"))
//                allowedRecipientEmails = System.Configuration.ConfigurationManager.AppSettings["TestRecipients"].Split(',');

//            if (args.Contains("-console"))
//            {
//                isConsoleMode = true;
//                newsTracker.DoWork();
//            }
//            else
//            {
//                ServiceBase[] ServicesToRun = new ServiceBase[] { new NewsTrackerService() };

//                Run(ServicesToRun);
//            }
//        }

//        #endregion

//        public void DoWork()
//        {
//            var now = DateTime.Now;
//            var userRepository = new UserRepository();
//            var newsRepository = new NewsRepository();
//            var searchRepository = new SearchTermRepository();

//            List<aspnet_User> users;

//            if (allowedRecipientEmails == null)
//            {
//                users = userRepository.FindAll(m => m.UserExt != null
//                   && m.UserExt.RssNextEmailDate.HasValue
//                   && m.UserExt.RssNextEmailDate.Value <= now).ToList();
//            }
//            else
//            {
//                users = userRepository.FindAll(m => m.UserExt != null
//                && m.UserExt.RssNextEmailDate.HasValue
//                && m.UserExt.RssNextEmailDate.Value <= now
//                && allowedRecipientEmails.Contains(m.UserName)).ToList();
//            }

//            var allNewsItems = newsRepository.FindAll(m => m.PublishedDate >= now.AddMonths(-1)).ToList();

//            foreach (var user in users)
//            {
//                //get search terms
//                var searchTerms = searchRepository.GetSearchTermAndAliasesForUser(user.UserId).ToList();
//                var startDate = new DateTimeOffset();
//                var emailOffSet = String.Empty;
//                var date = user.UserExt.RssNextEmailDate.Value;
//                //get start date for retrieving news items and update user next email date
//                switch (user.UserExt.RssNextEmailOffsetEnum)
//                {
//                    //day
//                    case 1:
//                        startDate = user.UserExt.RssNextEmailDate.Value.AddDays(-1);
//                        while (date < DateTime.Now)
//                        {
//                            date = date.AddDays(1);
//                        }
//                        userRepository.UpdateUserEmailDate(user.UserId, date);
//                        emailOffSet = startDate.ToString("MMMM d, yyyy");
//                        break;
//                    //week
//                    case 2:
//                        startDate = user.UserExt.RssNextEmailDate.Value.AddDays(-7);
//                        date = user.UserExt.RssNextEmailDate.Value;
//                        while (date < DateTime.Now)
//                        {
//                            date = date.AddDays(7);
//                        }
//                        userRepository.UpdateUserEmailDate(user.UserId, date);
//                        emailOffSet = startDate.ToString("MMMM d, yyyy") + " - " + date.ToString("MMMM d, yyyy");

//                        break;
//                    //month
//                    case 3:
//                        startDate = user.UserExt.RssNextEmailDate.Value.AddMonths(-1);
//                        date = user.UserExt.RssNextEmailDate.Value;
//                        while (date < DateTime.Now)
//                        {
//                            date = date.AddMonths(1);
//                        }
//                        userRepository.UpdateUserEmailDate(user.UserId, date);
//                        emailOffSet = startDate.ToString("MMMM d, yyyy") + " - " + date.ToString("MMMM d, yyyy");
//                        break;
//                    default:
//                        LogEvents.WriteLog("Unknown email offset type used. UserId = " + user.UserId,
//                                           EventLogEntryType.Error);
//                        break;
//                }

//                //get items
//                var items = allNewsItems.Where(m => m.PublishedDate >= startDate).ToList();


//                //get email content
//                var emailContent = string.Empty;

//                //group by search term
//                foreach (var searchTerm in searchTerms.OrderBy(x => x.Key))
//                {
//                    //find all news articles containing term
//                    var filteredNewsItems = items.Where(item => SearchNewsItem(item, searchTerm.Key, searchTerm.Value)).ToList();

//                    //put items in NewsViewModel that includes search terms.
//                    foreach (var filteredNewsItem in filteredNewsItems)
//                    {
//                        //add search terms to news item
//                        News item = filteredNewsItem;
//                        filteredNewsItem.SearchTerms = searchTerms.Where(s => SearchNewsItem(item, s.Key, s.Value)).Select(s => s.Key).ToList();
//                    }

//                    //skip term if no items
//                    if (!filteredNewsItems.Any()) continue;

//                    //build list of articles in term group for email body
//                    emailContent += BuildEmailContent(searchTerm.Key, filteredNewsItems.OrderBy(o => o.PublishedDate));
//                }

//                //empty body equals no email
//                if (string.IsNullOrWhiteSpace(emailContent)) continue;

//                //get email body
//                var emailBody = GetEmailBody(user.UserExt.FirstName, user.UserExt.LastName, emailOffSet, emailContent);


//                //send email
//                var to = user.aspnet_Membership.Email;
//                var cc = user.UserRssEmails.Any()
//                                           ? user.UserRssEmails.Select(x => x.RssEmailAddress).Aggregate(
//                                               (seed, next) => seed + ", " + next)
//                                           : string.Empty;
//                var from = Settings.Default.MailFrom;
//                if (String.IsNullOrEmpty(from))
//                {
//                    from = AppSettings.MailFrom;
//                }
//                var subject = Settings.Default.MailSubject;
//                if (String.IsNullOrEmpty(subject))
//                {
//                    //subject = "Atlas Diligence News";
//                    subject = "GPScout Navigator - News";
//                }
//                SendMail(to, cc, from, subject, emailBody);
//            }
//        }

//        private bool SearchNewsItem(News item, String searchTerm, IEnumerable<String> aliases)
//        {
//            if (item.Description.ToLower().Contains(searchTerm.ToLower()) //description contains search term
//                || item.Subject.ToLower().Contains(searchTerm.ToLower())) //subject contains search term
//            {
//                return true;
//            }
//            if (aliases != null && (aliases.Any(x => item.Description.ToLower().Contains(x.ToLower())) //description contains search term alias
//                || aliases.Any(x => item.Subject.ToLower().Contains(x.ToLower())))) //subject contains search term alias
//            {
//                return true;
//            }
//            return false;
//        }

//        private static void SendMail(string to, string cc, string from, string subject, string body)
//        {
//            var mail = new MailMessage
//            {
//                From = new MailAddress(from),
//                Subject = subject,
//                IsBodyHtml = true,
//                Body = body
//            };
//            mail.BodyEncoding = new System.Text.UTF8Encoding();
//            mail.To.Add(to);
//            //mail.To.Add("doug.brandl+atlas@atlanticbt.com");
//            foreach (var str in cc.Split(','))
//            {
//                if (!String.IsNullOrEmpty(str))
//                    mail.To.Add(str);
//            }
//            try
//            {
//                new SmtpClient().Send(mail);
//                if (isConsoleMode) Console.WriteLine(string.Format("Email sent to {0}.", to));
//            }
//            catch (Exception e)
//            {
//                LogEvents.WriteLog("Error sending email to SMTP server. To: " + to, EventLogEntryType.Error);
//            }
//        }
//        private static string GetEmailBody(string firstName, string lastName, string timePeriod, string emailContent)
//        {
//            var emailTemplate = Resources.Templates.RssEmail;
//            emailTemplate = emailTemplate.Replace("|TIMEPERIOD|", timePeriod);
//            emailTemplate = emailTemplate.Replace("|CURRENTYEAR|", DateTime.Now.Year.ToString());
//            emailTemplate = emailTemplate.Replace("|CONTENT|", emailContent);
//            return emailTemplate;
//        }
//        private static string BuildEmailContent(string searchTerm, IEnumerable<News> newsItems)
//        {
//            var builder = new StringBuilder();

//            builder.Append(
//                 @"<tr>
//                        <td height=""45"" valign=""top"" style=""padding: 0 20px; font-size: 18px; font-weight: bold;"" width=""600"" colspan=""2"">
//                            <br />Search Term: " + searchTerm + @"
//                        </td>
//                    </tr>");
//            foreach (var item in newsItems.OrderByDescending(m => m.PublishedDate))
//            {
//                builder.Append(
//                    @"<tr>
//                        <td class=""article-title"" height=""45"" valign=""top"" style=""padding: 0 20px; font-size: 16px; font-weight: bold;"" width=""600"" colspan=""2"">
//                            <br /><a href='" + item.Url + @"' style=""color: #104b78;"">" + item.Subject + @"</a><br />
//                        </td>
//                    </tr>");
//                builder.Append(@"<tr>
//                                <td class=""article-source"" height=""20"" valign=""top"" style=""padding: 0 20px; font-size: 14px;"" width=""600"" colspan=""2"">
//                                    <span style=""font-weight: bold;"">Source:</span> " + item.OrganizationSourceName + @"<br />
//                                </td>
//                            </tr>");
//                builder.Append(
//@"<tr>
//                        <td class=""article-searchterms"" height=""20"" valign=""top"" style=""padding: 0 20px; font-size: 14px;"" width=""600"" colspan=""2"">
//                            <span style=""font-weight: bold;"">Search Terms:</span> " + string.Join(", ", item.SearchTerms) + @"<br />
//                        </td>
//                    </tr>");
//                builder.Append(@"<tr>
//                                <td class=""article-date"" height=""20"" valign=""top"" style=""padding: 0 20px; font-size: 14px;"" width=""600"" colspan=""2"">
//                                    <span style=""font-weight: bold;"">Date:</span> " + item.PublishedDate.ToString("M/d/yyyy") + @"<br />
//                                </td>
//                            </tr>");
//                if (!String.IsNullOrEmpty(item.Summary))
//                {

//                    if (!String.IsNullOrEmpty(item.Summary))//empty row
//                    {
//                        builder.Append(@"<tr >
//                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2"">&nbsp;
//                                    <br/>
//                                </td>
//                            </tr>");
//                    }
//                    //content
//                    builder.Append(@"<tr >
//                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2""><span style='font-size:14px; font-weight: bold; font-style:italic; padding-right:8px;'>Summary:</span>
//                                    " + item.Summary + @"
//                                </td>
//                            </tr>");
//                }

////                if (!String.IsNullOrEmpty(item.Summary))//test
////                {
////                    if (!String.IsNullOrEmpty(item.Summary))
////                    {
////                        builder.Append(@"<tr >
////                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2"">&nbsp;
////                                    <br/>
////                                </td>
////                            </tr>");
////                    }
////                    builder.Append(@"<tr >
////                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2""><span style='font-size:14px; font-weight: bold; font-style:italic; padding-right:8px;'>Content:</span>
////                                    " + item.Summary + @"
////                                </td>
////                            </tr>");
////                }

//                if (!String.IsNullOrEmpty(item.Description))
//                {
//                    if (!String.IsNullOrEmpty(item.Summary))
//                    {
//                        builder.Append(@"<tr >
//                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2"">&nbsp;
//                                    <br/>
//                                </td>
//                            </tr>");
//                    }
//                    builder.Append(@"<tr >
//                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2""><span style='font-size:14px; font-weight: bold; font-style:italic; padding-right:8px;'>Content:</span>
//                                    " + item.FormattedContent + @"
//                                </td>
//                            </tr>");
//                }

//                builder.Append(@"<tr >
//                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 12px; line-height: 20px;"" colspan=""2"">&nbsp;
//                                    <br/> <br/>
//                                </td>
//                            </tr>");
//            }
//            return builder.ToString();
//        }


//    }
//}
