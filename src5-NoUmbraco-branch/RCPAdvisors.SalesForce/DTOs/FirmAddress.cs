﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPScout.Salesforce.DTOs
{
    public class FirmAddress
    {
        public string Id { get; set; }
        public string Street_Address__c { get; set; }
        public string City__c { get; set; }
        public string State__c { get; set; }
        public string Country__c { get; set; }
        public string Firm_Location__c { get; set; }
        public string Address_Description__c { get; set; }
        public string Phone__c { get; set; }
        public string Fax__c { get; set; }
        public string Zip_Code__c { get; set; }
        public string Address_2__c { get; set; }

    }
}
