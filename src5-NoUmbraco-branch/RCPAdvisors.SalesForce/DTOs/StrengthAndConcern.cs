﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class StrengthAndConcern
    {
        public string Id { get; set; }
        public decimal Display_Order__c { get; set; }
        public bool Publish__c { get; set; }
        public string Account__c { get; set; }
        public string Type__c { get; set; }
        public string Name__c { get; set; }
        public string External_Text__c { get; set; }

    }
}
