﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Account
    {
        public string Id { get; set; }
        public string Website { get; set; }
        public string Most_Recent_Fund_Industry_Focus__c { get; set; }
        public string YearGPFounded__c { get; set; }
        public decimal Most_Recent_Fund_Eff_Size__c { get; set; }
        public string Alias__c { get; set; }
        public decimal AUM__c { get; set; }
        public bool Emerging_Manager__c { get; set; }
        public bool Co_Investment_Opportunities_for_Non_LPs__c { get; set; }
        public string Radar_List__c { get; set; }
        public string Overview__c { get; set; }
        public string Investment_Thesis__c { get; set; }
        public string Team_Overview__c { get; set; }
        public string Key_Takeaway__c { get; set; }
        public string Grade_Qualitative_Text__c { get; set; }
        public string Grade_Quantitative_Text__c { get; set; }
        public string Grade_Scatterchart_Text__c { get; set; }
        public string Track_Record_Text__c { get; set; }
        public string Evaluation_Text__c { get; set; }
        public decimal Total_Closed_Funds__c { get; set; }
        public string Most_Recent_Fund_Market_Stage__c { get; set; }
    }
}
