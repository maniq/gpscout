﻿using GPScout.DataImport.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.Data
{
    public class Repository : IRepository
    {
        public string ConnectionString { get; set; }

        public Repository()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DataStaging_ConnectionString"].ConnectionString;
        }

        public IEnumerable<T> Select<T>() where T: class
        {
            if (!typeof(T).IsAssignableFrom(typeof(SFEntityImportConfig)))
                throw new NotImplementedException();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("SF_GetEntityImportConfig", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                var dr = cmd.ExecuteReader( CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    yield return new SFEntityImportConfig {
                         Enabled = Convert.ToBoolean(dr["Enabled"]),
                         EntityCode = Convert.ToString(dr["EntityCode"]),
                         EntityCodeInSource = Convert.ToString(dr["EntityCodeInSource"]),
                         EntityFriendlyName = Convert.ToString(dr["EntityFriendlyName"]),
                         ID = Convert.ToInt32(dr["ID"]),
                         SelectPageSize = Convert.ToInt32(dr["SelectPageSize"]),
                         SelectQuery = Convert.ToString(dr["SelectQuery"]),
                         SourceUri = Convert.ToString(dr["SourceUri"]),
                         TargetDTO = Convert.ToString(dr["TargetDTO"]),
                         OrderBy = Convert.ToInt32(dr["OrderBy"])
                    } as T;
                }

            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public T Delete<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public T Add<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public Domain.Contracts.Models.DataSyncResult BulkInsert<T>()
        {
            throw new NotImplementedException();
        }
    }
}
