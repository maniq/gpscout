﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.Domain.Contracts.Enums;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using SFP.Extensions;
using SFP.Infrastructure.Email.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Utility;

namespace GPScout.Domain.Implementation.Services
{
    public class EmailDistributionListService : IEmailDistributionListService
    {
        #region Properties
        IRepository<EmailDistributionList> _emailDistributionListRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRepository<EmailDistributionList>>();
            }
        }

        IRepository<EmailDistributionListEmail> _emailDistributionListEmailRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRepository<EmailDistributionListEmail>>();
            }
        }

        IUserService _userService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        IEmail _emailHelper
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEmail>();
            }
        }
        #endregion


        #region IEmailDistributionListService
        public IEnumerable<EmailDistributionList> GetAllActiveLists()
        {
            return _emailDistributionListRepository.FindAll(x => x.Active).OrderBy(x => x.Name);
        }

        public IResults AddEmail(EmailDistributionListEmail email)
        {
            var results = new Results();
            if (email != null)
            {
                email.Email = email.Email.Trim();
                if (!Utility.Utils.IsValidEmail(email.Email))
                {
                    results.Add(ResultCode.Validation_InvalidEmail, email.Email);
                    return results;
                }

                var distribList = _emailDistributionListRepository.FindAll(x => x.ID == email.DistributionListID).Single();
                if (distribList != null)
                {
                    if (!distribList.Emails.Any(e => e.Email == email.Email))
                    {
                        distribList.Emails.Add(email);
                        _emailDistributionListRepository.SubmitChanges();
                    } else
                    {
                        results.Add(ResultCode.EmailDistributionList_DuplicateEmail, email.Email, distribList.Name);
                    }

                } else
                {
                    results.Add(ResultCode.EmailDistributionList_NotFound);
                }
            }
            return results;
        }

        public IEnumerable<EmailDistributionListEmail> GetActiveEmailsByListId(int id)
        {
            var list = _emailDistributionListRepository.FindAll(l => l.ID == id).SingleOrDefault();
            return list != null ? list.Emails.Where(e => e.Active).OrderBy(e => e.Email).AsEnumerable() : new EmailDistributionListEmail[] { };
        }


        public IResults DeleteEmail(int emailID)
        {
            var results = new Results();
            var email = _emailDistributionListEmailRepository.FindAll(e => e.ID == emailID).FirstOrDefault();
            if (email != null)
            {
                _emailDistributionListEmailRepository.DeleteOnSubmit(email);
                _emailDistributionListEmailRepository.SubmitChanges();
                return results;
            }
            results.Add(ResultCode.ItemNotFound);
            return results;      
        }


        public IResults SendWeeklyReportEmails(string from, DateTime? weekOfDate = null)
        {
            var results = new Results();
            try
            {
                // some cleanup of attachment files folder
                string path = ConfigurationManager.AppSettings["ExcelExportFileLocation"];
                int errNo;
                int deletedNo = Utils.DeleteFiles(path, "*.xlsx", DateTime.Now.AddDays(-60), out errNo);
                results.Add(ResultCode.InfoMsg, string.Format("{0} obsolete attachment file(s) deleted, {1} error(s).", deletedNo, errNo));

                // calculate previous week
                var week = weekOfDate.PreviousWeek();
                string dateRangeStr = string.Format("{0} - {1}", week.StartDate.Value.ToString("MM.dd.yyyy"), week.EndDate.Value.ToString("MM.dd.yyyy"));

                // create attachment files
                string fileName = string.Format("{0} {1}.xlsx", "Profile View Weekly Report", dateRangeStr);
                fileName = Path.Combine(path, fileName);
                string userProfileViewTrackingFile = _userService.CreateUserProfileViewTrackingExcel(TrackUsersType.ProfileVisit, week.StartDate.Value, week.EndDate.Value, fileName);
                results.Add(ResultCode.InfoMsg, string.Format("{0} attachment file generated.", userProfileViewTrackingFile));

                fileName = string.Format("{0} {1}.xlsx", "Profile Pdf Generation Weekly Report", dateRangeStr);
                fileName = Path.Combine(path, fileName);
                string userOrgPdfGenerationTrackingFile = _userService.CreateUserProfileViewTrackingExcel(TrackUsersType.ProfileFullPdfReport, week.StartDate.Value, week.EndDate.Value, fileName);
                results.Add(ResultCode.InfoMsg, string.Format("{0} attachment file generated.", userOrgPdfGenerationTrackingFile));

                // send emails to distribution list
                string subject = "GPScout Weekly Report";
                string body = string.Format("Attached are the profile view and PDF generation reports for {0} - {1}.", week.StartDate.Value.ToShortDateString(), week.EndDate.Value.ToShortDateString());

                var sendingResults = SendToEmailDistributionList(EmailDistribution.ProfileViewReport, subject, body, from, new string[] { userProfileViewTrackingFile, userOrgPdfGenerationTrackingFile });
                results.Add(sendingResults);
            }
            catch (Exception ex)
            {
                results.Add(ex);
            }
            return results;
        }

        public IResults SendToEmailDistributionList(EmailDistribution emailDistribution, string subject, string body, string from)
        {
            return this.SendToEmailDistributionList(emailDistribution, subject, body, from, null);
        }

        public IResults SendOrganizationFullReportPdfQuotaAchieved(string from, Group group)
        {
            var results = new Results();
            try
            {
                string subject = string.Format("GPScout User Group '{0}' - Organization Full Report Pdf Quota Achieved", group.Name);
                string additionalText = (group.OrganizationProfilePdfQuotaActionID == Group.ProfilePdfQuotaActionEnum.BlockDownloadsAndNotifyRCP) ? "Further downloads not allowed." : group.OrganizationProfilePdfQuotaActionID == Group.ProfilePdfQuotaActionEnum.NotifyRCPOnQuotaAchieved ? "Further downloads are allowed." : "WARNING: Further download status unclear.";
                string body = string.Format("Organization Full Report Pdf Quota of <b>{0} pdfs per {1} days</b> has been achieved by User Group <b>{2}</b> (group id: {3}).<p>{4}</p>", group.OrganizationProfilePdfQuota, group.OrganizationProfilePdfQuotaDays, group.Name, group.Id, additionalText);

                var sendingResults = SendToEmailDistributionList(EmailDistribution.OrgFullReportPdfUserGroupQuotaAchieved, subject, body, from, null);
                results.Add(sendingResults);
            }
            catch (Exception ex)
            {
                results.Add(ex);
            }
            return results;
        }
        #endregion

        #region Helper Methods
        protected IResults SendToEmailDistributionList(EmailDistribution emailDistribution, string subject, string body, string from, IEnumerable<string> attachments)
        {
            var results = new Results();
            List<Attachment> _attachments = null;
            int errors = 0, sent = 0;

            var list = _emailDistributionListRepository.FindAll(l => l.Active && l.ID == (int)emailDistribution).FirstOrDefault();

            if (list != null)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("Start sending emails to {0} distribution list:", emailDistribution).AppendLine()
                    .AppendFormat("Subject: '{0}'", subject).AppendLine()
                    .AppendFormat("Body:    '{0}'", body).AppendLine()
                    .AppendFormat("Attachment(s): {0}", attachments != null ? attachments.Count() : 0);

                if (attachments != null && attachments.Any())
                {
                    sb.AppendLine();
                    attachments.ToList().ForEach(a => sb.AppendLine(a));
                    _attachments = _emailHelper.CreateAttachments(attachments).ToList();
                }

                results.Add(ResultCode.InfoMsg, sb.ToString());
                foreach (EmailDistributionListEmail email in list.Emails.Where(e => e.Active))
                {
                    try
                    {
                        _emailHelper.Send(subject, body, from, email.Email, _attachments);
                        results.Add(ResultCode.InfoMsg, string.Format("{0} email sent to {1}.", emailDistribution, email.Email));
                        sent++;
                    }
                    catch (Exception ex)
                    {
                        results.Add(ex);
                        errors++;
                    }
                }
            }
            else
                results.Add(ResultCode.EmailDistributionList_NotFound, emailDistribution);

            results.Add(ResultCode.InfoMsg, string.Format("Sending to {0} distribution list finished, {1} email(s) sent, {2} error(s).", emailDistribution, sent, errors));
            return results;
        }
        #endregion
    }
}
