﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SearchOption = AtlasDiligence.Common.Data.Models.SearchOption;

namespace GPScout.Domain.Implementation.Services
{

    public class SearchService : ISearchService
    {
        private AtlasWebDbDataContext context;
        private IRepository<Organization> OrganizationRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRepository<Organization>>();
            }
        }


        private IRepository<aspnet_User> UserRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRepository<aspnet_User>>();
            }
        }

        // If you want to add a field for the 'universal search' for a term, please do not add it here, add it to the IndexService.ReIndex universal search field.  It is more efficient
        private readonly List<SearchField> fieldsToTermSearch = new List<SearchField>
                                                                 {
                                                                     SearchField.UniversalSearch
                                                                 };

        public SearchService()
        {
			context = new AtlasWebDbDataContext();
		}


        public IEnumerable<LuceneSearchResult> GetOrganizations(SearchFilter filter, out int total)
        {
            var stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();

            var queryString = this.BuildQueryString(filter);
            var result = this.GetOrganizationsFromLuceneString(queryString.ToString(), out total, null);

            stopWatch.Stop();
            System.Diagnostics.Debug.WriteLine("Get Organizations from lucene took " + stopWatch.Elapsed + " seconds");
            return result;
        }

        /// <summary>
        /// Returns all organizations that match the specific segment that the specified one matches on
        /// </summary>
        /// <param name="availableSegments"></param>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        public IEnumerable<LuceneSearchResult> GetOrganizationsMatchOrganizationSegment(IList<Segment> availableSegments, Guid organizationId)
        {
            var retval = new List<LuceneSearchResult>();
            foreach (var segment in availableSegments)
            {
                int total;
                var results =
                    this.GetOrganizations(new SearchFilter { Segments = new List<Segment> { segment } }, out total);
                if (results.Any(m => m.Id == organizationId))
                {
                    retval.AddRange(results);
                }
            }
            return retval.Distinct();
        }

        public IEnumerable<LuceneSearchResult> GetOrganizationsContainingTerm(IList<Segment> segments, IList<Guid> purchasedOrganizations, string term)
        {
            var builder = new StringBuilder();
            var writeAnd = false;
            if (segments.Any())
            {
                builder.AppendLine(this.AppendSegmentSearch(segments, purchasedOrganizations, ref writeAnd));
            }
            // remove any non alpha-numeric character 
            //term = Regex.Replace(term, @"[^A-Za-z0-9 ]", string.Empty); // might want to replace this with a space instead
            var termSplit = EscapeAndSplitSearchTerms(term);
            foreach (var split in termSplit)
            {
                if (writeAnd)
                {
                    builder.Append(" AND ");
                }
                builder.Append(string.Format("{0}:*{1}*", SearchField.OrganizationName.GetFieldName(), split));
                writeAnd = true;
            }
            int total;
            return this.GetOrganizationsFromLuceneString(builder.ToString(), out total, null);
        }

        public IEnumerable<Organization> Search(SearchFilter filter, out int total)
        {
            var orderedIds = this.GetOrganizationIdsFromLucene(filter, out total).Skip(filter.Skip).Take(filter.Take).ToList();
            var organizations = OrganizationRepository.FindAll(m => orderedIds.Contains(m.Id)).ToList().OrderBy(m => orderedIds.IndexOf(m.Id));

            return organizations;
        }


        public IEnumerable<Guid> Search(SearchFilter filter)
        {
            int total;
            var orderedIds = this.GetOrganizationIdsFromLucene(filter, out total);
            //var organizations = OrganizationRepository.FindAll(m => orderedIds.Contains(m.Id)).ToList();

            return orderedIds;
        }


        public IEnumerable<Organization> Search(IEnumerable<Guid> orderedOrganizationIds, SearchFilter filter)
        {
            var orderedIds = orderedOrganizationIds.Skip(filter.Skip).Take(filter.Take).ToList();
            var organizations = OrganizationRepository.FindAll(m => orderedIds.Contains(m.Id)).ToList().OrderBy(m => orderedIds.IndexOf(m.Id));

            return organizations;
        }


        public IEnumerable<LuceneSearchResult> GetOrganizationsFromLuceneString(string queryString, out int total, ProximitySearch geoProximitySearch)
        {
            var query = ParseQueryString(queryString, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));

            var sort = new Sort(new SortField(SearchField.OrganizationName.GetFieldName() + "Sort", SearchField.OrganizationName.GetLuceneFieldAttribute().SortField, false));
            var directoryInfo = new DirectoryInfo(AppSettings.IndexPath);
            using (var directory = FSDirectory.Open(directoryInfo))
            using (var reader = IndexReader.Open(directory, true))
            using (var searcher = new IndexSearcher(directory, true))
            {
                var collector = TopFieldCollector.Create(sort, reader.MaxDoc, false, false, false, false);
                searcher.Search(query, collector);
                var results = collector.TopDocs();

                total = results.TotalHits;

                var docs = results.ScoreDocs.OrderBy(m => m.Score).Select(m => m.Doc);

                var retval = new List<LuceneSearchResult>();
                bool isGeoProximitySearch = geoProximitySearch != null;
                if (!isGeoProximitySearch) geoProximitySearch = new ProximitySearch(); // just to help translating latlong strings to locations
                foreach (var doc in docs)
                {
                    // Id
                    var orgId = new Guid(searcher.Doc(doc).Get(SearchField.OrganizationId.GetFieldName()));

                    // only add distinct values
                    if (retval.Any(a => a.Id == orgId))
                    {
                        continue;
                    }

                    // Positions
                    IEnumerable<LatitudeAndLongitude> position = new List<LatitudeAndLongitude>();

                    var searchDoc = searcher.Doc(doc).Get(SearchField.OrganizationPosition.GetFieldName());
                    if (searchDoc != null)
                    {
                        position = geoProximitySearch.GetLocations(searchDoc, isGeoProximitySearch);
                        if (isGeoProximitySearch && !position.Any()) continue;
                    }

                    // Quantity
                    var quantity = new double?();
                    var pulledQuantity = searcher.Doc(doc).Get(SearchField.OrganizationQuantity.GetFieldName());
                    if (!pulledQuantity.IsNullOrWhiteSpace())
                    {
                        quantity = Convert.ToDouble(pulledQuantity);
                    }

                    // Quality
                    var quality = new double?();
                    var pulledQuality = searcher.Doc(doc).Get(SearchField.OrganizationQuality.GetFieldName());
                    if (!pulledQuality.IsNullOrWhiteSpace())
                    {
                        quality = Convert.ToDouble(pulledQuality);
                    }

                    // LastUpdated
                    var lastUpdated = new DateTime?();
                    var pulledLastUpdated = searcher.Doc(doc).Get(SearchField.OrganizationLastUpdated.GetFieldName());
                    if (!pulledLastUpdated.IsNullOrWhiteSpace())
                    {
                        lastUpdated = Lucene.Net.Documents.DateTools.StringToDate(pulledLastUpdated);
                    }

                    // ExpectedNextFundraise
                    var pulledExpectedFundraise = searcher.Doc(doc).Get(SearchField.OrganizationExpectedNextFundraise.GetFieldName());
                    var expectedFundraise = String.IsNullOrWhiteSpace(pulledExpectedFundraise) ? new DateTime?() : Lucene.Net.Documents.DateTools.StringToDate(pulledExpectedFundraise);
                    var fundraisingStatus = searcher.Doc(doc).Get(SearchField.OrganizationFundraisingStatus.GetFieldName());

                    // IsPublished
                    var isPublished = Convert.ToBoolean(searcher.Doc(doc).Get(SearchField.OrganizationPublishOverviewAndTeam.GetFieldName()));

                    // IsPublishProfile
                    var isPublishProfile = Convert.ToBoolean(searcher.Doc(doc).Get(SearchField.OrganizationPublishProfile.GetFieldName()));

                    // Name
                    var name = searcher.Doc(doc).Get(SearchField.OrganizationName.GetFieldName());

                    // Focus radar
                    var focusRadar = searcher.Doc(doc).Get(SearchField.FocusRadar.GetFieldName());

                    // Inactive Firm
                    var inactiveFirm = Convert.ToBoolean(searcher.Doc(doc).Get(SearchField.OrganizationInactiveFirm.GetFieldName()));

                    // assign LuceneSearchResult
                    if (!string.IsNullOrWhiteSpace(name))
                    {

                        retval.Add(new LuceneSearchResult { Id = orgId, Name = name, Quantity = quantity, Quality = quality, LastUpdated = lastUpdated, ExpectedNextFundraise = expectedFundraise, FundraisingStatus = fundraisingStatus, IsPublished = isPublished, Positions = position, FocusRadar = focusRadar, IsPublishProfile = isPublishProfile, InactiveFirm = inactiveFirm });
                    }
                }
                return retval;
            }
        }


        private static Query ParseQueryString(string queryString, Analyzer analyzer)
        {
            if (String.IsNullOrWhiteSpace(queryString))
            {
                queryString = string.Format("{0}:{1}", SearchField.IsOrganization.GetFieldName(), "true");
            }

            // just using a standard parser since we are creating our own query
            var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, SearchField.FundName.GetFieldName(),
                                         analyzer);

            Query query;
            try
            {
                parser.AllowLeadingWildcard = true;
                query = parser.Parse(queryString);
            }
            catch (ParseException e)
            {
                query = parser.Parse(QueryParser.Escape(queryString));
            }
            return query;
        }

        private IEnumerable<Guid> GetOrganizationIdsFromLucene(SearchFilter filter, out int total)
        {
            var queryString = this.BuildQueryString(filter);
            if (String.IsNullOrWhiteSpace(queryString))
            {
                queryString = string.Format("{0}:{1}", SearchField.IsOrganization.GetFieldName(), "true");
            }
            System.Diagnostics.Debug.WriteLine(queryString);

            // just using a standard parser since we are creating our own query
            var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, SearchField.FundName.GetFieldName(),
                                         new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));

            Query query;
            try
            {
                parser.AllowLeadingWildcard = true;
                query = parser.Parse(queryString);
            }
            catch (ParseException e)
            {
                query = parser.Parse(QueryParser.Escape(queryString));
            }


            var sortFields = new List<SortField>();
            
            for (int i=0; i< filter.SortFields.Length; i++)
            {
                sortFields.Add(new SortField(filter.SortFields[i].GetFieldName() + "Sort", filter.SortFields[i].GetLuceneFieldAttribute().SortField, !filter.IsDescending[i]));
            }


            var sort = new Sort(sortFields.ToArray());

            //var sort = new Sort(new SortField(filter.SortField.GetFieldName() + "Sort", filter.SortField.GetLuceneFieldAttribute().SortField, filter.IsDescending));
            var directoryInfo = new DirectoryInfo(AppSettings.IndexPath);
            var directory = FSDirectory.Open(directoryInfo);
            var reader = IndexReader.Open(directory, true);
            var searcher = new IndexSearcher(directory, true);

            //var collector = TopFieldCollector.Create(sort, reader.MaxDoc, false, false, false, false);
            var collector = TopFieldCollector.Create(sort, reader.MaxDoc, false, false, false, false);
            searcher.Search(query, collector);
            var results = collector.TopDocs();
            var docs = results.ScoreDocs.OrderBy(m => m.Score).Select(m => m.Doc);

            IEnumerable<Guid> retval;
            if (filter.ProximitySearch != null)
            {
                retval = docs
                    .Where(m => filter.ProximitySearch.IsInRadius(searcher.Doc(m).Get(SearchField.OrganizationPosition.GetFieldName())))
                    .Select(m => new Guid(searcher.Doc(m).Get(SearchField.OrganizationId.GetFieldName()))).Distinct().ToList();
            }
            else
                retval = docs.Select(m => new Guid(searcher.Doc(m).Get(SearchField.OrganizationId.GetFieldName()))).Distinct().ToList();

            total = retval.Count();
            return retval;
        }

        public string BuildQueryString(SearchFilter filter)
        {
            var builder = new StringBuilder();
            var writeAnd = false;

            // segements
            if (filter.Segments.Any())
            {
                builder.Append(AppendSegmentSearch(filter.Segments.ToList(), filter.PurchasedOrganizationIds.ToList(), ref writeAnd));
            }

            // terms
            if (!string.IsNullOrWhiteSpace(filter.Term))
            {
                string[] splitTerm = EscapeAndSplitSearchTerms(filter.Term);
                if (splitTerm.Length > 0)
                {

                    if (writeAnd)
                    {
                        builder.Append(" AND ");
                    }
                    builder.Append("(");


                    for (var i = 0; i < this.fieldsToTermSearch.Count(); i++)
                    {
                        builder.Append("(");
                        for (var j = 0; j < splitTerm.Length; j++)
                        {
                            builder.Append(string.Format("{0}:*{1}*", this.fieldsToTermSearch[i].GetFieldName(), splitTerm[j].ToLower()));
                            if (j + 1 < splitTerm.Length)
                            {
                                builder.Append(" AND ");
                            }
                        }
                        builder.Append(")");
                        if (i + 1 < this.fieldsToTermSearch.Count())
                        {
                            builder.Append(" OR ");
                        }
                    }
                    builder.Append(")");
                    writeAnd = true;
                }
            }

            // other Search fields
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationStrategies, filter.Strategies, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationSubStrategy, filter.SubStrategies, ref writeAnd));
            //builder.Append(AppendFieldExactSearch(SearchField.OrganizationInvestmentSubRegion, filter.InvestmentSubRegions, ref writeAnd));
            //builder.Append(AppendFieldExactSearch(SearchField.OrganizationCountries, filter.Countries, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationQualitativeGrades, filter.QualitativeGrades, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationQuantitativeGrades, filter.QuantitativeGrades, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationMarketStage, filter.MarketStage, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationSectorFocus, filter.Sector, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationInvestmentRegion, filter.InvestmentRegions, ref writeAnd));

            builder.Append(AppendFieldExactSearch(SearchField.OrganizationNumberOfFunds, filter.NumberOfFundsClosedMinimum, filter.NumberOfFundsClosedMaximum, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationDiligenceLevel, filter.DiligenceStatusMinimum, filter.DiligenceStatusMaximum, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationEmergingManager, filter.EmergingManager, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationFocusList, filter.FocusList, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationSectorSpecialist, filter.SectorSpecialist, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationRegionalSpecialist, filter.RegionalSpecialist, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationAccessConstrained, filter.AccessConstrained, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationSBICFund, filter.SBICFund, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.FocusRadar, filter.FocusRadar, ref writeAnd));
            builder.Append(AppendFieldExactSearch(SearchField.OrganizationFundraisingStatus, filter.FundraisingStatus, ref writeAnd));

            if (!string.IsNullOrEmpty(filter.FirmFilter))
            {
                if (filter.FirmFilter == FirmFilter.RecentlyUpdated)
                {
                    if (writeAnd)
                    {
                        builder.Append(" AND ");
                    }
                    builder.Append("(");
                    //var startString = Lucene.Net.Documents.DateTools.DateToString(new DateTime(1970, 1, 1), Lucene.Net.Documents.DateTools.Resolution.DAY);
                    var startString = Lucene.Net.Documents.DateTools.DateToString(DateTime.Now.Date.AddDays(-90), Lucene.Net.Documents.DateTools.Resolution.DAY);

                    var endString = Lucene.Net.Documents.DateTools.DateToString(DateTime.Now.AddYears(1), Lucene.Net.Documents.DateTools.Resolution.DAY);

                    builder.Append(string.Format("{0}:[{1} TO {2}]", SearchField.OrganizationLastUpdated.GetFieldName(), startString, endString));
                    builder.Append(")");
                    writeAnd = true;
                }
                else if (filter.FirmFilter == FirmFilter.FocusList && (filter.FocusList == null || !filter.FocusList.Any()))
                {
                    builder.Append(AppendFieldExactSearch(SearchField.OrganizationFocusList, new List<string> { SearchFilter.FocusListStr }, ref writeAnd));
                }
            }

            if (filter.ShowInactiveFirms == false)
                // show firms whose InactiveFirm == false
                builder.Append(AppendFieldExactSearch(SearchField.OrganizationInactiveFirm, false, ref writeAnd));

            // published filter
            //if (filter.ShowOnlyPublished)
            //{
            //    if (writeAnd)
            //    {
            //        builder.Append(" AND ");
            //    }
            //    builder.Append("(");
            //    builder.Append(string.Format("{0}:{1}", SearchField.OrganizationPublishOverviewAndTeam.GetFieldName(), filter.ShowOnlyPublished.ToString()));
            //    builder.Append(")");
            //    writeAnd = true;
            //}

            // special date range logic
            if (filter.ExpectedNextFundraiseStartDate.HasValue || filter.ExpectedNextFundraiseEndDate.HasValue)
            {
                if (writeAnd)
                {
                    builder.Append(" AND ");
                }
                builder.Append("(");
                var startString = Lucene.Net.Documents.DateTools.DateToString(
                                          filter.ExpectedNextFundraiseStartDate.GetValueOrDefault(DateTime.Now),
                                          Lucene.Net.Documents.DateTools.Resolution.DAY);

                var endString = filter.ExpectedNextFundraiseEndDate.HasValue
                                      ? Lucene.Net.Documents.DateTools.DateToString(
                                          filter.ExpectedNextFundraiseEndDate.Value,
                                          Lucene.Net.Documents.DateTools.Resolution.DAY)
                                      : "99999999";

                // if start date is today, also search on 'open' fundraising status
                if (filter.ExpectedNextFundraiseStartDate.GetValueOrDefault(DateTime.Now.Date) == DateTime.Now.Date)
                {
                    builder.Append(string.Format("{0}:{1} OR {0}:{2} OR ", SearchField.OrganizationFundraisingStatus.GetFieldName(), "open", "\"upcoming final close\""));
                }
                builder.Append(string.Format("{0}:[{1} TO {2}]", SearchField.OrganizationExpectedNextFundraise.GetFieldName(),
                                             startString, endString));
                builder.Append(")");
                writeAnd = true;
            }

            // fund size
            if (filter.FundSizeMinimum.HasValue || filter.FundSizeMaximum.HasValue)
            {
                string advancedFundQuery = string.Format("{0}:[{1} TO {2}] AND ({3}:\"{4}\" OR {5}:\"{6}\")",
                                                         SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
                                                         filter.FundSizeMinimum.HasValue
                                                             ? filter.FundSizeMinimum.Value.ToString("F").PadLeft(10, '0')
                                                             : "0.00".PadLeft(10, '0'),
                                                         filter.FundSizeMaximum.HasValue
                                                             ? filter.FundSizeMaximum.Value.ToString("F").PadLeft(10, '0')
                                                             : "9".PadRight(7, '9') + ".99",
                                                             SearchField.OrganizationCurrency.GetFieldName(),
                                                             filter.Currencies.Any() ? filter.Currencies.FirstOrDefault() : String.Empty,
                                                             SearchField.FundCurrency.GetFieldName(),
                                                             filter.Currencies.Any() ? filter.Currencies.FirstOrDefault() : String.Empty);

                if (writeAnd)
                {
                    builder.Append(String.Format(" AND ({0})", advancedFundQuery));
                }
                else
                {
                    builder.Append(advancedFundQuery);
                }

                writeAnd = true;
            }

            System.Diagnostics.Debug.WriteLine("LUCENE QUERY STRING:  " + builder.ToString());
            return builder.ToString();
        }

        private string AppendSegmentSearch(IList<Segment> segments, IList<Guid> purchasedOrganizations, ref bool writeAnd)
        {
            if (!segments.Any())
            {
                return string.Empty;
            }

            var retval = new StringBuilder();
            retval.Append("(");
            var getSearchFieldFilter = new Func<FieldType, List<String>, string>((field, values) =>
            {
                var searchFieldReturn = new StringBuilder();
                searchFieldReturn.Append("(");
                SearchField type;
                switch (field)
                {
                    case FieldType.Strategy:
                        type = SearchField.OrganizationStrategies;
                        break;
                    case FieldType.MarketStage:
                        type = SearchField.OrganizationMarketStage;
                        break;
                    case FieldType.InvestmentRegion:
                    //case FieldType.SubRegion:
                        type = SearchField.OrganizationInvestmentRegion;
                        break;
                    //case FieldType.Country:
                    //    type = SearchField.OrganizationCountries;
                    //    break;
                    case FieldType.SBICFund:
                        type = SearchField.OrganizationSBICFund;
                        values = values.Select(s => s.Equals("1") ? "true" : "false").ToList(); // sbic field is stored as true and false in lucene index
                        break;
                    default:
                        // unknown filter
                        throw new ApplicationException("Unknown segment search field type");
                        break;
                }
                var valuesList = values.Select(m => string.Format("{0}:\"{1}\"", type.GetFieldName(), m.ToLower())).ToList();
                searchFieldReturn.Append(string.Join(" OR ", valuesList));
                searchFieldReturn.Append(")");
                return searchFieldReturn.ToString();
            });
            var getPairFieldFilter = new Func<Dictionary<FieldType, List<string>>, string>((pairs) =>
            {
                var pairFieldReturn = new StringBuilder();
                pairFieldReturn.Append("(");
                pairFieldReturn.Append(String.Join(" AND ", pairs.Select(m => getSearchFieldFilter(m.Key, m.Value))));
                pairFieldReturn.Append(")");
                return pairFieldReturn.ToString();
            });

            var filters = segments.Select(m => getPairFieldFilter(m.Filters));
            retval.Append(string.Join(" OR ", filters));
            if (purchasedOrganizations != null && purchasedOrganizations.Any())
            {
                retval.Append(" OR (");
                retval.Append(string.Format("({0}:{1} AND (", SearchField.IsOrganization.GetFieldName(), "true"));
                retval.Append(string.Join(" OR ",
                                                     purchasedOrganizations.Select(
                                                         m =>
                                                         string.Format("{0}:\"{1}\"",
                                                                       SearchField.OrganizationId.GetFieldName(),
                                                                       m.ToString()))));
                retval.Append(")))");
            }
            retval.Append(")");
            writeAnd = true;
            return retval.ToString().Replace("() OR ", string.Empty);
        }

        private string AppendFieldExactSearch(SearchField field, string value, ref bool writeAnd)
        {
            var retval = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                if (writeAnd)
                {
                    retval += " AND ";
                }
                retval += (string.Format("{0}:{1}", field.GetFieldName(), value));
                writeAnd = true;
            }
            return retval;
        }

        private string AppendFieldExactSearch(SearchField field, IEnumerable<string> values, ref bool writeAnd)
        {
            if (values == null)
                return string.Empty;
            var vals = values.Where(m => !String.IsNullOrWhiteSpace(m)).ToArray();
            var retval = string.Empty;
            if (vals.Any())
            {
                if (writeAnd)
                {
                    retval += " AND ";
                }
                retval += "(";
                for (var i = 0; i < vals.Count(); i++)
                {
                    retval += (string.Format("{0}:\"{1}\"", field.GetFieldName(), vals[i]));
                    if (i < vals.Count() - 1)
                    {
                        retval += " OR ";
                    }
                }
                retval += ")";
                writeAnd = true;
            }
            return retval;
        }

        private string AppendFieldExactSearch(SearchField field, bool? value, ref bool writeAnd)
        {
            var retval = string.Empty;
            if (value.HasValue)
            {
                if (writeAnd)
                {
                    retval += " AND ";
                }
                retval += (string.Format("{0}:{1}", field.GetFieldName(), value.Value.ToString()));
                writeAnd = true;
            }
            return retval;
        }

        private string AppendFieldExactSearch(SearchField field, int? minimum, int? maximum, ref bool writeAnd)
        {
            var retval = string.Empty;
            if (minimum.HasValue || maximum.HasValue)
            {
                if (writeAnd)
                {
                    retval += " AND ";
                }
                retval += (string.Format("{0}:[{1} TO {2}]",
                    field.GetFieldName(),
                    minimum.HasValue ? minimum.Value.ToString().PadLeft(10, '0') : string.Empty.PadLeft(10, '0'),
                    maximum.HasValue ? maximum.Value.ToString().PadLeft(10, '0') : "9".PadRight(10, '9')));
                writeAnd = true;
            }
            return retval;
        }
        private string AppendFieldExactSearch(SearchField field, double? minimum, double? maximum, ref bool writeAnd)
        {
            var retval = string.Empty;
            if (minimum.HasValue || maximum.HasValue)
            {
                if (writeAnd)
                {
                    retval += " AND ";
                }
                retval += (string.Format("{0}:[{1} TO {2}]",
                    field.GetFieldName(),
                    minimum.HasValue ? minimum.Value.ToString("N2").PadLeft(10, '0') : "0.00".PadLeft(10, '0'),
                    maximum.HasValue ? maximum.Value.ToString("N2").PadLeft(10, '0') : "9".PadRight(7, '9') + ".99"));
                writeAnd = true;
            }
            return retval;
        }

        private string[] EscapeAndSplitSearchTerms(string term)
        {
            term = term.Trim();

            // + - && || ! ( ) { } [ ] ^ " ~ * ? :
            string[] escapeChars = { "&", "|", "!", "^", "\"", "~", "*", "?", "+" };
            string[] replaceChars = { "(", ")", "{", "}", "[", "]", ".", "-", ",", "/" };
            //remove replace charaters
            foreach (var ec in replaceChars)
            {
                term = term.Replace(ec, " ");
            }
            //escape operator charaters
            foreach (var ec in escapeChars)
            {
                term = term.Replace(ec, QueryParser.Escape(ec));
            }

            var escapedChars = escapeChars.Select(x => QueryParser.Escape(x)).ToList();

            var sc = StringComparer.OrdinalIgnoreCase;
            //split and return excluding empty and singled out escape characters
            var termSplit = term.Split(new[] { ' ' }).Except(StandardAnalyzer.STOP_WORDS_SET, sc).Where(m => !String.IsNullOrWhiteSpace(m)).ToList();

            //only remove escaped charaters when there are more then 1 search term
            if (termSplit.Count() > 1)
            {
                termSplit = termSplit.Where(m => !escapedChars.Any(x => x == m)).ToList();
            }

            return termSplit.ToArray();
        }

        public void InsertSearchOptions()
        {
            ////entitty multiple values
            //var searchOptions =
            //    context.EntityMultipleValues.Select(x => new { Field = x.Field, Value = x.Value }).Distinct();
            //foreach (var searchOption in searchOptions)
            //{
            //    context.SearchOptions.InsertOnSubmit(new SearchOption { Field = searchOption.Field, Value = searchOption.Value });
            //}


            //org & fund geographic focus
            var geographicFocues = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.GeographicFocus).Distinct().Union(context.Funds.Where(m => m.Organization.PublishSearch || m.Organization.PublishOverviewAndTeam).Select(x => x.GeographicFocus).Distinct());
            foreach (var geographicFocus in geographicFocues.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.GeographicFocus, Value = geographicFocus });
            }

            var strategiesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.StrategyPipeDelimited).Distinct();
            var strategies = String.Join("|", strategiesPiped).Split(new[] { '|' }).Distinct();
            foreach (var strategy in strategies.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.Strategy, Value = strategy });
            }

            var subStrategyPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SubStrategyPipeDelimited).Distinct();
            var subStrategies = String.Join("|", subStrategyPiped).Split(new[] { '|' }).Distinct();
            foreach (var subStrategy in subStrategies.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.SubStrategy, Value = subStrategy });
            }

            var sectorFocusesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SectorFocusPipeDelimited).Distinct();
            var sectorFocuses = String.Join("|", sectorFocusesPiped).Split(new[] { '|' }).Distinct();
            foreach (var focus in sectorFocuses.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.SectorFocus, Value = focus });
            }

            //var countriesPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.CountriesPipeDelimited).Distinct();
            //var countries = String.Join("|", countriesPiped).Split(new[] { '|' }).Distinct();
            //foreach (var country in countries.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            //{
            //    context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.Country, Value = country });
            //}

            var investmentRegionsPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.InvestmentRegionPipeDelimited).Distinct();
            var investmentRegions = String.Join("|", investmentRegionsPiped).Split(new[] { '|' }).Distinct();
            foreach (var investmentRegion in investmentRegions.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.InvestmentRegion, Value = investmentRegion });
            }

            //var subRegionsPiped = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.SubRegionsPipeDelimited).Distinct();
            //var subRegions = String.Join("|", subRegionsPiped).Split(new[] { '|' }).Distinct();
            //foreach (var subRegion in subRegions.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            //{
            //    context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.InvestmentSubRegion, Value = subRegion });
            //}

            var quantitativeGrades = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.QuantitativeGrade).Distinct();
            foreach (var grade in quantitativeGrades.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.QuantitativeGrade, Value = grade });
            }

            var qualitativeGrades = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.QualitativeGrade).Distinct();
            foreach (var grade in qualitativeGrades.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.QualitativeGrade, Value = grade });
            }

            var marketStages = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.MarketStage).Distinct().Union(context.Funds.Where(m => m.Organization.PublishSearch || m.Organization.PublishOverviewAndTeam).Select(x => x.MarketStage).Distinct());
            foreach (var marketStage in marketStages.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.MarketStage, Value = marketStage });
            }

            var fundraisingStatuses = context.Organizations.Where(m => m.PublishSearch || m.PublishOverviewAndTeam).Select(x => x.FundRaisingStatus).Distinct();
            foreach (var status in fundraisingStatuses.ToList().Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                context.SearchOptions.InsertOnSubmit(new SearchOption { Field = (int)FieldType.FundraisingStatus, Value = status });
            }

            context.SearchOptions.InsertAllOnSubmit(
                context.Organizations.Select(m => m.FocusRadar).Distinct().ToList().Where(m => !String.IsNullOrWhiteSpace(m)).Select(
                    m => new SearchOption { Field = (int)FieldType.FocusList, Value = m }));

            context.SearchOptions.InsertAllOnSubmit(context.Currencies.ToList().Select(m => new SearchOption { Field = (int)FieldType.Currency, Value = m.Name }));

            context.SubmitChanges();
        }


        public IEnumerable<LuceneSearchResult> GetAvailableOrganizations(Guid userId)
        {
            var user = UserRepository.GetById(userId);
            AtlasDiligence.Common.Data.Models.Group group = null;
            // get segments and purchased orgs
            if (user != null && user.UserExt != null)
                group = user.UserExt.Group;

            var segments = group != null ? group.GroupSegments.Select(m => m.Segment).ToList() : new List<AtlasDiligence.Common.Data.Models.Segment>();
            var purchasedOrgs = group != null ? group.GroupOrganizations.Select(m => m.OrganizationId) : new List<Guid>();

            // get available orgs
            int total;
            var availableOrgs = GetOrganizations(new SearchFilter { Segments = segments, PurchasedOrganizationIds = purchasedOrgs }, out total).ToList();
            return availableOrgs;
        }
    }
}
