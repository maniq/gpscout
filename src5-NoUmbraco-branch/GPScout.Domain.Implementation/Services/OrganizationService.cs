﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utility;

namespace GPScout.Domain.Implementation.Services
{
    public class OrganizationService : IOrganizationService
    {
        private IRepository<Organization> _organizationRepository;
        private IRepository<Organization> OrganizationRepository
        {
            get
            {
                if (_organizationRepository == null)
                    _organizationRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<Organization>>();
                return _organizationRepository;
            }
        }


        private IRepository<OrganizationViewing> _organizationViewingRepository;
        private IRepository<OrganizationViewing> OrganizationViewingRepository
        {
            get
            {
                if (_organizationViewingRepository == null)
                    _organizationViewingRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<OrganizationViewing>>();
                return _organizationViewingRepository;
            }
        }


        private IClientRequestRepository _clientRequestRepository;
        private IClientRequestRepository ClientRequestRepository
        {
            get
            {
                if (_clientRequestRepository == null)
                    _clientRequestRepository = ComponentBrokerInstance.RetrieveComponent<IClientRequestRepository>();
                return _clientRequestRepository;
            }
        }


        private IRepository<Benchmark> _benchmarkRepository;
        private IRepository<Benchmark> BenchmarkRepository
        {
            get
            {
                if (_benchmarkRepository == null)
                    _benchmarkRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<Benchmark>>();
                return _benchmarkRepository;
            }
        }

        #region Organization Full Report PDF Related
        
        public string GetFullReportFileName(string instanceId)
        {
            return string.Format("org_printall_{0}", instanceId);
        }


        public string GetFullReportFileNameFullPath(string instanceId)
        {
            string fileName = GetFullReportFileName(instanceId);
            return Path.Combine(AppSettings.TempPath, fileName + ".pdf");
        }


        public string GetFullReportHeaderFileName(string instanceId)
        {
            return string.Format("org_printall_{0}_h.html", instanceId);
        }


        public string GetFullReportDownloadFileName(string organizationName)
        {
            return string.Format("GPScout - {0} ({1}).pdf", Utils.SafeFileName(organizationName), DateTime.Now.ToString("M.d.yyyy"));
        }

        public void RemoveObsoleteDataItems()
        {
#if DEBUG
            DateTime limitDate = DateTime.Now.AddDays(-AppSettings.CleanupIntervalDays);
#else
            DateTime limitDate = DateTime.Now.AddDays(-AppSettings.CleanupIntervalDays);
#endif
            var userNotificationRepository = ComponentBrokerInstance.RetrieveComponent<IUserNotificationRepository>();
            userNotificationRepository.DeleteAllOnSubmit(
                userNotificationRepository.FindAll(x => x.DateCreated < limitDate)
            );
            userNotificationRepository.SubmitChanges();

            var task = new Task<int>(() =>
            {
                var taskRepository = ComponentBrokerInstance.RetrieveComponent<ITaskRepository>();
                taskRepository.DeleteAllOnSubmit(
                    taskRepository.FindAll(x => x.DateCreated < limitDate)
                );
                taskRepository.SubmitChanges();

                // delete old organization report files
                foreach (string fileName in Directory.GetFiles(AppSettings.TempPath, "org_printall_*.*"))
                {
                    DateTime creationTime = File.GetCreationTime(fileName);
                    if (creationTime < limitDate)
                    {
                        Utils.DeleteFile(fileName);
                    }
                }
                return 0;
            });
            task.Start();
        }

        public string GetFullReportFooterFileName(string instanceId)
        {
            return string.Format("org_printall_{0}_f.html", instanceId);
        }

        public void FullReportCleanup(string contextId)
        {
            if (!string.IsNullOrEmpty(contextId))
            {
                string pattern = string.Format("org_printall_{0}*.*", contextId);
                foreach (string fileName in Directory.GetFiles(AppSettings.TempPath, pattern))
                {
                    Utils.DeleteFile(fileName);
                }
            }
        }
        #endregion


        public Organization GetById(Guid id)
        {
            return this.OrganizationRepository.GetById(id);
        }


        public void AddOrganizationViewing(Guid userId, Guid organizationId)
        {
            OrganizationViewingRepository.InsertOnSubmit(new OrganizationViewing
            {
                Id = Guid.NewGuid(),
                OrganizationId = organizationId,
                Timestamp = DateTime.Now,
                UserId = userId
            });
            OrganizationViewingRepository.SubmitChanges();
        }


        public IEnumerable<ClientRequest> GetAllClientRequests()
        {
            return ClientRequestRepository.GetAll().ToList();
        }


        public IEnumerable<Organization> GetAllPositions()
        {
            return this.OrganizationRepository.GetAll().Where(w =>
                                                                (w.PublishOverviewAndTeam || w.PublishSearch)
                                                                && w.Latitude != null
                                                                && w.Longitude != null).ToList()
                                                        .Select(s => new Organization()
                                                        {
                                                            Id = s.Id,
                                                            Name = s.Name,
                                                            FocusRadar = s.FocusRadar,
                                                            FundRaisingStatus = s.FocusRadar,
                                                            Latitude = s.Latitude,
                                                            Longitude = s.Longitude,
                                                            PublishOverviewAndTeam = s.PublishOverviewAndTeam,
                                                            PublishSearch = s.PublishSearch
                                                        });
        }

        public IEnumerable<Organization> GetAllOrganizations()
        {
            return this.OrganizationRepository.GetAll().ToList();
        }

        public IEnumerable<int> GetAvailbleFundVintageYears(IEnumerable<Guid> orgIds)
        {
            var vintageYearList = new List<int>();
            var orgIdsList = orgIds.ToList();

            int page = 0, pageSize = 2000, count = 0;
            do
            {
                var orgs = GetAllByIds(orgIdsList.Skip(page * pageSize).Take(pageSize)).ToList();
                count = orgs.Count();

                orgs.Where(w => orgIds.Contains(w.Id))
                    //.SelectMany(sm => sm.Funds.Where(w => w.VintageYear != null))
                        //.SelectMany(sm => sm.Funds.Where(w => !string.IsNullOrEmpty(w.VintageYearStr)))
                        //.Select(s => Convert.ToInt32(s.VintageYearStr)).ToList()
                        .SelectMany(sm => sm.Funds.Where(w => w.VintageYear.HasValue))
                        .Select(s => s.VintageYear.Value).ToList()
                        .ForEach(vintageYearList.Add);

                page++;
            } while (count == pageSize);

            return vintageYearList;
        }


        public IEnumerable<Organization> GetAllByIds(IEnumerable<Guid> ids)
        {
            return OrganizationRepository.FindAll(m => ids.Contains(m.Id));
        }


        public IEnumerable<Benchmark> GetBenchmarks()
        {

            //var latest = (from b in benchmarkRepository.GetAll()
            //               group b by b.VintageYear into g
            //               select new { VintageYear = g.Key, AsOf = g.Max(x=>x.AsOf )});

            //var results = (from b in benchmarkRepository.GetAll()
            //               join l in latest on new {b.VintageYear, b.AsOf} equals new {l.VintageYear, l.AsOf}
            //               select b);

            //return results;

            var benchmarks = BenchmarkRepository.GetAll();
            var result = new List<Benchmark>();
            var vintageYears = benchmarks.Select(s => s.VintageYear).Distinct();
            foreach (var vintageYear in vintageYears)
            {
                var latest = benchmarks.Where(w => w.VintageYear == vintageYear).OrderByDescending(o => o.AsOf).Take(1).FirstOrDefault();
                if (latest != null)
                {
                    result.Add(latest);
                }
            }

            return result;
        }


        public IEnumerable<Organization> GetComparetablesOrganizationFunds(IEnumerable<Guid> orgIds)
        {
            var orgList = new List<Organization>();
            var orgIdsList = orgIds.ToList();

            int page = 0, pageSize = 2000, count = 0;
            do
            {
                var orgs = GetAllByIds(orgIdsList.Skip(page * pageSize).Take(pageSize)).ToList();
                orgs.Where(w => orgIdsList.Contains(w.Id)).Select(s => new
                {
                    Id = s.Id,
                    Name = s.Name,
                    Funds = s.Funds
                }).ToList().ForEach(fe => orgList.Add(new Organization { Id = fe.Id, Name = fe.Name, Funds = fe.Funds }));

                page++;
            } while (count == pageSize);

            return orgList;
        }


        public IQueryable<Organization> GetOrganizationsByName(string organizationName)
        {
            return this.OrganizationRepository.FindAll(m => m.Name == organizationName && (m.PublishOverviewAndTeam || m.PublishSearch));
        }


        public Dictionary<Guid, string> GetComparTableOrgsByIds(IEnumerable<Guid> ids)
        {
            int page = 0, pageSize = 500, count = 0;
            var returnDictionary = new Dictionary<Guid, string>();

            // get organizations in batches to avoid 2100 parameter limit 
            do
            {
                var idBatch = ids.Skip(page * pageSize).Take(pageSize).ToList();
                count = idBatch.Count();

                // search orgs with at least one fund and As Of is not null
                OrganizationRepository.FindAll(f => idBatch.Contains(f.Id))
                    .Where(w => w.Funds.Count > 1 && w.Funds.Any(a => a.AsOf.HasValue || a.PreqinAsOf.HasValue))
                    .ToList().ForEach(fe => returnDictionary.Add(fe.Id, fe.Name));

                page++;
            } while (count == pageSize);

            return returnDictionary.OrderBy(o => o.Value).ToDictionary(k => k.Key, v => v.Value);
        }


        #region For News service
        public List<KeyValuePair<Guid, List<string>>> GetAllOrgIdAndAliases()
        {
            var orgs = OrganizationRepository.GetAll().ToArray();
            var retval = from a in orgs.Where(o=>o.Active)
                         select new
                         {
                             a.Id,
                             a.Name,
                             Alias = a.AliasesPipeDelimited
                         };

            return
                retval.ToList()
                      .Select(
                          m =>
                          new KeyValuePair<Guid, List<string>>(m.Id,
                                                               m.Alias == null
                                                                   ? new List<string> { m.Name }
                                                                   : m.Alias.Split(new[] { '|' })
                                                                      .Union(new[] { m.Name })
                                                                      .ToList())).ToList();
        }


        public List<KeyValuePair<Guid, List<string>>> GetAllContactIdAndAliases()
        {
            var context = new AtlasWebDbDataContext();
            var retval = (from a in context.OrganizationMembers
                          select new
                          {
                              a.Id,
                              a.FirstName,
                              a.LastName,
                              Alias = (string)null //a.new_FirstNameAliases
                          }).ToList();

            return
                retval.Select(
                          m =>
                          new KeyValuePair<Guid, List<string>>(m.Id,
                                                               m.Alias == null
                                                                   ? new List<string> { String.Format("{0} {1}", m.FirstName, m.LastName) }
                                                                   : m.Alias.Split(',').Select(s => s.Trim() + " " + m.LastName)
                                                                      .Union(new[] { String.Format("{0} {1}", m.FirstName, m.LastName) })
                                                                      .ToList())).ToList();
        }


        public IQueryable<Organization> SearchByNameWildcard(string term)
        {
            return OrganizationRepository.FindAll(o => (o.Name == null && term == null ) || o.Name.Contains(term));
        }

        #endregion
    }
}
