﻿using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPScout.Domain.Contracts.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using SFP.Infrastructure.Logging.Contracts;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.Models;

namespace GPScout.Domain.Implementation.Services
{
    internal class RequestService : IRequestService
    {
        #region Properties
        private IRequestRepository RequestRepository {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRequestRepository>();
            }
        }


        private IEmailDistributionListService EmailDistributionListService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEmailDistributionListService>();
            }
        }

        private IOrganizationService OrganizationService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IOrganizationService>();
            }
        }

        private IUserService UserService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        private ILog Log
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ILog>();
            }
        }
        #endregion

        #region IRequestService
        public IResults AddRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail)
        {
            RequestRepository.InsertRequest(requestType, organizationId, userId, comment);
            var organization = OrganizationService.GetById(organizationId);
            return SendRequestNotification(requestType, organization.Name, userId, comment, null, fromEmail);
        }


        public IResults AddRequest(RequestType requestType, string organizationName, Guid userId, string comment, string fromEmail)
        {
            RequestRepository.InsertRequest(requestType, organizationName, userId, comment);
            return SendRequestNotification(requestType, organizationName, userId, comment, null, fromEmail);
        }


        public IResults AddRequest(RequestType requestType, string organizationName, Guid? userId, string comment, string userEmail, string fromEmail)
        {
            RequestRepository.InsertRequest(requestType, organizationName, userId, comment, userEmail);
            return SendRequestNotification(requestType, organizationName, userId, comment, userEmail, fromEmail);
        }


        public IResults RemoveRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail)
        {
            var requestsToRemove = RequestRepository.FindAll(r => r.Type == (int)requestType && r.UserId == userId && r.OrganizationId == organizationId);
            if (string.IsNullOrEmpty(comment))
            {
                int cnt = requestsToRemove.Count();
                comment = string.Format("SYSTEM: {0} request record(s) deleted from the database.", cnt);
            }
            RequestRepository.DeleteAllOnSubmit(requestsToRemove);
            var organization = OrganizationService.GetById(organizationId);
            return SendRequestNotification("GPScout Request Cancelled By User", "Request cancelled by user:", requestType, organization.Name, userId, comment, null, fromEmail);
        }
        #endregion

        #region Helper Methods
        private IResults SendRequestNotification(RequestType requestType, string organizationName, Guid? userId, string comment, string userMail, string fromEmail)
        {
            return SendRequestNotification("New GPScout Request Submitted", "New GPScout request has been submitted:", requestType, organizationName, userId, comment, userMail, fromEmail);
        }


        private IResults SendRequestNotification(string title, string firstLine, RequestType requestType, string organizationName, Guid? userId, string comment, string userMail, string fromEmail)
        {
            var results = new Results();

            aspnet_User user = userId.HasValue ? UserService.GetUserById(userId.Value) : null;
            
            string groupName = string.Empty;
            string userName = string.Empty;
            string emailLine = string.Empty;

            if (user != null) {
                userName = user.UserName;

                if (user.UserExt != null && user.UserExt.Group != null)
                groupName = user.UserExt.Group.Name;
            } else
                emailLine = string.Format("Email: <strong>{0}</strong><br/>", userMail);

            /*
            string body = string.Format(@"
                {6}<br/><br/>
                Request Type: <strong>{0} Request</strong><br/>
                Username: <strong>{1}</strong><br/>
                Group:  <strong>{2}</strong><br/>
                Organization Name: <strong>{3}</strong><br/>
                Date of request: <strong>{4}</strong><br/>
                {6}
                {7}
                Comments:<br/><strong>{5}</strong>
                ", requestType.GetDescription(), userName, groupName, organizationName, DateTime.Now.ToShortDateString(), comment, firstLine, emailLine);
            */

            string body = string.Format(@"
                {6}<br/><br/>
                Request Type: <strong>{0} Request</strong><br/>
                Username: <strong>{1}</strong>{7}<br/>
                Group:  <strong>{2}</strong><br/>
                Organization Name: <strong>{3}</strong><br/>
                Date of request: <strong>{4}</strong><br/>
                {6}                
                Comments:<br/><strong>{5}</strong>
                ", requestType.GetDescription(), userName, groupName, organizationName, DateTime.Now.ToShortDateString(), comment, firstLine, userMail);

            var sendingResults = EmailDistributionListService.SendToEmailDistributionList(EmailDistribution.RequestNotification, title, body, fromEmail);

            sendingResults.LogTo(Log);
            return results;
        }
        #endregion
    }
}
