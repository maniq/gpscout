﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using GPScout.Domain.Contracts.Models.Tasks;
using GPScout.Domain.Contracts.Services;
using System;
using System.Linq;

namespace GPScout.Domain.Implementation.Services
{
    public class TaskService : ITaskService
    {
        ITaskRepository TaskRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ITaskRepository>();
            }
        }


        public bool Save(ITask task)
        {
            return this.TaskRepository.Save(ToModel(task));
        }

        public bool Delete(ITask task)
        {
            if (task != null)
            {
                var _task = this.TaskRepository.GetById(task.Id);
                if (_task != null)
                {
                    this.TaskRepository.DeleteOnSubmit(_task);
                    this.TaskRepository.SubmitChanges();
                }
            }
            return true;
        }


        #region Helper Methods
        private ITask TaskFactory(TaskEntity task)
        {
            ITask _task = null;
            if (task != null)
            {
               switch (task.TaskType) {
                   case TaskType.OrganizationFullReportTask:
                        _task = new OrganizationFullReportTask(task.Data);
                        break;
                    case TaskType.DataImportTask:
                        _task = new DataImportTask(task.Data);
                        break;
                }
               _task.Id = task.Id;
               _task.UserId = task.UserId.GetValueOrDefault();
               _task.Data = task.Data;
               _task.Name = task.Name;
               _task.ReferenceId = task.ReferenceId;
               _task.Status = task.StatusId;
               _task.DateExpired = task.DateExpired;
            }
            return _task;
        }

        private TaskEntity ToModel(ITask task)
        {
            TaskEntity _task = null;
            if (task != null)
            {
                _task = new TaskEntity()
                {
                    DateCreated = task.DateCreated,
                    Name = task.Name,
                    ReferenceId = task.ReferenceId,
                    StatusId = task.Status,
                    Data = task.SerializeData(),
                    Id = task.Id,
                    UserId = task.UserId,
                    DateExpired = task.DateExpired
                };
                if (task is OrganizationFullReportTask)
                    _task.TaskType = TaskType.OrganizationFullReportTask;
                else if (task is DataImportTask)
                    _task.TaskType = TaskType.DataImportTask;
            }
            return _task;
        }
        #endregion


        public ITask GetById(Guid id)
        {
            var task = this.TaskRepository.GetById(id);
            return this.TaskFactory(task);
        }


        public int GetCountOfRunningTasks(Guid? userId)
        {
            return this.TaskRepository.FindAll(t => 
                (t.StatusId == (int)DownloadableItemLifeCycle.Running || t.StatusId == (int)DownloadableItemLifeCycle.Started)
                && t.DateExpired >= DateTime.Now && (!userId.HasValue || t.UserId == userId)
            ).Count();
        }
    }
}
