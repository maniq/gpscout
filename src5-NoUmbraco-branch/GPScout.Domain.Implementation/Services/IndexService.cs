﻿using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace GPScout.Domain.Implementation.Services
{
    public class IndexService : IIndexService
    {
        private static readonly object Lock = new object();
        private const int GET_DATA_MAX_ATTEMPTS = 9;

        private readonly ILog _log;
        private Repository<Organization> OrganizationRepository { get; set; }


        public IndexService(ILog log)
        {
            _log = log;
        }

        public void ReIndex()
        {
            OrganizationRepository = new Repository<Organization>();
            //var allOrganizations = OrganizationRepository.FindAll(m => m.PublishOverviewAndTeam || m.PublishSearch);
            var allOrganizations = GetOrgDataForLuceneReindexing(OrganizationRepository, GET_DATA_MAX_ATTEMPTS);

            lock (Lock)
            {
                using (var indexWriter = this.GetIndexWriter(true))
                {
                    foreach (var organization in allOrganizations)
                    {
                        UpdateLuceneIndex(organization, indexWriter);
                    }
                    indexWriter.Optimize();
                }
            }
        }


        private Organization[] GetOrgDataForLuceneReindexing(Repository<Organization> organizationRepository, int attempts, int currentAttempt = 1)
        {
            try
            {
                _log.Write(string.Format("Getting Organization data for lucene re-indexing, attempt {0}:", currentAttempt), "Info");
                return organizationRepository.FindAll(m => m.PublishOverviewAndTeam || m.PublishSearch).ToArray();
            } catch (Exception ex)
            {
                _log.Write(string.Format("Getting Organization data for lucene re-indexing, attempt {0} failed. Error: {1}", currentAttempt, ex.ToString()), "Error");
                if (currentAttempt < attempts)
                {
                    Thread.Sleep(new Random().Next(3000, 9000));
                    return GetOrgDataForLuceneReindexing(organizationRepository, attempts, ++currentAttempt);
                }
                else
                    throw;
            }
        }

        private void UpdateLuceneIndex(Organization organization, IndexWriter indexWriter)
        {
            _log.Write(organization.Id.ToString(), "Info - UpdateLuceneIndex");
            AddOrganizationToIndex(organization, indexWriter);

            AddFundsToIndex(organization, indexWriter);

            AddMembersToIndex(organization, indexWriter);
        }

        private void AddOrganizationToIndex(Organization organization, IndexWriter indexWriter)
        {
            var orgDocument = CreateDefaultDocumentFields(organization);

            // set is organization
            orgDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), true.ToString(), Field.Store.NO, Field.Index.ANALYZED));

            // universial search string
            var organizationUniversalSearchString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                    organization.Name,
                    organization.PrimaryOffice,
                    organization.InvestmentRegionPipeDelimited == null ? string.Empty : organization.InvestmentRegionPipeDelimited.Replace("|", " | "),
                    organization.CountriesPipeDelimited == null ? string.Empty : organization.CountriesPipeDelimited.Replace("|", " | "),
                    organization.MarketStage,
                    organization.StrategyPipeDelimited == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | "),
                    organization.AliasesPipeDelimited == null ? string.Empty : organization.AliasesPipeDelimited.Replace("|", " | "),
                    organization.OtherAddresses.Any() ? String.Join(" ", organization.OtherAddresses.Select(m => m.FullAddress)) : string.Empty,
                    organization.SectorFocusPipeDelimited == null ? string.Empty : organization.SectorFocusPipeDelimited.Replace("|", " | ")
                );
            orgDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), organizationUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));

            // fund size value used in advanced search
            orgDocument.Add(
                new Field(
                    SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
                    organization.AdvancedSearchFundSize.HasValue ? organization.AdvancedSearchFundSize.ToString().PadLeft(10, '0') : "NULL",
                    Field.Store.YES,
                    Field.Index.ANALYZED));

            // update document
            var term = new Term("id", organization.Id.ToString());
            indexWriter.UpdateDocument(term, orgDocument);
        }


        private void AddFundsToIndex(Organization organization, IndexWriter indexWriter)
        {
            var funds = GetData(organization.Funds, GET_DATA_MAX_ATTEMPTS);
            foreach (var fund in funds)
            {
                var fundDocument = CreateDefaultDocumentFields(organization);
                var fundUniversalSearchString = fund.Name ?? String.Empty;

                fundDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), fundUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundName.GetFieldName(), fund.Name ?? string.Empty, Field.Store.YES, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundGeographicFocus.GetFieldName(), fund.GeographicFocus ?? string.Empty, Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundSectorFocus.GetFieldName(), fund.SectorFocusPipeDelimited == null ? string.Empty : fund.SectorFocusPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundCurrency.GetFieldName(), fund.Currency == null ? "NULL" : fund.Currency.Name, Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), false.ToString(), Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundStatus.GetFieldName(), fund.Status ?? string.Empty, Field.Store.NO, Field.Index.ANALYZED));
                fundDocument.Add(new Field(SearchField.FundTargetSize.GetFieldName(), fund.TargetSize.HasValue ? fund.TargetSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.NO, Field.Index.ANALYZED));

                // fund size value used in advanced search
                //if (organization.FundRaisingStatus == "Pre-Marketing" || organization.FundRaisingStatus == "Open")
                //{
                //    if (fund.Status == "Pre-Marketing" || fund.Status == "Open")
                //    {
                //        fundDocument.Add(new Field(SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
                //                                   fund.TargetSize.HasValue ? fund.TargetSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.YES,
                //                                   Field.Index.ANALYZED));
                //    }
                //    else
                //    {
                //        var mostRecentFund = organization.Funds.OrderByDescending(o => o.VintageYear);
                //        if (mostRecentFund.Any())
                //        {
                //            var fundSize = mostRecentFund.FirstOrDefault().FundSize;
                //            fundDocument.Add(new Field(SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
                //                                   fundSize.HasValue ? fundSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.YES,
                //                                   Field.Index.ANALYZED));
                //        }
                //    }
                //}

                var term = new Term("id", fund.Id.ToString());
                indexWriter.UpdateDocument(term, fundDocument);
            }
        }

        private void AddMembersToIndex(Organization organization, IndexWriter indexWriter)
        {
            var organizationMembers = GetData(organization.OrganizationMembers, GET_DATA_MAX_ATTEMPTS);
            foreach (var member in organizationMembers)
            {
                var orgMemberDocument = CreateDefaultDocumentFields(organization);
                var memberUniversalSearchString = member.FullName;

                orgMemberDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), memberUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));
                orgMemberDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), false.ToString(), Field.Store.NO, Field.Index.ANALYZED));

                var term = new Term("id", member.Id.ToString());
                indexWriter.UpdateDocument(term, orgMemberDocument);
            }
        }

        private Document CreateDefaultDocumentFields(Organization organization)
        {
            var document = new Document();

            // not analyzed columns for sorting
            document.Add(new Field(SearchField.OrganizationName.GetFieldName() + "Sort", organization.Name.ToLower(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationPrimaryOffice.GetFieldName() + "Sort", organization.PrimaryOffice != null ? organization.PrimaryOffice.ToLower() : String.Empty, Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationGeographicFocus.GetFieldName() + "Sort", organization.GeographicFocus != null ? organization.GeographicFocus.ToLower() : string.Empty, Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationStrategies.GetFieldName() + "Sort", organization.Strategy == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | ").ToLower(), Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationMarketStage.GetFieldName() + "Sort", organization.MarketStage != null ? organization.MarketStage.ToLower() : string.Empty, Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationFundraisingStatus.GetFieldName() + "Sort", organization.FundRaisingStatus != null ? organization.FundRaisingStatus.ToLower() : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationYearFounded.GetFieldName() + "Sort", organization.YearFounded.HasValue ? organization.YearFounded.Value.ToString() : "0000", Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationLastUpdated.GetFieldName() + "Sort", organization.LastUpdated.HasValue ? DateTools.DateToString(organization.LastUpdated.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.NO, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationInactiveFirm.GetFieldName() + "Sort", organization.InactiveFirm ? "1" : "0", Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationCustomSortColumn.GetFieldName() + "Sort", organization.CustomSortColumn.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationSectorFocus.GetFieldName() + "Sort", organization.SectorFocusUI, Field.Store.YES, Field.Index.NOT_ANALYZED));

            // add rest of the org fields
            document.Add(new Field(SearchField.OrganizationId.GetFieldName(), organization.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationPublishOverviewAndTeam.GetFieldName(), organization.PublishOverviewAndTeam.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationPublishProfile.GetFieldName(), organization.PublishProfile.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationPublishSearch.GetFieldName(), organization.PublishSearch.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationName.GetFieldName(), organization.Name, Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationAliases.GetFieldName(), organization.AliasesPipeDelimited == null ? string.Empty : organization.AliasesPipeDelimited.Replace("|", " | "), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationPrimaryOffice.GetFieldName(), organization.PrimaryOffice != null ? organization.PrimaryOffice : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationInvestmentRegion.GetFieldName(), organization.InvestmentRegionPipeDelimited == null ? string.Empty : organization.InvestmentRegionPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationMarketStage.GetFieldName(), organization.MarketStage != null ? organization.MarketStage : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            //document.Add(new Field(SearchField.OrganizationCountries.GetFieldName(), organization.CountriesPipeDelimited == null ? string.Empty : organization.CountriesPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationStrategies.GetFieldName(), organization.StrategyPipeDelimited == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationGeographicFocus.GetFieldName(), organization.GeographicFocus != null ? organization.GeographicFocus : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            //document.Add(new Field(SearchField.OrganizationInvestmentSubRegion.GetFieldName(), organization.SubRegionsPipeDelimited == null ? string.Empty : organization.SubRegionsPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationSubStrategy.GetFieldName(), organization.SubStrategyPipeDelimited == null ? string.Empty : organization.SubStrategyPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationQualitativeGrades.GetFieldName(), organization.QualitativeGrade != null ? organization.QualitativeGrade : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationQuantitativeGrades.GetFieldName(), organization.QuantitativeGrade != null ? organization.QuantitativeGrade : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationSectorFocus.GetFieldName(), organization.SectorFocusPipeDelimited == null ? string.Empty : organization.SectorFocusPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationFundraisingStatus.GetFieldName(), organization.FundRaisingStatus != null ? organization.FundRaisingStatus : string.Empty, Field.Store.YES, Field.Index.ANALYZED));

            document.Add(new Field(SearchField.OrganizationYearFounded.GetFieldName(), organization.YearFounded.HasValue ? organization.YearFounded.Value.ToString() : "0000", Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationLastUpdated.GetFieldName(), organization.LastUpdated.HasValue ? DateTools.DateToString(organization.LastUpdated.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationExpectedNextFundraise.GetFieldName(), organization.ExpectedNextFundRaise.HasValue ? DateTools.DateToString(organization.ExpectedNextFundRaise.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationNumberOfFunds.GetFieldName(), organization.NumberOfFunds.HasValue ? organization.NumberOfFunds.ToString().PadLeft(10, '0') : "0".PadLeft(10, '0'), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationEmergingManager.GetFieldName(), organization.EmergingManager.ToString(), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationFocusList.GetFieldName(), organization.FocusRadar != null ? organization.FocusRadar : string.Empty, Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationSectorSpecialist.GetFieldName(), organization.SectorSpecialist.ToString(), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationRegionalSpecialist.GetFieldName(), organization.RegionalSpecialist.ToString(), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationAccessConstrained.GetFieldName(), organization.AccessConstrained.ToString(), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationDiligenceLevel.GetFieldName(), organization.DiligenceLevel.HasValue ? organization.DiligenceLevel.Value.ToString().PadLeft(10, '0') : "0".PadLeft(10, '0'), Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationQuality.GetFieldName(), organization.QualitativeGradeNumber.HasValue ? organization.QualitativeGradeNumber.Value.ToString("N2").PadLeft(10) : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationQuantity.GetFieldName(), organization.QuantitativeGradeNumber.HasValue ? organization.QuantitativeGradeNumber.Value.ToString("N2").PadLeft(10) : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationSBICFund.GetFieldName(), organization.SBICFund.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationInactiveFirm.GetFieldName(), organization.InactiveFirm.ToString(), Field.Store.YES, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.FocusRadar.GetFieldName(), organization.FocusRadar != null ? organization.FocusRadar : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            document.Add(new Field(SearchField.OrganizationCurrency.GetFieldName(), organization.Currency == null ? String.Empty : organization.Currency.Name, Field.Store.NO, Field.Index.ANALYZED));
            document.Add(new Field(SearchField.OrganizationLastFundSize.GetFieldName(), organization.LastFundSize.HasValue ? organization.LastFundSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.NO, Field.Index.ANALYZED));

            // lat and long postions
            var positions = organization.OtherAddresses.Where(m => m.Latitude.HasValue && m.Longitude.HasValue).Select(m => new LatitudeAndLongitude { Latitude = m.Latitude, Longitude = m.Longitude, City = m.City }).ToList();
            if (organization.Latitude.HasValue && organization.Longitude.HasValue)
            {
                positions.Add(new LatitudeAndLongitude { Latitude = organization.Latitude, Longitude = organization.Longitude, City = organization.Address1City });
            }
            document.Add(new Field(SearchField.OrganizationPosition.GetFieldName(), string.Join("|", positions.Select(m => string.Format("{0},{1},{2}", m.Latitude, m.Longitude, m.City))), Field.Store.YES, Field.Index.NOT_ANALYZED));

            return document;
        }

        private IndexWriter GetIndexWriter(bool create)
        {
            var directory = FSDirectory.Open(AppSettings.IndexPath);
            return new IndexWriter(directory, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30), create, IndexWriter.MaxFieldLength.UNLIMITED);
        }

        private TResult[] GetData<TResult>(IEnumerable<TResult> data, int attempts, int currentAttempt = 1)
        {
            try {
                return data.ToArray();
            } catch (Exception ex)
            {
                _log.Write(string.Format("Getting data for {2}, attempt {0} failed. Error: {1}", currentAttempt, ex.ToString(), typeof(TResult).Name), "Error");
                if (currentAttempt < attempts)
                {
                    Thread.Sleep(new Random().Next(3000, 9000));
                    return GetData(data, attempts, ++currentAttempt);
                }
                else
                    throw;
            }
        }
    }
}
