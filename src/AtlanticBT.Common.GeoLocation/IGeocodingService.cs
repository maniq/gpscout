﻿using System;

namespace AtlanticBT.Common.GeoLocation
{
    /// <summary>
    /// IGeocodingService
    /// </summary>
    public interface IGeocodingService
    {
        /// <summary>
        /// Gets full address information from Geocoding API
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        GeocodeComponents GetAddress(string address);

        /// <summary>
        /// Returns a tuple of lantitude then longitude
        /// </summary>
        /// <returns></returns>
        Tuple<decimal, decimal> GetGeoCode(string address, string address2, string city, string state, string zip);
    }
}