﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Data.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Web;
using AtlasDiligence.RssService.Properties;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.RssService
{
    public partial class RssService : ServiceBase
    {
        #region service stuff
        private Timer _timer;

        public RssService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            LogEvents.WriteLog("Started service.", EventLogEntryType.Information);
            _timer = new Timer(3600000);

            _timer.Elapsed += new ElapsedEventHandler(TimerElapsed);

            _timer.Enabled = true;

            TimerElapsed(null, null);
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();

            DoWork();

            _timer.Start();
        }

        protected override void OnStop()
        {
            _timer.Stop();
            _timer.Dispose();
            _timer = null;
        }
        #endregion

        public void DoWork()
        {
            var now = DateTime.Now;
            var userRepository = new UserRepository();
            var newsRepository = new NewsRepository();
            var searchRepository = new SearchTermRepository();

            var users = userRepository.FindAll(m => m.UserExt != null
                && m.UserExt.RssNextEmailDate.HasValue
                && m.UserExt.RssNextEmailDate.Value <= now).ToList();

            var allNewsItems = newsRepository.FindAll(m => m.PublishedDate >= now.AddMonths(-1)).ToList();

            foreach (var user in users)
            {
                //get search terms
                var searchTerms = searchRepository.GetSearchTermsForUser(user);
                var startDate = new DateTimeOffset();
                var emailOffSet = String.Empty;
                var date = user.UserExt.RssNextEmailDate.Value;
                //get start date for retrieving news items and update user next email date
                switch (user.UserExt.RssNextEmailOffsetEnum)
                {
                    //day
                    case 1:
                        startDate = user.UserExt.RssNextEmailDate.Value.AddDays(-1);
                        while(date < DateTime.Now)
                        {
                            date = date.AddDays(1);
                        }
                        userRepository.UpdateUserEmailDate(user.UserId, date);
                        emailOffSet = EmailFrequency.Day.ToString().ToLower();
                        break;
                    //week
                    case 2:
                        startDate = user.UserExt.RssNextEmailDate.Value.AddDays(-7);
                        date = user.UserExt.RssNextEmailDate.Value;
                        while(date < DateTime.Now)
                        {
                            date = date.AddDays(7);
                        }
                        userRepository.UpdateUserEmailDate(user.UserId, date);
                        emailOffSet = EmailFrequency.Week.ToString().ToLower();
                        break;
                    //month
                    case 3:
                        startDate = user.UserExt.RssNextEmailDate.Value.AddMonths(-1);
                        date = user.UserExt.RssNextEmailDate.Value;
                        while(date < DateTime.Now)
                        {
                            date = date.AddMonths(1);
                        }
                        userRepository.UpdateUserEmailDate(user.UserId, date);
                        emailOffSet = EmailFrequency.Month.ToString().ToLower();
                        break;
                    default:
                        LogEvents.WriteLog("Unknown email offset type used. UserId = " + user.UserId,
                                           EventLogEntryType.Error);
                        break;
                }

                //get items
                var items = allNewsItems.Where(m => m.PublishedDate >= startDate);
                var filteredNewsItems =
                    items.Where(item => searchTerms.Any(s => item.Description.Contains(s) || item.Subject.Contains(s))).
                        ToList();

                //put items in NewsViewModel that includes search terms.
                foreach (var filteredNewsItem in filteredNewsItems.OrderBy(o => o.PublishedDate))
                {
                    News item = filteredNewsItem;
                    filteredNewsItem.SearchTerms = searchTerms.Where(
                        s => item.Description.Contains(s) || item.Subject.Contains(s));
                }

                if (!filteredNewsItems.Any()) continue;
                //get email body
                var emailBody = GetEmailBody(user.UserExt.FirstName, user.UserExt.LastName, emailOffSet,
                                             filteredNewsItems);

                //send email
                var to = user.aspnet_Membership.Email;
                var cc = user.UserRssEmails.Any()
                                           ? user.UserRssEmails.Select(x => x.RssEmailAddress).Aggregate(
                                               (seed, next) => seed + ", " + next)
                                           : string.Empty;
                var from = Settings.Default.MailFrom;
                if (String.IsNullOrEmpty(from))
                {
                    from = "no-reply@atlasdiligence.com";
                }
                var subject = Settings.Default.MailSubject;
                if (String.IsNullOrEmpty(subject))
                {
                    subject = "Atlas Diligence News";
                }
                SendMail(to, cc, from, subject, emailBody);
            }
        }
        private static void SendMail(string to, string cc, string from, string subject, string body)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(from),
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };
            mail.BodyEncoding = new System.Text.UTF8Encoding();
            mail.To.Add(to);
            //mail.To.Add("doug.brandl+atlas@atlanticbt.com");
            foreach (var str in cc.Split(','))
            {
                if (!String.IsNullOrEmpty(str))
                    mail.To.Add(str);
            }
            try
            {
                new SmtpClient().Send(mail);
            } 
            catch(Exception e)
            {
                LogEvents.WriteLog("Error sending email to SMTP server. To: " + to, EventLogEntryType.Error);
            }
        }

        private static string GetEmailBody(string firstName, string lastName, string timePeriod, IEnumerable<News> newsItems)
        {
            var emailTemplate = Resources.Templates.RssEmail;
            emailTemplate = emailTemplate.Replace("|TIMEPERIOD|", timePeriod);
            var builder = new StringBuilder();
            foreach (var item in newsItems.OrderByDescending(m => m.PublishedDate))
            {
                builder.Append(
                    @"<tr>
                        <td class=""article-title"" height=""45"" valign=""top"" style=""padding: 0 20px; font-family: Georgia; font-size: 20px; font-weight: bold;"" width=""600"" colspan=""2"">
                            <br /><a href='" + item.Url + @"' >" + item.Subject + @"</a><br />
                        </td>
                    </tr>");
                builder.Append(@"<tr>
                                <td class=""article-source"" height=""20"" valign=""top"" style=""padding: 0 20px; font-family: Georgia; font-size: 12px; font-weight: bold;"" width=""600"" colspan=""2"">
                                    Source: " + item.OrganizationSourceName + @"<br />
                                </td>
                            </tr>");
                builder.Append(
@"<tr>
                        <td class=""article-searchterms"" height=""20"" valign=""top"" style=""padding: 0 20px; font-family: Georgia; font-size: 12px; font-weight: bold;"" width=""600"" colspan=""2"">
                            Search Terms: " + string.Join(", ", item.SearchTerms) + @"<br />
                        </td>
                    </tr>");
                builder.Append(@"<tr>
                                <td class=""article-date"" height=""20"" valign=""top"" style=""padding: 0 20px; font-family: Georgia; font-size: 12px; font-weight: bold;"" width=""600"" colspan=""2"">
                                    " + item.PublishedDate.ToString("MM/dd/yyyy hh:mm tt") + @"<br />
                                </td>
                            </tr>");
                if (!String.IsNullOrEmpty(item.Summary))
                {
                    builder.Append(@"<tr >
                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;"" colspan=""2""><h2 style='display:inline; font-size:13px; font-style:italic; padding-right:8px;'>Summary:</h2>
                                    " + item.Summary + @"
                                    <br/>
                                </td>
                            </tr>");
                }
                if (!String.IsNullOrEmpty(item.Description))
                {
                    builder.Append(@"<tr >
                                <td class=""content-copy"" valign=""top"" style=""padding: 0 20px; color: #000; font-size: 14px; font-family: Georgia; line-height: 20px;"" colspan=""2""><h2 style='display:inline; font-size:13px; font-style:italic; padding-right:8px;'>Content:</h2>
                                    " + item.FormattedContent + @"
                                    <br/> <br/> <br/>
                                </td>
                            </tr>");
                }
            }
            return emailTemplate.Replace("|CONTENT|", builder.ToString());
        }

    }
}
