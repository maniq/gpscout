﻿using System;
using System.Diagnostics;

namespace AtlasDiligence.RssService
{
    public class LogEvents
    {
        private const string strLogName = "AtlanticBT";
        private const string source = "AtlasDiligenceRssService";

        /// <summary>
        /// If the log does not exist for us to write on make sure to create it
        /// </summary>
        private static void CheckLogExist()
        {
            if (!EventLog.Exists(strLogName) || !EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, strLogName);
            }
        }

        /// <summary>
        /// Able to write to the event log
        /// </summary>
        /// <param name="Message">Message you would like to display. Default: Information Message</param>
        public static void WriteLog(string Message)
        {
            WriteLog(Message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Pass the exception it will export the stack trace as well as any exceptions under it
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="messageType"></param>
        public static void WriteLog(Exception exception, EventLogEntryType messageType)
        {
            string message = exception.Message;
            if (exception.InnerException != null)
            {
                message += " --- " + exception.InnerException.Message;
            }

            message += " --- " + exception.StackTrace;

            WriteLog(message, messageType);
        }

        /// <summary>
        /// Able to write to the event log
        /// </summary>
        /// <param name="message">Message you would like to display</param>
        /// <param name="Type">What type of message is this?</param>
        public static void WriteLog(string message, EventLogEntryType MessageType)
        {
            EventLog SQLEventLog = new EventLog(strLogName);

            try
            {
                // Check to make sure we can write to the event log
                CheckLogExist();

                // set the source
                SQLEventLog.Source = source;

                // write the log
                SQLEventLog.WriteEntry(message, MessageType);

            }
            catch (Exception ex)
            {
                SQLEventLog.Source = source;
                SQLEventLog.WriteEntry(Convert.ToString("INFORMATION: ")
                                      + Convert.ToString(ex.Message),
                EventLogEntryType.Error);
            }
            finally
            {
                SQLEventLog.Dispose();
                SQLEventLog = null;
            }
        }
    }
}
