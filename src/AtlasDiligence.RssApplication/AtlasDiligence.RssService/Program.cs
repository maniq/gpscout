﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace AtlasDiligence.RssService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            var t = new RssService();
            t.DoWork();
#else
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                new RssService() 
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception e)
            {
                LogEvents.WriteLog(e, EventLogEntryType.Error);
            }
#endif
        }
    }
}
