﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.General
{
    public static class LinqExtensions
    {
        public static TResult Median<T, TResult>(this IEnumerable<T> source, Func<T, TResult> expression)
        {
            if (!source.Any())
            {
                throw new InvalidOperationException("Cannot compute median for an empty set.");
            }
            var sortedList = source.Select(expression.Invoke).OrderBy(m => m);

            var itemIndex = sortedList.Count() / 2;

            if (sortedList.Count() % 2 == 0)
            {
                // Even number of items. 
                var higherMiddle = (dynamic)sortedList.ElementAt(itemIndex);
                var lowerMiddle = (dynamic)sortedList.ElementAt(itemIndex - 1);

                return (TResult)((higherMiddle + lowerMiddle) / 2);
            }
            return sortedList.ElementAt(itemIndex);
        }
    }
}