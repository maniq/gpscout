﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace AtlasDiligence.Web.General
{
    public static class StringExtensions
    {
        public static string NewlineToBreak(this string s)
        {
            if (s != null)
            {
                return s.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
            }
            else
            {
                return s;
            }
        }

        public static HtmlString Currency(decimal value, AtlasDiligence.Common.Data.Models.Currency currency, int decimalPlaces = 2, string decimalFormat = "#,#")
        {
            decimalFormat = decimalFormat.PadRight(decimalFormat.Length + decimalPlaces, '0');
            var retval = new StringBuilder();
            retval.Append("<span title=\"");
            retval.Append(String.IsNullOrWhiteSpace(currency.Name) ? currency.Code : currency.Name);
            retval.Append("\">");
            retval.Append(currency.FormattedSymbol);
            retval.Append(value.ToString(decimalFormat));
            retval.Append("</span>");
            return new HtmlString(retval.ToString());
        }

        public static HtmlString Currency(decimal? value, AtlasDiligence.Common.Data.Models.Currency currency, int decimalPlaces = 2, string decimalFormat = "#,#")
        {
            return (!value.HasValue || currency == null)
                       ? new HtmlString(string.Empty)
                       : Currency(value.Value, currency, decimalPlaces, decimalFormat);
        }

        public static HtmlString IfNullOrEmptyThen(this string s, string ifEmpty)
        {
            return new HtmlString(String.IsNullOrWhiteSpace(s) ? ifEmpty : s);
        }
    }
}