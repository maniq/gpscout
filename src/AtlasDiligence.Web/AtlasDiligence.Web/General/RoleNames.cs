﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.General
{
    /// <summary>
    /// NOTE: These names must be in sync with roles in the asp_roles table.
    /// </summary>
    public static class RoleNames
    {
        /// <summary>
        /// Atlas Admin.
        /// </summary>
        public const string Admin = "Admin";

        /// <summary>
        /// Role is added after additional user info is collected when user first logs in.
        /// Used to direct redirect in custom AtlasAuthorize
        /// </summary>
        public const string CollectedInfo = "CollectedInfo";

        public const string GroupLeader = "GroupLeader";

        public const string AdminAndGroupLeader = Admin + ", " + GroupLeader;

        public const string Employee = "Employee";

        public const string AdminAndEmployee = Admin + ", " + Employee;

    }
}
