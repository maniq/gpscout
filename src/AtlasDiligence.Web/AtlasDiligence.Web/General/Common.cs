﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;

namespace AtlasDiligence.Web.General
{
    public static class Common
    {
        /// <summary>
        /// this will return an embedded text file as a string.
        /// </summary>
        /// <param name="asm">The assembly in which the file was embedded.</param>
        /// <param name="resource">The name of the file in the assembly.</param>
        /// <returns>The contents of the specified resource.</returns>
        public static String GetEmbeddedString(this Assembly asm, String resource)
        {
            StreamReader sr = new StreamReader(asm.GetManifestResourceStream(resource));
            return sr.ReadToEnd();
        }


        /// <summary>
        /// Gets string name of generic item passed in via lamda.
        /// Use:
        /// GetName( () => myObject.myProperty)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string GetName<T>(Expression<Func<T>> e)
        {
            var member = (MemberExpression)e.Body;
            return member.Member.Name;
        }

        /// <summary>
        /// Returns DisplayName property of DisplayNameAttribute if applicable to property param.
        /// Otherwise, returns property name.
        /// </summary>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string GetDisplayName<TProperty>(Expression<Func<TProperty>> e)
        {
            var member = (MemberExpression)e.Body;
            var attrs =
                member.Member.GetCustomAttributes(typeof(DisplayNameAttribute), false) as DisplayNameAttribute[];

            return (attrs != null && attrs.Length > 0) ? attrs[0].DisplayName : member.Member.Name;
        }
    }
}