﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Mvc;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.Models.ViewModels;
using System.Linq;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels.Search;
using Task = System.Threading.Tasks.Task;

namespace AtlasDiligence.Web.Controllers
{
    using AtlanticBT.Common.ComponentBroker;
    using Common.Data.Repositories;
    using Common.DTO.Enums;

    [AtlasAuthorize(Roles = RoleNames.CollectedInfo)]
    [EulaAuthorize]
    public class OrganizationController : ControllerBase
    {
        private OrganizationViewModel _viewModel;

        private IGroupRepository _groupRepository { get { return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>(); } }

        public ActionResult Index(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set GroupProductType
            if (UserEntity.GroupId.HasValue)
            {
                if (UserEntity.Group != null)
                {
                    var groupOrganization = UserEntity.Group.GroupOrganizations.SingleOrDefault(m => m.OrganizationId == id && m.GroupId == UserEntity.Group.Id);
                    _viewModel.GroupProductType = (groupOrganization != null)
                        ? ProductTypes.DiligenceAccount
                        : (ProductTypes)UserEntity.Group.DefaultProductType;
                }
            }
            else
            {
                _viewModel.GroupProductType = ProductTypes.DiligenceAccount;
            }

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
            if (UserEntity.GroupId.HasValue)
            {
	            var availableOrgs = GetAvailableOrganizations();
                if (!availableOrgs.Any())
                {
                    ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                    return this.View("Error");
                }

				_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
            }

			// set folders
            _viewModel.Folders = UserEntity.Folders;

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            OrganizationService.AddOrganizationViewing(UserEntity.Id, id);
            SetUserEntity(UserService.GetUserEntityByUserId(UserEntity.Id));

			// set Preview
            _viewModel.Preview = User.IsInRole(RoleNames.Admin) || User.IsInRole(RoleNames.Employee);

			UserService.TrackOrganizationProfileView(UserEntity.Id, id);

            return View(_viewModel);
        }

        public ActionResult Overview(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationNotes
            _viewModel.OrganizationNotes = NoteRepository.GetByUserIdAndOrganizationId(UserEntity.Id, id).ToList();

			// set OrganizationsInSameSegment
            _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
            if (UserEntity.GroupId.HasValue)
            {
	            var availableOrgs = GetAvailableOrganizations();
				if (!availableOrgs.Any())
                {
                    ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                    return this.View("Error");
                }

                _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
            }
            else
            {
                int total;
                _viewModel.OrganizationsInSameSegment.AddRange(SearchService.GetOrganizations(new SearchFilter(), out total));
            }

			// set NewsItems
			//if (_viewModel.NewsItems == null)
			//{
			//	_viewModel.NewsItems =
			//		NewsItemService.GetByOrganizationId(id).OrderByDescending(m => m.PublishedDate).Take(3).ToList();
			//}

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return this.View(_viewModel);
        }

        public ActionResult Grade(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set SearchOptions
            _viewModel.SearchOptions = new SearchOptionsViewModel();
            _viewModel.SearchOptions.Build();

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
                else
                {
                    int total;
                    _viewModel.OrganizationsInSameSegment.AddRange(SearchService.GetOrganizations(new SearchFilter(), out total));
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            return this.View(_viewModel);
        }

        /// <summary>
        /// Team tab
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Team(Guid id)
        {
            PopulateViewModelFromSession(id);
			
			// set Organization
            if (_viewModel.Organization == null)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => OrganizationService.GetById(id))
                    .ContinueWith(t => _viewModel.Organization = t.Result);
            }

			// set OrganizationMembers
            _viewModel.OrganizationMembers = _viewModel.Organization.OrganizationMembers.ToList();

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return this.View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Strategy(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Process(Guid id)
        {
            PopulateViewModelFromSession(id);
			
			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Firm(Guid id)
        {
            PopulateViewModelFromSession(id);
			
			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }
            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Funds(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult TrackRecord(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>g
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //       [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Evaluation(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PrintOverview(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set Folders
            _viewModel.Folders = FolderRepository.GetByUserId(UserEntity.Id);

			// set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PrintAll(Guid id)
        {
            PopulateViewModelFromSession(id);

			// set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

			// set Folders
            _viewModel.Folders = FolderRepository.GetByUserId(UserEntity.Id);

			// set ORganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
	                var availableOrgs = GetAvailableOrganizations();
					if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

					_viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }
			
			// set GroupProductType
            if (UserEntity.GroupId.HasValue)
            {
                var group = GroupRepository.GetById(UserEntity.GroupId.Value);
                if (group.DefaultProductType.HasValue)
                {
                    _viewModel.GroupProductType = (ProductTypes)group.DefaultProductType.Value;
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        public JsonResult GetScatterChart(Guid id, SearchOptionsViewModel SearchOptions)
        {
            SearchOptions.SectorSpecialist = SearchOptions.SectorSpecialist.GetValueOrDefault(false) ? true : new bool?();

            PopulateViewModelFromSession(id);

            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            var viewModel = new ScatterChartViewModel
                                {
                                    OrganizationId = _viewModel.Organization.Id,
                                    OrganizationName = _viewModel.Organization.Name,
                                    OrganizationQuantitativeScore =
                                        _viewModel.Organization.QuantitativeGradeNumber.GetValueOrDefault(10),
                                    OrganizationQualitativeScore =
                                        _viewModel.Organization.QualitativeGradeNumber.GetValueOrDefault(10),
                                    HtmlId = "scatterVisualization"
                                };
            var matchingSegments = new List<Segment>();
            var purchasedOrgs = new List<Guid>();
            if (UserEntity.GroupId.HasValue)
            {
                var segments = GroupRepository.GetById(UserEntity.GroupId.Value).GroupSegments.Select(m => m.Segment);
                matchingSegments.AddRange(_viewModel.Organization.FindMatchingSegments(segments.ToList()));

                purchasedOrgs = GroupRepository.GetById(UserEntity.GroupId.Value).GroupOrganizations.Select(s => s.OrganizationId).ToList();
            }

            int total;
            var filter = SearchOptions.ToSearchFilter();
            filter.Segments = matchingSegments;
            filter.PurchasedOrganizationIds = purchasedOrgs;

            viewModel.OtherOrganizations = SearchService.GetOrganizations(filter, out total).Where(m => (m.Quality.HasValue || m.Quantity.HasValue));

            return Json(new MessageViewModel { Data = total, RenderView = this.RenderPartialViewToString("FluidScatterChartPartial", viewModel) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLargeScatterChart(SearchOptionsViewModel SearchOptions)
        {
            SearchOptions.SectorSpecialist = SearchOptions.SectorSpecialist.GetValueOrDefault(false) ? true : new bool?();

            var segments = new List<Segment>();
            var purchasedOrgs = new List<Guid>();
            var viewModel = new ScatterChartViewModel();

            if (UserEntity.GroupId.HasValue)
            {
                segments = GroupRepository.GetById(UserEntity.GroupId.Value).GroupSegments.Select(m => m.Segment).ToList();
                purchasedOrgs = GroupRepository.GetById(UserEntity.GroupId.Value).GroupOrganizations.Select(s => s.OrganizationId).ToList();
            }

            int total;
            var filter = SearchOptions.ToSearchFilter();
            filter.Segments = segments;
            filter.PurchasedOrganizationIds = purchasedOrgs;

            viewModel.OtherOrganizations = SearchService.GetOrganizations(filter, out total).Where(m => (m.Quality.HasValue || m.Quantity.HasValue));

            return Json(new MessageViewModel { Data = total, RenderView = this.RenderPartialViewToString("LargeScatterChartPartial", viewModel) }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Populate and cache view model.
        /// </summary>
        /// <param name="id"></param>
        private void PopulateViewModelFromSession(Guid id)
        {
            var cache = EntityCache.Get<OrganizationViewModel>(Resources.Params.OrganizationViewModelCache);
            if (cache == null || cache.Id != id)
            {
                cache = new OrganizationViewModel { Id = id };
                EntityCache.Add(Resources.Params.OrganizationViewModelCache, cache);
            }
            _viewModel = cache;
        }

        /// <summary>
        /// Add viewmodel to EntityCache.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            EntityCache.Add(Resources.Params.OrganizationViewModelCache, _viewModel);
            base.Dispose(disposing);
        }
    }
}
