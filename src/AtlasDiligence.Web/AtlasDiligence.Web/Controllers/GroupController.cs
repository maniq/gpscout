﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Web.Resources;
using log4net;
using Group = AtlasDiligence.Common.DTO.Model.Group;

namespace AtlasDiligence.Web.Controllers
{
    [AtlasAuthorize(Roles = RoleNames.AdminAndGroupLeader)]
    [EulaAuthorize]
    public class GroupController : ControllerBase
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(GroupController));

        private IGroupRepository _groupRepository { get { return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>(); } }

        private IGroupService _groupService { get { return ComponentBrokerInstance.RetrieveComponent<IGroupService>(); } }

        private IUserService _userService { get { return ComponentBrokerInstance.RetrieveComponent<IUserService>(); } }

        /// <summary>
        /// List of groups and create group page. Admin only.
        /// </summary>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Group grid
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult GroupGrid(int pageStart, string sortColumn, bool sortAscending)
        {
            var excludedUsers = new List<Guid>() { GetLoggedInUserId() };

            var results = new GridList<GroupDataGrid>();
            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "Name";
                sortAscending = true;
            }

            Expression<Func<AtlasDiligence.Common.Data.Models.Group, bool>> predicate = c => true;
            var allUsers = _groupRepository.FindAll(predicate);
            var users = _groupRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, DataGridRowsPerPage());

            results.Total = allUsers.Count();
            results.Result = users.Select(x => new GroupDataGrid(x)).ToArray();

            return Json(results);
        }

        /// <summary>
        /// Display details for a Group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult View(Guid? id)
        {
            //if group id is null, default to the UserEntity.GroupId
            var groupId = id.GetValueOrDefault(UserEntity.GroupId.GetValueOrDefault(Guid.Empty));
            AtlasDiligence.Common.Data.Models.Group group = _groupRepository.GetById(groupId);

            if (group == null || !base.CanAccessGroup(group.Id))
            {
                ViewData["Message"] = string.Format(Resources.ErrorMessages.InvalidId, "Group");
                return View();
            }

            var model = GroupViewModel.Build(group, _groupRepository);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public ActionResult Create()
        {
            var model = new GroupViewModel();
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        [HttpPost]
        public ActionResult Create(GroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Id = CreateGroup(model);
                //update crm system on different thread
                System.Threading.Tasks.Task.Factory.StartNew(() => SaveCrmGroup(model));

                TempData["Message"] = string.Format("Group \"{0}\" has been created", model.Name);
                return RedirectToAction("Edit", new { id = model.Id });
            }

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public ActionResult Edit(Guid id)
        {
            var group = _groupRepository.GetById(id);
            if (group == null || !base.CanAccessGroup(group.Id))
            {
                ViewData["Message"] = string.Format(ErrorMessages.InvalidId, "Group");
                return View();
            }

            var model = GroupViewModel.Build(group, _groupRepository);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        [HttpPost]
        public ActionResult Edit(GroupViewModel model)
        {
            if (ViewData.ModelState.IsValid)
            {
                var group = _groupRepository.GetById(model.Id);
                if (group == null || !base.CanAccessGroup(group.Id))
                {
                    ViewData["Message"] = string.Format(ErrorMessages.InvalidId, "Group");
                    return View();
                }

                model.CrmId = group.CrmId;
                UpdateGroup(model);

                //update crm system on different thread
                System.Threading.Tasks.Task.Factory.StartNew(() => SaveCrmGroup(model));

                TempData["Message"] = string.Format("Group \"{0}\" has been edited", model.Name);
                // return RedirectToAction("View", new { groupId = model.Id });
            }

            GroupViewModel.PopulateDefaults(model, _groupRepository);
            return View(model);
        }


        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult Delete(Guid id)
        {
            var group = GroupRepository.GetById(id);
            if (group == null)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = String.Format(ErrorMessages.InvalidId, "Group") });
            }

            var adminRole =
                new Repository<aspnet_Role>().FindAll(m => m.RoleName == RoleNames.Admin).FirstOrDefault();

            foreach (var user in group.UserExts)
            {
                user.GroupId = null;

                // if the user is not an admin
                if (!user.aspnet_User.aspnet_UsersInRoles.Any(m => m.RoleId == adminRole.RoleId))
                {
                    user.aspnet_User.aspnet_Membership.IsApproved = false;
                }
            }

            System.Threading.Tasks.Task.Factory.StartNew(() => _groupService.DeleteGroup(group.CrmId));
            GroupRepository.DeleteOnSubmit(group);
            GroupRepository.SubmitChanges();
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);

        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult RemoveGroupOrganization(Guid groupId, Guid organizationId)
        {
            _groupRepository.RemoveGroupOrganization(groupId, organizationId);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AddGroupOrganization(Guid groupId, string organizationName)
        {
            var organizations = OrganizationService.GetOrganizationsByName(organizationName);
            if (organizations.Count() != 1)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "Could not match a single organization with a name \"" + organizationName + "\"" }, JsonRequestBehavior.AllowGet);
            }
            _groupRepository.AddGroupOrganization(groupId, organizations.FirstOrDefault().Id);

            return Json(new MessageViewModel { Status = MessageStatus.Success, Data = new { organizations.FirstOrDefault().Id, organizations.FirstOrDefault().Name } }, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult RemoveGroupSegment(Guid groupId, Guid segmentId)
        {
            _groupRepository.RemoveGroupSegment(groupId, segmentId);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AddGroupSegment(Guid groupId, String segmentName)
        {
            var segments = _groupRepository.GetSegmentsByName(segmentName);
            if (segments == null)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "Could not match a segment with name of \"" + segmentName + "\"" }, JsonRequestBehavior.AllowGet);
            }

            if (segments.Count() > 1)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "Could not match a single segment with name of \"" + segmentName + "\"" }, JsonRequestBehavior.AllowGet);
            }

            var segment = segments.First();
            _groupRepository.AddGroupSegment(groupId, segment.Id);

            return Json(new MessageViewModel { Status = MessageStatus.Success, Data = new { segment.Id, segment.Name } }, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult AddExistingUser(Guid groupId, string email)
        {
            var user = _userService.GetUserEntityByEmail(email);
            if (user == null)
            {
                return Json(new MessageViewModel { Message = "User with email " + email + " does not exist", Status = MessageStatus.Failure }, JsonRequestBehavior.AllowGet);
            }
            var group = _groupRepository.GetById(groupId);
            if (group.UserExts.Count(m => m.aspnet_User.aspnet_Membership.IsApproved) >= group.MaxUsers && user.IsApproved)
            {
                return Json(new MessageViewModel { Message = "Max Users exceeded for group", Status = MessageStatus.Failure, Data = user.Id }, JsonRequestBehavior.AllowGet);
            }
            user.GroupId = groupId;
            _userService.UpdateUser(user);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        [AtlasAuthorize(Roles = RoleNames.Admin)]
        public JsonResult RemoveUser(Guid id)
        {
            var user = UserService.GetUserEntityByUserId(id);
            user.GroupId = null;
            UserService.UpdateUser(user);
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        private void SaveCrmGroup(GroupViewModel model)
        {
            try
            {
                bool isNew = false;
                var crmGroup = _groupService.GetById(model.CrmId) ?? _groupService.GetByName(model.Name);
                if (crmGroup == null)
                {
                    crmGroup = new Group();
                    isNew = true;
                }
                crmGroup.MaxUsers = model.MaxUsers;
                crmGroup.Name = model.Name;
                // TODO: [RM 11935] crmGroup.DefaultProductType = (int)model.DefaultProductType;

                if (isNew)
                {
                    model.CrmId = _groupService.Create(crmGroup);
                }
                else
                {
                    _groupService.Update(crmGroup);
                }

                //update local w/ crmId
                UpdateGroup(model);
            }
            catch (TimeoutException ex)
            {
                _log.Error(string.Format(Resources.ErrorMessages.TimeoutError, ex.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void UpdateGroup(GroupViewModel model)
        {
            AtlasDiligence.Common.Data.Models.Group group = _groupRepository.GetById(model.Id);
            if (group != null)
            {
                group.CrmId = model.CrmId;
                group.Name = model.Name;
                group.MaxUsers = model.MaxUsers;
                group.DefaultProductType = (int)model.DefaultProductType;
                _groupRepository.SubmitChanges();
            }
        }

        private Guid CreateGroup(GroupViewModel model)
        {
            var group = new AtlasDiligence.Common.Data.Models.Group();
            group.Id = Guid.NewGuid();
            group.Name = model.Name;
            group.MaxUsers = model.MaxUsers;
            group.DefaultProductType = (int)model.DefaultProductType;

            _groupRepository.InsertOnSubmit(group);
            _groupRepository.SubmitChanges();

            return group.Id;
        }
    }
}
