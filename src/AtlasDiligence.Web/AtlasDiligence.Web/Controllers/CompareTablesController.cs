﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.CompareTables;
using Newtonsoft.Json;
using Organization = AtlasDiligence.Common.Data.Models.Organization;

namespace AtlasDiligence.Web.Controllers
{
	[AtlasAuthorize]
	[EulaAuthorize]
	[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
	public class CompareTablesController : ControllerBase
	{
		#region ctor and properties
		private Dictionary<Guid, string> _publishedOrgs
		{
			get { return GetPublishedOrganizations(); }
		}

		private IList<Guid> CompareOrganizationIds
		{
			get
			{
				var sessionIds = (List<Guid>)Session["sOrgIds"];

				return sessionIds ?? new List<Guid>();
			}
			set { Session["sOrgIds"] = value; }
		}

		public int BenchmarkVintageYear
		{
			get
			{
				var vintageYear = Session["benchmarkSelectedVintageYear"];

				return vintageYear != null ? int.Parse(vintageYear.ToString()) : 0;
			}
			set { Session["benchmarkSelectedVintageYear"] = value; }
		}

		public CompareTablesController()
		{
		}
		#endregion

		#region requests
		public ActionResult Index()
		{
			return View(new CompareTablesViewModel { OrganizationItems = _publishedOrgs, SelectedOrganizationIds = CompareOrganizationIds, SelectedOrganizations = GetSelectedOrganizations() });
		}

		[HttpPost]
		public ActionResult Index(List<Guid> selectedOrganizationIds)
		{
			CompareOrganizationIds = selectedOrganizationIds;

			return RedirectToAction("Index");
		}
		#endregion

		#region performance tabs
		public ActionResult TVPI()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
						{
							Id = fe.Id,
							Name = fe.Name,
							CellData = new Dictionary<int, List<string>>(),
							Funds = fe.Funds,
							VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
						};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
								fd =>
								{
									switch (fd.PerformanceDatasource)
									{
										case "General Partner":
											if (fd.AsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
														   fd.Tvpi.HasValue
															   ? fd.Tvpi.Value.ToString("0.00") + "x"
															   : "N/A");
											}
											break;
										case "Preqin":
											if (fd.PreqinAsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
														   fd.PreqinTvpi.HasValue
															   ? fd.PreqinTvpi.Value.ToString("0.00") + "x"
															   : "N/A");
											}
											break;
									}
								});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult NetIRR()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						CellData = new Dictionary<int, List<string>>(),
						Funds = fe.Funds,
						VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
					};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
								fd =>
								{
									switch (fd.PerformanceDatasource)
									{
										case "General Partner":
											if (fd.AsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																   fd.Irr.HasValue
																	   ? fd.Irr.Value.ToString("0.0") + "%"
																	   : "N/A");
											}
											break;
										case "Preqin":
											if (fd.PreqinAsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																	   fd.PreqinIrr.HasValue
																		   ? fd.PreqinIrr.Value.ToString("0.0") + "%"
																		   : "N/A");
											}
											break;
									}
								});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult DPI()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						CellData = new Dictionary<int, List<string>>(),
						Funds = fe.Funds,
						VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
					};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
								fd =>
								{
									switch (fd.PerformanceDatasource)
									{
										case "General Partner":
											if (fd.AsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																	   fd.Dpi.HasValue
																		   ? fd.Dpi.Value.ToString("0.00") + "x"
																		   : "N/A");
											}
											break;
										case "Preqin":
											if (fd.PreqinAsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																	   fd.PreqinDpi.HasValue
																		   ? fd.PreqinDpi.Value.ToString("0.00") + "x"
																		   : "N/A");
											}
											break;
									}
								});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult MOIC()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						CellData = new Dictionary<int, List<string>>(),
						Funds = fe.Funds,
						VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
					};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
									fd =>
									{
										if (fd.PerformanceDatasource.Equals("General Partner") && fd.AsOf.HasValue)
										{
											tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																   fd.Moic.HasValue
																	   ? fd.Moic.Value.ToString("0.00") + "x"
																	   : "N/A");
										}
									});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult GrossIRR()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						CellData = new Dictionary<int, List<string>>(),
						Funds = fe.Funds,
						VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
					};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
									fd =>
									{
										if (fd.PerformanceDatasource.Equals("General Partner") && fd.AsOf.HasValue)
										{
											tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																   fd.GrossIrr.HasValue
																	   ? fd.GrossIrr.Value.ToString("0.0") + "%"
																	   : "N/A");
										}
									});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult FundSize()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						CellData = new Dictionary<int, List<string>>(),
						Funds = fe.Funds,
						VintageYears = fe.Funds.Where(w => !String.IsNullOrEmpty(w.VintageYear)).Select(s => Convert.ToInt32(s.VintageYear))
					};

					fe.Funds.Where(w => w.PublishTrackRecord)
							.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
							.ThenBy(t => t.Name)
							.ToList().ForEach(
								fd =>
								{
									switch (fd.PerformanceDatasource)
									{
										case "General Partner":
											if (fd.AsOf.HasValue)
											{

												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																	   fd.FundSize.HasValue
																		   ? String.Format("{0}{1}",
																						   fd.Currency.FormattedSymbol,
																						   fd.FundSize.Value.ToString(
																							   "#,#"))
																		   : "N/A");
											}
											break;
										case "Preqin":
											if (fd.PreqinAsOf.HasValue)
											{
												tableData.AddCellData(Convert.ToInt32(fd.VintageYear),
																	   fd.PreqinFundSize.HasValue
																		   ? String.Format("{0}{1}",
																						   fd.Currency.FormattedSymbol,
																						   fd.PreqinFundSize.Value.ToString(
																							   "#,#"))
																		   : "N/A");
											}
											break;
									}
								});

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("PerformanceTable", tableDataList);
		}

		public ActionResult Charts()
		{
			var chartData = new List<ChartData>();

			GetSelectedOrganizations().ToList().ForEach(fe => fe.Funds
																.Where(w => w.PublishTrackRecord && !String.IsNullOrEmpty(w.VintageYear))
																.OrderBy(o => o.SortOrder.GetValueOrDefault(0))
																.ThenBy(t => t.Name)
																.ToList().ForEach(fd =>
																	{
																		switch (fd.PerformanceDatasource)
																		{
																			case "General Partner":
																				if (fd.AsOf.HasValue && (fd.Tvpi.HasValue || fd.Irr.HasValue))
																				{
																					chartData.Add(new ChartData
																						{
																							OrganizationName = fd.Organization.Name,
																							FundName = fd.Name,
																							FundSize = fd.FundSize != null ? String.Format("{0}{1}", fd.FundSize.Value.ToString("#,#"), "mm") : String.Empty,
																							TVPI = fd.Tvpi.HasValue ? fd.Tvpi.Value.ToString("0.00") : String.Empty,
																							NetIRR = fd.Irr.HasValue ? fd.Irr.Value.ToString("0.0") : String.Empty,
																							VintageYear = Convert.ToInt32(fd.VintageYear),
																							AsOf = fd.AsOf.Value.ToShortDateString(),
																							DPI = fd.Dpi.HasValue ? String.Format("{0}{1}", fd.Dpi.Value.ToString("0.00"), "x") : String.Empty,
																							MOIC = String.Format("{0}{1}", fd.Moic.Value.ToString("0.00"), "x"),
																							GrossIRR = String.Format("{0}{1}", fd.GrossIrr.Value.ToString("0.0"), "%"),
																							Source = fd.PerformanceDatasource
																						});
																				}
																				break;
																			case "Preqin":
																				if (fd.PreqinAsOf.HasValue && (fd.PreqinTvpi.HasValue || fd.PreqinIrr.HasValue))
																				{
																					chartData.Add(new ChartData
																						{
																							OrganizationName = fd.Organization.Name,
																							FundName = fd.Name,
																							FundSize = fd.FundSize != null ? String.Format("{0}{1}", fd.FundSize.Value.ToString("#,#"), "mm") : String.Empty,
																							TVPI = fd.PreqinTvpi.HasValue ? fd.PreqinTvpi.Value.ToString("0.00") : String.Empty,
																							NetIRR = fd.PreqinIrr.HasValue ? fd.PreqinIrr.Value.ToString("0.0") : String.Empty,
																							VintageYear = Convert.ToInt32(fd.VintageYear),
																							AsOf = fd.PreqinAsOf.Value.ToShortDateString(),
																							DPI = fd.PreqinDpi.HasValue ? String.Format("{0}{1}", fd.PreqinDpi.Value.ToString("0.00"), "x") : String.Empty,
																							MOIC = String.Empty,
																							GrossIRR = String.Empty,
																							Source = fd.PerformanceDatasource
																						});
																				}
																				break;
																		}
																	}));

			if (!chartData.Any())
			{
				chartData = GetSelectedOrganizations().Select(s => new ChartData { OrganizationName = s.Name }).ToList();
			}

			return View("Charts", chartData);
		}

		public ActionResult Benchmarks()
		{
			var availableOrgIds = GetAvailableOrganizations().Select(s => s.Id).Distinct();

			// variables
			var selectedOrgs = GetSelectedOrganizations().ToList();
			var benchmarkViewModel = new BenchmarkViewModel
				{
					SelectedVintageYear = BenchmarkVintageYear,
					AvailableVintageYears = OrganizationService.GetAvailbleFundVintageYears(selectedOrgs.Select(s=> s.Id))
				};

			// build benchmark
			if (selectedOrgs.Any())
			{
				benchmarkViewModel.Build(OrganizationService.GetAvailbleFundVintageYears(availableOrgIds), selectedOrgs);
			}

			return View("Benchmarks", benchmarkViewModel);
		}

		#endregion

		#region scorecard tabs

		public ActionResult Qualitative()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						Grades = fe.Grades
					};

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("QualitativeScorecardTable", tableDataList);
		}

		public ActionResult Quantitative()
		{
			var tableDataList = new List<TableDataViewModel>();

			GetSelectedOrganizations().ToList().ForEach(
				fe =>
				{
					var tableData = new TableDataViewModel
					{
						Id = fe.Id,
						Name = fe.Name,
						Grades = fe.Grades
					};

					tableDataList.Add(tableData);
				});

			PopulateTableData(tableDataList);

			return View("QuantitativeScorecardTable", tableDataList);
		}

		#endregion

		#region jquery json requests
		public JsonResult GetBenchmarkData(int vintageYear)
		{
			// cache selected vintage year
			BenchmarkVintageYear = vintageYear;

			// setup vars
			var selectedOrgs = GetSelectedOrganizations().ToList();
			var benchmarkViewModel = new BenchmarkViewModel();

			// build
			benchmarkViewModel.Build(OrganizationService.GetComparetablesOrganizationFunds(GetAvailableOrganizations().Select(s => s.Id).Distinct()), selectedOrgs, OrganizationService.GetBenchmarks(), vintageYear, OrganizationService.GetAvailbleFundVintageYears(selectedOrgs.Select(s => s.Id)));

			return Json(benchmarkViewModel, JsonRequestBehavior.AllowGet);
		}

		public JsonResult AddOrganizationId(string organizationId)
		{
			if (CompareOrganizationIds.Count >= 5)
			{
				return Json(">5", JsonRequestBehavior.AllowGet);
			}

			AddCompareOrganizationId(organizationId);

			return Json("success", JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult CompareOrganizations(string organizationIds)
		{
			try
			{
				CompareOrganizationIds = JsonConvert.DeserializeObject<List<Guid>>(organizationIds);
			}
			catch (Exception)
			{
				// TODO: JN - return json error to handled by jquery
			}

			return Json(string.Empty, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region helper methods

		private void AddCompareOrganizationId(string orgId)
		{
			try
			{
				// get session ids
				var sessionIds = CompareOrganizationIds;
				sessionIds.Add(new Guid(orgId));

				// update session ids
				CompareOrganizationIds = sessionIds;
			}
			catch
			{
			}
		}

		private Dictionary<Guid, string> GetPublishedOrganizations()
		{
			// get available orgs
			var availableOrgs = GetAvailableOrganizations();

			// filter lucene results
			var filterOrganizations = availableOrgs.Select(s => s.Id).Distinct().ToList();

			return OrganizationService.GetComparTableOrgsByIds(filterOrganizations);
		}

		private IEnumerable<Common.Data.Models.Organization> GetSelectedOrganizations()
		{
			return CompareOrganizationIds.Any() ? OrganizationService.GetAllByIds(CompareOrganizationIds) : new List<Common.Data.Models.Organization>();
		}

		private void PopulateTableData(List<TableDataViewModel> tableDataList)
		{
			while (tableDataList.Count < 5)
			{
				tableDataList.Add(new TableDataViewModel());
			}
		}

		#endregion
	}
}
