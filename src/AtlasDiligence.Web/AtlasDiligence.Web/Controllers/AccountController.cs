﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Account;

namespace AtlasDiligence.Web.Controllers
{
    public class AccountController : ControllerBase
    {
        private IUserService _userService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        private IFormsAuthenticationService _formsService { get { return ComponentBrokerInstance.RetrieveComponent<IFormsAuthenticationService>(); } }

        public AccountController() { }

        /// <summary>
        /// Request Access to Atlas Diligence.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RequestAccess(RequestAccessViewModel model)
        {
            var retval = new MessageViewModel { Status = MessageStatus.Failure };

            if (ModelState.IsValid)
            {
                var returnMessage = _userService.CreateRequestAccess(model);

                if (returnMessage == MembershipCreateStatus.Success)
                {
                    retval.Status = MessageStatus.Success;
                }
                else if (returnMessage == MembershipCreateStatus.DuplicateUserName ||
                         returnMessage == MembershipCreateStatus.DuplicateEmail)
                {
                    retval.Status = MessageStatus.Failure;
                    ModelState.AddModelError("Email", AccountValidation.ErrorCodeToString(returnMessage));
                }
                else
                {
                    retval.Status = MessageStatus.Failure;
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(returnMessage));
                }
            }

            retval.RenderView = retval.Status == MessageStatus.Success
                ? RenderPartialViewToString("_RequestAccessSuccessPartial", model)
                : RenderPartialViewToString("_RequestAccessPartial", model);

            return Json(retval);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOn()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Basic");
            }
            return View(new LogOnViewModel());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult LogOnPartial()
        {
            return PartialView("_LogOnForm", new LogOnViewModel());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult RequestAccessPartial()
        {
            return PartialView("_RequestAccessPartial", new RequestAccessViewModel());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LogOn(LogOnViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (this._userService.ValidateUser(model.UserName, model.Password))
                {
                    //call first to wipe out any previous session & auth stores on server side.
                    HttpContext.Session.RemoveAll();
                    _formsService.SignOut();
                    _formsService.SignIn(model.UserName, model.RememberMe);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Basic");
                    }
                }
                else
                {
                    ModelState.AddModelError("",
                                           _userService.IsLockedOut(model.UserName)
                                               ? Resources.ErrorMessages.UserIsLockedOut
                                               : Resources.ErrorMessages.UserNameOrPasswordIncorrect);
                }
            }

            return View(model);
        }

        [Authorize]
        [EulaAuthorize]
        public ActionResult CollectInfo()
        {
            var user = _userService.GetUserEntityByEmail(User.Identity.Name);
            var model = new CollectInfoViewModel()
                            {
                                Id = user.Id,
                                OrganizationName = user.OrganizationName,
                                OrganizationType = user.OrganizationType,
                                Phone = user.Phone,
                                ReferredBy = user.ReferredBy,
                                Title = user.Title
                            };
            return View(model);
        }

        [Authorize]
        [EulaAuthorize]
        [HttpPost]
        public ActionResult CollectInfo(CollectInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!_userService.SaveCollectedInfo(model))
                {
                    ModelState.AddModelError("", Resources.ErrorMessages.DefaultError);
                    return View(model);
                }
                return RedirectToAction("Index", "Basic");
            }

            return View(model);
        }

        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
			// track user logoff
			UserService.TrackUserLogout(UserEntity.Id);

			// signout
            HttpContext.Session.RemoveAll();
            _formsService.SignOut();

            return RedirectToAction("Index", "Basic");
        }

        //
        // GET: /Account/ChangePassword

        [AtlasAuthorize]
        [EulaAuthorize]
        public ActionResult ChangePassword()
        {
            ViewBag.PasswordLength = _userService.MinPasswordLength;
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [AtlasAuthorize]
        [EulaAuthorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                if (_userService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewBag.PasswordLength = _userService.MinPasswordLength;
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult ForgotPassword()
        {
            var model = new ForgotPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetUserEntityByEmail(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError("", Resources.ErrorMessages.UserEmailDoesNotExist);
                    return View(model);
                }

                if (!_userService.ResetPassword(user, null))
                {
                    ModelState.AddModelError("", Resources.ErrorMessages.PasswordResetError);
                    return View(model);
                }

                TempData["Message"] = string.Format(Resources.ErrorMessages.PasswordIsReset, model.Email);
                return RedirectToAction("LogOn");
            }

            return View(model);
        }

        public ActionResult TermsAndConditions()
        {
            var viewModel = new TermsAndConditionsViewModel();
            viewModel.Build(EulaRepository);
            return this.View(viewModel);
        }

        [HttpPost]
        public ActionResult TermsAndConditions(TermsAndConditionsViewModel viewModel)
        {
            if (!viewModel.IsChecked)
            {
                ModelState.AddModelError("IsChecked", "You must accept the Terms and Conditions to continue.");
            }
            if (ModelState.IsValid)
            {
                EulaRepository.UserSignedEula(User.Identity.Name);
                return Session[EulaAuthorizeAttribute.EulaRedirectSessionKey] == null
                           ? this.Redirect("/Search/Start")
                           : this.Redirect(Session[EulaAuthorizeAttribute.EulaRedirectSessionKey].ToString());
            }
            viewModel.Build(EulaRepository);
            return this.View(viewModel);
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        #region RenderPartialViewToString
        /// <summary>
        /// Render a partial view to string to return as a Json value
        /// </summary>
        /// <returns></returns>
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}
