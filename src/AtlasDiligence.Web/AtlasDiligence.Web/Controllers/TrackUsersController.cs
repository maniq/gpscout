﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels.TrackUsers;

namespace AtlasDiligence.Web.Controllers
{
	[Authorize(Roles = "Admin,Employee")]
	public class TrackUsersController : ControllerBase
	{
		private IUserService _userService
		{
			get
			{
				return ComponentBrokerInstance.RetrieveComponent<IUserService>();
			}
		}

		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Index(TrackUsersViewModel model)
		{
			if (ModelState.IsValid)
			{
				if (model.EndDate < model.StartDate)
				{
					ModelState.AddModelError("EndDate", "End Date must be less than the Start Date");
				}
				else
				{
					return GenerateExcelExport(model);
				}
			}

			return RedirectToAction("Index", "Admin");
		}

		private ExcelResult GenerateExcelExport(TrackUsersViewModel model)
		{
			var export = CreateExcelResult(model.TrackType);
			export.Rows = GetExcelData(model, export.Headers);

			return export;
		}

		private IEnumerable<IDictionary<string, string>> GetExcelData(TrackUsersViewModel model, IEnumerable<string> headers)
		{
			var rows = new List<IDictionary<string, string>>();
			_userService.GetUserData(model.TrackType, model.StartDate, model.EndDate).OrderBy(o => o.Date).ToList()
				.ForEach(
					fe =>
					{
						// convert timezone to eastern per requirement
						TimeZoneInfo estZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

						var dictionary = new Dictionary<string, string>
								{
									{"Date", fe.Date.ToShortDateString()},
									{"Time", TimeZoneInfo.ConvertTimeFromUtc(fe.Date, estZone).ToShortTimeString()},
									{"First Name", fe.UserExt.FirstName},
									{"Last Name", fe.UserExt.LastName},
									{"Email", fe.UserExt.aspnet_User.UserName},
									{"Group", fe.UserExt.Group != null ? fe.UserExt.Group.Name : String.Empty}
								};

						if (headers.Contains("Organization Name"))
						{
							var trackUsersProfileView = fe.TrackUsersProfileViews.SingleOrDefault(s => s.TrackUsersId == fe.Id);
							if (trackUsersProfileView != null)
							{
								dictionary.Add("Organization Name", trackUsersProfileView.Organization.Name);
							}
						}

						rows.Add(dictionary);
					});

			return rows;
		}

		private ExcelResult CreateExcelResult(TrackUsersType type)
		{
			var headers = new List<string> { "Date", "Time", "First Name", "Last Name", "Email", "Group" };
			if (type == TrackUsersType.ProfileVisit)
			{
				headers.Add("Organization Name");
			}

			var excelResult = new ExcelResult
			{
				Headers = headers,
				DeleteFile = true,
				FileName = "Trackusers.xlsx",
				FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
			};

			return excelResult;
		}
	}
}
