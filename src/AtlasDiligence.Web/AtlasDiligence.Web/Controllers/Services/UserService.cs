﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Security;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Account;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using log4net;

namespace AtlasDiligence.Web.Controllers.Services
{
	public class UserService : IUserService
	{
		private readonly MembershipProvider _provider = Membership.Provider;
		private readonly ILog _log = LogManager.GetLogger(typeof(UserService));

		private IUserRepository _userRepository
		{
			get { return ComponentBrokerInstance.RetrieveComponent<IUserRepository>(); }
		}

		private IWebUserService _crmWebUserService
		{
			get { return ComponentBrokerInstance.RetrieveComponent<IWebUserService>(); }
		}

		private IGroupRepository _groupRepository
		{
			get { return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>(); }
		}

		private IGroupService _crmGroupService { get { return ComponentBrokerInstance.RetrieveComponent<IGroupService>(); } }

		public UserService() { }

		/// <summary>
		/// Used in Admin for user management
		/// </summary>
		/// <param name="pageStart"></param>
		/// <param name="sortColumn"></param>
		/// <param name="sortAscending"></param>
		/// <param name="rowsPerPage"></param>
		/// <param name="email"></param>
		/// <param name="firstName"></param>
		/// <param name="lastName"></param>
		/// <param name="excludedUsers">users to exclude from the grid (like Admins?)</param>
		/// <param name="accessGranted">has the users access been granted</param>
		/// <param name="groupId"></param>
		/// <returns></returns>
		public GridList<UserDataGrid> GetUserGridListResults(int pageStart, string sortColumn, bool sortAscending, int rowsPerPage,
			string email, string firstName, string lastName, List<Guid> excludedUsers, bool accessGranted, Guid? groupId)
		{
			var results = new GridList<UserDataGrid>();
			if (excludedUsers == null) excludedUsers = new List<Guid>();

			if (string.IsNullOrEmpty(sortColumn))
			{
				sortColumn = "LastName";
				sortAscending = true;
			}

			//need to set key to full dot-notation data model path.
			switch (sortColumn)
			{
				case "Email":
					sortColumn = "aspnet_Membership.Email";
					break;
				case "FirstName":
					sortColumn = "UserExt.FirstName";
					break;
				case "LastName":
					sortColumn = "UserExt.LastName";
					break;
				case "EulaSigned":
					sortColumn = "UserExt.EulaSigned";
					break;
				default:
					break;
			}

			Expression<Func<aspnet_User, bool>> predicate = c => (c.aspnet_Membership.Email.StartsWith(email) || string.IsNullOrEmpty(email))
				&& (c.UserName.StartsWith(email) || string.IsNullOrEmpty(email))
				&& (c.UserExt.FirstName.StartsWith(firstName) || string.IsNullOrEmpty(firstName))
				&& (c.UserExt.LastName.StartsWith(lastName) || string.IsNullOrEmpty(lastName))
				&& (!excludedUsers.Contains(c.UserId))
				&& (c.UserExt.AccessGranted == accessGranted)
				&& (c.UserExt.GroupId == groupId || groupId == null);
			var allUsers = _userRepository.FindAll(predicate);
			var users = _userRepository.Paginate(predicate, sortColumn, sortAscending, pageStart, rowsPerPage);

			results.Total = allUsers.Count();
			results.Result = users.Select(x => new UserDataGrid(x)).ToArray();

			return results;
		}

		/// <summary>
		/// return userEntities for a given group
		/// </summary>
		/// <param name="groupId"></param>
		/// <returns></returns>
		public IEnumerable<UserEntity> GetUsersForGroup(Guid groupId)
		{
			var retval =
				_userRepository.GetAll().Where(x => x.UserExt.GroupId == groupId).Select(y => UserEntity.GetInstance(y));
			return retval;
		}

		/// <summary>
		/// Returns Minimum password length from Membership Provider.
		/// </summary>
		public int MinPasswordLength
		{
			get
			{
				return _provider.MinRequiredPasswordLength;
			}
		}

		/// <summary>
		/// Validate user. Create user in membership if it only exists on Crm. Updates user with Crm data.
		/// </summary>
		/// <remarks>Crm is the master source of data, local db is slave. Therefore, local data will always be updated with crm data.
		/// Certain scenarios will fail:
		/// - If Crm data changes email and local user is locked out.
		/// </remarks>
		/// <param name="email"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public bool ValidateUser(string email, string password)
		{
			if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "userName");
			if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

			var user = _userRepository.GetByEmail(email);
			if (user != null)
			{
				var lastActivityBeforeThisLogin = user.LastActivityDate;

				if (this.IsUserLockedOut(email, password))
					return false;

				//if login is valid in asp_net model
				if (_provider.ValidateUser(email, password))
				{
					// update previous login
					user.UserExt.PreviousLogin = lastActivityBeforeThisLogin;

					// track user login
					TrackUserLogin(user.UserId);

					// save changes
					_userRepository.SubmitChanges();

					return true;
				}
			}

			return false;
		}

		/// <summary>
		///  Change user password in Membership model.
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="oldPassword"></param>
		/// <param name="newPassword"></param>
		/// <returns></returns>
		public bool ChangePassword(string userName, string oldPassword, string newPassword)
		{
			if (String.IsNullOrEmpty(userName))
				throw new ArgumentException("Value cannot be null or empty.", "userName");
			if (String.IsNullOrEmpty(oldPassword))
				throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
			if (String.IsNullOrEmpty(newPassword))
				throw new ArgumentException("Value cannot be null or empty.", "newPassword");

			// The underlying ChangePassword() will throw an exception rather
			// than return false in certain failure scenarios.
			try
			{
				MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
				if (currentUser != null && currentUser.ChangePassword(oldPassword, newPassword))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (ArgumentException)
			{
				return false;
			}
			catch (MembershipPasswordException)
			{
				return false;
			}
			catch (Exception)
			{
				return false;
			}
		}

		/// <summary>
		/// update both local dB and Crm users.
		/// </summary>
		/// <param name="userEntity"></param>
		/// <returns></returns>
		public UpdateUserMessage UpdateUser(UserEntity userEntity)
		{
			userEntity.AccessGranted = true;

			//check for dups
			if (HasDuplicate(userEntity)) return UpdateUserMessage.DeniedDuplicateEmail;

			if (SaveUserDetails(userEntity))
			{
				//create crmuser on a separate thread
				System.Threading.Tasks.Task.Factory.StartNew(() => SaveCrmUser(userEntity));

				return UpdateUserMessage.Success;
			}

			return UpdateUserMessage.DeniedUnknownError;
		}

		/// <summary>
		/// Gets data from web dB.
		/// </summary>
		/// <param name="email"></param>
		/// <returns></returns>
		public UserEntity GetUserEntityByEmail(string email)
		{
			UserEntity retval = null;

			var dbUser = _userRepository.GetAll().Where(x => x.aspnet_Membership.Email.Equals(email)).FirstOrDefault();
			if (dbUser != null)
			{
				retval = GetUserEntityByUserId(dbUser.UserId);
			}

			return retval;
		}

		/// <summary>
		/// Merges data from CRM and web dB.
		/// </summary>
		/// <param name="userId">UserId</param>
		/// <returns></returns>
		public UserEntity GetUserEntityByUserId(Guid userId)
		{
			UserEntity retval = null;

			var dbUser = _userRepository.GetAll().Where(m => m.UserId == userId).FirstOrDefault();
			if (dbUser != null)
			{
				retval = UserEntity.GetInstance(dbUser);
			}

			return retval;
		}

		/// <summary>
		/// Reset password and email user with new password.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="password">Optional. If null or empty, a random password will be supplied for reset.</param>
		/// <returns></returns>
		public bool ResetPassword(UserEntity user, string password)
		{
			bool retval = false;

			if (user != null)
			{
				if (string.IsNullOrWhiteSpace(password))
				{
					password = _provider.ResetPassword(user.UserName, "");
				}
				else
				{
					var oldPassword = _provider.ResetPassword(user.UserName, "");
					_provider.ChangePassword(user.UserName, oldPassword, password);
				}
				SendResetPasswordEmailToUser(user.Email, user.UserName, password);
				retval = true;
			}

			return retval;
		}

		/// <summary>
		/// Unlock user and reset password.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public bool UnlockUser(UserEntity user)
		{
			return UnlockUser(user, false);
		}

		/// <summary>
		/// Unlock user.
		/// </summary>
		/// <param name="user"></param>
		/// <param name="resetPassword">Option to also reset password</param>
		/// <returns></returns>
		private bool UnlockUser(UserEntity user, bool resetPassword)
		{
			bool retval = false;

			if (user != null)
			{
				if (_provider.UnlockUser(user.UserName))
				{
					if (resetPassword)
						ResetPassword(user, null);
					retval = true;
				}
			}

			return retval;
		}

		/// <summary>
		/// Change active status on both CRM and local (local only on CRM success)
		/// </summary>
		/// <param name="userEntity"></param>
		/// <returns></returns>
		public ActivateUserMessage ChangeActiveStatus(UserEntity userEntity)
		{
			userEntity.IsApproved = !userEntity.IsApproved;

			//if in a group, check group max users for a potential activation
			if (userEntity.IsApproved && userEntity.GroupId.HasValue && this.IsGroupMaxUsers(userEntity.GroupId.Value))
				return ActivateUserMessage.DeniedMaxUsers;

			if (UpdateUser(userEntity) == UpdateUserMessage.Success)
			{
				return ActivateUserMessage.Success;
			}

			return ActivateUserMessage.DeniedUnknownError;
		}

		/// <summary>
		/// Flip flag for RssSearchAlias in UserExt.
		/// </summary>
		/// <param name="userEntity"></param>
		public bool ChangeRssSearchAlias(UserEntity userEntity)
		{
			userEntity.RssSearchAliases = !userEntity.RssSearchAliases;

			if (UpdateUser(userEntity) == UpdateUserMessage.Success)
			{
				return true;
			}

			return false;
		}

		public IEnumerable<string> GetUserEmails(string likeEmail)
		{
			return this._userRepository.FindAll(m => m.LoweredUserName.Contains(likeEmail)).Select(m => m.LoweredUserName);
		}

		/// <summary>
		/// Generate a random password based off the length and complexity enforced by 
		/// the app settings.
		/// </summary>
		/// <returns></returns>
		public string GeneratePassword()
		{
			return Membership.GeneratePassword(_provider.MinRequiredPasswordLength,
											   _provider.MinRequiredNonAlphanumericCharacters);
		}

		/// <summary>
		/// Create a user access request.
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public MembershipCreateStatus CreateRequestAccess(RequestAccessViewModel model)
		{
			var userEntity = UserEntity.Map(model);
			userEntity.AccessGranted = false;

			MembershipCreateStatus status;
			_provider.CreateUser(userEntity.Email, GeneratePassword(), userEntity.Email, null, null, true, null, out status);
			//save all attributes
			if (status.Equals(MembershipCreateStatus.Success))
			{
				//note: do not create a user in crm

				SaveUserDetails(userEntity);
				//Email user their password here.
				SendRequestAccessEmails(userEntity);
			}

			return status;
		}

		/// <summary>
		/// Create a user in the Crm.
		/// </summary>
		/// <param name="model"></param>
		/// <param name="isAdmin"></param>
		/// <returns></returns>
		public MembershipCreateStatus CreateUser(UserViewModel model, bool isAdmin)
		{
			MembershipCreateStatus status;
			var password = GeneratePassword();
			_provider.CreateUser(model.Email, password, model.Email, null, null, true, null, out status);
			//save all attributes
			if (status.Equals(MembershipCreateStatus.Success))
			{
				var userEntity = UserEntity.Map(GetUserEntityByEmail(model.Email), model, isAdmin);
				userEntity.AccessGranted = true;

				//if not overridden by admin, set to inactive if in group and groups is at maxUsers
				if (model.GroupId.HasValue && IsGroupMaxUsers(model.GroupId.Value))
				{
					userEntity.IsApproved = false;
				}

				SaveUserDetails(userEntity);

				//Only send u/p email to user if they are Active
				if (userEntity.IsApproved)
					SendCreateUserEmails(userEntity, password);

				//create crmuser on a separate thread
				System.Threading.Tasks.Task.Factory.StartNew(() => SaveCrmUser(userEntity));
			}

			return status;
		}

		/// <summary>
		/// Get all local groups
		/// </summary>
		/// <returns></returns>
		public IEnumerable<AtlasDiligence.Common.Data.Models.Group> GetAllGroups()
		{
			return _groupRepository.GetAll();
		}

		/// <summary>
		/// Does the group already have its max active users (or more?)
		/// </summary>
		/// <param name="groupId"></param>
		/// <returns></returns>
		public bool IsGroupMaxUsers(Guid groupId)
		{
			var group = _groupRepository.GetById(groupId);
			var groupUsersCount = _userRepository.GetActiveUsers(groupId).Count();
			return groupUsersCount >= group.MaxUsers;
		}

		/// <summary>
		/// Returns true is a valid user and user is locked out
		/// </summary>
		/// <remarks>Differs from the private method in that this only checks whether or not the user is locked.
		/// The private method will also validate and (possibly) unlock.</remarks>
		/// <param name="userName"></param>
		/// <returns></returns>
		public bool IsLockedOut(string userName)
		{
			var user = _provider.GetUser(userName, false);
			return user != null ? user.IsLockedOut : false;
		}

		public IEnumerable<RecentlyViewedOrganizationsResult> GetRecentlyViewedOrganizations(Guid userId, int take)
		{
			return _userRepository.GetRecentlyViewedOrganizations(userId, take);
		}

		public void TrackUserLogin(Guid userId)
		{
			_userRepository.InsertUserLogin(userId);
		}

		public void TrackUserLogout(Guid userId)
		{
			_userRepository.InsertUserLogout(userId);
		}

		public void TrackOrganizationProfileView(Guid userId, Guid orgId)
		{
			_userRepository.InsertUserProfileView(userId, orgId);
		}

		public IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate)
		{
			return _userRepository.GetUserData(type, startDate, endDate);
		}

		/// <summary>
		/// Save additional user info
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public bool SaveCollectedInfo(CollectInfoViewModel model)
		{
			var userEntity = GetUserEntityByUserId(model.Id);

			if (userEntity != null)
			{
				try
				{
					//map collectedInfo
					userEntity.Title = model.Title;
					userEntity.Phone = model.Phone;
					userEntity.OrganizationName = model.OrganizationName;
					userEntity.OrganizationType = model.OrganizationType;
					userEntity.ReferredBy = model.ReferredBy;

					UpdateUser(userEntity);
					if (!Roles.IsUserInRole(userEntity.UserName, RoleNames.CollectedInfo))
					{
						Roles.AddUserToRole(userEntity.UserName, RoleNames.CollectedInfo);
					}

					return true;
				}
				catch (Exception)
				{
					return false;
				}
			}

			return false;
		}

		/// <summary>
		/// Change GrantAccess flag in local db.
		/// </summary>
		/// <param name="userEntity"></param>
		/// <returns></returns>
		public bool GrantAccessRequest(UserEntity userEntity)
		{
			try
			{
				userEntity.AccessGranted = true;
				userEntity.IsApproved = true;
				if (SaveUserDetails(userEntity))
				{
					SendCreateUserEmails(userEntity, _provider.ResetPassword(userEntity.UserName, ""));
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		/// <summary>
		/// Will Only deleted users where AccessGranted == false
		/// </summary>
		/// <param name="userEntity"></param>
		public void DeleteUser(UserEntity userEntity)
		{
			var dbUser = _userRepository.GetById(userEntity.Id);
			var crmService = new WebUserService();
			System.Threading.Tasks.Task.Factory.StartNew(() => crmService.Delete(dbUser.UserExt.CrmId));
			_userRepository.DeleteUser(dbUser);
			_userRepository.SubmitChanges();
		}

		/// <summary>
		/// Delete all RssEmails for user and replace with given emailList
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="emailList"></param>
		public void UpdateRssEmails(Guid userId, List<string> emailList)
		{
			_userRepository.UpdateRssEmails(userId, emailList);
		}

		public void RemoveRssEmail(Guid userId, string email)
		{
			_userRepository.RemoveRssEmail(userId, email);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		private static IEnumerable<string> GetAllRoles()
		{
			return Roles.GetAllRoles().AsEnumerable();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <param name="userName"></param>
		/// <param name="password"></param>
		private void SendResetPasswordEmailToUser(string emailAddress, string userName, string password)
		{
			//TODO: refactor all emails. use config or Resource values for strings
			var from = new MailAddress("no-reply@atlasdiligence.com");
			var to = new MailAddress(emailAddress);
			var mail = new MailMessage();
			mail.From = from;
			mail.To.Add(to);
			mail.Subject = "Atlas Diligence Password Reset";
			mail.IsBodyHtml = true;
			mail.Body = Resources.Templates.PasswordResetEmail.Replace("__USERNAME__", userName).Replace(
				"__PASSWORD__", password);
			var client = new SmtpClient();
			client.Send(mail);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userEntity"></param>
		private static void SendRequestAccessEmails(UserEntity userEntity)
		{
			var from = new MailAddress("no-reply@atlasdiligence.com");
			var to = new MailAddress(userEntity.Email);
			var mail = new MailMessage();
			mail.From = from;
			mail.To.Add(to);
			mail.Subject = "Atlas Diligence Request Access";
			mail.IsBodyHtml = true;
			mail.Body = Resources.Templates.RequestAccessEmail
				.Replace("__FIRSTNAME__", userEntity.FirstName)
				.Replace("__LASTNAME__", userEntity.LastName)
				.Replace("__EMAIL__", userEntity.Email);
			var client = new SmtpClient();
			client.Send(mail);
		}

		/// <summary>
		/// Send email (w/ password) to newly created user
		/// </summary>
		/// <param name="userEntity"></param>
		/// <param name="password"></param>
		private static void SendCreateUserEmails(UserEntity userEntity, string password)
		{
			var from = new MailAddress("no-reply@atlasdiligence.com");
			var to = new MailAddress(userEntity.Email);
			var mail = new MailMessage();
			mail.From = from;
			mail.To.Add(to);
			mail.Subject = "Atlas Diligence Access";
			mail.IsBodyHtml = true;
			mail.Body = Resources.Templates.CreateUserEmail
				.Replace("__FIRSTNAME__", userEntity.FirstName)
				.Replace("__LASTNAME__", userEntity.LastName)
				.Replace("__EMAIL__", userEntity.Email)
				.Replace("__PASSWORD__", password);
			var client = new SmtpClient();
			client.Send(mail);
		}

		/// <summary>
		/// Check to see if user is locked out. Includes Validate to check/reset the lockout timestamp
		/// </summary>
		/// <remarks>
		/// THIS METHOD SHOULD NOT BE MADE PUBLIC! USE THE PUBLIC METHOD IsLockedOut(email) for checking lockout status!!!
		/// </remarks>
		/// <param name="email"></param>
		/// <param name="password"></param>
		/// <returns>false if user is not locked out OR user is not valid</returns>
		private bool IsUserLockedOut(string email, string password)
		{
			//call ValidateUser to check (and possibly unlock) user.
			if (!_provider.ValidateUser(email, password))
			{
				MembershipUser mu = _provider.GetUser(email, false);
				return mu != null ? mu.IsLockedOut : false;
			}

			return false;
		}

		/// <summary>
		/// Save webUser and user details for given UserEntity.
		/// </summary>
		/// <remarks>assumes membership user is already created</remarks>
		/// <param name="userEntity"></param>
		private bool SaveUserDetails(UserEntity userEntity)
		{
			//Because username is used as identifier in Provider methods, update username before anything else
			var dbUser = _userRepository.GetById(userEntity.Id) ?? _userRepository.GetByEmail(userEntity.Email);
			if (dbUser != null)
			{
				try
				{
					dbUser.UserName = userEntity.Email;
					dbUser.LoweredUserName = userEntity.Email.ToLower();
					if (dbUser.UserExt == null)
					{
						dbUser.UserExt = new UserExt();
						dbUser.UserExt.UserId = dbUser.UserId;
					}
					dbUser.UserExt.CrmId = userEntity.CrmId;
					dbUser.UserExt.FirstName = userEntity.FirstName;
					dbUser.UserExt.LastName = userEntity.LastName;
					dbUser.UserExt.RssAllowed = userEntity.RssAllowed;
					dbUser.UserExt.RssNextEmailDate = userEntity.RssNextEmailDate;
					dbUser.UserExt.RssNextEmailOffsetEnum = userEntity.RssEmailFrequency.HasValue
																? (short)userEntity.RssEmailFrequency.Value
																: new short?();
					dbUser.UserExt.RssSearchAliases = userEntity.RssSearchAliases;
					dbUser.UserExt.DiligenceAllowed = userEntity.DiligenceAllowed;
					dbUser.UserExt.AccessGranted = userEntity.AccessGranted;
					dbUser.UserExt.Title = userEntity.Title;
					dbUser.UserExt.Phone = userEntity.Phone;
					dbUser.UserExt.OrganizationName = userEntity.OrganizationName;
					dbUser.UserExt.OrganizationType = (int)userEntity.OrganizationType;
					dbUser.UserExt.AUM = userEntity.AUM;
					dbUser.UserExt.ReferredBy = userEntity.ReferredBy;

					//remove any Group.GroupLeaderId relations for userEntity
					RemoveUserAsGroupLeader(dbUser.UserId);

					//add groupLeaderId 
					if (userEntity.GroupId.HasValue)
					{
						var group = _userRepository.GetGroupById(userEntity.GroupId.Value);
						dbUser.UserExt.Group = group;
						if (group != null && userEntity.IsGroupLeader)
							group.GroupLeaderId = userEntity.Id;
					}
					else
					{
						dbUser.UserExt.Group = null;
					}

					_userRepository.SubmitChanges();

					var membershipUser = _provider.GetUser(userEntity.Email, false);
					if (membershipUser != null)
					{
						membershipUser.Email = userEntity.Email;
						membershipUser.IsApproved = userEntity.IsApproved;
						_provider.UpdateUser(membershipUser);
					}

					//add&remove group Leader role
					if (!Roles.IsUserInRole(userEntity.UserName, RoleNames.GroupLeader) && userEntity.IsGroupLeader)
						Roles.AddUserToRole(userEntity.UserName, RoleNames.GroupLeader);
					if (Roles.IsUserInRole(userEntity.UserName, RoleNames.GroupLeader) && !userEntity.IsGroupLeader)
						Roles.RemoveUserFromRole(userEntity.UserName, RoleNames.GroupLeader);

					//add&remove admin roles
					if (!Roles.IsUserInRole(userEntity.UserName, RoleNames.Admin) && userEntity.IsAdmin)
						Roles.AddUserToRole(userEntity.UserName, RoleNames.Admin);
					if (Roles.IsUserInRole(userEntity.UserName, RoleNames.Admin) && !userEntity.IsAdmin)
						Roles.RemoveUserFromRole(userEntity.UserName, RoleNames.Admin);
					//add&remove employee roles
					if (!Roles.IsUserInRole(userEntity.UserName, RoleNames.Employee) && userEntity.IsEmployee)
						Roles.AddUserToRole(userEntity.UserName, RoleNames.Employee);
					if (Roles.IsUserInRole(userEntity.UserName, RoleNames.Employee) && !userEntity.IsEmployee)
						Roles.RemoveUserFromRole(userEntity.UserName, RoleNames.Employee);

					return true;
				}
				catch (Exception ex)
				{
					_log.Error("SaveUserDetails", ex);
					return false;
				}
			}
			return false;
		}

		/// <summary>
		/// Updtae Groups and remove given user as group leader
		/// </summary>
		/// <param name="userId"></param>
		private void RemoveUserAsGroupLeader(Guid userId)
		{
			var leaderGroups =
				_groupRepository.GetGroupsByGroupLeaderId(userId);
			foreach (var leaderGroup in leaderGroups)
			{
				leaderGroup.GroupLeaderId = null;
			}
			_groupRepository.SubmitChanges();
		}

		/// <summary>
		/// Create/Update crmUser and update local user with CrmId.
		/// </summary>
		/// <param name="userEntity"></param>
		private void SaveCrmUser(UserEntity userEntity)
		{
			try
			{
				Common.DTO.Model.Group crmGroup = null;
				WebUser crmUser = _crmWebUserService.GetById(userEntity.CrmId) ?? _crmWebUserService.GetByEmail(userEntity.Email);
				Guid? crmId = null;
				Guid? crmGroupId = null;

				if (userEntity.GroupId.HasValue)
				{
					var group = _groupRepository.GetById(userEntity.GroupId.Value);
					if (group != null)
					{
						crmGroup = _crmGroupService.GetById(group.CrmId);
						if (crmGroup != null)
							crmGroupId = crmGroup.Id;
					}
				}

				if (crmUser == null)
				{
					crmId = CreateCrmUser(userEntity, crmGroupId);
				}
				else
				{
					crmUser.DiligenceApp = userEntity.DiligenceAllowed;
					crmUser.Email = userEntity.Email;
					crmUser.FirstName = userEntity.FirstName;
					crmUser.GroupId = crmGroupId;
					crmUser.IsValidated = userEntity.IsApproved;
					crmUser.LastName = userEntity.LastName;
					crmUser.RssApp = userEntity.RssAllowed;

					_crmWebUserService.Update(crmUser);

					crmId = crmUser.Id;
				}

				if (userEntity.IsGroupLeader && crmGroup != null)
				{
					if (crmId.HasValue) _crmGroupService.RemoveUserAsGroupLeader(crmId.Value);
					crmGroup.GroupLeaderWebUserId = crmId;
					_crmGroupService.Update(crmGroup);
				}

				//update local UserExt.CrmId
				if (crmId.HasValue)
				{
					userEntity.CrmId = crmId.Value;
					SaveUserDetails(userEntity);
				}
			}
			catch (TimeoutException ex)
			{
				_log.Error(string.Format(Resources.ErrorMessages.TimeoutError, ex.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private Guid? CreateCrmUser(UserEntity userEntity, Guid? crmGroupId)
		{
			try
			{
				var crmUser = new WebUser
								  {
									  DiligenceApp = userEntity.DiligenceAllowed,
									  Email = userEntity.Email,
									  FirstName = userEntity.FirstName,
									  IsValidated = userEntity.IsApproved,
									  LastName = userEntity.LastName,
									  RssApp = userEntity.RssAllowed
								  };
				if (crmGroupId.HasValue)
					crmUser.GroupId = crmGroupId.Value;
				return _crmWebUserService.Create(crmUser);
			}
			catch (TimeoutException ex)
			{
				_log.Error(string.Format(Resources.ErrorMessages.TimeoutError, ex.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			return null;
		}

		/// <summary>
		/// Check to see if local system has a duplicate.
		/// </summary>
		/// <param name="userEntity"></param>
		/// <returns></returns>
		private bool HasDuplicate(UserEntity userEntity)
		{
			return _userRepository.HasDuplicate(userEntity.Id, userEntity.UserName);
		}

		public aspnet_User GetUserById(Guid userId)
		{
			return _userRepository.GetById(userId);
		}
	}
}
