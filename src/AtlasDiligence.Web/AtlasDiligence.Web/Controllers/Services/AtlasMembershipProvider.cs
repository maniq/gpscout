﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace AtlasDiligence.Web.Controllers.Services
{
    /// <summary>
    /// Uses SqlMembershipProvider, but ads lockout/unlock functionality
    /// </summary>
    public class AtlasMembershipProvider : SqlMembershipProvider
    {
        private int _autoUnlockTimeout = 60;  //Default to 60 minutes

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            string unlockTimeOut = config["autoUnlockTimeout"];
            if (!String.IsNullOrEmpty(unlockTimeOut))
                _autoUnlockTimeout = Int32.Parse(unlockTimeOut);
            config.Remove("autoUnlockTimeout");
            base.Initialize(name, config);
        }

        private bool AutoUnlockUser(string username)
        {
            MembershipUser mu = this.GetUser(username, false);
            if ((mu != null) &&
             (mu.IsLockedOut) &&
             (mu.LastLockoutDate.ToUniversalTime().AddMinutes(_autoUnlockTimeout) < DateTime.UtcNow))
            {
                bool retval = mu.UnlockUser();
                if (retval)
                    return true;
                else
                    return false; //something went wrong with the unlock
            }
            else
                return false; //not locked out in the first place
            //or still in lockout period
        }

        /// <summary>
        /// Validate given user. Also checks lock status against automatic unlock.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            bool retval = base.ValidateUser(username, password);
            //The account may be locked out at this point
            if (retval == false)
            {
                bool successfulUnlock = AutoUnlockUser(username);
                if (successfulUnlock)
                    //re-attempt the login
                    return base.ValidateUser(username, password);
                else
                    return false;
            }
            else
                return retval;  //first login was successful
        }

    }
}