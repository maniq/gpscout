﻿using System.Net.Mail;
using AtlasDiligence.Web.Models.ViewModels;

namespace AtlasDiligence.Web.Controllers.Services
{
    public class ContactService : IContactService
    {
        #region Implementation of IContactService

        public void SendContactEmail(ContactViewModel viewModel)
        {
            var from = new MailAddress("no-reply@atlasdiligence.com");
            var to = new MailAddress("alex@atlasdiligence.com");
            var mail = new MailMessage();
            mail.From = from;
            mail.To.Add(to);
            mail.Subject = "Atlas Diligence Contact";
            mail.IsBodyHtml = true;
            mail.Body = Resources.Templates.ContactEmail
                .Replace("__FIRSTNAME__", viewModel.FirstName)
                .Replace("__LASTNAME__", viewModel.LastName)
                .Replace("__ORGANIZATION__", viewModel.Organization)
                .Replace("__EMAIL__", viewModel.Email)
                .Replace("__QUESTIONS__", viewModel.Questions);
            var client = new SmtpClient();
            client.Send(mail);
        }

        #endregion
    }
}