﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize]
    public class DealLogController : ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search(int pageStart, string sortColumn, bool sortAscending, int rowsPerPage, string Search)
        {
            var group = UserEntity.Group;
            if (group == null)
            {
                throw new NullReferenceException("Group is not set");
            }
            if (rowsPerPage > 100) rowsPerPage = 100;
            var clientRequests = OrganizationService.GetClientRequestAndAssociatedOrganization(group.CrmId).OrderByDescending(m => m.Key.DateReceived);
            var count = clientRequests.Count();
            switch (sortColumn)
            {
                case "DateReceived":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.DateReceived)
                                         : clientRequests.OrderByDescending(m => m.Key.DateReceived);
                    break;
                case "FirmName":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.OrganizationName)
                                         : clientRequests.OrderByDescending(m => m.Key.OrganizationName);
                    break;
                case "FundName":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.FundName)
                                         : clientRequests.OrderByDescending(m => m.Key.FundName);
                    break;
                case "PlacementAgent":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.PlacementAgent)
                                         : clientRequests.OrderByDescending(m => m.Key.PlacementAgent);
                    break;
                case "MarketStage":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.MarketStage)
                                         : clientRequests.OrderByDescending(m => m.Key.MarketStage);
                    break;
                case "Strategies":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.StrategyPipeDelimited)
                                         : clientRequests.OrderByDescending(m => m.Key.StrategyPipeDelimited);
                    break;
                case "InvestmentRegions":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.InvestmentRegionPipeDelimited)
                                         : clientRequests.OrderByDescending(m => m.Key.InvestmentRegionPipeDelimited);
                    break;
                case "Source":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.Source)
                                         : clientRequests.OrderByDescending(m => m.Key.Source);
                    break;
                case "Status":
                    clientRequests = sortAscending
                                         ? clientRequests.OrderBy(m => m.Key.Status)
                                         : clientRequests.OrderByDescending(m => m.Key.Status);
                    break;
                default:
                    clientRequests = clientRequests.OrderByDescending(m => m.Key.DateReceived);
                    break;
            }
            var requests = clientRequests.Skip(pageStart).Take(rowsPerPage);
            var results = new GridList<DealLogDataGrid> { Total = count, Result = requests.Select(x => new DealLogDataGrid(x.Key, x.Value.PublishOverviewAndTeam)).ToArray() };

            return Json(results);
        }
    }
}
