﻿using System;
using System.Linq;
using System.Web.Mvc;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    public class AdminController : ControllerBase
    {
        public ActionResult ReIndex()
        {
            new IndexService().ReIndex();
            return Content("Re-indexed");
        }

        public ActionResult Index()
        {
            var model = new AdminViewModel() { UserEntity = UserEntity };
            return View(model);
        }

        public ActionResult UpdateEula()
        {
            var viewModel = new EulaViewModel();
            viewModel.Build(EulaRepository);
            return this.View(viewModel);
        }

        [HttpPost]
        public ActionResult UpdateEula(EulaViewModel viewModel)
        {
            if (String.IsNullOrWhiteSpace(viewModel.Text))
            {
                ModelState.AddModelError("Text", "You must enter at least 1 character.");
            }
            if (ModelState.IsValid)
            {
                EulaRepository.InsertEula(new Eula
                    {
                        Id = Guid.NewGuid(),
                        Text = viewModel.Text
                    });
                viewModel.Build(EulaRepository);
            }
            return this.View(viewModel);
        }

        public ExcelResult ExportUserSearch()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Search Term" },
                DeleteFile = true,
                FileName = "UserSearchTerms.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.SearchTerms != null && m.SearchTerms.Count() > 0))
            {
                rows.AddRange(user.SearchTerms.Select(searchTerm => new Dictionary<string, string>
                                                                        {
                                                                            {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                                            {"Email", user.aspnet_Membership.LoweredEmail},
                                                                            {"Search Term", searchTerm.SearchTerm}
                                                                        }));
            }
            retval.Rows = rows;
            return retval;
        }

        public ExcelResult ExportUserInformation()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Phone", "Title", "Organization Name", "Organization Type", "Referred By" },
                DeleteFile = true,
                FileName = "UserInformation.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.UserExt != null))
            {
                rows.Add(new Dictionary<string, string>{
                                                        {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                        {"Email", user.aspnet_Membership.LoweredEmail},
                                                        {"Phone", user.UserExt.Phone},
                                                        {"Title",  user.UserExt.Title},
                                                        {"Organization Name",  user.UserExt.OrganizationName},
                                                        {"Organization Type",  user.UserExt.OrganizationType.HasValue && user.UserExt.OrganizationType.Value != 0 ? ((OrganizationType) user.UserExt.OrganizationType).GetDescription() : string.Empty},
                                                        {"Referred By",  user.UserExt.ReferredBy},
                                                    });
            }
            retval.Rows = rows;
            return retval;
        }
    }
}
