/*

BASE Functions

==================================================================================================================*/

//jQuery Load
Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
function daysBetween(first, second) {
    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = two.getTime() - one.getTime();
    var days = millisBetween / millisecondsPerDay;

    return Math.floor(days);
}
$.fn.createDateRange = function (options) {
    if (!options.endDate) return;
    var now = new Date();
    var endDate = $(options.endDate);
    var thisStartDate = new Date(this.val());
    var thisDateDifference = daysBetween(now, thisStartDate);
    endDate.datepicker({ showAnim: 'fadeIn', minDate: thisDateDifference });
    this.datepicker({
        showAnim: 'fadeIn',
        onSelect: function (dateText, inst) {
            var startDate = new Date(dateText);
            var dateDifference = daysBetween(now, startDate);
            endDate.datepicker('option', 'minDate', dateDifference);
        }
    });
};

var getQueryString = function () {
    var queryString = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof queryString[pair[0]] === "undefined") {
            queryString[pair[0]] = pair[1];
            // If second entry with this name
        } else if (typeof queryString[pair[0]] === "string") {
            var arr = [queryString[pair[0]], pair[1]];
            queryString[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            queryString[pair[0]].push(pair[1]);
        }
    }
    return queryString;
}();
$(function () {
    $.fn.separateCommas = function () {
        return this.each(function () {
            if (!isNaN($(this).val()) && $(this).val() != "")
                $(this).val($(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        });
    };
    //set UI based on class or id
    $('body').on('mouseover', ':not(#map-canvas [title])[title]',
        function (event) {
            // Bind the qTip within the event handler
            $(this).qtip({
                overwrite: false, // Make sure the tooltip won't be overridden once created
                show: {
                    event: event.type, // Use the same show event as the one that triggered the event handler
                    ready: true // Show the tooltip as soon as it's bound, vital so it shows up the first time you hover!
                }
            }, event); // Pass through our original event to qTip

        });
    $('.date-picker').datepicker();
    $('.multiselect').multiselect();
	
	// date pickers
    $('.date-picker-rss-start-date').datepicker({ minDate: new Date("November 1, 2011") });
    $('.date-picker-tracker-start-date').datepicker({
    	beforeShow: function () {
    		$(this).datepicker('option', 'maxDate', $('.date-picker-tracker-end-date').val());
    	}
    });
    $('.date-picker-tracker-end-date').datepicker({
    	beforeShow: function () {
    		$(this).datepicker('option', 'minDate', $('.date-picker-tracker-start-date').val());
    	}
    });

    $('.grid-search').hide();
    $('.tablesorter').tablesorter({
        cssHeader: 'tsHeader',
        cssAsc: 'tsHeaderSortUp',
        cssDesc: 'tsHeaderSortDown',
        textExtraction: function (node) { return $(node).text(); }
    });
    $('.tabs').tabs();
    $('#admin-tabs').tabs();
    $('select.chosen').chosen({ allow_single_deselect: true });
    if (getQueryString.lt !== undefined) {
        var param = getQueryString.lt;
        $('#admin-tabs').tabs('select', new Number(param));
    }
    $('.comma-separated').separateCommas();
    $('.comma-separated').live('blur', function () {
        $('.comma-separated').separateCommas();
    });
    $('.advanced-search input:submit').click(function () {
        $('.comma-separated').each(function () {
            $(this).val($(this).val().replace(',', ''));
        });
    });
    //    $('.comma-separated').live('focus', function () {
    //        $(this).val($(this).val().replace(',', ''));
    //    });
    //add validation box to .error if there is content. Done with js to prevent an empty box when there is not an error message
    $('.alert').removeClass('alert-active');
    $('.alert').each(function () {
        if ($(this).html().length > 0)
            $(this).addClass('alert-active');
    });

    $('.alert.alert-active').delay(3000).fadeOut(1000);

    //grid search
    $('.toggle-grid-search').live("click", function (e, ui) {
        $('.grid-search').toggle();
    });

    //custom alert
    customAlert = function (txt, alertType, title) {
        var modalDiv = $(document.createElement('div')),
            alertClass = alertType != undefined ? alertType : 'Error';

        var alertImg; // = '<span class="alert ' + alertClass + '"></span>';
        if (alertType == 'Information') {

            alertImg = '';
        } else {

            alertImg = '<span class="alert ' + alertClass + '"></span>';
        }
        var newTitle = title ? title : alertImg + alertClass + " ";
        modalDiv.html(txt)
            .appendTo(document.body)
            .dialog({
                autoOpen: true,
                width: 300,
                height: 250,
                buttons: { "Close": function () { $(this).dialog("destroy").remove(); } },
                close: function () { $(this).dialog("destroy").remove(); },
                title: newTitle
            });
    };

    //generic Help dialog. 
    $('.help').live("click", function (e, ui) {
        var helpText = $(".help-content").html();
        if (helpText.length == 0)
            helpText = 'TODO: Add Help Here.';
        $('<div>' + helpText + '</div>').dialog({
            title: 'Help',
            width: '660px'
        });
    });

    //YuiDataTable CellClickEvent function
    $.fn.cellClickEvent = function (e) {
        var dt = this.get(0);
        var column = dt.getColumn(e.target);
        if (column.action == 'delete') {
            if (confirm('Are you sure?')) {
                var url = $(e.target).find('.delete').attr('rel');
                $.getJSON(url, function (msg) {
                    if (msg.Status == "Success") {
                        dt.showTableMessage("Loading...");
                        dt.getDataSource().sendRequest(dt.configs.generateRequest(null, dt), dt.onDataReturnInitializeTable, dt);
                    } else {
                        customAlert(msg.Message, 'Error');
                    }
                });
            }
        } else {
            dt.onEventShowCellEditor(e);
        }
    };

    $.fn.refresh = function (e) {
        var dt = this.data('dataTable');
        dt.showTableMessage("Loading...");
        dt.getDataSource().sendRequest(dt.configs.generateRequest(null, dt), dt.onDataReturnInitializeTable, dt);
    };

    /* user grid functions */
    $.fn.unlockUser = function (url) {
        var caller = this;
        var td = $(this).parent();
        $(caller).removeClass('locked').addClass('small-loader');
        $.getJSON(url, { 'timestamp': new Date().getTime() }, function (msg) {
            if (msg.Status == "Success") {
                $(caller).remove();
                $(td).append('<span class="grid-row-action unlocked">Unlocked</span>');
            } else {
                $(caller).addClass('locked').removeClass('small-loader');
            }
            customAlert(msg.Message, msg.Status);
        });
    };

    $.fn.resetPassword = function (url) {
        var caller = this;
        $(caller).removeClass('reset-password').addClass('small-loader');

        $('#reset-password-dialog').dialog({
            autoOpen: true,
            width: 400,
            height: 250,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    $('.ui-dialog-buttonset').addClass('small-loader');
                    $('.ui-button').hide();
                    $.getJSON(url, { 'timestamp': new Date().getTime(), 'password': $('#reset-password-dialog > #password').val() },
                        function (msg) {
                            $('.ui-dialog-buttonset').removeClass('small-loader');
                            $('.ui-button').show();
                            if (msg.Status == "Success") {
                                $('#reset-password-dialog > .error').removeClass('error-active').html('');
                                $(caller).addClass('reset-password').removeClass('small-loader');
                                $(thisDialog).dialog("destroy");
                            } else {
                                $('#reset-password-dialog > .error').addClass('error-active').html(msg.Message);
                            }
                        });
                }
            },
            open: function () {
                $('#reset-password-dialog > .error').removeClass('error-active').html('');
                $('#reset-password-dialog > #password').val('');
            },
            close: function () {
                $(this).dialog("destroy");
                $(caller).addClass('reset-password').removeClass('small-loader');
            },
            title: "Reset Password"
        });
    };

    $.fn.changeActiveStatus = function (id) {
        var caller = this;
        var currentlyActive = $(caller).hasClass('active');
        $(caller).removeClass('active').removeClass('inactive').addClass('small-loader');
        $.getJSON($(caller).attr('href') + '/' + id, { 'timestamp': new Date().getTime() }, function (msg) {
            if (msg.Status == "Failure") {
                customAlert(msg.Message, msg.Status);
                $(caller).addClass(currentlyActive ? 'active' : 'inactive');
            } else {
                $(caller).addClass(currentlyActive ? 'inactive' : 'active').html(currentlyActive ? 'Inactive' : 'Active');
            }
            $(caller).removeClass('small-loader');
        });
    };

    $.fn.grantAccessRequest = function (id) {
        var caller = this;
        $(caller).removeClass('request-access').addClass('small-loader');
        $.getJSON($(caller).attr('href') + '/' + id, { 'timestamp': new Date().getTime() }, function (msg) {
            if (msg.Status == "Failure") {
                customAlert(msg.Message, msg.Status);
                $(caller).addClass('request-access');
            } else {
                window.location.href = msg.Data.RedirectUrl;
            }
            $(caller).removeClass('small-loader');
        });
    };
    /* end user grid functions */

    //generic call to replace buttons div of current form with loading animation or return to before-loading state.
    $.fn.loadingAnimation = function () {
        var buttons = $(this).find('.buttons');
        $(buttons).each(function (e) {
            $(this).children().hide();
            $(this).append('<span class=\"small-loader\"></span');
        });
    };

    //search grid folder widget call (populates folder-widget-listitems for attached folder-widget-list)
    $.fn.createFolderList = function (orgId, folderIds) {
        var folderWidget = this;
        var folderWidgetList = $(this).siblings('.folder-widget-list');

        //add list if it does not already exist in cell
        if ($(folderWidgetList).length == 0) {
            var folderList = $('#folder-widget-container .folder-widget-list').clone();
            $('input.organization-id', folderList).val(orgId);
            folderList.last().find('a').attr('rel', orgId);
            $(folderWidget).parent().append(folderList);
            folderWidgetList = $(folderWidget).siblings('.folder-widget-list');
            $("input.folder-id", folderWidgetList).each(function () {
                if ($.inArray($(this).val(), folderIds) > -1)
                    $(this).addClass('folder-widget-selected');
            });
        }
        //slide up/down
        //current
        if ($(folderWidgetList).hasClass('hide-widget')) {
            $(folderWidgetList).slideToggle('fast').removeClass('hide-widget').addClass('show-widget').addClass('current');
        } else {
            $(folderWidgetList).slideToggle('fast').removeClass('show-widget').addClass('hide-widget').removeClass('current');
        }
        //hide all but current
        $('.folder-widget-list').not(folderWidgetList).each(function () {

            $(this).slideUp('fast').removeClass('show-widget').addClass('hide-widget');

        });
    };

    //helper function to set <ul> or <ol> line items into columns
    $.fn.autoColumn = function (maxColumns, maxPerColumn, containerSelector) {
        $(this).detach();
        var lis = $(this).find("li");
        var total = lis.length;
        var columns = total / maxPerColumn;
        columns = columns > maxColumns ? maxColumns : Math.ceil(columns);
        var totalPerColumn = total / columns;
        var extraLeftover = total % columns;
        var extraUsed = 0;
        var $container = $(containerSelector);
        for (var i = 0; i < columns; i++) {
            var extra = extraLeftover > 0 ? 1 : 0;
            $container.append("<ul class='auto-column " + (i + 1) + "-col'></ul>");
            $container.find("ul." + (i + 1) + "-col").append(lis.slice((Math.floor(totalPerColumn) * i) + extraUsed, (Math.floor(totalPerColumn) * (i + 1)) + extra + extraUsed));
            extraUsed = extra > 0 ? extraUsed + 1 : extraUsed;
            extraLeftover--;
        }
    };

    $.fn.replaceFolderWidget = function (newHtml) {
        this.each(function () {
            var $this = $(this);
            var orgId = $this.find('.organization-id:first').val();
            var replaceHtml = $(newHtml);
            if (orgId !== undefined) {
                replaceHtml.find('.folder-widget-title').each(function () {
                    $(this).addClass('search-grid-widget');
                    $(this).attr('onClick', "$(this).createFolderList('" + orgId + "', [])");
                });
                replaceHtml.find('.organization-id').each(function () {
                    $(this).val(orgId);
                });
                $this.replaceWith(replaceHtml.wrap('<div></div>').parent().html());
            }
        });
    };

    //Rss Schedule Email Ajax
    $("#schedule-email-submit").live("click", function (e, ui) {
        if ($("#subscribe-list").children("option:selected").val() == "False") {
            $(".time-picker").attr("value", "01:00 PM");
            $(".date-picker").attr("value", Date.today().add(1).days().toString("MM/dd/yyyy"));
        }
        var thisForm = $("#schedule-form");
        thisForm.loadingAnimation();
        $.post(thisForm.attr('action'),
            thisForm.serialize(),
            function (model) {
                _gaq.push(['_trackPageview', '/schedule/save']);
                $("#schedule-email-submit").closest(".schedule-tab").replaceWith(model);
            });
        e.preventDefault();
    });
    $("#subscribe-list").live("change", function () {
        if ($("#subscribe-list").children("option:selected").val() == "True") {
            $(".subscribe-yes").show();
            $(".subscribe-no").hide();
        } else {
            $(".subscribe-no").show();
            $(".subscribe-yes").hide();
        }
    });

    /* Recent Searches toggle */
    $('#recent-searches > ul').hide();
    $('#recent-searches h4').click(function () {
        $(this).toggleClass('open').next().slideToggle(250);
    });
    $('.save-search').click(function () {
        var caller = $(this);
        $('#save-search-name').val('');
        $('#save-search-dialog').dialog({
            width: 500,
            height: 200,
            title: 'Save your search',
            close: function () {
                $(this).dialog("destroy");
            },
            buttons: {
                'Submit': function () {
                    var dialog = $(this);
                    var deferred = $.getJSON(caller.attr('href'), { name: $('#save-search-name').val() });
                    deferred.fail(function () {
                        customAlert('An error has occurred');
                    });
                    deferred.done(function (result) {
                        if (result.Status != 'Success') {
                            customAlert(result.Message);
                            dialog.dialog('destroy');
                            return;
                        }
                        customAlert("Your search has been saved.", 'Information');
                        dialog.dialog('destroy');
                        return;
                    });
                }
            }
        });
        return false;
    });
    $('.edit-search').click(function () {
        var caller = $(this);
        var container = $(this).closest('tr');
        var name = container.find('.saved-search-name');
        $('#rename-search-name').val(name.text());
        $('#rename-search-dialog').dialog({
            width: 500,
            height: 200,
            title: 'Rename your search',
            close: function () {
                $(this).dialog("destroy");
            },
            buttons: {
                'Submit': function () {
                    var dialog = $(this);
                    var deferred = $.getJSON(caller.attr('href'), { name: $('#rename-search-name').val() });
                    deferred.fail(function () {
                        customAlert('An error has occurred');
                    });
                    deferred.done(function (result) {
                        if (result.Status != 'Success') {
                            customAlert(result.Message);
                            dialog.dialog('destroy');
                            return;
                        }
                        name.text($('#rename-search-name').val());
                        customAlert("Your search has been renamed.", 'Information');
                        dialog.dialog('destroy');
                        return;
                    });
                }
            }
        });
        return false;
    });
    $('.delete-search').click(function () {
        var caller = $(this);
        var container = $(this).closest('tr');
        var name = container.find('.saved-search-name');

        yesNoCustomAlert("Are you sure you want to delete this search?", "Delete", function (item) {
            $.getJSON(caller.attr('href'), null, function (result) {
                if (result.Status == "Success") {
                    item.hide();
                    customAlert("This saved search has been deleted.", 'Information');
                } else {
                    customAlert("There was an error deleting this note.");
                }
            });
        }, container);
        return false;
    });
    /*Rss terms, preview, and export */
    $("#rss-export-submit").live("click", function () {
        _gaq.push(['_trackPageview', '/news/export']);
        $("#RssViewType").attr("value", 1);
    });
    $("#rss-preview-submit").live("click", function () {
        _gaq.push(['_trackPageview', '/news/preview']);
        $("#RssViewType").attr("value", 2);
    });
    $("#rss-print-submit").live("click", function () {
        _gaq.push(['_trackPageview', '/news/print']);
        printThis();
    });
    $(".remove-search-term").live("click", function () {
        var element = $(this);
        var term = element.siblings("span").html();
        $.ajax({
            type: 'POST',
            url: element.attr('href'),
            dataType: 'json',
            data: { "term": term },
            success: function () {
                _gaq.push(['_trackPageview', '/news/remove']);
                _gaq.push(['_trackEvent', 'Rss', 'SearchTermRemove', term]);
                element.parent().fadeOut('fast', function () {
                    element.parent().remove();
                });
            }
        });
        return false;
    });
    $("div.append-search-term > .button").live("click", function () {
        _gaq.push(['_trackPageview', '/news/add']);
        var searchTerms = $.map($("#rss-search-term").val().split(",").clean("").clean(undefined).clean("\n"), $.trim);
        //track each searchTerm in textarea
        $.each(searchTerms, function (idx, val) {
            _gaq.push(['_trackEvent', 'Rss', 'SearchTermAdd', val]);
        });
        $("#search-terms-container").find("li").each(function () {
            searchTerms.push($(this).children("span").html());
        });
        var postData = { terms: searchTerms };
        $.ajax({
            url: $("#rss-search-term").attr('rel'),
            type: 'POST',
            dataType: 'json',
            data: postData,
            traditional: true,
            success: function (result) {
                $("#search-terms-container").html("<ul id=\"rss-search-list\"></ul>");
                $.map(result.Data, function (val, i) {
                    $("#rss-search-list").append(
                        "<li><span>" +
                            $.trim(val) +
                                "</span><a href='/NewsTracker/RemoveSearchTerm' class='remove-search-term'></a>" +
                                    "<input type='hidden' name='searchTerms' value='" + $.trim(val) + "'/>" +
                                        "</li>");
                });
                $("#rss-search-list").autoColumn(3, 10, "#search-terms-container");
            }
        });
        $("#rss-search-term").attr("value", "");
        return false;
    });
    $("#rss-search-term").live("keypress", function (event) {
        if (event.which == 13 || event.keyCode == 13) {
            $("div.append-search-term > .button").click();
            $(this).attr("value", "");
        }
    });
    $("#rss-search-list").autoColumn(3, 10, "#search-terms-container");
    $(".toggle-search-container").find("a").click(function () {
        var text = $(this).html();
        if (text === "Show Search Terms") {
            $(this).html("Hide Search Terms");
            $("#search-terms-container").slideDown();
        } else {
            $(this).html("Show Search Terms");
            $("#search-terms-container").slideUp();
        }
    });
    /*Rss Export searchAliases selection*/
    $("#SearchAliases").change(function (e, ui) {
        var caller = this;
        $.getJSON($(caller).attr('rel'), { 'timestamp': new Date().getTime() }, function (msg) {
            if (msg.Status != "Success") {
                customAlert(msg.Message, msg.Status);
            }
        });
    });
    /*end rss terms preview and export */

    /*Rss scheduling*/
    $(".remove-rss-email").live("click", function () {
        var element = $(this);
        var emailAddress = element.siblings("span").html();
        $.post($(element).attr('href'), { "email": emailAddress }, function () {
            _gaq.push(['_trackPageview', '/schedule/emailremove']);
            _gaq.push(['_trackEvent', 'Rss', 'EmailRemove', emailAddress]);
            element.parent().fadeOut('fast', function () {
                element.parent().remove();
            });
        }
        );
        return false;
    });
    $("div.append-rss-email > .button").live("click", function () {
        var emailList = $.map($("#rss-emails").val().split(",").clean("").clean(undefined).clean("\n"), $.trim);
        _gaq.push(['_trackPageview', '/schedule/emailadd']);
        //track each searchTerm in textarea
        $.each(emailList, function (idx, val) {
            _gaq.push(['_trackEvent', 'Rss', 'EmailAdd', val]);
        });

        $("#rss-email-container").find("li").each(function () {
            emailList.push($(this).children("span").html());
        });
        var postData = { emails: emailList };
        $.ajax({
            url: $("#rss-emails").attr('rel'),
            type: 'POST',
            dataType: 'json',
            data: postData,
            traditional: true,
            success: function (result) {
                $("#rss-email-container").html("<ul id=\"rss-email-list\"></ul>");
                $.map(result.Data, function (val, i) {
                    $("#rss-email-list").append(
                        "<li><span>" +
                            $.trim(val) +
                                "</span><a href='/NewsTracker/RemoveRssEmail' class='remove-rss-email'></a>" +
                                    "<input type='hidden' name='emails' value='" + $.trim(val) + "'/>" +
                                        "</li>");
                });
                $("#rss-email-list").autoColumn(3, 10, "#rss-email-container");
            }
        });
        $("#rss-emails").attr("value", "");
        return false;
    });
    $("#rss-emails").live("keypress", function (event) {
        if (event.which == 13 || event.keyCode == 13) {
            $("div.append-rss-email > .button").click();
            $(this).attr("value", "");
        }
    });

    $(".toggle-email-container").find("a").click(function () {
        var text = $(this).html();
        if (text === "Show Additional Emails") {
            $(this).html("Hide Additional Emails");
            $("#rss-email-container").slideDown();
        } else {
            $(this).html("Show Additional Emails");
            $("#rss-email-container").slideUp();
        }
    });
    /*end rss scheduling*/

    /*Organization tabs */
    $("tr.fund a").live("click", function () {
        $("#fund-tab").hide();
        $("#" + $(this).attr("id").replace("-fund", "-container")).show();
        window.scrollTo(0, 0);
    });
    $(".back-to-fund-list").live("click", function () {
        $(this).closest(".tab-container").hide();
        $("#fund-tab").show();
        window.scrollTo(0, 0);
    });
    $(".member a").live("click", function () {
        $("#team-tab").hide();
        $("#" + $(this).attr("id").replace("-member", "-container")).show();
        window.scrollTo(0, 0);
    });
    $(".back-to-team-list").live("click", function () {
        $(this).closest(".tab-container").hide();
        $("#team-tab").show();
        window.scrollTo(0, 0);
    });
    $('.open-request-dialog').click(function () {
        $('#request-dialog').dialog();
    });
    $('.other-office-toggle').live('click', function () {
        $(this).toggleClass('expanded-offices');
        $('.other-office').each(function () {
            $(this).slideToggle('fast');
        });
        return false;
    });
    $('.analysis-slider').live('click', function () {
        $(this).toggleClass('expanded');
        var next = $(this).closest('tr').next('tr');
        if (next.hasClass('analysis-graph')) {
            next.slideToggle();
        }
    });
    $('#chart-options input[type="submit"]').live('click', function () {
        var form = $(this).closest('form');
        $('.scatter-container #loading-large-scatter').show();
        $.getJSON(form.attr('action'), form.serialize(), function (result) {
            $('.scatter-chart').html(result.RenderView);
        });
        return false;
    });
    /*end organization tabs*/

    /* notes widget */
    $(".add-note a").live("click", function () {

        var dialogWidth = Math.ceil(window.innerWidth / 3);
        if (dialogWidth < 480) dialogWidth = 480;

        $("#note-dialog").dialog({
            autoOpen: true,
            width: dialogWidth,
            height: 290,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    var noteText = $('#txt-note').val();
                    if (noteText.match("\n$")) {
                        noteText = noteText.substring(0, noteText.length - 1);
                    }
                    $.ajax({
                        type: 'post',
                        url: $('#note-dialog-url').val(),
                        data: {
                            'text': noteText,
                            'organizationId': $('#note-dialog-orgid').val(),
                            'isGroup': $('#chk-is-group').is(':checked')
                        },
                        traditional: true,
                        dataType: 'json',
                        success:
                            function (data) {
                                if (data.Status == "Success") {
                                    $(".notes").closest('.section').replaceWith(data.RenderView);
                                } else {
                                    customAlert("There was an error creating this note.");
                                }
                                $('#txt-note').val("");
                                $(thisDialog).dialog("destroy");
                            }
                    });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Create a new note"
        });
        return false;
    });
    $('.delete-note').live('click', function () {
        var thisNoteId = $(this).data('id');
        yesNoCustomAlert("Are you sure you want to delete this note?", "Delete", function (item) {
            var data = {
                id: thisNoteId
            };
            $.getJSON('/Note/Delete', data, function (result) {
                if (result.Status == "Success") {
                    item.fadeOut('fast');
                } else {
                    customAlert("There was an error deleting this note.");
                }
            });
        }, $(this).closest('.note-container'));
    });

    $('.modify-note').live('click', function () {
        var dialogWidth = Math.ceil(window.innerWidth / 3);
        if (dialogWidth < 480) dialogWidth = 480;
        var thisNoteId = $(this).data('id');
        var thisNote = $(this).closest('.note-container').find('.note');
        var modifyDialog = $('#modify-note-dialog');
        var note = thisNote.html().replace(/<br\/>/g, "\n").replace(/<br>/g, "\n");
        modifyDialog.find('textarea').val(note);
        modifyDialog.dialog({
            autoOpen: true,
            width: dialogWidth,
            height: 270,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    var noteText = modifyDialog.find('textarea').val();
                    if (noteText.match("\n$")) {
                        noteText = noteText.substring(0, noteText.length - 1);
                    }
                    $.ajax({
                        type: 'post',
                        traditional: true,
                        dataType: 'json',
                        url: "/Note/Modify",
                        data: {
                            'text': noteText,
                            'noteId': thisNoteId
                        },
                        success:
                            function (result) {
                                if (result.Status == "Success") {
                                    thisNote.html(noteText.replace(/\n/g, "<br>"));
                                }
                                $(thisDialog).dialog("destroy");
                            }
                    });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Modify"
        });
    });
    /* end notes widget */

    /* folder widget */
    $(".folder-widget").live({
        mouseenter: function () {
            if (!$(".folder-widget-title", this).hasClass('search-grid-widget') && !$(this).is('.selected-folder-widget'))
                $(".folder-widget-title", this).siblings(".folder-widget-list").show();
        },
        mouseleave: function () {
            if (!$(".folder-widget-title", this).hasClass('search-grid-widget') && !$(this).is('.selected-folder-widget'))
                $(".folder-widget-title", this).siblings(".folder-widget-list").hide();
        }
    });

    $('.create-folder', '.folder-dialog').click(function () {
        if ($(this).closest('.folder-widget').attr('id') == 'selected-folder-widget')
            return null;
        $.ajax({
            url: $('#folder-dialog-url').val(),
            type: 'POST',
            dataType: 'json',
            data: {
                name: $('.text', '.folder-dialog').val(),
                organizationId: $('#folder-dialog-orgid').val()
            },
            traditional: true,
            success: function (result) {
                $('#folder-widget').replaceWith(result.RenderView);
                $('.folder-dialog').dialog('close');
            }
        });
        return false;
    });
    $(".folder-widget-listitem").live("click", function () {
        if ($(this).closest('.folder-widget').hasClass('selected-folder-widget'))
            return null;
        if ($(this).hasClass('new-folder')) {
            var $anchor = $(this).find('a');
            var dialogWidth = Math.ceil(window.innerWidth / 3);
            if (dialogWidth < 480) dialogWidth = 480;
            $('#folder-dialog').dialog({
                autoOpen: true,
                width: dialogWidth,
                height: 230,
                buttons: {
                    "Submit": function () {
                        var thisDialog = $(this);
                        $.getJSON($anchor.attr('href'),
                            { 'name': $('#folder-dialog input').val(), 'organizationId': $anchor.attr('rel') },
                            function (data) {
                                customAlert('The organizations were successfully added', 'Information');
                                $('.folder-widget').not('#selected-folder-widget').replaceFolderWidget(data.RenderView);
                                $('#selected-folder-widget').replaceWith(data.SelectedRenderView);
                                $(thisDialog).dialog("destroy");
                            });
                    }
                },
                close: function () {
                    $(this).dialog("destroy");
                },
                title: "Create a new folder"
            });
        } else {
            var listItem = $(this);
            var dataVal = {
                "organizationId": $('.organization-id', this).val(),
                "folderId": $('.folder-id', this).val()
            };
            $.ajax({
                url: $('a', this).attr('href'),
                type: 'POST',
                dataType: 'json',
                data: dataVal,
                traditional: true,
                success: function (result) {
                    $(listItem).parent(".folder-widget-list").slideToggle(400);
                    $(listItem).addClass('folder-widget-selected');
                    customAlert('The organizations were successfully added', 'Information');
                }
            });
        }
        return false;
    });

    /* end folder widget */

    /* folders */
    $(".folder-toggle").live('click', function () {
        $(this).find('a').toggleClass('folder-closed').toggleClass('folder-open');
        $(this).siblings('div.organization-list').slideToggle();

        $.ajax({
            url: '/folder/registeruserclick',
            type: 'POST',
            dataType: 'json',
            data: { "folderId": $(this).find('a').attr('rel') },
            traditional: true
        });
    });
    yesNoCustomAlert = function (txt, dialogTitle, callback, element) {
        var modalDiv = $(document.createElement('div'));
        modalDiv.html(txt)
            .appendTo(document.body)
            .dialog({
                autoOpen: true,
                width: 300,
                height: 250,
                buttons: {
                    "Yes": function () {
                        callback(element);
                        $(this).dialog("destroy").remove();
                    },
                    "No": function () {
                        $(this).dialog("destroy").remove();
                    }
                },
                close: function () { $(this).dialog("destroy").remove(); },
                title: dialogTitle
            });
    };
    $("a.delete-folder").live("click", function () {
        var $this = $(this);
        yesNoCustomAlert("Are you sure you want to delete this folder? This action cannot be undone.", "Confirm",
            function (element) {
                var dataVal = { "folderId": element.attr('rel') };
                $.ajax({
                    url: element.attr('href'),
                    type: 'POST',
                    dataType: 'json',
                    data: dataVal,
                    traditional: true,
                    success: function (result) {
                        if (result.Status === "Success") {
                            element.closest('li').fadeOut('slow');
                        }
                    }
                });
            }, $this);
        return false;
    });
    $('a.rename-folder').live("click", function (e, ui) {
        var caller = $(this);
        var input = $('#rename-folder-dialog input');
        input.val('');
        $('#rename-folder-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        { 'folderId': $(caller).attr('rel'), 'name': input.val(), 'timestamp': new Date().getTime() },
                        function (result) {
                            if (result.Status == "Success") {
                                caller.closest('li').find('.folder-open, .folder-closed').html(input.val());
                            }
                            $(thisDialog).dialog("destroy");
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Rename Folder"
        });
        return false;
    });

    $("a.create-folder").live("click", function () {
        var $this = $(this);
        var dialogWidth = Math.ceil(window.innerWidth / 3);
        if (dialogWidth < 460) dialogWidth = 460;
        $('.dialog').dialog({
            width: 500,
            buttons: {
                "Submit": function () {
                    var $input = $('.dialog input');
                    if ($input.val().length == 0) return false;
                    var dataVal = { "name": $input.val() };
                    $.ajax({
                        url: $this.attr('href'),
                        type: 'POST',
                        dataType: 'json',
                        data: dataVal,
                        traditional: true,
                        success: function (result) {
                            if (result.Status === "Success") {
                                $("#main-area .wrap").html(result.RenderView);
                                $('.dialog').dialog('close');
                                return false;
                            }
                        }
                    });
                }
            }
        });
        return false;
    });
    $(".remove-organization").live('click', function () {
        var $this = $(this);
        var dataVal = {
            "organizationId": $(this).siblings('.organization-id').val(),
            "folderId": $(this).closest('li').find('h3').find('a').attr('rel')
        };
        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType: 'json',
            data: dataVal,
            traditional: true,
            success: function (result) {
                $this.closest('tr').fadeOut('slow', function () {
                    $this.closest('tr').remove();
                });
                return false;
            }
        });
        return false;
    });

    $('a.add-org-to-folder').live("click", function (e, ui) {
        var caller = $(this);
        var errorBox = $('#add-org-to-folder-dialog p.error-active');
        var input = $('#add-org-to-folder-dialog input');
        input.val('');
        errorBox.text('');
        // autocomplete for input box
        var cache = {}, lastXhr;
        $(input).autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }
                lastXhr = $.getJSON("/Folder/OrganizationsAutoComplete", request, function (data, status, xhr) {
                    cache[term] = data;
                    if (xhr === lastXhr) {
                        response(data);
                    }
                });
            }
        }).each(function () {
            $(this).data('autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a href="#" class="add-org-autocomplete">' + item.Name + "</a>")
                    .appendTo(ul);
            };
        });
        $('.ui-autocomplete').on('click', '.add-org-autocomplete', function () {
            $('#add-org-to-folder-name').val($(this).text());
            $(this).closest('.ui-autocomplete').hide();
            return false;
        });

        // dialog
        $('#add-org-to-folder-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Submit": function () {
                    errorBox.text('');
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        { 'folderId': $(caller).attr('rel'), 'organizationName': input.val(), 'timestamp': new Date().getTime() },
                        function (result) {
                            if (result.Status == "Success") {
                                if ($('#folder-' + $(caller).attr('rel') + ' input[value="' + $(result.Data.Id) + '"]').size() == 0)
                                    $('#folder-' + $(caller).attr('rel') + ' tbody').append(result.RenderView).children(':last').hide().fadeIn('slow');
                                $(thisDialog).dialog("destroy");
                            } else {
                                errorBox.text(result.Message);
                            }
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Add Organization"
        });
        return false;
    });

    // Add to Comparison button
    $('.add-to-comparison').click(function () {
        $.getJSON(
            "/CompareTables/AddOrganizationId", { "organizationId": $(this).attr('href') }, function (data) {
                if (data == ">5") {
                    customAlert('You may only add up to five firms to the Compare Tool. In order to add this firm, please navigate to the Compare Tool and remove one or more firms.', 'Information', 'Compare');
                } else if (data == "success") {
                    customAlert('Firm has been successfully added to the Compare Tool.', 'Information', 'Compare');
                }
            });

        return false;
    });

    // Compare Tables search input
    $('.compare-tables-input').chosen({
        allow_single_deslect: true,
        max_selected_options: 5
    });

    $('.compare-tables-input').bind('liszt:maxselected', function (e) {
        customAlert('You may only select up to five firms at one time.', 'Information', 'Atlas Compare');
    }); 

    // Compare Organization button on Search tab
    $('.compare-organization-link').click(function () {
        if ($('.selected-organization:checked').size() == 0) {
            customAlert('No firms selected. Please select up to five firms to add to the Compare Tool.', 'Information', 'Compare');
            return false;
        }

        if ($('.selected-organization:checked').size() > 5) {
            customAlert('You may only add up to five firms to the Compare Tool.', 'Information', 'Compare');
            return false;
        }

        var idArray = new Array();
        $('.selected-organization:checked').each(function () {
            idArray.push($(this).val());
        });

        // check size is less than 5
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/CompareTables/CompareOrganizations/",
            data: { "organizationIds": JSON.stringify(idArray) },
            success: function () {
                window.location = "/CompareTables";
            }
        });

        return false;
    });

    //request diligence for given org
    $('a.request-organization-diligence').live("click", function (e, ui) {
        var caller = $(this);
        $('#request-diligence-comment').val('');
        if ($(this).attr('data-orgname') != undefined) {
            $('#request-diligence-dialog em strong').text($(this).attr('data-orgname'));
        }
        $('#request-diligence-dialog').dialog({
            autoOpen: true,
            width: 550,
            height: 430,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        {
                            'organizationId': $(caller).attr('rel'),
                            'comment': $('#request-diligence-comment').val(),
                            'type': $('#request-diligence-dialog .radio:checked').val(),
                            'timestamp': new Date().getTime()
                        },
                        function (data) {
                            customAlert('Your research request was successfully submitted.', 'Information', 'Successful Request');
                            $(thisDialog).dialog("destroy");
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Request Research"
        });
        return false;
    });
    $('a.request-organization-introduction').live("click", function (e, ui) {
        var caller = $(this);
        $('#request-introduction-comment').val('');
        if ($(this).attr('data-orgname') != undefined) {
            $('#request-introduction-dialog em strong').text($(this).attr('data-orgname'));
        }
        $('#request-introduction-dialog').dialog({
            autoOpen: true,
            width: 550,
            height: 360,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        {
                            'organizationId': $(caller).attr('rel'),
                            'comment': $('#request-introduction-comment').val(),
                            'type': 3,
                            'timestamp': new Date().getTime()
                        },
                        function (data) {
                            customAlert('Your request for an introduction has been successfully submitted.', 'Information', 'Successful Request');
                            $(thisDialog).dialog("destroy");
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Request An Introduction"
        });
        return false;
    });
    /* end folder*/

    /*start search*/


    // request diligence on search
    $('#search-results-grid .request-information, #firm-tabs .request-information').live('click', function () {
        var caller = $(this);
        $('#request-diligence-comment').val('');
        if ($(this).data('name') != undefined) {
            $('#request-diligence-dialog em strong').text($(this).data('name'));
        }
        $('#request-diligence-dialog').dialog({
            autoOpen: true,
            width: 550,
            height: 430,
            buttons: {
                "Submit": function () {
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        {
                            'organizationId': $(caller).data('id'),
                            'comment': $('#request-diligence-comment').val(),
                            'type': $('#request-diligence-dialog .radio:checked').val(),
                            'timestamp': new Date().getTime()
                        },
                        function (data) {
                            customAlert('Your research request was successfully submitted.', 'Information', 'Successful Request');
                            $(thisDialog).dialog("destroy");
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Request Research"
        });
        return false;
    });


    var autocompleteCache = {}, autocompleteLastXhr;
    $('.organization-autocomplete').each(function () {
        var caller = $(this);
        var selectedItem = null;
        caller.autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in autocompleteCache) {
                    response(autocompleteCache[term]);
                    return;
                }
                autocompleteLastXhr = $.getJSON("/Folder/OrganizationsAutoComplete", request, function (data, status, xhr) {
                    autocompleteCache[term] = data;
                    if (xhr === autocompleteLastXhr) {
                        response(data);
                    }
                });
            },
            close: function () {
                selectedItem = null;
            },
            focus: function (e, ui) {
                var menu = $(this).data('uiAutocomplete').menu.element,
                    focused = menu.find('a.ui-state-focus'),
                    href = focused.attr('href');
                selectedItem = focused;
            },
            select: function () {
                console.log(selectedItem);
                if (selectedItem.hasClass('auto-search')) {
                    $('#nav-search').val(selectedItem.text());
                    $('#nav-search').closest('form').submit();
                } else {
                    window.location = selectedItem.attr('data-href');
                }
            }
        });
        caller.data('autocomplete')._renderItem = function (ul, item) {
            if (item.IsPublished) {
                return $("<li>")
                    .append('<a href="javascript:void();" data-href="/Organization/Index/' + item.Id + '">' + item.Name + "</a>")
                    .appendTo(ul);
            } else {
                return $('<li>')
                    .append('<a href="javascript:void();" class="auto-search">' + item.Name + "</a>")
                    .appendTo(ul);
            }
        };
    });
    /* end search*/


    $('#loading-bar a.close-load').click(function () {
        $('#loading-bar').hide();
    });

    /* add org to group */
    $('a.add-org-to-group').live("click", function (e, ui) {
        var caller = $(this);
        var errorBox = $('#add-org-to-group-dialog p.error-active');
        var input = $('#add-org-to-group-dialog input');
        input.val('');
        errorBox.text('');
        // autocomplete for input box
        var cache = {}, lastXhr;
        $(input).autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }
                lastXhr = $.getJSON("/Folder/OrganizationsAutoComplete", request, function (data, status, xhr) {
                    var ary = new Array();
                    $.map(data, function (elem) {
                        ary.push(elem.Name);
                    });
                    cache[term] = ary;
                    if (xhr === lastXhr) {
                        response(ary);
                    }
                });
            }
        });

        // dialog
        $('#add-org-to-group-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Submit": function () {
                    errorBox.text('');
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        { groupId: $('#group-id').val(), organizationName: input.val() },
                        function (result) {
                            if (result.Status == "Success") {
                                var id = result.Data.Id;
                                var name = result.Data.Name;
                                var clonedTemplate = $('#purchased-organizations .template-remove').clone().removeClass('template-remove');
                                clonedTemplate.find('.remove-group-organization').data('id', id);
                                clonedTemplate.find('.org-name').text(name);
                                clonedTemplate.show();
                                $('#purchased-organizations .template-remove').before(clonedTemplate);
                                $(thisDialog).dialog("destroy");
                            } else {
                                errorBox.text(result.Message);
                            }
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Add Organization to Group"
        });
        return false;
    });

    $('.remove-group-organization').live('click', function () {
        var element = $(this);
        $.getJSON(element.attr('href'), { groupId: $('#group-id').val(), organizationId: $(this).data('id') }, function (result) {
            element.closest('li').fadeOut('fast').remove();
        });
        return false;
    });

    /* add segment to group */
    $('.add-segment-to-group').live("click", function (e, ui) {
        var caller = $(this);
        var errorBox = $('#add-segment-to-group-dialog p.error-active');
        var input = $('#add-segment-to-group-dialog input');
        input.val('');
        errorBox.text('');
        // autocomplete for input box
        var cache = {}, lastXhr;
        $(input).autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }
                lastXhr = $.getJSON("/Segment/SegmentsAutoComplete", request, function (data, status, xhr) {
                    cache[term] = data;
                    if (xhr === lastXhr) {
                        response(data);
                    }
                });
            }
        });

        // dialog
        $('#add-segment-to-group-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Submit": function () {
                    errorBox.text('');
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'),
                        { groupId: $('#group-id').val(), segmentName: input.val() },
                        function (result) {
                            if (result.Status == "Success") {
                                var id = result.Data.Id;
                                var name = result.Data.Name;
                                var clonedTemplate = $('#group-segments .template-remove').clone().removeClass('template-remove');
                                clonedTemplate.find('.remove-group-segment').data('id', id);
                                clonedTemplate.find('.segment-name').text(name);
                                clonedTemplate.show();
                                $('#group-segments .template-remove').before(clonedTemplate);
                                $(thisDialog).dialog("destroy");
                            } else {
                                errorBox.text(result.Message);
                            }
                        });
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Add Segment to Group"
        });
        return false;
    });

    /* delete user */
    $('.delete-user').live("click", function (e, ui) {
        var caller = $(this);
        var error = $('#delete-user-dialog .error');
        // dialog
        $('#delete-user-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Delete": function () {
                    error.text('');
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'), null, function (result) {
                        if (result.Status == "Success") {
                            $('#user-grid').refresh();
                            $(thisDialog).dialog("destroy");
                        } else {
                            error.text(result.Message);
                        }
                    });
                },
                "Cancel": function () {
                    $(this).dialog("destroy");
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Delete User"
        });
        return false;
    });
    /* delete group */
    $('.delete-group').live("click", function (e, ui) {
        var caller = $(this);
        var error = $('#delete-group-dialog .error');
        // dialog
        $('#delete-group-dialog').dialog({
            autoOpen: true,
            width: 360,
            height: 250,
            buttons: {
                "Delete": function () {
                    error.text('');
                    var thisDialog = $(this);
                    $.getJSON($(caller).attr('href'), null, function (result) {
                        if (result.Status == "Success") {
                            $('#group-grid').refresh();
                            $(thisDialog).dialog("destroy");
                        } else {
                            error.text(result.Message);
                        }
                    });
                },
                "Cancel": function () {
                    $(this).dialog("destroy");
                }
            },
            close: function () {
                $(this).dialog("destroy");
            },
            title: "Delete Group"
        });
        return false;
    });

    $('.remove-group-segment').live('click', function () {
        var element = $(this);
        $.getJSON(element.attr('href'), { groupId: $('#group-id').val(), segmentId: $(this).data('id') }, function (result) {
            element.closest('li').fadeOut('fast').remove();
        });
        return false;
    });

    $('.toggle-document').live('click', function () {
        $(this).toggleClass('expanded');
        $(this).closest('.document').find('.image-container').slideToggle('fast');
        return false;
    });
});

var AtlasDiligence = (function (jQuery) {
    var app = this,
            $ = jQuery;
    app.folders = {
        init: function (config) {
            $('.change-all').live('click', function () {
                var isChecked = $(this).is(':checked');
                if (isChecked) {
                    $('.selected-organization').attr('checked', 'checked');
                } else {
                    $('.selected-organization').removeAttr('checked');
                }
            });
            $('.selected-folder-widget .folder-widget-title').hover(function () { return false; }, function () { return false; });
            $('.selected-folder-widget .folder-widget-title').live('click', function () {
                $(this).siblings('ul').slideToggle(100);
            });
            $('.selected-folder-widget li').live('click', function () {
                var idArray = new Array();
                $('.selected-organization:checked').each(function () {
                    idArray.push($(this).val());
                });
                var postedData = {
                    organizationIds: idArray,
                    folderId: $(this).find('.folder-id').val()
                };
                var jsonUrl = $(this).find('a').attr('href');
                if ($(this).closest('li').hasClass('new-folder')) {
                    var dialogWidth = Math.ceil(window.innerWidth / 3);
                    if (dialogWidth < 480) dialogWidth = 480;
                    $('#folder-dialog').dialog({
                        autoOpen: true,
                        width: dialogWidth,
                        height: 230,
                        buttons: {
                            "Submit": function () {
                                var thisDialog = $(this);
                                postedData.name = thisDialog.find('input').val();
                                $.ajax({
                                    url: jsonUrl,
                                    type: 'POST',
                                    traditional: true,
                                    data: postedData,
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.Status == "Success") {
                                            customAlert('The organizations were successfully added', 'Information');
                                            $('.folder-widget').not('#selected-folder-widget').replaceFolderWidget(response.RenderView);
                                            $('#selected-folder-widget').replaceWith(response.SelectedRenderView);
                                        } else {
                                            customAlert('There was an error adding your selected organizations.');
                                        }
                                        $('.folder-widget-list').slideUp('fast');
                                        $(thisDialog).dialog("destroy");
                                    }
                                });
                            }
                        },
                        close: function () {
                            $(this).dialog("destroy");
                        },
                        title: "Create a new folder"
                    });
                } else {
                    if (postedData.organizationIds.length > 0) {
                        $.ajax({
                            url: jsonUrl,
                            type: 'POST',
                            traditional: true,
                            data: postedData,
                            dataType: 'json',
                            success: function (response) {
                                customAlert('The organization was successfully added', 'Information');
                                $('.folder-widget-list').slideUp('fast');
                            }
                        });
                    } else {
                        $('.folder-widget-list').slideUp('fast');
                        customAlert("No organizations selected.", "Information");
                    }
                }

                return false;
            });
        }
    };
    app.organization = {
        events: {
            viewScoutingReport: function () {
                $('#tabs').on('click', '#view-large-scatter', function () {
                    $($(this).attr('href')).click();
                    $('#ui-id-11').click();
                    return false;
                });
            },
            scoutingReportScoringSystem: function () {
                $('#tabs').on('click', '#scoring-system-dialog-link', function () {
                    $('#scoring-system-dialog').dialog({
                        width: 800,
                        title: "Atlas Scoring System: Key Terms and Methodology",
                        buttons: {
                            "Close": function () {
                                $(this).dialog("destroy");
                            }
                        },
                        close: function () {
                            $(this).dialog("destroy");
                        }
                    });
                    return false;
                });
            }
        },
        init: function () {
            app.organization.events.viewScoutingReport();
            app.organization.events.scoutingReportScoringSystem();
        }
    };
    app.search = {
        helper: {
            setSearchOptionsText: function () {
                var description = $('<ul>');

                if ($('#Search').val().length > 0) {
                    description.append('<li>Search: ' + $('#Search').val() + '</li>');
                }
                // previous blank/yes/no dropdown
                $('#advanced-search-options select').each(function () {
                    var label = $(this).siblings('label').text();
                    var values = $(this).val();
                    if (values != null && values.length > 0 && typeof (values) == typeof ([])) {
                        description.append('<li>' + label + ' ' + values.join(', ') + '</li>');
                    }
                    else if ($(this).hasClass('dropdownlist') && values.length > 0) {
                        var textVal = $(this).find(":selected").text();

                        if ($(this).hasClass('grades')) {
                            textVal += ' or higher';
                        }

                        if (textVal.length > 0) {
                            description.append('<li>' + label + ' ' + textVal + '</li>');
                        }
                    }
                });
                var hasFundSizeValue = false;
                $('#search-fund-sizes input').each(function () {
                    if ($(this).val().length > 0) {
                        hasFundSizeValue = true;
                    }
                });
                if (hasFundSizeValue) {
                    description.append('<li>Currency: ' + $('#search-currency').val() + '</li>');
                }
                $('#advanced-search-options input[type="checkbox"]:checked').each(function () {
                    var label = $(this).siblings('label').text();
                    description.append('<li>' + label + ' Yes</li>');
                });
                $('#advanced-search-options .multi-input').each(function () {
                    var ary = [];
                    var label = $(this).find('label').text();
                    $(this).find('input').each(function () {
                        if ($(this).val() != '') {
                            ary.push($(this).val());
                        }
                    });
                    if (ary.length > 0) {
                        description.append('<li>' + label + ' ' + ary.join(' to ') + '</li>');
                    }
                });
                $('#advanced-search-options .radio-button-list input[value!=""]:checked').each(function () {
                    var label = $(this).closest('.field').find('>label').text();
                    var value = $('[for="' + $(this).attr('id') + '"]').text();
                    description.append('<li>' + label + ' ' + value + '</li>');
                });
                $('#advanced-search-description').html(description);
            }
        },
        events: {
            bindStartEndDatepicker: function () {
                $('#search-next-fundraise-dates .start-date').createDateRange({ endDate: '#search-next-fundraise-dates .end-date' });
            },
            requestResearchClick: function () {
                $('.request-research').click(function () {
                    var caller = $(this);
                    $('#request-research-dialog').dialog({
                        autoOpen: true,
                        width: 550,
                        height: 490,
                        buttons: {
                            "Submit": function () {
                                var thisDialog = $(this);
                                $.getJSON(caller.attr('href'),
                                    {
                                        'name': $('#request-research-name').val(),
                                        'comment': $('#request-research-comment').val(),
                                        'type': $('#request-research-dialog .radio:checked').val(),
                                        'timestamp': new Date().getTime()
                                    },
                                function (data) {
                                    customAlert('Your research request was successfully submitted.', 'Information', 'Successful Request');
                                    $(thisDialog).dialog("destroy");
                                });
                            }
                        },
                        close: function () {
                            $(this).dialog("destroy");
                        },
                        title: "Request Research"
                    });
                    return false;
                });
            },
            legendClick: function () {
                $('.advanced-search legend').click(function () {
                    $('#advanced-search-options input:checked').attr('checked', 'checked');
                    $('#advanced-search-options option:selected').attr('selected', 'selected');
                    var html = $('#advanced-search-options').clone(true);
                    $('#advanced-search-options').dialog({
                        modal: true,
                        title: "Advanced Search Options",
                        resizable: false,
                        height: 600,
                        width: 1000,
                        buttons: {
                            "Clear": function () {
                                $(this).dialog('destroy');
                                return $(app.search.dom.clearButton).click();
                            },
                            "Close": function () {
                                app.search.helper.setSearchOptionsText();
                                $(this).dialog('destroy');
                            },
                            "Search": function () {
                                app.search.helper.setSearchOptionsText();
                                $(this).dialog('destroy');
                                return $(app.search.dom.searchButton).click();
                            }
                        },
                        open: function () {
                            $('#advanced-search-options .group-3 .chzn-container').each(function () {
                                var amt = ($('#advanced-search-options').offset().top + $('#advanced-search-options').height()) - $(this).offset().top - 5;
                                $(this).find('.chzn-results').css('max-height', amt);
                            });
                        },
                        close: function () {
                            $(this).dialog('destroy');
                            $('#advanced-search-options').replaceWith(html);
                            $('#advanced-search-options .chzn-done').removeClass('chzn-done').show();
                            $('#advanced-search-options .chzn-container').remove();
                            $('#advanced-search-options select.chosen').chosen({ allow_single_deselect: true });
                            $('#advanced-search-options .hasDatepicker').datepicker('destroy');
                            app.search.events.bindStartEndDatepicker();
                        }
                    });
                });
            },
            searchClick: function () {
                $(app.search.dom.searchButton).click(function () {
                    if ($('.advanced-search #advanced-search-options').length == 0) {
                        $('#advanced-search-options').appendTo('.advanced-search fieldset');
                    }
                });
            },
            clearClick: function () {
                $(app.search.dom.clearButton).click(function () {
                    $(this).closest('form').find('input.number, input.date, input.text').attr('value', '');
                    $('#advanced-search-options').find('input.number, input.date, input.text').attr('value', '');
                    $('#advanced-search-options select').each(function () {
                        $(this).find('option:selected').removeAttr('selected');
                    });
                    $('.search-choice').remove();
                    $('.result-selected').addClass('active-result').removeClass('result-selected');
                    $('#advanced-search-description').html('');
                    $('.radio-button-list').each(function () {
                        $(this).find('input[type="radio"]').each(function () {
                            $(this).removeAttr('checked');
                        });
                        $(this).find('input[type="radio"][value=""]').attr('checked', 'checked');
                    });
                    return $(app.search.dom.searchButton).click();
                });
            },
            searchInputChange: function () {
                $('#Search').keypress(app.search.helper.setSearchOptionsText);
            }
        },
        dom: {
            searchButton: '.advanced-search input:submit:not(#clear-search)',
            clearButton: '#clear-search'
        },
        init: function () {
            app.search.helper.setSearchOptionsText();
            app.search.events.legendClick();
            app.search.events.searchClick();
            app.search.events.clearClick();
            app.search.events.bindStartEndDatepicker();
            app.search.events.searchInputChange();
            app.search.events.requestResearchClick();
        }
    };
    return app;
}(jQuery));
// Print Page
function printThis() {
    (window.print) ? window.print() : alert('To print his page press Ctrl-P on your keyboard \nor choose print from your browser or device after clicking OK');
}