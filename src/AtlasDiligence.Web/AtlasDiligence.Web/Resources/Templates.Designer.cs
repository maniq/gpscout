﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AtlasDiligence.Web.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Templates {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Templates() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AtlasDiligence.Web.Resources.Templates", typeof(Templates).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot; xmlns=&quot;http://www.w3.org/1999/xhtml&quot; style=&quot;margin: 0; font-family: &apos;Helvetica Neue&apos;, &apos;Helvetica&apos;, Helvetica, Arial, sans-serif; padding: 0;&quot;&gt;
        ///  &lt;head&gt;
        ///&lt;!-- If you delete this meta tag, Half Life 3 will never be released. --&gt;
        ///    &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width&quot; /&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; ch [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ContactEmail {
            get {
                return ResourceManager.GetString("ContactEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot; xmlns=&quot;http://www.w3.org/1999/xhtml&quot; style=&quot;margin: 0; font-family: &apos;Helvetica Neue&apos;, &apos;Helvetica&apos;, Helvetica, Arial, sans-serif; padding: 0;&quot;&gt;
        ///  &lt;head&gt;
        ///&lt;!-- If you delete this meta tag, Half Life 3 will never be released. --&gt;
        ///    &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width&quot; /&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; ch [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CreateUserEmail {
            get {
                return ResourceManager.GetString("CreateUserEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot; xmlns=&quot;http://www.w3.org/1999/xhtml&quot; style=&quot;margin: 0; font-family: &apos;Helvetica Neue&apos;, &apos;Helvetica&apos;, Helvetica, Arial, sans-serif; padding: 0;&quot;&gt;
        ///  &lt;head&gt;
        ///&lt;!-- If you delete this meta tag, Half Life 3 will never be released. --&gt;
        ///    &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width&quot; /&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; ch [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PasswordResetEmail {
            get {
                return ResourceManager.GetString("PasswordResetEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html PUBLIC &quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot; &quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;&gt;
        ///&lt;html xmlns=&quot;http://www.w3.org/1999/xhtml&quot; xmlns=&quot;http://www.w3.org/1999/xhtml&quot; style=&quot;margin: 0; font-family: &apos;Helvetica Neue&apos;, &apos;Helvetica&apos;, Helvetica, Arial, sans-serif; padding: 0;&quot;&gt;
        ///  &lt;head&gt;
        ///&lt;!-- If you delete this meta tag, Half Life 3 will never be released. --&gt;
        ///    &lt;meta name=&quot;viewport&quot; content=&quot;width=device-width&quot; /&gt;
        ///    &lt;meta http-equiv=&quot;Content-Type&quot; content=&quot;text/html; ch [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string RequestAccessEmail {
            get {
                return ResourceManager.GetString("RequestAccessEmail", resourceCulture);
            }
        }
    }
}
