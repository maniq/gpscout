﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class BarGraphViewModel
    {
        public string GraphTitle { get; set; }
        public double BestScore { get; set; }
        public double OrganizationScore { get; set; }
        public double MedianScore { get; set; }
        public double WorstScore { get; set; }
        public string HtmlId { get; set; }
        public string OrganizationName { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public int? PaddingLeft { get; set; }
        public int? PaddingRight { get; set; }
        public int? PaddingTop { get; set; }
        public int? PaddingBottom { get; set; }
    }
}