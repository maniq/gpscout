﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Validation;
using Email = AtlasDiligence.Web.Helpers.EmailAttribute;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class ContactViewModel
    {
        [Required]
        [DisplayName("First Name")]
        public String FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public String LastName { get; set; }

        [DisplayName("Organization")]
        public String Organization { get; set; }

        [Required]
        [Email(ErrorMessage = "Invalid email address")]
        [DisplayName("Email")]
        public String Email { get; set; }

        [DisplayName("Questions & Comments")]
        public String Questions { get; set; }

        public bool Submitted { get; set; }
    }
}