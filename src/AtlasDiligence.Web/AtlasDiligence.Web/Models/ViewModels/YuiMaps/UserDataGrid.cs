﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class UserDataGrid
    {
        public UserDataGrid(aspnet_User backingUser)
        {
            Id = backingUser.UserId;
            ActiveText = "inactive";

            if (backingUser.UserExt != null)
            {
                FirstName = backingUser.UserExt.FirstName;
                LastName = backingUser.UserExt.LastName;
                GroupTemplate = backingUser.UserExt.GroupId.HasValue ? string.Format("<a href=\"/Group/Edit/{0}?returnRoute=/Admin?lt=1\">{1}</a>", backingUser.UserExt.GroupId.ToString(), backingUser.UserExt.Group.Name) : string.Empty;
                IsGroupLeader = backingUser.UserExt.GroupId.HasValue
                                    ? backingUser.UserExt.Group.GroupLeaderId == backingUser.UserId ? "Yes" : "No"
                                    : "No";
                EulaSigned = backingUser.UserExt.EulaSigned.HasValue
                                 ? backingUser.UserExt.EulaSigned.Value.ToShortDateString()
                                 : string.Empty;
            }
            if (backingUser.aspnet_Membership != null)
            {
                Email = backingUser.aspnet_Membership.Email;

                IsLockedOut = backingUser.aspnet_Membership.IsLockedOut
                              ? String.Format(
                                  "<a href=\"#\" onclick=\"$(this).unlockUser('{0}'); return false;\" class=\"grid-row-action locked\" title=\"Unlock User\">Unlock User</a>",
                                  VirtualPathUtility.ToAbsolute("~/User/Unlock/" + backingUser.UserId)
                                    )
                              : "<span class=\"grid-row-action unlocked\">Unlocked</span>";

                ActiveText = backingUser.aspnet_Membership.IsApproved ? "active" : "inactive";
            }

            ResetPassword =
                String.Format(
                    "<a href=\"#\" onclick=\"$(this).resetPassword('{0}'); return false;\" class=\"grid-row-action reset-password\">Reset Password</a>",
                    VirtualPathUtility.ToAbsolute("~/User/ResetPassword/" + backingUser.UserId));
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsGroupLeader { get; set; }
        public string GroupTemplate { get; set; }

        public string IsLockedOut { get; set; }
        public string ResetPassword { get; set; }
        public string EulaSigned { get; set; }

        /// <summary>
        /// text string "active" or "inactive"
        /// </summary>
        public string ActiveText { get; set; }
    }
}