﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.DTO.Enums;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class GroupDataGrid
    {
        public GroupDataGrid(Group backingGroup)
        {
            Id = backingGroup.Id;

            Name = backingGroup.Name;

            MaxUsers = backingGroup.MaxUsers;

            Segments = String.Join(", ", backingGroup.GroupSegments.Select(m => m.Segment.Name).OrderBy(m => m));

            GroupLeader = backingGroup.aspnet_User != null ? backingGroup.aspnet_User.UserExt.FirstName + " " + backingGroup.aspnet_User.UserExt.LastName : "No Group Leader";

            CurrentUsers = backingGroup.UserExts.Count(m => m.aspnet_User.aspnet_Membership.IsApproved);

            Product = backingGroup.DefaultProductType.HasValue ? ((ProductTypes)backingGroup.DefaultProductType.Value).GetDescription() : ProductTypes.ProfileAccount.GetDescription();

            EditLink = String.Format("<a href=\"{0}\" class=\"grid-row-action edit\">Edit</a>",
          VirtualPathUtility.ToAbsolute("~/Group/Edit/" + backingGroup.Id)
          );

            ViewLink = String.Format("<a href=\"{0}\" class=\"grid-row-action view\">View</a>",
          VirtualPathUtility.ToAbsolute("~/Group/View/" + backingGroup.Id)
          );
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Product { get; set; }
        public string Name { get; set; }
        public int MaxUsers { get; set; }
        public string Segments { get; set; }
        public int CurrentUsers { get; set; }
        public string GroupLeader { get; set; }
        public string ViewLink { get; set; }
        public string EditLink { get; set; }
    }
}