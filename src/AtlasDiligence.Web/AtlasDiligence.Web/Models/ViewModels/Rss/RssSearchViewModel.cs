﻿using System.Collections.Generic;

namespace AtlasDiligence.Web.Models.ViewModels.Rss
{
    public class RssSearchViewModel
    {
        public RssSearchViewModel()
        {
            SearchTerms = new List<string>();
        }

        public IList<string> SearchTerms;
    }
}