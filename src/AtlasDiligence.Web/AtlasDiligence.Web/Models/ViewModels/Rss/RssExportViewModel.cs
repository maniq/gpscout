﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AtlasDiligence.Web.General;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.Rss
{
    public class RssExportViewModel
    {
        public RssExportViewModel()
        {
            NewsItems = new Dictionary<string, List<News>>();
        }

        public IDictionary<string, List<News>> NewsItems { get; set; }

        [Required]
        [Display(Name = "Start Date:")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}", HtmlEncode = true, NullDisplayText = "")]
        public DateTime? StartDate { get; set; }

        [Required]
        [Display(Name = "End Date:")]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}", HtmlEncode = true, NullDisplayText = "")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Search Term Aliases?")]
        public bool SearchAliases { get; set; }

        public RssViewType RssViewType { get; set; }
    }
}
