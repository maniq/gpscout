﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.Controllers.Services;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class ScatterChartViewModel
    {
        public string HtmlId { get; set; }
        public IEnumerable<LuceneSearchResult> OtherOrganizations { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public double OrganizationQualitativeScore { get; set; }
        public double OrganizationQuantitativeScore { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public int? PaddingLeft { get; set; }
        public int? PaddingRight { get; set; }
        public int? PaddingTop { get; set; }
        public int? PaddingBottom { get; set; }
        public int? XAxisDistanceFromBottomOfChart { get; set; }

	    public void Build(List<LuceneSearchResult> availableOrgs, int height, int width, int paddingLeft,
			int paddingBottom, int paddingRight, int paddingTop, int xAxisDistanceFromBottromOfChart)
	    {
		    OtherOrganizations = availableOrgs;
		    Height = height;
		    Width = width;
		    PaddingLeft = paddingLeft;
		    PaddingRight = paddingRight;
		    PaddingTop = paddingTop;
		    XAxisDistanceFromBottomOfChart = xAxisDistanceFromBottromOfChart;
	    }
    }
}