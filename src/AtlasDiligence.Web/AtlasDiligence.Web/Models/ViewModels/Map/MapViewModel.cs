﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.Controllers.Services;

namespace AtlasDiligence.Web.Models.ViewModels.Map
{
	public class MapViewModel
	{
		#region properties

		public IEnumerable<LuceneSearchResult> Organizations;

		public IEnumerable<Common.Data.Models.Organization> PreviewOrganizations;

		[Display(Name = @"Atlas Focus/Radar List")]
		public bool FocusRadarFilter { get; set; }

		[Display(Name = @"In The Market")]
		public bool FundRaisingFilter { get; set; }

		#endregion

		public void Build(IEnumerable<LuceneSearchResult> availableOrgs, IOrganizationService organizationService)
		{
			// get preview orgs
			var previewOrgs =
					organizationService.GetAllPositions().Where(w => !availableOrgs.Where(a => a.Positions.Any()).Select(s => s.Id).Contains(w.Id));

			// set properties
			Organizations = availableOrgs.Where(w => w.Positions.Any());
			PreviewOrganizations = previewOrgs;
		}
	}
}