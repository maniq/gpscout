﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels.Search
{
    public class SnapshotViewModel
    {
        public SnapshotViewModel(IList<LuceneSearchResult> allAvailableOrganizations, IEnumerable<string> segments, int firmsInProcess)
        {
            var fundRepository = ComponentBrokerInstance.RetrieveComponent<Repository<Fund>>();
            var allFunds = (from fund in fundRepository.GetAll()
                            select new
                            {
                                fund.Name,
                                fund.OrganizationId
                            }).ToList();
            TotalFunds = allFunds.Count(m => allAvailableOrganizations.Select(v => v.Id).Contains(m.OrganizationId));
            TotalFirms = allAvailableOrganizations.Count();
            CurrentlyFundraising = allAvailableOrganizations.Count(m => new[] { "Open", "Upcoming Final Close" }.Contains(m.FundraisingStatus)) ;
            ComingToMarket =
                allAvailableOrganizations.Count(
                    m =>
                    m.ExpectedNextFundraise.GetValueOrDefault(new DateTime(1990, 1, 1)).Date <=
                    DateTime.Now.Date.AddMonths(6) &&
                    m.ExpectedNextFundraise.GetValueOrDefault(new DateTime(1990, 1, 1)).Date > DateTime.Now.Date);
            AvailableSegments = segments;
            InProcess = firmsInProcess;
        }

        public IEnumerable<string> AvailableSegments { get; set; } 
        public int TotalFirms { get; set; }
        public int TotalFunds { get; set; }
        public int CurrentlyFundraising { get; set; }
        public int ComingToMarket { get; set; }
        public int InProcess { get; set; }
    }
}