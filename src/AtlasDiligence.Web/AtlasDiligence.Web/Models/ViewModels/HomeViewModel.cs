﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Web.Models.ViewModels.Search;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class HomeViewModel
    {
        public UserEntity UserEntity { get; set; }

        public SearchViewModel SearchViewModel { get; set; }
    }
}