﻿using System;
using System.Linq;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class EulaViewModel
    {
        public string Text { get; set; }

        public DateTime Date { get; set; }
        public string CurrentText { get; set; }

        public void Build(IEulaRepository eulaRepository)
        {
            var eula = eulaRepository.GetAll().SingleOrDefault();
            Text = eula == null ? string.Empty : eula.Text;
        }
    }
}