﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Models.ViewModels.TrackUsers
{
	public class TrackUsersViewModel
	{
		public TrackUsersType TrackType { get; set; }

		[Required]
		[Display(Name = "Start Date:")]
		[DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}", HtmlEncode = true, NullDisplayText = "")]
		public DateTime StartDate { get; set; }

		[Required]
		[Display(Name = "End Date:")]
		[DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}", HtmlEncode = true, NullDisplayText = "")]
		public DateTime EndDate { get; set; }
	}
}