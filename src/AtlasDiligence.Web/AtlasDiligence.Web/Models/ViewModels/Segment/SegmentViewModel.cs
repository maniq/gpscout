﻿using System;
using System.Linq;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Repositories;

namespace AtlasDiligence.Web.Models.ViewModels.Segment
{
    public class SegmentViewModel
    {
        public SegmentOptionsViewModel SegmentOptions { get; set; }

        /// <summary>
        /// Build new segment options.
        /// </summary>
        /// <param name="segmentRepository"></param>
        public void Build(ISegmentRepository segmentRepository)
        {
            var options = segmentRepository.GetDistinctOptions().ToList();

            if (SegmentOptions == null)
            {
                SegmentOptions = new SegmentOptionsViewModel();
            }

            SegmentOptions.Strategies = options.Where(m => m.Field == (int)FieldType.Strategy).Select(m => m.Value).ToList();
            SegmentOptions.MarketStages = options.Where(m => m.Field == (int)FieldType.MarketStage).Select(m => m.Value).ToList();
            SegmentOptions.InvestmentRegions = options.Where(m => m.Field == (int)FieldType.InvestmentRegion).Select(m => m.Value).ToList();
            SegmentOptions.Countries = options.Where(m => m.Field == (int)FieldType.Country).Select(m => m.Value).ToList();
        }

        /// <summary>
        /// Build from existing segment options.
        /// </summary>
        /// <param name="segmentRepository"></param>
        /// <param name="segmentId"></param>
        public void Build(ISegmentRepository segmentRepository, Guid segmentId)
        {
            Build(segmentRepository);
            var segment = segmentRepository.GetSegmentById(segmentId);
            SegmentOptions.Id = segmentId;
            SegmentOptions.Name = segment.Name;
            SegmentOptions.SelectedInvestmentRegions = segment.SegmentFilters.Where(m => m.Field == (int)FieldType.InvestmentRegion).Select(m => m.Value).ToList();
            SegmentOptions.SelectedMarketStages = segment.SegmentFilters.Where(m => m.Field == (int)FieldType.MarketStage).Select(m => m.Value).ToList();
            SegmentOptions.SelectedStrategies = segment.SegmentFilters.Where(m => m.Field == (int)FieldType.Strategy).Select(m => m.Value).ToList();
            SegmentOptions.SelectedCountries = segment.SegmentFilters.Where(m => m.Field == (int)FieldType.Country).Select(m => m.Value).ToList();
        }
    }
}