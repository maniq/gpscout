﻿using System.ComponentModel.DataAnnotations;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Web.Helpers
{
    public class EmailAttribute : RegularExpressionAttribute
    {
        public EmailAttribute() : base(RegExPattern.GetRegExpPatternEmail()) { }
    }
}