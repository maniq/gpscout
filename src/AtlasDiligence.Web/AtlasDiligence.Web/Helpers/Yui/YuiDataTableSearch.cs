﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public class YuiDataTableSearch<T>
    {
        public YuiDataTableSearch(string formId, string buttonId)
        {
            this.FormId = formId;
            this.ButtonId = buttonId;
        }

        #region Properties

        /// <summary>
        /// Gets the name of the field.
        /// </summary>
        /// <value>The name of the field.</value>
        public string FormId { get; private set; }

        public string ButtonId { get; private set; }
        #endregion
    }
}
