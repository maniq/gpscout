﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public class YuiDataTableColumnSettings : IComparable<YuiDataTableColumnSettings>
    {
        private string _friendlyName;

        public YuiDataTableColumnSettings()
        {
            ColumnSortable = true;
            ColumnResizeable = true;
            ColumnIndex = -1;
            ColumnMaxAutoWidth = -1;
            ColumnMinWidth = -1;
            ColumnWidth = -1;
            ColumnDefaultDir = YuiSortDirection.NONE;
        }

        #region Fields

        public int ColumnIndex { get; private set; }

        public YuiSortDirection ColumnDefaultDir { get; private set; }

        public bool ColumnSortable { get; private set; }

        public string ColumnLabel { get; private set; }

        public string ColumnKey { get; private set; }

        public bool ColumnHidden { get; private set; }

        public string ColumnClassName { get; private set; }

        public string ColumnFormatter { get; private set; }

        public int ColumnMaxAutoWidth { get; private set; }

        public int ColumnMinWidth { get; private set; }

        public bool ColumnResizeable { get; private set; }

        public int ColumnWidth { get; private set; }

        public string ColumnAction { get; private set; }

        /// <summary>
        /// set the column formatter based on the template string.
        /// NOTE: brackets {} are reserved characters for the template variables
        /// NOTE: template variables must be defined column properties.
        /// </summary>
        /// <remarks>
        /// Any bracket enclosed variables in the template string will be replaced by oRecord.getData calls.
        /// Bracketed variable must match a column Key.
        /// For Example,
        /// Template: {Test}
        /// Processed Replace: ' + oRecord.getData("Test") + '
        /// </remarks>
        /// <param name="template"></param>
        public void CustomTemplate(string template)
        {            
            var processedTemplate = template.Replace("'", "\\\'").Replace("{", "' + oRecord.getData(\"").Replace("}", "\") + '");

            ColumnFormatter = "function(elLiner, oRecord, oColumn, oData) { " +
                    "elLiner.innerHTML = '" + processedTemplate + "'; }";
        }

        /// <summary>
        /// Produces a name much like the label, but useful when the column header needs 
        /// to have a graphic, and the "show/hide" options need to have a textual identifier.
        /// 
        /// Defaults to the value of "Key" if not set.
        /// </summary>
        public String ColumnFriendlyName
        {
            get { return _friendlyName ?? ColumnKey; }
            private set { _friendlyName = value; }
        }

        #endregion

        #region Chain Methods

        /// <summary>
        /// Indicates column order.
        /// </summary>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public YuiDataTableColumnSettings ColIndex(int colIndex)
        {
            ColumnIndex = colIndex;
            return this;
        }

        /// <summary>
        /// Should the column be sorted, if so, asc or desc?
        /// </summary>
        /// <remarks>If this is NULL, it is omitted from the resulting JSON column markup.</remarks>
        public YuiDataTableColumnSettings DefaultDir(YuiSortDirection defaultDir)
        {
            ColumnDefaultDir = defaultDir;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        public YuiDataTableColumnSettings Sortable(bool sortable)
        {
            ColumnSortable = sortable;
            return this;
        }

        /// <summary>
        /// This is the column label. It can be any valid HTML.
        /// </summary>
        public YuiDataTableColumnSettings Label(string label)
        {
            ColumnLabel = label;
            return this;
        }

        /// <summary>
        /// The name of the data that will be sent from the controller.
        /// </summary>
        public YuiDataTableColumnSettings Key(string key)
        {
            ColumnKey = key;
            return this;
        }

        /// <summary>
        /// Should this column be shown.
        /// </summary>
        public YuiDataTableColumnSettings Hidden(bool hidden)
        {
            ColumnHidden = hidden;
            return this;
        }

        /// <summary>
        /// Should this column recieve any special css class treatment.
        /// </summary>
        /// <remarks>
        /// Omitted from the JSON definition if null. 
        /// </remarks>
        public YuiDataTableColumnSettings ClassName(string className)
        {
            ColumnClassName = className;
            return this;
        }

        /// <summary>
        /// How should this column be rendered.
        /// </summary>
        /// <remarks>
        /// Omitted from the JSON definition if null. 
        /// Since this can also be a function, enclosing quotes are ommitted from the JSON representation, 
        /// if quotes are needed, include them as part of the "set" value.
        /// 
        /// an inline function should look like the following:
        /// <![CDATA[
        /// function(cell, record, column, data){
        ///     cell.innerHTML = "<b>"+record.getData('KEY')+"</b>";
        /// }]]>
        /// </remarks>
        public YuiDataTableColumnSettings Formatter(string formatter)
        {
            ColumnFormatter = formatter;
            return this;
        }

        /// <summary>
        /// Should the column have a max size
        /// </summary>
        /// <remarks>
        /// Omitted from the JSON definition if null. 
        /// </remarks>
        public YuiDataTableColumnSettings MaxAutoWidth(int maxAutoWidth)
        {
            ColumnMaxAutoWidth = maxAutoWidth;
            return this;
        }

        /// <summary>
        /// What is the minimum column width?
        /// </summary>
        /// <remarks>
        /// Omitted from the JSON definition if null. 
        /// </remarks>
        public YuiDataTableColumnSettings MinWidth(int minWidth)
        {
            ColumnMinWidth = minWidth;
            return this;
        }

        /// <summary>
        /// Should the column be resizeable?
        /// </summary>
        /// <remarks>
        /// Defaults to True.
        /// </remarks>
        public YuiDataTableColumnSettings Resizeable(bool resizeable)
        {
            ColumnResizeable = resizeable;
            return this;
        }

        /// <summary>
        /// Set the width of the column.
        /// </summary>
        /// <param name="width"></param>
        /// <returns></returns>
        public YuiDataTableColumnSettings Width(int width)
        {
            ColumnWidth = width;
            return this;
        }

        public YuiDataTableColumnSettings FriendlyName(string friendlyName)
        {
            ColumnFriendlyName = friendlyName;
            return this;
        }

        public YuiDataTableColumnSettings Action(string actionName)
        {
            ColumnAction = actionName;
            return this;
        }

        #endregion

        /// <summary>
        /// This will cause the columns to be ordered properly in the rendered Javascript.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(YuiDataTableColumnSettings other)
        {

            int retval = 0;
            if (this.ColumnHidden != other.ColumnHidden)
            {
                if (this.ColumnHidden == true)
                {
                    retval = 1;
                }
                else
                {
                    retval = -1;
                }
            }
            else if (this.ColumnIndex != -1 && other.ColumnIndex == -1)
            {
                retval = -1;
            }
            else if (other.ColumnIndex != -1 && this.ColumnIndex == -1)
            {
                retval = 1;
            }
            else if (this.ColumnIndex != -1 && other.ColumnIndex != -1)
            {
                int intermediary = this.ColumnIndex.CompareTo(other.ColumnIndex);
                if (intermediary == 0)
                {
                    //order by label if the indices are equal
                    intermediary = this.ColumnLabel.CompareTo(other.ColumnLabel);
                }
                retval = intermediary;
            }
            else
            {
                //order by label if the indices are equal
                retval = this.ColumnLabel.CompareTo(other.ColumnLabel);
            }

            return retval;
        }

        /// <summary>
        /// Generates a string representation of this attribute.
        /// </summary>
        /// <param name="serializeToJSON">If the serializeToJSON is set to true, serilizes to JSON, else, uses the default ToString implementation.</param>
        /// <returns></returns>
        public String ToString(bool serializeToJSON)
        {
            String retval = null;
            if (serializeToJSON)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("{{key : '{0}',label : '{1}',resizeable : {2},hidden: {3},sortable : {4}, friendlyName : '{5}',",
                    this.ColumnKey, this.ColumnLabel, this.ColumnResizeable.ToString().ToLower(),
                    this.ColumnHidden.ToString().ToLower(), this.ColumnSortable.ToString().ToLower(), this.ColumnFriendlyName);

                if (this.ColumnWidth != -1)
                {
                    sb.AppendFormat("width : {0},", this.ColumnWidth);
                }
                if (this.ColumnMinWidth != -1)
                {
                    sb.AppendFormat("minwidth : {0},", this.ColumnMinWidth);
                }
                if (this.ColumnMaxAutoWidth != -1)
                {
                    sb.AppendFormat("maxAutoWidth : {0},", this.ColumnMaxAutoWidth);
                }
                if (!string.IsNullOrEmpty(this.ColumnFormatter))
                {
                    sb.AppendFormat("formatter : {0},", this.ColumnFormatter);
                }
                if (this.ColumnDefaultDir != YuiSortDirection.NONE)
                {
                    if (this.ColumnDefaultDir == YuiSortDirection.ASC)
                    {
                        sb.Append("sortOptions : { 'YAHOO.widget.DataTable.CLASS_ASC' },");
                    }
                    else
                    {
                        sb.Append("sortOptions : { 'YAHOO.widget.DataTable.CLASS_DESC' },");
                    }
                }
                if (!string.IsNullOrEmpty(this.ColumnClassName))
                {
                    sb.AppendFormat("className : '{0}',", this.ColumnClassName);
                }
                if (!string.IsNullOrEmpty(this.ColumnAction))
                    sb.AppendFormat("action : '{0}',", this.ColumnAction);

                retval = sb.ToString();
                retval = retval.TrimEnd(',') + "}";
            }
            else
            {
                retval = this.ToString();
            }
            return retval;
        }
    }

    /// <summary>
    /// Indicates if the column should be sorted or not.
    /// </summary>
    public enum YuiSortDirection
    {
        ASC,
        DESC,
        NONE
    }
}
