﻿using System.Web.Mvc;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public static class YuiHelperExtension
    {
        public static YuiDataTable<T> YuiGrid<T>(this HtmlHelper htmlHelper) where T : class
        {
            return new YuiDataTable<T>();
        }
    }
}