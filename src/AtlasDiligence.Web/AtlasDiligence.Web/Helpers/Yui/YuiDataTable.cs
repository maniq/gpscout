﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Web.UI;
using System.IO;
using AtlanticBT.Common.Types;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;

namespace AtlasDiligence.Web.Helpers.Yui
{
    /// <summary>
    /// Fluent Helper for YUI DataTable.
    /// </summary>
    public class YuiDataTable<T> where T : class
    {
        #region Private Methods
        private readonly Type _resultType;
        private Dictionary<string, object> _requestParams = new Dictionary<string, object>();
        private string _url;
        private string _gridId = "YuiDataTable_" + Guid.NewGuid().ToString();
        private SelectionMode _rowSelectionMode = SelectionMode.None;
        private int _rowsPerPage = ConfigurationManager.AppSettings.Get("DataGridRowsPerPage").Maybe<int>() ?? 10;
        private bool _showColumnSelector;
        private bool _showPagination = true;
        private KeyValuePair<bool, int[]> _rowsPerPageOptions = new KeyValuePair<bool, int[]>(false, new int[] { });
        private bool _showSelectOptions;
        private YuiDataTableActionBuilder<T> _tableActions = new YuiDataTableActionBuilder<T>();
        private readonly YuiDataTableColumnCollection<T> _columns = new YuiDataTableColumnCollection<T>();
        private string _cellClickEvent = "function(e) {}";
        private YuiDataTableSearch<T> _tableSearch;
        public string _postRenderEvent = "function() {}";

        private readonly int[] _defaultRowsPerPageOptions = new int[] { 10, 25, 50 };
        #endregion

        public YuiDataTable()
        {
            _resultType = typeof(GridList<T>);
        }

        #region Public Methods
        /// <summary>
        /// Parameters passed in ajax call to retrieve grid data.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public YuiDataTable<T> RequestParams(Dictionary<string, object> items)
        {
            //var items = new Dictionary<string, object>();
            //action(items);
            _requestParams = items;
            return this;
        }

        /// <summary>
        /// Url of ajax call to retrieve grid data.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public YuiDataTable<T> Url(string url)
        {
            _url = url;
            return this;
        }

        /// <summary>
        /// DOM element id for grid.
        /// </summary>
        /// <param name="gridId"></param>
        /// <returns></returns>
        public YuiDataTable<T> GridId(string gridId)
        {
            _gridId = gridId;
            return this;
        }

        /// <summary>
        /// Allows rows to be selected onclick depending on mode enum.
        /// </summary>
        /// <param name="selectionMode"></param>
        /// <returns></returns>
        public YuiDataTable<T> RowSelectionMode(SelectionMode selectionMode)
        {
            _rowSelectionMode = selectionMode;
            return this;
        }

        /// <summary>
        /// Overrides default rows per page limit. This, in turn will be overriden by the first option in 
        /// RowsPerPage.Value if RowsPerPageOptions is set to true.
        /// </summary>
        /// <param name="rowsPerPage"></param>
        /// <returns></returns>
        public YuiDataTable<T> RowsPerPage(int rowsPerPage)
        {
            _rowsPerPage = rowsPerPage;
            return this;
        }

        /// <summary>
        /// Displays a UI column selector below grid if set to true.
        /// </summary>
        /// <param name="showColumnSelector"></param>
        /// <returns></returns>
        public YuiDataTable<T> ShowColumnSelector(bool showColumnSelector)
        {
            _showColumnSelector = showColumnSelector;
            return this;
        }

        /// <summary>
        /// Displays pagination links if set to true
        /// </summary>
        /// <param name="showPagination"></param>
        /// <returns></returns>
        public YuiDataTable<T> ShowPagination(bool showPagination)
        {
            _showPagination = showPagination;
            return this;
        }

        /// <summary>
        /// Displays rows per page dropdown in pagination if set to true.
        /// Integer array overrides default option values in dropdown.
        /// </summary>
        /// <param name="showOptions"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public YuiDataTable<T> RowsPerPageOptions(bool showOptions, int[] options)
        {
            if (options == null)
                options = _defaultRowsPerPageOptions;
            _rowsPerPageOptions = new KeyValuePair<bool, int[]>(showOptions, options);
            return this;
        }

        /// <summary>
        /// Override for explicit false RowsPerPageOptions
        /// </summary>
        /// <param name="showOptions"></param>
        /// <returns></returns>
        public YuiDataTable<T> RowsPerPageOptions(bool showOptions)
        {
            return RowsPerPageOptions(showOptions, null);
        }

        /// <summary>
        /// Displays a toggle to select/deselect all visible data rows.
        /// </summary>
        /// <param name="showSelectOptions"></param>
        /// <returns></returns>
        public YuiDataTable<T> ShowSelectOptions(bool showSelectOptions)
        {
            _showSelectOptions = showSelectOptions;
            return this;
        }

        public YuiDataTable<T> Search(string formId, string buttonId)
        {
            _tableSearch = new YuiDataTableSearch<T>(formId, buttonId);
            return this;
        }

        /// <summary>
        /// Set any actions that will appear as button links below grid.
        /// </summary>
        /// <remarks>Usage example: Can be used in conjunction with row selction to execute an action on one or many selected rows.</remarks>
        /// <param name="builder"></param>
        /// <returns></returns>
        public YuiDataTable<T> TableActions(Action<YuiDataTableActionBuilder<T>> builder)
        {
            builder(_tableActions);
            return this;
        }

        public YuiDataTable<T> Columns(Action<YuiDataTableColumnCollection<T>> action)
        {
            action(this._columns);
            return this;
        }

        /// <summary>
        /// The function that will be called for any cell click events on this grid.
        /// </summary>
        /// <remarks>
        /// The cellclickevent function can be used in collaboration with the Column.Action setting to define
        /// actions on a particular column.
        /// Example:
        /// YuiGrid...CellClickEvent("function(e) { $(this).cellClickEvent(e); }")
        /// Calls js function:
        /// $.fn.cellClickEvent = function(e) {
        ///     var dt = this.get(0);
        ///     var column = dt.getColumn(e.target);
        ///     if (column.action == 'delete') {...
        /// </remarks>
        /// <param name="cellClickFunction"></param>
        /// <returns></returns>
        public YuiDataTable<T> CellClickEvent(string cellClickFunction)
        {
            _cellClickEvent = cellClickFunction;
            return this;
        }

        /// <summary>
        /// This funtion will be fired after the datatable DOM is rendered or modified including column width validations
        /// </summary>
            /// <param name="postRenderEventFunction"></param>
        /// <returns></returns>
        public YuiDataTable<T> PostRenderEvent(string postRenderEventFunction)
        {
            _postRenderEvent = postRenderEventFunction;
            return this;
        }
        #endregion

        private string Render()
        {
            String scriptProto = Assembly.GetExecutingAssembly().GetEmbeddedString("AtlasDiligence.Web.Helpers.Yui.YuiDataTable.js");

            String actionScriptProto =
                "jQuery('#{0}').bind('click', function(e,ui){{({1}).call(this,e,ui,dataTable.data('dataTable'));}});";

            String fields = "";
            String columnDef = "";

            #region define fields and colDefs
            foreach (var column in _columns)
            {
                fields += !string.IsNullOrEmpty(column.Settings.ColumnKey)
                              ? String.Format("'{0}'", column.Settings.ColumnKey)
                              : String.Format("'{0}'", column.FieldName);

                fields += ",";
            }
            fields = fields.TrimEnd(',');

            columnDef = _columns.Select(x => x.Settings).Aggregate("", (seed, current) =>
            {
                if (seed != "")
                {
                    seed += ",";
                }
                seed += current.ToString(true);
                return seed;
            });
            #endregion

            String metaFields = "total: 'Total'";

            #region populate init params.
            String initialParams = "{}";
            if (_requestParams != null)
            {
                initialParams = _requestParams.Aggregate("{", (seed, current) =>
                {
                    seed += String.Format("'{0}':'{1}',", current.Key, current.Value);
                    return seed;
                }).TrimEnd(',') + "}";
            }

            #endregion

            if (this._showSelectOptions)
            {
                var selectRow = new YuiDataTableAction<T>("li");
                selectRow.WithClickJS("function(e,ui,dt){ var b = dt.getRecordSet().getRecords();  for(i in b){ dt.selectRow(b[i].getId()); } }");
                selectRow.InnerHtml = "<a href=\"#\">Select All</a> ";
                selectRow.Attributes["id"] = this._gridId + "selectRow";
                var deselectRow = new YuiDataTableAction<T>("li");
                deselectRow.WithClickJS("function(e,ui,dt){ dt.unselectAllRows(); }");
                deselectRow.InnerHtml = " <a href=\"#\">Clear Selection</a>";
                deselectRow.Attributes["id"] = this._gridId + "deselectRow";

                _tableActions.TableActions.Add(selectRow);
                _tableActions.TableActions.Add(deselectRow);
            }

            #region build buttons js.
            String buttonJSFns = this._tableActions.TableActions
                .Aggregate("", (seed, current) =>
                {
                    seed += String.Format(actionScriptProto,
                        current.Attributes["id"], current.ClickJS.TrimEnd(';'));
                    return seed;
                });
            if (_tableSearch != null)
            {

                buttonJSFns += string.Format(actionScriptProto, _tableSearch.ButtonId,
                                             "function(e,ui,dt){ dt.showTableMessage('Loading...'); " +
                                             "dt.getDataSource().sendRequest(dt.configs.generateRequest(null, dt), { success: dt.onDataReturnInitializeTable }, dt); }");
            }

            #endregion

            String rowsPerPageOptions = _rowsPerPageOptions.Value.Aggregate("[", (seed, current) =>
            {
                seed += current.ToString() + ",";
                return seed;
            }).TrimEnd(',') + "]";

            //if we are showing rowsPerPageOptions, the first choice will override; otherwise use rowsPerPage
            int rowsPerPage = _rowsPerPageOptions.Key ? _rowsPerPageOptions.Value.First() : _rowsPerPage;

            scriptProto = scriptProto
                .Replace("__ID__", this._gridId)
                .Replace("__URL__", this._url)
                .Replace("__COLDEF__", columnDef)
                .Replace("__SELECTIONMODE__", this._rowSelectionMode.ToString().ToLower())
                .Replace("__ONSELECTFN__", "function(args){}")
                .Replace("__SELECTFIRSTROW__", "false")
                .Replace("__ONCHECKBOXCLICK__", "function(e) {}")
                .Replace("__ONCELLCLICK__", this._cellClickEvent)
                .Replace("__DOBEFORELOADDATA__", "function(sRequest,oResponse,oPayload){ return true; }")
                .Replace("__ROWSPERPAGE__", rowsPerPage.ToString())
                .Replace("__FIELDS__", fields)
                .Replace("__METAFIELDS__", metaFields)
                .Replace("__BUTTONHOOKS__", buttonJSFns)
                .Replace("__SEARCHFORMID__", _tableSearch != null ? _tableSearch.FormId : "")
                .Replace("__SHOWCOLUMNSELECTOR__", this._showColumnSelector.ToString().ToLower())
                .Replace("__SHOWPAGINATION__", this._showPagination.ToString().ToLower())
                .Replace("__INITIALREQUESTPARAMS__", initialParams)
                .Replace("__SHOWPAGINATION_ROWSPERPAGEOPTIONS__", _rowsPerPageOptions.Key.ToString().ToLower())
                .Replace("__POSTRENDEREVENT__", _postRenderEvent)
                .Replace("__ROWSPERPAGEOPTIONS__", rowsPerPageOptions);

            using (var sw = new StringWriter())
            {
                using (var writer = new HtmlTextWriter(sw))
                {
                    writer.Write("<div class=\"rx-grid yui-skin-sam\">");
                    writer.Write("<div id=\"" + this._gridId + "\" class=\"yuidatatable\"></div>");
                    writer.Write("<div class=\"rx-grid-footer\">");
                    writer.Write("<div class=\"dt-paginator pagination reset yui-skin-sam\">");
                    writer.Write(string.Format("<div id='{0}Paginator'></div></div>", this._gridId));
                    writer.Write("<div class=\"rx-grid-actions\"><ul>");
                    //TableActionButtons will go here
                    foreach (var action in _tableActions.TableActions)
                    {
                        if (action.RenderButton)
                            writer.Write(action.ToString(TagRenderMode.Normal));
                    }
                    writer.Write("</ul>");
                    if (this._showColumnSelector)
                    {
                        writer.Write("<div id=\"" + this._gridId + "COLUMNCONTAINER\" class=\"col-selector\"></div>");
                    }
                    writer.Write("</div>"); //rx-actions
                    writer.Write("</div>"); //rx-grid-footer
                    writer.Write("</div>"); //rx-grid

                    // Create the javascript tag.
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, @"text/javascript");
                    writer.RenderBeginTag(HtmlTextWriterTag.Script);
                    writer.Write(scriptProto);
                    writer.RenderEndTag();
                }

                return sw.ToString();
            }
        }

        /// <summary>
        /// Returns the final string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Render();
        }
    }

    public enum SelectionMode
    {
        None,
        Single,
        Standard
    }
}
