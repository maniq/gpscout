﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Helpers.Yui
{
    public class YuiDataTableAction<T> : TagBuilder
    {
        protected String _clickJS = "function(e,ui,dt){ }";
        protected bool _renderButton = true;
        protected String _searchFormId = "";

        public YuiDataTableAction()
            : this("YuiDataTableAction_" + Guid.NewGuid().ToString())
        {
        }

        public YuiDataTableAction(string id)
            : base("li")
        {
            this.AddCssClass("yui-dt-action");
            this.Attributes["id"] = id;
        }

        public string ClickJS
        {
            get { return _clickJS; }
            private set { _clickJS = value; }
        }

        /// <summary>
        /// True by default. If false, a table button will not be Rendered.
        /// This allows the onClick action to be used with an outside element (provided the id is provided in the constructor)
        /// </summary>
        public bool RenderButton
        {
            get { return _renderButton; }
            private set { _renderButton = value;  }
        }

        public string SearchFormId
        {
            get { return _searchFormId; }
            private set { _searchFormId = value; }
        }

        /// <summary>
        /// InnerHtml content chainable method.
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public YuiDataTableAction<T> WithContent(string content)
        {
            this.InnerHtml = content;
            return this;
        }

        /// <summary>
        /// js onclick chainable method. 
        /// </summary>
        /// <remarks>
        /// Format:
        /// function(e,ui,dt){ thecode; }
        /// Is Rendered as {1} in this string format:
        /// function(e,ui){{({1}).call(this,e,ui,dataTable.data('dataTable'));}});
        /// </remarks>
        /// <param name="clickJS"></param>
        /// <returns></returns>
        public YuiDataTableAction<T> WithClickJS(string clickJS)
        {
            ClickJS = clickJS;
            return this;
        }

        /// <summary>
        /// renderButton chainable method. If set to false, an action button will not be generated.
        /// Can be used to associate an action with a DOM element outside of the grid (id can be passed in constructor).
        /// </summary>
        /// <param name="renderButton"></param>
        /// <returns></returns>
        public YuiDataTableAction<T> WithRenderButton(bool renderButton)
        {
            RenderButton = renderButton;
            return this;
        }

        public YuiDataTableAction<T> WithSearchFormId(string searchFormId)
        {
            SearchFormId = searchFormId;
            return this;
        }
    }
}
