﻿using System;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.ServiceDelegate;

namespace AtlasDiligence.Common.DTO.Services
{
    public class GroupService : IGroupService
    {
        private readonly GroupServiceDelegate _serviceDelegate;
        public GroupService()
        {
            _serviceDelegate = new GroupServiceDelegate();
        }

        public Group GetById(Guid id)
        {
            return CrmEntityMapper.GroupFromCrmEntity(_serviceDelegate.GetById<new_group>(id));
        }

        public Group GetByName(string name)
        {
            return CrmEntityMapper.GroupFromCrmEntity(_serviceDelegate.GetByName(name));
        }

        public Guid Create(Group group)
        {
            return _serviceDelegate.Create(CrmEntityMapper.CrmEntityFromGroup(group));
        }

        public void Update(Group group)
        {
            _serviceDelegate.Update(CrmEntityMapper.CrmEntityFromGroup(group));
        }
        public void RemoveUserAsGroupLeader(Guid userId)
        {
            _serviceDelegate.RemoveUserAsGroupLeader(userId);
        }
        public void DeleteGroup(Guid groupId)
        {
            _serviceDelegate.Delete(_serviceDelegate.GetById<new_group>(groupId));
        }
    }
}
