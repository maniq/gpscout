﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;

namespace AtlasDiligence.Common.DTO.Services
{

	public class SearchService : ISearchService
	{
		private IRepository<Data.Models.Organization> OrganizationRepository
		{
			get
			{
				return ComponentBrokerInstance.RetrieveComponent<IRepository<Data.Models.Organization>>();
			}
		}

		// If you want to add a field for the 'universal search' for a term, please do not add it here, add it to the IndexService.ReIndex universal search field.  It is more efficient
		private readonly List<SearchField> fieldsToTermSearch = new List<SearchField>
                                                                 {
                                                                     SearchField.UniversalSearch
                                                                 };

		public IEnumerable<LuceneSearchResult> GetOrganizations(SearchFilter filter, out int total)
		{
			var stopWatch = new System.Diagnostics.Stopwatch();
			stopWatch.Start();

			var queryString = this.BuildQueryString(filter);
			var result = this.GetOrganizationsFromLuceneString(queryString.ToString(), out total);

			stopWatch.Stop();
			System.Diagnostics.Debug.WriteLine("Get Organizations from lucene took " + stopWatch.Elapsed + " seconds");
			return result;
		}

		/// <summary>
		/// Returns all organizations that match the specific segment that the specified one matches on
		/// </summary>
		/// <param name="availableSegments"></param>
		/// <param name="organizationId"></param>
		/// <returns></returns>
		public IEnumerable<LuceneSearchResult> GetOrganizationsMatchOrganizationSegment(IList<Segment> availableSegments, Guid organizationId)
		{
			var retval = new List<LuceneSearchResult>();
			foreach (var segment in availableSegments)
			{
				int total;
				var results =
					this.GetOrganizations(new SearchFilter { Segments = new List<Segment> { segment } }, out total);
				if (results.Any(m => m.Id == organizationId))
				{
					retval.AddRange(results);
				}
			}
			return retval.Distinct();
		}

		public IEnumerable<LuceneSearchResult> GetOrganizationsContainingTerm(IList<Segment> segments, IList<Guid> purchasedOrganizations, string term)
		{
			var builder = new StringBuilder();
			var writeAnd = false;
			if (segments.Any())
			{
				builder.AppendLine(this.AppendSegmentSearch(segments, purchasedOrganizations, ref writeAnd));
			}
			// remove any non alpha-numeric character 
			//term = Regex.Replace(term, @"[^A-Za-z0-9 ]", string.Empty); // might want to replace this with a space instead
			var termSplit = EscapeAndSplitSearchTerms(term);
			foreach (var split in termSplit)
			{
				if (writeAnd)
				{
					builder.Append(" AND ");
				}
				builder.Append(string.Format("{0}:*{1}*", SearchField.OrganizationName.GetFieldName(), split));
				writeAnd = true;
			}
			int total;
			return this.GetOrganizationsFromLuceneString(builder.ToString(), out total);
		}

		public IEnumerable<Data.Models.Organization> Search(SearchFilter filter, out int total)
		{
			var orderedIds = this.GetOrganizationIdsFromLucene(filter, out total).Skip(filter.Skip).Take(filter.Take).ToList();
			var organizations = OrganizationRepository.FindAll(m => orderedIds.Contains(m.Id)).ToList().OrderBy(m => orderedIds.IndexOf(m.Id));
			
			return organizations;
		}

		private IEnumerable<LuceneSearchResult> GetOrganizationsFromLuceneString(string queryString, out int total)
		{
			var query = ParseQueryString(queryString, new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));

			var sort = new Sort(new SortField(SearchField.OrganizationName.GetFieldName() + "Sort", SearchField.OrganizationName.GetLuceneFieldAttribute().SortField, false));
			var directoryInfo = new DirectoryInfo(IndexService.IndexPath);
			var directory = FSDirectory.Open(directoryInfo);
			var reader = IndexReader.Open(directory, true);
			var searcher = new IndexSearcher(directory, true);
			var collector = TopFieldCollector.Create(sort, reader.MaxDoc, false, false, false, false);

			searcher.Search(query, collector);
			var results = collector.TopDocs();
			total = results.TotalHits;

			var docs = results.ScoreDocs.OrderBy(m => m.Score).Select(m => m.Doc);

			var retval = new List<LuceneSearchResult>();
			foreach (var doc in docs)
			{
				// Id
				var orgId = new Guid(searcher.Doc(doc).Get(SearchField.OrganizationId.GetFieldName()));

				// only add distinct values
				if (retval.Any(a => a.Id == orgId))
				{
					continue;
				}

				// Quantity
				var quantity = new double?();
				var pulledQuantity = searcher.Doc(doc).Get(SearchField.OrganizationQuantity.GetFieldName());
				if (!pulledQuantity.IsNullOrWhiteSpace())
				{
					quantity = Convert.ToDouble(pulledQuantity);
				}

				// Quality
				var quality = new double?();
				var pulledQuality = searcher.Doc(doc).Get(SearchField.OrganizationQuality.GetFieldName());
				if (!pulledQuality.IsNullOrWhiteSpace())
				{
					quality = Convert.ToDouble(pulledQuality);
				}

				// LastUpdated
				var lastUpdated = new DateTime?();
				var pulledLastUpdated = searcher.Doc(doc).Get(SearchField.OrganizationLastUpdated.GetFieldName());
				if (!pulledLastUpdated.IsNullOrWhiteSpace())
				{
					lastUpdated = Lucene.Net.Documents.DateTools.StringToDate(pulledLastUpdated);
				}

				// ExpectedNextFundraise
				var pulledExpectedFundraise = searcher.Doc(doc).Get(SearchField.OrganizationExpectedNextFundraise.GetFieldName());
				var expectedFundraise = String.IsNullOrWhiteSpace(pulledExpectedFundraise) ? new DateTime?() : Lucene.Net.Documents.DateTools.StringToDate(pulledExpectedFundraise);
				var fundraisingStatus = searcher.Doc(doc).Get(SearchField.OrganizationFundraisingStatus.GetFieldName());

				// IsPublished
				var isPublished = Convert.ToBoolean(searcher.Doc(doc).Get(SearchField.OrganizationPublishProfile.GetFieldName()));

				// Name
				var name = searcher.Doc(doc).Get(SearchField.OrganizationName.GetFieldName());

				// Focus radar
				var focusRadar = searcher.Doc(doc).Get(SearchField.FocusRadar.GetFieldName());

				// Positions
				IEnumerable<LatitudeAndLongitude> position = new List<LatitudeAndLongitude>();

				var searchDoc = searcher.Doc(doc).Get(SearchField.OrganizationPosition.GetFieldName());
				if (searchDoc != null)
				{
					position = searchDoc.Split(new[] { '|' }).Where(m => !string.IsNullOrWhiteSpace(m)).Select(m => new LatitudeAndLongitude
					{
						Latitude = Convert.ToDouble(m.Split(new[] { ',' })[0]),
						Longitude = Convert.ToDouble(m.Split(new[] { ',' })[1]),
						City = m.Split(new[] { ',' })[2]
					});
				}

				// assign LuceneSearchResult
				if (!string.IsNullOrWhiteSpace(name))
				{

					retval.Add(new LuceneSearchResult { Id = orgId, Name = name, Quantity = quantity, Quality = quality, LastUpdated = lastUpdated, ExpectedNextFundraise = expectedFundraise, FundraisingStatus = fundraisingStatus, IsPublished = isPublished, Positions = position, FocusRadar = focusRadar });
				}
			}
			return retval;
		}

		private static Query ParseQueryString(string queryString, Analyzer analyzer)
		{
			if (String.IsNullOrWhiteSpace(queryString))
			{
				queryString = string.Format("{0}:{1}", SearchField.IsOrganization.GetFieldName(), "true");
			}

			// just using a standard parser since we are creating our own query
			var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, SearchField.FundName.GetFieldName(),
										 analyzer);

			Query query;
			try
			{
				parser.AllowLeadingWildcard = true;
				query = parser.Parse(queryString);
			}
			catch (ParseException e)
			{
				query = parser.Parse(QueryParser.Escape(queryString));
			}
			return query;
		}

		private IEnumerable<Guid> GetOrganizationIdsFromLucene(SearchFilter filter, out int total)
		{
			var queryString = this.BuildQueryString(filter);
			if (String.IsNullOrWhiteSpace(queryString))
			{
				queryString = string.Format("{0}:{1}", SearchField.IsOrganization.GetFieldName(), "true");
			}
			System.Diagnostics.Debug.WriteLine(queryString);

			// just using a standard parser since we are creating our own query
			var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, SearchField.FundName.GetFieldName(),
										 new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));

			Query query;
			try
			{
				parser.AllowLeadingWildcard = true;
				query = parser.Parse(queryString);
			}
			catch (ParseException e)
			{
				query = parser.Parse(QueryParser.Escape(queryString));
			}

			var sort = new Sort(new SortField(filter.SortField.GetFieldName() + "Sort", filter.SortField.GetLuceneFieldAttribute().SortField, filter.IsDescending));
			var directoryInfo = new DirectoryInfo(IndexService.IndexPath);
			var directory = FSDirectory.Open(directoryInfo);
			var reader = IndexReader.Open(directory, true);
			var searcher = new IndexSearcher(directory, true);
			var collector = TopFieldCollector.Create(sort, reader.MaxDoc, false, false, false, false);
			searcher.Search(query, collector);
			var results = collector.TopDocs();
			//total = results.TotalHits;
			var docs = results.ScoreDocs.OrderBy(m => m.Score).Select(m => m.Doc);
			var retval = docs.Select(m => new Guid(searcher.Doc(m).Get(SearchField.OrganizationId.GetFieldName()))).Distinct().ToList();
			total = retval.Count();
			return retval;
		}

		private string BuildQueryString(SearchFilter filter)
		{
			var builder = new StringBuilder();
			var writeAnd = false;

			// segements
			if (filter.Segments.Any())
			{
				builder.Append(AppendSegmentSearch(filter.Segments.ToList(), filter.PurchasedOrganizationIds.ToList(), ref writeAnd));

			}

			// terms
			if (!string.IsNullOrWhiteSpace(filter.Term))
			{
				if (writeAnd)
				{
					builder.Append(" AND ");
				}
				builder.Append("(");
				string[] splitTerm = EscapeAndSplitSearchTerms(filter.Term);

				for (var i = 0; i < this.fieldsToTermSearch.Count(); i++)
				{
					builder.Append("(");
					for (var j = 0; j < splitTerm.Length; j++)
					{
						builder.Append(string.Format("{0}:*{1}*", this.fieldsToTermSearch[i].GetFieldName(), splitTerm[j].ToLower()));
						if (j + 1 < splitTerm.Length)
						{
							builder.Append(" AND ");
						}
					}
					builder.Append(")");
					if (i + 1 < this.fieldsToTermSearch.Count())
					{
						builder.Append(" OR ");
					}
				}
				builder.Append(")");
				writeAnd = true;
			}

			// other Search fields
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationStrategies, filter.Strategies, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationSubStrategy, filter.SubStrategies, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationInvestmentSubRegion, filter.InvestmentSubRegions, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationCountries, filter.Countries, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationQualitativeGrades, filter.QualitativeGrades, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationQuantitativeGrades, filter.QuantitativeGrades, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationMarketStage, filter.MarketStage, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationSectorFocus, filter.Sector, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationInvestmentRegion, filter.InvestmentRegions, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationFundraisingStatus, filter.FundraisingStatus, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationNumberOfFunds, filter.NumberOfFundsClosedMinimum, filter.NumberOfFundsClosedMaximum, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationDiligenceLevel, filter.DiligenceStatusMinimum, filter.DiligenceStatusMaximum, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationEmergingManager, filter.EmergingManager, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationFocusList, filter.FocusList, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationSectorSpecialist, filter.SectorSpecialist, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationAccessConstrained, filter.AccessConstrained, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.OrganizationSBICFund, filter.SBICFund, ref writeAnd));
			builder.Append(AppendFieldExactSearch(SearchField.FocusRadar, filter.FocusRadar, ref writeAnd));

			// published filter
			if (filter.ShowOnlyPublished)
			{
				if (writeAnd)
				{
					builder.Append(" AND ");
				}
				builder.Append("(");
				builder.Append(string.Format("{0}:{1}", SearchField.OrganizationPublishProfile.GetFieldName(), filter.ShowOnlyPublished.ToString()));
				builder.Append(")");
				writeAnd = true;
			}

			// special date range logic
			if (filter.ExpectedNextFundraiseStartDate.HasValue || filter.ExpectedNextFundraiseEndDate.HasValue)
			{
				if (writeAnd)
				{
					builder.Append(" AND ");
				}
				builder.Append("(");
				var startString = Lucene.Net.Documents.DateTools.DateToString(
										  filter.ExpectedNextFundraiseStartDate.GetValueOrDefault(DateTime.Now),
										  Lucene.Net.Documents.DateTools.Resolution.DAY);

				var endString = filter.ExpectedNextFundraiseEndDate.HasValue
									  ? Lucene.Net.Documents.DateTools.DateToString(
										  filter.ExpectedNextFundraiseEndDate.Value,
										  Lucene.Net.Documents.DateTools.Resolution.DAY)
									  : "99999999";

				// if start date is today, also search on 'open' fundraising status
				if (filter.ExpectedNextFundraiseStartDate.GetValueOrDefault(DateTime.Now.Date) == DateTime.Now.Date)
				{
					builder.Append(string.Format("{0}:{1} OR {0}:{2} OR ", SearchField.OrganizationFundraisingStatus.GetFieldName(), "open", "\"upcoming final close\""));
				}
				builder.Append(string.Format("{0}:[{1} TO {2}]", SearchField.OrganizationExpectedNextFundraise.GetFieldName(),
											 startString, endString));
				builder.Append(")");
				writeAnd = true;
			}

			// fund size
			if (filter.FundSizeMinimum.HasValue || filter.FundSizeMaximum.HasValue)
			{
				string advancedFundQuery = string.Format("{0}:[{1} TO {2}] AND ({3}:\"{4}\" OR {5}:\"{6}\")",
														 SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
														 filter.FundSizeMinimum.HasValue
															 ? filter.FundSizeMinimum.Value.ToString("F").PadLeft(10, '0')
															 : "0.00".PadLeft(10, '0'),
														 filter.FundSizeMaximum.HasValue
															 ? filter.FundSizeMaximum.Value.ToString("F").PadLeft(10, '0')
															 : "9".PadRight(7, '9') + ".99",
															 SearchField.OrganizationCurrency.GetFieldName(),
															 filter.Currencies.Any() ? filter.Currencies.FirstOrDefault() : String.Empty,
															 SearchField.FundCurrency.GetFieldName(),
															 filter.Currencies.Any() ? filter.Currencies.FirstOrDefault() : String.Empty);

				if (writeAnd)
				{
					builder.Append(String.Format(" AND ({0})", advancedFundQuery));
				}
				else
				{
					builder.Append(advancedFundQuery);
				}

				writeAnd = true;
			}

			System.Diagnostics.Debug.WriteLine("LUCENE QUERY STRING:  " + builder.ToString());
			return builder.ToString();
		}

		private string AppendSegmentSearch(IList<Segment> segments, IList<Guid> purchasedOrganizations, ref bool writeAnd)
		{
			if (!segments.Any())
			{
				return string.Empty;
			}

			var retval = new StringBuilder();
			retval.Append("(");
			var getSearchFieldFilter = new Func<FieldType, List<String>, string>((field, values) =>
					 {
						 var searchFieldReturn = new StringBuilder();
						 searchFieldReturn.Append("(");
						 SearchField type;
						 switch (field)
						 {
							 case FieldType.Strategy:
								 type = SearchField.OrganizationStrategies;
								 break;
							 case FieldType.MarketStage:
								 type = SearchField.OrganizationMarketStage;
								 break;
							 case FieldType.InvestmentRegion:
							 case FieldType.SubRegion:
								 type = SearchField.OrganizationInvestmentRegion;
								 break;
							 case FieldType.Country:
								 type = SearchField.OrganizationCountries;
								 break;
							 default:
								 // unknown filter
								 throw new ApplicationException("Unknown segment search field type");
								 break;
						 }
						 var valuesList = values.Select(m => string.Format("{0}:\"{1}\"", type.GetFieldName(), m.ToLower())).ToList();
						 searchFieldReturn.Append(string.Join(" OR ", valuesList));
						 searchFieldReturn.Append(")");
						 return searchFieldReturn.ToString();
					 });
			var getPairFieldFilter = new Func<Dictionary<FieldType, List<string>>, string>((pairs) =>
					{
						var pairFieldReturn = new StringBuilder();
						pairFieldReturn.Append("(");
						pairFieldReturn.Append(String.Join(" AND ", pairs.Select(m => getSearchFieldFilter(m.Key, m.Value))));
						pairFieldReturn.Append(")");
						return pairFieldReturn.ToString();
					});

			var filters = segments.Select(m => getPairFieldFilter(m.Filters));
			retval.Append(string.Join(" OR ", filters));
			if (purchasedOrganizations != null && purchasedOrganizations.Any())
			{
				retval.Append(" OR (");
				retval.Append(string.Format("({0}:{1} AND (", SearchField.IsOrganization.GetFieldName(), "true"));
				retval.Append(string.Join(" OR ",
													 purchasedOrganizations.Select(
														 m =>
														 string.Format("{0}:\"{1}\"",
																	   SearchField.OrganizationId.GetFieldName(),
																	   m.ToString()))));
				retval.Append(")))");
			}
			retval.Append(")");
			writeAnd = true;
			return retval.ToString().Replace("() OR ", string.Empty);
		}

		private string AppendFieldExactSearch(SearchField field, string value, ref bool writeAnd)
		{
			var retval = string.Empty;
			if (!string.IsNullOrEmpty(value))
			{
				if (writeAnd)
				{
					retval += " AND ";
				}
				retval += (string.Format("{0}:{1}", field.GetFieldName(), value));
				writeAnd = true;
			}
			return retval;
		}

		private string AppendFieldExactSearch(SearchField field, IEnumerable<string> values, ref bool writeAnd)
		{
			if (values == null)
				return string.Empty;
			var vals = values.Where(m => !String.IsNullOrWhiteSpace(m)).ToArray();
			var retval = string.Empty;
			if (vals.Any())
			{
				if (writeAnd)
				{
					retval += " AND ";
				}
				retval += "(";
				for (var i = 0; i < vals.Count(); i++)
				{
					retval += (string.Format("{0}:\"{1}\"", field.GetFieldName(), vals[i]));
					if (i < vals.Count() - 1)
					{
						retval += " OR ";
					}
				}
				retval += ")";
				writeAnd = true;
			}
			return retval;
		}

		private string AppendFieldExactSearch(SearchField field, bool? value, ref bool writeAnd)
		{
			var retval = string.Empty;
			if (value.HasValue)
			{
				if (writeAnd)
				{
					retval += " AND ";
				}
				retval += (string.Format("{0}:{1}", field.GetFieldName(), value.Value.ToString()));
				writeAnd = true;
			}
			return retval;
		}

		private string AppendFieldExactSearch(SearchField field, int? minimum, int? maximum, ref bool writeAnd)
		{
			var retval = string.Empty;
			if (minimum.HasValue || maximum.HasValue)
			{
				if (writeAnd)
				{
					retval += " AND ";
				}
				retval += (string.Format("{0}:[{1} TO {2}]",
					field.GetFieldName(),
					minimum.HasValue ? minimum.Value.ToString().PadLeft(10, '0') : string.Empty.PadLeft(10, '0'),
					maximum.HasValue ? maximum.Value.ToString().PadLeft(10, '0') : "9".PadRight(10, '9')));
				writeAnd = true;
			}
			return retval;
		}
		private string AppendFieldExactSearch(SearchField field, double? minimum, double? maximum, ref bool writeAnd)
		{
			var retval = string.Empty;
			if (minimum.HasValue || maximum.HasValue)
			{
				if (writeAnd)
				{
					retval += " AND ";
				}
				retval += (string.Format("{0}:[{1} TO {2}]",
					field.GetFieldName(),
					minimum.HasValue ? minimum.Value.ToString("N2").PadLeft(10, '0') : "0.00".PadLeft(10, '0'),
					maximum.HasValue ? maximum.Value.ToString("N2").PadLeft(10, '0') : "9".PadRight(7, '9') + ".99"));
				writeAnd = true;
			}
			return retval;
		}

		private string[] EscapeAndSplitSearchTerms(string term)
		{
			term = term.Trim();

			// + - && || ! ( ) { } [ ] ^ " ~ * ? :
			string[] escapeChars = { "&", "|", "!", "^", "\"", "~", "*", "?", "+" };
			string[] replaceChars = { "(", ")", "{", "}", "[", "]", ".", "-", "," };
			//remove replace charaters
			foreach (var ec in replaceChars)
			{
				term = term.Replace(ec, " ");
			}
			//escape operator charaters
			foreach (var ec in escapeChars)
			{
				term = term.Replace(ec, QueryParser.Escape(ec));
			}

			var escapedChars = escapeChars.Select(x => QueryParser.Escape(x)).ToList();

			//split and return excluding empty and singled out escape characters
			var termSplit = term.Split(new[] { ' ' }).Where(m => !String.IsNullOrWhiteSpace(m)).ToList();

			//only remove escaped charaters when there are more then 1 search term
			if (termSplit.Count() > 1)
			{
				termSplit = termSplit.Where(m => !escapedChars.Any(x => x == m)).ToList();
			}

			return termSplit.ToArray();
		}
	}
}
