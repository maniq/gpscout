﻿using System;

namespace AtlasDiligence.Common.DTO.Model
{
    public class Group : ServiceModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? GroupLeaderWebUserId { get; set; }
        public string GroupLeaderWebUserName { get; set; }
        public int MaxUsers { get; set; }

        public Group()
        {
            Id = Guid.NewGuid();
        }
    }
}
