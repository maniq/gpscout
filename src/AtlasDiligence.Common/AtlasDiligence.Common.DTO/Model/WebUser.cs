﻿using System;


namespace AtlasDiligence.Common.DTO.Model
{
    public class WebUser : ServiceModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsValidated { get; set; }

        /// <summary>
        /// Password initially set in CRM for user.
        /// </summary>
        /// <remarks>Only used when a user is Created in web Membership.</remarks>
        public string InitialPassword { get; set; }

        /// <summary>
        /// Is RssApp activated for Web User?
        /// </summary>
        public bool RssApp { get; set; }

        /// <summary>
        /// Is DiligenceApp activated for Web User?
        /// </summary>
        public bool DiligenceApp { get; set; }

        public Guid? GroupId { get; set; }
        public string GroupName { get; set; }
        public Group Group { get; set; }
        public WebUser()
        {
            Id = Guid.NewGuid();
        }
    }
}
