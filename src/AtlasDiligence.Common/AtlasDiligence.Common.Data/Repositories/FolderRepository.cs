﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class FolderRepository : Repository<Folder>, IFolderRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Folder> GetByUserId(Guid id)
        {
            var folders = DataContext.Folders.Where(m => m.UserId == id).ToList();
            if (!folders.Any(m => m.Name == "Favorites"))
            {
                var favoritesFolder = new Folder { UserId = id, Id = Guid.NewGuid(), Name = "Favorites" };
                DataContext.Folders.InsertOnSubmit(favoritesFolder);
                DataContext.SubmitChanges();
                folders.Add(favoritesFolder);
            }
            return folders;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="organizationId"></param>
        public void AddOrganizationToFolder(Guid folderId, Guid organizationId)
        {
            if (DataContext.FolderOrganizations.Any(m => m.FolderId == folderId && m.OrganizationId == organizationId))
                return;
            DataContext.FolderOrganizations.InsertOnSubmit(new FolderOrganization { FolderId = folderId, OrganizationId = organizationId });
            DataContext.SubmitChanges();
        }

        public void AddOrganizationsToFolder(Guid folderId, IEnumerable<Guid> organizationIds)
        {
            foreach (var organizationId in organizationIds)
            {
                if (DataContext.FolderOrganizations.Any(m => m.FolderId == folderId && m.OrganizationId == organizationId))
                    continue;
                DataContext.FolderOrganizations.InsertOnSubmit(new FolderOrganization { FolderId = folderId, OrganizationId = organizationId });
            }
            DataContext.SubmitChanges();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="organizationId"></param>
        public void RemoveOrganizationFromFolder(Guid folderId, Guid organizationId)
        {
            var folderOrganization =
                DataContext.FolderOrganizations.SingleOrDefault(m => m.FolderId == folderId && m.OrganizationId == organizationId);
            if (folderOrganization == null) return;
            DataContext.FolderOrganizations.DeleteOnSubmit(folderOrganization);
            DataContext.SubmitChanges();
        }

        public IQueryable<Organization> GetOrganizationByName(string organizationName)
        {
            return DataContext.Organizations.Where(m => m.Name == organizationName && (m.PublishOverviewAndTeam || m.PublishSearch));
        }

        public IQueryable<Organization> GetOrganizationsStartsWith(string request)
        {
            return DataContext.Organizations.Where(m => m.Name.Contains(request) && m.PublishOverviewAndTeam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void RemoveFolder(Guid id)
        {
            var folder = this.GetById(id);
            if (folder == null) return;
            DataContext.FolderClicks.DeleteAllOnSubmit(DataContext.FolderClicks.Where(m => m.FolderId == id));
            DataContext.Folders.DeleteOnSubmit(folder);
            DataContext.SubmitChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        public void AddFolder(Folder folder)
        {
            DataContext.Folders.InsertOnSubmit(folder);
            DataContext.SubmitChanges();
        }

        public void RegisterUserClick(Guid userId, Guid folderId)
        {
            // If user/folder combo exists, grab it.
            var userFolderClick = DataContext.FolderClicks.SingleOrDefault(f => f.UserId == userId && f.FolderId == folderId);

            // If it doesn't already exist, create one.
            if (userFolderClick == null)
            {
                userFolderClick = new FolderClick
                {
                    UserId = userId,
                    FolderId = folderId,
                    LastOpened = DateTime.Now
                };

                DataContext.FolderClicks.InsertOnSubmit(userFolderClick);
            }
            else // If one exists, update the date
            {
                userFolderClick.LastOpened = DateTime.Now;
            }

            // Save
            DataContext.SubmitChanges();
        }

        public IQueryable<Folder> GetRecentFoldersForUser(Guid userId, int numToTake)
        {
            return DataContext.FolderClicks.Where(f => f.UserId == userId).OrderByDescending(f => f.LastOpened).Take(numToTake).Select(f => f.Folder);
        }
    }
}
