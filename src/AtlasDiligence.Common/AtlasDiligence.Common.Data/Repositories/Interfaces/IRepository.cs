﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Linq.Expressions;
using System.Data.Linq;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(Guid id);
        void Attatch(T oldEntity, T newEntity);
        void DeleteAllOnSubmit(IQueryable<T> entities);
        void DeleteAllOnSubmit(Expression<Func<T, bool>> expression);
        void DeleteOnSubmit(T entity);
        void InsertOnSubmit(T entity);
        void InsertAllOnSubmit(IEnumerable<T> entities);
        void SubmitChanges();

        IQueryable<T> FindAll(Expression<Func<T, bool>> expression);


        IQueryable<T> Paginate(IQueryable<T> retval, string sortCol, bool sortAsc, int startResult, int maxResults);

        /// <summary>
        /// overload with expressions tree
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="sortCol"></param>
        /// <param name="sortAsc"></param>
        /// <param name="startResult"></param>
        /// <param name="maxResults"></param>
        /// <returns></returns>
        IQueryable<T> Paginate(Expression<Func<T, bool>> expression, string sortCol, bool sortAsc, int startResult,
                                int maxResults);
    }
}
