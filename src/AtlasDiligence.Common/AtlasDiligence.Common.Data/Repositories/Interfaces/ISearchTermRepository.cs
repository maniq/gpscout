﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface ISearchTermRepository : IRepository<SearchTerms>
    {
        void Add(SearchTerms searchTerm);
        void DeleteAllForUser(Guid userId);
        IEnumerable<SearchTerms> GetAllByUserId(Guid userId);
        IEnumerable<String> GetSearchTermsForUser(aspnet_User user);

        /// <summary>
        /// Overload with just userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<String> GetSearchTermsForUser(Guid userId);

        IDictionary<String, IEnumerable<String>> GetSearchTermAndAliasesForUser(Guid userId);

        /// <summary>
        /// Delete for given search term string for user.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="userId"></param>
        void Delete(string term, Guid userId);
    }
}
