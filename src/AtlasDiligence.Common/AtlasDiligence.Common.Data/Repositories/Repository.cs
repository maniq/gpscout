﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        protected AtlasWebDbDataContext DataContext { get; private set; }

        public Repository()
        {
            DataContext = DataContext ?? new AtlasWebDbDataContext();
#if DEBUG
            DataContext.Log = new AtlanticBT.Common.Instrumentation.DebugConsoleStream();
#endif
            var loadOptions = DataContext.LoadOptions ?? new DataLoadOptions();
            loadOptions.LoadWith((Fund m) => m.Currency);
            //loadOptions.LoadWith((Organization m) => m.DueDiligenceItems);
            //loadOptions.LoadWith((Organization m) => m.Funds);
            //loadOptions.LoadWith((Organization m) => m.Evaluations);
            //loadOptions.LoadWith((Organization m) => m.OtherAddresses);
            //loadOptions.LoadWith((Organization m) => m.PortfolioCompanies);
            loadOptions.LoadWith((OrganizationMember m) => m.EducationHistories);
            loadOptions.LoadWith((OrganizationMember m) => m.EmploymentHistories);
            loadOptions.LoadWith((OrganizationMember m) => m.BoardSeats);
            loadOptions.LoadWith((SearchTemplate m) => m.SearchTemplateFilters);
            //loadOptions.LoadWith((Folder m) => m.FolderOrganizations);
            //loadOptions.LoadWith((FolderOrganization m) => m.Organization);
            loadOptions.LoadWith((aspnet_User m) => m.aspnet_Membership);
            loadOptions.LoadWith((aspnet_User m) => m.UserExt);
            loadOptions.LoadWith((Group m) => m.GroupOrganizations);
            DataContext.LoadOptions = loadOptions;
        }

        private ITable GetTable()
        {
            return DataContext.GetTable<T>();
        }

        #region IRepository<T> Members

        public IQueryable<T> GetAll()
        {
            return DataContext.GetTable<T>().AsQueryable();
        }

        /// <summary>
        /// Returns a single entry or null if none exist. Will throw an error if more than one entry has the same id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetById(Guid id)
        {
            var itemParameter = Expression.Parameter(typeof(T), "item");
            var whereExpression = Expression.Lambda<Func<T, bool>>
                (
                Expression.Equal(
                    Expression.Property(
                        itemParameter,
                        GetPrimaryKey()
                        ),
                    Expression.Constant(id)
                    ),
                new[] { itemParameter }
                );
            return GetAll().Where(whereExpression).SingleOrDefault();
        }

        /// <summary>
        /// Returns primary key name for entity object.
        /// </summary>
        /// <returns></returns>
        private string GetPrimaryKey()
        {
            var type = this.DataContext.Mapping.GetMetaType(typeof(T));

            var id = type.DataMembers.Where(x => x.IsPrimaryKey).Select(y => y.Name).Single();

            return id;
        }

        public void Attatch(T oldEntity, T newEntity)
        {
            GetTable().Attach(newEntity, oldEntity);
        }

        /// <summary>
        /// Delete all given entities in a batch query.
        /// </summary>
        /// <param name="entities"></param>
        public void DeleteAllOnSubmit(IQueryable<T> entities)
        {
            DataContext.GetTable<T>().DeleteBatch(entities);
        }

        /// <summary>
        /// Delete all entities in expression
        /// </summary>
        /// <param name="expression"></param>
        public void DeleteAllOnSubmit(Expression<Func<T, bool>> expression)
        {
            DataContext.GetTable<T>().DeleteBatch(expression);
        }

        public void DeleteOnSubmit(T entity)
        {
            GetTable().DeleteOnSubmit(entity);
        }

        public void InsertOnSubmit(T entity)
        {
            GetTable().InsertOnSubmit(entity);
        }

        public void InsertAllOnSubmit(IEnumerable<T> entities)
        {
            GetTable().InsertAllOnSubmit(entities);
        }

        public void SubmitChanges()
        {
            SaveAudits();
            DataContext.SubmitChanges();
        }

        public IQueryable<T> FindAll(Expression<Func<T, bool>> expression)
        {
            return DataContext.GetTable<T>().Where(expression);
        }


        public IQueryable<T> Paginate(Expression<Func<T, bool>> expression, string sortCol, bool sortAsc, int startResult, int maxResults)
        {
            var retval = FindAll(expression);

            return Paginate(retval, sortCol, sortAsc, startResult, maxResults);
        }

        public IQueryable<T> Paginate(IQueryable<T> retval, string sortCol, bool sortAsc, int startResult, int maxResults)
        {
            if (!string.IsNullOrEmpty(sortCol))
            {
                if (!sortAsc)
                {
                    retval = retval.OrderBy(sortCol + " DESC");
                }
                else
                {
                    retval = retval.OrderBy(sortCol);
                }
            }

            if (startResult > 0)
            {
                retval = retval.Skip(startResult);
            }

            if (maxResults > 0)
            {
                retval = retval.Take(maxResults);
            }

            return retval;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            DataContext.Dispose();
        }

        #endregion

        #region Audit Private Methods
        private void SaveAudits()
        {
            foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Updates.OfType<IAuditable>())
            {
                MapAudit(modifiedEntity, AuditType.Update);
            }

            foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Inserts.OfType<IAuditable>())
            {
                MapAudit(modifiedEntity, AuditType.Insert);
            }

            foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Deletes.OfType<IAuditable>())
            {
                MapAudit(modifiedEntity, AuditType.Delete);
            }
        }

        private void MapAudit(IAuditable modifiedEntity, AuditType auditType)
        {
            Type type = modifiedEntity.GetType();
            string tableName = type.Name;
            var entityId = type.GetProperty(GetPrimaryKey()).GetValue(modifiedEntity, null);

            Models.Audit audit = new Models.Audit();
            audit.Id = Guid.NewGuid();
            audit.Modified = modifiedEntity.Modified;
            audit.ModifiedBy = modifiedEntity.ModifiedBy;
            audit.ModifiedTable = tableName;
            audit.ModifiedEntity = entityId is Guid ? (Guid)entityId : Guid.Empty;
            audit.AuditType = (int)auditType;

            DataContext.GetTable<Models.Audit>().InsertOnSubmit(audit);
        }
        #endregion
    }
}
