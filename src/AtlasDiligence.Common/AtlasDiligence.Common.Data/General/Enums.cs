﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Common.Data.General
{
    /// <summary>
    /// For database audit table
    /// </summary>
    public enum AuditType
    {
        Insert,
        Update,
        Delete
    }

    public enum NewsEntityType
    {
        Organization = 1,
        Contact
    }

    /// <summary>
    /// Used in RssEmailFrequency
    /// </summary>
    public enum EmailFrequency
    {
        [Description("day")]
        Day = 1,
        [Description("week")]
        Week = 2,
        [Description("month")]
        Month = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EntityType
    {
        Organization = 1,
        Fund
    }

    public enum RequestType
    {
        Queue, Priority, Diligence, Introduction
    }

    public enum Grades
    {
        Poor = 1,
        Weak,
        Standard,
        Strong,
        Exceptional
    }

    /// <summary>
    /// Field Type maps to field name. For referencing fields in db lookup table
    /// </summary>
    public enum FieldType
    {
        [Description("Sector Focus")]
        SectorFocus = 1,
        Strategy,
        [Description("Geographic Focus")]
        GeographicFocus,

        [Description("Market/Stage")]
        MarketStage,
        [Description("Fundraising Status")]
        FundraisingStatus,
        [Description("Fund Size Minimum")]
        FundSizeMin,
        [Description("Fund Size Maximum")]
        FundSizeMax,
        [Description("Number of Funds Closed Minimum")]
        NumberOfFundsClosedMin,
        [Description("Number of Funds Closed Maximum")]
        NumberOfFundsClosedMax,

        [Description("Diligence Status Minimum")]
        DiligenceStatusMin,
        [Description("Diligence Status Maximum")]
        DiligenceStatusMax,
        [Description("Emerging Manager")]
        EmergingManager,
        [Description("Focus List")]
        FocusList,
        [Description("Access Constrained")]
        AccessConstrained,

        Search,
        [Description("Investment Region")]
        InvestmentRegion,
        [Description("Investment Sub-Region")]
        SubRegion,
        Country,
        [Description("Expected Next Fundraise Start Date")]
        ExpectedNextFundraiseStartDate,
        [Description("Expected Next Fundraise End Date")]
        ExpectedNextFundraiseEndDate,
        [Description("Limit to Firms with Profiles")]
        ShowOnlyPublished,
        [Description("Sub-Strategy")]
        SubStrategy,
        [Description("Investment Sub-Region")]
        InvestmentSubRegion,
        [Description("Quantitative Grade")]
        QuantitativeGrade,
        [Description("Qualitative Grade")]
        QualitativeGrade,
        [Description("Sector Specialist")]
        SectorSpecialist,
        [Description("Currency")]
        Currency,
        [Description("SBIC Fund")]
        SBICFund,
        [Description("Expected Next Fundraise")]
        SelectedNextFundraiseValue
    }

	public enum TrackUsersType
	{
		[Description("Logins")]
		Login,

		[Description("Logouts")]
		Logout,

		[Description("Profile Views")]
		ProfileVisit
	}
}
