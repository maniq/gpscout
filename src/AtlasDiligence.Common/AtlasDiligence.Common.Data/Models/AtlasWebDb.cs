﻿using System.Configuration;

namespace AtlasDiligence.Common.Data.Models
{
    using System.Configuration;

    public partial class AtlasWebDbDataContext
    {
        public AtlasWebDbDataContext()
            : base(ConfigurationManager.ConnectionStrings["atlasdilig_01ConnectionString"].ConnectionString)
        {
            this.OnCreated();
        }
    }
    #region Auditables
    /// <summary>
    /// Auditable Tables. Adding a partial class that inherits IAuditable will include this data model
    /// in entities that will be added to the audit table on CUD.
    /// <remarks>
    /// The associated data table must have Modified (DateTime) and ModifiedBy (Guid) fields in the dB.
    /// </remarks>
    /// </summary>

    #endregion
}