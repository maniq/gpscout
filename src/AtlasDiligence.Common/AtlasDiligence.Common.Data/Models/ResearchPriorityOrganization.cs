﻿// -----------------------------------------------------------------------
// <copyright file="ResearchPriorityOrganization.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace AtlasDiligence.Common.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public partial class ResearchPriorityOrganization
    {
        public IEnumerable<string> Strategy
        {
            get { return string.IsNullOrWhiteSpace(this.StrategyPipeDelimited) ? new string[0] : this.StrategyPipeDelimited.Split(new[] { '|' }); }
        }

        public IEnumerable<string> InvestmentRegions
        {
            get { return string.IsNullOrWhiteSpace(this.InvestmentRegionPipeDelimited) ? new string[0] : this.InvestmentRegionPipeDelimited.Split(new[] { '|' }); }
        }
    }
}
