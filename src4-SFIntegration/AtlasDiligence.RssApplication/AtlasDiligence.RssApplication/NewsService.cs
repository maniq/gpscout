﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using log4net;
using GPScout.Domain.Contracts.Services;
using GPScout.Domain.Implementation.Services;
using GPScout.Domain.Contracts.Models;

namespace AtlasDiligence.RssApplication
{
    public static class NewsService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NewsService));


        public static IEnumerable<KeyValuePair<Guid, List<string>>> GetOrganizationList()
        {
            Log.Debug("Getting organizations");
            IEnumerable<KeyValuePair<Guid, List<string>>> result = new List<KeyValuePair<Guid, List<string>>>();
            try
            {
                result = new OrganizationService().GetAllOrgIdAndAliases();

                Log.Debug("Retrieved " + result.Count() + " organizations");
            }
            catch (Exception e)
            {
                Log.Fatal("Error retrieving the list of organizations.  Inner exception... " + e.Message);
                throw;
            }
            return result;
        }


        public static IEnumerable<KeyValuePair<Guid, List<string>>> GetContactList()
        {
            Log.Debug("Getting contacts");
            IEnumerable<KeyValuePair<Guid, List<string>>> result = new List<KeyValuePair<Guid, List<string>>>();
            try
            {
                result = new OrganizationService().GetAllContactIdAndAliases();

                Log.Debug("Retrieved " + result.Count() + " contacts");
            }
            catch (Exception e)
            {
                Log.Fatal("Error retrieving the list of contacts. Inner exception... " + e.Message);
                throw;
            }
            return result;
        }


        public static void InsertNews(NewsItem newsItem)
        {
            try
            {
                var repo = new NewsRepository();

                repo.AddNews(new News
                                                 {
                                                     Description = newsItem.Content,
                                                     Summary = newsItem.Summary,
                                                     Id = newsItem.Id,
                                                     PublishedDate = newsItem.PublishedDate,
                                                     Subject = newsItem.Subject,
                                                     Url = newsItem.Url,
                                                     OrganizationSourceId = newsItem.OrganizationSourceId,
                                                     OrganizationSourceName = newsItem.OrganizationSourceName,
                                                     DateCreated = newsItem.DateCreated
                                                 });
                foreach (var id in newsItem.OrganizationIds)
                {
                    repo.AddNewsEntity(new NewsEntity
                    {
                        EntityId = id,
                        EntityType = (int)NewsEntityType.Organization,
                        NewsId = newsItem.Id
                    });
                }
                foreach (var id in newsItem.ContactIds)
                {
                    repo.AddNewsEntity(new NewsEntity
                    {
                        EntityId = id,
                        EntityType = (int)NewsEntityType.Contact,
                        NewsId = newsItem.Id
                    });
                }
            }
            catch (Exception e)
            {
                Log.Warn("\n" + "Error creating news item in GPScout database. " + "\n" + "    Article Subject: " + newsItem.Subject + "\n" + "    Inner exception... " + e.Message);
            }
        }

        /// <summary>
        /// Deletes and rebuilds the AssociatedTerms table
        /// </summary>
        /// <param name="entityTerms"></param>
        /// <returns>Number of rows inserted</returns>
        public static void InsertAssociatedTerms(IEnumerable<KeyValuePair<Guid, List<string>>> entityTerms)
        {
            var associatedTermRepo = new Repository<AssociatedTerm>();
            associatedTermRepo.DeleteAllOnSubmit(associatedTermRepo.GetAll());

            var associatedTerms = GetAssociatedTerms(entityTerms);

            associatedTermRepo.InsertAllOnSubmit(associatedTerms);
            associatedTermRepo.SubmitChanges();
        }

        /// <summary>
        /// Returns a normalized list of term associations (AssociatedTerms db entity) from 
        /// a given list of entity/term relations
        /// </summary>
        /// <param name="entityTerms"></param>
        /// <returns></returns>
        public static IEnumerable<AssociatedTerm> GetAssociatedTerms(IEnumerable<KeyValuePair<Guid, List<string>>> entityTerms)
        {
            var associatedTerms = new List<AssociatedTerm>();

            // loop through each entity 
            foreach (var entityTerm in entityTerms.Select(x => x))
            {
                //loop through each value as the source
                var sourceTerms = entityTerm.Value;
                foreach (var sourceTerm in sourceTerms)
                {
                    string sourceTermCleansed = sourceTerm.Trim().ToLower();
                    var sourceAssociatedTerms = sourceTerms.Where(x => x != sourceTerm);
                    //loop through other values than source as associates
                    foreach (var sourceAssociatedTerm in sourceAssociatedTerms)
                    {
                        string sourceAssociatedTermCleansed = sourceAssociatedTerm.Trim().ToLower();
                        if (!associatedTerms.Any(x => x.SourceTerm == sourceTermCleansed && x.AssociatedTerm1 == sourceAssociatedTermCleansed))
                        {
                            associatedTerms.Add(new AssociatedTerm()
                                                    {
                                                        Id = Guid.NewGuid(),
                                                        SourceTerm = sourceTermCleansed,
                                                        AssociatedTerm1 = sourceAssociatedTerm
                                                    });
                        }
                    }
                }
            }
            return associatedTerms;
        }

        /// <summary>
        /// Batch delete all current AssociatedTerms.
        /// </summary>
        internal static void DeleteAllAssociatedTerms()
        {
            var associatedTermRepo = new Repository<AssociatedTerm>();
            associatedTermRepo.DeleteAllOnSubmit(associatedTermRepo.GetAll());
            associatedTermRepo.SubmitChanges();
        }
    }
}
