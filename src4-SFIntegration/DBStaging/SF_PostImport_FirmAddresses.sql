﻿USE [GPScoutSFImportStagingStage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SF_PostImport_FirmAddresses] 
AS
	UPDATE	O
	SET		Address1City = NULL,
			-- Address1Country = NULL,
			Address1 = NULL,
			Address2 = NULL,
			PostalCode = NULL,
			StateProvince = NULL,
			PrimaryOffice = NULL,
			Latitude = NULL,
			Longitude = NULL
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.PrimaryOfficeSFID = FA.Id
	WHERE ISNULL(FA.[Address_Description__c], '') NOT IN ('Headquarters', 'Billing Headquarters')

	UPDATE	O
	SET		Address1City = FA.City__c,
			Address1Country = FA.Country__c,
			Address1 = FA.Street_Address__c,
			Address2 = FA.Address_2__c,
			PostalCode = FA.Zip_Code__c,
			StateProvince = FA.State__c,
			PrimaryOffice = FA.City__c + IIF(FA.State__c IS NOT NULL OR FA.State__c <> '', ', ' + FA.State__c, ''),
			Latitude = FA.Firm_Location_Lat,
			Longitude = FA.Firm_Location_Long,
			PrimaryOfficeSFID = FA.Id
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.SFID = FA.Account__c
	WHERE	FA.[Address_Description__c] = 'Billing Headquarters'

	UPDATE	O
	SET		Address1City = FA.City__c,
			Address1Country = FA.Country__c,
			Address1 = FA.Street_Address__c,
			Address2 = FA.Address_2__c,
			PostalCode = FA.Zip_Code__c,
			StateProvince = FA.State__c,
			PrimaryOffice = FA.City__c + IIF(FA.State__c IS NOT NULL OR FA.State__c <> '', ', ' + FA.State__c, ''),
			Latitude = FA.Firm_Location_Lat,
			Longitude = FA.Firm_Location_Long,
			PrimaryOfficeSFID = FA.Id
	FROM	alexabe_01.dbo.Organization O
	INNER JOIN FirmAddreses FA ON O.SFID = FA.Account__c
	WHERE	FA.[Address_Description__c] = 'Headquarters' AND O.PrimaryOffice IS NULL

	-- delete addresses selected as Primary Office the OtherAddresses
	DELETE OA
	FROM alexabe_01.dbo.OtherAddress OA
	JOIN alexabe_01.dbo.Organization O ON OA.SFID = O.PrimaryOfficeSFID AND O.PrimaryOffice IS NOT NULL
