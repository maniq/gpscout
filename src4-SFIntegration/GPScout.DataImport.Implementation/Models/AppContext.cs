﻿using GPScout.DataImport.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.DataImport.Implementation.Models
{
    public class AppContext : IAppContext
    {
        public int DataImportRespawnSeconds { get; set; }
        public string SSPN_DataImportLastStartDate { get; set; }
        public string SSPN_DataImportFromDateUtc { get; set; }
        public bool SalesforceImportEnabled { get; set; }
        public DateTime DefaultDataImportFromDateUtc { get; set; }
        public string FromEmail { get; set; }
        public string PUCNEmailSubject { get; set; }
        public int SalesForceRequestTimeoutSeconds { get; set; }
        public int SalesForceMaxRetryAttempts { get; set; }


        public AppContext()
        {
            IAppContext appContext = this as IAppContext;
            appContext.SSPN_DataImportLastStartDate = GetAppSetting("SSPN_DataImportLastStartDate");
            appContext.SSPN_DataImportFromDateUtc = GetAppSetting("SSPN_DataImportFromDateUtc");
            appContext.DataImportRespawnSeconds = GetAppSettingInt("DataImportRespawnSeconds", 600);
            appContext.SalesforceImportEnabled = GetAppSettingBool("SalesforceImportEnabled", true);
            appContext.DefaultDataImportFromDateUtc = GetAppSettingDateTime("DefaultDataImportFromDateUtc", DateTime.MinValue);
            appContext.FromEmail = GetAppSetting("FromEmail");
            appContext.PUCNEmailSubject = GetAppSetting("PUCNEmailSubject");
            appContext.SalesForceRequestTimeoutSeconds = GetAppSettingInt("SalesForceRequestTimeoutSeconds", 600);
            appContext.SalesForceMaxRetryAttempts = GetAppSettingInt("SalesForceMaxRetryAttempts", 3);
        }

        private DateTime GetAppSettingDateTime(string key, DateTime defaultValue)
        {
            DateTime dt;
            string str = ConfigurationManager.AppSettings[key];
            if (!DateTime.TryParse(str, out dt))
                dt = defaultValue;

            return dt;
        }


        private string GetAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }


        private int GetAppSettingInt(string key, int defaultValue)
        {
            int value;
            if (int.TryParse(ConfigurationManager.AppSettings[key], out value))
                return value;

            return defaultValue;
        }


        private bool GetAppSettingBool(string key, bool defaultValue)
        {
            bool value;
            if (bool.TryParse(ConfigurationManager.AppSettings[key], out value))
                return value;

            return defaultValue;
        }
    }
}
