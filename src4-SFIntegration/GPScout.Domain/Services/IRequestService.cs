﻿using AtlasDiligence.Common.Data.General;
using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Services
{
    public interface IRequestService
    {
        IResults AddRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail);
        IResults AddRequest(RequestType requestType, string organizationName, Guid userId, string comment, string fromEmail);
        IResults RemoveRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail);
    }
}
