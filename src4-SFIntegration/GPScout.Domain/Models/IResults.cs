﻿using ErrorHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public interface IResults<TEnum, TResult, TContext> where TEnum: struct
    {
        bool HasError { get; }
        IResult<TEnum, TContext> this[int index] { get; }
        void Add(TEnum resultCode);
        void Add(TEnum resultCode, TContext context);
        List<TResult>.Enumerator GetEnumerator();
        void Throw(TEnum resultCode);
    }
}
