﻿using AtlasDiligence.Common.Data.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.Tasks
{
    public class DownloadableItemGenerationTask : TaskBase
    {
        public new DownloadableItemData Data { get; set; }

        public new DownloadableItemLifeCycle Status
        {
            get
            {
                return (DownloadableItemLifeCycle)base.Status;
            }
            set
            {
                base.Status = (int)value;
            }
        }
        
        public DownloadableItemGenerationTask()
        {
            Data = new DownloadableItemData();
        }

        public DownloadableItemGenerationTask(string data)
        {
            DeserializeData(data);
        }

        public override bool IsCompleted()
        {
            return this.Status == DownloadableItemLifeCycle.GenerationCompleted;
        }

        public override void DeserializeData(string data)
        {
            DownloadableItemData obj = JsonConvert.DeserializeObject<DownloadableItemData>(data);
            Data = obj ?? new DownloadableItemData();
        }

        public override string SerializeData()
        {
            return JsonConvert.SerializeObject(this.Data);
        }
    }
}
