﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPScout.Domain.Contracts.Models.Tasks
{
    [Serializable]
    public class DownloadableItemData
    {
        public string GeneratedFileName { get; set; }
        public string DownloadedFileName { get; set; }
        public string ContentType { get; set; }
    }
}
