﻿using System;

namespace GPScout.Domain.Contracts.Models
{
    public class FirmNote
    {
        public string Description { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
    }
}
