﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace GPScout.Domain.Contracts.Models
{
    public class ProximitySearch
    {
        static int R = 6378137; // Earth’s mean radius in meter

        double LatRadCos;
        double _lat;
        public double Lat
        {
            get { return _lat; }
            set
            {
                _lat = value;
                LatRadCos = Math.Cos(Utils.Radians(_lat));
            }
        }


        public double Lng { get; set; }

        double _radius, _radiusMeters;
        public double Radius {
            get { return _radius; }
            set
            {
                _radius = value;
                _radiusMeters = value * 1609.34;
            }
        }


        public bool IsInRadius(string positionsString)
        {
            if (positionsString == null) return false;

            var commaSeparator = new[] { ',' };
            foreach (string latLngStr in positionsString.Split(new[] { '|' }))
            {
                try {
                    var coords = latLngStr.Split(commaSeparator);
                    double lat = Convert.ToDouble(coords[0]);
                    double lng = Convert.ToDouble(coords[1]);
                    if (GetDistanceTo(lat, lng) <= _radiusMeters) return true;
                } catch
                {
                    return false;
                }
            }
            return false;
        }


        //public IEnumerable<LuceneSearchResult> GetInRadius(IEnumerable<LuceneSearchResult> items)
        //{
        //    foreach (LuceneSearchResult lsr in items)
        //        if (lsr != null && lsr.Positions != null)
        //        {
        //            foreach (LatitudeAndLongitude latLng in lsr.Positions)
        //            {
        //                if (GetDistanceTo(latLng) <= Radius)
        //                    yield return lsr;
        //            }
        //        }
        //}


        private double? GetDistanceTo(double? latitude, double? longitude)
        {
            if (!latitude.HasValue || !longitude.HasValue)
                return null;

            var dLat = Utils.Radians(latitude.Value - Lat);
            var dLong = Utils.Radians(longitude.Value - Lng);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
              Math.Cos(Utils.Radians(latitude.Value)) * LatRadCos *
              Math.Sin(dLong / 2) * Math.Sin(dLong / 2);

            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c;
            return d; // returns the distance in meter
        }
    }
}
