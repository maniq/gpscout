﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Models
{
    public interface ITimeStamped
    {
        double Tick { get; set; }
        double SetTimeStamp();
    }
}
