﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Domain.Contracts.Enums
{
    public enum ResultCode : int
    {
        // Errors
        Unknown = 0,
        ErrorMsg = 1,
        Dataimport_IsRunningAlready = 2,
        DataImport_StagingError = 3,

        EmailDistributionList_DuplicateEmail = 20,
        EmailDistributionList_NotFound = 21,

        ItemNotFound = 301,

        Validation_InvalidEmail = 401,

        // Warnings
        WARNINGS = 500,
        DataImport_Starting_Forcibly = 501,

        // Info
        INFO = 1000,
        OK = 1001,
        InfoMsg = 1002,
        DataImport_Started = 1101,
        DataImport_Starting_Import_Into_GPScout = 1102,
        DataImport_Finished = 1103,
        DataImport_From_Salesforce_Started = 1104,
        DataImport_Salesforce_to_GPScout_Staging_Finished = 1105,
        DataImport_EntityNotTouchedInStaging = 1106,
        DataImport_DataWrittenToStaging = 1107,
        DataImport_DataReadFromSource = 1108,
        DataImport_DataWrittenToStagingInsertsOnly = 1109,
        DataImport_ReIndex_Started = 1110,
        DataImport_ReIndex_Finished = 1111,
        DataImport_NewImportFromDateSet = 1112,
        DataImport_Inserting_SearchOptions_Started,
        DataImport_Inserting_SearchOptions_Finished,
        DataImport_No_Data_Imported_From_Salesforce,
        DataImport_FullImport_From_Salesforce_Started,
        DataImport_Getting_Geocode_Info_Started,
        DataImport_Getting_Geocode_Info_Finished,
        PUCN_SendingMailsStarted,
        PUCN_SendingMailsCompleted,
        PUCN_MailSent,
    }
}
