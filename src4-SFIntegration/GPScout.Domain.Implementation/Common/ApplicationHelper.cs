﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using GPScout.Domain.Contracts.Services;
using GPScout.Domain.Implementation.Services;
using SFP.Infrastructure.Email.Contracts;
using SFP.Infrastructure.Email.Implementation;
using SFP.Infrastructure.Logging.Contracts;
using SFP.Infrastructure.Logging.Implementation;
using TableauServer.Contracts;

namespace GPScout.Domain.Implementation.Common
{
    public static class ApplicationHelper
    {
        public static void RegisterTypes()
        {
            ComponentBrokerInstance.RegisterType<ITaskRepository, TaskRepository>();
            ComponentBrokerInstance.RegisterType<ITaskService, TaskService>();
            ComponentBrokerInstance.RegisterType<IUserNotificationRepository, UserNotificationRepository>();
            ComponentBrokerInstance.RegisterType<IUserNotificationService, UserNotificationService>();
            ComponentBrokerInstance.RegisterType<IEmailDistributionListService, EmailDistributionListService>();
            ComponentBrokerInstance.RegisterType<IRepository<EmailDistributionList>, EFRepository<EmailDistributionList>>();
            ComponentBrokerInstance.RegisterType<IRepository<EmailDistributionListEmail>, EFRepository<EmailDistributionListEmail>>();

            //ComponentBrokerInstance.RegisterType<IGroupService, GroupService>();
            ComponentBrokerInstance.RegisterType<IIndexService, IndexService>();
            ComponentBrokerInstance.RegisterType<ISearchService, SearchService>();
            ComponentBrokerInstance.RegisterType<IOrganizationService, OrganizationService>();
            // TO DO: for now there are two IUserService interfaces. GPScout.Domain.Contracts.Services.IUserService is the intended one for future development.
            // AtlasDiligenceWeb.Controllers.Services.IUserInterface is an older version which on long term should be merged into GPScout.Domain.Contracts.Services.IUserService.
            ComponentBrokerInstance.RegisterType<IUserService, UserService>();
            ComponentBrokerInstance.RegisterType<IEmail, Email>();
            ComponentBrokerInstance.RegisterType<IRequestService, RequestService>();
            ComponentBrokerInstance.RegisterType<ILog, Log>();
            ComponentBrokerInstance.RegisterType<ITableauServer, TableauServer.Implementation.TableauServer>();
        }
    }
}
