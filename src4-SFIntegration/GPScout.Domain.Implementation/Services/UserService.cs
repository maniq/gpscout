﻿using AtlasDiligence.Common.Data.General;
using GPScout.Domain.Contracts.Services;
using SFP.ExcelHelper;
using SFP.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlanticBT.Common.ComponentBroker;
using System.IO;

namespace GPScout.Domain.Implementation.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository
        {
            get { return ComponentBrokerInstance.RetrieveComponent<IUserRepository>(); }
        }


        public IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate)
        {
            return _userRepository.GetUserData(type, startDate, endDate);
        }

        public IEnumerable<IDictionary<string, string>> GetExcelData(TrackUsersType trackType, DateTime startDate, DateTime endDate, IEnumerable<string> headers)
        {
            bool organizationNameNeeded = headers.Contains("Organization Name");
            var rows = new List<IDictionary<string, string>>();
            (this as IUserService).GetUserData(trackType, startDate, endDate).OrderBy(o => o.Date).ToList()
                .ForEach(
                    fe =>
                    {
                        // convert timezone to eastern per requirement
                        TimeZoneInfo estZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                        var _date = TimeZoneInfo.ConvertTimeFromUtc(fe.Date, estZone);

                        var dictionary = new Dictionary<string, string>
                                {
                                    {"Date", _date.ToShortDateString()},
                                    {"Time", _date.ToShortTimeString()},
                                    {"First Name", fe.UserExt.FirstName},
                                    {"Last Name", fe.UserExt.LastName},
                                    {"Email", fe.UserExt.aspnet_User.UserName},
                                    {"Group", fe.UserExt.Group != null ? fe.UserExt.Group.Name : String.Empty}
                                };

                        if (organizationNameNeeded)
                        {
                            var trackUsersProfileView = fe.TrackUsersProfileViews.SingleOrDefault(s => s.TrackUsersId == fe.Id);
                            if (trackUsersProfileView != null)
                            {
                                dictionary.Add("Organization Name", trackUsersProfileView.Organization.Name);
                            }
                            else
                            {
                                dictionary.Add("Organization Name", String.Empty);
                            }
                        }

                        rows.Add(dictionary);
                    });

            return rows;
        }



        public string CreateUserProfileViewTrackingExcel(TrackUsersType type, DateTime startDate, DateTime endDate, string fileName)
        {
            var headers = new List<string> { "Date", "Time", "First Name", "Last Name", "Email", "Group", "Organization Name" };

            var excelInfo = new ExcelInfo
            {
                Headers = headers,
                FileName = fileName,
                Rows = GetExcelData(type, startDate, endDate, headers)
            };

            return new ExcelHelper().CreateExcel(excelInfo);
        }


        public aspnet_User GetUserById(Guid id)
        {
            return _userRepository.GetById(id);
        }
    }
}
