﻿using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPScout.Domain.Contracts.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using SFP.Infrastructure.Logging.Contracts;
using AtlanticBT.Common.Types;

namespace GPScout.Domain.Implementation.Services
{
    internal class RequestService : IRequestService
    {
        #region Properties
        private IRequestRepository RequestRepository {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRequestRepository>();
            }
        }


        private IEmailDistributionListService EmailDistributionListService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEmailDistributionListService>();
            }
        }

        private IOrganizationService OrganizationService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IOrganizationService>();
            }
        }

        private IUserService UserService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        private ILog Log
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ILog>();
            }
        }
        #endregion

        #region IRequestService
        public IResults AddRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail)
        {
            RequestRepository.InsertRequest(requestType, organizationId, userId, comment);
            var organization = OrganizationService.GetById(organizationId);
            return SendRequestNotification(requestType, organization.Name, userId, comment, fromEmail);
        }


        public IResults AddRequest(RequestType requestType, string organizationName, Guid userId, string comment, string fromEmail)
        {
            RequestRepository.InsertRequest(requestType, organizationName, userId, comment);
            return SendRequestNotification(requestType, organizationName, userId, comment, fromEmail);
        }


        public IResults RemoveRequest(RequestType requestType, Guid organizationId, Guid userId, string comment, string fromEmail)
        {
            var requestsToRemove = RequestRepository.FindAll(r => r.Type == (int)requestType && r.UserId == userId && r.OrganizationId == organizationId);
            if (string.IsNullOrEmpty(comment))
            {
                int cnt = requestsToRemove.Count();
                comment = string.Format("SYSTEM: {0} request record(s) deleted from the database.", cnt);
            }
            RequestRepository.DeleteAllOnSubmit(requestsToRemove);
            var organization = OrganizationService.GetById(organizationId);
            return SendRequestNotification("GPScout Request Cancelled By User", "Request cancelled by user:", requestType, organization.Name, userId, comment, fromEmail);
        }
        #endregion

        #region Helper Methods
        private IResults SendRequestNotification(RequestType requestType, string organizationName, Guid userId, string comment, string fromEmail)
        {
            return SendRequestNotification("New GPScout Request Submitted", "New GPScout request has been submitted:", requestType, organizationName, userId, comment, fromEmail);
        }


        private IResults SendRequestNotification(string title, string firstLine, RequestType requestType, string organizationName, Guid userId, string comment, string fromEmail)
        {
            var results = new Results();

            var user = UserService.GetUserById(userId);
            
            string groupName = string.Empty;
            if (user.UserExt != null && user.UserExt.Group != null)
                groupName = user.UserExt.Group.Name;


            string body = string.Format(@"
                {6}<br/><br/>
                Request Type: <strong>{0} Request</strong><br/>
                Username: <strong>{1}</strong><br/>
                Group:  <strong>{2}</strong><br/>
                Organization Name: <strong>{3}</strong><br/>
                Date of request: <strong>{4}</strong><br/>
                Comments:<br/><strong>{5}</strong>
                ", requestType.GetDescription(), user.UserName, groupName, organizationName, DateTime.Now.ToShortDateString(), comment, firstLine);

            var sendingResults = EmailDistributionListService.SendToEmailDistributionList(EmailDistribution.RequestNotification, title, body, fromEmail);

            sendingResults.LogTo(Log);
            return results;
        }
        #endregion
    }
}
