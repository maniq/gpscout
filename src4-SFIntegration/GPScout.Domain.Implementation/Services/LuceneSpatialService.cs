﻿//using Lucene.Net.Documents;
//using Lucene.Net.Util;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace GPScout.Domain.Implementation.Services
//{
//    public class LuceneSpatialService
//    {
//        private static readonly object SyncRoot = new object();
//        private static bool Initialised { get; set; }

//        private static int _startTier;
//        private static int _endTier;
//        private static Dictionary<int, CartesianTierPlotter> Plotters { get; set; }

//        public static void AddCartesianTiers(Coordinate cord, Document document)
//        {
//            for (var tier = _startTier; tier <= _endTier; tier++)
//            {
//                var ctp = Plotters[tier];
//                var boxId = ctp.GetTierBoxId(cord.Latitude, cord.Longitude);
//                document.Add(
//                  new Field(
//                    ctp.GetTierFieldName(),
//                    NumericUtils.DoubleToPrefixCoded(boxId),
//                    Field.Store.YES,
//                    Field.Index.NOT_ANALYZED_NO_NORMS));
//            }
//        }


//        public static void Initialise()
//        {
//            Initialised = false;
//            lock (SyncRoot)
//            {
//                //if (IndexDirectory != null)
//                //{
//                //    IndexDirectory.Dispose();
//                //}

//                //Thread.MemoryBarrier();
//                //IndexDirectory = new RAMDirectory();
//                //TimeTakenToInitialiseMs = 0;

//                //var sw = new Stopwatch();
//                //sw.Start();

//                IProjector projector = new SinusoidalProjector();
//                var ctp = new CartesianTierPlotter(0, projector,
//                                                   Fields.LocationTierPrefix);
//                _startTier = ctp.BestFit(MaxKms);
//                _endTier = ctp.BestFit(MinKms);

//                Plotters = new Dictionary<int, CartesianTierPlotter>();
//                for (var tier = _startTier; tier <= _endTier; tier++)
//                {
//                    Plotters.Add(tier, new CartesianTierPlotter(tier,
//                                    projector, Fields.LocationTierPrefix));
//                }

//                //IndexMany(LocationRepository.All());
//                //sw.Stop();
//                //TimeTakenToInitialiseMs = sw.ElapsedMilliseconds;
//                Initialised = true;
//            }
//        }
//    }
//}
