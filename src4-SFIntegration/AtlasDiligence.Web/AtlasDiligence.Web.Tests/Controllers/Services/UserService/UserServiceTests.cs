﻿using System;
using System.Web.Security;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Common.DTO.Model;
using AtlasDiligence.Common.DTO.Services;
using AtlasDiligence.Web.Controllers.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;
using Rhino.Mocks;


namespace AtlasDiligence.Web.Tests
{
    /// <summary>
    /// Summary description for UserServiceTests
    /// </summary>
    [TestClass]
    public class UserServiceTests : TestBase
    {
        public UserServiceTests()
        {
        }

        #region ValidateUser 
        /// <summary>
        /// Test a valid user that is locked out
        /// </summary>
        [TestMethod]
        public void ValidateUserValidUserIsLockedOutTest()
        {
            Fixture fixture = new Fixture();
            var id = Guid.NewGuid();
            var crmUser = fixture.CreateAnonymous<WebUser>();
            var dbUser = base.CreateTransientAspNetUser(id);

            var membershipUser = MockRepository.GenerateMock<MembershipUser>();
            membershipUser.Expect(m => m.UserName).Return("foo");
            membershipUser.Expect(m => m.IsLockedOut).Return(true);
            var mockedProvider = MockRepository.GenerateMock<MembershipProvider>();
            mockedProvider.Expect(m => m.ValidateUser("foo", "bar")).Return(true);
            mockedProvider.Expect(m => m.GetUser("foo", false)).Return(membershipUser);
            var mockedRepository = MockRepository.GenerateMock<IUserRepository>();
            mockedRepository.Expect(m => m.GetUserByCrmId(crmUser.Id)).Return(dbUser);
            mockedRepository.Expect(m => m.GetByEmail(crmUser.Email)).Return(dbUser);
            mockedRepository.Expect(m => m.HasDuplicate(dbUser.UserId, dbUser.UserName)).Return(false);
            var mockedWebUserService = MockRepository.GenerateMock<IWebUserService>();
            mockedWebUserService.Expect(m => m.GetByEmail("foo")).Return(crmUser);
            mockedWebUserService.Expect(m => m.Update(crmUser));

            var userService = new UserService();
            ComponentBrokerInstance.RegisterComponent<IUserRepository>(mockedRepository);
            ComponentBrokerInstance.RegisterComponent<MembershipProvider>(mockedProvider);
            ComponentBrokerInstance.RegisterComponent<IWebUserService>(mockedWebUserService);          

            Assert.IsFalse(userService.ValidateUser("foo", "bar"));
        }
        #endregion

        [TestMethod]
        [Ignore]
        public void GetUserEntityTest()
        {
            //Fixture fixture = new Fixture();
            //var webUser = fixture.CreateAnonymous<WebUser>();
            //var mockRepo = MockRepository.GenerateMock<IUserRepository>();
            //mockRepo.Expect(m => m.GetById(Guid.Empty)).Return(CreateTransientAspNetUser(Guid.Empty));
            //ComponentBrokerInstance.RegisterComponent<IUserRepository>(mockRepo);
            //ComponentBrokerInstance.RegisterComponent<MembershipProvider>(CreateMockMembershipProvider());
            //ComponentBrokerInstance.RegisterComponent<IWebUserService>(webUser);

            //var userService = new UserService();
            //var userEntity = userService.GetUserEntityByUserId(Guid.Empty);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            ComponentBrokerInstance.UnregisterAllComponents();
        }

        private static MembershipProvider CreateMockMembershipProvider()
        {
            var mockedProvider = MockRepository.GenerateMock<MembershipProvider>();
            return mockedProvider;
        }


    }
}
