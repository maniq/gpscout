﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AtlasDiligence.Web.General
{
    public class AccessHelper
    {
        #region Private

        private static readonly string cookie_name = "session_id_helper";
        
        private static List<AccessInfo> CurrentAccesses = new List<AccessInfo>();


        private static AccessInfo GetAcessInfo(HttpRequest request)
        {
            if (request != null)
            {
                return AccessHelper.GetAcessInfo(request.Cookies);
            }
            return new AccessInfo();
        }

        private static AccessInfo GetAcessInfo(HttpRequestBase request)
        {
            if (request != null)
            {
                return AccessHelper.GetAcessInfo(request.Cookies);
            }
            return new AccessInfo();
        }


        private static AccessInfo GetAcessInfo(HttpCookieCollection cookies)
        {
            var accessInfo = new AccessInfo();
            if (cookies != null)
            {
                var cookie = cookies[cookie_name];
                if (cookie != null)
                {
                    DateTime dt;
                    if (DateTime.TryParse(cookie.Values["Timestamp"], out dt))
                        accessInfo.Timestamp = dt;

                    Guid id;
                    if (Guid.TryParse(cookie.Values["ID"], out id))
                        accessInfo.ID = id;

                    accessInfo.UserName = cookie.Values["UserName"];
                }
            }
            return accessInfo;
        }

        private static void AddCurrentUser(AccessInfo accessInfo)
        {
            lock (CurrentAccesses)
            {
                var usersAlreadyExisting = CurrentAccesses.Where(u => u.UserName == accessInfo.UserName).ToArray();
                foreach (var user in usersAlreadyExisting)
                {
                    CurrentAccesses.Remove(user);
                }
                CurrentAccesses.Add(accessInfo);
            }
        }

        internal static bool AcessAllowed(HttpContextBase httpContext)
        {
            if (Common.IsPrint) return true;
            if (httpContext != null && httpContext.User.Identity != null && httpContext.User.Identity.IsAuthenticated)
            {
                var accessInfo = AccessHelper.GetAcessInfo(httpContext.Request);
                if (accessInfo.ID.HasValue && accessInfo.UserName == httpContext.User.Identity.Name)
                {
                    lock (CurrentAccesses)
                    {
                        var usersAlreadyExisting = CurrentAccesses.Where(u => u.ID == accessInfo.ID && u.UserName == accessInfo.UserName);
                        return usersAlreadyExisting.Count() == 1;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Internal & Public

        public static void OnSessionStart()
        {
            var httpContext = HttpContext.Current;
            if (httpContext != null && httpContext.User.Identity != null && httpContext.User.Identity.IsAuthenticated && !Common.IsPrint)
            {
                var accessInfo = AccessHelper.GetAcessInfo(httpContext.Request);
                if (accessInfo.ID.HasValue && accessInfo.UserName == httpContext.User.Identity.Name)
                {
                    lock (CurrentAccesses)
                    {
                        var usersAlreadyExisting = CurrentAccesses.Where(u => u.UserName == accessInfo.UserName); //.ToArray();
                        //if (usersAlreadyExisting.Count() > 1) throw new Exception(string.Format("{0} found multiple time in CurentAccesses records."));
                        if (!usersAlreadyExisting.Any())
                        {
                            CurrentAccesses.Add(accessInfo);
                        }
                    }
                }
            }
        }


        internal static bool IsUserLoggedInAlready(string userName, HttpContextBase httpContext)
        {
            bool userAlreadyOnline;
            lock (CurrentAccesses)
            {
                var accessInfo = AccessHelper.GetAcessInfo(httpContext.Request);
                if (accessInfo.ID.HasValue)
                {
                    userAlreadyOnline = CurrentAccesses.Any(u => u.UserName == userName && accessInfo.ID != u.ID);
                    
                } else
                {
                    userAlreadyOnline = CurrentAccesses.Any(u => u.UserName == userName);
                }
            }
            return userAlreadyOnline;
        }

        internal static void SignIn(string username, HttpResponseBase response, bool setCookie)
        {
            if (setCookie)
            {
                var accessInfo = new AccessInfo { Timestamp = DateTime.Now, ID = Guid.NewGuid(), UserName = username };
                var cookie = new HttpCookie(cookie_name);
                cookie.Values.Add("Timestamp", accessInfo.Timestamp.ToString());
                cookie.Values.Add("ID", accessInfo.ID.ToString());
                cookie.Values.Add("UserName", accessInfo.UserName);
                response.Cookies.Add(cookie);

                AddCurrentUser(accessInfo);
            } else
            {
                response.Cookies.Remove(cookie_name);
            }
        }


        internal static void SignOut(HttpResponseBase response)
        {
            var accessInfo = AccessHelper.GetAcessInfo(HttpContext.Current.Request);
            lock (CurrentAccesses)
            {
                var accessEntry = CurrentAccesses.FirstOrDefault(x => x.ID == accessInfo.ID);
                if (accessEntry == null && accessInfo.ID.GetValueOrDefault() == Guid.Empty && HttpContext.Current.User.Identity != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    accessEntry = CurrentAccesses.FirstOrDefault(x => x.UserName == HttpContext.Current.User.Identity.Name);
                if (accessEntry != null) CurrentAccesses.Remove(accessEntry);
                response.Cookies.Remove(cookie_name);
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                    HttpContext.Current.Session.RemoveAll();

                ComponentBrokerInstance.RetrieveComponent<IFormsAuthenticationService>().SignOut();
            }
        }
        #endregion
    }
}
