﻿using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.General
{
    /// <summary>
    /// Authorize override to redirect to first time login steps based on User's Role.
    /// </summary>
    public class AtlasAuthorize : AuthorizeAttribute
    {
        public bool IsAuthorize(HttpContextBase httpContext)
        {
            return AuthorizeCore(httpContext);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
                return false;

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            //check Role to determine redirect
            //new users need to complete additional info screen
            if (!httpContext.User.IsInRole(RoleNames.CollectedInfo))
            {
                httpContext.Response.Redirect("/Account/CollectInfo");
                return false;
            }

            //Only allow Confirmed Users and Admins to pass Authentication
            return httpContext.User.IsInRole(RoleNames.CollectedInfo) || httpContext.User.IsInRole(RoleNames.Admin);
        }
    }
}