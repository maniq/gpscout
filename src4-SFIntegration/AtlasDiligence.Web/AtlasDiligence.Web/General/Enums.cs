﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Web.General
{
    public enum UpdateUserMessage
    {
        Success = 1,
        [Description("A user already exists with this email.")]
        DeniedDuplicateEmail = 2,
        [Description("An error has occurred")]
        DeniedUnknownError = 3
    }

    public enum ActivateUserMessage
    {
        Success = 1,
        DeniedMaxUsers = 2,
        DeniedUnknownError = 3
    }

    public enum RssViewType
    {
        Excel = 1,
        Preview = 2,
        Print = 3
    }

    /// <summary>
    /// drop down options in USerExt.OrganizationType
    /// </summary>
    public enum OrganizationType
    {
        Endowment = 1,
        Foundation,
        [Description("Fund of Funds")]
        FundOfFunds,
        [Description("Family Office")]
        FamilyOffice,
        [Description("Multi-Family Office")]
        MultiFamilyOffice,
        Pension,
        Sovereign,
        Consultant,
        Bank,
        Insurance,
        Other
    }
}