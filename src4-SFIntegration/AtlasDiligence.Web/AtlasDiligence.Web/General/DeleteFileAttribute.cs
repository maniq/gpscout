﻿using AtlanticBT.Common.ComponentBroker;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace AtlasDiligence.Web.General
{
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();
            var filePathResult = filterContext.Result as FilePathResult;
            if (filePathResult != null)
            {
                Utils.DeleteFile(filePathResult.FileName);
            }
            string contextId = filterContext.Controller.ViewBag.InstanceId as string;
            ComponentBrokerInstance.RetrieveComponent<IOrganizationService>().FullReportCleanup(contextId);
        }
    }
}