﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtlasDiligence.Web.General
{
    public class AccessInfo
    {
        public DateTime? Timestamp { get; set; }
        public Guid? ID { get; set; }
        public string UserName { get; set; }
    }
}
