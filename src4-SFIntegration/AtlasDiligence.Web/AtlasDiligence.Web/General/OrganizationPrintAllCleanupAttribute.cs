﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using GPScout.Domain.Contracts.Models.UserNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utility;
using ControllerBase = AtlasDiligence.Web.Controllers.ControllerBase;

namespace AtlasDiligence.Web.General
{
    public class OrganizationPrintAllCleanupAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            ValueProviderResult notificationIdValue = filterContext.Controller.ValueProvider.GetValue("id");
            Guid notificationId;
            if (notificationIdValue != null && Guid.TryParse(notificationIdValue.AttemptedValue, out notificationId))
            {
                ControllerBase baseController = filterContext.Controller as ControllerBase;
                if (baseController != null)
                {
                    var notification = baseController.UserNotificationService.GetById(notificationId) as OrganizationFullReportNotification;
                    if (notification != null)
                    {
                        baseController.UserEntity.Notifications.TryRemove(notification.Id);
                        baseController.UserNotificationService.Delete(notification);
                        filterContext.HttpContext.Response.Flush();
                        baseController.OrganizationService.FullReportCleanup(notification.ReferenceId);
                    }
                }
            }
        }
    }
}