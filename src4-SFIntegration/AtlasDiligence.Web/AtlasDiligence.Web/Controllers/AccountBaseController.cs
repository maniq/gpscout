﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace AtlasDiligence.Web.Controllers
{
    public class AccountBaseController : ControllerBase
    {
        protected static Regex passwordStrengthRegexp = new Regex(Membership.PasswordStrengthRegularExpression);

        protected IUserService _userService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }
    }
}
