﻿using System;
using System.Web.Security;
using System.Xml;
using System.Xml.Linq;
using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.General;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Account;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Controllers.Services
{
    /// <summary>
    /// ZK SFP 12/09/2015: This interface is partially obsolete.
    /// TO DO: On long term it should be merged into BLL IUserService (GPScout.Domain.Contracts.Services.IUserService)
    /// </summary>
    public interface IUserService
    {
        GridList<UserDataGrid> GetUserGridListResults(int pageStart, string sortColumn, bool sortAscending,
            int rowsPerPage, string email, string firstName, string lastName,
            List<Guid> excludedUsers, bool accessGranted, Guid? groupId);

        /// <summary>
        /// Returns mapped USerEntity objects for given group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<UserEntity> GetUsersForGroup(Guid groupId);

        /// <summary>
        /// return all groups
        /// </summary>
        /// <returns></returns>
        IEnumerable<AtlasDiligence.Common.Data.Models.Group> GetAllGroups();

        int MinPasswordLength { get; }

        /// <summary>
        /// Validate user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool ValidateUser(string userName, string password, out bool isTrialUser);

        /// <summary>
        /// Change user password in Membership model.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        bool ChangePassword(string userName, string oldPassword, string newPassword);

        /// <summary>
        /// Update local user 
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        UpdateUserMessage UpdateUser(UserEntity userEntity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        UserEntity GetUserEntityByEmail(string email);

        /// <summary>
        /// maps data from web db. Overload with impersonateUser set to false
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserEntity GetUserEntityByUserId(Guid userId);

        /// <summary>
        /// Reset users password.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool ResetPassword(UserEntity user, string password);

        /// <summary>
        /// Unlocks user in Membership model.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool UnlockUser(UserEntity user);

        /// <summary>
        /// Change active status in local (Membership Provider). Method will set inverse of IsActive in userEntity parameter. 
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        ActivateUserMessage ChangeActiveStatus(UserEntity userEntity);

        /// <summary>
        /// Grant access request to local user.
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>
        bool GrantAccessRequest(UserEntity userEntity);

        /// <summary>
        /// Generate a random password based off the length and complexity enforced by 
        /// the app settings.
        /// </summary>
        /// <returns></returns>
        string GeneratePassword();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        MembershipCreateStatus CreateRequestAccess(RequestAccessViewModel model);
        void TrackForcedLogon(Guid userId);

        /// <summary>
        /// Creates a new User
        /// </summary>
        /// <param name="model"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        MembershipCreateStatus CreateUser(UserViewModel model, bool isAdmin);

        bool IsGroupMaxUsers(Guid guid);

        bool SaveCollectedInfo(CollectInfoViewModel model);

        /// <summary>
        /// Delete User.
        /// </summary>
        /// <param name="userEntity"></param>
        void DeleteUser(UserEntity userEntity);

        /// <summary>
        /// Flip RssSearchAliases flag
        /// </summary>
        /// <param name="userEntity"></param>
        bool ChangeRssSearchAlias(UserEntity userEntity);

        IEnumerable<string> GetUserEmails(string likeEmail); 

        void UpdateRssEmails(Guid guid, List<string> result);

        void RemoveRssEmail(Guid guid, string email);

        bool IsLockedOut(string email);

        IEnumerable<RecentlyViewedOrganizationsResult> GetRecentlyViewedOrganizations(Guid userId, int take);

	    void TrackUserLogin(Guid userId);

	    void TrackUserLogout(Guid userId);

	    void TrackOrganizationProfileView(Guid userId, Guid orgId);

		IEnumerable<TrackUser> GetUserData(TrackUsersType type, DateTime startDate, DateTime endDate);

	    aspnet_User GetUserById(Guid userId);
        void TrackOrganizationProfileFullReportPdf(Guid userId, Guid orgId);
    }
}