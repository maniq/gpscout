﻿using System.Net.Mail;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Common.Data.Models;
using System;

namespace AtlasDiligence.Web.Controllers.Services
{
    public class ContactService : IContactService
    {
        #region Implementation of IContactService

        public void SendContactEmail(ContactViewModel viewModel)
        {
            var from = new MailAddress(AppSettings.MailFrom);
            var to = new MailAddress(AppSettings.ContactToMail);
            var mail = new MailMessage();
            mail.From = from;
            mail.To.Add(to);
            mail.Subject = AppSettings.ContactMailSubject;
            mail.IsBodyHtml = true;
            mail.Body = Resources.Templates.ContactEmail
                .Replace("__FIRSTNAME__", viewModel.FirstName)
                .Replace("__LASTNAME__", viewModel.LastName)
                .Replace("__ORGANIZATION__", viewModel.Organization)
                .Replace("__EMAIL__", viewModel.Email)
                .Replace("__QUESTIONS__", viewModel.Questions)
                .Replace("__CURRENTYEAR__", DateTime.Now.Year.ToString());
            var client = new SmtpClient();
            client.Send(mail);
        }

        #endregion
    }
}