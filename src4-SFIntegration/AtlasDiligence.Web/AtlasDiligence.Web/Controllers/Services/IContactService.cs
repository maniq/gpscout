﻿using AtlasDiligence.Web.Models.ViewModels;

namespace AtlasDiligence.Web.Controllers.Services
{
    public interface IContactService
    {
        void SendContactEmail(ContactViewModel viewModel);
    }
}