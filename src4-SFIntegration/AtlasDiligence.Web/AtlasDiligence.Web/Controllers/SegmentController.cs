﻿using System;
using System.Linq;
using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.Mappers;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Segment;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using AtlasDiligence.Web.Resources;

namespace AtlasDiligence.Web.Controllers
{
    [AtlasAuthorize(Roles = RoleNames.Admin)]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class SegmentController : ControllerBase
    {
        private ISegmentRepository _segmentRepository;

        public SegmentController()
        {
            _segmentRepository = ComponentBrokerInstance.RetrieveComponent<ISegmentRepository>();
        }

        public ActionResult Index()
        {
            var segmentViewModel = new SegmentViewModel();
            segmentViewModel.Build(_segmentRepository);
            return View(segmentViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var segmentViewModel = new SegmentViewModel();

            segmentViewModel.Build(_segmentRepository);

            return View(segmentViewModel);
        }

        [HttpPost]
        public ActionResult Create(SegmentOptionsViewModel model)
        {
            var segmentViewModel = new SegmentViewModel { SegmentOptions = model };
            if (ModelState.IsValid)
            {
                var segment = new Segment() { Id = Guid.NewGuid() };
                SegmentMapper.MapToSegment(model, segment);
                _segmentRepository.Insert(segment);
                TempData["Message"] = string.Format("Segment \"{0}\" has been created.", model.Name);
            }
            return this.Redirect("/Admin?lt=2");
            //return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id, string returnRoute)
        {
            var segment = _segmentRepository.GetById(id);
            if (segment == null)
            {
                TempData["Message"] = string.Format(ErrorMessages.InvalidId, "Segment");
                return View();
            }

            var segmentViewModel = new SegmentViewModel();
            segmentViewModel.Build(_segmentRepository, id);
            segmentViewModel.SegmentOptions.ReturnRoute = returnRoute;

            return View(segmentViewModel);
        }

        [HttpPost]
        public ActionResult Update(SegmentOptionsViewModel model)
        {
            var segmentViewModel = new SegmentViewModel { SegmentOptions = model };
            if (ModelState.IsValid)
            {
                var segment = _segmentRepository.GetById(model.Id);
                SegmentMapper.MapToSegment(model, segment);
                _segmentRepository.SubmitChanges();
                TempData["Message"] = string.Format("Segment \"{0}\" has been updated.", model.Name);
                if(!string.IsNullOrWhiteSpace(model.ReturnRoute))
                {
                    return this.Redirect(model.ReturnRoute);
                }
            }

            //segmentViewModel.Build(_segmentRepository);
            return RedirectToAction("Edit", new {id = model.Id});
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AtlasAuthorize(Roles = RoleNames.Admin)]
        [HttpGet]
        public JsonResult Delete(Guid id)
        {
            var model = new MessageViewModel();
            var segment = _segmentRepository.GetById(id);
            if (segment == null)
            {
                model.Message = string.Format(Resources.ErrorMessages.InvalidId, "Segment");
                model.Status = MessageStatus.Failure;
            }
            else
            {
                _segmentRepository.DeleteOnSubmit(segment);
                _segmentRepository.SubmitChanges();

                model.Message = Resources.ErrorMessages.DeletedRow;
                model.Status = MessageStatus.Success;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="rowsPerPage"></param>
        /// <returns></returns>
        public JsonResult SegmentGrid(int pageStart, string sortColumn, bool sortAscending, int rowsPerPage)
        {
            if (rowsPerPage > 100) rowsPerPage = 100;

            if (string.IsNullOrEmpty(sortColumn))
            {
                sortColumn = "Name";
                sortAscending = true;
            }

            //need to set key to full dot-notation data model path.
            switch (sortColumn)
            {
                case "SubCategory":
                    sortColumn = "MarketStage";
                    break;
                default:
                    break;
            }

            var allSegments = _segmentRepository.GetAll();
            var segments = _segmentRepository.Paginate(allSegments, sortColumn, sortAscending, pageStart, rowsPerPage);

            var results = new GridList<SegmentDataGrid>();
            results.Total = allSegments.Count();
            results.Result = segments.Select(x => new SegmentDataGrid(x)).ToArray();

            return Json(results);
        }

        public JsonResult SegmentsAutoComplete(string term)
        {
            var segments = _segmentRepository.GetSegmentsStartsWith(term);
            return Json(segments.Select(x => x.Name).ToArray(), JsonRequestBehavior.AllowGet);
        }
    }
}
