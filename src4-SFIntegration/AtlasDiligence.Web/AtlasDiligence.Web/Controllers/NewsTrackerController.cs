﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Validation;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Rss;
using WebCommon = AtlasDiligence.Web.General.Common;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Controllers
{
	[AtlasAuthorize]
	[EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class NewsTrackerController : ControllerBase
	{
		public ActionResult Index()
		{
			var viewModel = new RssIndexViewModel
								{
									RssSearchViewModel = new RssSearchViewModel()
															 {
																 SearchTerms = SearchTermRepository.GetAllByUserId(UserEntity.Id).Select(m => m.SearchTerm).OrderBy(m => m).ToList()
															 }
								};
			viewModel.RssScheduleEmailViewModel.Build(UserEntity);
			viewModel.RssExportViewModel.SearchAliases = UserEntity.RssSearchAliases;

			return View(viewModel);
		}

		[HttpPost]
		public ActionResult Index(RssExportViewModel exportViewModel)
		{
			var searchTerms = SearchTermRepository.GetSearchTermAndAliasesForUser(UserEntity.Id);

			if (exportViewModel.StartDate.HasValue && exportViewModel.EndDate.HasValue && (exportViewModel.StartDate.Value > exportViewModel.EndDate.Value))
			{
				ModelState.AddModelError("StartDate", Resources.ErrorMessages.RssExportDate);
			}
			if (ModelState.IsValid)
			{
				var minStartDate =
					ConfigurationManager.AppSettings["NewsTrackerMinStartDate"].Maybe<DateTime>().GetValueOrDefault(
						new DateTime(2011, 09, 01));
				var startDate = exportViewModel.StartDate.Value < minStartDate
									? minStartDate
									: exportViewModel.StartDate.Value;

				var newsItems =
					NewsRepository.FindAll(
						m =>
						m.PublishedDate.Date >= startDate &&
						m.PublishedDate < exportViewModel.EndDate.Value.Date.AddDays(1)).ToList();

				var filteredNewsItems = new Dictionary<string, List<News>>();

				foreach (var searchTerm in searchTerms.OrderBy(o => o.Key))
				{
					KeyValuePair<string, IEnumerable<string>> term1 = searchTerm;
					var itemsForTerm = newsItems.Where(item => term1.Value.Any(s => item.Description.ToLower().Contains(s.ToLower()) || item.Subject.ToLower().Contains(s.ToLower()))).
						OrderByDescending(o => o.PublishedDate).ToList();
					filteredNewsItems.Add(term1.Key, itemsForTerm);
				}

				switch (exportViewModel.RssViewType)
				{
					case RssViewType.Excel:
						var excelResult = new ExcelResult
										 {
											 Headers = new List<string> { "Search Term", "Title", "Published Date", "Source", "URL", "Summary", "Content" },
											 DeleteFile = true,
											 FileName = "NewsExport.xlsx",
											 FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
										 };
						var rows = new List<IDictionary<string, string>>();
						foreach (var filteredNewsItem in filteredNewsItems)
						{
							var item = filteredNewsItem;
							rows.AddRange(item.Value.Select(t => new Dictionary<string, string>
                                                                     {
                                                                         {"Search Term", item.Key}, 
                                                                         {"Title", t.Subject},
                                                                         {"Published Date", t.PublishedDate.ToShortDateString()},
                                                                         {"Source", t.OrganizationSourceName}, 
                                                                         {"URL", t.Url}, 
                                                                         {"Summary", t.Summary}, 
                                                                         {"Content", t.Description},
                                                                     }));
						}
						excelResult.Rows = rows;
						return excelResult;
					case RssViewType.Preview:
						exportViewModel.NewsItems = filteredNewsItems;
						if (!exportViewModel.NewsItems.Any())
							ViewData["Message"] = "There are no News Items for the search terms and dates selected.";
						break;
					default:
						break;
				}
			}

			//if !valid rebuild ViewModel
			var viewModel = new RssIndexViewModel();
			viewModel.RssSearchViewModel = new RssSearchViewModel()
			{
				SearchTerms = searchTerms.Select(x => x.Key).OrderBy(o => o).ToList()
			};
			viewModel.RssScheduleEmailViewModel.Build(UserEntity);
			viewModel.RssExportViewModel = exportViewModel;
			return View(viewModel);
		}
		[HttpGet]
		public JsonResult ChangeRssSearchAlias()
		{
			var retval = new MessageViewModel
							 {
								 Status = MessageStatus.Failure,
								 Message = Resources.ErrorMessages.DefaultError
							 };

			try
			{
				if (UserService.ChangeRssSearchAlias(UserEntity))
				{
					retval.Status = MessageStatus.Success;
					//repopulate UserEntity
					UserEntity.RssSearchAliases = !UserEntity.RssSearchAliases;
				}
			}
			catch (Exception ex)
			{
				retval.Message = Resources.ErrorMessages.DefaultError;
			}

			return Json(retval, JsonRequestBehavior.AllowGet);
		}

		#region terms
		/// <summary>
		/// Adds terms
		/// </summary>
		/// <param name="terms"></param>
		/// <returns></returns>
		public JsonResult AddSearchTerms(List<string> terms)
		{
			terms.RemoveAll(s => s.Trim() == String.Empty);
			var result = terms.Distinct().OrderBy(m => m).ToList();
			try
			{
				SearchTermRepository.DeleteAllForUser(UserEntity.Id);
				foreach (var term in result)
				{
					SearchTermRepository.Add(new SearchTerms { SearchTerm = term, UserId = UserEntity.Id });
				}
			}
			catch (Exception ex)
			{
				return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "An error occurred adding the search term." });
			}
			return Json(new MessageViewModel { Status = MessageStatus.Success, Data = result });
		}

		/// <summary>
		/// Remove a search term
		/// </summary>
		/// <param name="term"></param>
		/// <returns></returns>
		public JsonResult RemoveSearchTerm(string term)
		{
			try
			{
				SearchTermRepository.Delete(term, UserEntity.Id);
			}
			catch (Exception ex)
			{
				return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "An error occurred removing the search term." });
			}
			return Json(new MessageViewModel { Status = MessageStatus.Success });
		}
		#endregion

		#region schedule

		[HttpPost]
		public ActionResult Schedule(RssScheduleEmailViewModel viewModel)
		{
			bool valid = false;

			if (viewModel.IsSubscribed)
			{
				ValidateSchedule(viewModel);

				if (ModelState.IsValid)
				{
					UserEntity.RssNextEmailDate = GetDateTimeEntered(viewModel.NextEmailDate, viewModel.TimeOfDay);
					UserEntity.RssEmailFrequency = viewModel.Frequency;
					valid = true;
				}
			}
			else
			{
				UserEntity.RssNextEmailDate = null;
				UserEntity.RssEmailFrequency = null;
				valid = true;
			}

			//update if valid
			if (valid)
			{
				UserService.UpdateUser(UserEntity);
				//repopulate UserEntity user being edited is logged in user
				SetUserEntity(UserService.GetUserEntityByUserId(UserEntity.Id));
				ViewData["Message"] = "The schedule has been updated.";
			}

			SetUserEntity(UserEntity);

			viewModel.Build(UserEntity);

			return View(viewModel);
		}

		public JsonResult AddRssEmails(List<string> emails)
		{
			emails.RemoveAll(s => s.Trim() == String.Empty ||
				!Regex.IsMatch(s, RegExPattern.GetRegExpPatternEmail(), RegexOptions.IgnoreCase));
			var result = emails.Distinct().OrderBy(m => m).ToList();
			try
			{
				UserService.UpdateRssEmails(UserEntity.Id, result);
				UserEntity.RssEmails = result;
			}
			catch (Exception)
			{
				return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "An error occurred adding the email addresses." });
			}
			return Json(new MessageViewModel { Status = MessageStatus.Success, Data = result });
		}

		public JsonResult RemoveRssEmail(string email)
		{
			try
			{
				UserService.RemoveRssEmail(UserEntity.Id, email);
				var emails = UserEntity.RssEmails.ToList();
				emails.Remove(email);
				UserEntity.RssEmails = emails;
			}
			catch (Exception)
			{
				return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "An error occurred removing the email address." });
			}
			return Json(new MessageViewModel { Status = MessageStatus.Success });
		}

		/// <summary>
		/// Custom Validation for Schedule fields
		/// </summary>
		/// <param name="viewModel"></param>
		private void ValidateSchedule(RssScheduleEmailViewModel viewModel)
		{
			if (!viewModel.NextEmailDate.HasValue)
			{
				ModelState.AddModelError(WebCommon.GetName(() => viewModel.NextEmailDate), "Please enter a valid date.");
			}
			if (!viewModel.Frequency.HasValue)
			{
				ModelState.AddModelError(WebCommon.GetName(() => viewModel.Frequency), "Please enter a valid frequency.");
			}
			if (!viewModel.TimeOfDay.Maybe<DateTime>().HasValue)
			{
				ModelState.AddModelError(WebCommon.GetName(() => viewModel.TimeOfDay), "Please enter a valid time of day.");
			}

			DateTime? dateTimeEntered = GetDateTimeEntered(viewModel.NextEmailDate, viewModel.TimeOfDay);
			if (dateTimeEntered.HasValue && dateTimeEntered.Value < DateTime.Now)
			{
				ModelState.AddModelError(WebCommon.GetName(() => viewModel.NextEmailDate), Resources.ErrorMessages.RssEmailDate);
			}
		}

		/// <summary>
		/// Attempts to parse timeToAdd and add it to the startDate 
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="timeToAdd"></param>
		/// <returns></returns>
		private DateTime? GetDateTimeEntered(DateTime? startDate, string timeToAdd)
		{
			try
			{
				return startDate.HasValue
						   ? startDate.Value.AddTicks(DateTime.Parse(timeToAdd).TimeOfDay.Ticks)
						   : (DateTime?)null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		#endregion
	}
}
