﻿using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Search;
using GPScout.Domain.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace AtlasDiligence.Web.Controllers
{
    using AtlanticBT.Common.ComponentBroker;
    using AtlasDiligence.Common.Data.General;
    using AtlasDiligence.Common.Data.Repositories;
    using AtlasDiligence.Web.Controllers.Services;
    using AtlasDiligence.Web.Helpers;
    using GPScout.Domain.Contracts.Enums;
    using GPScout.Domain.Contracts.Models;
    using GPScout.Domain.Contracts.Models.Tasks;
    using GPScout.Domain.Contracts.Models.UserNotifications;
    using GPScout.Domain.Contracts.Services;
    using log4net;
    using System.IO;
    using Utility;

    [AtlasAuthorize(Roles = RoleNames.CollectedInfo)]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class OrganizationController : ControllerBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(OrganizationController));
        private OrganizationViewModel _viewModel;

        private IGroupRepository _groupRepository { get { return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>(); } }

        public ActionResult Index(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            if (!UserEntity.IsAdmin && !UserEntity.IsEmployee && _viewModel.Organization.PublishLevelID <= (byte)OrgPublishLevelType.PublishSearch)
            {
                ViewBag.ErrorMessage = "You do not have enough permission to access this organization.";
                return this.View("Error");
            }


            // set GroupProductType
            if (UserEntity.GroupId.HasValue)
            {
                if (!UserEntity.IsAdmin && !UserEntity.IsEmployee)
                {
                    var groupRestriction = OrgGroupRestrictedRepository.FindAll(ogr => ogr.OrganizationId == id && ogr.GroupId == UserEntity.GroupId.Value).FirstOrDefault();
                    if (groupRestriction != null)
                    {
                        string orgName = _viewModel.Organization.Name;
                        string reqResearchUrl = string.Format("<a href=\"{0}\" rel=\"{1}\" class=\"request-organization-diligence\" data-request-type=\"{3}\" data-orgname-enc=\"{2}\">here</a>", Url.Action("RequestOrganization", "Request"), id, Server.HtmlEncode(orgName), RequestType.Access);
                        ViewBag.RawHtml = true;
                        ViewBag.ErrorMessage = String.Format("<b>{0}</b> has restricted access to its profile. In order to request access, click {1}.", orgName, reqResearchUrl);
                        return this.View("Error");
                    }
                }

                if (UserEntity.Group != null)
                {
                    var groupOrganization = UserEntity.Group.GroupOrganizations.SingleOrDefault(m => m.OrganizationId == id && m.GroupId == UserEntity.Group.Id);
                    _viewModel.GroupProductType = (groupOrganization != null)
                        ? ProductTypes.DiligenceAccount
                        : (ProductTypes)UserEntity.Group.DefaultProductType;
                }
            }
            else
            {
                _viewModel.GroupProductType = ProductTypes.DiligenceAccount;
            }


            // set OrganizationsInSameSegment
            _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
            if (UserEntity.GroupId.HasValue)
            {
                var availableOrgs = GetAvailableOrganizations();
                if (!availableOrgs.Any())
                {
                    ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                    return this.View("Error");
                }

                _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
            }

            // set folders
            _viewModel.Folders = UserEntity.Folders;

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            OrganizationService.AddOrganizationViewing(UserEntity.Id, id);
            SetUserEntity(UserService.GetUserEntityByUserId(UserEntity.Id));

            // set Preview
            _viewModel.Preview = User.IsInRole(RoleNames.Admin) || User.IsInRole(RoleNames.Employee);

            UserService.TrackOrganizationProfileView(UserEntity.Id, id);

            return View(_viewModel);
        }

        public ActionResult Overview(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationNotes
            _viewModel.OrganizationNotes = NoteRepository.GetByUserIdAndOrganizationId(UserEntity.Id, id).ToList();

            // set OrganizationsInSameSegment
            _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
            if (UserEntity.GroupId.HasValue)
            {
                var availableOrgs = GetAvailableOrganizations();
                if (!availableOrgs.Any())
                {
                    ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                    return this.View("Error");
                }

                _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
            }
            else
            {
                int total;
                _viewModel.OrganizationsInSameSegment.AddRange(SearchService.GetOrganizations(new SearchFilter(), out total));
            }

            // set NewsItems
            //if (_viewModel.NewsItems == null)
            //{
            //	_viewModel.NewsItems =
            //		NewsItemService.GetByOrganizationId(id).OrderByDescending(m => m.PublishedDate).Take(3).ToList();
            //}

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return this.View(_viewModel);
        }

        public ActionResult Grade(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set SearchOptions
            _viewModel.SearchOptions = new SearchOptionsViewModel();
            _viewModel.SearchOptions.Build();

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
                else
                {
                    int total;
                    _viewModel.OrganizationsInSameSegment.AddRange(SearchService.GetOrganizations(new SearchFilter(), out total));
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            return this.View(_viewModel);
        }

        /// <summary>
        /// Team tab
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Team(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                //System.Threading.Tasks.Task.Factory.StartNew(() => OrganizationService.GetById(id))
                //    .ContinueWith(t => _viewModel.Organization = t.Result);
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationMembers
            _viewModel.OrganizationMembers = _viewModel.Organization.OrganizationMembers.ToList();

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return this.View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Strategy(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Process(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Firm(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }
            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }
            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Funds(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //        [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult TrackRecord(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>g
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //       [OutputCache(Duration = 60 * 60 * 6)] // 60 seconds per minute * 60 minutes per hour * 6 hours
        public ActionResult Evaluation(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PrintOverview(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set Folders
            _viewModel.Folders = FolderRepository.GetByUserId(UserEntity.Id);

            // set OrganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View(_viewModel);
        }

        [OrganizationPrintAllCleanupAttribute]
        public ActionResult DownloadFullReport(Guid id)
        {
            var notification = this.UserNotificationService.GetById(id) as OrganizationFullReportNotification;
            if (notification != null && notification.Data != null && System.IO.File.Exists(notification.Data.GeneratedFileName))
            {
                return File(notification.Data.GeneratedFileName, notification.Data.ContentType, notification.Data.DownloadedFileName);
            }
            ViewBag.ErrorMessage = "Report file not found. Perhaps it was already downloaded or deleted.";
            return View("Error");
        }


        [OrganizationPrintAllCleanupAttribute]
        public ActionResult DeleteFullReport(Guid id)
        {
            var notification = this.UserNotificationService.GetById(id) as OrganizationFullReportNotification;
            if (notification != null && notification.Data != null) {
                Utils.DeleteFile(notification.Data.GeneratedFileName);
                return Json("success");
            }
            return Json("error");
        }

        public ActionResult PrintAllHeader(string instanceId)
        {
            string fileName = Path.Combine(AppSettings.TempPath, OrganizationService.GetFullReportHeaderFileName(instanceId));
            return File(fileName, "text/html");
        }

        public ActionResult PrintAllFooter(string instanceId)
        {
            string fileName = Path.Combine(AppSettings.TempPath, OrganizationService.GetFullReportFooterFileName(instanceId));
            return File(fileName, "text/html");
        }

        public string PrintAllPdf(Guid id)
        {
            Guid taskId = Guid.NewGuid();
            string instanceId = Utils.GetTempFileName(taskId);

            string fileName = OrganizationService.GetFullReportFileName(instanceId);
            string sourceFileName = Path.Combine(AppSettings.TempPath, fileName);
            string userName = this.UserEntity.UserName;

            string generatedFile = Path.Combine(AppSettings.TempPath, sourceFileName + ".pdf");
            string url = Url.Action("PrintFullReport", "Request", new { instanceId }, Request.Url.Scheme);
            //string headerUrl = Url.Action("PrintAllHeader", "Request", new { instanceId }, Request.Url.Scheme);
            //string footerUrl = Url.Action("PrintAllFooter", "Request", new { instanceId }, Request.Url.Scheme);
            string headerUrl = Url.Action("PrintAllHeader", "Organization", new { instanceId }, Request.Url.Scheme);
            string footerUrl = Url.Action("PrintAllFooter", "Organization", new { instanceId }, Request.Url.Scheme);
            ViewBag.PrintAllPdfFile = generatedFile;

            var organization = this.GetCurrentOrganization(id);
            var headerModel = new OrganizationPrintAllHeaderViewModel { UserName = userName, OrganizationName = _viewModel.Organization.Name };
            string headerHtml = base.RenderPartialViewToString("PrintAllHeader", headerModel);
            string headerFileName = Path.Combine(AppSettings.TempPath, OrganizationService.GetFullReportHeaderFileName(instanceId));
            System.IO.File.WriteAllText(headerFileName, headerHtml);

            string footerHtml = base.RenderPartialViewToString("PrintAllFooter");
            string footerFileName = Path.Combine(AppSettings.TempPath, OrganizationService.GetFullReportFooterFileName(instanceId));
            System.IO.File.WriteAllText(footerFileName, footerHtml);

            var reportTask = new OrganizationFullReportTask()
            {
                Id = taskId,
                ReferenceId = id.ToString(), // OrganizationId
                UserId = UserEntity.Id
            };
            TaskService.Save(reportTask);

            var notification = new OrganizationFullReportNotification()
            {
                Id = Guid.NewGuid(),
                Data = new OrganizationFullReportNotificationData { Title = _viewModel.Organization.Name, Status= DownloadableItemLifeCycle.Started, Password=userName },
                PollingExpirationDate = DateTime.Now.AddMilliseconds(AppSettings.PdfGenerationTimeoutMS),
                UserId = UserEntity.Id,
                ReferenceId = taskId.ToString()
            };

            UserNotificationService.Save(notification);
            this.UserEntity.Notifications.AddOrUpdate(notification);

            var task = new Task<int>(() =>
            {
                Guid[] orgIDs = null;
                if (UserEntity.Group != null)
                {
                    orgIDs = UserEntity.Group.GetOrganizationIdsOfFullPdfGenerations().ToArray();
                }

                if (!UserEntity.UserCanGenerateProfilePdf(id, orgIDs))
                    throw Utils.Throw("User '{0}' (user id: {1}) not allowed to generate pdf of firm {2} or user's group Organization Full Report pdf generation quota exceeded.", UserEntity.FullName, UserEntity.Id, reportTask.ReferenceId);

                UserService.TrackOrganizationProfileFullReportPdf(UserEntity.Id, id);
                int? generatedPdfsCnt = orgIDs.Count();
                if (!orgIDs.Contains(id)) generatedPdfsCnt++;
                if (generatedPdfsCnt.HasValue && generatedPdfsCnt >= UserEntity.Group.OrganizationProfilePdfQuota)
                {
                    IResults results = EmailDistributionListService.SendOrganizationFullReportPdfQuotaAchieved(AppSettings.MailFrom, UserEntity.Group);
                    foreach (IResult result in results)
                    {
                        Log.Info(result.Message);
                    }
                }

                string downloadedFileName = String.Empty;
                try
                {
                    bool success = PDFHelper.GeneratePDF(url, sourceFileName, AppSettings.PdfZoom, AppSettings.PdfGenerationJSDelayMS,
                        headerUrl, footerUrl, instanceId, AppSettings.PdfGenerationTimeoutMS);
                    if (!success)
                        throw new Exception(String.Format("Organization Full Report generation failed. (OrganizationId: '{0}')", id));

#if DEBUG
                    string pw = (this.UserEntity.IsAdmin || this.UserEntity.IsEmployee) ? null : "a";
#else
                    string pw = (this.UserEntity.IsAdmin || this.UserEntity.IsEmployee) ? null : userName;
#endif
                    string groupName = String.Empty;
                    if (UserEntity.Group != null) groupName = UserEntity.Group.Name;
                    PDFHelper.SetPasswordAndWatermark(generatedFile, pw, UserEntity.FullName, groupName);

                    downloadedFileName = OrganizationService.GetFullReportDownloadFileName(_viewModel.Organization.Name);
                    reportTask.Data.GeneratedFileName = generatedFile;
                    reportTask.Data.DownloadedFileName = downloadedFileName;
                    reportTask.Data.ContentType = "application/pdf";
                    reportTask.Status = DownloadableItemLifeCycle.GenerationCompleted;
                    TaskService.Save(reportTask);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    reportTask.Status = DownloadableItemLifeCycle.Error;
                    TaskService.Save(reportTask);
                    OrganizationService.FullReportCleanup(instanceId);
                }
                return 0;
            });
            task.Start();
            return base.RenderPartialViewToString("UserNotification", notification);
        }

        public ActionResult PrintAllDone()
        {
            var _formsService = ComponentBrokerInstance.RetrieveComponent<IFormsAuthenticationService>();
            _formsService.SignOut();
            HttpContext.Session.RemoveAll();
            return new EmptyResult();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PrintAll(Guid instanceId)
        {
            ITask task = TaskService.GetById(instanceId);
            Guid id;
            if (task == null || !Guid.TryParse(task.ReferenceId, out id))
            {
                ViewBag.ErrorMessage = "Organization Full Report generation cannot be performed.";
                return this.View("Error");
            }

            if (!UserEntity.OrganizationProfilePdfAllowed)
            {
                ViewBag.ErrorMessage = "You do not have permission to generate Organization Full Report.";
                return this.View("Error");
            }

            if (!UserEntity.UserCanGenerateProfilePdf(id, null))
            {
                ViewBag.ErrorMessage = "Your User Group exceeded its Organization Full Report generation quota.";
                return this.View("Error");
            }

            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set Folders
            _viewModel.Folders = FolderRepository.GetByUserId(UserEntity.Id);

            // set ORganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // set GroupProductType
            if (UserEntity.GroupId.HasValue)
            {
                var group = GroupRepository.GetById(UserEntity.GroupId.Value);
                if (group.DefaultProductType.HasValue)
                {
                    _viewModel.GroupProductType = (ProductTypes)group.DefaultProductType.Value;
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            _viewModel.Preview = User.IsInRole(RoleNames.Admin) || User.IsInRole(RoleNames.Employee);
            return View(_viewModel);
        }


        public ActionResult PrintAllOld(Guid id)
        {
            PopulateViewModelFromSession(id);

            // set Organization
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            // set Folders
            _viewModel.Folders = FolderRepository.GetByUserId(UserEntity.Id);

            // set ORganizationsInSameSegment
            if (_viewModel.OrganizationsInSameSegment == null)
            {
                _viewModel.OrganizationsInSameSegment = new List<LuceneSearchResult>();
                if (UserEntity.GroupId.HasValue)
                {
                    var availableOrgs = GetAvailableOrganizations();
                    if (!availableOrgs.Any())
                    {
                        ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                        return this.View("Error");
                    }

                    _viewModel.OrganizationsInSameSegment.AddRange(availableOrgs.ToList());
                }
            }

            // set GroupProductType
            if (UserEntity.GroupId.HasValue)
            {
                var group = GroupRepository.GetById(UserEntity.GroupId.Value);
                if (group.DefaultProductType.HasValue)
                {
                    _viewModel.GroupProductType = (ProductTypes)group.DefaultProductType.Value;
                }
            }

            // user in a group and id not in organizations in segments and id not in purchased organization and group has segments
            if (UserEntity.GroupId.HasValue && !_viewModel.OrganizationsInSameSegment.Select(m => m.Id).Contains(id) && !UserEntity.Group.GroupOrganizations.Any(m => m.OrganizationId == id) && UserEntity.Group.GroupSegments.Select(m => m.Segment).Any())
            {
                ViewBag.ErrorMessage = "The organization you are trying to view is not available in the segment(s) you have purchased.";
                return this.View("Error");
            }

            return View("PrintAll", _viewModel);
        }


        public JsonResult GetScatterChart(Guid id, SearchOptionsViewModel SearchOptions)
        {
            SearchOptions.SectorSpecialist = SearchOptions.SectorSpecialist.GetValueOrDefault(false) ? true : new bool?();
            SearchOptions.RegionalSpecialist = SearchOptions.RegionalSpecialist.GetValueOrDefault(false) ? true : new bool?();

            PopulateViewModelFromSession(id);

            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }

            var viewModel = new ScatterChartViewModel
            {
                OrganizationId = _viewModel.Organization.Id,
                OrganizationName = _viewModel.Organization.Name,
                OrganizationQuantitativeScore =
                    _viewModel.Organization.QuantitativeGradeNumber.GetValueOrDefault(GradeRangeLimit.Ungraded),
                OrganizationQualitativeScore =
                    _viewModel.Organization.QualitativeGradeNumber.GetValueOrDefault(GradeRangeLimit.Ungraded),
                HtmlId = "scatterVisualization"
            };
            var matchingSegments = new List<Segment>();
            var purchasedOrgs = new List<Guid>();
            if (UserEntity.GroupId.HasValue)
            {
                var segments = GroupRepository.GetById(UserEntity.GroupId.Value).GroupSegments.Select(m => m.Segment);
                matchingSegments.AddRange(_viewModel.Organization.FindMatchingSegments(segments.ToList()));

                purchasedOrgs = GroupRepository.GetById(UserEntity.GroupId.Value).GroupOrganizations.Select(s => s.OrganizationId).ToList();
            }

            int total = 0;
            var filter = SearchOptions.ToSearchFilter();
            filter.Segments = matchingSegments;
            filter.PurchasedOrganizationIds = purchasedOrgs;

            viewModel.OtherOrganizations = SearchService.GetOrganizations(filter, out total).Where(m => m.IsAvailableForGPLScatterPlot);

            return Json(new MessageViewModel { Data = total, RenderView = this.RenderPartialViewToString("FluidScatterChartPartial", viewModel) }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult KeepAwayTooltip(Guid orgId)
        {
            var orgModel = base.OrganizationService.GetById(orgId);
            return PartialView("KeyTakeAway", orgModel);
        }


        public JsonResult GetLargeScatterChart(SearchOptionsViewModel SearchOptions)
        {
            SearchOptions.SectorSpecialist = SearchOptions.SectorSpecialist.GetValueOrDefault(false) ? true : new bool?();
            SearchOptions.RegionalSpecialist = SearchOptions.RegionalSpecialist.GetValueOrDefault(false) ? true : new bool?();

            var segments = new List<Segment>();
            var purchasedOrgs = new List<Guid>();
            var viewModel = new ScatterChartViewModel();

            if (UserEntity.GroupId.HasValue)
            {
                segments = GroupRepository.GetById(UserEntity.GroupId.Value).GroupSegments.Select(m => m.Segment).ToList();
                purchasedOrgs = GroupRepository.GetById(UserEntity.GroupId.Value).GroupOrganizations.Select(s => s.OrganizationId).ToList();
            }

            int total = 0;
            var filter = SearchOptions.ToSearchFilter();
            filter.Segments = segments;
            filter.PurchasedOrganizationIds = purchasedOrgs;

            viewModel.OtherOrganizations = SearchService.GetOrganizations(filter, out total).Where(m => (m.Quality.HasValue || m.Quantity.HasValue));

            return Json(new MessageViewModel { Data = total, RenderView = this.RenderPartialViewToString("LargeScatterChartPartial", viewModel) }, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Populate and cache view model.
        /// </summary>
        /// <param name="id"></param>
        private void PopulateViewModelFromSession(Guid id)
        {
            var cache = EntityCache.Get<OrganizationViewModel>(Resources.Params.OrganizationViewModelCache);
            if (cache == null || cache.Id != id)
            {
                cache = new OrganizationViewModel { Id = id };
                EntityCache.Add(Resources.Params.OrganizationViewModelCache, cache);
            }
            _viewModel = cache;
        }


        private AtlasDiligence.Common.Data.Models.Organization GetCurrentOrganization(Guid id)
        {
            if (_viewModel == null)
            {
                PopulateViewModelFromSession(id);
            }
            if (_viewModel.Organization == null)
            {
                _viewModel.Organization = OrganizationService.GetById(id);
            }
            return _viewModel.Organization;
        }


        /// <summary>
        /// Add viewmodel to EntityCache.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            EntityCache.Add(Resources.Params.OrganizationViewModelCache, _viewModel);
            base.Dispose(disposing);
        }


    }
}
