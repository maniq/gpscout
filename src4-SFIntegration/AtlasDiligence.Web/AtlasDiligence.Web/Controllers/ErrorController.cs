﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error(string message)
        {
            ViewBag.Message = message;
            return View();
        }
    }
}
