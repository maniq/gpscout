﻿using System.Web.Mvc;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.General;

namespace AtlasDiligence.Web.Controllers
{
    [MultipleLocationsAuthorize]
    public class ContactController : Controller
    {
        private IContactService ContactService
        {
            get { return ComponentBrokerInstance.RetrieveComponent<IContactService>(); }
        }

        /// <summary>
        /// Create the contact form.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Process the contact form
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(ContactViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                ContactService.SendContactEmail(viewModel);
                viewModel = new ContactViewModel();
                viewModel.Submitted = true;
            }

            return View(viewModel);
        }
    }
}
