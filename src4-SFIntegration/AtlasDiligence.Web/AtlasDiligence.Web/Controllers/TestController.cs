﻿using AtlanticBT.Common.ComponentBroker;
using GPScout.Domain.Contracts.Services;
using System.Linq;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    public class TestController : ControllerBase
    {
        public ActionResult Index()
        {
            var requests = ComponentBrokerInstance.RetrieveComponent<IOrganizationService>().GetAllClientRequests();
            return Content(requests.Count().ToString());
        }
    }
}
