﻿using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.Types;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Controllers.Services;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using IUserService = AtlasDiligence.Web.Controllers.Services.IUserService;

namespace AtlasDiligence.Web.Controllers
{
    public class ControllerBase : AsyncController
    {
        #region diagnostics
        private DateTime startActionExecuting;

        private DateTime startResultExecuting;

        private readonly DateTime startRequest = DateTime.Now;

        protected void CopyModelState()
        {
            ModelStateDictionary ms = TempData["ModelState"] as ModelStateDictionary;
            if (ms != null)
            {
                ms.ToList().ForEach(e => {
                    ModelState.Add(e.Key, e.Value);
                });
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.UserEntity = this.UserEntity;
            }
            startActionExecuting = DateTime.Now;
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var totalSeconds = (DateTime.Now - startActionExecuting).TotalSeconds;
            System.Diagnostics.Debug.WriteLine(string.Format("Request for {0}, {1} seconds.",
                                                             filterContext.RequestContext.HttpContext.Request.Url.
                                                                 AbsoluteUri,
                                                             totalSeconds.ToString("N2")));
            base.OnActionExecuted(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            startResultExecuting = DateTime.Now;
            base.OnResultExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var totalSeconds = (DateTime.Now - startResultExecuting).TotalSeconds;

            System.Diagnostics.Debug.WriteLine(string.Format("Result executed for {0}, {1} seconds.",
                                                                filterContext.RequestContext.HttpContext.Request.Url.
                                                                    AbsoluteUri,
                                                                totalSeconds.ToString("N2")));
            base.OnResultExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            //System.Diagnostics.Debug.WriteLine(filterContext.Exception.Message, "error");
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            //always reup the session variable with current UserEntity
            EntityCache.Add(Resources.Params.UserEntityCache, UserEntity);
            base.Dispose(disposing);
        }

        #region repositories
        private IOrganizationService _organizationService;

        protected internal IRequestService RequestService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRequestService>();
            }
        }

        protected internal IUserNotificationService UserNotificationService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserNotificationService>();
            }
        }

        protected ITaskService TaskService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ITaskService>();
            }
        }

        public IOrganizationService OrganizationService
        {
            get
            {
                if (_organizationService == null) 
                    _organizationService = ComponentBrokerInstance.RetrieveComponent<IOrganizationService>();
                return _organizationService;
            }
        }

        protected IEmailDistributionListService EmailDistributionListService {
            get {
                return ComponentBrokerInstance.RetrieveComponent<IEmailDistributionListService>();
            }
        }

        public IEulaRepository EulaRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEulaRepository>();
            }
        }

        public ISearchTermRepository SearchTermRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ISearchTermRepository>();
            }
        }

        public IFolderRepository FolderRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IFolderRepository>();
            }
        }

        public INoteRepository NoteRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<INoteRepository>();
            }
        }

        public INewsRepository NewsRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<INewsRepository>();
            }
        }

        public IRequestRepository RequestRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IRequestRepository>();
            }
        }

        public IGroupRepository GroupRepository
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IGroupRepository>();
            }
        }


        private IRepository<GroupOrganizationRestricted> _orgGroupRestrictedRepository;
        protected IRepository<GroupOrganizationRestricted> OrgGroupRestrictedRepository
        {
            get
            {
                if (_orgGroupRestrictedRepository == null)
                    _orgGroupRestrictedRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<GroupOrganizationRestricted>>();
                return _orgGroupRestrictedRepository;
            }
        }


        public IUserService UserService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IUserService>();
            }
        }

        public ISearchService SearchService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ISearchService>();
            }
        }

        #endregion

        #region ETag
        protected static string ETagApp
        {
            get
            {
                return ConfigurationManager.AppSettings["ETagApp"];
            }
        }
        #endregion

        #region AppSettings
        protected string MailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings["MailFrom"];
            }
        }
        #endregion
        #region user entity
        private UserEntity _userEntity { get; set; }

        /// <summary>
        /// Used to set UserEntity when user is edited.
        /// </summary>
        /// <remarks>Use this method sparingly.</remarks>
        /// <param name="userEntity"></param>
        [Authorize]
        protected void SetUserEntity(UserEntity userEntity)
        {
            _userEntity = userEntity;
        }

        protected internal UserEntity UserEntity
        {
            get
            {
                if (_userEntity == null)
                {
                    _userEntity = ControllerBase.CachedUserEntity();
                }
                return _userEntity;
            }
        }
        #endregion

        protected void ClearUserEntity()
        {
            EntityCache.Remove(Resources.Params.UserEntityCache);
            _userEntity = null;
        }

        public static UserEntity CachedUserEntity()
        {
            var cache = EntityCache.Get<UserEntity>(Resources.Params.UserEntityCache);
            if (cache == null)
            {
                var membershipUser = Membership.GetUser();
                if (membershipUser != null && membershipUser.ProviderUserKey != null)
                {
                    var userId = (membershipUser.ProviderUserKey.ToString()).Maybe<Guid>();

                    if (userId.HasValue)
                    {
                        var userService = ComponentBrokerInstance.RetrieveComponent<IUserService>();
                        cache = userService.GetUserEntityByUserId(userId.Value);
                        EntityCache.Add(Resources.Params.UserEntityCache, cache);
                    }
                }
            }
            return cache;
        }

        #region RenderPartialViewToString
        /// <summary>
        /// Render a partial view to string to return as a Json value
        /// </summary>
        /// <returns></returns>
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }

        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }

        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

		#region helpers

		/// <summary>
		/// returns UserId for logged in user.
		/// </summary>
		/// <returns>UserId or Empty Guid</returns>
		protected Guid GetLoggedInUserId()
		{
			var loggedInUser = Membership.GetUser();

			if (loggedInUser != null && loggedInUser.IsOnline && loggedInUser.ProviderUserKey != null)
			{
				return loggedInUser.ProviderUserKey.ToString().Maybe<Guid>().GetValueOrDefault(Guid.Empty);
			}
			return Guid.Empty;
		}

		/// <summary>
		/// Return the number of rowsPerPage displayed in a YUI datagrid
		/// </summary>
		/// <returns></returns>
		protected int DataGridRowsPerPage()
		{
			return ConfigurationManager.AppSettings.Get("DataGridRowsPerPage").Maybe<int>() ?? 10;
		}

		/// <summary>
		/// Check to see that current UserEntity can access group.
		/// </summary>
		/// <param name="groupId"></param>
		/// <returns></returns>
		protected bool CanAccessGroup(Guid groupId)
		{
			if (User.IsInRole(RoleNames.Admin))
				return true;
			if (groupId == UserEntity.GroupId)
				return true;

			return false;
		}

	    protected IEnumerable<LuceneSearchResult> GetAvailableOrganizations()
	    {
            return SearchService.GetAvailableOrganizations(UserEntity.Id);
	    }

        internal string ProcessReturnUrl()
        {
            string returnUrl = Convert.ToString(Session["returnUrl"]);
            if (returnUrl != null) Session["returnUrl"] = null;
            return returnUrl;
        }

        internal void BeforeLogon()
        {
            string returnUrl = HttpContext.Request.QueryString["returnUrl"];
            if (returnUrl != null)
            {
                returnUrl = Server.UrlDecode(returnUrl);
                if (!returnUrl.StartsWith("http"))
                    returnUrl = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + returnUrl;

                Session["returnUrl"] = returnUrl;
            }
        }
        #endregion
    }
}