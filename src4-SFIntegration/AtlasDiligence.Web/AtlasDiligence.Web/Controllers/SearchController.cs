﻿using AtlanticBT.Common.ComponentBroker;
using AtlanticBT.Common.GeoLocation;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using AtlasDiligence.Web.Models.ViewModels.Map;
using AtlasDiligence.Web.Models.ViewModels.Search;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    public class SearchController : ControllerBase
    {
        private readonly ISearchRepository searchRepository;

        public SearchController()
        {
            this.searchRepository = ComponentBrokerInstance.RetrieveComponent<ISearchRepository>();
        }

        public ActionResult Saved()
        {
            var savedSearches =
                searchRepository.GetSavedSearches(UserEntity.Id);
            return this.View(savedSearches);
        }

        public JsonResult Delete(Guid id)
        {
            var savedSearch = searchRepository.GetSavedSearch(id);
            if (UserEntity.Id != savedSearch.SearchTemplate.UserId)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "You are unable to perform this action." },
                            JsonRequestBehavior.AllowGet);
            }
            searchRepository.DeleteSavedSearch(id);
            UserEntity.SavedSearches.Remove(UserEntity.SavedSearches.First(m => m.SearchTemplateId == id));
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Rename(Guid id, string name)
        {
            var savedSearch = searchRepository.GetSavedSearch(id);
            if (UserEntity.Id != savedSearch.SearchTemplate.UserId)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "You are unable to perform this action." },
                            JsonRequestBehavior.AllowGet);
            }
            savedSearch.Name = name;
            searchRepository.SubmitChanges();

            UserEntity.SavedSearches.First(m => m.SearchTemplateId == id).Name = name;
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Save(Guid id, string name)
        {
            var savedSearch = searchRepository.GetSavedSearch(id);
            if (savedSearch != null)
            {
                return Json(new MessageViewModel { Status = MessageStatus.Failure, Message = "This search has already been saved." },
                            JsonRequestBehavior.AllowGet);
            }
            savedSearch = new SavedSearch
                {
                    Name = name,
                    SavedOn = DateTime.Now,
                    SearchTemplateId = id
                };
            searchRepository.InsertSavedSearch(savedSearch);
            this.SetUserEntity(UserService.GetUserEntityByUserId(UserEntity.Id));
            return Json(new MessageViewModel { Status = MessageStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Start()
        {
            var searchViewModel = new SearchViewModel();

            searchViewModel.Build(UserEntity.Id);

            return View(searchViewModel);
        }

        [HttpGet]
        public ActionResult Index(Guid? id)
        {
			var searchViewModel = new SearchViewModel();

			searchViewModel.Build(UserEntity.Id, id);

			return View(searchViewModel);
        }

        [HttpPost]
        public ActionResult SaveTemplate(SearchOptionsViewModel optionsViewModel)
        {
            var searchViewModel = new SearchViewModel { SearchOptions = optionsViewModel };
            if (ModelState.IsValid)
            {
                if (optionsViewModel.SelectedInvestmentRegions != null)
                {
                    optionsViewModel.SelectedInvestmentRegions = optionsViewModel.SelectedInvestmentRegions.Select(x => x.Trim()).Where(x => x != String.Empty).Distinct().ToList();
                }

                if (optionsViewModel.ProximitySearchLat.HasValue)
                {
                    searchViewModel.ProximitySearch = new ProximitySearch { Lat = optionsViewModel.ProximitySearchLat.Value, Lng = optionsViewModel.ProximitySearchLng.Value, Radius = optionsViewModel.SelectedProximitySearchMiles.Value };
                }

                if (optionsViewModel.SelectedNextFundraiseValue != null)
                {
                    optionsViewModel.SelectedNextFundraiseStartDate = DateTime.Now.Date;
                    optionsViewModel.SelectedNextFundraiseEndDate = DateTime.Now.Date.AddMonths(Convert.ToInt32(optionsViewModel.SelectedNextFundraiseValue));
                }

                SetGradesList(optionsViewModel);

                var template = MapToSearchTemplate(optionsViewModel);
                this.searchRepository.Insert(template);

                var templateFilters = MapToSearchTemplateFilters(optionsViewModel, template.Id);
                this.searchRepository.InsertAll(templateFilters);
                var recentTemplates =
                    this.searchRepository.GetSearchTemplatesByUserId(UserEntity.Id).OrderByDescending(o => o.CreatedDate).
                        Take(5).ToList();
                UserEntity.RecentSearches = recentTemplates.ToDictionary(x => x.Id, x =>
                    (String.IsNullOrWhiteSpace(x.Name) ? "Advanced Search - " : x.Name + " - ") + x.CreatedDate.ToString("MM/dd"));
                UserEntity.RecentSearchTemplates = recentTemplates;
                if (!optionsViewModel.FundSizeMinimum.HasValue && !optionsViewModel.FundSizeMaximum.HasValue)
                {
                    optionsViewModel.SelectedCurrencies = new List<string>();
                    ModelState.Remove("SelectedCurrencies");
                }
            }
            searchViewModel.Build(UserEntity.Id);
            return View("Index", searchViewModel);
        }


        #region Proximity Search
        IGeocodingService _geocodingService;
        IGeocodingService GeocodingService
        {
            get
            {
                if (_geocodingService == null) _geocodingService = new GeocodingService();
                return _geocodingService;
            }
        }


        public ActionResult OrganizationProximityAutoComplete(string term)
        {
            //var geocodingResponse = GeocodingService.GetAddress(term);
            //if (geocodingResponse != null)
            //{
            //    geocodingResponse.Results[]
            //}

            var segments = new List<Segment>();
            var purchasedOrgs = new List<Guid>();
            if (UserEntity.GroupId.HasValue)
            {
                var group = this.GroupRepository.GetById(UserEntity.GroupId.Value);
                segments = group.GroupSegments.Select(m => m.Segment).ToList();
                purchasedOrgs = group.GroupOrganizations.Select(m => m.OrganizationId).ToList();
            }
            var orgNames = SearchService.GetOrganizationsContainingTerm(segments, purchasedOrgs, term);
            return Json(orgNames.Select(m => new { Id = m.Id, Name = m.Name, IsPublished = m.IsPublished }).Distinct(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult SearchLuceneGrid(
				int pageStart, string[] sortColumn, bool[] sortAscending, int rowsPerPage, string Search,
				string SelectedStrategy, string SelectedMarketStage, string SelectedSector,
				//string SelectedInvestmentSubRegion,
                string SelectedInvestmentRegions,
                string SelectedSubStrategy, //string SelectedCountry,
				int? SelectedQuantitativeGradeId, int? SelectedQualitativeGradeId,
				string SelectedFundraisingStatus, double? FundSizeMinimum, double? FundSizeMaximum,
				int? NumberOfFundsClosedMinimum, int? NumberOfFundsClosedMaximum,
				bool? EmergingManager, string SelectedFocusList, bool? AccessConstrained,
				int? DiligenceStatusMinimum, int? DiligenceStatusMaximum,
				DateTime? SelectedNextFundraiseStartDate, DateTime? SelectedNextFundraiseEndDate,
				bool? ShowOnlyPublished, bool? SectorSpecialist, string SelectedCurrencies, bool? SBICFund, string SelectedNextFundraiseValue,
                int? SelectedProximitySearchMiles, double? ProximitySearchLat, double? ProximitySearchLng, bool? RegionalSpecialist, bool? ShowInactiveFirms
            //ProximitySearch ProximitySearch
            )
		{
			if (rowsPerPage > 100) rowsPerPage = 100;

            if (sortColumn.Length == 1 && sortColumn[0] == String.Empty)
            {
                sortColumn = new string[] { "CustomSortColumn", String.Empty };
                //sortColumn = new string[] { "InactiveFirm", String.Empty };
                sortAscending = new bool[] { true, true };
            }

			List<SearchField> sortFields = new List<SearchField>();

            //need to set key to full dot-notation data model path.
            foreach (string _sortColumn in sortColumn)
            {
                switch (_sortColumn)
                {
                    case "CustomSortColumn":
                        sortFields.Add(SearchField.OrganizationCustomSortColumn);
                        break;
                    case "InactiveFirm":
                        sortFields.Add(SearchField.OrganizationInactiveFirm);
                        break;
                    case "SubCategory":
                        sortFields.Add(SearchField.OrganizationMarketStage);
                        break;
                    case "PrimaryOffice":
                        sortFields.Add(SearchField.OrganizationPrimaryOffice);
                        break;
                    case "GeographicFocus":
                    case "InvestmentRegion":
                        sortFields.Add(SearchField.OrganizationInvestmentRegion);
                        break;
                    case "Strategy":
                        sortFields.Add(SearchField.OrganizationStrategies);
                        break;
                    case "YearFounded":
                        sortFields.Add(SearchField.OrganizationYearFounded);
                        break;
                    case "FundraisingStatus":
                        sortFields.Add(SearchField.OrganizationFundraisingStatus);
                        break;
                    case "LastUpdated":
                        sortFields.Add(SearchField.OrganizationLastUpdated);
                        break;
                    case "DiligenceLevel":
                        sortFields.Add(SearchField.OrganizationDiligenceLevel);
                        break;
                    case "AtlasQualitative":
                        sortFields.Add(SearchField.OrganizationQualitativeGrades);
                        break;
                    case "QoRPerformance":
                        sortFields.Add(SearchField.OrganizationQuantitativeGrades);
                        break;
                    default:
                        sortFields.Add(SearchField.OrganizationName);
                        break;
                }
            }

			int selectedNextFundraiseIntValue = Convert.ToInt16(SelectedNextFundraiseValue);

			if (!String.IsNullOrWhiteSpace(SelectedNextFundraiseValue) && selectedNextFundraiseIntValue != 0)
			{
				SelectedNextFundraiseStartDate = DateTime.Now.Date;
				SelectedNextFundraiseEndDate = DateTime.Now.Date.AddMonths(selectedNextFundraiseIntValue);
			}
			var qualitativeGrades = new List<string>();
			foreach (var grade in Enum.GetValues(typeof(Grades)))
			{
				if ((int)grade >= SelectedQualitativeGradeId)
				{
					qualitativeGrades.Add(Enum.GetName(typeof(Grades), grade));
				}
			}
			var quantitativeGrades = new List<string>();
			foreach (var grade in Enum.GetValues(typeof(Grades)))
			{
				if ((int)grade >= SelectedQuantitativeGradeId)
				{
					quantitativeGrades.Add(Enum.GetName(typeof(Grades), grade));
				}
			}
            var filter = new SearchFilter
            {
                AccessConstrained = AccessConstrained,
                DiligenceStatusMaximum = DiligenceStatusMaximum,
                DiligenceStatusMinimum = DiligenceStatusMinimum,
                EmergingManager = EmergingManager,
                SectorSpecialist = SectorSpecialist,
                RegionalSpecialist = RegionalSpecialist,
                FocusList = string.IsNullOrEmpty(SelectedFocusList) ? new List<string>() : SelectedFocusList.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                FundSizeMaximum = FundSizeMaximum,
                FundSizeMinimum = FundSizeMinimum,
                FundraisingStatus = string.IsNullOrEmpty(SelectedFundraisingStatus) ? new List<string>() : SelectedFundraisingStatus.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                //InvestmentRegions = SelectedInvestmentRegions == null ? new List<string>() : SelectedInvestmentRegions.Select(x=>x.Value).Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                //InvestmentRegions = string.IsNullOrEmpty(SelectedInvestmentRegions) ? new List<string>() : SelectedInvestmentRegions.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                InvestmentRegions = GetInvestmentRegions(SelectedInvestmentRegions),
                //InvestmentSubRegions = string.IsNullOrEmpty(SelectedInvestmentSubRegion) ? new List<string>() : SelectedInvestmentSubRegion.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                //SubStrategies = string.IsNullOrEmpty(SelectedSubStrategy) ? new List<string>() : SelectedSubStrategy.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                //Countries = string.IsNullOrEmpty(SelectedCountry) ? new List<string>() : SelectedCountry.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                QualitativeGrades = !SelectedQualitativeGradeId.HasValue ? new List<string>() : qualitativeGrades,
                QuantitativeGrades = !SelectedQuantitativeGradeId.HasValue ? new List<string>() : quantitativeGrades,
                Currencies = string.IsNullOrEmpty(SelectedCurrencies) ? new List<string>() : SelectedCurrencies.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                MarketStage = string.IsNullOrEmpty(SelectedMarketStage) ? new List<string>() : SelectedMarketStage.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                NumberOfFundsClosedMaximum = NumberOfFundsClosedMaximum,
                NumberOfFundsClosedMinimum = NumberOfFundsClosedMinimum,
                Sector = string.IsNullOrEmpty(SelectedSector) ? new List<string>() : SelectedSector.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                Strategies = string.IsNullOrEmpty(SelectedStrategy) ? new List<string>() : SelectedStrategy.Split(",".ToCharArray()).Where(m => !String.IsNullOrWhiteSpace(m)).ToList(),
                Skip = pageStart,
                Take = rowsPerPage,
                Term = Search,
                IsDescending = sortAscending,
                SortFields = sortFields.ToArray(),
                ExpectedNextFundraiseEndDate = SelectedNextFundraiseEndDate,
                ExpectedNextFundraiseStartDate = SelectedNextFundraiseStartDate,
                ShowOnlyPublished = ShowOnlyPublished.HasValue && ShowOnlyPublished.Value,
                SBICFund = SBICFund,
                ProximitySearch = (ProximitySearchLat.HasValue && ProximitySearchLng.HasValue) ? new ProximitySearch { Lat = ProximitySearchLat.Value, Lng = ProximitySearchLng.Value, Radius = SelectedProximitySearchMiles.Value } : null,
                //ProximitySearch = ProximitySearch
                ShowInactiveFirms = ShowInactiveFirms
            };
			if (UserEntity.GroupId.HasValue)
			{
				var group = this.GroupRepository.GetById(UserEntity.GroupId.Value);
				filter.Segments = group.GroupSegments.Select(m => m.Segment);
				filter.PurchasedOrganizationIds = group.GroupOrganizations.Select(m => m.OrganizationId);
			}
			int total;
			var orgs = ComponentBrokerInstance.RetrieveComponent<ISearchService>().Search(filter, out total).ToList();

			//preview unpublished profile pages
			if (User.IsInRole(RoleNames.Admin) || User.IsInRole(RoleNames.Employee))
			{
				orgs.ForEach(x => {
                    x.PublishOverviewAndTeam = true;
                    if (UserEntity.PUCNOrganizationIDs.Contains(x.Id))
                        x.IsUserRegisteredForPUCN = true;
                    else if (UserEntity.AvailableOrganizationIDs.Contains(x.Id))
                        x.IsUserRegisteredForPUCN = false;
                    else
                        x.IsUserRegisteredForPUCN = null;
                });
			}
			var results = new GridList<SearchDataGrid> { Total = total, Result = orgs.Select(x => {
                string FPVHtml =  base.RenderPartialViewToString("KeyTakeAway", x);
                var rowData = new SearchDataGrid(x, FPVHtml);
                return rowData;
            }).ToArray() };

			return Json(results);
		}

        private IList<string> GetInvestmentRegions(string selectedInvestmentRegions)
        {
            if (selectedInvestmentRegions == null) return null;
            var subRegions = searchRepository.GetDistinctOptions()
                .Where(x => x.Field == (int)FieldType.InvestmentRegion && x.Value != null && x.Value.Contains("-"))
                .Select(x => x.Value).Distinct().ToArray();
            var regions = new List<string>();
            foreach (string region in selectedInvestmentRegions.Split(',').Select(x => x.Trim()).Where(x => x != string.Empty))
            {
                regions.Add(region);
                if (!region.Contains("-"))
                    regions.AddRange(subRegions.Where(sr => sr.StartsWith(region)));
            }
            return regions.Distinct().ToList();
        }

        public JsonResult LoadFirmsInProgress(int skip, int take)
		{
			var firms =
				ComponentBrokerInstance.RetrieveComponent<IRepository<ResearchPriorityOrganization>>()
							   .GetAll()
							   .OrderBy(m => m.Name)
							   .Skip(skip)
							   .Take(take);
			return
				Json(
					new MessageViewModel
					{
						RenderView = this.RenderPartialViewToString("FirmsInProgressRows", firms),
						Data = firms.Any()
					},
					JsonRequestBehavior.AllowGet);
		}


        public JsonResult LoadRecentlyUpdated(int skip, int take, bool? showLastLoginOnly, double? tick)
        {
            var availableOrgs = GetAvailableOrganizations();
            //var firms = OrganizationService.GetAllByIds(availableOrgs.Where(m => m.LastUpdated.HasValue).OrderByDescending(m => m.LastUpdated).Select(m => m.Id).Skip(skip).Take(take));

            var firms = OrganizationService.GetAllByIds(availableOrgs.Where(m => m.LastUpdated.HasValue && (!showLastLoginOnly.HasValue || showLastLoginOnly == false || m.LastUpdated >= UserEntity.LastLogin))
                .OrderByDescending(m => m.LastUpdated).Select(m => m.Id).Skip(skip).Take(take));

            return
                Json(
                    new MessageViewModel
                    {
                        RenderView = this.RenderPartialViewToString("RecentlyUpdatedRows", firms),
                        Data = firms.Any(),
                        Tick = tick ?? 0
                    },
                    JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetInfoWindowContent(Guid id)
		{
			var retVal = ComponentBrokerInstance.RetrieveComponent<IOrganizationService>().GetById(id);

			return retVal == null ? Json(null) : Json(new
			{
				Name = retVal.Name,
				City = retVal.Address1City,
				Country = retVal.Address1Country,
				Website = retVal.WebsiteUrl,
				InvestmentRegions = string.Join(", ", retVal.InvestmentRegions),
				Strategies = retVal.Strategy,
				Market = retVal.MarketStage,
				FundraisingStatus = retVal.FundRaisingStatus,
				InfoWindowHtml = this.RenderPartialViewToString("InfoWindow", retVal)
			}, JsonRequestBehavior.AllowGet);
		}

		#region Dashboard tabs

		public ActionResult Map()
		{
			var mapViewModel = new MapViewModel();

			mapViewModel.Build(GetAvailableOrganizations(), OrganizationService);

			return View("Map", mapViewModel);
		}

		public ActionResult Scatter()
		{
			var scatterViewModel = new ScatterTabViewModel();

			scatterViewModel.Build(GetAvailableOrganizations());

			return View("Scatter", scatterViewModel);
		}

		public ActionResult RecentFirms()
		{
			return View("RecentFirms");
		}

		public ActionResult RecentlyUpdated(bool? showLastLoginOnly)
		{
			var recentlyUpdatedViewModel = new RecentlyUpdatedViewModel();
            recentlyUpdatedViewModel.SetTimeStamp();
            recentlyUpdatedViewModel.LastLoginDate = UserEntity.LastLogin;
            recentlyUpdatedViewModel.SinceLastLoginOnly = showLastLoginOnly.HasValue ? showLastLoginOnly.Value : true;
			recentlyUpdatedViewModel.Build(GetAvailableOrganizations(), OrganizationService);

			return View("RecentlyUpdated", recentlyUpdatedViewModel);
		}

		public ActionResult FirmsInProcess()
		{
			var researchPriorityRepository = ComponentBrokerInstance.RetrieveComponent<IRepository<ResearchPriorityOrganization>>();
			var firms = researchPriorityRepository.GetAll().OrderBy(m => m.Name).Take(10);

			return View("FirmsInProcess", firms);
		}

		public ActionResult RecentFolders()
		{
			var folderRepository = ComponentBrokerInstance.RetrieveComponent<IFolderRepository>();
			var folders = folderRepository.GetRecentFoldersForUser(UserEntity.Id, 5).Select(f => new FolderViewModel(f));

			return View("RecentFolders", folders);
		}

		#endregion
		
        #region AddSearchTemplateFilterToCollection methods

        private void AddSearchTemplateFilterToCollection(string value, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            collection.Add(
                    new SearchTemplateFilter
                    {
                        Field = (int)fieldType,
                        SearchTemplateId = searchTemplateId,
                        Value = value,
                        Id = Guid.NewGuid()
                    });
        }

        private void AddSearchTemplateFilterToCollection(IEnumerable<string> selectedValues, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            if (selectedValues == null)
            {
                return;
            }

            foreach (var value in selectedValues)
            {
                this.AddSearchTemplateFilterToCollection(value, fieldType, searchTemplateId, collection);
            }
        }

        private void AddSearchTemplateFilterToCollection(int? input, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            if (input.HasValue)
            {
                this.AddSearchTemplateFilterToCollection(input.Value.ToString(), fieldType, searchTemplateId, collection);
            }
        }

        private void AddSearchTemplateFilterToCollection(double? input, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            if (input.HasValue)
            {
                this.AddSearchTemplateFilterToCollection(input.Value.ToString(), fieldType, searchTemplateId, collection);
            }
        }

        private void AddSearchTemplateFilterToCollection(bool? input, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            if (input.HasValue)
            {
                this.AddSearchTemplateFilterToCollection(input.Value.ToString(), fieldType, searchTemplateId, collection);
            }
        }

        private void AddSearchTemplateFilterToCollection(DateTime? input, FieldType fieldType, Guid searchTemplateId, ICollection<SearchTemplateFilter> collection)
        {
            if (input.HasValue)
            {
                this.AddSearchTemplateFilterToCollection(input.Value.ToString(), fieldType, searchTemplateId, collection);
            }
        }

        #endregion

		#region helpers

		private void SetGradesList(SearchOptionsViewModel optionsViewModel)
		{
			var quantIndex = optionsViewModel.SelectedQuantitativeGradeID;
			var qualIndex = optionsViewModel.SelectedQualitativeGradeID;

			// initialize lists
			optionsViewModel.SelectedQuantitativeGrades = new List<string>();
			optionsViewModel.SelectedQualitativeGrades = new List<string>();

			// add values to lists
			var GradesList = Enum.GetValues(typeof(Grades));
			for (int i = 0; i < GradesList.Length; i++)
			{
				if (i >= (quantIndex - 1))
				{
					optionsViewModel.SelectedQuantitativeGrades.Add(Enum.GetName(typeof(Grades), i + 1));
				}

				if (i >= (qualIndex - 1))
				{
					optionsViewModel.SelectedQualitativeGrades.Add(Enum.GetName(typeof(Grades), i + 1));
				}
			}
		}

		private SearchTemplate MapToSearchTemplate(SearchOptionsViewModel viewModel)
		{
			return new SearchTemplate
			{
				Id = Guid.NewGuid(),
				CreatedDate = DateTime.Now,
				Name = viewModel.Search,    //Figure this line out...
				KeywordField = viewModel.Search,
				UserId = UserEntity.Id
			};
		}

		private IEnumerable<SearchTemplateFilter> MapToSearchTemplateFilters(SearchOptionsViewModel viewModel, Guid searchTemplateId)
		{
			var result = new List<SearchTemplateFilter>();

			this.AddSearchTemplateFilterToCollection(viewModel.SelectedStrategy, FieldType.Strategy, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedMarketStage, FieldType.MarketStage, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedSector, FieldType.SectorFocus, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedInvestmentRegions, FieldType.InvestmentRegion, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedFundraisingStatus, FieldType.FundraisingStatus, searchTemplateId, result);
			//this.AddSearchTemplateFilterToCollection(viewModel.SelectedSubStrategy, FieldType.SubStrategy, searchTemplateId, result);
			//this.AddSearchTemplateFilterToCollection(viewModel.SelectedInvestmentSubRegion, FieldType.SubRegion, searchTemplateId, result);
			//this.AddSearchTemplateFilterToCollection(viewModel.SelectedCountry, FieldType.Country, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedQualitativeGradeID, FieldType.QualitativeGrade, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedQuantitativeGradeID, FieldType.QuantitativeGrade, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.FundSizeMinimum, FieldType.FundSizeMin, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.FundSizeMaximum, FieldType.FundSizeMax, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.NumberOfFundsClosedMinimum, FieldType.NumberOfFundsClosedMin, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.NumberOfFundsClosedMaximum, FieldType.NumberOfFundsClosedMax, searchTemplateId, result);
			if (viewModel.FundSizeMinimum.HasValue || viewModel.FundSizeMaximum.HasValue)
			{
				this.AddSearchTemplateFilterToCollection(viewModel.SelectedCurrencies, FieldType.Currency, searchTemplateId, result);
			}
			this.AddSearchTemplateFilterToCollection(viewModel.DiligenceStatusMinimum, FieldType.DiligenceStatusMin, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.DiligenceStatusMaximum, FieldType.DiligenceStatusMax, searchTemplateId, result);
            if (viewModel.FocusListSelectedFlag)
            {
                if (viewModel.SelectedFocusList == null || !viewModel.SelectedFocusList.Any(x => x == SearchOptionsViewModel.FocusListStr))
                {
                    if (viewModel.SelectedFocusList == null) viewModel.SelectedFocusList = new List<string>() { SearchOptionsViewModel.FocusListStr };
                    else viewModel.SelectedFocusList.Add(SearchOptionsViewModel.FocusListStr);
                }
            }
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedFocusList, FieldType.FocusList, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.AccessConstrained, FieldType.AccessConstrained, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.EmergingManager, FieldType.EmergingManager, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SectorSpecialist, FieldType.SectorSpecialist, searchTemplateId, result);
            this.AddSearchTemplateFilterToCollection(viewModel.RegionalSpecialist, FieldType.RegionalSpecialist, searchTemplateId, result);
            this.AddSearchTemplateFilterToCollection(viewModel.SelectedNextFundraiseStartDate, FieldType.ExpectedNextFundraiseStartDate, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedNextFundraiseEndDate, FieldType.ExpectedNextFundraiseEndDate, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.ShowOnlyPublished, FieldType.ShowOnlyPublished, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SBICFund, FieldType.SBICFund, searchTemplateId, result);
			this.AddSearchTemplateFilterToCollection(viewModel.SelectedNextFundraiseValue, FieldType.SelectedNextFundraiseValue, searchTemplateId, result);

            if (!string.IsNullOrWhiteSpace(viewModel.ProximitySearchAddress))
            {
                this.AddSearchTemplateFilterToCollection(viewModel.SelectedProximitySearchMiles, FieldType.ProximitySearchMiles, searchTemplateId, result);
                this.AddSearchTemplateFilterToCollection(viewModel.ProximitySearchAddress, FieldType.ProximitySearchAddress, searchTemplateId, result);
                this.AddSearchTemplateFilterToCollection(viewModel.ProximitySearchLat, FieldType.ProximitySearchLat, searchTemplateId, result);
                this.AddSearchTemplateFilterToCollection(viewModel.ProximitySearchLng, FieldType.ProximitySearchLng, searchTemplateId, result);
            }

            this.AddSearchTemplateFilterToCollection(viewModel.ShowInactiveFirms, FieldType.ShowInactiveFirms, searchTemplateId, result);
			return result;
		}

		#endregion
    }
}