﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TableauServer.Contracts;

namespace AtlasDiligence.Web.Controllers
{
    [AtlasAuthorize]
    [EulaAuthorize]
    [MultipleLocationsAuthorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    public class MarketDataController : ControllerBase
    {
        protected internal ITableauServer TableauServer
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<ITableauServer>();
            }
        }


        public ActionResult Index(string id)
        {
            string key = TableauServer.Create(null).GetTrustedAuthenticationTicket(UserEntity.UserName);
            return View( new MarketDataViewModel { TableauServerTrustedAuthKey = key, ViewName = id } );
        }

    }
}