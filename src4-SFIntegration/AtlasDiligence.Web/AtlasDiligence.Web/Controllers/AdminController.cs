﻿using System;
using System.Linq;
using System.Web.Mvc;
using AtlanticBT.Common.Types;
using AtlanticBT.Common.Web.Mvc.ActionResults;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Web.General;
using AtlasDiligence.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using AtlasDiligence.Common.Data.Repositories;
using AtlasDiligence.Web.Models.ViewModels.YuiMaps;
using System.Web;
using AtlanticBT.Common.ComponentBroker;
using GPScout.Domain.Contracts.Services;
using AtlasDiligence.Web.Models.ViewModels.EmailDistributionList;
using AtlasDiligence.Web.Extensions;
using System.Globalization;

namespace AtlasDiligence.Web.Controllers
{
    [Authorize(Roles = RoleNames.Admin)]
    [MultipleLocationsAuthorize]
    public class AdminController : ControllerBase
    {

        private IIndexService IndexService
        {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IIndexService>();
            }
        }

        private IEmailDistributionListService EmailDistributionListService
         {
            get
            {
                return ComponentBrokerInstance.RetrieveComponent<IEmailDistributionListService>();
            }
        }

        public ActionResult ReIndex()
        {
            IndexService.ReIndex();
            return Content("Re-indexed");
        }

        public ActionResult Index()
        {
            AdminViewModel model = TempData["AdminViewModel"] as AdminViewModel;
            if (model == null) model = new AdminViewModel();
            model.UserEntity = UserEntity;
            return View(model);
        }

        public ActionResult UpdateEula()
        {
            var viewModel = new EulaViewModel();
            viewModel.Build(EulaRepository);
            return this.View(viewModel);
        }

        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult UpdateEula(EulaViewModel viewModel)
        {
            if (String.IsNullOrWhiteSpace(viewModel.Text))
            {
                ModelState.AddModelError("Text", "You must enter at least 1 character.");
            }
            if (ModelState.IsValid)
            {
                EulaRepository.InsertEula(new Eula
                    {
                        Id = Guid.NewGuid(),
                        Text = HttpUtility.HtmlDecode(viewModel.Text)
                    });
                ModelState.Remove("Text");
                viewModel.Build(EulaRepository);
            }
            return this.View(viewModel);
        }

        public ExcelResult ExportUserSearch()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Search Term" },
                DeleteFile = true,
                FileName = "UserSearchTerms.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.SearchTerms != null && m.SearchTerms.Count() > 0))
            {
                rows.AddRange(user.SearchTerms.Select(searchTerm => new Dictionary<string, string>
                                                                        {
                                                                            {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                                            {"Email", user.aspnet_Membership.LoweredEmail},
                                                                            {"Search Term", searchTerm.SearchTerm}
                                                                        }));
            }
            retval.Rows = rows;
            return retval;
        }

        public ExcelResult ExportUserInformation()
        {
            var userRepository = new UserRepository();
            var retval = new ExcelResult
            {
                Headers = new List<string> { "User Name", "Email", "Phone", "Title", "Organization Name", "Organization Type", "Referred By", "Group", "Active" },
                DeleteFile = true,
                FileName = "UserInformation.xlsx",
                FilePath = ConfigurationManager.AppSettings["ExcelExportFileLocation"]
            };
            var rows = new List<IDictionary<string, string>>();
            foreach (var user in userRepository.GetAll().Where(m => m.UserExt != null))
            {
                rows.Add(new Dictionary<string, string>{
                                                        {"User Name", String.Format("{0} {1}", user.UserExt.FirstName, user.UserExt.LastName)},
                                                        {"Email", user.aspnet_Membership.LoweredEmail},
                                                        {"Phone", user.UserExt.Phone},
                                                        {"Title",  user.UserExt.Title},
                                                        {"Organization Name",  user.UserExt.OrganizationName},
                                                        {"Organization Type",  user.UserExt.OrganizationType.HasValue && user.UserExt.OrganizationType.Value != 0 ? ((OrganizationType) user.UserExt.OrganizationType).GetDescription() : string.Empty},
                                                        {"Referred By",  user.UserExt.ReferredBy},
                                                        {"Group", user.UserExt.Group != null ? user.UserExt.Group.Name : String.Empty},
                                                        {"Active", user.aspnet_Membership != null && user.aspnet_Membership.IsApproved ? "Yes" : "No" }
                                                    });
            }
            retval.Rows = rows;
            return retval;
        }


        #region Email Distribution Lists - EDL

        public ActionResult EmailDistributionLists()
        {
            var lists = EmailDistributionListService.GetAllActiveLists().Select(x => new EmailDistributionListViewModel()
            {
                Active = x.Active,
                Emails = x.Emails,
                ID = x.ID,
                Name = x.Name
            });
            return PartialView(lists);
        }

        [HttpPost]
        public ActionResult EDLAddEmail(EmailDistributionListViewModel model)
        {
            if (ModelState.IsValid)
            {
                var results = EmailDistributionListService.AddEmail(new Common.Data.EmailDistributionListEmail { Email = model.EmailToAdd, Active = true, DistributionListID = model.ID });
                ModelState.AddModelErrorUI("EmailToAdd", results);
            }
            else
                ModelState.AddModelError("EmailToAdd", "Invalid data.");

            var distriblist = EmailDistributionListService.GetAllActiveLists().Single(x => x.ID == model.ID);
            //model.EmailToAdd = string.Empty;
            ModelState.SetModelValue("EmailToAdd",  new ValueProviderResult("", "", CultureInfo.CurrentCulture));
            model.Emails = distriblist.Emails;
            model.Name = distriblist.Name;
            return PartialView("EmailDistributionList", model);
        }


        [HttpPost]
        public ActionResult EDLRemoveEmail(int edlListID, int emailID)
        {
            if (ModelState.IsValid)
            {
                var results = EmailDistributionListService.DeleteEmail(emailID);
                ModelState.AddModelErrorUI(String.Empty, results);
            }
            else
                ModelState.AddModelError(String.Empty, "Invalid data.");

            var distriblist = EmailDistributionListService.GetAllActiveLists().Single(x => x.ID == edlListID);
            var model = new EmailDistributionListViewModel();
            model.ID = distriblist.ID;
            model.Emails = distriblist.Emails;
            model.Name = distriblist.Name;
            return PartialView("EmailDistributionList", model);
        }
        #endregion
    }
}
