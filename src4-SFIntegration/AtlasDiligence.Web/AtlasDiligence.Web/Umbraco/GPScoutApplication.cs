﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using AtlasDiligence.Web.Config;
using GPScout.Domain.Contracts.Services;
using GPScout.Domain.Implementation.Common;
using Umbraco_ = Umbraco;
using System.Web.Optimization;
using System.Web.Mvc;


namespace AtlasDiligence.Web.Umbraco
{
    public class GPScoutApplication : Umbraco_.Cms.Web.UI.MvcApplication
    {
        public override void Init()
        {
            base.Init();
            if (IsApplicationStart)
            {
                ApplicationHelper.RegisterTypes();
                ComponentBrokerInstance.RetrieveComponent<IOrganizationService>().RemoveObsoleteDataItems();
                BundleConfig.RegisterBundles(BundleTable.Bundles);

#if !DEBUG
                GlobalFilters.Filters.Add(new RequireHttpsAttribute());
#endif
            }
        }

        public bool IsApplicationStart { 
            get {
                return !ComponentBrokerInstance.HasType<ITaskRepository>();
            }
        }
    }
}