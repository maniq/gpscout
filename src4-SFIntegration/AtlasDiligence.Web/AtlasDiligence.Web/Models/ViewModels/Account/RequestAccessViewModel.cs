﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AtlanticBT.Common.Validation;


namespace AtlasDiligence.Web.Models.ViewModels.Account
{
    public class RequestAccessViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Organization { get; set; }

        [AtlanticBT.Common.Validation.Email(ErrorMessage = "Email is not in correct format.")]
        [DataType(DataType.EmailAddress)]
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}