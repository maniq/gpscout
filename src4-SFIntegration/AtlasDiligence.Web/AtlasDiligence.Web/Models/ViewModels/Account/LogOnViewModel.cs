﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Account
{
    public class LogOnViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public bool IsDuplicateLogin { get; set; }
        public bool ForceLogin { get; set; }
        public string LogonPageId { get {
                return "_Logon_1999";
            } }

        public bool TrialAccepted { get; internal set; }
        public bool ShowAgreementReminder { get; internal set; }
    }
}