﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.Rss
{
    public class RssIndexViewModel
    {
        public RssIndexViewModel()
        {
            RssSearchViewModel = new RssSearchViewModel();
            RssExportViewModel = new RssExportViewModel();
            RssScheduleEmailViewModel = new RssScheduleEmailViewModel();
        }

        public RssSearchViewModel RssSearchViewModel { get; set; }
        public RssExportViewModel RssExportViewModel { get; set; }
        public RssScheduleEmailViewModel RssScheduleEmailViewModel { get; set; }
    }
}