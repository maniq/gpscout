﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AtlasDiligence.Web.General;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Models.ViewModels.Rss
{
    public class RssScheduleEmailViewModel
    {
        public EmailFrequency? Frequency { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:MM/dd/yyyy}", HtmlEncode = true, NullDisplayText = "")]
        public DateTime? NextEmailDate { get; set; }

        public string TimeOfDay { get; set; }

        public bool IsSubscribed { get; set; }

        public IEnumerable<string> ExtraEmails { get; set; }

        public void Build(UserEntity user)
        {
            NextEmailDate = user.RssNextEmailDate;
            Frequency = user.RssEmailFrequency.HasValue
                    ? user.RssEmailFrequency.Value
                    : EmailFrequency.Day;
            TimeOfDay = user.RssNextEmailDate.HasValue
                    ? user.RssNextEmailDate.Value.ToString(
                        "hh:mm tt")
                    : string.Empty;
            ExtraEmails = user.RssEmails ?? new List<string>();
            IsSubscribed = NextEmailDate != null;
        }
    }
}