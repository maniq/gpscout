﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class MarketDataViewModel
    {
        public string TableauServerTrustedAuthKey { get; set; }
        public string ViewName { get; set; }
    }
}