﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class OrgGroupAccessVM
    {
        public Guid OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public List<Group> AllGroups { get; set; }
        public List<Guid> IncludedGroups { get; set; }

        public string Term { get; set; }
     }
}