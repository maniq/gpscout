﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Web.Controllers.Services;
using GPScout.Domain.Contracts.Models;
using GPScout.Domain.Contracts.Services;


namespace AtlasDiligence.Web.Models.ViewModels.Search
{
	public class RecentlyUpdatedViewModel : TimeStamped
    {
		public IEnumerable<Common.Data.Models.Organization> FirmsRecentlyUpdated { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool SinceLastLoginOnly { get; set; }

        public void Build(IEnumerable<LuceneSearchResult> availableOrgs, IOrganizationService organizationService)
        {
            // set properties
            FirmsRecentlyUpdated = organizationService.GetAllByIds(availableOrgs.Where(m => m.LastUpdated.HasValue && (!SinceLastLoginOnly || !LastLoginDate.HasValue || m.LastUpdated >= LastLoginDate))
                .OrderByDescending(m => m.LastUpdated).Select(m => m.Id).Take(10));
		}
	}
}