﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Web.General;
using GPScout.Domain.Contracts.Enums;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class SearchDataGrid
    {
        public SearchDataGrid(AtlasDiligence.Common.Data.Models.Organization backingOrg, string FPVHtml)
        {
            this.Id = backingOrg.Id;

            this.AccessConstrained = backingOrg.AccessConstrained;
            this.EmergingManager = backingOrg.EmergingManager;
            this.FocusRadar = backingOrg.FocusRadar;
            this.KeyTakeAway = backingOrg.KeyTakeAway;
            var name = backingOrg.PublishOverviewAndTeam ? string.Format("<a href=\"{0}\" class=\"org-name\">{1}</a>", VirtualPathUtility.ToAbsolute("~/Organization/Index/" + backingOrg.Id), backingOrg.Name)
                : "<span class=\"org-name\">" + backingOrg.Name + "</span><a href=\"/Request/RequestOrganization\" class=\"request-information simple-tooltip\" data-id=\"" + backingOrg.Id + "\" data-name=\"" + backingOrg.Name + "\" title=\"Click to Request Research\">Request Information</a>";
            var emptyDiv = "<div class=\"empty\">&nbsp;</div>";
            var rightEmptyDiv = "<div class=\"empty\" style=\"width:16px;\">&nbsp;</div>";
            //this.Name = string.Format("{0} {1}",
            //                          name,
            //                          !string.IsNullOrWhiteSpace(backingOrg.KeyTakeAway)
            //                            ? string.Format("<div class='key-takeaway simple-tooltip' title='{0}'>{0}</div>", backingOrg.KeyTakeAway)
            //                            : string.Empty);

            //var lastFundSize = (backingOrg.LastFundSize != null) ? (backingOrg.LastFundSize.Value > 0 ? backingOrg.Currency.FormattedSymbol + Math.Round(backingOrg.LastFundSize.Value, 0, MidpointRounding.AwayFromZero) : "") : "";
            //var expectedNextFundRaise = (!String.IsNullOrEmpty(backingOrg.FundRaisingStatus) ? backingOrg.FundRaisingStatus.ToLower() == "open" || backingOrg.FundRaisingStatus.ToLower() == "pre-marketing" ? "N/A" : String.Format("{0:y}", backingOrg.ExpectedNextFundRaise) : "");
            //var tooltipText = String.Format("<strong>Primary Strategy: </strong>{0}<br />"
            //    + "<strong>Sector: </strong>{1}<br />"
            //    + "<strong>Market/Stage: </strong>{2}<br />"
            //    + "<strong>Fundraising Status: </strong>{3}<br />"
            //    + "<strong>Expected Next Fundraise: </strong>{4}<br />"
            //    + "<strong>Last Fund Size (mm): </strong>{5}"
            //    + "{6}{7}"
            //    , (!String.IsNullOrEmpty(backingOrg.StrategyPipeDelimited) ? backingOrg.StrategyPipeDelimited.Replace("|", ",") : "")
            //    , (!String.IsNullOrEmpty(backingOrg.SectorFocusPipeDelimited) ? backingOrg.SectorFocusPipeDelimited.Replace("|", ",") : "")
            //    , backingOrg.MarketStage
            //    , backingOrg.FundRaisingStatus
            //    , expectedNextFundRaise
            //    , lastFundSize
            //    , (!String.IsNullOrEmpty(backingOrg.OrganizationOverview)
            //        ? "<hr class='horizontalRule' /><strong>Overview:  </strong>" + HttpContext.Current.Server.HtmlEncode(backingOrg.OrganizationOverview.NewlineToBreak())
            //        : "")
            //    , HttpContext.Current.Server.HtmlEncode(FPVHtml)
            //    );


            this.Name = string.Format("{0} {1} {2}",
                                      "<div class=\"divParent\"><div>" + name + "</div>",
                                      FPVHtml,
                                      backingOrg.IsInProcess ? "<div class=\"under-construction\" data-org-id=\"" + backingOrg.Id + "\" data-user-registered-for-PCN=\"" + (backingOrg.IsUserRegisteredForPUCN.HasValue ? backingOrg.IsUserRegisteredForPUCN.Value ? "1" : "2" : "") + "\"></div>" : String.Empty);

            var focusRadar = String.IsNullOrWhiteSpace(this.FocusRadar)
                                 ? string.Format(emptyDiv)
                                 : string.Format("<div class=\"{0} icon\" title=\"{1}\">{1}</div>",
                                                 this.FocusRadar.ToLower().Replace(' ', '-'), this.FocusRadar);

            var emergingManager = this.EmergingManager ?
                string.Format("<div class=\"emerging-manager-true icon\" title=\"Emerging Manager\">{0}</div>",
                this.EmergingManager.ToString().ToLower()) : string.Format(emptyDiv);

            var accessConstrained = this.AccessConstrained ?
                string.Format("<div class=\"access-constrained-true icon\" title=\"Access Constrained\">{0}</div>",
                this.AccessConstrained.ToString().ToLower()) : string.Format(rightEmptyDiv);

            if (backingOrg.InactiveFirm)
                this.Icons = "Inactive";
            else
                this.Icons = string.Format("{0} {1} {2}", focusRadar, emergingManager, accessConstrained);
            this.PrimaryOffice = backingOrg.PrimaryOffice;
            this.InvestmentRegion = String.Join(", ", backingOrg.InvestmentRegions.OrderBy(m => m));
            this.Strategy = string.Join(", ", backingOrg.Strategy);
            this.Category = backingOrg.Category;
            this.SubCategory = backingOrg.MarketStage;
            this.YearFounded = backingOrg.YearFounded.HasValue ? backingOrg.YearFounded.ToString() : string.Empty;
            this.FundraisingStatus = backingOrg.FundRaisingStatus;
            this.LastUpdated = backingOrg.LastUpdated.HasValue
                                   ? backingOrg.LastUpdated.Value.ToShortDateString()
                                   : string.Empty;
            this.DiligenceLevel = backingOrg.DiligenceLevel.GetValueOrDefault(0).ToString();
            this.FolderIds = backingOrg.FolderOrganizations.Aggregate("[", (seed, current) =>
            {
                seed += String.Format("'{0}',", current.FolderId);
                return seed;
            }).TrimEnd(',') + "]";
            if (backingOrg.QualitativeGradeNumber.HasValue)
            {
                this.AtlasQualitative = @"<div class=""rating"">
                                            <div style=""width: " + Convert.ToInt32(Math.Round(backingOrg.QualitativeGradeStars.Value * 20, 0)) + @"%;"">
                                                1 star
                                            </div>
                                          </div>";
            }
            if (backingOrg.QuantitativeGradeNumber.GetValueOrDefault(0) > GradeRangeLimit.Ungraded)
            {
                this.QoRPerformance = @"<div class=""rating"">
                                            <div style=""width: " + Convert.ToInt32(Math.Round(backingOrg.QuantitativeGradeStars.Value * 20, 0)) + @"%;"">
                                                1 star
                                            </div>
                                          </div>";
            }
            this.InactiveFirm = backingOrg.InactiveFirm;
        }

        public Guid Id { get; set; }

        public string Icons { get; set; }
        public String Name { get; set; }

        public bool AccessConstrained { get; set; }
        public bool EmergingManager { get; set; }
        public string FocusRadar { get; set; }

        public String PrimaryOffice { get; set; }

        public String KeyTakeAway { get; set; }

        public String InvestmentRegion { get; set; }

        public String GeographicFocus { get; set; }

        public String Category { get; set; }

        public String Strategy { get; set; }

        /// <summary>
        /// MarketStage
        /// </summary>
        public string SubCategory { get; set; }

        public string YearFounded { get; set; }

        public string FundraisingStatus { get; set; }

        public string LastUpdated { get; set; }

        public string DiligenceLevel { get; set; }

        /// <summary>
        /// Returns Guids in a json array
        /// </summary>
        public string FolderIds { get; set; }

        public string AtlasQualitative { get; set; }

        public string QoRPerformance { get; set; }
        public bool InactiveFirm { get; set; }

        private string _FPVHtml;
    }
}