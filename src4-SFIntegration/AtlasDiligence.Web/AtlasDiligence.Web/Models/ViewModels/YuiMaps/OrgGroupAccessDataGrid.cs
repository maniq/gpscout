﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class OrgGroupAccessDataGrid
    {
        public OrgGroupAccessDataGrid(Organization org)
        {
            Id = org.Id;
            Name = org.Name;

            EditLink = String.Format(
                "<a href=\"javascript:void(0)\" class=\"grid-row-action edit\" onclick=\"common.submitTo($('#frm_group_org_access'), '{0}');\">Edit</a>",
                VirtualPathUtility.ToAbsolute("~/GroupAccess/Edit/" + this.Id));
        }

        public Guid Id
        {
            get;
            set;
        }

        public string Name { get; set; }

        public string EditLink { get; set; }
    }
}