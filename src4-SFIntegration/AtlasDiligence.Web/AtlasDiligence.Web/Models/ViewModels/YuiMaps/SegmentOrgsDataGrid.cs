﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class SegmentOrgsDataGrid
    {
        public SegmentOrgsDataGrid(AtlasDiligence.Common.Data.Models.Organization backingOrg)
        {
            this.Id = backingOrg.Id;

            this.AccessConstrained = backingOrg.AccessConstrained;
            this.EmergingManager = backingOrg.EmergingManager;
            this.FocusList = backingOrg.FocusList;
            this.KeyTakeAway = backingOrg.KeyTakeAway;

            this.Name = string.Format("<a href=\"{0}\" class=\"org-name\">{1}</a> {2} {3} {4} {5}",
                                      VirtualPathUtility.ToAbsolute("~/Organization/Index/" + backingOrg.Id),
                                      backingOrg.Name,
                                      !string.IsNullOrWhiteSpace(backingOrg.KeyTakeAway)
                                        ? string.Format("<div class=\"key-takeaway simple-tooltip\" title=\"{0}\">{0}</div>", backingOrg.KeyTakeAway)
                                        : string.Empty,
                                        string.Format("<div class=\"access-constrained-{0}\" title=\"Access Constrained\">{0}</div>", this.AccessConstrained.ToString().ToLower()),
                                      string.Format("<div class=\"emerging-manager-{0}\" title=\"Emerging Manager\">{0}</div>", this.EmergingManager.ToString().ToLower()),
                                      string.Format("<div class=\"focus-list-{0}\" title=\"Focus List\">{0}</div>", this.FocusList.ToString().ToLower()));

            this.PrimaryOffice = backingOrg.PrimaryOffice;
            this.GeographicFocus = backingOrg.GeographicFocus;
            this.Strategy =
               string.Join(",", backingOrg.Entity.EntityMultipleValues.Where(x => x.Field == (int)FieldType.Strategy).Select(y => y.Value));
            this.Category = backingOrg.Category;
            this.SubCategory = backingOrg.MarketStage;
            this.YearFounded = backingOrg.YearFounded.HasValue ? backingOrg.YearFounded.ToString() : string.Empty;
            this.FundraisingStatus = backingOrg.FundRaisingStatus;
            this.LastUpdated = backingOrg.LastUpdated.HasValue
                                   ? backingOrg.LastUpdated.Value.ToShortDateString()
                                   : string.Empty;
            this.DiligenceLevel = backingOrg.DiligenceLevel.GetValueOrDefault(0).ToString();
        }

        public Guid Id { get; set; }

        public String Name { get; set; }

        public bool AccessConstrained { get; set; }
        public bool EmergingManager { get; set; }
        public bool FocusList { get; set; }

        public String PrimaryOffice { get; set; }

        public String KeyTakeAway { get; set; }

        public String GeographicFocus { get; set; }

        public String Category { get; set; }

        public String Strategy { get; set; }

        /// <summary>
        /// MarketStage
        /// </summary>
        public string SubCategory { get; set; }

        public string YearFounded { get; set; }

        public string FundraisingStatus { get; set; }

        public string LastUpdated { get; set; }

        public string DiligenceLevel { get; set; }
    }
}