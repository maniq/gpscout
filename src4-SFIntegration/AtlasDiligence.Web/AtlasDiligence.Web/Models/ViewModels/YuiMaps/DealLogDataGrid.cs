﻿using System;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels.YuiMaps
{
    public class DealLogDataGrid
    {
        public string DateReceived { get; set; }
        public string FirmName { get; set; }
        public string FundName { get; set; }
        public string PlacementAgent { get; set; }
        public string MarketStage { get; set; }
        public string Strategies { get; set; }
        public string InvestmentRegions { get; set; }
        public string Source { get; set; }
        public string Status { get; set; }

        public DealLogDataGrid(ClientRequest clientRequest, bool isPublished)
        {
            this.DateReceived = clientRequest.DateReceived.GetValueOrDefault(DateTime.Now).ToShortDateString();
            this.FundName = clientRequest.FundName;
            this.FirmName = isPublished ? string.Format("<a href=\"/Organization/Index/{0}\">{1}</a>", clientRequest.OrganizationId, clientRequest.OrganizationName) : clientRequest.OrganizationName;
            this.PlacementAgent = clientRequest.PlacementAgent;
            this.MarketStage = clientRequest.MarketStage;
            this.Strategies = clientRequest.StrategyPipeDelimited.Replace("|", ", ");
            this.InvestmentRegions = clientRequest.InvestmentRegionPipeDelimited.Replace("|", ", ");
            this.Source = clientRequest.Source;
            this.Status = string.IsNullOrWhiteSpace(clientRequest.Status) ? "In Queue" : clientRequest.Status == "Fully Complete" ? "Fully Complete" : "In Process";
        }
    }
}