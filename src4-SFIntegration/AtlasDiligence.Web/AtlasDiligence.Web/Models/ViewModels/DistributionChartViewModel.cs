using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class DistributionChartViewModel
    {
        public string HtmlId { get; set; }
        public bool IsQuality { get; set; }
        public IEnumerable<LuceneSearchResult> OtherOrganizations { get; set; }
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public double OrganizationScore { get; set; }
        public string ChartTitle { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public int? PaddingLeft { get; set; }
        public int? PaddingRight { get; set; }
        public int? PaddingTop { get; set; }
        public int? PaddingBottom { get; set; }
        public int? XAxisDistanceFromBottomOfChart { get; set; }
    }
}