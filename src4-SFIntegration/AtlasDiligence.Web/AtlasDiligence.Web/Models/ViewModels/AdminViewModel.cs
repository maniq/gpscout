﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Web.Models.ViewModels
{
    public class AdminViewModel
    {
        public UserEntity UserEntity { get; set; }
        public string DefaultTabUrl { get; set; }
    }
}