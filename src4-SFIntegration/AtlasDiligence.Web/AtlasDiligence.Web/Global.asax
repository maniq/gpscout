﻿<%@ Application Codebehind="Global.asax.cs" Inherits="AtlasDiligence.Web.Umbraco.GPScoutApplication" Language="C#" %>
<script runat="server">
    private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AtlasDiligence.Web.Umbraco.GPScoutApplication));

    //#if DEBUG
    void Session_OnStart() {
        //new AtlasDiligence.Common.DTO.Services.IndexService().ReIndex();
        AccessHelper.OnSessionStart();
    }
    //#endif

    void Application_Error(Object sender, EventArgs e)
    {
        // avoid app pool restart during wkhtmltopdf calls
        // error logging is performed by Umbraco automatically
        if (Common.IsPrint)
        {
            Server.ClearError();
            return;
        }

        try
        {
            Exception ex = Server.GetLastError();
            Log.Error("Application_Error", ex);

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Error");
            routeData.Values.Add("message", ex.Message);
            IController controller = new AtlasDiligence.Web.Controllers.ErrorController();
            var reqContext = new RequestContext(new HttpContextWrapper(Context), routeData);
            controller.Execute(reqContext);
            Server.ClearError();
        }
        catch (Exception ex) {
            ex.ToString();
        }
    }
</script>

