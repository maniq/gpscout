﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AtlasDiligence.Web.CommonCode.Extensions
{
    public static class StringExtensionMethods
    {
        public static string IfNullOrEmpty(this string s, string replacementValue)
        {
            return !string.IsNullOrEmpty(s) ? s : replacementValue;
        }
    }
}