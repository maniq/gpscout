﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace AtlasDiligence.Web.Config
{
    public class BundleConfig
    {
        public static string Screen = @"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" media=""screen"" />";
        public static string Print = @"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" media=""print"" />";

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/screen").Include(
                "~/css/screen.css"
            ));


            bundles.Add(new StyleBundle("~/css/adminscreen").Include(
                "~/css/admin-screen.css"
            ));

            bundles.Add(new StyleBundle("~/css/print").Include(
               "~/css/print.css"
            ));

            bundles.Add(new StyleBundle("~/css/base").Include(
                "~/css/yuiSkin.css",
                "~/css/custom-theme/jquery-ui-1.8.16.custom.css",
                "~/css/chosen.css",
                "~/css/jquery.qtip.min.css"
            ));

            bundles.Add(new StyleBundle("~/css/print-organization").Include(
                "~/css/print-organization.css"
            ));

            bundles.Add(new StyleBundle("~/css/jquery-ui").Include(
                "~/css/custom-theme/jquery-ui-1.8.16.custom.css"
            ));

            bundles.Add(new ScriptBundle("~/scriptbase").Include(
                "~/js/modernizr-1.7.min.js",
                "~/js/jquery.tablesorter.min.js",
                "~/js/jquery.unobtrusive-ajax.min.js",
                "~/js/jquery.validate.min.js",
                "~/js/jquery.validate.unobtrusive.min.js",
                "~/js/yui.js",
                "~/js/yui.tweaks.js",
                "~/js/jquery.yuidatatable.js",
                "~/js/jquery.placeholder.js",
                "~/js/jquery.ui.timepicker.js",
                "~/js/date.js",
                /*"~/js/jquery.tools.min.js",*/
                "~/js/jquery.hoverIntent.min.js",
                "~/js/jquery.qtip.min.js",
                "~/js/site.js",
                "~/js/jquery.multiselect.js",
                "~/js/chosen.jquery.min.js"
            ));

            bundles.Add(new ScriptBundle("~/ga").Include(
                "~/js/google-analytics.js"
            ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            //BundleTable.EnableOptimizations = true;
        }
    }
}
