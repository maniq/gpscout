﻿jQuery.fn.YUIDataTable = function (settings) {
    // most of the settings are pulled straight from the YUI Docs 
    // for the DataTable or DataSource objects.
    // Sane defaults for ASP.Net webservices have been selected, but YMMV.
    var settings = jQuery.extend({
        srcUrl: '',
        colDef: [],
        selectionMode: "single",
        rowSelectEvent: function (args) { /* args.el == html element, args.record == YAHOO.widget.Record */ },
        selectFirstRow: false,
        responseSchema: {
            resultList: "TypeName",
            fields: ["Id", "Title"],
            metaNode: "GridListOfTypeName",
            metaFields: ["Total"]
        },
        handleDataReturnPayload: function (req, response, payload) {
            if (payload == null) {
                payload = {};
            }
            payload.totalRecords = response.meta.total;
            return payload;
        },
        rowClickEvent: undefined, /*something like 'function(e){ row = e.target;}'*/
        checkboxClickEvent: undefined, /* function(oArgs) { checkbox el = oArgs.target } */
        cellClickEvent: undefined, /*something like 'function(e){ row = e.target;}'*/
        postRenderEvent: undefined,
        doBeforeLoadData: function (sRequest, oResponse, oPayload) { return true; },
        requestParams: {},
        searchFormId: '',
        draggableColumns: true,
        dynamicData: true,
        initialRequest: '',
        width: '100%',
        responseType: YAHOO.util.DataSource.TYPE_JSON,
        connMethodPost: true
    }, settings);

    //create a datasource to be used by the table.
    var dataSource = new YAHOO.util.DataSource(settings.srcUrl, settings);
    var defaultRequest = function (oState, oSelf) {
        // Get states or use defaults
        oState = oState || { pagination: null, sortedBy: null };
        var sort = (oState.sortedBy) ? oState.sortedBy.key : '';
        var dir = (oState.sortedBy && oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC) ? "false" : "true";
        var startIndex = (oState.pagination) ? oState.pagination.recordOffset : 0;
        var rowsPerPage = (oState.pagination) ? oState.pagination.rowsPerPage : 0;
        //these will always be included for the controller calls.
        return "sortColumn=" + sort + "&sortAscending=" + dir + "&pageStart=" + startIndex + "&rowsPerPage=" + rowsPerPage;
    };



    var genReq = function (state, self) {
        var defaultResult = defaultRequest(state, self);
        var customRequest = self.get('data');
        var customParams = "";
        var searchParams = "";
        if (defaultResult === undefined) {
            defaultResult = "intentionally=leftBlank";
        }
        if (customRequest != undefined) {
            for (prop in customRequest) {
                customParams += "&" + prop + "=" + customRequest[prop];
            }
        }
        //params from a custom search form
        if (settings.searchFormId != "" && settings.searchFormId != undefined)
            searchParams = "&" + $('#' + settings.searchFormId).serialize();

        return defaultResult + customParams + searchParams;
    }
    //configure the initial load.
    for (prop in settings.requestParams) {
        settings.initialRequest += "&" + prop + "=" + settings.requestParams[prop];
    }
    settings = jQuery.extend({ generateRequest: genReq }, settings);
    //set formatRow to select first row of grid when data is loaded
    if (settings.selectFirstRow) {
        settings.formatRow = function (elTr, oRecord) {
            if (this.getRecordIndex(oRecord) == 0) {
                this.selectRow(oRecord);
            }
            return true;
        };
    }

    var dataTable = new YAHOO.widget.DataTable(this.attr('id'), settings.colDef, dataSource, settings);
    dataTable.set('selectionMode', settings.selectionMode);
    dataTable.setAttributeConfig('data', {}, true);
    dataTable.set('data', settings.requestParams);
    dataTable.handleDataReturnPayload = settings.handleDataReturnPayload;
    dataTable.doBeforeLoadData = settings.doBeforeLoadData;
    dataTable.subscribe("postRenderEvent", settings.postRenderEvent);
    dataTable.subscribe("checkboxClickEvent", function (e) {
        if (!e.target.checked) {
            var thHead = this.getColumn(e.target).getThEl();
            $('input', thHead).attr('checked', false);
        }
    });
    dataTable.subscribe("checkboxClickEvent", settings.checkboxClickEvent);
    dataTable.subscribe("theadLabelClickEvent", function (e) {
        var column = this.getColumn(e.target);
        if (column.formatter == 'checkbox') {
            var checkState = $('input', $(e.target)).attr('checked');
            var records = this.getRecordSet().getRecords();
            for (var i in records) {
                if ($('input', this.getTrEl(records[i])).attr('checked') != checkState) {
                    records[i].setData(column.key, checkState);
                    $('input', this.getTrEl(records[i])).click();
                }
            }
        }
    });

    //cell click events
    dataTable.subscribe("cellClickEvent", settings.cellClickEvent);

    if (dataTable.get('selectionMode') != 'none') {
        dataTable.subscribe("rowMouseoverEvent", dataTable.onEventHighlightRow);
        dataTable.subscribe("rowMouseoutEvent", dataTable.onEventUnhighlightRow);
        dataTable.subscribe("rowSelectEvent", settings.rowSelectEvent);
        if (settings.rowClickEvent === undefined) {
            dataTable.subscribe("rowClickEvent", function (e) {
                if (!dataTable.isSelected(e.target)) {
                    if (dataTable.get('selectionMode') === 'single') {
                        dataTable.unselectAllRows();
                    }
                    dataTable.selectRow(e.target);
                }
                else {
                    if (dataTable.get('selectionMode') === 'standard') {
                        dataTable.unselectRow(e.target);
                    }
                }
                dataTable.clearTextSelection();
            });
        }
        else {
            dataTable.subscribe('rowClickEvent', settings.rowClickEvent);
        }
    }

    //declare the special attribute.
    dataTable.setAttributeConfig('requestParams', {}, true);
    //set the special attribute, based on the
    dataTable.set('requestParams', settings.requestParams);
    dataTable.render();
    //save the dataTable object into the jQuery element for future reference.
    jQuery(this).data('dataTable', dataTable);

    //when this page is reloaded, we need to kill the previous inhabitants!
    jQuery(this).addClass('destroy');
    jQuery(this).bind('destroyEvent', function () {
        dataTable.destroy();
        dataTable = null;
        jQuery(this).data('dataTable', null);
    });
    return jQuery(this);
};
