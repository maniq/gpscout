﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Infrastructure.Email.Contracts
{
    public interface IEmail
    {
        void EmailNotification(string subject, string body);
        IList<Attachment> CreateAttachments(IEnumerable<string> attachmentFileNames);
        void Send(string subject, string body, string form, string to, IEnumerable<Attachment> attachments);
    }
}
