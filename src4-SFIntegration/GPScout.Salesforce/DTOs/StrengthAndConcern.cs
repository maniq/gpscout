﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class StrengthAndConcern
    {
        public string Id { get; set; }
        public decimal? Display_Order__c { get; set; }
        public bool Publish__c { get; set; }
        public string Account__c { get; set; }
        public string Type__c { get; set; }
        public string Name__c { get; set; }
        public string External_Text__c { get; set; }
        public string Internal_Notes__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return Account__r != null ? Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return Account__r != null ? Account__r.Publish_Level__c : "";
            }
        }
        public ParentAccount Account__r { get; set; }
        //public DateTime DateImportedUtc { get; set; }

    }
}
