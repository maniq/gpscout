﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Account
    {
        public string Id { get; set; }
        public string Website { get; set; }
        public string Most_Recent_Fund_Industry_Focus__c { get; set; }
        public string YearGPFounded__c { get; set; }
        public decimal? Most_Recent_Fund_Eff_Size__c { get; set; }
        public string Alias__c { get; set; }
        public decimal? AUM_Calc__c { get; set; }
        public bool? Emerging_Manager__c { get; set; }
        public string Co_Investment_Opportunities_for_Non_LPs__c { get; set; }
        //public string Radar_List__c { get; set; }
        public string Overview__c { get; set; }
        public string Investment_Thesis__c { get; set; }
        public string Team_Overview__c { get; set; }
        public string Key_Takeaway__c { get; set; }
        public string Grade_Qualitative_Text__c { get; set; }
        public string Grade_Quantitative_Text__c { get; set; }
        public string Grade_Scatterchart_Text__c { get; set; }
        public string Track_Record_Text__c { get; set; }
        public string Evaluation_Text__c { get; set; }
        public decimal? Total_Closed_Funds__c { get; set; }
        public string Most_Recent_Fund_Market_Stage__c { get; set; }
        public string Region__c { get; set; }
        public string Industry_Focus__c { get; set; }
        public DateTime? Expected_Next_Fundraise__c { get; set; }
        public string Co_Investment_Opportunities_for_Fund_LP__c { get; set; }
        public string Firm_History__c { get; set; }
        public string Most_Recent_Fund_Region__c { get; set; }
        public string Most_Recent_Fund_Regional_Specialist__c { get; set; }
        public string Most_Recent_Fund_Primary_Strategy__c { get; set; }
        public string Publish_Level__c { get; set; }
        public string Most_Recent_Fund_Secondary_Strategy__c { get; set; }
        //public DateTime? Last_Modified_Date__c { get; set; }
        public DateTime? GPScout_Last_Updated__c { get; set; }
        public bool? Access_Constrained_Firm__c { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public string BillingCountry { get; set; }
        public string Most_Recent_Fund_SBIC__c { get; set; }
        public string Most_Recent_Fund_is_Sector_Specialist__c { get; set; }
        public string Most_Recent_Fund_Sub_Region__c { get; set; }
        public bool IsDeleted { get; set; }
        public string Most_Recent_Fund_Currency__c { get; set; }
        public string Most_Recent_Fund_Fundraising_Status__c { get; set; }
        public string MostRecentScorecardSFID
        {
            get
            {
                if (GPScout_Qualitative_Scorecards__r != null && ((dynamic)GPScout_Qualitative_Scorecards__r).records.Count > 0)
                    return ((dynamic)GPScout_Qualitative_Scorecards__r).records[0].Id;

                return null;
            }
        }
        public string MostRecentFundCountries
        {
            get
            {
                if (AIM__Funds__r != null && ((dynamic)AIM__Funds__r).records.Count > 0)
                    return ((dynamic)AIM__Funds__r).records[0].Country__c;

                return null;
            }
        }

        public string GPScout_Phase_Assigned__c { get; set; }
        public string GPScout_Phase_Completed__c { get; set; }

        //public string RecordTypeName
        //{
        //    get
        //    {
        //        return RecordType != null ? ((dynamic)RecordType).Name : (string)null;
        //    }
        //}

        public object GPScout_Qualitative_Scorecards__r { get; set; }
        public object AIM__Funds__r { get; set; }
        //public object RecordType { get; set; }

        public string FirmGrade__c { get; set; }

        //// properties not existing in Salesforce should appear at the end of the DTO class b/c of bulkcopy
        //// property-position based field mapping logic
        // public DateTime DateImportedUtc { get; set; }
        // public string NextRecordsUrl { get; set; }
        //public string nextRecordsUrl { get; set; }
    }
}
