﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPScout.Salesforce.DTOs
{
    public class FirmAddress
    {
        public string Id { get; set; }
        public string Account__c { get; set; }
        public string Street_Address__c { get; set; }
        public string City__c { get; set; }
        public string State__c { get; set; }
        public string Country__c { get; set; }
        public object Firm_Location__c { get; set; }
        public string Address_Description__c { get; set; }
        public string Phone__c { get; set; }
        public string Fax__c { get; set; }
        public string Zip_Code__c { get; set; }
        public string Address_2__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return Account__r != null ? Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return Account__r != null ? Account__r.Publish_Level__c : "";
            }
        }
        public decimal? Firm_Location_Lat
        {
            get
            {
                if (Firm_Location__c != null)
                    return ((dynamic)Firm_Location__c).latitude;
                
                return null;
            }
        }
        public decimal? Firm_Location_Long
        {
            get
            {
                if (Firm_Location__c != null)
                    return ((dynamic)Firm_Location__c).longitude;

                return null;
            }
        }
        public ParentAccount Account__r { get; set; }
        //public DateTime DateImportedUtc { get; set; }

    }
}
