﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class FundMetric
    {
        public string Id { get; set; }
        public DateTime? AIMPortMgmt__Effective_Date__c { get; set; }
        public decimal? Preqin_Called__c { get; set; }
        public decimal? Preqin_DPI__c { get; set; }
        public decimal? Preqin_TVPI__c { get; set; }
        public decimal? Preqin_Net_IRR__c { get; set; }
        public decimal? Preqin_Fund_Size__c { get; set; }
        public string AIMPortMgmt__Account__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool Publish_Metric__c { get; set; }
        public string ParentAccount_Id
        {
            get
            {
                return AIMPortMgmt__Account__r != null ? AIMPortMgmt__Account__r.Id : null;
            }
        }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return AIMPortMgmt__Account__r != null ? AIMPortMgmt__Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return AIMPortMgmt__Account__r != null ? AIMPortMgmt__Account__r.Publish_Level__c : "";
            }
        }
        public decimal? Invested_Capital__c { get; set; }
        public decimal? Realized_Value__c { get; set; }
        public decimal? Unrealized_Value__c { get; set; }
        public decimal? Total_Value__c { get; set; }
        public decimal? Fund_Total_IRR__c { get; set; }
        public decimal? Fund_Total_Cash_on_Cash__c { get; set; }
        public decimal? DPI__c { get; set; }
        public decimal? Fund_Net_IRR__c { get; set; }
        public decimal? Fund_Net_Cash_on_Cash__c { get; set; }
        public string AIMPortMgmt__Fund__c { get; set; }

        public bool ParentFund_IsDeleted
        {
            get
            {
                return AIMPortMgmt__Fund__r != null ? AIMPortMgmt__Fund__r.IsDeleted : false;
            }
        }
        public bool ParentFund_Publish__c
        {
            get
            {
                return AIMPortMgmt__Fund__r != null ? AIMPortMgmt__Fund__r.Publish__c : false;
            }
        }
        public string Public_Metric_Source__c { get; set; }


        public ParentAccount AIMPortMgmt__Account__r { get; set; }
        public ParentFund AIMPortMgmt__Fund__r { get; set; }
        //public DateTime DateImportedUtc { get; set; }

    }
}
