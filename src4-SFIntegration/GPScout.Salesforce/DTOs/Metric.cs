﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Metric
    {
        public string Id { get; set; }
        public decimal? Invested_Capital__c { get; set; }
        public decimal? Realized_Value__c { get; set; }
        public decimal? Unrealized_Value__c { get; set; }
        public decimal? Total_Value__c { get; set; }
        public decimal? Fund_Total_IRR__c { get; set; }
        public decimal? Fund_Total_Cash_on_Cash__c { get; set; }
        public decimal? DPI__c { get; set; }
        public decimal? Fund_Net_IRR__c { get; set; }
        public decimal? Fund_Net_Cash_on_Cash__c { get; set; }
        public DateTime? AIMPortMgmt__Effective_Date__c { get; set; }
        public string AIMPortMgmt__Account__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool Publish_Metric__c { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return AIMPortMgmt__Account__r != null ? AIMPortMgmt__Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return AIMPortMgmt__Account__r != null ? AIMPortMgmt__Account__r.Publish_Level__c : "";
            }
        }
        public ParentAccount AIMPortMgmt__Account__r { get; set; }
        //public DateTime DateImportedUtc { get; set; }

    }
}
