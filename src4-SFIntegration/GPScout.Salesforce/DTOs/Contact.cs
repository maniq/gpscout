﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Contact
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public decimal? Year_Started_With_Current_Firm__c { get; set; }
        public string Bio__c { get; set; }
        public string MailingStreet  { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingPostalCode { get; set; }
        public string MailingCountry { get; set; }
        public bool IsDeleted { get; set; }
        public bool Publish_Contact__c { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return Account != null ? Account.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return Account != null ? Account.Publish_Level__c : String.Empty;
            }
        }
        public string FirstName {get; set; }
        public string LastName {get; set; }
        public decimal? Display_Order__c { get; set; }

        public ParentAccount Account { get; set; }
    }
}
