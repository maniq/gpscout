﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Fund
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AIM__Account__c { get; set; }
        public string Currency__c { get; set; }
        public string Fundraising_Status__c { get; set; }
        public DateTime? First_Drawn_Capital__c { get; set; }
        public decimal? Fund_Total_Cash_on_Cash__c { get; set; }
        public decimal? Fund_Total_IRR__c { get; set; }
        public string Total_Deal_Professionals__c { get; set; }
        public decimal? FundTargetSize__c { get; set; }
        public decimal? FundNumber__c { get; set; }
        public DateTime? FundLaunchDate__c { get; set; }
        public string Vintage_Year__c { get; set; }
        public decimal? Eff_Size__c { get; set; }
        public string Deal_Table_URL__c { get; set; }
        public string SBIC__c { get; set; }
        public string Preqin_Fund_Name__c { get; set; }
        public DateTime? Effective_Date__c { get; set; }
        public bool IsDeleted { get; set; }
        public bool Publish__c { get; set; }
        public bool ParentAccount_IsDeleted
        {
            get
            {
                return AIM__Account__r != null ? AIM__Account__r.IsDeleted : false;
            }
        }
        public string ParentAccount_Publish_Level__c
        {
            get
            {
                return AIM__Account__r != null ? AIM__Account__r.Publish_Level__c : "";
            }
        }

        public string MostRecentMetricSFID
        {
            get
            {
                if (AIMPortMgmt__Investment_Metrics__r != null && ((dynamic)AIMPortMgmt__Investment_Metrics__r).records.Count > 0)
                    return ((dynamic)AIMPortMgmt__Investment_Metrics__r).records[0].Id;

                return null;
            }
        }

        public object AIMPortMgmt__Investment_Metrics__r { get; set; }
        public ParentAccount AIM__Account__r { get; set; }
        public DateTime? DateImportedUtc { get; set; }
    
    }
}
