﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class ID
    {
        public Guid UID { get; set; }
        public string GPScout_Scorecard__c { get; set; }
        public string AIMPortMgmt__Fund__c { get; set; }

        public string MostRecentRCPFundMetricId
        {
            get
            {
                if (this.FieldSelectionHint == FieldSelectionHint.None && AIMPortMgmt__Investment_Metrics__r != null && ((dynamic)AIMPortMgmt__Investment_Metrics__r).records.Count > 0)
                    return ((dynamic)AIMPortMgmt__Investment_Metrics__r).records[0].Id;

                return null;
            }
        }


        public string MostRecentPreqinFundMetricId
        {
            get
            {
                if (this.FieldSelectionHint == FieldSelectionHint.MostRecentPreqinFundMetricId && AIMPortMgmt__Investment_Metrics__r != null && ((dynamic)AIMPortMgmt__Investment_Metrics__r).records.Count > 0)
                    return ((dynamic)AIMPortMgmt__Investment_Metrics__r).records[0].Id;

                return null;
            }
        }


        public object AIMPortMgmt__Investment_Metrics__r { get; set; }

        public ID() {
            UID = Guid.NewGuid();
        }

        public FieldSelectionHint FieldSelectionHint { get; set; }
    }


    public enum FieldSelectionHint : byte
    {
        None,
        MostRecentPreqinFundMetricId
    }
}
