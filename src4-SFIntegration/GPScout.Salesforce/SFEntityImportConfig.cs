﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce
{
    internal class SFEntityImportConfig
    {
        public int ID { get; set; }
        public string EntityCode { get; set; }
        public string EntityCodeInSource { get; set; }
        public string EntityFriendlyName { get; set; }
        public string SourceUri { get; set; }
        public string SelectQuery { get; set; }
        public string FirstSelectQuery { get; set; }
        public int SelectPageSize { get; set; }
        public string TargetDTO { get; set; }
        public bool Enabled { get; set; }
        public int OrderBy { get; set; }
    }
}
