﻿using GPScout.DataImport.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFP.Extensions;
using GPScout.Domain.Contracts.Models;
using Microsoft.Samples.EntityDataReader;

namespace GPScout.Salesforce.Data
{
    public class Repository : IRepository, IDisposable
    {
        public string ConnectionString { get; set; }


        private static SqlBulkCopy _bulkCopy;
        protected static SqlBulkCopy bulkCopy
        {
            get
            {
                if (_bulkCopy == null)
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["DataStaging_ConnectionString"].ConnectionString;
                    _bulkCopy = new SqlBulkCopy(connectionString);
                    _bulkCopy.BulkCopyTimeout = 300;
                }
                return _bulkCopy;
            }
        }


        public Repository()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DataStaging_ConnectionString"].ConnectionString;
        }

        public IEnumerable<T> Select<T>() where T : class
        {
            if (!typeof(T).IsAssignableFrom(typeof(SFEntityImportConfig)))
                throw new NotImplementedException();

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("SF_GetEntityImportConfig", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    yield return new SFEntityImportConfig
                    {
                        Enabled = dr.Get<bool>("Enabled"),
                        EntityCode = dr.Get<string>("EntityCode"),
                        EntityCodeInSource = dr.Get<string>("EntityCodeInSource"),
                        EntityFriendlyName = dr.Get<string>("EntityFriendlyName"),
                        ID = dr.Get<int>("ID"),
                        SelectPageSize = dr.Get<int>("SelectPageSize"),
                        SelectQuery = dr.Get<string>("SelectQuery"),
                        SourceUri = dr.Get<string>("SourceUri"),
                        TargetDTO = dr.Get<string>("TargetDTO"),
                        OrderBy = dr.Get<int>("OrderBy"),
                        FirstSelectQuery = dr.Get<string>("FirstSelectQuery"),
                    } as T;
                }

            }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void TruncateStagingTables()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("SF_TruncateStagingtables", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public T Delete<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public T Add<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public DataSyncResult BulkInsert<T>(IEnumerable<T> data, string targetTableName)
        {
            var results = new DataSyncResult();
            if (data != null)
            {
                using (var a = new SqlBulkCopy("")) { 
                bulkCopy.DestinationTableName = targetTableName;
                bulkCopy.BatchSize = data.Count();
                var dr = data.AsDataReader();
                bulkCopy.WriteToServer(dr);
                results.Inserted = bulkCopy.BatchSize;
                    }
            }
            return results;
        }


        public void PostImport(DateTime DateImportedUtc)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("SF_PostImport", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("DateImportedUtc", DateImportedUtc));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void Dispose()
        {
            if (_bulkCopy != null)
            {
                _bulkCopy.Close();
            }
        }


        public IEnumerable<string> GetAdditionalAccountIdsClause()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var cmd = new SqlCommand("SF_GetAdditionalAccountIdsClause", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                var dr = cmd.ExecuteReader();
                try
                {
                    while (dr.Read()) { yield return Convert.ToString(dr["Id"]); }
                }
                finally { dr.Close(); }
                //return cmd.ExecuteScalar().ToString();
            }
        }


        public void SetFundMetricImportStep(string mode, byte step)
        {
            using (var dbContext = new GPScoutSFImportStagingEntities())
            {
                dbContext.SF_ImportHelper(mode, step);
            }
        }


        public IEnumerable<Guid?> GetOrgsInProcessBeforeDataImport()
        {
            using (var dbContext = new GPScoutSFImportStagingEntities())
            {
                return dbContext.Ids.Where(x => x.OrgInProcessBeforeImport.HasValue).Select(x => x.OrgInProcessBeforeImport).ToArray();
            }
        }
    }
}
