﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.Contracts
{
    public interface ISalesforceInstanceInfo
    {
        string InstanceUrl { get; }
        string ApiVersion { get; set; }
        string TokenRequestUrl { get; }
    }
}
