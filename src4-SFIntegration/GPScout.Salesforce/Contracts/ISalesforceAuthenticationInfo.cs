﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.Contracts
{
    public interface ISalesforceAuthenticationInfo
    {
        string ConsumerKey { get; set; }
        string ConsumerSecret { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
    }
}
