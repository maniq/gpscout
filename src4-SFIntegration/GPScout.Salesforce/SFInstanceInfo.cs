﻿using GPScout.Salesforce.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce
{
    public class SFInstanceInfo: ISalesforceInstanceInfo
    {
        public string InstanceUrl { get; set; }
        public string ApiVersion { get; set; }
        public string TokenRequestUrl { get; set; }

        public SFInstanceInfo()
        {
            InstanceUrl = ConfigurationManager.AppSettings["SFInstanceUrl"];
            ApiVersion = ConfigurationManager.AppSettings["SFApiVersion"];
            TokenRequestUrl = ConfigurationManager.AppSettings["SFTokenRequestUrl"];
        }
    }
}
