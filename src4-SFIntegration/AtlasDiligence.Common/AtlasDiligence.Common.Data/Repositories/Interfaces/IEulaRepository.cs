﻿using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public interface IEulaRepository : IRepository<Eula>
    {
        void InsertEula(Eula eula);
        Eula GetCurrentEula();
        bool HasUserSignedCurrentEula(string username);
        void UserSignedEula(string username);
    }
}