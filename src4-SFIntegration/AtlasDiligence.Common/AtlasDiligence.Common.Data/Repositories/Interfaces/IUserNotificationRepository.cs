﻿using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Repositories.Interfaces
{
    public interface IUserNotificationRepository : IRepository<UserNotification>
    {
        bool Save(UserNotification notification);
    }
}
