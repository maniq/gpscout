﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.General;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class RequestRepository : Repository<OrganizationRequest>, IRequestRepository
    {
        public void InsertRequest(RequestType type, Guid organizationId, Guid userId, string comment)
        {
            var organization = DataContext.Organizations.SingleOrDefault(m => m.Id == organizationId);
            if (organization == null) throw new ArgumentException("OrganizationId is invalid.  This organization does not exist.");
            var request = new OrganizationRequest
            {
                Id = Guid.NewGuid(),
                Comment = comment,
                UserId = userId,
                OrganizationId = organizationId,
                OrganizationName = organization.Name,
                CreatedOn = DateTime.Now,
                IsActive = true,
                Type = (int)type
            };
            DataContext.OrganizationRequests.InsertOnSubmit(request);
            DataContext.SubmitChanges();
        }

        public void InsertRequest(RequestType type, string organizationName, Guid userId, string comment)
        {
            var request = new OrganizationRequest
            {
                Id = Guid.NewGuid(),
                Comment = comment,
                UserId = userId,
                OrganizationName = organizationName,
                CreatedOn = DateTime.Now,
                IsActive = true,
                Type = (int)type
            };
            DataContext.OrganizationRequests.InsertOnSubmit(request);
            DataContext.SubmitChanges();
        }

        public void Edit(OrganizationRequest request)
        {
            if (request == null) return;
            var thisRequest = this.DataContext.OrganizationRequests.SingleOrDefault(m => m.Id == request.Id);
            if (thisRequest == null) return;
            thisRequest.IsActive = request.IsActive;
            thisRequest.Comment = request.Comment;
            thisRequest.CreatedOn = request.CreatedOn;
            thisRequest.OrganizationId = request.OrganizationId;
            thisRequest.OrganizationName = request.OrganizationName;
            thisRequest.UserId = request.UserId;
            thisRequest.Type = request.Type;
            DataContext.SubmitChanges();
        }
    }
}
