﻿using AtlasDiligence.Common.Data.General;
using AtlasDiligence.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class EFRepository<T> : IDisposable, IRepository<T> where T : class
    {
        protected MainDbEntities DataContext { get; private set; }


        public EFRepository()
        {
            DataContext = DataContext ?? new MainDbEntities();
        }

        #region IRepository

        public void Attatch(T oldEntity, T newEntity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAllOnSubmit(Expression<Func<T, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public void DeleteAllOnSubmit(IQueryable<T> entities)
        {
            throw new NotImplementedException();
        }

        public void DeleteOnSubmit(T entity)
        {
            DataContext.Set<T>().Remove(entity);
        }

        public IQueryable<T> FindAll(Expression<Func<T, bool>> expression)
        {
            return DataContext.Set<T>().Where(expression);
        }

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public void InsertAllOnSubmit(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        public void InsertOnSubmit(T entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Paginate(Expression<Func<T, bool>> expression, string sortCol, bool sortAsc, int startResult, int maxResults)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Paginate(IQueryable<T> retval, string sortCol, bool sortAsc, int startResult, int maxResults)
        {
            throw new NotImplementedException();
        }

        public void SubmitChanges()
        {
            //SaveAudits();
            DataContext.SaveChanges();
        }
        #endregion

        #region Audit Private Methods
        //private void SaveAudits()
        //{
        //    DataContext.ChangeTracker.Entries().Where(e => e.State == System.Data.Entity.EntityState.Modified);
        //    foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Updates.OfType<IAuditable>())
        //    {
        //        MapAudit(modifiedEntity, AuditType.Update);
        //    }

        //    foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Inserts.OfType<IAuditable>())
        //    {
        //        MapAudit(modifiedEntity, AuditType.Insert);
        //    }

        //    foreach (IAuditable modifiedEntity in DataContext.GetChangeSet().Deletes.OfType<IAuditable>())
        //    {
        //        MapAudit(modifiedEntity, AuditType.Delete);
        //    }
        //}

        //private void MapAudit(IAuditable modifiedEntity, AuditType auditType)
        //{
        //    Type type = modifiedEntity.GetType();
        //    string tableName = type.Name;
        //    var entityId = type.GetProperty(GetPrimaryKey()).GetValue(modifiedEntity, null);

        //    Models.Audit audit = new Models.Audit();
        //    audit.Id = Guid.NewGuid();
        //    audit.Modified = modifiedEntity.Modified;
        //    audit.ModifiedBy = modifiedEntity.ModifiedBy;
        //    audit.ModifiedTable = tableName;
        //    audit.ModifiedEntity = entityId is Guid ? (Guid)entityId : Guid.Empty;
        //    audit.AuditType = (int)auditType;

        //    DataContext.Set<Models.Audit>().InsertOnSubmit(audit);
        //}
        #endregion

        #region IDisposable
        public void Dispose()
        {
            DataContext.Dispose();
        }
        #endregion
    }
}
