﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class NoteRepository : Repository<OrganizationNote>, INoteRepository
    {
        public IEnumerable<OrganizationNote> GetByUserIdAndOrganizationId(Guid userId, Guid organizationId)
        {
            var groupId = DataContext.UserExts.FirstOrDefault(m => m.UserId == userId).GroupId;
            var notes = DataContext.OrganizationNotes.Where(n => (n.UserId == userId) && n.OrganizationId == organizationId).ToList();
            if(groupId != null)
            {
                notes.Union(
                    DataContext.OrganizationNotes.Where(m => m.GroupId == groupId && m.OrganizationId == organizationId));
            }
            return notes.OrderBy(n => n.DateEntered);
        }

        public void AddNote(OrganizationNote note)
        {
            DataContext.OrganizationNotes.InsertOnSubmit(note);
            DataContext.SubmitChanges();
        }

        public IQueryable<OrganizationNote> GetAllUserNotes(Guid userId)
        {
            return DataContext.OrganizationNotes.Where(m => m.UserId == userId);
        }
    }
}
