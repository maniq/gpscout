﻿using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class UserNotificationRepository : Repository<UserNotification>, IUserNotificationRepository
    {
        public bool Save(UserNotification notification)
        {
            UserNotification oldEntity = base.DataContext.UserNotifications.Where(x => x.Id == notification.Id).FirstOrDefault();
            if (oldEntity != null)
            {
                base.DeleteOnSubmit(oldEntity);
            }

            base.InsertOnSubmit(notification);
            base.SubmitChanges();
            return true;
        }
    }
}
