﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using AtlasDiligence.Common.Data.Models;

namespace AtlasDiligence.Common.Data.Repositories
{
    public class SegmentRepository : Repository<Segment>, ISegmentRepository
    {
        #region ISegmentRepository Members

        public IEnumerable<string> GetDistinctOptionsByName(General.FieldType type)
        {
            var result = from so in this.DataContext.SearchOptions
                         where so.Field == (int)type
                         select so.Value;
            return result.Distinct();
        }

        public IEnumerable<SearchOption> GetDistinctOptions()
        {
            var result = from so in this.DataContext.SearchOptions
                         select so;
            return result.Distinct();
        }

        public void Insert<T>(T item) where T : class
        {
            DataContext.GetTable<T>().InsertOnSubmit(item);
            DataContext.SubmitChanges();
        }

        public void InsertAll<T>(IEnumerable<T> items) where T : class
        {
            DataContext.GetTable<T>().InsertAllOnSubmit(items);
            DataContext.SubmitChanges();
        }

        public IEnumerable<Segment> GetSegmentByGroupId(Guid groupId)
        {
            return DataContext.Segments.Where(x => x.GroupSegments.Any(y => y.GroupId == groupId));
        }

        public Segment GetSegmentById(Guid id)
        {
            return DataContext.Segments.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Segment> GetSegmentsStartsWith(string term)
        {
            return DataContext.Segments.Where(x => x.Name.StartsWith(term));
        }

        #endregion
    }
}
