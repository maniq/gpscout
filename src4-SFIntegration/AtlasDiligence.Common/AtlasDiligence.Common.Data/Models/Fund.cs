﻿// -----------------------------------------------------------------------
// <copyright file="Fund.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace AtlasDiligence.Common.Data.Models
{
    using General;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public partial class Fund
    {
        public IEnumerable<string> SectorFocus
        {
            get { return SectorFocusPipeDelimited.Split(new[] {'|'}); }
        }


        private bool? _isPreqinMostRecentMetric;
        public bool IsPreqinMostRecentMetric
        {
            get
            {
                if (!_isPreqinMostRecentMetric.HasValue)
                    _isPreqinMostRecentMetric = this.Source == FundMetricSource.Preqin;

                return _isPreqinMostRecentMetric.GetValueOrDefault();
            }
        }


        public decimal? SourceAwareDpi
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinDpi : this.Dpi;
            }
        }


        public decimal? SourceAwareTvpi
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinTvpi : this.Tvpi;
            }
        }


        public decimal? SourceAwareIrr
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinIrr : this.Irr;
            }
        }


        public DateTime? SourceAwareAsOf
        {
            get
            {
                return IsPreqinMostRecentMetric ? this.PreqinAsOf : this.AsOf;
            }
        }

        public bool? HasPreqinMetric { get; set; }


        public string DisplaySource
        {
            get
            {
                return this.Source == "RCP" ? "GP" : this.Source;
            }
        }
    }
}
