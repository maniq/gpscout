﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class OtherAddress
    {
        /// <summary>
        /// Return City, State Country
        /// </summary>
        public string CityStateCountry
        {
            get
            {
                return
                    City + (!string.IsNullOrWhiteSpace(StateProvince) ? ", " + StateProvince : string.Empty) + ", " + Country;
            }
        }
        public string FullAddress
        {
            get
            {
                return
                    string.Join(", ", new[]
                                          {
                                              Address1, Address2, Address3, City, StateProvince, Country
                                          }.Where(m => !String.IsNullOrWhiteSpace(m)));
            }
        }
    }
}
