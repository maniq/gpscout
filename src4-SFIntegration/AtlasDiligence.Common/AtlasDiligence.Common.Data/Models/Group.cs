﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace AtlasDiligence.Common.Data.Models
{
    partial class Group
    {
        public enum ProfilePdfQuotaActionEnum : byte
        {
            [Description("Notify RCP when quota is achieved")]
            NotifyRCPOnQuotaAchieved = 1,

            [Description("Block further PDF downloads and notify RCP")]
            BlockDownloadsAndNotifyRCP = 2
        }

        public string OrganizationProfilePdfGeneration
        {
            get
            {
                if (!OrganizationProfilePdfAllowed)
                    return "No";

                if (OrganizationProfilePdfQuota.HasValue && OrganizationProfilePdfQuotaDays.HasValue && GetOrganizationFullPdfGeneratedCount() >= OrganizationProfilePdfQuota)
                    return "Quota Achieved";

                return "Yes";
            }
        }

        public ProfilePdfQuotaActionEnum OrganizationProfilePdfQuotaActionID
        {
            get
            {
                return (ProfilePdfQuotaActionEnum)__OrganizationProfilePdfQuotaActionID;
            }
            set
            {
                __OrganizationProfilePdfQuotaActionID = (byte)value;
            }
        }

        public bool AllowOrganizationProfilePdfGenerationAfterQuotaAchieved
        {
            get
            {
                return OrganizationProfilePdfQuotaActionID == ProfilePdfQuotaActionEnum.NotifyRCPOnQuotaAchieved;
            }
        }



        public int GetOrganizationFullPdfGeneratedCount()
        {
            return GetOrganizationIdsOfFullPdfGenerations().Count();
        }


        public IEnumerable<Guid> GetOrganizationIdsOfFullPdfGenerations()
        {
            if (OrganizationProfilePdfQuotaDays.HasValue)
            {
                DateTime limitDate = DateTime.Now.AddDays(-OrganizationProfilePdfQuotaDays.Value);

                return UserExts.SelectMany(u => u.TrackUsers).Where(t => t.Type == (int)TrackUsersType.ProfileFullPdfReport && t.Date > limitDate)
                    .SelectMany(t => t.TrackUsersProfileViews).Select(x => x.OrganizationId).Distinct();
            }
            else
                return new Guid[] { };
        }

    }
}
