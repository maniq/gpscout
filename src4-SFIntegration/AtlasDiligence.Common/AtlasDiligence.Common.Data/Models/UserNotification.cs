﻿using AtlasDiligence.Common.Data.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models
{
    public partial class UserNotification
    {
        public UserNotificationType NotificationType
        {
            get
            {
                return (UserNotificationType)this.NotificationTypeId;
            }
            set
            {
                this.NotificationTypeId = (byte)value;
            }
        }
    }
}
