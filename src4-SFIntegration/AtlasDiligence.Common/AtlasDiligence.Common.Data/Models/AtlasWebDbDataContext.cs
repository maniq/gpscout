﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.Data.Models 
{
    public partial class AtlasWebDbDataContext
    {
        public int DeleteUser(Guid userId)
        {
            return this.ExecuteCommand("EXEC dbo.udp_DeleteUser {0}", userId);
        }

        partial void OnCreated()
        {
            base.CommandTimeout = AppSettings.DbCommandTimeoutSeconds;
        }
    }
}
