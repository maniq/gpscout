﻿// -----------------------------------------------------------------------
// <copyright file="Extensions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace AtlasDiligence.Common.Data.General
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Extensions
    {
        public static bool IsNullOrWhiteSpace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }


        public static string AsQuartileString(this decimal? value)
        {
            if (value.HasValue) return value.Value.ToString("N2")+"x";
            return "N/A";
        }

        public static string AsQuartileNetIRRString(this decimal? value)
        {
            if (value.HasValue) return value.Value.ToString("N1") + "%";
            return "N/A";
        }

        public static string AsDateOrNA(this DateTime? dt)
        {
            if (dt.HasValue && dt.Value != DateTime.MinValue) return dt.Value.ToShortDateString();
            return "N/A";
        }
    }
}
