﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AtlanticBT.Common.Types;

namespace AtlasDiligence.Common.Data.General
{
	/// <summary>
	/// For database audit table
	/// </summary>
	public enum AuditType
	{
		Insert,
		Update,
		Delete
	}

	public enum NewsEntityType
	{
		Organization = 1,
		Contact
	}

	/// <summary>
	/// Used in RssEmailFrequency
	/// </summary>
	public enum EmailFrequency
	{
		[Description("day")]
		Day = 1,
		[Description("week")]
		Week = 2,
		[Description("month")]
		Month = 3
	}

	/// <summary>
	/// 
	/// </summary>
	public enum EntityType
	{
		Organization = 1,
		Fund
	}

	public enum RequestType
	{
        [Description("Queue")]
		Queue = 0,

        [Description("Priority")]
        Priority = 1,

        [Description("Research")]
        Research = 2,

        [Description("Introduction")]
        Introduction = 3,

        [Description("Restricted GP Access")]
        Access = 4,

        [Description("Profile Update Completed Notification")]
        ProfileUpdateCompletedNotification = 5
    }

	public enum Grades
	{
		Poor = 1,
		Weak = 2,
		Standard = 3,
		Strong = 4,
		Exceptional = 5
	}

	/// <summary>
	/// Field Type maps to field name. For referencing fields in db lookup table
	/// </summary>
	public enum FieldType
	{
		[Description("Sector Focus")]
		SectorFocus = 1,
		Strategy = 2,
		[Description("Geographic Focus")]
		GeographicFocus = 3,
		[Description("Market/Stage")]
		MarketStage = 4,
		[Description("Fundraising Status")]
		FundraisingStatus = 5,
		[Description("Fund Size Minimum")]
		FundSizeMin = 6,
		[Description("Fund Size Maximum")]
		FundSizeMax = 7,
		[Description("Number of Funds Closed Minimum")]
		NumberOfFundsClosedMin = 8,
		[Description("Number of Funds Closed Maximum")]
		NumberOfFundsClosedMax = 9,
		[Description("Diligence Status Minimum")]
		DiligenceStatusMin = 10,
		[Description("Diligence Status Maximum")]
		DiligenceStatusMax = 11,
		[Description("Emerging Manager")]
		EmergingManager = 12,
		[Description("Focus List")]
		FocusList = 13,
		[Description("Access Constrained")]
		AccessConstrained = 14,
		Search = 15,
		[Description("Investment Region")]
		InvestmentRegion = 16,
		//[Description("Investment Sub-Region")]
		//SubRegion = 17,
		//Country = 18,
		[Description("Expected Next Fundraise Start Date")]
		ExpectedNextFundraiseStartDate = 19,
		[Description("Expected Next Fundraise End Date")]
		ExpectedNextFundraiseEndDate = 20,
		[Description("Limit to Firms with Profiles")]
		ShowOnlyPublished = 21,
		[Description("Sub-Strategy")]
		SubStrategy = 22,
		//[Description("Investment Sub-Region")]
		//InvestmentSubRegion = 23,
		[Description("Quantitative Grade")]
		QuantitativeGrade = 24,
		[Description("Qualitative Grade")]
		QualitativeGrade = 25,
		[Description("Sector Specialist")]
		SectorSpecialist = 26,
		[Description("Currency")]
		Currency = 27,
		[Description("SBIC Fund")]
		SBICFund = 28,
		[Description("Expected Next Fundraise")]
		SelectedNextFundraiseValue = 29,
        [Description("Firms Not Farther")]
        ProximitySearchMiles = 30,
        [Description("From")]
        ProximitySearchAddress = 31,
        ProximitySearchLat = 32,
        ProximitySearchLng = 33,
        [Description("Regional Specialist")]
        RegionalSpecialist = 34,
        [Description("Include Inactive Firms")]
        ShowInactiveFirms = 35
    }

	public enum TrackUsersType
	{
		[Description("Logins")]
		Login,

		[Description("Logouts")]
		Logout,

		[Description("Profile Views")]
		ProfileVisit,

        [Description("Profile Full Report PDFs")]
        ProfileFullPdfReport,

        [Description("Forced Logons")]
        ForcedLogon
    }

    public enum UserNotificationType : byte
    {
        GenerateOrganizationFullReportPdf = 1
    }

    public enum DownloadableItemLifeCycle : int
    {
        Unknown = 0,
        Started = 1,
        Running = 2,
        GenerationCompleted = 3,
        Downloaded = 4,
        Error = 5
    }

    public enum TaskType : int
    {
        OrganizationFullReportTask = 1
    }

    public struct FundMetricSource
    {
        public const string Unpublished = "";
        public const string RCP = "RCP";
        public const string Preqin = "Preqin";
    }

    public enum EmailDistribution : byte
    {
        ProfileViewReport = 1,
        RequestNotification = 2,
        OrgFullReportPdfUserGroupQuotaAchieved = 3
    }
}
