﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AtlasDiligence.Common.DTO.Model;
using Microsoft.Xrm.Sdk;

namespace AtlasDiligence.Common.DTO.Tests
{
    [TestClass]
    public class CrmEntityMapperTests
    {
        #region web user -> crm
        [TestMethod]
        public void CrmEntityFromWebUserTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var groupId = Guid.NewGuid();
            var crmEntity = new new_webuser()
            {
                Id = id,
                new_firstname = "foo",
                new_lastname = "bar",
                new_email = "foo@bar.com",
                new_validated = true,
                new_InitialPassword = "bar",
                new_rssRole = true,
                new_DiligenceApp = false
            };

            //Act
            var webUser = CrmEntityMapper.WebUserFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNotNull(webUser);
            Assert.IsInstanceOfType(webUser, typeof(WebUser));
            Assert.AreEqual("foo", webUser.FirstName);
            Assert.AreEqual("bar", webUser.LastName);
            Assert.AreEqual(true, webUser.IsValidated);
            Assert.AreEqual("bar", webUser.InitialPassword);
            Assert.IsTrue(webUser.RssApp);
            Assert.IsFalse(webUser.DiligenceApp);
        }

        [TestMethod]
        public void CrmEntityFromWebUserNullGroupIdTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var crmEntity = new new_webuser()
            {
                Id = id,
                new_firstname = "foo",
                new_lastname = "bar",
                new_groupid = null
            };

            //Act
            var webUser = CrmEntityMapper.WebUserFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNull(webUser.GroupId);
            Assert.IsNull(webUser.Group);
            Assert.AreEqual(webUser.GroupName, string.Empty);
        }

        [TestMethod]
        public void CrmEntityFromWebUserNullCrmEntityTest()
        {
            //Assign
            new_webuser crmEntity = null;

            //Act
            var webUser = CrmEntityMapper.WebUserFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNull(webUser);
        }
        #endregion
        #region crm -> web user
        [TestMethod]
        public void WebUserFromCrmEntityTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var groupId = Guid.NewGuid();
            var webUser = new WebUser
            {
                Id = id,
                Email = "foo@bar.com",
                FirstName = "foo",
                GroupId = groupId,
                GroupName = "foo",
                IsValidated = true,
                LastName = "bar",
                InitialPassword = "foo",
                RssApp = false,
                DiligenceApp = true
            };

            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromWebUser(webUser);

            //Assert
            Assert.IsNotNull(webUser);
            Assert.IsInstanceOfType(crmEntity, typeof(new_webuser));
            Assert.AreEqual("foo@bar.com", crmEntity.new_email);
            Assert.AreEqual("foo", crmEntity.new_firstname);
            Assert.IsNotNull(crmEntity.new_groupid);
            Assert.AreEqual("foo", crmEntity.new_groupid.Name);
            Assert.AreEqual(true, crmEntity.new_validated);
            Assert.AreEqual(false, crmEntity.new_rssRole);
            Assert.AreEqual(true, crmEntity.new_DiligenceApp);
        }

        [TestMethod]
        public void WebUserFromCrmEntityNullGroupIdTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var webUser = new WebUser
            {
                Id = id,
                Email = "foo@bar.com",
                FirstName = "foo",
                GroupId = null,
                GroupName = "foo",
                IsValidated = true,
                LastName = "bar",
                InitialPassword = "foo",
            };

            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromWebUser(webUser);

            //Assert
            Assert.IsNull(crmEntity.new_groupid);
        }

        [TestMethod]
        public void WebUserFromCrmEntityNullCrmEntityTest()
        {
            //Assign
            WebUser nullUser = null;

            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromWebUser(nullUser);

            //Assert
            Assert.IsNull(crmEntity);
        }

        #endregion

        #region news item -> crm
        //check normal 
        [TestMethod]
        public void CrmEntityFromNewsItemTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var orgSourceId = Guid.NewGuid();
            var date = DateTime.Now;
            var newsItem = new NewsItem
                               {
                                   Id = id,
                                   Content = "foo",
                                   ContactIds = new List<Guid>(),
                                   OrganizationIds = new List<Guid>(),
                                   OrganizationSourceId = orgSourceId,
                                   PublishedDate = date,
                                   Subject = "bar",
                                   Url = "www.foo.bar"
                               };
            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromNewsItem(newsItem);

            //Assert
            Assert.IsNotNull(crmEntity);
            Assert.IsInstanceOfType(crmEntity, typeof (new_newsitem));
            Assert.AreEqual(id, crmEntity.Id);
            Assert.AreEqual("foo", crmEntity.new_Description);
            Assert.AreEqual("bar", crmEntity.new_Subject);
            Assert.IsTrue(crmEntity.new_PublishedDate.HasValue);
            Assert.AreEqual(date, crmEntity.new_PublishedDate.Value);
            Assert.IsNotNull(crmEntity.new_organizationId);
            Assert.AreEqual(orgSourceId, crmEntity.new_organizationId.Id);
        }
        //check + some nulls
        [TestMethod]
        public void CrmEntityFromNewsItemNullSourceIdTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var newsItem = new NewsItem
            {
                Id = id,
                OrganizationSourceId = null,
            };
            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromNewsItem(newsItem);

            //Assert
            Assert.IsNull(crmEntity.new_organizationId);
        }
        //check null 
        [TestMethod]
        public void CrmEntityFromNewsItemNullNewsItemTest()
        {
            //Assign
            NewsItem newsItem = null;
            //Act
            var crmEntity = CrmEntityMapper.CrmEntityFromNewsItem(newsItem);

            //Assert
            Assert.IsNull(crmEntity);
        }
        #endregion
        #region crm -> news item
        //check normal 
        [TestMethod]
        public void NewsItemFromCrmEntityTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var orgSourceId = Guid.NewGuid();
            var date = DateTime.Now;
            var crmEntity = new new_newsitem
            {
                Id = id,
                new_Description = "foo",
                new_Subject = "bar",
                new_organizationId = new EntityReference(Account.EntityLogicalName, orgSourceId),
                new_PublishedDate = date,
                new_URL = "http://www.foo.bar"
            };

            //Act
            var newsItem = CrmEntityMapper.NewsItemFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNotNull(newsItem);
            Assert.IsInstanceOfType(newsItem, typeof(NewsItem));
            Assert.AreEqual(id, newsItem.Id);
            Assert.AreEqual("foo", newsItem.Content);
            Assert.AreEqual("bar", newsItem.Subject);
            Assert.AreEqual(date, newsItem.PublishedDate);
            Assert.IsTrue(newsItem.OrganizationSourceId.HasValue);
            Assert.AreEqual(orgSourceId, newsItem.OrganizationSourceId.Value);
            Assert.IsNotNull(newsItem.OrganizationIds);
            Assert.IsTrue(newsItem.OrganizationIds.Count == 0);
            Assert.IsNotNull(newsItem.ContactIds);
            Assert.IsTrue(newsItem.ContactIds.Count == 0);
        }
        //check + some nulls
        [TestMethod]
        public void NewsItemFromCrmEntityNullSourceIdTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var crmEntity = new new_newsitem
            {
                Id = id,
                new_organizationId = null
            };
            //Act
            var newsItem = CrmEntityMapper.NewsItemFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNull(newsItem.OrganizationSourceId);
        }
        //check null 
        [TestMethod]
        public void NewsItemFromCrmEntityNullCrmEntityTest()
        {
            //Assign
            new_newsitem crmEntity = null;
            //Act
            var newsItem = CrmEntityMapper.NewsItemFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNull(newsItem);
        }
        #endregion

        #region crm -> group
        //check normal 
        [TestMethod]
        public void GroupFromCrmEntityTest()
        {
            //Assign
            var id = Guid.NewGuid();
            var groupLeaderId = Guid.NewGuid();
            var crmEntity = new new_group
                                {
                                    Id = id,
                                    new_name = "foo",
                                    new_maxusers = 5,
                                    new_webuserid = new EntityReference(new_webuser.EntityLogicalName, groupLeaderId)
                                };
            crmEntity.new_webuserid.Name = "bar";

            //Act
            var group = CrmEntityMapper.GroupFromCrmEntity(crmEntity);

            //Assert
            Assert.IsNotNull(group);
            Assert.IsInstanceOfType(group, typeof (Group));
            Assert.AreEqual(id, group.Id);
            Assert.AreEqual("foo", group.Name);
            Assert.AreEqual("bar", group.GroupLeaderWebUserName);
            Assert.AreEqual(groupLeaderId, group.GroupLeaderWebUserId);
            Assert.AreEqual(5, group.MaxUsers);
        }
        //check + some nulls
        //check null
        [TestMethod]
        public void GroupFromCrmEntityNullGroupTest()
        {
            //Assign
            //Act
            //Assert
        }
        #endregion

    }
}
