﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Services
{
    public static class ConnectionExtensionMethods
    {
        public static bool IsInRole(this Connection connection, string roleName)
        {
            if (connection == null) return false;
            return (connection.Record1RoleId != null && connection.Record1RoleId.Name == roleName);
        }
    }
}
