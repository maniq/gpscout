﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Model;

namespace AtlasDiligence.Common.DTO.Services
{
    public interface IFolderService
    {
        IList<Folder> GetByWebUserId(Guid id);
    }
}
