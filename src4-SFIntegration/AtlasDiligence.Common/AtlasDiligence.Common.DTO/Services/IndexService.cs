﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.IO;
using System.Linq;
using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.DTO.Services.Interfaces;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using AtlasDiligence.Common.DTO.Model;
using Organization = AtlasDiligence.Common.Data.Models.Organization;

namespace AtlasDiligence.Common.DTO.Services
{
	public class IndexService : IIndexService
	{
		private static readonly object Lock = new object();

		private Repository<Data.Models.Organization> OrganizationRepository { get; set; }

		public static string IndexPath
		{
			get
			{
				return ConfigurationManager.AppSettings["IndexLocation"];
			}
		}

		public void ReIndex()
		{
			OrganizationRepository = new Repository<Data.Models.Organization>();
			var allOrganizations = OrganizationRepository.FindAll(m => m.PublishOverviewAndTeam || m.PublishSearch);

			lock (Lock)
			{
				using (var indexWriter = this.GetIndexWriter(true))
				{
					foreach (var organization in allOrganizations)
					{
						UpdateLuceneIndex(organization, indexWriter);
					}
					indexWriter.Optimize();
				}
			}
		}

		private void UpdateLuceneIndex(Data.Models.Organization organization, IndexWriter indexWriter)
		{
			AddOrganizationToIndex(organization, indexWriter);

			AddFundsToIndex(organization, indexWriter);

			AddMembersToIndex(organization, indexWriter);
		}

		private void AddOrganizationToIndex(Data.Models.Organization organization, IndexWriter indexWriter)
		{
			var orgDocument = CreateDefaultDocumentFields(organization);

			// set is organization
			orgDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), true.ToString(), Field.Store.NO, Field.Index.ANALYZED));

			// universial search string
			var organizationUniversalSearchString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7}",
					organization.Name,
					organization.PrimaryOffice,
					organization.InvestmentRegionPipeDelimited == null ? string.Empty : organization.InvestmentRegionPipeDelimited.Replace("|", " | "),
					organization.CountriesPipeDelimited == null ? string.Empty : organization.CountriesPipeDelimited.Replace("|", " | "),
					organization.MarketStage,
					organization.StrategyPipeDelimited == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | "),
					organization.AliasesPipeDelimited == null ? string.Empty : organization.AliasesPipeDelimited.Replace("|", " | "),
					organization.OtherAddresses.Any() ? String.Join(" ", organization.OtherAddresses.Select(m => m.FullAddress)) : string.Empty
				);
			orgDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), organizationUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));

			// fund size value used in advanced search
			orgDocument.Add(
				new Field(
					SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
					organization.AdvancedSearchFundSize.HasValue ? organization.AdvancedSearchFundSize.ToString().PadLeft(10, '0') : "NULL",
					Field.Store.YES,
					Field.Index.ANALYZED));

			// update document
			var term = new Term("id", organization.Id.ToString());
			indexWriter.UpdateDocument(term, orgDocument);
		}

		private void AddFundsToIndex(Data.Models.Organization organization, IndexWriter indexWriter)
		{
			foreach (var fund in organization.Funds)
			{
				var fundDocument = CreateDefaultDocumentFields(organization);
				var fundUniversalSearchString = fund.Name;

				fundDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), fundUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundName.GetFieldName(), fund.Name, Field.Store.YES, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundGeographicFocus.GetFieldName(), fund.GeographicFocus, Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundSectorFocus.GetFieldName(), fund.SectorFocusPipeDelimited == null ? string.Empty : fund.SectorFocusPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundCurrency.GetFieldName(), fund.Currency == null ? "NULL" : fund.Currency.Name, Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), false.ToString(), Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundStatus.GetFieldName(), fund.Status, Field.Store.NO, Field.Index.ANALYZED));
				fundDocument.Add(new Field(SearchField.FundTargetSize.GetFieldName(), fund.TargetSize.HasValue ? fund.TargetSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.NO, Field.Index.ANALYZED));

				// fund size value used in advanced search
				if (organization.FundRaisingStatus == "Pre-Marketing" || organization.FundRaisingStatus == "Open")
				{
					if (fund.Status == "Pre-Marketing" || fund.Status == "Open")
					{
						fundDocument.Add(new Field(SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
												   fund.TargetSize.HasValue ? fund.TargetSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.YES,
												   Field.Index.ANALYZED));
					}
					else
					{
						var mostRecentFund = organization.Funds.OrderByDescending(o => o.VintageYear);
						if (mostRecentFund.Any())
						{
							var fundSize = mostRecentFund.FirstOrDefault().FundSize;
							fundDocument.Add(new Field(SearchField.OrganizationAdvancedSearchFundSize.GetFieldName(),
												   fundSize.HasValue ? fundSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.YES,
												   Field.Index.ANALYZED));
						}
					}
				}

				var term = new Term("id", fund.Id.ToString());
				indexWriter.UpdateDocument(term, fundDocument);
			}
		}

		private void AddMembersToIndex(Data.Models.Organization organization, IndexWriter indexWriter)
		{
			foreach (var member in organization.OrganizationMembers)
			{
				var orgMemberDocument = CreateDefaultDocumentFields(organization);
				var memberUniversalSearchString = member.FullName;

				orgMemberDocument.Add(new Field(SearchField.UniversalSearch.GetFieldName(), memberUniversalSearchString, Field.Store.NO, Field.Index.ANALYZED));
				orgMemberDocument.Add(new Field(SearchField.IsOrganization.GetFieldName(), false.ToString(), Field.Store.NO, Field.Index.ANALYZED));

				var term = new Term("id", member.Id.ToString());
				indexWriter.UpdateDocument(term, orgMemberDocument);
			}
		}

		private Document CreateDefaultDocumentFields(Data.Models.Organization organization)
		{
			var document = new Document();

			// not analyzed columns for sorting
			document.Add(new Field(SearchField.OrganizationName.GetFieldName() + "Sort", organization.Name.ToLower(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationPrimaryOffice.GetFieldName() + "Sort", organization.PrimaryOffice.ToLower(), Field.Store.NO, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationGeographicFocus.GetFieldName() + "Sort", organization.GeographicFocus.ToLower(), Field.Store.NO, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationStrategies.GetFieldName() + "Sort", organization.Strategy == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | ").ToLower(), Field.Store.NO, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationMarketStage.GetFieldName() + "Sort", organization.MarketStage.ToLower(), Field.Store.NO, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationFundraisingStatus.GetFieldName() + "Sort", organization.FundRaisingStatus.ToLower(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationYearFounded.GetFieldName() + "Sort", organization.YearFounded.HasValue ? organization.YearFounded.Value.ToString() : "0000", Field.Store.NO, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationLastUpdated.GetFieldName() + "Sort", organization.LastUpdated.HasValue ? DateTools.DateToString(organization.LastUpdated.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.NO, Field.Index.NOT_ANALYZED));

			// add rest of the org fields
			document.Add(new Field(SearchField.OrganizationId.GetFieldName(), organization.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationPublishProfile.GetFieldName(), organization.PublishOverviewAndTeam.ToString(), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationPublishSearch.GetFieldName(), organization.PublishSearch.ToString(), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationName.GetFieldName(), organization.Name, Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationAliases.GetFieldName(), organization.AliasesPipeDelimited == null ? string.Empty : organization.AliasesPipeDelimited.Replace("|", " | "), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationPrimaryOffice.GetFieldName(), organization.PrimaryOffice, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationInvestmentRegion.GetFieldName(), organization.InvestmentRegionPipeDelimited == null ? string.Empty : organization.InvestmentRegionPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationMarketStage.GetFieldName(), organization.MarketStage, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationCountries.GetFieldName(), organization.CountriesPipeDelimited == null ? string.Empty : organization.CountriesPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationStrategies.GetFieldName(), organization.StrategyPipeDelimited == null ? string.Empty : organization.StrategyPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationGeographicFocus.GetFieldName(), organization.GeographicFocus, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationInvestmentSubRegion.GetFieldName(), organization.SubRegionsPipeDelimited == null ? string.Empty : organization.SubRegionsPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationSubStrategy.GetFieldName(), organization.SubStrategyPipeDelimited == null ? string.Empty : organization.SubStrategyPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationQualitativeGrades.GetFieldName(), organization.QualitativeGrade, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationQuantitativeGrades.GetFieldName(), organization.QuantitativeGrade, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationSectorFocus.GetFieldName(), organization.SectorFocusPipeDelimited == null ? string.Empty : organization.SectorFocusPipeDelimited.Replace("|", " | "), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationFundraisingStatus.GetFieldName(), organization.FundRaisingStatus, Field.Store.YES, Field.Index.ANALYZED));

			document.Add(new Field(SearchField.OrganizationYearFounded.GetFieldName(), organization.YearFounded.HasValue ? organization.YearFounded.Value.ToString() : "0000", Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationLastUpdated.GetFieldName(), organization.LastUpdated.HasValue ? DateTools.DateToString(organization.LastUpdated.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationExpectedNextFundraise.GetFieldName(), organization.ExpectedNextFundRaise.HasValue ? DateTools.DateToString(organization.ExpectedNextFundRaise.Value, DateTools.Resolution.DAY) : DateTools.DateToString(new DateTime(1990, 1, 1), DateTools.Resolution.DAY), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationNumberOfFunds.GetFieldName(), organization.NumberOfFunds.HasValue ? organization.NumberOfFunds.ToString().PadLeft(10, '0') : "0".PadLeft(10, '0'), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationEmergingManager.GetFieldName(), organization.EmergingManager.ToString(), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationFocusList.GetFieldName(), organization.FocusRadar.ToString(), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationSectorSpecialist.GetFieldName(), organization.SectorSpecialist.ToString(), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationAccessConstrained.GetFieldName(), organization.AccessConstrained.ToString(), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationDiligenceLevel.GetFieldName(), organization.DiligenceLevel.HasValue ? organization.DiligenceLevel.Value.ToString().PadLeft(10, '0') : "0".PadLeft(10, '0'), Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationQuality.GetFieldName(), organization.QualitativeGradeNumber.HasValue ? organization.QualitativeGradeNumber.Value.ToString("N2").PadLeft(10) : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationQuantity.GetFieldName(), organization.QuantitativeGradeNumber.HasValue ? organization.QuantitativeGradeNumber.Value.ToString("N2").PadLeft(10) : string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationSBICFund.GetFieldName(), organization.SBICFund.ToString(), Field.Store.YES, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.FocusRadar.GetFieldName(), organization.FocusRadar, Field.Store.YES, Field.Index.NOT_ANALYZED));
			document.Add(new Field(SearchField.OrganizationCurrency.GetFieldName(), organization.Currency == null ? String.Empty : organization.Currency.Name, Field.Store.NO, Field.Index.ANALYZED));
			document.Add(new Field(SearchField.OrganizationLastFundSize.GetFieldName(), organization.LastFundSize.HasValue ? organization.LastFundSize.Value.ToString().PadLeft(10, '0') : "NULL", Field.Store.NO, Field.Index.ANALYZED));

			// lat and long postions
			var positions = organization.OtherAddresses.Where(m => m.Latitude.HasValue && m.Longitude.HasValue).Select(m => new LatitudeAndLongitude { Latitude = m.Latitude, Longitude = m.Longitude, City = m.City }).ToList();
			if (organization.Latitude.HasValue && organization.Longitude.HasValue)
			{
				positions.Add(new LatitudeAndLongitude { Latitude = organization.Latitude, Longitude = organization.Longitude, City = organization.Address1City });
			}
			document.Add(new Field(SearchField.OrganizationPosition.GetFieldName(), string.Join("|", positions.Select(m => string.Format("{0},{1},{2}", m.Latitude, m.Longitude, m.City))), Field.Store.YES, Field.Index.NOT_ANALYZED));

			return document;
		}

		private IndexWriter GetIndexWriter(bool create)
		{
			var directory = FSDirectory.Open(IndexPath);
			return new IndexWriter(directory, new StandardAnalyzer(Version.LUCENE_30), create, IndexWriter.MaxFieldLength.UNLIMITED);
		}
	}
}
