﻿using AtlanticBT.Common.ComponentBroker;
using AtlasDiligence.Common.Data.Models;
using AtlasDiligence.Common.Data.Repositories.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;
using Utility;

namespace AtlasDiligence.Common.DTO.Services
{
    public class OrganizationHelperService
    {
        public static string GetFullReportFileName(string instanceId)
        {
            return string.Format("org_printall_{0}", instanceId);
        }


        public static string GetFullReportFileNameFullPath(string instanceId)
        {
            string fileName = GetFullReportFileName(instanceId);
            return Path.Combine(AppSettings.TempPath, fileName + ".pdf");
        }


        public static string GetFullReportHeaderFileName(string instanceId)
        {
            return string.Format("org_printall_{0}_h.html", instanceId);
        }


        public static string GetFullReportDownloadFileName(string organizationName)
        {
            return string.Format("GPScout - {0} ({1}).pdf", Utils.SafeFileName(organizationName), DateTime.Now.ToString("M.d.yyyy"));
        }

        public static void RemoveObsoleteDataItems()
        {
#if DEBUG
            DateTime limitDate = DateTime.Now.AddDays(-AppSettings.CleanupIntervalDays);
#else
            DateTime limitDate = DateTime.Now.AddDays(-AppSettings.CleanupIntervalDays);
#endif
            var userNotificationRepository = ComponentBrokerInstance.RetrieveComponent<IUserNotificationRepository>();
            userNotificationRepository.DeleteAllOnSubmit(
                userNotificationRepository.FindAll(x => x.DateCreated < limitDate)
            );
            userNotificationRepository.SubmitChanges();

            var task = new Task<int>(() => 
            {
                var taskRepository = ComponentBrokerInstance.RetrieveComponent<ITaskRepository>();
                taskRepository.DeleteAllOnSubmit(
                    taskRepository.FindAll(x => x.DateCreated < limitDate)
                );
                taskRepository.SubmitChanges();

                // delete old organization report files
                foreach (string fileName in Directory.GetFiles(AppSettings.TempPath, "org_printall_*.*"))
                {
                    DateTime creationTime = File.GetCreationTime(fileName);
                    if (creationTime < limitDate)
                    {
                        Utils.DeleteFile(fileName);
                    }
                }
                return 0;
            });
            task.Start();
        }

        public static string GetFullReportFooterFileName(string instanceId)
        {
            return string.Format("org_printall_{0}_f.html", instanceId);
        }

        public static void FullReportCleanup(string contextId)
        {
            if (!string.IsNullOrEmpty(contextId))
            {
                string pattern = string.Format("org_printall_{0}*.*", contextId);
                foreach (string fileName in Directory.GetFiles(AppSettings.TempPath, pattern))
                {
                    Utils.DeleteFile(fileName);
                }
            }
        }
    }
}
