﻿// -----------------------------------------------------------------------
// <copyright file="LuceneSearchResult.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace AtlasDiligence.Common.DTO.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class LuceneSearchResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double? Quality { get; set; }
        public double? Quantity { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string FundraisingStatus { get; set; }
        public DateTime? ExpectedNextFundraise { get; set; }
        public bool IsPublished { get; set; }
        public IEnumerable<LatitudeAndLongitude> Positions { get; set; }
        public string FocusRadar { get; set; }
    }
}
