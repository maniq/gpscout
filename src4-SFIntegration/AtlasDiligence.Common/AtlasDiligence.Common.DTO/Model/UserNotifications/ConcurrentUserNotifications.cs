﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace AtlasDiligence.Common.DTO.Model.UserNotifications
{
    public class ConcurrentUserNotifications : ConcurrentDictionaryGuidKey<IUserNotification>
    {
        public bool Hidden { get; set; }

        public ConcurrentUserNotifications()
        {}

        public ConcurrentUserNotifications(IEnumerable<IUserNotification> notifications)
            :base(notifications, KeyExtractor)
        { }

        public bool TryRemove(Guid notificationId)
        {
            IUserNotification _n;
            return base.TryRemove(notificationId, out _n);
        }

        public IUserNotification AddOrUpdate(IUserNotification notification)
        {
            if (notification != null)
            {
                return base.AddOrUpdate(notification.Id, notification, (key, oldvalue) => { return notification; });
            }
            return null;
        }

        private static Guid KeyExtractor(IUserNotification notification)
        {
            return notification != null ? notification.Id : Guid.Empty;
        }
    }
}
