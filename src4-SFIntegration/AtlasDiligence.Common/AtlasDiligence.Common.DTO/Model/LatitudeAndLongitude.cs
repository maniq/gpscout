﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AtlasDiligence.Common.DTO.Model
{
    public class LatitudeAndLongitude
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string City { get; set; }
    }
}
