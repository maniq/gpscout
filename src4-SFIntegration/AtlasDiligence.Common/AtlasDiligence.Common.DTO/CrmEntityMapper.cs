﻿using System;
using System.Collections.Generic;
using AtlasDiligence.Common.DTO.Enums;
using AtlasDiligence.Common.DTO.General;
using AtlasDiligence.Common.DTO.Model;
using System.Linq;
using AtlasDiligence.Common.Data.Models;
using Microsoft.Xrm.Sdk;
using AtlanticBT.Common.Types;
using Group = AtlasDiligence.Common.DTO.Model.Group;

namespace AtlasDiligence.Common.DTO
{
    public static class CrmEntityMapper
    {
        #region extensions
        public static Guid? GetIdForEntityLogicalName(this EntityReference entityReference, string matchedLogicalName)
        {
            return entityReference != null
               ? (entityReference.LogicalName == matchedLogicalName ? new Guid?(entityReference.Id) : null)
               : null;
        }

        public static string GetName(this EntityReference entityReference)
        {
            if (entityReference != null)
            {
                return entityReference.Name;
            }
            return String.Empty;
        }

        public static decimal? GetDecimalValue(this Money money)
        {
            return money != null ? money.Value : new decimal?();
        }

        public static string GetOptionSetValue(this OptionSetValue value, string optionSetName)
        {
            return value != null ? EntityConfig.GetOptionSetValue(optionSetName, value.Value) : String.Empty;
        }

        /// <summary>
        /// Replace values first, then split on given strings.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="splitParams"></param>
        /// <param name="replaceParams"></param>
        /// <returns></returns>
        private static IEnumerable<string> ParseToArray(this string value, Dictionary<string, string> replaceParams, string[] splitParams)
        {
            if (value == null)
                return Enumerable.Empty<string>();

            //replace
            value = replaceParams.Aggregate(value, (current, replaceParam) => current.Replace(replaceParam.Key, replaceParam.Value));

            //split
            var retval = value.Split(splitParams, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim());

            return retval;
        }

        /// <summary>
        /// Specific string parser for Atlas custom multi-selects that are in the CRM.
        /// These are multi-checkbox lists that are saved as a single field, semi-colon delimited,
        /// with a series of dashes to separate
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static IEnumerable<string> ParseCrmSelectString(this string value)
        {
            if (value != null)
            {
                var split = value.Split(';');
                return split.Where(s => !value.Contains("--") && !String.IsNullOrWhiteSpace(s));
            }
            return new List<string>();
        }

        #endregion

        #region recent priority organization

        public static ResearchPriorityOrganization RecentPriorityOrganizationFromCrmEntity(Account account)
        {
            if (account == null) return null;

            var result = new ResearchPriorityOrganization();
            result.Id = account.Id;
            result.Name = account.Name;
            result.MarketStage = account.new_OrgSubTypeId.GetName();
            result.StrategyPipeDelimited = String.Join("|", account.new_Strategypick_value.ParseCrmSelectString());
            result.InvestmentRegionPipeDelimited = String.Join("|", account.new_RegionPick_Value.ParseCrmSelectString());
            result.Status = account.new_ProfileStatus.GetOptionSetValue("ProfileStatus");
            return result;
        }

        public static IEnumerable<ResearchPriorityOrganization> RecentPriorityOrganizationsFromCrmEntities(IEnumerable<Account> accounts)
        {
            return accounts.Select(RecentPriorityOrganizationFromCrmEntity);
        }

        #endregion

        #region organization
        public static AtlasDiligence.Common.Data.Models.Organization OrganizationFromCrmEntity(Account account)
        {
            if (account == null) return null;
            
            var result = new AtlasDiligence.Common.Data.Models.Organization();
            result.Id = account.Id;
            result.AdditionalFirmResources = account.new_AdditionalFirmResources;
            result.AdditionalFirmResourcesDoc = account.new_FirmResourcesDoc;
            result.AdditionalFirmResourcesFlag = account.new_FirmResourcesFlag.GetOptionSetValue("OrgStyle");
            result.Address1 = account.Address1_Line1;
            result.Address2 = account.Address1_Line2;
            result.Address3 = account.Address1_Line3;
            result.Address1City = account.Address1_City;
            result.PostalCode = account.Address1_PostalCode;
            result.Address1Country = account.Address1_Country;
            result.AppropriatenessOfFundSize = account.new_AppropriatenessofFundSize;
            result.AppropriatenessOfFundSizeDoc = account.new_FundSizeDoc;
            result.AppropriatenessOfFundSizeFlag = account.new_FundSizeFlag.GetOptionSetValue("ColorCode");
            result.Aum = account.new_AUM.GetDecimalValue();
            result.Category = account.new_OrgTypeId.GetName();
            result.CoInvestmentOpportunities = account.new_CoInvestmentOpportunities;
            result.CoInvestmentOpportunitiesDoc = account.new_CoInvestDoc;
            result.CoInvestmentOpportunitiesFlag = account.new_CoInvestFlag.GetOptionSetValue("OrgStyle");
            result.CompensationStructure = account.new_CompensationStrucutre;
            result.CompensationStructureDoc = account.new_CompDoc;
            result.CompensationStructureFlag = account.new_CompFlag.GetOptionSetValue("OrgStyle");
            result.CompetitionAndCompetitiveAdvantage = account.new_CompetitionandCompetitiveAdvantage;
            result.CompetitionAndCompetitiveAdvantageDoc = account.new_CompetitionDoc;
            result.CompetitionAndCompetitiveAdvantageFlag = account.new_CompetitionFlag.GetOptionSetValue("ColorCode");
            result.Culture = account.new_Culture;
            result.CultureDoc = account.new_CultureDoc;
            result.CultureFlag = account.new_CultureFlag.GetOptionSetValue("OrgStyle");
            result.CurrencyId =
                account.TransactionCurrencyId.GetIdForEntityLogicalName(TransactionCurrency.EntityLogicalName);
            result.DealExecution = account.new_DealExecution;
            result.DealExecutionDoc = account.new_DealExecutionDoc;
            result.DealExecutionFlag = account.new_DealExecutionFlag.GetOptionSetValue("OrgStyle");
            result.DealPipeline = account.new_DealPipeline;
            result.DealPipelineDoc = account.new_DealPipelineDoc;
            result.DealPipelineFlag = account.new_DealPipelineFlag.GetOptionSetValue("OrgStyle");
            result.DealStructuring = account.new_DealStructuring;
            result.DealStructuringDoc = account.new_DealDoc;
            result.DealStructuringFlag = account.new_DealFlag.GetOptionSetValue("ColorCode");
            result.DecisionMakingProcess = account.new_DecisionMakingProcess;
            result.DecisionMakingProcessDoc = account.new_DecisionDoc;
            result.DecisionMakingProcessFlag = account.new_DecisionFlag.GetOptionSetValue("OrgStyle");
            result.DiligenceLevel = account.new_DiligenceLevel;
            result.DueDiligenceProcess = account.new_DueDiligenceProcess;
            result.DueDiligenceProcessDoc = account.new_DDProcessDoc;
            result.DueDiligenceProcessFlag = account.new_DDProcessFlag.GetOptionSetValue("OrgStyle");
            result.EbitdaMaxValue = account.new_EBITDAMax.GetDecimalValue();
            result.EbitdaMinValue = account.new_EBITDAMin.GetDecimalValue();
            result.EmergingManager = account.new_EmergingManager.HasValue && account.new_EmergingManager.Value;
            result.EnterpriseMaxValue = account.new_TargetEVMax.GetDecimalValue();
            result.EnterpriseMinValue = account.new_TargetEVMin.GetDecimalValue();
            result.EquityCheckMaxValue = account.new_InvestmentSizeMax.GetDecimalValue();
            result.EquityCheckMinValue = account.new_InvestmentSizeMin.GetDecimalValue();
            result.ExitExecution = account.new_ExitExecution;
            result.ExitExecutionDoc = account.new_ExitexecutionDoc;
            result.ExitExecutionFlag = account.new_ExitExecutionFlag.GetOptionSetValue("OrgStyle");
            result.ExitStrategy = account.new_ExitStrategies;
            result.ExitStrategyDoc = account.new_ExitDoc;
            result.ExitStrategyFlag = account.new_ExitFlag.GetOptionSetValue("ColorCode");
            result.ExpectedNextFundRaise = account.new_ExpectedNextFundraise;
            result.FirmCapabilitiesAndResources = account.new_FirmCapabiltiiesandResourcetoExecuteStrat;
            result.FirmCapabilitiesAndResourcesDoc = account.new_CapabilitiesDoc;
            result.FirmCapabilitiesAndResourcesFlag = account.new_CapabilitiesFlag.GetOptionSetValue("OrgStyle");
            result.FundRaisingStatus = account.new_FundraisingStatus.GetOptionSetValue("FundraisingStatus");
            result.GeographicFocus = account.TerritoryId.GetName();
            result.History = account.new_OrganizationHistory;
            result.InvestmentStrategyDetails = account.new_InvestmentStrategyDetails;
            result.InvestmentStrategyDetailsDoc = account.new_StratDetailsDoc;
            result.InvestmentStrategyDetailsFlag = account.new_StratDetailsFlag.GetOptionSetValue("ColorCode");
            result.InvestmentThesis = account.new_InvestmentThesis;
            result.KeyTakeAway = account.new_KeyTakeaway;
            result.LastFundSize = account.new_LastFundSize.GetDecimalValue();
            result.LastUpdated = account.new_UpdatedAsOf;
            result.LegalReview = account.new_LegalReview;
            result.LegalReviewDoc = account.new_LegalDoc;
            result.LegalReviewFlag = account.new_LegalFlag.GetOptionSetValue("OrgStyle");
            result.MarketStage = account.new_OrgSubTypeId.GetName();
            result.Monitoring = account.new_Monitoring;
            result.MonitoringDoc = account.new_MonitoringDoc;
            result.MonitoringFlag = account.new_MonitoringFlag.GetOptionSetValue("OrgStyle");
            result.Name = account.Name;
            result.NumberOfFunds = account.new_NumberofClosedFunds;
            result.NumberOfInvestmentProfessionals = account.new_NumberofProfessionals;
            result.OperationalDiligence = account.new_OperationalDiligence;
            result.OperationalDiligenceDoc = account.new_OpsDoc;
            result.OperationalDiligenceFlag = account.new_OpsFlag.GetOptionSetValue("OrgStyle");
            result.OrganizationOverview = account.new_Overview;
            result.PortfolioConstruction = account.new_PortfolioConstuction;
            result.PortfolioConstructionDoc = account.new_PortConstDoc;
            result.PortfolioConstructionFlag = account.new_PortConstFlag.GetOptionSetValue("ColorCode");
            result.PortfolioDoc = account.new_portfoliodoc;
            result.PrimaryOffice = account.Address1_City + (!string.IsNullOrWhiteSpace(account.Address1_StateOrProvince) ? ", " + account.Address1_StateOrProvince : string.Empty);
            result.RecentAdditionsAndDepartures = account.new_RecentAdditionsandDepartures;
            result.RecentAdditionsAndDeparturesDoc = account.new_RecentAddDepartDoc;
            result.RecentAdditionsAndDeparturesFlag = account.new_RecentAddDepartFlag.GetOptionSetValue("OrgStyle");
            result.ReportingAndInvestorRelations = account.new_ReportingandInvestmentRelations;
            result.ReportingAndInvestorRelationsDoc = account.new_ReportingDoc;
            result.ReportingAndInvestorRelationsFlag = account.new_ReportingFlag.GetOptionSetValue("OrgStyle");
            result.RevenueMaxValue = account.new_RevMax.GetDecimalValue();
            result.RevenueMinValue = account.new_RevMin.GetDecimalValue();
            result.RiskManagement = account.new_RiskManagement;
            result.RiskManagementDoc = account.new_RiskDoc;
            result.RiskManagementFlag = account.new_RiskFlag.GetOptionSetValue("ColorCode");
            result.SectorFocusPipeDelimited = String.Join("|", account.new_sectorpick_value.ParseCrmSelectString());
            result.SourcingStrategy = account.new_SourcingStrategy;
            result.SourcingStrategyDoc = account.new_SourcingDoc;
            result.SourcingStrategyFlag = account.new_SourcingFlag.GetOptionSetValue("OrgStyle");
            result.StaffingModel = account.new_StaffingModel;
            result.StaffingModelDoc = account.new_StaffingDoc;
            result.StaffingModelFlag = account.new_StaffingFlag.GetOptionSetValue("OrgStyle");
            result.StrategyPipeDelimited = String.Join("|", account.new_Strategypick_value.ParseCrmSelectString());
            result.StrategyDrift = account.new_StrategyDrift;
            result.StrategyDriftDoc = account.new_DriftDoc;
            result.StrategyDriftFlag = account.new_DriftFlag.GetOptionSetValue("ColorCode");
            result.SubCategory = account.new_OrgSubTypeId.GetName();
            result.SubSectorFocusPipeDelimited = String.Join("|", account.new_picklisttest_value.ParseCrmSelectString());
            result.TeamOverview = account.new_TeamOverview;
            result.Type = account.new_OrgTypeId.GetName();
            result.TypicalLeverageMaxValue = account.new_DebtxMax;
            result.TypicalLeverageMinValue = account.new_DebtxMin;
            result.ValueAddedApproach = account.new_PortfolioVallueAdd;
            result.ValueAddedApproachDoc = account.new_PortValueAddDoc;
            result.ValueAddedApproachFlag = account.new_PortValueAddFlag.GetOptionSetValue("OrgStyle");
            result.WhatMakesThemUnique = account.new_WhatMakesThemUnique;
            result.WhatMakesThemUniqueDoc = account.new_UniqueDoc;
            result.WhatMakesThemUniqueFlag = account.new_UniqueFlag.GetOptionSetValue("ColorCode");
            result.YearFounded = account.new_YearFounded;
            result.WebsiteUrl = account.WebSiteURL;
            result.AccessConstrained = account.new_AccessConstrained.HasValue && account.new_AccessConstrained.Value;
            result.SubStrategyPipeDelimited = String.Join("|", account.new_SubStrategy_value.ParseCrmSelectString());
            result.TrackRecordOverview = account.new_TrackRecordOverview;
            result.TrackRecordOverviewFlag = account.new_TrackRecordFlag.GetOptionSetValue("ColorCode");
            result.TrackRecordOverviewDoc = account.new_TrackRecordDoc;
            result.FirmValuationPolicy = account.new_FirmValuationPolicy;
            result.FirmValuationPolicyDoc = account.new_ValuationDoc;
            result.FirmValuationPolicyFlag = account.new_ValuationFlag.GetOptionSetValue("ColorCode");
            result.PublishEvaluation = account.new_PublishLevel4.HasValue ? account.new_PublishLevel4.Value : false;
            result.PublishSearch = account.new_PublishLevel0.HasValue ? account.new_PublishLevel0.Value : false;
            result.PublishOverviewAndTeam = account.new_PublishLevel1.HasValue ? account.new_PublishLevel1.Value : false;
            result.PublishStratProcessFirmAndFund = account.new_PublishLevel2.HasValue ? account.new_PublishLevel2.Value : false;
            result.PublishTrackRecord = account.new_PublishLevel3.HasValue ? account.new_PublishLevel3.Value : false;
            result.CountriesPipeDelimited = String.Join("|", account.new_CountryPick_Value.ParseCrmSelectString());
            result.SubRegionsPipeDelimited = String.Join("|", account.new_SubRegionPick_Value.ParseCrmSelectString());
            result.InvestmentRegionPipeDelimited = String.Join("|", account.new_RegionPick_Value.ParseCrmSelectString());
            result.SectorSpecialist = account.new_SectorSpecialist;
            result.QualitativeGrade = account.new_GradeQualitativeFinalGrade.GetOptionSetValue("FinalGrade");
            result.QuantitativeGrade = account.new_GradeQuantitativeFinalGrade.GetOptionSetValue("FinalGrade");
            result.QualitativeGradeNumber = account.new_GradeQualitativeFinal.HasValue ? Convert.ToDouble(account.new_GradeQualitativeFinal) : new double?();
            result.QuantitativeGradeNumber = account.new_GradeQuantitativeFinal.HasValue ? Convert.ToDouble(account.new_GradeQuantitativeFinal) : new double?();
            result.EvaluationLevel = account.new_EvaluationLevel.GetOptionSetValue("EvaluationLevel");
            result.FirmHighlights = account.new_FirmHighlights;
            result.EvaluationText = account.new_EvaluationText;
            result.TrackRecordText = account.new_TrackRecordText;
            result.FocusRadar = account.new_FocusTargetList.GetOptionSetValue("FocusRadar");
            result.CoInvestWithExistingLPs = account.new_CoInvestmentOpportunitiesforFundLPs.GetOptionSetValue("YesNo");
            result.CoInvestWithOtherLPs = account.new_CoInvestmentOpportunitiesforNonFundLPs.GetOptionSetValue("YesNo");
            result.StateProvince = account.Address1_StateOrProvince;
            result.Address3 = account.Address1_Line3;
            result.Latitude = account.Address1_Latitude;
            result.Longitude = account.Address1_Longitude;
            result.SBICFund = account.new_SBICFund.GetValueOrDefault(false);

            //account.address
            if (account.new_FormerName != null)
            {
                result.AliasesPipeDelimited = String.Join("|", account.new_FormerName.Split(new char[] { ';' }).Select(m => m.Trim()).ToList());
            }
            PopulateGradeFromCrmEntity(account, result);
            return result;
        }

        public static IEnumerable<AtlasDiligence.Common.Data.Models.Organization> OrganizationsFromCrmEntities(IEnumerable<Account> accounts)
        {
            return accounts.Select(OrganizationFromCrmEntity);
        }

        private static void PopulateGradeFromCrmEntity(Account account, Data.Models.Organization organization)
        {

            if (!organization.Grades.Any())
            {
                organization.Grades.Add(new Grade { Id = Guid.NewGuid() });
            }
            var grade = organization.Grades.FirstOrDefault();
            grade.OrganizationId = organization.Id;
            grade.ExpertiseAlignedWithStrategy = account.new_GradeExpertiseAlignedwithStrategy.GetOptionSetValue("Grade");
            grade.HistoryTogether = account.new_GradeHistoryTogether.GetOptionSetValue("Grade");
            grade.ComplementarySkills = account.new_GradeComplementarySkills.GetOptionSetValue("Grade");
            grade.TeamDepth = account.new_GradeTeamDepth.GetOptionSetValue("Grade");
            grade.ValueAddResources = account.new_GradeValueaddResources.GetOptionSetValue("Grade");
            grade.InvestmentThesis = account.new_GradeInvestmentThesis.GetOptionSetValue("Grade");
            grade.CompetitiveAdvantage = account.new_GradeCompetitiveAdvantage.GetOptionSetValue("Grade");
            grade.PortfolioConstruction = account.new_GradePortfolioConstruction.GetOptionSetValue("Grade");
            grade.AppropriatenessOfFundSize = account.new_GradeAppropriatenessofFundSize.GetOptionSetValue("Grade");
            grade.ConsistencyOfStrategy = account.new_GradeConsistencyofStrategy.GetOptionSetValue("Grade");
            grade.Sourcing = account.new_GradeSourcing.GetOptionSetValue("Grade");
            grade.DueDiligence = account.new_GradeDueDiligence.GetOptionSetValue("Grade");
            grade.DecisionMaking = account.new_GradeDecisionMaking.GetOptionSetValue("Grade");
            grade.DealExecutionStructure = account.new_GradeDealExecutionStructure.GetOptionSetValue("Grade");
            grade.PostInvestmentValueAdd = account.new_GradePostInvestmentValueadd.GetOptionSetValue("Grade");
            grade.TeamStability = account.new_GradeTeamStability.GetOptionSetValue("Grade");
            grade.OwnershipAndCompensation = account.new_GradeOwnershipandCompensation.GetOptionSetValue("Grade");
            grade.Culture = account.new_GradeCulture.GetOptionSetValue("Grade");
            grade.AlignmentWithLPs = account.new_GradeAlignmentwithLPs.GetOptionSetValue("Grade");
            grade.Terms = account.new_GradeTerms.GetOptionSetValue("Grade");
            grade.AbsolutePerformance = account.new_GradeAbsolutePerformance.GetOptionSetValue("Grade");
            grade.RelativePerformance = account.new_GradeRelativePerformance.GetOptionSetValue("Grade");
            grade.RealizedPerformance = account.new_GradeRealizedPerformance.GetOptionSetValue("Grade");
            grade.DepthOfTrackRecord = account.new_GradeDepthofTrackRecord.GetOptionSetValue("Grade");
            grade.RelevanceOfTrackRecord = account.new_GradeRelevanceofTrackRecord.GetOptionSetValue("Grade");
            grade.LossRatioAnalysis = account.new_GradeLossRatioAnalysis.GetOptionSetValue("Grade");
            grade.UnrealizedPortfolio = account.new_GradeUnrealizedPortfolio.GetOptionSetValue("Grade");
            grade.AtlasValueCreationAnalysis = account.new_GradeAtlasValueCreationAnalysis.GetOptionSetValue("Grade");
            grade.AtlasHitsAndMisses = account.new_GradeAtlasHitsandMisses.GetOptionSetValue("Grade");
            grade.DealSize = account.new_GradeDealSize.GetOptionSetValue("Grade");
            grade.Strategy = account.new_GradeStrategy.GetOptionSetValue("Grade");
            grade.Time = account.new_GradeTime.GetOptionSetValue("Grade");
            grade.Team = account.new_GradeTeam.GetOptionSetValue("Grade");
            grade.ScatterchartText = account.new_GradeScatterchartText;
            grade.AQScorecardText = account.new_GradeQualitativeText;
            grade.QoRScorecardText = account.new_GradeQuantitativeText;
        }

        #endregion

        #region organization member
        public static OrganizationMember OrganizationMemberFromCrmEntity(Contact orgMember)
        {
            if (orgMember == null) return null;
            var yearsAtOrg = Maybe.ToInt32(orgMember.new_YearStarterwithFirm);
            var result = new OrganizationMember
            {
                Id = orgMember.Id,
                Email = orgMember.EMailAddress1,
                FirstName = orgMember.FirstName,
                LastName = orgMember.LastName,
                Address1 = orgMember.Address1_Line1,
                Address2 = orgMember.Address1_Line2,
                Address3 = orgMember.Address1_Line3,
                City = orgMember.Address1_City,
                Country = orgMember.Address1_Country,
                JobTitle = orgMember.JobTitle,
                OrganizationId = orgMember.ParentCustomerId.GetIdForEntityLogicalName(Account.EntityLogicalName).Value,
                Phone = orgMember.Telephone1,
                StateOrProvince = orgMember.Address1_StateOrProvince,
                PostalCode = orgMember.Address1_PostalCode,
                YearsAtOrganization = yearsAtOrg.HasValue ? (DateTime.Now.Year - yearsAtOrg.Value).ToString() : String.Empty,
                Description = orgMember.Description,
                DisplayOrder = orgMember.new_DisplayOrder,
            };
            if (orgMember.new_FirstNameAliases != null)
            {
                result.AliasesPipeDelimited = String.Join("|", orgMember.new_FirstNameAliases.Split(new char[] { ',' }).Select(
                        m => m.Trim() + " " + orgMember.LastName));
            }
            return result;
        }

        public static IEnumerable<OrganizationMember> OrganizationMembersFromCrmEntities(IEnumerable<Contact> members)
        {
            return members.Select(OrganizationMemberFromCrmEntity);
        }

        #endregion

        #region web user
        public static WebUser WebUserFromCrmEntity(new_webuser user)
        {
            return WebUserFromCrmEntity(user, null);
        }

        public static WebUser WebUserFromCrmEntity(new_webuser user, new_group group)
        {
            if (user == null) return null;

            var retval = new Model.WebUser
            {
                Id = user.Id,
                Email = user.new_email,
                FirstName = user.new_firstname,
                LastName = user.new_lastname,
                GroupId = user.new_groupid.GetIdForEntityLogicalName(new_group.EntityLogicalName),
                GroupName = user.new_groupid != null ? user.new_groupid.Name : String.Empty,
                Group = GroupFromCrmEntity(group),
                IsValidated = user.new_validated.HasValue && user.new_validated.Value,
                InitialPassword = user.new_InitialPassword,
                RssApp = user.new_rssRole ?? false,
                DiligenceApp = user.new_DiligenceApp ?? false
            };

            return retval;
        }

        public static IEnumerable<WebUser> WebUsersFromCrmEntities(IEnumerable<new_webuser> users)
        {
            return users.Select(WebUserFromCrmEntity);
        }

        public static IEnumerable<WebUser> WebUsersFromCrmEntities(IDictionary<new_webuser, new_group> users)
        {
            return users.Select(user => WebUserFromCrmEntity(user.Key, user.Value));
        }

        public static new_webuser CrmEntityFromWebUser(WebUser user)
        {
            if (user == null) return null;
            var result = new new_webuser
                       {
                           Id = user.Id,
                           new_name = string.Format("{0} {1}", user.FirstName, user.LastName),
                           new_email = user.Email,
                           new_firstname = user.FirstName,
                           new_lastname = user.LastName,
                           new_groupid = user.GroupId.HasValue ? new EntityReference(new_group.EntityLogicalName, user.GroupId.Value) : null,
                           new_InitialPassword = user.InitialPassword,
                           new_validated = user.IsValidated,
                           new_rssRole = user.RssApp,
                           new_DiligenceApp = user.DiligenceApp
                       };
            if (result.new_groupid != null)
            {
                result.new_groupid.Name = user.GroupName;
            }
            return result;
        }
        #endregion

        #region group
        public static Group GroupFromCrmEntity(new_group group)
        {
            if (group == null) return null;
            var retval = new Group
                       {
                           Id = group.Id,
                           MaxUsers = group.new_maxusers ?? 0,
                           Name = group.new_name
                       };
            if (group.new_webuserid != null)
            {
                retval.GroupLeaderWebUserId = group.new_webuserid.Id;
                retval.GroupLeaderWebUserName = group.new_webuserid.Name;
            }

            return retval;
        }

        public static new_group CrmEntityFromGroup(Group group)
        {
            if (group == null) return null;
            var result = new new_group
                             {
                                 Id = group.Id,
                                 new_name = group.Name,
                                 new_maxusers = group.MaxUsers,
                                 new_webuserid =
                                     group.GroupLeaderWebUserId.HasValue
                                         ? new EntityReference(new_webuser.EntityLogicalName, group.GroupLeaderWebUserId.Value)
                                         : null
                             };

            return result;
        }
        #endregion

        #region news item
        public static NewsItem NewsItemFromCrmEntity(new_newsitem newsItem)
        {
            if (newsItem == null) return null;
            return new NewsItem
            {
                Content = newsItem.new_Description,
                Summary = newsItem.new_summary,
                Id = newsItem.Id,
                PublishedDate = newsItem.new_PublishedDate.HasValue ? newsItem.new_PublishedDate.Value : DateTime.MinValue,
                Subject = newsItem.new_Subject,
                Url = newsItem.new_URL,
                OrganizationSourceId = newsItem.new_organizationId.GetIdForEntityLogicalName(Account.EntityLogicalName),
                OrganizationSourceName = newsItem.new_organizationId != null ? newsItem.new_organizationId.Name : String.Empty
            };
        }
        public static IEnumerable<NewsItem> NewsItemsFromCrmEntities(IEnumerable<new_newsitem> newsItems)
        {
            foreach (var newsItem in newsItems)
            {
                yield return NewsItemFromCrmEntity(newsItem);
            }
        }
        public static new_newsitem CrmEntityFromNewsItem(NewsItem newsItem)
        {
            if (newsItem == null) return null;
            return new new_newsitem
            {
                new_Description = newsItem.Content.Length >= 15000 ? newsItem.Content.Substring(0, 15000) : newsItem.Content,
                new_summary = newsItem.Summary.Length >= 10000 ? newsItem.Summary.Substring(0, 10000) : newsItem.Summary,
                Id = newsItem.Id,
                new_PublishedDate = newsItem.PublishedDate,
                new_Subject = newsItem.Subject,
                new_URL = newsItem.Url,
                new_organizationId = newsItem.OrganizationSourceId.HasValue ? new EntityReference(Account.EntityLogicalName, newsItem.OrganizationSourceId.Value) : null
            };
        }
        #endregion

        #region rss feed
        public static RssFeed RssFeedFromCrmEntity(new_rssfeed rssFeed)
        {
            if (rssFeed == null) return null;
            return new RssFeed
                       {
                           Address = rssFeed.new_address,
                           Content = rssFeed.new_content,
                           Id = rssFeed.Id,
                           OrganizationSourceId = rssFeed.new_organizationId.GetIdForEntityLogicalName(Account.EntityLogicalName),
                           OrganizationSourceName = rssFeed.new_organizationId.GetName()
                       };
        }
        public static IEnumerable<RssFeed> RssFeedsFromCrmEntities(IEnumerable<new_rssfeed> rssFeeds)
        {
            return rssFeeds.Select(RssFeedFromCrmEntity);
        }

        #endregion

        #region fund
        public static Fund FundFromCrmEntity(new_fund fund)
        {
            if (fund == null) return null;
            var result = new Fund();
            
            //pulls new_GPCommittment (two t's). new_GPCommitment is depricated.
            result.GpCommitment = (decimal?)fund.new_GPCommittment;
            result.Dpi = fund.new_DPI;
            result.Description = fund.new_Description;
            result.Geography = fund.new_TerritoryId.GetName();
            result.Id = fund.Id;
            result.Name = fund.new_name;
            result.FundSize = fund.new_FundSize.GetDecimalValue();
            result.TargetSize = fund.new_TargetFundSize.GetDecimalValue();
            result.Status = fund.new_FundraisingStatus.GetOptionSetValue("FundraisingStatus");
            result.VintageYear = fund.new_GPVintageYear;
            result.NumberOfPortfolioCompanies = fund.new_NumberofCompanies;
            result.OrganizationId = fund.new_OrganizationId.GetIdForEntityLogicalName(Account.EntityLogicalName).Value;
            result.AsOf = fund.new_AsOfDate;
            result.AdvisoryBoard = fund.new_AdvisoryBoard;
            result.AdvisoryBoardNotes = fund.new_AdvisoryBoardNotes;
            result.CarriedInterest = fund.new_CarriedInterest;
            result.CarriedInterestNotes = fund.new_CarriedInterestNotes;
            result.Clawback = fund.new_Clawback;
            result.ClawbackNotes = fund.new_ClawbackNotes;
            result.CurrencyId =
                fund.TransactionCurrencyId.GetIdForEntityLogicalName(TransactionCurrency.EntityLogicalName);
            result.DebtMax = fund.new_DebtxMax;
            result.DebtMin = fund.new_DebtxMin;
            result.EbitdaMax = fund.new_TargetEBITDAMax.GetDecimalValue();
            result.EbitdaMin = fund.new_TargetEBITDAMin.GetDecimalValue();
            result.EquityCheckSizeMax = fund.new_InvestmentSizeMax.GetDecimalValue();
            result.EquityCheckSizeMin = fund.new_InvestmentSizeMin.GetDecimalValue();
            result.EvaluationOfKeyTerms = fund.new_EvaluationofKeyTerms;
            result.EvaluationOfKeyTermsFlag = fund.new_KeyTermsFlag.GetOptionSetValue("ColorCode");
            result.EvMax = fund.new_TargetEVMax.GetDecimalValue();
            result.EvMin = fund.new_TargetEVMin.GetDecimalValue();
            result.FinalClose = fund.new_FinalClose;
            result.FinalCloseNote = fund.new_FinalCloseNote;
            result.FirstClose = fund.new_FirstClose;
            result.FirstCloseNote = fund.new_FirstCloseNote;
            result.FundSizeNote = fund.new_FundSizeNote;
            result.FundTerm = fund.new_FundTerm;
            result.FundTermNotes = fund.new_FundTermNotes;
            result.FundraisingStatusNote = fund.new_FundraisingStatusNote;
            result.GeneralNotesOnWaterfall = fund.new_GeneralNotesonWaterfall;
            result.GeographicFocus = fund.new_TerritoryId.GetName();
            result.GpCatchUp = fund.new_GPCatchUp;
            result.GpCatchUpNotes = fund.new_GPCatchupNotes;
            result.GpCommitmentNote = fund.new_GPCommNote;
            result.GpRemovalNotes = fund.new_GPRemovalNotes;
            result.GpRemovalVote = fund.new_GPRemoval;
            result.HardCapSize = fund.new_HardCapSize.GetDecimalValue();
            result.InterimClawback = fund.new_InterimClawback.GetOptionSetValue("YesNo");
            result.InterimClawbackNotes = fund.new_InterimClawbackNotes;
            result.InvestedCapital = fund.new_InvestedCapital.GetDecimalValue();
            result.InvestmentPeriod = fund.new_InvestmentPeriod;
            result.InvestmentPeriodNotes = fund.new_InvestmentPeriodNotes;
            result.InvestmentRestrictions = fund.new_InvestmentRestrictions;
            result.Irr = fund.new_NetIRR;
            result.KeyMan = fund.new_KeyMan;
            result.KeyManTrigger = fund.new_KeyManTrigger.GetOptionSetValue("KeymanTrigger");
            result.KeyManTriggerNotes = fund.new_K;
            result.ManagementFee = fund.new_ManagementFee;
            result.ManagementFeeNotes = fund.new_ManagementFeeNotes;
            result.MarketStage = fund.new_FundSubStrategyId.GetName();
            result.Mfn = fund.new_MFN.GetOptionSetValue("YesNo");
            result.MfnNotes = fund.new_MFNNotes;
            result.Moic = fund.new_MOIC;
            result.NoFaultDivorceVote = fund.new_NoFaultDivorceVote;
            result.NoFaultNotes = fund.new_NoFaultNotes;
            result.OtherDealFees = fund.new_OtherDealFees;
            result.OtherFeeNotes = fund.new_OtherDealFeeNotes;
            result.PlacementAgent = fund.new_PlacementAgent.GetName();
            result.PreferredReturn = fund.new_PreferredReturn;
            result.PreferredReturnNotes = fund.new_PreferredReturnNotes;
            result.RealizedValue = fund.new_RealizedValue.GetDecimalValue();
            result.RevenueMax = fund.new_TargetRevenueMax.GetDecimalValue();
            result.RevenueMin = fund.new_TargetRevenueMin.GetDecimalValue();
            result.SectorFocusPipeDelimited = String.Join("|", fund.new_Sectorpick_value.ParseCrmSelectString());
            result.Strategy = fund.new_StrategyPick_value;
            result.SubSector = fund.new_SubSectorpick_value;
            result.SubStrategy = fund.new_SubStrategypick_value;
            result.TargetFundSizeNote = fund.new_TargetFundSizeNote;
            result.TieredCarryLevel = fund.new_TieredCarryLevel;
            result.TieredCarryNotes = fund.new_TierredCarryNotes;
            result.TotalValue = fund.new_TotalValue.GetDecimalValue();
            result.TransFeeNotes = fund.new_TransOffsetFeeNotes;
            result.TransFeeOffset = fund.new_FeeOffset;
            result.Tvpi = fund.new_TVPI;
            result.TypicalLeverageMax = fund.new_DebtxMax;
            result.TypicalLeverageMin = fund.new_DebtxMin;
            result.UnrealizedValue = fund.new_UnrealizedValue.GetDecimalValue();
            result.AnalysisUrl = fund.new_FundPortfolioDoc;
	        result.PublishOverview = fund.new_PublishLevel1.GetValueOrDefault(false);
            result.PublishDetail = fund.new_PublishLevel2.GetValueOrDefault(false);
            result.PublishTrackRecord = fund.new_PublishLevel3.GetValueOrDefault(false);
            result.ModifiedOn = fund.ModifiedOn;
            result.SortOrder = Maybe.ToInt32(fund.new_FundNumber);
            result.GrossIrr = fund.new_GrossIRR;

            // Preqin schema
            result.PerformanceDatasource = fund.new_PerformanceDataSource == true
                ? "Preqin" : "General Partner";
            result.PreqinAsOf = fund.new_PreqinAsOfDate;
            result.PreqinTvpi = fund.new_PreqinTVPImultiple;
            result.PreqinDpi = fund.new_PreqinDPImultiple;
            result.PreqinIrr = fund.new_PreqinNetIRR;
            result.PreqinFundSize = fund.new_PreqinFundSize_USD;
            result.PreqinCalled = fund.new_PreqinCalled;

            return result;
        }

        public static Fund FundFromCrmEntity(new_fund fund, IEnumerable<Account> accounts)
        {
            if (fund == null) return null;
            var result = FundFromCrmEntity(fund);
            //result.LimitedPartners = LimitedPartnersFromCrmEntities(accounts).ToList();
            return result;
        }

        public static IEnumerable<Fund> FundsFromCrmEntities(IDictionary<new_fund, IEnumerable<Account>> funds)
        {
            return funds.Select(f => FundFromCrmEntity(f.Key, f.Value));
        }

        public static IEnumerable<Fund> FundsFromCrmEntities(IEnumerable<new_fund> funds)
        {
            return funds.Select(FundFromCrmEntity);
        }

        #endregion

        #region portfolio company
        public static PortfolioCompany GetPorfolioCompanyFromConnection(Connection connection)
        {
            if (connection == null) return null;
            return new PortfolioCompany
                       {
                           OrganizationId = connection.Record1Id.Id,
                           CompanyId = connection.Record2Id.Id,
                           CompanyName = connection.Record2Id.Name,
                           Description = connection.account_connections2.Description,
                           Id = connection.Id
                       };
        }
        public static IEnumerable<PortfolioCompany> GetPortfolioCompaniesFromConnections(IEnumerable<Connection> connections)
        {
            return connections.Select(GetPorfolioCompanyFromConnection);
        }

        #endregion

        #region evaluation
        public static Evaluation EvaluationFromCrmEntity(new_strengths strength)
        {
            if (strength == null) return null;
            return new Evaluation
            {
                Id = strength.Id,
                Description = strength.new_name,
                Note = strength.new_Note,
                OrganizationId = strength.new_Organization.GetIdForEntityLogicalName(Account.EntityLogicalName).Value,
                Type = (int)EvaluationType.Strength,
                SortOrder = strength.new_Order.HasValue ? strength.new_Order.Value : 0
            };
        }
        public static Evaluation EvaluationFromCrmEntity(new_concerns concern)
        {
            if (concern == null) return null;
            return new Evaluation
            {
                Id = concern.Id,
                Description = concern.new_name,
                Note = concern.new_Note,
                OrganizationId = concern.new_Organization.GetIdForEntityLogicalName(Account.EntityLogicalName).Value,
                Type = (int)EvaluationType.Concern,
                SortOrder = concern.new_Order.HasValue ? concern.new_Order.Value : 0
            };
        }
        public static IEnumerable<Evaluation> EvaluationsFromCrmEntities(IEnumerable<new_strengths> strengths)
        {
            return strengths.Select(EvaluationFromCrmEntity);
        }
        public static IEnumerable<Evaluation> EvaluationsFromCrmEntities(IEnumerable<new_concerns> concerns)
        {
            return concerns.Select(EvaluationFromCrmEntity);
        }
        #endregion

        #region limited partner
        public static LimitedPartner LimitedPartnerFromCrmEntity(Account account)
        {
            if (account == null) return null;
            return new LimitedPartner
                       {
                           Id = account.Id,
                           Name = account.Name,
                           Type = account.new_OrgTypeId.GetName()
                       };
        }
        public static IEnumerable<LimitedPartner> LimitedPartnersFromCrmEntities(IEnumerable<Account> accounts)
        {
            return accounts.Select(LimitedPartnerFromCrmEntity);
        }
        #endregion

        #region board seat
        public static BoardSeat BoardSeatFromCrmEntity(Connection connection)
        {
            return new BoardSeat
                       {
                           Id = connection.Id,
                           OrganizationMemberId = connection.Record1Id.LogicalName == Contact.EntityLogicalName
                           ? connection.Record1Id.Id
                           : connection.Record2Id.Id,
                           OrganizationName = connection.Record1Id.LogicalName == Account.EntityLogicalName
                           ? connection.Record1Id.Name
                           : connection.Record2Id.Name,
                           YearJoined = connection.EffectiveStart,
                           PastPresent = connection.new_PastPresent.GetOptionSetValue("PastPresent")
                       };
        }
        public static IEnumerable<BoardSeat> BoardSeatsFromCrmEntities(IEnumerable<Connection> connections)
        {
            return connections.Select(BoardSeatFromCrmEntity).ToList().Distinct();
        }
        #endregion

        #region education history
        public static EducationHistory EducationHistoryFromCrmEntity(Connection connection)
        {
            return new EducationHistory
                       {
                           Id = connection.Id,
                           Degree = connection.new_EducationDegree.GetOptionSetValue("EducationHistoryDegree"),
                           Notes = connection.Description,
                           School = connection.Record1Id.LogicalName == Account.EntityLogicalName
                           ? connection.Record1Id.Name
                           : connection.Record2Id.Name,
                           Year = connection.EffectiveEnd,
                           OrganizationMemberId = connection.Record1Id.LogicalName == Contact.EntityLogicalName
                           ? connection.Record1Id.Id
                           : connection.Record2Id.Id
                       };
        }
        public static IEnumerable<EducationHistory> EducationHistoryFromCrmEntities(IEnumerable<Connection> connections)
        {
            return connections.Select(EducationHistoryFromCrmEntity).Distinct();
        }
        #endregion

        #region employment history
        public static EmploymentHistory EmploymentHistoryFromCrmEntity(Connection connection)
        {
            var yearStart = connection.EffectiveStart.HasValue ? connection.EffectiveStart.Value.Year.ToString() : String.Empty;
            var yearEnd = connection.EffectiveEnd.HasValue ? connection.EffectiveEnd.Value.Year.ToString() : String.Empty;
            return new EmploymentHistory
                       {
                           Id = connection.Id,
                           OrganizationMemberId = connection.Record1Id.Id,
                           JobTitle = connection.new_Title,
                           OrganizationName = connection.Record1Id.LogicalName == Account.EntityLogicalName
                           ? connection.Record1Id.Name
                           : connection.Record2Id.Name,
                           Role = connection.Description,
                           YearsAtOrganization = (String.IsNullOrEmpty(yearStart) && String.IsNullOrEmpty(yearEnd)) ? String.Empty : yearStart + " - " + yearEnd
                       };
        }
        public static IEnumerable<EmploymentHistory> EmploymentHistoryFromCrmEntities(IEnumerable<Connection> connections)
        {
            return connections.Select(EmploymentHistoryFromCrmEntity).Distinct();
        }
        #endregion

        #region due diligence item
        public static DueDiligenceItem DueDiligenceItemFromCrmEntity(new_duediligenceitems item)
        {
            return new DueDiligenceItem
                       {
                           Id = item.Id,
                           Description = item.new_Note,
                           OrganizationId = item.new_Organization.GetIdForEntityLogicalName("account").Value,
                           TabName = item.new_DDItemTab.GetOptionSetValue("DueDiligenceItem"),
                           Title = item.new_name
                       };
        }
        public static IEnumerable<DueDiligenceItem> DueDiligenceItemsFromCrmEntities(IEnumerable<new_duediligenceitems> items)
        {
            return items.Select(DueDiligenceItemFromCrmEntity).Distinct();
        }
        #endregion

        #region other address
        public static OtherAddress OtherAddressFromCrmEntity(new_addressesprimary address)
        {
            return new OtherAddress
                       {
                           Address1 = address.new_Street1,
                           Address2 = address.new_Street2,
                           Address3 = address.new_Street3,
                           City = address.new_City,
                           StateProvince = address.new_StateProvince,
                           PostalCode = address.new_ZipPostalCode,
                           Country = address.new_CountryRegion,
                           Email = address.new_Email,
                           Fax = address.new_Fax,
                           Id = address.Id,
                           OrganizationId = address.new_Organization.Id,
                           Phone = address.new_Phone,
                           Latitude = address.new_Latitude,
                           Longitude = address.new_Longitude
                       };
        }
        public static IEnumerable<OtherAddress> OtherAddressesFromCrmEntities(IEnumerable<new_addressesprimary> items)
        {
            return items.Select(OtherAddressFromCrmEntity);
        }
        #endregion

        #region currency
        public static Currency CurrencyFromCrmEntity(TransactionCurrency currency)
        {
            if (currency == null) return new Currency { Code = "USD", Id = Guid.NewGuid(), Name = "US Dollars", Symbol = "$" };
            return new Currency
                       {
                           Code = currency.ISOCurrencyCode,
                           Id = currency.Id,
                           Name = currency.CurrencyName,
                           Symbol = currency.CurrencySymbol
                       };
        }

        public static IEnumerable<Currency> CurrenciesFromCrmEntity(IEnumerable<TransactionCurrency> currencies)
        {
            return currencies.Select(CurrencyFromCrmEntity);
        }
        #endregion

        #region track record
        public static TrackRecord TrackRecordFromCrmEntity(new_trackrecorditem trackRecord)
        {
            return new TrackRecord
                       {
                           Id = trackRecord.Id,
                           OrganizationId = trackRecord.new_Organization.Id,
                           Title = trackRecord.new_name,
                           Description = trackRecord.new_Description,
                           FileUrl = trackRecord.new_filename,
                           Order = trackRecord.new_DisplayOrder,
                           ModifiedOn = trackRecord.ModifiedOn
                       };
        }
        public static IEnumerable<TrackRecord> TrackRecordsFromCrmEntities(IEnumerable<new_trackrecorditem> items)
        {
            return items.Select(TrackRecordFromCrmEntity);
        }
        #endregion

        #region advisory board
        public static AdvisoryBoard AdvisoryBoardFromCrmEntity(Connection item)
        {
            return new AdvisoryBoard
            {
                Id = item.Id,
                FundId = item.Record1Id.Id,
                Name = item.account_connections2.Name,
                Type = item.account_connections2.new_OrgTypeId.GetName(),
                Description = item.Description
            };
        }
        public static IEnumerable<AdvisoryBoard> AdvisoryBoardFromCrmEntities(IEnumerable<Connection> items)
        {
            return items.Select(AdvisoryBoardFromCrmEntity).Distinct();
        }
        #endregion

        #region client request
        public static ClientRequest ClientRequestFromCrmEntity(new_clientrequestentity clientRequest)
        {
            var retval = new ClientRequest();
            retval.Id = clientRequest.Id;
            retval.GroupId = clientRequest.new_GPScoutGroup.Id;
            retval.DateReceived = clientRequest.new_DateofClientRequest;
            retval.Source = clientRequest.new_ClientRequestType.GetOptionSetValue("ClientRequestType");
            if (clientRequest.new_AssociatedFund != null)
            {
                retval.FundId = clientRequest.new_AssociatedFund.Id;
                retval.FundName = clientRequest.new_AssociatedFund.Name;
            }
            if (clientRequest.new_PlacementAgent != null)
            {
                retval.PlacementAgent = clientRequest.new_PlacementAgent.Name;
            }
            if (clientRequest.new_account_new_clientrequestentity_AssociatedGP != null)
            {
                retval.OrganizationId = clientRequest.new_account_new_clientrequestentity_AssociatedGP.Id;
                retval.OrganizationName = clientRequest.new_account_new_clientrequestentity_AssociatedGP.Name;
                retval.MarketStage =
                    clientRequest.new_account_new_clientrequestentity_AssociatedGP.new_OrgSubTypeId.GetName();
                retval.StrategyPipeDelimited = String.Join("|",
                                                           clientRequest
                                                               .new_account_new_clientrequestentity_AssociatedGP
                                                               .new_Strategypick_value.ParseCrmSelectString());
                retval.InvestmentRegionPipeDelimited = String.Join("|",
                                                                   clientRequest
                                                                       .new_account_new_clientrequestentity_AssociatedGP
                                                                       .new_RegionPick_Value.ParseCrmSelectString());
                retval.Status =
                    clientRequest.new_account_new_clientrequestentity_AssociatedGP.new_ProfileStatus.GetOptionSetValue(
                        "ProfileStatus");
            }
            return retval;
        }

        public static IEnumerable<ClientRequest> ClientRequestsFromCrmEntities(
            IEnumerable<new_clientrequestentity> clientRequest)
        {
            return clientRequest.Select(ClientRequestFromCrmEntity);
        }
        #endregion

		#region benchmarks

		public static Benchmark BenchmarkFromCrmEntity(new_benchmark benchmark)
		{
			var map = new Benchmark();

			map.Id = benchmark.Id;
			map.Name = benchmark.new_name;
			map.VintageYear = benchmark.new_BenchmarkVintageYear;
			
			if (benchmark.new_BenchmarkAsOf != null)
			{
				map.AsOf = benchmark.new_BenchmarkAsOf.Value.Date;
    }

			map.FundCount = benchmark.new_BenchmarkFundCount;

			map.MarketStage = benchmark.new_BenchmarkMarketStage != null ? benchmark.new_BenchmarkMarketStage.ToString() : String.Empty; // check this
			map.Region = benchmark.new_BenchmarkRegion != null ? benchmark.new_BenchmarkRegion.GetOptionSetValue("Regions") : String.Empty; // check this
			map.Strategy = benchmark.new_BenchmarkStrategy != null ? benchmark.new_BenchmarkStrategy.GetOptionSetValue("Strategy") : String.Empty; // check this

			map.PreqinDownloadDate = benchmark.new_PreqinDownloadDate;
			map.DpiFirstQuartile = benchmark.new_DPIFirstQuartile;
			map.DpiMedian = benchmark.new_DPIMedian;
			map.DpiThirdQuartile = benchmark.new_DPIThirdQuartile;
			map.NetIrrFirstQuartile = benchmark.new_NetIRRFirstQuartile;
			map.NetIrrMedian = benchmark.new_NetIRRMedian;
			map.NetIrrThirdQuartile = benchmark.new_NetIRRThirdQuartile;
			map.TvpiFirstQuartile = benchmark.new_TVPIFirstQuartile;
			map.TvpiMedian = benchmark.new_TVPIMedian;
			map.TvpiThirdQuartile = benchmark.new_TVPIThirdQuartile;

			return map;
}

		#endregion
	}
}
