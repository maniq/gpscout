﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Web;
using System.Configuration;

namespace AtlasDiligence.Common.DTO.General
{
    public static class EntityConfig
    {
        public static string GetOptionSetValue(string optionSetName, int value)
        {
            var file = XDocument.Parse(General.Resources.EntityConfig);
            var temp = from os in file.Descendants("OptionSets").Descendants("OptionSet")
                       where os.Attribute("name").Value == optionSetName
                       select os;
            var temp1 = temp.Descendants("Option");
            foreach (var t in temp1)
            {
                if (t.Descendants("Key").FirstOrDefault().Value == value.ToString())
                {
                    return t.Descendants("Value").FirstOrDefault().Value;
                }
            }

            return string.Empty;
        }
        public static Guid GetConnectionRoleGuid(string connectionRoleName)
        {
            var file = XDocument.Parse(General.Resources.EntityConfig);
            var temp = from os in file.Descendants("GuidSets").Descendants("Guids")
                       where os.Attribute("name").Value == "ConnectionRole"
                       select os.Descendants("Guid");
            foreach (var t in temp)
            {
                if (t.Descendants("Value").FirstOrDefault().Value == connectionRoleName)
                {
                    return new Guid(t.Descendants("Key").FirstOrDefault().Value);
                }
            }

            return Guid.Empty;
        }

        public static IEnumerable<string> GetFundraisingStatuses()
        {
            var file = XDocument.Parse(General.Resources.EntityConfig);
            var results = from os in file.Descendants("OptionSets").Descendants("OptionSet")
                          where os.Attribute("name").Value == "FundraisingStatus"
                          select os.Descendants("Option");
            return results.FirstOrDefault().Descendants("Value").Select(res => res.Value).ToList();
        }
    }
}
