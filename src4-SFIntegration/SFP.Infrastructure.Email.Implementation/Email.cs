﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SFP.Infrastructure.Email.Implementation
{
    public class Email: SFP.Infrastructure.Email.Contracts.IEmail
    {

        public Email()
        { 
        }

        public void EmailNotification(string subject, string body)
        {
            string from = WebConfigurationManager.AppSettings["FromEmail"].ToString();
            string to = WebConfigurationManager.AppSettings["NotificationsEmail"].ToString();

            var mail = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };
            mail.To.Add(to);

            SendEmail(mail);
        }

        public static void SendEmail(MailMessage mail)
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Send(mail);
        }


        public IList<Attachment> CreateAttachments(IEnumerable<string> attachmentFileNames)
        {
            if (attachmentFileNames == null || !attachmentFileNames.Any())
                return new List<Attachment>();

            return attachmentFileNames.Select(f => new Attachment(f)).ToList();
        }

        public void Send(string subject, string body, string from, string to, IEnumerable<Attachment> attachments)
        {
            var mail = new MailMessage()
            {
                From = new MailAddress(from),
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };
            mail.To.Add(to);

            if (attachments != null)
                foreach (Attachment attachment in attachments)
                    mail.Attachments.Add(attachment);

            SendEmail(mail);
        }
    }
}
