﻿using GPScout.Domain.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.DataImport.Contracts.Repositories
{
    public interface IRepository
    {
        IEnumerable<T> Select<T>() where T: class;
        int SaveChanges();
        T Delete<T>(T entity) where T : class;
        T Add<T>(T entity) where T : class;
        DataSyncResult BulkInsert<T>(IEnumerable<T> data, string destinationTableName);
        void TruncateStagingTables();
        void PostImport(DateTime DateImportedUtc);
        IEnumerable<string> GetAdditionalAccountIdsClause();
        void SetFundMetricImportStep(string mode, byte step);
        IEnumerable<Guid?> GetOrgsInProcessBeforeDataImport();
    }
}
