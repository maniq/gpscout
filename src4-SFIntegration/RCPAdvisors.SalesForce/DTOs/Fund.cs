﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class Fund
    {
        public string Id { get; set; }
        public string Currency__c { get; set; }
        public string Fundraising_Status__c { get; set; }
        public DateTime First_Drawn_Capital__c { get; set; }
        public decimal Fund_Total_Cash_on_Cash__c { get; set; }
        public decimal Fund_Total_IRR__c { get; set; }
        public string Total_Deal_Professionals__c { get; set; }
        public decimal FundTargetSize__c { get; set; }
        public decimal FundNumber__c { get; set; }
        public DateTime FundLaunchDate__c { get; set; }
        public decimal Eff_Size__c { get; set; }
    
    }
}
