﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPScout.Salesforce.DTOs
{
    public class GPScoutScorecard
    {
        public string Id { get; set; }
        public string Expertise_Aligned_with_Strategy__c { get; set; }
        public string History_Together__c { get; set; }
        public string Complementary_Skills__c { get; set; }
        public string Team_Depth__c { get; set; }
        public string Value_Add_Resources__c { get; set; }
        public string Investment_Thesis__c { get; set; }
        public string Competitive_Advantage__c { get; set; }
        public string Portfolio_Construction__c { get; set; }
        public string Appropriateness_of_Fund_Size__c { get; set; }
        public string Consistency_of_Strategy__c { get; set; }
        public string Sourcing__c { get; set; }
        public string Due_Diligence__c { get; set; }
        public string Decision_Making__c { get; set; }
        public string Deal_Execution_Structure__c { get; set; }
        public string Post_Investment_Value_add__c { get; set; }
        public string Team_Stability__c { get; set; }
        public string Ownership_and_Compensation__c { get; set; }
        public string Culture__c { get; set; }
        public string Alignment_with_LPs__c { get; set; }
        public string Terms__c { get; set; }
        public decimal Scaled_Score__c { get; set; }
        public string Absolute_Performance__c { get; set; }
        public string Relative_Performance__c { get; set; }
        public string Realized_Performance__c { get; set; }
        public string Depth_of_Track_Record__c { get; set; }
        public string Relevance_of_Track_Record__c { get; set; }
        public string Loss_Ratio_Analysis__c { get; set; }
        public string Unrealized_Portfolio__c { get; set; }
        public string RCP_Value_Creation_Analysis__c { get; set; }
        public string RCP_Hits_Misses__c { get; set; }
        public string Deal_Size__c { get; set; }
        public string Strategy__c { get; set; }
        public string Time__c { get; set; }
        public string Team__c { get; set; }
        public string Qualitative_Final_Grade__c { get; set; }
        public string Quantitative_Final_Grade__c { get; set; }
        public decimal Quantitative_Final_Number__c { get; set; }

    }
}
