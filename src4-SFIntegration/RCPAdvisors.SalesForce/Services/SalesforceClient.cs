﻿using GPScout.Salesforce.Contracts;
using SalesforceSharp;
using SalesforceSharp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _SalesforceClient = SalesforceSharp.SalesforceClient;

namespace GPScout.Salesforce
{
    public class SalesforceClient
    {
        #region Private
        private _SalesforceClient _client;
        private readonly ISalesforceInstanceInfo _instanceInfo;
        private readonly ISalesforceAuthenticationInfo _authenticationInfo;
        #endregion

        #region Protected
        protected _SalesforceClient Client
        {
            get
            {
                if (_client == null || !_client.IsAuthenticated)
                {
                    _client = new _SalesforceClient();
                    try
                    {
                        _client.ApiVersion = _instanceInfo.ApiVersion;
                        var authFlow = new UsernamePasswordAuthenticationFlow(_authenticationInfo.ConsumerKey, _authenticationInfo.ConsumerSecret, _authenticationInfo.UserName, _authenticationInfo.Password);
                        authFlow.TokenRequestEndpointUrl = _instanceInfo.TokenRequestUrl;

                        //authFlow.TokenRequestEndpointUrl = "https://test.salesforce.com";
                        _client.Authenticate(authFlow);

                        //_client.Query<Account>("", );
                        
                    }
                    catch (SalesforceException sfex)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                return _client;
            }
        }
        #endregion

        #region Ctor
        public SalesforceClient(ISalesforceInstanceInfo instanceInfo, ISalesforceAuthenticationInfo authenticationInfo)
        {
            _instanceInfo = instanceInfo;
            _authenticationInfo = authenticationInfo;
        }
        #endregion

        public void Test()
        {
            var client = this.Client;
        }
    }
}
