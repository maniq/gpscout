﻿using GPScout.DataImport.Contracts.Repositories;
using GPScout.DataImport.Contracts.Services;
using GPScout.Domain.Contracts.Enums;
using GPScout.Domain.Contracts.Models;
using GPScout.Salesforce.Contracts;
using SFP.Infrastructure.Logging.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Samples.EntityDataReader;
using GPScout.Salesforce.DTOs;

namespace GPScout.Salesforce.Services
{
    public class SalesforceImportManager: IDataImportPlugin
    {
        #region Private members
        private readonly ISalesforceClient _salesforceClient;
        private readonly IAppContext _appContext;
        //private readonly IWorkLogRepository _worklogRepository;
        //private readonly ISerializedDataRepository<ProgramAndCustomerSyncResult> _billingInfoRepository;
        private readonly IRepository _targetRepository;
        private readonly ILog _log;
        private readonly IResults _messages;
        //private readonly JiraImportMapping _mapper;
        private DateTime _dateImported;

        #endregion

        #region CTor

        public SalesforceImportManager(//SalesforceImportMapping mapper, 
            //IAppContext appContext,
            ILog log,
            ISalesforceClient salesforceClient, 
            //IWorkLogRepository worklogRepository,
            //ISerializedDataRepository<ProgramAndCustomerSyncResult> billingInfoRepository,
            IRepository targetRepository
            )
        {
            //_appContext = appContext;
            _log = log;
            _salesforceClient = salesforceClient;
            //_worklogRepository = worklogRepository;
            //_billingInfoRepository = billingInfoRepository;
            _targetRepository = targetRepository;
            //_messages = new Messages();
            //_mapper = mapper;


        }
        #endregion

        #region SalesforceImportManager
        public DateTime DateImported { get; set; }


        public bool Enabled()
        {
            return true;
            //return _appContext.JiraImportEnabled;
        }

        public IResults StartImport()
        {
            LogStart();            

            _targetRepository.Select<SFEntityImportConfig>().ToList().ForEach(entity => {

                var accounts = _salesforceClient.Query<Account>(entity.SelectQuery);
                
                //accounts.AsDataReader();
                //Import<Account>("", _salesforceClient.Query<Account>);
            });

            // reading non-paginated response
            //#region Roles
            //Import<JiraRole, int, TargetRole>("Role", _jiraClient.GetAllRoles, roleFromJira => roleFromJira.Id, existingRole => existingRole.JiraId);
            //#endregion

            //#region Users
            
            //IEnumerable<JiraUserEntity> usersFromJira;
            // TO DO import call
            //#endregion

            Log(ResultCode.DataImport_Salesforce_to_GPScout_Staging_Finished);

            return _messages;
        }
        #endregion

        #region Helper Methods

        public delegate IEnumerable<T> DataReaderDelegate<T>(int skip, int pageSize, out int total);



        private void Import<TEntity>(string entityFriendlyName, DataReaderDelegate<TEntity> getData)
        {
            IEnumerable<TEntity> sourceData = null;
            try
            {
                var syncResults = new DataSyncResult();
                int total = 1; 
                int skip = 0;
                while (skip < total)
                {
                    sourceData = getData(skip, /*_appContext.MaxNrOfUsersPerRequest*/50, out total).ToArray();
                    if (!sourceData.Any()) break;
                    skip += sourceData.Count();
                    //syncResults = syncResults + _targetRepository.Sync<JiraUserEntity, string, User>(usersFromJira, userFromJira => userFromJira.key, existingUser => existingUser.Key);
                    syncResults = syncResults + _targetRepository.BulkInsert<TEntity>();

                }
                LogCloudRead(entityFriendlyName, total);
                LogStagingWrite(entityFriendlyName, syncResults);
            }
            catch (Exception ex)
            {
                LogStagingError(ex, entityFriendlyName, sourceData);
            }
        }

        //private void Import<TJira, TKey, TDestination>(string entityName, Func<IEnumerable<TJira>> getFromCloud, Expression<Func<TJira, TKey>> jiraEntitySelector, Expression<Func<TDestination, TKey>> destEntitySelector)
        //    where TKey : IEquatable<TKey>
        //    where TDestination : class
        //{
        //    IEnumerable<TJira> fromJira = null;
        //    try
        //    {
        //        fromJira = getFromCloud();
        //        LogCloudRead(entityName, fromJira.Count());
        //        var syncResults = _targetRepository.Sync<TJira, TKey, TDestination>(fromJira, jiraEntitySelector, destEntitySelector);
        //        LogStagingWrite(entityName, syncResults);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError(ex, entityName, fromJira);
        //    }
        //}


        private void LogStagingError(Exception ex, string entityName, object source)
        {
            _messages.Add(ResultCode.DataImport_StagingError, entityName);
            _log.Write(ex);
            if (source == null)
            {
                var r = _messages.Add(ResultCode.DataImport_EntityNotTouchedInStaging, entityName);
                _log.Write(r.Message, r.ResultType.ToString());
            }
        }


        private void LogStagingWrite(string entityName, DataSyncResult syncResults)
        {
            var r = _messages.Add(ResultCode.DataImport_DataWrittenToStaging, syncResults.Updated, entityName, syncResults.Inserted);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        private void LogCloudRead(string entityName, int total)
        {
            var r = _messages.Add(ResultCode.DataImport_DataReadFromSource, total, entityName);
            _log.Write(r.Message, r.ResultType.ToString());
        }


        //private void LogInfo(string message)
        //{
        //    _messages.InfoMessages.Add(message);
        //    _log.Write(message, Category.JiraCloudImportStaging, TraceEventType.Information);
        //}


        private void Log(ResultCode code)
        {
            var r = _messages.Add(code);
            _log.Write(r.Message, r.ResultType.ToString());
        }

        private void LogStart()
        {
            var result = _messages.Add(ResultCode.InfoMsg, DateImported);
            _log.Write( result.Message, result.ResultType.ToString());
        }
        #endregion
    }
}
