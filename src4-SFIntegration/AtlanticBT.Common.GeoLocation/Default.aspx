﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AtlanticBT.Common.GeoLocation._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="btnSendRequest" runat="server" onclick="btnSendRequest_Click" 
            Text="Send Request" />
        <br />
        <br />
        <asp:Label ID="lblRAWText" runat="server"></asp:Label>
    
        <br />
        <br />
        <br />
        IP Address:
        <asp:Label ID="lblMyIPAddress" runat="server"></asp:Label>
    
        <br />
        <br />
        Country Code:         <asp:Label ID="lblCountryCode" runat="server"></asp:Label>
    
        <br />
        Country Name:
        <asp:Label ID="lblCountryName" runat="server"></asp:Label>
    
        <br />
        <br />
        State:
        <asp:Label ID="lblRegionName" runat="server"></asp:Label>
    
        <br />
        City:
        <asp:Label ID="lblCity" runat="server"></asp:Label>
    
        <br />
        Zipcode:
        <asp:Label ID="lblZipPostalCode" runat="server"></asp:Label>
    
        <br />
        <br />
        TimeZone Name:
        <asp:Label ID="lblTimezoneName" runat="server"></asp:Label>
    
        <br />
        GMT offset:
        <asp:Label ID="lblGmtoffset" runat="server"></asp:Label>
    
        <br />
        SToffSet:
        <asp:Label ID="lblDstoffset" runat="server"></asp:Label>
    
        <br />
        <br />
        Latitude:
        <asp:Label ID="lblLatitude" runat="server"></asp:Label>
    
        <br />
        Longitude:
        <asp:Label ID="lblLongitude" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>
