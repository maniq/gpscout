﻿using System.Runtime.Serialization;
namespace AtlanticBT.Common.GeoLocation
{
    [DataContract]
    public class Location
    {
        public string Status { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public string ZipPostalCode { get; set; }
        public TimeZone TimeZone { get; set; }

        [DataMember(Name = "lat")]
        public decimal Latitude { get; set; }

        [DataMember(Name = "lng")]
        public decimal Longitude { get; set; }
    }
}