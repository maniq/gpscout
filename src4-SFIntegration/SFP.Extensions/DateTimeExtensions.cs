﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFP.Extensions
{
    public class DateRange
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public static class DateTimeExtensions
    {
        public static DateRange PreviousFullWeek(this DateTime? date)
        {
            DateTime? dt = date;
            if (!dt.HasValue) dt = DateTime.Now;
            dt = dt.Value.Date;
            var fdow = System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek;
            var sinceLastDayOfPreviousWeek = dt.Value.DayOfWeek - fdow + 1;

            var dateRange = new DateRange
            {
                EndDate = dt.Value.AddDays(-sinceLastDayOfPreviousWeek)
            };

            dateRange.StartDate = dateRange.EndDate.Value.AddDays(-6);
            dateRange.EndDate = dateRange.EndDate.Value.AddDays(1).AddMilliseconds(-1);
            return dateRange;
        }


        public static DateRange PreviousWeek(this DateTime? date)
        {
            DateTime? dt = date;
            if (!dt.HasValue) dt = DateTime.Now;
            dt = dt.Value.Date;

            var dateRange = new DateRange
            {
                EndDate = dt.Value.AddDays(-1)
            };

            dateRange.StartDate = dateRange.EndDate.Value.AddDays(-6);
            dateRange.EndDate = dateRange.EndDate.Value.AddDays(1).AddMilliseconds(-1);
            return dateRange;
        }
    }
}
